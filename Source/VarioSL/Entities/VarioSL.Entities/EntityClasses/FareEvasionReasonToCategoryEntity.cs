﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareEvasionReasonToCategory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareEvasionReasonToCategoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private FareEvasionCategoryEntity _fareEvasionCategory;
		private bool	_alwaysFetchFareEvasionCategory, _alreadyFetchedFareEvasionCategory, _fareEvasionCategoryReturnsNewIfNotFound;
		private FareEvasionReasonEntity _fareEvasionReason;
		private bool	_alwaysFetchFareEvasionReason, _alreadyFetchedFareEvasionReason, _fareEvasionReasonReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FareEvasionCategory</summary>
			public static readonly string FareEvasionCategory = "FareEvasionCategory";
			/// <summary>Member name FareEvasionReason</summary>
			public static readonly string FareEvasionReason = "FareEvasionReason";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareEvasionReasonToCategoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareEvasionReasonToCategoryEntity() :base("FareEvasionReasonToCategoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		public FareEvasionReasonToCategoryEntity(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID):base("FareEvasionReasonToCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, fareEvasionReasonID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareEvasionReasonToCategoryEntity(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IPrefetchPath prefetchPathToUse):base("FareEvasionReasonToCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, fareEvasionReasonID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="validator">The custom validator object for this FareEvasionReasonToCategoryEntity</param>
		public FareEvasionReasonToCategoryEntity(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IValidator validator):base("FareEvasionReasonToCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, fareEvasionReasonID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareEvasionReasonToCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareEvasionCategory = (FareEvasionCategoryEntity)info.GetValue("_fareEvasionCategory", typeof(FareEvasionCategoryEntity));
			if(_fareEvasionCategory!=null)
			{
				_fareEvasionCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionCategoryReturnsNewIfNotFound = info.GetBoolean("_fareEvasionCategoryReturnsNewIfNotFound");
			_alwaysFetchFareEvasionCategory = info.GetBoolean("_alwaysFetchFareEvasionCategory");
			_alreadyFetchedFareEvasionCategory = info.GetBoolean("_alreadyFetchedFareEvasionCategory");

			_fareEvasionReason = (FareEvasionReasonEntity)info.GetValue("_fareEvasionReason", typeof(FareEvasionReasonEntity));
			if(_fareEvasionReason!=null)
			{
				_fareEvasionReason.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionReasonReturnsNewIfNotFound = info.GetBoolean("_fareEvasionReasonReturnsNewIfNotFound");
			_alwaysFetchFareEvasionReason = info.GetBoolean("_alwaysFetchFareEvasionReason");
			_alreadyFetchedFareEvasionReason = info.GetBoolean("_alreadyFetchedFareEvasionReason");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareEvasionReasonToCategoryFieldIndex)fieldIndex)
			{
				case FareEvasionReasonToCategoryFieldIndex.FareEvasionCategoryID:
					DesetupSyncFareEvasionCategory(true, false);
					_alreadyFetchedFareEvasionCategory = false;
					break;
				case FareEvasionReasonToCategoryFieldIndex.FareEvasionReasonID:
					DesetupSyncFareEvasionReason(true, false);
					_alreadyFetchedFareEvasionReason = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareEvasionCategory = (_fareEvasionCategory != null);
			_alreadyFetchedFareEvasionReason = (_fareEvasionReason != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FareEvasionCategory":
					toReturn.Add(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
					break;
				case "FareEvasionReason":
					toReturn.Add(Relations.FareEvasionReasonEntityUsingFareEvasionReasonID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareEvasionCategory", (!this.MarkedForDeletion?_fareEvasionCategory:null));
			info.AddValue("_fareEvasionCategoryReturnsNewIfNotFound", _fareEvasionCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionCategory", _alwaysFetchFareEvasionCategory);
			info.AddValue("_alreadyFetchedFareEvasionCategory", _alreadyFetchedFareEvasionCategory);
			info.AddValue("_fareEvasionReason", (!this.MarkedForDeletion?_fareEvasionReason:null));
			info.AddValue("_fareEvasionReasonReturnsNewIfNotFound", _fareEvasionReasonReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionReason", _alwaysFetchFareEvasionReason);
			info.AddValue("_alreadyFetchedFareEvasionReason", _alreadyFetchedFareEvasionReason);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FareEvasionCategory":
					_alreadyFetchedFareEvasionCategory = true;
					this.FareEvasionCategory = (FareEvasionCategoryEntity)entity;
					break;
				case "FareEvasionReason":
					_alreadyFetchedFareEvasionReason = true;
					this.FareEvasionReason = (FareEvasionReasonEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FareEvasionCategory":
					SetupSyncFareEvasionCategory(relatedEntity);
					break;
				case "FareEvasionReason":
					SetupSyncFareEvasionReason(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FareEvasionCategory":
					DesetupSyncFareEvasionCategory(false, true);
					break;
				case "FareEvasionReason":
					DesetupSyncFareEvasionReason(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fareEvasionCategory!=null)
			{
				toReturn.Add(_fareEvasionCategory);
			}
			if(_fareEvasionReason!=null)
			{
				toReturn.Add(_fareEvasionReason);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID)
		{
			return FetchUsingPK(fareEvasionCategoryID, fareEvasionReasonID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareEvasionCategoryID, fareEvasionReasonID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareEvasionCategoryID, fareEvasionReasonID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareEvasionCategoryID, fareEvasionReasonID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareEvasionCategoryID, this.FareEvasionReasonID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareEvasionReasonToCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public FareEvasionCategoryEntity GetSingleFareEvasionCategory()
		{
			return GetSingleFareEvasionCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public virtual FareEvasionCategoryEntity GetSingleFareEvasionCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionCategory || forceFetch || _alwaysFetchFareEvasionCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
				FareEvasionCategoryEntity newEntity = new FareEvasionCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionCategoryID);
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionCategory = newEntity;
				_alreadyFetchedFareEvasionCategory = fetchResult;
			}
			return _fareEvasionCategory;
		}


		/// <summary> Retrieves the related entity of type 'FareEvasionReasonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionReasonEntity' which is related to this entity.</returns>
		public FareEvasionReasonEntity GetSingleFareEvasionReason()
		{
			return GetSingleFareEvasionReason(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionReasonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionReasonEntity' which is related to this entity.</returns>
		public virtual FareEvasionReasonEntity GetSingleFareEvasionReason(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionReason || forceFetch || _alwaysFetchFareEvasionReason) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionReasonEntityUsingFareEvasionReasonID);
				FareEvasionReasonEntity newEntity = new FareEvasionReasonEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionReasonID);
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionReasonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionReasonReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionReason = newEntity;
				_alreadyFetchedFareEvasionReason = fetchResult;
			}
			return _fareEvasionReason;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FareEvasionCategory", _fareEvasionCategory);
			toReturn.Add("FareEvasionReason", _fareEvasionReason);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="validator">The validator object for this FareEvasionReasonToCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareEvasionCategoryID, fareEvasionReasonID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_fareEvasionCategoryReturnsNewIfNotFound = false;
			_fareEvasionReasonReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionReasonID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fareEvasionCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticFareEvasionReasonToCategoryRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, signalRelatedEntity, "FareEvasionReasonToCategories", resetFKFields, new int[] { (int)FareEvasionReasonToCategoryFieldIndex.FareEvasionCategoryID } );		
			_fareEvasionCategory = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionCategory(IEntityCore relatedEntity)
		{
			if(_fareEvasionCategory!=relatedEntity)
			{		
				DesetupSyncFareEvasionCategory(true, true);
				_fareEvasionCategory = (FareEvasionCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticFareEvasionReasonToCategoryRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, ref _alreadyFetchedFareEvasionCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareEvasionReason</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionReason(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionReason, new PropertyChangedEventHandler( OnFareEvasionReasonPropertyChanged ), "FareEvasionReason", VarioSL.Entities.RelationClasses.StaticFareEvasionReasonToCategoryRelations.FareEvasionReasonEntityUsingFareEvasionReasonIDStatic, true, signalRelatedEntity, "FareEvasionReasonToCategories", resetFKFields, new int[] { (int)FareEvasionReasonToCategoryFieldIndex.FareEvasionReasonID } );		
			_fareEvasionReason = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionReason</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionReason(IEntityCore relatedEntity)
		{
			if(_fareEvasionReason!=relatedEntity)
			{		
				DesetupSyncFareEvasionReason(true, true);
				_fareEvasionReason = (FareEvasionReasonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionReason, new PropertyChangedEventHandler( OnFareEvasionReasonPropertyChanged ), "FareEvasionReason", VarioSL.Entities.RelationClasses.StaticFareEvasionReasonToCategoryRelations.FareEvasionReasonEntityUsingFareEvasionReasonIDStatic, true, ref _alreadyFetchedFareEvasionReason, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionReasonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="fareEvasionReasonID">PK value for FareEvasionReasonToCategory which data should be fetched into this FareEvasionReasonToCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareEvasionCategoryID, System.Int64 fareEvasionReasonID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareEvasionReasonToCategoryFieldIndex.FareEvasionCategoryID].ForcedCurrentValueWrite(fareEvasionCategoryID);
				this.Fields[(int)FareEvasionReasonToCategoryFieldIndex.FareEvasionReasonID].ForcedCurrentValueWrite(fareEvasionReasonID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareEvasionReasonToCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareEvasionReasonToCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareEvasionReasonToCategoryRelations Relations
		{
			get	{ return new FareEvasionReasonToCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection(), (IEntityRelation)GetRelationsForField("FareEvasionCategory")[0], (int)VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity, (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, 0, null, null, null, "FareEvasionCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionReason'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionReason
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection(), (IEntityRelation)GetRelationsForField("FareEvasionReason")[0], (int)VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity, (int)VarioSL.Entities.EntityType.FareEvasionReasonEntity, 0, null, null, null, "FareEvasionReason", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FareEvasionCategoryID property of the Entity FareEvasionReasonToCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONREASONTOCATEGORY"."FAREEVASIONCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareEvasionCategoryID
		{
			get { return (System.Int64)GetValue((int)FareEvasionReasonToCategoryFieldIndex.FareEvasionCategoryID, true); }
			set	{ SetValue((int)FareEvasionReasonToCategoryFieldIndex.FareEvasionCategoryID, value, true); }
		}

		/// <summary> The FareEvasionReasonID property of the Entity FareEvasionReasonToCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONREASONTOCATEGORY"."FAREEVASIONREASONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareEvasionReasonID
		{
			get { return (System.Int64)GetValue((int)FareEvasionReasonToCategoryFieldIndex.FareEvasionReasonID, true); }
			set	{ SetValue((int)FareEvasionReasonToCategoryFieldIndex.FareEvasionReasonID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'FareEvasionCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionCategoryEntity FareEvasionCategory
		{
			get	{ return GetSingleFareEvasionCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionReasonToCategories", "FareEvasionCategory", _fareEvasionCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionCategory. When set to true, FareEvasionCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionCategory is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionCategory
		{
			get	{ return _alwaysFetchFareEvasionCategory; }
			set	{ _alwaysFetchFareEvasionCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionCategory already has been fetched. Setting this property to false when FareEvasionCategory has been fetched
		/// will set FareEvasionCategory to null as well. Setting this property to true while FareEvasionCategory hasn't been fetched disables lazy loading for FareEvasionCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionCategory
		{
			get { return _alreadyFetchedFareEvasionCategory;}
			set 
			{
				if(_alreadyFetchedFareEvasionCategory && !value)
				{
					this.FareEvasionCategory = null;
				}
				_alreadyFetchedFareEvasionCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionCategory is not found
		/// in the database. When set to true, FareEvasionCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionCategoryReturnsNewIfNotFound
		{
			get	{ return _fareEvasionCategoryReturnsNewIfNotFound; }
			set { _fareEvasionCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionReasonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionReason()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionReasonEntity FareEvasionReason
		{
			get	{ return GetSingleFareEvasionReason(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionReason(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionReasonToCategories", "FareEvasionReason", _fareEvasionReason, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionReason. When set to true, FareEvasionReason is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionReason is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionReason(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionReason
		{
			get	{ return _alwaysFetchFareEvasionReason; }
			set	{ _alwaysFetchFareEvasionReason = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionReason already has been fetched. Setting this property to false when FareEvasionReason has been fetched
		/// will set FareEvasionReason to null as well. Setting this property to true while FareEvasionReason hasn't been fetched disables lazy loading for FareEvasionReason</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionReason
		{
			get { return _alreadyFetchedFareEvasionReason;}
			set 
			{
				if(_alreadyFetchedFareEvasionReason && !value)
				{
					this.FareEvasionReason = null;
				}
				_alreadyFetchedFareEvasionReason = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionReason is not found
		/// in the database. When set to true, FareEvasionReason will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionReasonReturnsNewIfNotFound
		{
			get	{ return _fareEvasionReasonReturnsNewIfNotFound; }
			set { _fareEvasionReasonReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
