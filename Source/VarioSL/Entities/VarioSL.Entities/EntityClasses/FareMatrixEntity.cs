﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareMatrix'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareMatrixEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareMatrixCollection	_fareMatrices;
		private bool	_alwaysFetchFareMatrices, _alreadyFetchedFareMatrices;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_fareMatrixEntry;
		private bool	_alwaysFetchFareMatrixEntry, _alreadyFetchedFareMatrixEntry;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceClass;
		private bool	_alwaysFetchTicketDeviceClass, _alreadyFetchedTicketDeviceClass;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private FareMatrixEntity _fareMatrix;
		private bool	_alwaysFetchFareMatrix, _alreadyFetchedFareMatrix, _fareMatrixReturnsNewIfNotFound;
		private FareStageListEntity _fareStageList;
		private bool	_alwaysFetchFareStageList, _alreadyFetchedFareStageList, _fareStageListReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name FareMatrix</summary>
			public static readonly string FareMatrix = "FareMatrix";
			/// <summary>Member name FareStageList</summary>
			public static readonly string FareStageList = "FareStageList";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name FareMatrices</summary>
			public static readonly string FareMatrices = "FareMatrices";
			/// <summary>Member name FareMatrixEntry</summary>
			public static readonly string FareMatrixEntry = "FareMatrixEntry";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketDeviceClass</summary>
			public static readonly string TicketDeviceClass = "TicketDeviceClass";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareMatrixEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareMatrixEntity() :base("FareMatrixEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		public FareMatrixEntity(System.Int64 fareMatrixID):base("FareMatrixEntity")
		{
			InitClassFetch(fareMatrixID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareMatrixEntity(System.Int64 fareMatrixID, IPrefetchPath prefetchPathToUse):base("FareMatrixEntity")
		{
			InitClassFetch(fareMatrixID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="validator">The custom validator object for this FareMatrixEntity</param>
		public FareMatrixEntity(System.Int64 fareMatrixID, IValidator validator):base("FareMatrixEntity")
		{
			InitClassFetch(fareMatrixID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareMatrixEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareMatrices = (VarioSL.Entities.CollectionClasses.FareMatrixCollection)info.GetValue("_fareMatrices", typeof(VarioSL.Entities.CollectionClasses.FareMatrixCollection));
			_alwaysFetchFareMatrices = info.GetBoolean("_alwaysFetchFareMatrices");
			_alreadyFetchedFareMatrices = info.GetBoolean("_alreadyFetchedFareMatrices");

			_fareMatrixEntry = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_fareMatrixEntry", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchFareMatrixEntry = info.GetBoolean("_alwaysFetchFareMatrixEntry");
			_alreadyFetchedFareMatrixEntry = info.GetBoolean("_alreadyFetchedFareMatrixEntry");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketDeviceClass = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClass", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClass = info.GetBoolean("_alwaysFetchTicketDeviceClass");
			_alreadyFetchedTicketDeviceClass = info.GetBoolean("_alreadyFetchedTicketDeviceClass");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_fareMatrix = (FareMatrixEntity)info.GetValue("_fareMatrix", typeof(FareMatrixEntity));
			if(_fareMatrix!=null)
			{
				_fareMatrix.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareMatrixReturnsNewIfNotFound = info.GetBoolean("_fareMatrixReturnsNewIfNotFound");
			_alwaysFetchFareMatrix = info.GetBoolean("_alwaysFetchFareMatrix");
			_alreadyFetchedFareMatrix = info.GetBoolean("_alreadyFetchedFareMatrix");

			_fareStageList = (FareStageListEntity)info.GetValue("_fareStageList", typeof(FareStageListEntity));
			if(_fareStageList!=null)
			{
				_fareStageList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageListReturnsNewIfNotFound = info.GetBoolean("_fareStageListReturnsNewIfNotFound");
			_alwaysFetchFareStageList = info.GetBoolean("_alwaysFetchFareStageList");
			_alreadyFetchedFareStageList = info.GetBoolean("_alreadyFetchedFareStageList");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareMatrixFieldIndex)fieldIndex)
			{
				case FareMatrixFieldIndex.FareStageListID:
					DesetupSyncFareStageList(true, false);
					_alreadyFetchedFareStageList = false;
					break;
				case FareMatrixFieldIndex.MasterMatrixID:
					DesetupSyncFareMatrix(true, false);
					_alreadyFetchedFareMatrix = false;
					break;
				case FareMatrixFieldIndex.OwnerClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case FareMatrixFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareMatrices = (_fareMatrices.Count > 0);
			_alreadyFetchedFareMatrixEntry = (_fareMatrixEntry.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketDeviceClass = (_ticketDeviceClass.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedFareMatrix = (_fareMatrix != null);
			_alreadyFetchedFareStageList = (_fareStageList != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientID);
					break;
				case "FareMatrix":
					toReturn.Add(Relations.FareMatrixEntityUsingFareMatrixIDMasterMatrixID);
					break;
				case "FareStageList":
					toReturn.Add(Relations.FareStageListEntityUsingFareStageListID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "FareMatrices":
					toReturn.Add(Relations.FareMatrixEntityUsingMasterMatrixID);
					break;
				case "FareMatrixEntry":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingFareMatrixID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingMatrixID);
					break;
				case "TicketDeviceClass":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingMatrixID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareMatrices", (!this.MarkedForDeletion?_fareMatrices:null));
			info.AddValue("_alwaysFetchFareMatrices", _alwaysFetchFareMatrices);
			info.AddValue("_alreadyFetchedFareMatrices", _alreadyFetchedFareMatrices);
			info.AddValue("_fareMatrixEntry", (!this.MarkedForDeletion?_fareMatrixEntry:null));
			info.AddValue("_alwaysFetchFareMatrixEntry", _alwaysFetchFareMatrixEntry);
			info.AddValue("_alreadyFetchedFareMatrixEntry", _alreadyFetchedFareMatrixEntry);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketDeviceClass", (!this.MarkedForDeletion?_ticketDeviceClass:null));
			info.AddValue("_alwaysFetchTicketDeviceClass", _alwaysFetchTicketDeviceClass);
			info.AddValue("_alreadyFetchedTicketDeviceClass", _alreadyFetchedTicketDeviceClass);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_fareMatrix", (!this.MarkedForDeletion?_fareMatrix:null));
			info.AddValue("_fareMatrixReturnsNewIfNotFound", _fareMatrixReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareMatrix", _alwaysFetchFareMatrix);
			info.AddValue("_alreadyFetchedFareMatrix", _alreadyFetchedFareMatrix);
			info.AddValue("_fareStageList", (!this.MarkedForDeletion?_fareStageList:null));
			info.AddValue("_fareStageListReturnsNewIfNotFound", _fareStageListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageList", _alwaysFetchFareStageList);
			info.AddValue("_alreadyFetchedFareStageList", _alreadyFetchedFareStageList);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "FareMatrix":
					_alreadyFetchedFareMatrix = true;
					this.FareMatrix = (FareMatrixEntity)entity;
					break;
				case "FareStageList":
					_alreadyFetchedFareStageList = true;
					this.FareStageList = (FareStageListEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "FareMatrices":
					_alreadyFetchedFareMatrices = true;
					if(entity!=null)
					{
						this.FareMatrices.Add((FareMatrixEntity)entity);
					}
					break;
				case "FareMatrixEntry":
					_alreadyFetchedFareMatrixEntry = true;
					if(entity!=null)
					{
						this.FareMatrixEntry.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceClass":
					_alreadyFetchedTicketDeviceClass = true;
					if(entity!=null)
					{
						this.TicketDeviceClass.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "FareMatrix":
					SetupSyncFareMatrix(relatedEntity);
					break;
				case "FareStageList":
					SetupSyncFareStageList(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "FareMatrices":
					_fareMatrices.Add((FareMatrixEntity)relatedEntity);
					break;
				case "FareMatrixEntry":
					_fareMatrixEntry.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceClass":
					_ticketDeviceClass.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "FareMatrix":
					DesetupSyncFareMatrix(false, true);
					break;
				case "FareStageList":
					DesetupSyncFareStageList(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "FareMatrices":
					this.PerformRelatedEntityRemoval(_fareMatrices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrixEntry":
					this.PerformRelatedEntityRemoval(_fareMatrixEntry, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClass":
					this.PerformRelatedEntityRemoval(_ticketDeviceClass, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_fareMatrix!=null)
			{
				toReturn.Add(_fareMatrix);
			}
			if(_fareStageList!=null)
			{
				toReturn.Add(_fareStageList);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareMatrices);
			toReturn.Add(_fareMatrixEntry);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketDeviceClass);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixID)
		{
			return FetchUsingPK(fareMatrixID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareMatrixID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareMatrixID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareMatrixID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareMatrixID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareMatrixID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareMatrixRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch)
		{
			return GetMultiFareMatrices(forceFetch, _fareMatrices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrices(forceFetch, _fareMatrices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrices || forceFetch || _alwaysFetchFareMatrices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrices);
				_fareMatrices.SuppressClearInGetMulti=!forceFetch;
				_fareMatrices.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrices.GetMultiManyToOne(null, this, null, null, filter);
				_fareMatrices.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrices = true;
			}
			return _fareMatrices;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrices'. These settings will be taken into account
		/// when the property FareMatrices is requested or GetMultiFareMatrices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrices.SortClauses=sortClauses;
			_fareMatrices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch)
		{
			return GetMultiFareMatrixEntry(forceFetch, _fareMatrixEntry.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixEntry(forceFetch, _fareMatrixEntry.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixEntry(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixEntry || forceFetch || _alwaysFetchFareMatrixEntry) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixEntry);
				_fareMatrixEntry.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixEntry.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixEntry.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_fareMatrixEntry.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixEntry = true;
			}
			return _fareMatrixEntry;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixEntry'. These settings will be taken into account
		/// when the property FareMatrixEntry is requested or GetMultiFareMatrixEntry is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixEntry(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixEntry.SortClauses=sortClauses;
			_fareMatrixEntry.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClass(forceFetch, _ticketDeviceClass.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClass(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClass(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClass || forceFetch || _alwaysFetchTicketDeviceClass) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClass);
				_ticketDeviceClass.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClass.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClass.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_ticketDeviceClass.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClass = true;
			}
			return _ticketDeviceClass;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClass'. These settings will be taken into account
		/// when the property TicketDeviceClass is requested or GetMultiTicketDeviceClass is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClass(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClass.SortClauses=sortClauses;
			_ticketDeviceClass.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public FareMatrixEntity GetSingleFareMatrix()
		{
			return GetSingleFareMatrix(false);
		}

		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public virtual FareMatrixEntity GetSingleFareMatrix(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareMatrix || forceFetch || _alwaysFetchFareMatrix) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareMatrixEntityUsingFareMatrixIDMasterMatrixID);
				FareMatrixEntity newEntity = new FareMatrixEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MasterMatrixID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareMatrixEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareMatrixReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareMatrix = newEntity;
				_alreadyFetchedFareMatrix = fetchResult;
			}
			return _fareMatrix;
		}


		/// <summary> Retrieves the related entity of type 'FareStageListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageListEntity' which is related to this entity.</returns>
		public FareStageListEntity GetSingleFareStageList()
		{
			return GetSingleFareStageList(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageListEntity' which is related to this entity.</returns>
		public virtual FareStageListEntity GetSingleFareStageList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageList || forceFetch || _alwaysFetchFareStageList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageListEntityUsingFareStageListID);
				FareStageListEntity newEntity = new FareStageListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageListID);
				}
				if(fetchResult)
				{
					newEntity = (FareStageListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageList = newEntity;
				_alreadyFetchedFareStageList = fetchResult;
			}
			return _fareStageList;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("FareMatrix", _fareMatrix);
			toReturn.Add("FareStageList", _fareStageList);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("FareMatrices", _fareMatrices);
			toReturn.Add("FareMatrixEntry", _fareMatrixEntry);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketDeviceClass", _ticketDeviceClass);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="validator">The validator object for this FareMatrixEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareMatrixID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareMatrixID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareMatrices = new VarioSL.Entities.CollectionClasses.FareMatrixCollection();
			_fareMatrices.SetContainingEntityInfo(this, "FareMatrix");

			_fareMatrixEntry = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_fareMatrixEntry.SetContainingEntityInfo(this, "FareMatrix");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "FareMatrix");

			_ticketDeviceClass = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceClass.SetContainingEntityInfo(this, "FareMatrix");
			_clientReturnsNewIfNotFound = false;
			_fareMatrixReturnsNewIfNotFound = false;
			_fareStageListReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Depth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMatrixID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Formula", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterMatrixID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.ClientEntityUsingOwnerClientIDStatic, true, signalRelatedEntity, "FareMatrixes", resetFKFields, new int[] { (int)FareMatrixFieldIndex.OwnerClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.ClientEntityUsingOwnerClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareMatrix</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareMatrix(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.FareMatrixEntityUsingFareMatrixIDMasterMatrixIDStatic, true, signalRelatedEntity, "FareMatrices", resetFKFields, new int[] { (int)FareMatrixFieldIndex.MasterMatrixID } );		
			_fareMatrix = null;
		}
		
		/// <summary> setups the sync logic for member _fareMatrix</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareMatrix(IEntityCore relatedEntity)
		{
			if(_fareMatrix!=relatedEntity)
			{		
				DesetupSyncFareMatrix(true, true);
				_fareMatrix = (FareMatrixEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.FareMatrixEntityUsingFareMatrixIDMasterMatrixIDStatic, true, ref _alreadyFetchedFareMatrix, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareMatrixPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageList, new PropertyChangedEventHandler( OnFareStageListPropertyChanged ), "FareStageList", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.FareStageListEntityUsingFareStageListIDStatic, true, signalRelatedEntity, "Farematrices", resetFKFields, new int[] { (int)FareMatrixFieldIndex.FareStageListID } );		
			_fareStageList = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageList(IEntityCore relatedEntity)
		{
			if(_fareStageList!=relatedEntity)
			{		
				DesetupSyncFareStageList(true, true);
				_fareStageList = (FareStageListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageList, new PropertyChangedEventHandler( OnFareStageListPropertyChanged ), "FareStageList", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.FareStageListEntityUsingFareStageListIDStatic, true, ref _alreadyFetchedFareStageList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "FareMatrices", resetFKFields, new int[] { (int)FareMatrixFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareMatrixRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareMatrixID">PK value for FareMatrix which data should be fetched into this FareMatrix object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareMatrixID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareMatrixFieldIndex.FareMatrixID].ForcedCurrentValueWrite(fareMatrixID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareMatrixDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareMatrixEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareMatrixRelations Relations
		{
			get	{ return new FareMatrixRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrices")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixEntry
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("FareMatrixEntry")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "FareMatrixEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClass
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClass")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrix
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrix")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrix", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageListCollection(), (IEntityRelation)GetRelationsForField("FareStageList")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.FareStageListEntity, 0, null, null, null, "FareStageList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.FareMatrixEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Depth property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."DEPTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Depth
		{
			get { return (Nullable<System.Int16>)GetValue((int)FareMatrixFieldIndex.Depth, false); }
			set	{ SetValue((int)FareMatrixFieldIndex.Depth, value, true); }
		}

		/// <summary> The FareMatrixID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."FAREMATRIXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareMatrixID
		{
			get { return (System.Int64)GetValue((int)FareMatrixFieldIndex.FareMatrixID, true); }
			set	{ SetValue((int)FareMatrixFieldIndex.FareMatrixID, value, true); }
		}

		/// <summary> The FareStageListID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."FARESTAGELISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FareStageListID
		{
			get { return (System.Int64)GetValue((int)FareMatrixFieldIndex.FareStageListID, true); }
			set	{ SetValue((int)FareMatrixFieldIndex.FareStageListID, value, true); }
		}

		/// <summary> The Formula property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."FORMULA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Formula
		{
			get { return (System.String)GetValue((int)FareMatrixFieldIndex.Formula, true); }
			set	{ SetValue((int)FareMatrixFieldIndex.Formula, value, true); }
		}

		/// <summary> The MasterMatrixID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."MASTERMATRIXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MasterMatrixID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixFieldIndex.MasterMatrixID, false); }
			set	{ SetValue((int)FareMatrixFieldIndex.MasterMatrixID, value, true); }
		}

		/// <summary> The Name property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareMatrixFieldIndex.Name, true); }
			set	{ SetValue((int)FareMatrixFieldIndex.Name, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixFieldIndex.OwnerClientID, false); }
			set	{ SetValue((int)FareMatrixFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The ScaleID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."SCALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ScaleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareMatrixFieldIndex.ScaleID, false); }
			set	{ SetValue((int)FareMatrixFieldIndex.ScaleID, value, true); }
		}

		/// <summary> The TariffID property of the Entity FareMatrix<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREMATRIX"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)FareMatrixFieldIndex.TariffID, true); }
			set	{ SetValue((int)FareMatrixFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection FareMatrices
		{
			get	{ return GetMultiFareMatrices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrices. When set to true, FareMatrices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrices is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrices
		{
			get	{ return _alwaysFetchFareMatrices; }
			set	{ _alwaysFetchFareMatrices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrices already has been fetched. Setting this property to false when FareMatrices has been fetched
		/// will clear the FareMatrices collection well. Setting this property to true while FareMatrices hasn't been fetched disables lazy loading for FareMatrices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrices
		{
			get { return _alreadyFetchedFareMatrices;}
			set 
			{
				if(_alreadyFetchedFareMatrices && !value && (_fareMatrices != null))
				{
					_fareMatrices.Clear();
				}
				_alreadyFetchedFareMatrices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection FareMatrixEntry
		{
			get	{ return GetMultiFareMatrixEntry(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixEntry. When set to true, FareMatrixEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixEntry is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixEntry
		{
			get	{ return _alwaysFetchFareMatrixEntry; }
			set	{ _alwaysFetchFareMatrixEntry = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixEntry already has been fetched. Setting this property to false when FareMatrixEntry has been fetched
		/// will clear the FareMatrixEntry collection well. Setting this property to true while FareMatrixEntry hasn't been fetched disables lazy loading for FareMatrixEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixEntry
		{
			get { return _alreadyFetchedFareMatrixEntry;}
			set 
			{
				if(_alreadyFetchedFareMatrixEntry && !value && (_fareMatrixEntry != null))
				{
					_fareMatrixEntry.Clear();
				}
				_alreadyFetchedFareMatrixEntry = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClass
		{
			get	{ return GetMultiTicketDeviceClass(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClass. When set to true, TicketDeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClass is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClass
		{
			get	{ return _alwaysFetchTicketDeviceClass; }
			set	{ _alwaysFetchTicketDeviceClass = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClass already has been fetched. Setting this property to false when TicketDeviceClass has been fetched
		/// will clear the TicketDeviceClass collection well. Setting this property to true while TicketDeviceClass hasn't been fetched disables lazy loading for TicketDeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClass
		{
			get { return _alreadyFetchedTicketDeviceClass;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClass && !value && (_ticketDeviceClass != null))
				{
					_ticketDeviceClass.Clear();
				}
				_alreadyFetchedTicketDeviceClass = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrixes", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareMatrixEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareMatrix()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareMatrixEntity FareMatrix
		{
			get	{ return GetSingleFareMatrix(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareMatrix(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrices", "FareMatrix", _fareMatrix, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrix. When set to true, FareMatrix is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrix is accessed. You can always execute a forced fetch by calling GetSingleFareMatrix(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrix
		{
			get	{ return _alwaysFetchFareMatrix; }
			set	{ _alwaysFetchFareMatrix = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrix already has been fetched. Setting this property to false when FareMatrix has been fetched
		/// will set FareMatrix to null as well. Setting this property to true while FareMatrix hasn't been fetched disables lazy loading for FareMatrix</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrix
		{
			get { return _alreadyFetchedFareMatrix;}
			set 
			{
				if(_alreadyFetchedFareMatrix && !value)
				{
					this.FareMatrix = null;
				}
				_alreadyFetchedFareMatrix = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareMatrix is not found
		/// in the database. When set to true, FareMatrix will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareMatrixReturnsNewIfNotFound
		{
			get	{ return _fareMatrixReturnsNewIfNotFound; }
			set { _fareMatrixReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageListEntity FareStageList
		{
			get	{ return GetSingleFareStageList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Farematrices", "FareStageList", _fareStageList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageList. When set to true, FareStageList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageList is accessed. You can always execute a forced fetch by calling GetSingleFareStageList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageList
		{
			get	{ return _alwaysFetchFareStageList; }
			set	{ _alwaysFetchFareStageList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageList already has been fetched. Setting this property to false when FareStageList has been fetched
		/// will set FareStageList to null as well. Setting this property to true while FareStageList hasn't been fetched disables lazy loading for FareStageList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageList
		{
			get { return _alreadyFetchedFareStageList;}
			set 
			{
				if(_alreadyFetchedFareStageList && !value)
				{
					this.FareStageList = null;
				}
				_alreadyFetchedFareStageList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageList is not found
		/// in the database. When set to true, FareStageList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageListReturnsNewIfNotFound
		{
			get	{ return _fareStageListReturnsNewIfNotFound; }
			set { _fareStageListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareMatrices", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareMatrixEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
