﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ValidationRunRule'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ValidationRunRuleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ValidationResultCollection	_validationResults;
		private bool	_alwaysFetchValidationResults, _alreadyFetchedValidationResults;
		private ValidationRuleEntity _validationRule;
		private bool	_alwaysFetchValidationRule, _alreadyFetchedValidationRule, _validationRuleReturnsNewIfNotFound;
		private ValidationRunEntity _validationRun;
		private bool	_alwaysFetchValidationRun, _alreadyFetchedValidationRun, _validationRunReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ValidationRule</summary>
			public static readonly string ValidationRule = "ValidationRule";
			/// <summary>Member name ValidationRun</summary>
			public static readonly string ValidationRun = "ValidationRun";
			/// <summary>Member name ValidationResults</summary>
			public static readonly string ValidationResults = "ValidationResults";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ValidationRunRuleEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ValidationRunRuleEntity() :base("ValidationRunRuleEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		public ValidationRunRuleEntity(System.Int64 validationRunRuleID):base("ValidationRunRuleEntity")
		{
			InitClassFetch(validationRunRuleID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ValidationRunRuleEntity(System.Int64 validationRunRuleID, IPrefetchPath prefetchPathToUse):base("ValidationRunRuleEntity")
		{
			InitClassFetch(validationRunRuleID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="validator">The custom validator object for this ValidationRunRuleEntity</param>
		public ValidationRunRuleEntity(System.Int64 validationRunRuleID, IValidator validator):base("ValidationRunRuleEntity")
		{
			InitClassFetch(validationRunRuleID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ValidationRunRuleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_validationResults = (VarioSL.Entities.CollectionClasses.ValidationResultCollection)info.GetValue("_validationResults", typeof(VarioSL.Entities.CollectionClasses.ValidationResultCollection));
			_alwaysFetchValidationResults = info.GetBoolean("_alwaysFetchValidationResults");
			_alreadyFetchedValidationResults = info.GetBoolean("_alreadyFetchedValidationResults");
			_validationRule = (ValidationRuleEntity)info.GetValue("_validationRule", typeof(ValidationRuleEntity));
			if(_validationRule!=null)
			{
				_validationRule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_validationRuleReturnsNewIfNotFound = info.GetBoolean("_validationRuleReturnsNewIfNotFound");
			_alwaysFetchValidationRule = info.GetBoolean("_alwaysFetchValidationRule");
			_alreadyFetchedValidationRule = info.GetBoolean("_alreadyFetchedValidationRule");

			_validationRun = (ValidationRunEntity)info.GetValue("_validationRun", typeof(ValidationRunEntity));
			if(_validationRun!=null)
			{
				_validationRun.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_validationRunReturnsNewIfNotFound = info.GetBoolean("_validationRunReturnsNewIfNotFound");
			_alwaysFetchValidationRun = info.GetBoolean("_alwaysFetchValidationRun");
			_alreadyFetchedValidationRun = info.GetBoolean("_alreadyFetchedValidationRun");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ValidationRunRuleFieldIndex)fieldIndex)
			{
				case ValidationRunRuleFieldIndex.ValidationRuleID:
					DesetupSyncValidationRule(true, false);
					_alreadyFetchedValidationRule = false;
					break;
				case ValidationRunRuleFieldIndex.ValidationRunID:
					DesetupSyncValidationRun(true, false);
					_alreadyFetchedValidationRun = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedValidationResults = (_validationResults.Count > 0);
			_alreadyFetchedValidationRule = (_validationRule != null);
			_alreadyFetchedValidationRun = (_validationRun != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ValidationRule":
					toReturn.Add(Relations.ValidationRuleEntityUsingValidationRuleID);
					break;
				case "ValidationRun":
					toReturn.Add(Relations.ValidationRunEntityUsingValidationRunID);
					break;
				case "ValidationResults":
					toReturn.Add(Relations.ValidationResultEntityUsingValidationRunRuleID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_validationResults", (!this.MarkedForDeletion?_validationResults:null));
			info.AddValue("_alwaysFetchValidationResults", _alwaysFetchValidationResults);
			info.AddValue("_alreadyFetchedValidationResults", _alreadyFetchedValidationResults);
			info.AddValue("_validationRule", (!this.MarkedForDeletion?_validationRule:null));
			info.AddValue("_validationRuleReturnsNewIfNotFound", _validationRuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchValidationRule", _alwaysFetchValidationRule);
			info.AddValue("_alreadyFetchedValidationRule", _alreadyFetchedValidationRule);
			info.AddValue("_validationRun", (!this.MarkedForDeletion?_validationRun:null));
			info.AddValue("_validationRunReturnsNewIfNotFound", _validationRunReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchValidationRun", _alwaysFetchValidationRun);
			info.AddValue("_alreadyFetchedValidationRun", _alreadyFetchedValidationRun);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ValidationRule":
					_alreadyFetchedValidationRule = true;
					this.ValidationRule = (ValidationRuleEntity)entity;
					break;
				case "ValidationRun":
					_alreadyFetchedValidationRun = true;
					this.ValidationRun = (ValidationRunEntity)entity;
					break;
				case "ValidationResults":
					_alreadyFetchedValidationResults = true;
					if(entity!=null)
					{
						this.ValidationResults.Add((ValidationResultEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ValidationRule":
					SetupSyncValidationRule(relatedEntity);
					break;
				case "ValidationRun":
					SetupSyncValidationRun(relatedEntity);
					break;
				case "ValidationResults":
					_validationResults.Add((ValidationResultEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ValidationRule":
					DesetupSyncValidationRule(false, true);
					break;
				case "ValidationRun":
					DesetupSyncValidationRun(false, true);
					break;
				case "ValidationResults":
					this.PerformRelatedEntityRemoval(_validationResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_validationRule!=null)
			{
				toReturn.Add(_validationRule);
			}
			if(_validationRun!=null)
			{
				toReturn.Add(_validationRun);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_validationResults);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationRunRuleID)
		{
			return FetchUsingPK(validationRunRuleID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationRunRuleID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(validationRunRuleID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationRunRuleID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(validationRunRuleID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationRunRuleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(validationRunRuleID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ValidationRunRuleID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ValidationRunRuleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ValidationResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch)
		{
			return GetMultiValidationResults(forceFetch, _validationResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ValidationResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiValidationResults(forceFetch, _validationResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiValidationResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ValidationResultCollection GetMultiValidationResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedValidationResults || forceFetch || _alwaysFetchValidationResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_validationResults);
				_validationResults.SuppressClearInGetMulti=!forceFetch;
				_validationResults.EntityFactoryToUse = entityFactoryToUse;
				_validationResults.GetMultiManyToOne(null, null, this, filter);
				_validationResults.SuppressClearInGetMulti=false;
				_alreadyFetchedValidationResults = true;
			}
			return _validationResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ValidationResults'. These settings will be taken into account
		/// when the property ValidationResults is requested or GetMultiValidationResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersValidationResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_validationResults.SortClauses=sortClauses;
			_validationResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ValidationRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ValidationRuleEntity' which is related to this entity.</returns>
		public ValidationRuleEntity GetSingleValidationRule()
		{
			return GetSingleValidationRule(false);
		}

		/// <summary> Retrieves the related entity of type 'ValidationRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ValidationRuleEntity' which is related to this entity.</returns>
		public virtual ValidationRuleEntity GetSingleValidationRule(bool forceFetch)
		{
			if( ( !_alreadyFetchedValidationRule || forceFetch || _alwaysFetchValidationRule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ValidationRuleEntityUsingValidationRuleID);
				ValidationRuleEntity newEntity = new ValidationRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidationRuleID);
				}
				if(fetchResult)
				{
					newEntity = (ValidationRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_validationRuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ValidationRule = newEntity;
				_alreadyFetchedValidationRule = fetchResult;
			}
			return _validationRule;
		}


		/// <summary> Retrieves the related entity of type 'ValidationRunEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ValidationRunEntity' which is related to this entity.</returns>
		public ValidationRunEntity GetSingleValidationRun()
		{
			return GetSingleValidationRun(false);
		}

		/// <summary> Retrieves the related entity of type 'ValidationRunEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ValidationRunEntity' which is related to this entity.</returns>
		public virtual ValidationRunEntity GetSingleValidationRun(bool forceFetch)
		{
			if( ( !_alreadyFetchedValidationRun || forceFetch || _alwaysFetchValidationRun) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ValidationRunEntityUsingValidationRunID);
				ValidationRunEntity newEntity = new ValidationRunEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ValidationRunID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ValidationRunEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_validationRunReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ValidationRun = newEntity;
				_alreadyFetchedValidationRun = fetchResult;
			}
			return _validationRun;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ValidationRule", _validationRule);
			toReturn.Add("ValidationRun", _validationRun);
			toReturn.Add("ValidationResults", _validationResults);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="validator">The validator object for this ValidationRunRuleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 validationRunRuleID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(validationRunRuleID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_validationResults = new VarioSL.Entities.CollectionClasses.ValidationResultCollection();
			_validationResults.SetContainingEntityInfo(this, "ValidationRunRule");
			_validationRuleReturnsNewIfNotFound = false;
			_validationRunReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRuleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRunID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRunRuleID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _validationRule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncValidationRule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _validationRule, new PropertyChangedEventHandler( OnValidationRulePropertyChanged ), "ValidationRule", VarioSL.Entities.RelationClasses.StaticValidationRunRuleRelations.ValidationRuleEntityUsingValidationRuleIDStatic, true, signalRelatedEntity, "ValidationRunRules", resetFKFields, new int[] { (int)ValidationRunRuleFieldIndex.ValidationRuleID } );		
			_validationRule = null;
		}
		
		/// <summary> setups the sync logic for member _validationRule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncValidationRule(IEntityCore relatedEntity)
		{
			if(_validationRule!=relatedEntity)
			{		
				DesetupSyncValidationRule(true, true);
				_validationRule = (ValidationRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _validationRule, new PropertyChangedEventHandler( OnValidationRulePropertyChanged ), "ValidationRule", VarioSL.Entities.RelationClasses.StaticValidationRunRuleRelations.ValidationRuleEntityUsingValidationRuleIDStatic, true, ref _alreadyFetchedValidationRule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnValidationRulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _validationRun</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncValidationRun(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _validationRun, new PropertyChangedEventHandler( OnValidationRunPropertyChanged ), "ValidationRun", VarioSL.Entities.RelationClasses.StaticValidationRunRuleRelations.ValidationRunEntityUsingValidationRunIDStatic, true, signalRelatedEntity, "ValidationRunRules", resetFKFields, new int[] { (int)ValidationRunRuleFieldIndex.ValidationRunID } );		
			_validationRun = null;
		}
		
		/// <summary> setups the sync logic for member _validationRun</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncValidationRun(IEntityCore relatedEntity)
		{
			if(_validationRun!=relatedEntity)
			{		
				DesetupSyncValidationRun(true, true);
				_validationRun = (ValidationRunEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _validationRun, new PropertyChangedEventHandler( OnValidationRunPropertyChanged ), "ValidationRun", VarioSL.Entities.RelationClasses.StaticValidationRunRuleRelations.ValidationRunEntityUsingValidationRunIDStatic, true, ref _alreadyFetchedValidationRun, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnValidationRunPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="validationRunRuleID">PK value for ValidationRunRule which data should be fetched into this ValidationRunRule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 validationRunRuleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ValidationRunRuleFieldIndex.ValidationRunRuleID].ForcedCurrentValueWrite(validationRunRuleID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateValidationRunRuleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ValidationRunRuleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ValidationRunRuleRelations Relations
		{
			get	{ return new ValidationRunRuleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationResultCollection(), (IEntityRelation)GetRelationsForField("ValidationResults")[0], (int)VarioSL.Entities.EntityType.ValidationRunRuleEntity, (int)VarioSL.Entities.EntityType.ValidationResultEntity, 0, null, null, null, "ValidationResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationRule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationRuleCollection(), (IEntityRelation)GetRelationsForField("ValidationRule")[0], (int)VarioSL.Entities.EntityType.ValidationRunRuleEntity, (int)VarioSL.Entities.EntityType.ValidationRuleEntity, 0, null, null, null, "ValidationRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ValidationRun'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathValidationRun
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ValidationRunCollection(), (IEntityRelation)GetRelationsForField("ValidationRun")[0], (int)VarioSL.Entities.EntityType.ValidationRunRuleEntity, (int)VarioSL.Entities.EntityType.ValidationRunEntity, 0, null, null, null, "ValidationRun", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EndDateTime property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."ENDDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> EndDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationRunRuleFieldIndex.EndDateTime, false); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.EndDateTime, value, true); }
		}

		/// <summary> The StartDateTime property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."STARTDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StartDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationRunRuleFieldIndex.StartDateTime, false); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.StartDateTime, value, true); }
		}

		/// <summary> The Status property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ValidationState Status
		{
			get { return (VarioSL.Entities.Enumerations.ValidationState)GetValue((int)ValidationRunRuleFieldIndex.Status, true); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.Status, value, true); }
		}

		/// <summary> The ValidationRuleID property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."VALIDATIONRULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationRuleID
		{
			get { return (System.Int64)GetValue((int)ValidationRunRuleFieldIndex.ValidationRuleID, true); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.ValidationRuleID, value, true); }
		}

		/// <summary> The ValidationRunID property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."VALIDATIONRUNID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValidationRunID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ValidationRunRuleFieldIndex.ValidationRunID, false); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.ValidationRunID, value, true); }
		}

		/// <summary> The ValidationRunRuleID property of the Entity ValidationRunRule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VALIDATIONRUNRULE"."VALIDATIONRUNRULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ValidationRunRuleID
		{
			get { return (System.Int64)GetValue((int)ValidationRunRuleFieldIndex.ValidationRunRuleID, true); }
			set	{ SetValue((int)ValidationRunRuleFieldIndex.ValidationRunRuleID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ValidationResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiValidationResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ValidationResultCollection ValidationResults
		{
			get	{ return GetMultiValidationResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationResults. When set to true, ValidationResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationResults is accessed. You can always execute/ a forced fetch by calling GetMultiValidationResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationResults
		{
			get	{ return _alwaysFetchValidationResults; }
			set	{ _alwaysFetchValidationResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationResults already has been fetched. Setting this property to false when ValidationResults has been fetched
		/// will clear the ValidationResults collection well. Setting this property to true while ValidationResults hasn't been fetched disables lazy loading for ValidationResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationResults
		{
			get { return _alreadyFetchedValidationResults;}
			set 
			{
				if(_alreadyFetchedValidationResults && !value && (_validationResults != null))
				{
					_validationResults.Clear();
				}
				_alreadyFetchedValidationResults = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ValidationRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleValidationRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ValidationRuleEntity ValidationRule
		{
			get	{ return GetSingleValidationRule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncValidationRule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ValidationRunRules", "ValidationRule", _validationRule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationRule. When set to true, ValidationRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationRule is accessed. You can always execute a forced fetch by calling GetSingleValidationRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationRule
		{
			get	{ return _alwaysFetchValidationRule; }
			set	{ _alwaysFetchValidationRule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationRule already has been fetched. Setting this property to false when ValidationRule has been fetched
		/// will set ValidationRule to null as well. Setting this property to true while ValidationRule hasn't been fetched disables lazy loading for ValidationRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationRule
		{
			get { return _alreadyFetchedValidationRule;}
			set 
			{
				if(_alreadyFetchedValidationRule && !value)
				{
					this.ValidationRule = null;
				}
				_alreadyFetchedValidationRule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ValidationRule is not found
		/// in the database. When set to true, ValidationRule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ValidationRuleReturnsNewIfNotFound
		{
			get	{ return _validationRuleReturnsNewIfNotFound; }
			set { _validationRuleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ValidationRunEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleValidationRun()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ValidationRunEntity ValidationRun
		{
			get	{ return GetSingleValidationRun(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncValidationRun(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ValidationRunRules", "ValidationRun", _validationRun, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ValidationRun. When set to true, ValidationRun is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ValidationRun is accessed. You can always execute a forced fetch by calling GetSingleValidationRun(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchValidationRun
		{
			get	{ return _alwaysFetchValidationRun; }
			set	{ _alwaysFetchValidationRun = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ValidationRun already has been fetched. Setting this property to false when ValidationRun has been fetched
		/// will set ValidationRun to null as well. Setting this property to true while ValidationRun hasn't been fetched disables lazy loading for ValidationRun</summary>
		[Browsable(false)]
		public bool AlreadyFetchedValidationRun
		{
			get { return _alreadyFetchedValidationRun;}
			set 
			{
				if(_alreadyFetchedValidationRun && !value)
				{
					this.ValidationRun = null;
				}
				_alreadyFetchedValidationRun = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ValidationRun is not found
		/// in the database. When set to true, ValidationRun will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ValidationRunReturnsNewIfNotFound
		{
			get	{ return _validationRunReturnsNewIfNotFound; }
			set { _validationRunReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ValidationRunRuleEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
