﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EmailEventResendLock'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class EmailEventResendLockEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AutoloadSettingEntity _autoloadSetting;
		private bool	_alwaysFetchAutoloadSetting, _alreadyFetchedAutoloadSetting, _autoloadSettingReturnsNewIfNotFound;
		private BookingItemEntity _bookingItem;
		private bool	_alwaysFetchBookingItem, _alreadyFetchedBookingItem, _bookingItemReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AutoloadSetting</summary>
			public static readonly string AutoloadSetting = "AutoloadSetting";
			/// <summary>Member name BookingItem</summary>
			public static readonly string BookingItem = "BookingItem";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EmailEventResendLockEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public EmailEventResendLockEntity() :base("EmailEventResendLockEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		public EmailEventResendLockEntity(System.Int64 emailEventResendLockID):base("EmailEventResendLockEntity")
		{
			InitClassFetch(emailEventResendLockID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EmailEventResendLockEntity(System.Int64 emailEventResendLockID, IPrefetchPath prefetchPathToUse):base("EmailEventResendLockEntity")
		{
			InitClassFetch(emailEventResendLockID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="validator">The custom validator object for this EmailEventResendLockEntity</param>
		public EmailEventResendLockEntity(System.Int64 emailEventResendLockID, IValidator validator):base("EmailEventResendLockEntity")
		{
			InitClassFetch(emailEventResendLockID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EmailEventResendLockEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_autoloadSetting = (AutoloadSettingEntity)info.GetValue("_autoloadSetting", typeof(AutoloadSettingEntity));
			if(_autoloadSetting!=null)
			{
				_autoloadSetting.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_autoloadSettingReturnsNewIfNotFound = info.GetBoolean("_autoloadSettingReturnsNewIfNotFound");
			_alwaysFetchAutoloadSetting = info.GetBoolean("_alwaysFetchAutoloadSetting");
			_alreadyFetchedAutoloadSetting = info.GetBoolean("_alreadyFetchedAutoloadSetting");

			_bookingItem = (BookingItemEntity)info.GetValue("_bookingItem", typeof(BookingItemEntity));
			if(_bookingItem!=null)
			{
				_bookingItem.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bookingItemReturnsNewIfNotFound = info.GetBoolean("_bookingItemReturnsNewIfNotFound");
			_alwaysFetchBookingItem = info.GetBoolean("_alwaysFetchBookingItem");
			_alreadyFetchedBookingItem = info.GetBoolean("_alreadyFetchedBookingItem");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EmailEventResendLockFieldIndex)fieldIndex)
			{
				case EmailEventResendLockFieldIndex.AutoloadSettingID:
					DesetupSyncAutoloadSetting(true, false);
					_alreadyFetchedAutoloadSetting = false;
					break;
				case EmailEventResendLockFieldIndex.BookingItemID:
					DesetupSyncBookingItem(true, false);
					_alreadyFetchedBookingItem = false;
					break;
				case EmailEventResendLockFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case EmailEventResendLockFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				case EmailEventResendLockFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutoloadSetting = (_autoloadSetting != null);
			_alreadyFetchedBookingItem = (_bookingItem != null);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedPerson = (_person != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AutoloadSetting":
					toReturn.Add(Relations.AutoloadSettingEntityUsingAutoloadSettingID);
					break;
				case "BookingItem":
					toReturn.Add(Relations.BookingItemEntityUsingBookingItemID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_autoloadSetting", (!this.MarkedForDeletion?_autoloadSetting:null));
			info.AddValue("_autoloadSettingReturnsNewIfNotFound", _autoloadSettingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutoloadSetting", _alwaysFetchAutoloadSetting);
			info.AddValue("_alreadyFetchedAutoloadSetting", _alreadyFetchedAutoloadSetting);
			info.AddValue("_bookingItem", (!this.MarkedForDeletion?_bookingItem:null));
			info.AddValue("_bookingItemReturnsNewIfNotFound", _bookingItemReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBookingItem", _alwaysFetchBookingItem);
			info.AddValue("_alreadyFetchedBookingItem", _alreadyFetchedBookingItem);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AutoloadSetting":
					_alreadyFetchedAutoloadSetting = true;
					this.AutoloadSetting = (AutoloadSettingEntity)entity;
					break;
				case "BookingItem":
					_alreadyFetchedBookingItem = true;
					this.BookingItem = (BookingItemEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AutoloadSetting":
					SetupSyncAutoloadSetting(relatedEntity);
					break;
				case "BookingItem":
					SetupSyncBookingItem(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AutoloadSetting":
					DesetupSyncAutoloadSetting(false, true);
					break;
				case "BookingItem":
					DesetupSyncBookingItem(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_autoloadSetting!=null)
			{
				toReturn.Add(_autoloadSetting);
			}
			if(_bookingItem!=null)
			{
				toReturn.Add(_bookingItem);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventResendLockID)
		{
			return FetchUsingPK(emailEventResendLockID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventResendLockID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(emailEventResendLockID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventResendLockID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(emailEventResendLockID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventResendLockID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(emailEventResendLockID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EmailEventResendLockID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EmailEventResendLockRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AutoloadSettingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutoloadSettingEntity' which is related to this entity.</returns>
		public AutoloadSettingEntity GetSingleAutoloadSetting()
		{
			return GetSingleAutoloadSetting(false);
		}

		/// <summary> Retrieves the related entity of type 'AutoloadSettingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutoloadSettingEntity' which is related to this entity.</returns>
		public virtual AutoloadSettingEntity GetSingleAutoloadSetting(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutoloadSetting || forceFetch || _alwaysFetchAutoloadSetting) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutoloadSettingEntityUsingAutoloadSettingID);
				AutoloadSettingEntity newEntity = new AutoloadSettingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutoloadSettingID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AutoloadSettingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_autoloadSettingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AutoloadSetting = newEntity;
				_alreadyFetchedAutoloadSetting = fetchResult;
			}
			return _autoloadSetting;
		}


		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public BookingItemEntity GetSingleBookingItem()
		{
			return GetSingleBookingItem(false);
		}

		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public virtual BookingItemEntity GetSingleBookingItem(bool forceFetch)
		{
			if( ( !_alreadyFetchedBookingItem || forceFetch || _alwaysFetchBookingItem) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BookingItemEntityUsingBookingItemID);
				BookingItemEntity newEntity = new BookingItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BookingItemID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BookingItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bookingItemReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BookingItem = newEntity;
				_alreadyFetchedBookingItem = fetchResult;
			}
			return _bookingItem;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AutoloadSetting", _autoloadSetting);
			toReturn.Add("BookingItem", _bookingItem);
			toReturn.Add("Card", _card);
			toReturn.Add("Person", _person);
			toReturn.Add("Product", _product);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="validator">The validator object for this EmailEventResendLockEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 emailEventResendLockID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(emailEventResendLockID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_autoloadSettingReturnsNewIfNotFound = false;
			_bookingItemReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoloadSettingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingItemID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventResendLockID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _autoloadSetting</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutoloadSetting(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _autoloadSetting, new PropertyChangedEventHandler( OnAutoloadSettingPropertyChanged ), "AutoloadSetting", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.AutoloadSettingEntityUsingAutoloadSettingIDStatic, true, signalRelatedEntity, "EmailEventResendLocks", resetFKFields, new int[] { (int)EmailEventResendLockFieldIndex.AutoloadSettingID } );		
			_autoloadSetting = null;
		}
		
		/// <summary> setups the sync logic for member _autoloadSetting</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutoloadSetting(IEntityCore relatedEntity)
		{
			if(_autoloadSetting!=relatedEntity)
			{		
				DesetupSyncAutoloadSetting(true, true);
				_autoloadSetting = (AutoloadSettingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _autoloadSetting, new PropertyChangedEventHandler( OnAutoloadSettingPropertyChanged ), "AutoloadSetting", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.AutoloadSettingEntityUsingAutoloadSettingIDStatic, true, ref _alreadyFetchedAutoloadSetting, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutoloadSettingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bookingItem</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBookingItem(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.BookingItemEntityUsingBookingItemIDStatic, true, signalRelatedEntity, "EmailEventResendLocks", resetFKFields, new int[] { (int)EmailEventResendLockFieldIndex.BookingItemID } );		
			_bookingItem = null;
		}
		
		/// <summary> setups the sync logic for member _bookingItem</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBookingItem(IEntityCore relatedEntity)
		{
			if(_bookingItem!=relatedEntity)
			{		
				DesetupSyncBookingItem(true, true);
				_bookingItem = (BookingItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.BookingItemEntityUsingBookingItemIDStatic, true, ref _alreadyFetchedBookingItem, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBookingItemPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "EmailEventResendLocks", resetFKFields, new int[] { (int)EmailEventResendLockFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "EmailEventResendLocks", resetFKFields, new int[] { (int)EmailEventResendLockFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "EmailEventResendLocks", resetFKFields, new int[] { (int)EmailEventResendLockFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticEmailEventResendLockRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="emailEventResendLockID">PK value for EmailEventResendLock which data should be fetched into this EmailEventResendLock object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 emailEventResendLockID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EmailEventResendLockFieldIndex.EmailEventResendLockID].ForcedCurrentValueWrite(emailEventResendLockID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEmailEventResendLockDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EmailEventResendLockEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EmailEventResendLockRelations Relations
		{
			get	{ return new EmailEventResendLockRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadSetting'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadSetting
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection(), (IEntityRelation)GetRelationsForField("AutoloadSetting")[0], (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, 0, null, null, null, "AutoloadSetting", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItem
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemCollection(), (IEntityRelation)GetRelationsForField("BookingItem")[0], (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, (int)VarioSL.Entities.EntityType.BookingItemEntity, 0, null, null, null, "BookingItem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutoloadSettingID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."AUTOLOADSETTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AutoloadSettingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EmailEventResendLockFieldIndex.AutoloadSettingID, false); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.AutoloadSettingID, value, true); }
		}

		/// <summary> The BookingItemID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."BOOKINGITEMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BookingItemID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EmailEventResendLockFieldIndex.BookingItemID, false); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.BookingItemID, value, true); }
		}

		/// <summary> The CardID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EmailEventResendLockFieldIndex.CardID, false); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.CardID, value, true); }
		}

		/// <summary> The EmailEventResendLockID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."EMAILEVENTRESENDLOCKID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 EmailEventResendLockID
		{
			get { return (System.Int64)GetValue((int)EmailEventResendLockFieldIndex.EmailEventResendLockID, true); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.EmailEventResendLockID, value, true); }
		}

		/// <summary> The EventType property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."EVENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.EventType EventType
		{
			get { return (VarioSL.Entities.Enumerations.EventType)GetValue((int)EmailEventResendLockFieldIndex.EventType, true); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.EventType, value, true); }
		}

		/// <summary> The MessageType property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."MESSAGETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.MessageType MessageType
		{
			get { return (VarioSL.Entities.Enumerations.MessageType)GetValue((int)EmailEventResendLockFieldIndex.MessageType, true); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.MessageType, value, true); }
		}

		/// <summary> The NotificationDate property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."NOTIFICATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime NotificationDate
		{
			get { return (System.DateTime)GetValue((int)EmailEventResendLockFieldIndex.NotificationDate, true); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.NotificationDate, value, true); }
		}

		/// <summary> The PersonID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EmailEventResendLockFieldIndex.PersonID, false); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.PersonID, value, true); }
		}

		/// <summary> The ProductID property of the Entity EmailEventResendLock<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILEVENTRESENDLOCK"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EmailEventResendLockFieldIndex.ProductID, false); }
			set	{ SetValue((int)EmailEventResendLockFieldIndex.ProductID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AutoloadSettingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutoloadSetting()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutoloadSettingEntity AutoloadSetting
		{
			get	{ return GetSingleAutoloadSetting(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutoloadSetting(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailEventResendLocks", "AutoloadSetting", _autoloadSetting, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadSetting. When set to true, AutoloadSetting is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadSetting is accessed. You can always execute a forced fetch by calling GetSingleAutoloadSetting(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadSetting
		{
			get	{ return _alwaysFetchAutoloadSetting; }
			set	{ _alwaysFetchAutoloadSetting = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadSetting already has been fetched. Setting this property to false when AutoloadSetting has been fetched
		/// will set AutoloadSetting to null as well. Setting this property to true while AutoloadSetting hasn't been fetched disables lazy loading for AutoloadSetting</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadSetting
		{
			get { return _alreadyFetchedAutoloadSetting;}
			set 
			{
				if(_alreadyFetchedAutoloadSetting && !value)
				{
					this.AutoloadSetting = null;
				}
				_alreadyFetchedAutoloadSetting = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AutoloadSetting is not found
		/// in the database. When set to true, AutoloadSetting will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutoloadSettingReturnsNewIfNotFound
		{
			get	{ return _autoloadSettingReturnsNewIfNotFound; }
			set { _autoloadSettingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BookingItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBookingItem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BookingItemEntity BookingItem
		{
			get	{ return GetSingleBookingItem(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBookingItem(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailEventResendLocks", "BookingItem", _bookingItem, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItem. When set to true, BookingItem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItem is accessed. You can always execute a forced fetch by calling GetSingleBookingItem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItem
		{
			get	{ return _alwaysFetchBookingItem; }
			set	{ _alwaysFetchBookingItem = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItem already has been fetched. Setting this property to false when BookingItem has been fetched
		/// will set BookingItem to null as well. Setting this property to true while BookingItem hasn't been fetched disables lazy loading for BookingItem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItem
		{
			get { return _alreadyFetchedBookingItem;}
			set 
			{
				if(_alreadyFetchedBookingItem && !value)
				{
					this.BookingItem = null;
				}
				_alreadyFetchedBookingItem = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BookingItem is not found
		/// in the database. When set to true, BookingItem will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BookingItemReturnsNewIfNotFound
		{
			get	{ return _bookingItemReturnsNewIfNotFound; }
			set { _bookingItemReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailEventResendLocks", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailEventResendLocks", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailEventResendLocks", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
