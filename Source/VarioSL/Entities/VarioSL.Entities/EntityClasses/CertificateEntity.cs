﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Certificate'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CertificateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection	_deviceReaderKeyToCerts;
		private bool	_alwaysFetchDeviceReaderKeyToCerts, _alreadyFetchedDeviceReaderKeyToCerts;
		private VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection	_deviceReaderToCerts;
		private bool	_alwaysFetchDeviceReaderToCerts, _alreadyFetchedDeviceReaderToCerts;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceReaderKeyToCerts</summary>
			public static readonly string DeviceReaderKeyToCerts = "DeviceReaderKeyToCerts";
			/// <summary>Member name DeviceReaderToCerts</summary>
			public static readonly string DeviceReaderToCerts = "DeviceReaderToCerts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CertificateEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CertificateEntity() :base("CertificateEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		public CertificateEntity(System.Int64 certificateID):base("CertificateEntity")
		{
			InitClassFetch(certificateID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CertificateEntity(System.Int64 certificateID, IPrefetchPath prefetchPathToUse):base("CertificateEntity")
		{
			InitClassFetch(certificateID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="validator">The custom validator object for this CertificateEntity</param>
		public CertificateEntity(System.Int64 certificateID, IValidator validator):base("CertificateEntity")
		{
			InitClassFetch(certificateID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CertificateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceReaderKeyToCerts = (VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection)info.GetValue("_deviceReaderKeyToCerts", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection));
			_alwaysFetchDeviceReaderKeyToCerts = info.GetBoolean("_alwaysFetchDeviceReaderKeyToCerts");
			_alreadyFetchedDeviceReaderKeyToCerts = info.GetBoolean("_alreadyFetchedDeviceReaderKeyToCerts");

			_deviceReaderToCerts = (VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection)info.GetValue("_deviceReaderToCerts", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection));
			_alwaysFetchDeviceReaderToCerts = info.GetBoolean("_alwaysFetchDeviceReaderToCerts");
			_alreadyFetchedDeviceReaderToCerts = info.GetBoolean("_alreadyFetchedDeviceReaderToCerts");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceReaderKeyToCerts = (_deviceReaderKeyToCerts.Count > 0);
			_alreadyFetchedDeviceReaderToCerts = (_deviceReaderToCerts.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceReaderKeyToCerts":
					toReturn.Add(Relations.DeviceReaderKeyToCertEntityUsingCertificateID);
					break;
				case "DeviceReaderToCerts":
					toReturn.Add(Relations.DeviceReaderToCertEntityUsingCertificateID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceReaderKeyToCerts", (!this.MarkedForDeletion?_deviceReaderKeyToCerts:null));
			info.AddValue("_alwaysFetchDeviceReaderKeyToCerts", _alwaysFetchDeviceReaderKeyToCerts);
			info.AddValue("_alreadyFetchedDeviceReaderKeyToCerts", _alreadyFetchedDeviceReaderKeyToCerts);
			info.AddValue("_deviceReaderToCerts", (!this.MarkedForDeletion?_deviceReaderToCerts:null));
			info.AddValue("_alwaysFetchDeviceReaderToCerts", _alwaysFetchDeviceReaderToCerts);
			info.AddValue("_alreadyFetchedDeviceReaderToCerts", _alreadyFetchedDeviceReaderToCerts);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceReaderKeyToCerts":
					_alreadyFetchedDeviceReaderKeyToCerts = true;
					if(entity!=null)
					{
						this.DeviceReaderKeyToCerts.Add((DeviceReaderKeyToCertEntity)entity);
					}
					break;
				case "DeviceReaderToCerts":
					_alreadyFetchedDeviceReaderToCerts = true;
					if(entity!=null)
					{
						this.DeviceReaderToCerts.Add((DeviceReaderToCertEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceReaderKeyToCerts":
					_deviceReaderKeyToCerts.Add((DeviceReaderKeyToCertEntity)relatedEntity);
					break;
				case "DeviceReaderToCerts":
					_deviceReaderToCerts.Add((DeviceReaderToCertEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceReaderKeyToCerts":
					this.PerformRelatedEntityRemoval(_deviceReaderKeyToCerts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceReaderToCerts":
					this.PerformRelatedEntityRemoval(_deviceReaderToCerts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deviceReaderKeyToCerts);
			toReturn.Add(_deviceReaderToCerts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID)
		{
			return FetchUsingPK(certificateID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(certificateID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(certificateID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(certificateID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CertificateID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CertificateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, _deviceReaderKeyToCerts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, _deviceReaderKeyToCerts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderKeyToCerts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection GetMultiDeviceReaderKeyToCerts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderKeyToCerts || forceFetch || _alwaysFetchDeviceReaderKeyToCerts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderKeyToCerts);
				_deviceReaderKeyToCerts.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderKeyToCerts.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderKeyToCerts.GetMultiManyToOne(this, null, filter);
				_deviceReaderKeyToCerts.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderKeyToCerts = true;
			}
			return _deviceReaderKeyToCerts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderKeyToCerts'. These settings will be taken into account
		/// when the property DeviceReaderKeyToCerts is requested or GetMultiDeviceReaderKeyToCerts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderKeyToCerts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderKeyToCerts.SortClauses=sortClauses;
			_deviceReaderKeyToCerts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, _deviceReaderToCerts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, _deviceReaderToCerts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderToCerts || forceFetch || _alwaysFetchDeviceReaderToCerts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderToCerts);
				_deviceReaderToCerts.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderToCerts.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderToCerts.GetMultiManyToOne(this, null, filter);
				_deviceReaderToCerts.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderToCerts = true;
			}
			return _deviceReaderToCerts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderToCerts'. These settings will be taken into account
		/// when the property DeviceReaderToCerts is requested or GetMultiDeviceReaderToCerts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderToCerts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderToCerts.SortClauses=sortClauses;
			_deviceReaderToCerts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceReaderKeyToCerts", _deviceReaderKeyToCerts);
			toReturn.Add("DeviceReaderToCerts", _deviceReaderToCerts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="validator">The validator object for this CertificateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 certificateID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(certificateID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deviceReaderKeyToCerts = new VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection();
			_deviceReaderKeyToCerts.SetContainingEntityInfo(this, "Certificate");

			_deviceReaderToCerts = new VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection();
			_deviceReaderToCerts.SetContainingEntityInfo(this, "Certificate");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApiType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Certificate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateSignature", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptoArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptoClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncludeInQArchive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssuerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Label", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubjectName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="certificateID">PK value for Certificate which data should be fetched into this Certificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CertificateFieldIndex.CertificateID].ForcedCurrentValueWrite(certificateID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCertificateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CertificateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CertificateRelations Relations
		{
			get	{ return new CertificateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderKeyToCert' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderKeyToCerts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderKeyToCerts")[0], (int)VarioSL.Entities.EntityType.CertificateEntity, (int)VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity, 0, null, null, null, "DeviceReaderKeyToCerts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderToCert' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderToCerts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderToCerts")[0], (int)VarioSL.Entities.EntityType.CertificateEntity, (int)VarioSL.Entities.EntityType.DeviceReaderToCertEntity, 0, null, null, null, "DeviceReaderToCerts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ApiType property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."APITYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ApiType
		{
			get { return (System.Int16)GetValue((int)CertificateFieldIndex.ApiType, true); }
			set	{ SetValue((int)CertificateFieldIndex.ApiType, value, true); }
		}

		/// <summary> The Certificate property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CERTIFICATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Certificate
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.Certificate, true); }
			set	{ SetValue((int)CertificateFieldIndex.Certificate, value, true); }
		}

		/// <summary> The CertificateID property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CertificateID
		{
			get { return (System.Int64)GetValue((int)CertificateFieldIndex.CertificateID, true); }
			set	{ SetValue((int)CertificateFieldIndex.CertificateID, value, true); }
		}

		/// <summary> The CertificateSignature property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CERTIFICATESIGNATURE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CertificateSignature
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.CertificateSignature, true); }
			set	{ SetValue((int)CertificateFieldIndex.CertificateSignature, value, true); }
		}

		/// <summary> The CertificateStatus property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CERTIFICATESTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CertificateStatus
		{
			get { return (System.Int32)GetValue((int)CertificateFieldIndex.CertificateStatus, true); }
			set	{ SetValue((int)CertificateFieldIndex.CertificateStatus, value, true); }
		}

		/// <summary> The CertificateType property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CERTIFICATETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CertificateType
		{
			get { return (System.Int32)GetValue((int)CertificateFieldIndex.CertificateType, true); }
			set	{ SetValue((int)CertificateFieldIndex.CertificateType, value, true); }
		}

		/// <summary> The CryptoArchiveID property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CRYPTOARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CryptoArchiveID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CertificateFieldIndex.CryptoArchiveID, false); }
			set	{ SetValue((int)CertificateFieldIndex.CryptoArchiveID, value, true); }
		}

		/// <summary> The CryptoClientID property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."CRYPTOCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CryptoClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CertificateFieldIndex.CryptoClientID, false); }
			set	{ SetValue((int)CertificateFieldIndex.CryptoClientID, value, true); }
		}

		/// <summary> The Description property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.Description, true); }
			set	{ SetValue((int)CertificateFieldIndex.Description, value, true); }
		}

		/// <summary> The IncludeInQArchive property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."INCLUDEINQARCHIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IncludeInQArchive
		{
			get { return (System.Boolean)GetValue((int)CertificateFieldIndex.IncludeInQArchive, true); }
			set	{ SetValue((int)CertificateFieldIndex.IncludeInQArchive, value, true); }
		}

		/// <summary> The IssuerName property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."ISSUERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String IssuerName
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.IssuerName, true); }
			set	{ SetValue((int)CertificateFieldIndex.IssuerName, value, true); }
		}

		/// <summary> The KeyType property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."KEYTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 KeyType
		{
			get { return (System.Int16)GetValue((int)CertificateFieldIndex.KeyType, true); }
			set	{ SetValue((int)CertificateFieldIndex.KeyType, value, true); }
		}

		/// <summary> The Label property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."LABEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Label
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.Label, true); }
			set	{ SetValue((int)CertificateFieldIndex.Label, value, true); }
		}

		/// <summary> The LastModified property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CertificateFieldIndex.LastModified, true); }
			set	{ SetValue((int)CertificateFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.LastUser, true); }
			set	{ SetValue((int)CertificateFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.Name, true); }
			set	{ SetValue((int)CertificateFieldIndex.Name, value, true); }
		}

		/// <summary> The SubjectName property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."SUBJECTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SubjectName
		{
			get { return (System.String)GetValue((int)CertificateFieldIndex.SubjectName, true); }
			set	{ SetValue((int)CertificateFieldIndex.SubjectName, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CertificateFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CertificateFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)CertificateFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)CertificateFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)CertificateFieldIndex.ValidTo, true); }
			set	{ SetValue((int)CertificateFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The Version property of the Entity Certificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CERTIFICATE"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)CertificateFieldIndex.Version, true); }
			set	{ SetValue((int)CertificateFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyToCertEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderKeyToCerts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyToCertCollection DeviceReaderKeyToCerts
		{
			get	{ return GetMultiDeviceReaderKeyToCerts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderKeyToCerts. When set to true, DeviceReaderKeyToCerts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderKeyToCerts is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderKeyToCerts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderKeyToCerts
		{
			get	{ return _alwaysFetchDeviceReaderKeyToCerts; }
			set	{ _alwaysFetchDeviceReaderKeyToCerts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderKeyToCerts already has been fetched. Setting this property to false when DeviceReaderKeyToCerts has been fetched
		/// will clear the DeviceReaderKeyToCerts collection well. Setting this property to true while DeviceReaderKeyToCerts hasn't been fetched disables lazy loading for DeviceReaderKeyToCerts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderKeyToCerts
		{
			get { return _alreadyFetchedDeviceReaderKeyToCerts;}
			set 
			{
				if(_alreadyFetchedDeviceReaderKeyToCerts && !value && (_deviceReaderKeyToCerts != null))
				{
					_deviceReaderKeyToCerts.Clear();
				}
				_alreadyFetchedDeviceReaderKeyToCerts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderToCerts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection DeviceReaderToCerts
		{
			get	{ return GetMultiDeviceReaderToCerts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderToCerts. When set to true, DeviceReaderToCerts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderToCerts is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderToCerts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderToCerts
		{
			get	{ return _alwaysFetchDeviceReaderToCerts; }
			set	{ _alwaysFetchDeviceReaderToCerts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderToCerts already has been fetched. Setting this property to false when DeviceReaderToCerts has been fetched
		/// will clear the DeviceReaderToCerts collection well. Setting this property to true while DeviceReaderToCerts hasn't been fetched disables lazy loading for DeviceReaderToCerts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderToCerts
		{
			get { return _alreadyFetchedDeviceReaderToCerts;}
			set 
			{
				if(_alreadyFetchedDeviceReaderToCerts && !value && (_deviceReaderToCerts != null))
				{
					_deviceReaderToCerts.Clear();
				}
				_alreadyFetchedDeviceReaderToCerts = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CertificateEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
