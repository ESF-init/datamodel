﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ContractTermination'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ContractTerminationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private ContractTerminationTypeEntity _contractTerminationType;
		private bool	_alwaysFetchContractTerminationType, _alreadyFetchedContractTerminationType, _contractTerminationTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name ContractTerminationType</summary>
			public static readonly string ContractTerminationType = "ContractTerminationType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ContractTerminationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ContractTerminationEntity() :base("ContractTerminationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		public ContractTerminationEntity(System.Int64 contractTerminationID):base("ContractTerminationEntity")
		{
			InitClassFetch(contractTerminationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ContractTerminationEntity(System.Int64 contractTerminationID, IPrefetchPath prefetchPathToUse):base("ContractTerminationEntity")
		{
			InitClassFetch(contractTerminationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="validator">The custom validator object for this ContractTerminationEntity</param>
		public ContractTerminationEntity(System.Int64 contractTerminationID, IValidator validator):base("ContractTerminationEntity")
		{
			InitClassFetch(contractTerminationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ContractTerminationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_contractTerminationType = (ContractTerminationTypeEntity)info.GetValue("_contractTerminationType", typeof(ContractTerminationTypeEntity));
			if(_contractTerminationType!=null)
			{
				_contractTerminationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractTerminationTypeReturnsNewIfNotFound = info.GetBoolean("_contractTerminationTypeReturnsNewIfNotFound");
			_alwaysFetchContractTerminationType = info.GetBoolean("_alwaysFetchContractTerminationType");
			_alreadyFetchedContractTerminationType = info.GetBoolean("_alreadyFetchedContractTerminationType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ContractTerminationFieldIndex)fieldIndex)
			{
				case ContractTerminationFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case ContractTerminationFieldIndex.ContractTerminationTypeID:
					DesetupSyncContractTerminationType(true, false);
					_alreadyFetchedContractTerminationType = false;
					break;
				case ContractTerminationFieldIndex.EmployeeID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedContractTerminationType = (_contractTerminationType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingEmployeeID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "ContractTerminationType":
					toReturn.Add(Relations.ContractTerminationTypeEntityUsingContractTerminationTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_contractTerminationType", (!this.MarkedForDeletion?_contractTerminationType:null));
			info.AddValue("_contractTerminationTypeReturnsNewIfNotFound", _contractTerminationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContractTerminationType", _alwaysFetchContractTerminationType);
			info.AddValue("_alreadyFetchedContractTerminationType", _alreadyFetchedContractTerminationType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "ContractTerminationType":
					_alreadyFetchedContractTerminationType = true;
					this.ContractTerminationType = (ContractTerminationTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "ContractTerminationType":
					SetupSyncContractTerminationType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "ContractTerminationType":
					DesetupSyncContractTerminationType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_contractTerminationType!=null)
			{
				toReturn.Add(_contractTerminationType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractTerminationID)
		{
			return FetchUsingPK(contractTerminationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractTerminationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(contractTerminationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(contractTerminationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(contractTerminationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ContractTerminationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ContractTerminationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingEmployeeID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmployeeID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'ContractTerminationTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractTerminationTypeEntity' which is related to this entity.</returns>
		public ContractTerminationTypeEntity GetSingleContractTerminationType()
		{
			return GetSingleContractTerminationType(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractTerminationTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractTerminationTypeEntity' which is related to this entity.</returns>
		public virtual ContractTerminationTypeEntity GetSingleContractTerminationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedContractTerminationType || forceFetch || _alwaysFetchContractTerminationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractTerminationTypeEntityUsingContractTerminationTypeID);
				ContractTerminationTypeEntity newEntity = new ContractTerminationTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractTerminationTypeID);
				}
				if(fetchResult)
				{
					newEntity = (ContractTerminationTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractTerminationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ContractTerminationType = newEntity;
				_alreadyFetchedContractTerminationType = fetchResult;
			}
			return _contractTerminationType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserList", _userList);
			toReturn.Add("Contract", _contract);
			toReturn.Add("ContractTerminationType", _contractTerminationType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="validator">The validator object for this ContractTerminationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 contractTerminationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(contractTerminationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_userListReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_contractTerminationTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractTerminationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractTerminationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmployeeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.UserListEntityUsingEmployeeIDStatic, true, signalRelatedEntity, "ContractTerminations", resetFKFields, new int[] { (int)ContractTerminationFieldIndex.EmployeeID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.UserListEntityUsingEmployeeIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "ContractTerminations", resetFKFields, new int[] { (int)ContractTerminationFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contractTerminationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContractTerminationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contractTerminationType, new PropertyChangedEventHandler( OnContractTerminationTypePropertyChanged ), "ContractTerminationType", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.ContractTerminationTypeEntityUsingContractTerminationTypeIDStatic, true, signalRelatedEntity, "ContractTerminations", resetFKFields, new int[] { (int)ContractTerminationFieldIndex.ContractTerminationTypeID } );		
			_contractTerminationType = null;
		}
		
		/// <summary> setups the sync logic for member _contractTerminationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContractTerminationType(IEntityCore relatedEntity)
		{
			if(_contractTerminationType!=relatedEntity)
			{		
				DesetupSyncContractTerminationType(true, true);
				_contractTerminationType = (ContractTerminationTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contractTerminationType, new PropertyChangedEventHandler( OnContractTerminationTypePropertyChanged ), "ContractTerminationType", VarioSL.Entities.RelationClasses.StaticContractTerminationRelations.ContractTerminationTypeEntityUsingContractTerminationTypeIDStatic, true, ref _alreadyFetchedContractTerminationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractTerminationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="contractTerminationID">PK value for ContractTermination which data should be fetched into this ContractTermination object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 contractTerminationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ContractTerminationFieldIndex.ContractTerminationID].ForcedCurrentValueWrite(contractTerminationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateContractTerminationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ContractTerminationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ContractTerminationRelations Relations
		{
			get	{ return new ContractTerminationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.ContractTerminationEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.ContractTerminationEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractTerminationType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractTerminationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractTerminationTypeCollection(), (IEntityRelation)GetRelationsForField("ContractTerminationType")[0], (int)VarioSL.Entities.EntityType.ContractTerminationEntity, (int)VarioSL.Entities.EntityType.ContractTerminationTypeEntity, 0, null, null, null, "ContractTerminationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContractID property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)ContractTerminationFieldIndex.ContractID, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.ContractID, value, true); }
		}

		/// <summary> The ContractTerminationID property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."CONTRACTTERMINATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ContractTerminationID
		{
			get { return (System.Int64)GetValue((int)ContractTerminationFieldIndex.ContractTerminationID, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.ContractTerminationID, value, true); }
		}

		/// <summary> The ContractTerminationTypeID property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."CONTRACTTERMINATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractTerminationTypeID
		{
			get { return (System.Int64)GetValue((int)ContractTerminationFieldIndex.ContractTerminationTypeID, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.ContractTerminationTypeID, value, true); }
		}

		/// <summary> The Description property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ContractTerminationFieldIndex.Description, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.Description, value, true); }
		}

		/// <summary> The EmployeeID property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."EMPLOYEEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 EmployeeID
		{
			get { return (System.Int64)GetValue((int)ContractTerminationFieldIndex.EmployeeID, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.EmployeeID, value, true); }
		}

		/// <summary> The LastModified property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ContractTerminationFieldIndex.LastModified, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ContractTerminationFieldIndex.LastUser, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TerminationDate property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."TERMINATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TerminationDate
		{
			get { return (System.DateTime)GetValue((int)ContractTerminationFieldIndex.TerminationDate, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.TerminationDate, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ContractTermination<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACTTERMINATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ContractTerminationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ContractTerminationFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ContractTerminations", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ContractTerminations", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractTerminationTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContractTerminationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractTerminationTypeEntity ContractTerminationType
		{
			get	{ return GetSingleContractTerminationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContractTerminationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ContractTerminations", "ContractTerminationType", _contractTerminationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ContractTerminationType. When set to true, ContractTerminationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractTerminationType is accessed. You can always execute a forced fetch by calling GetSingleContractTerminationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractTerminationType
		{
			get	{ return _alwaysFetchContractTerminationType; }
			set	{ _alwaysFetchContractTerminationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractTerminationType already has been fetched. Setting this property to false when ContractTerminationType has been fetched
		/// will set ContractTerminationType to null as well. Setting this property to true while ContractTerminationType hasn't been fetched disables lazy loading for ContractTerminationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractTerminationType
		{
			get { return _alreadyFetchedContractTerminationType;}
			set 
			{
				if(_alreadyFetchedContractTerminationType && !value)
				{
					this.ContractTerminationType = null;
				}
				_alreadyFetchedContractTerminationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ContractTerminationType is not found
		/// in the database. When set to true, ContractTerminationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractTerminationTypeReturnsNewIfNotFound
		{
			get	{ return _contractTerminationTypeReturnsNewIfNotFound; }
			set { _contractTerminationTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ContractTerminationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
