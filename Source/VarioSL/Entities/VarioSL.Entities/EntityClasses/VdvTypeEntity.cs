﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProductNmType;
		private bool	_alwaysFetchVdvProductNmType, _alreadyFetchedVdvProductNmType;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProductSeType;
		private bool	_alwaysFetchVdvProductSeType, _alreadyFetchedVdvProductSeType;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProductsPriority;
		private bool	_alwaysFetchVdvProductsPriority, _alreadyFetchedVdvProductsPriority;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name VdvProductNmType</summary>
			public static readonly string VdvProductNmType = "VdvProductNmType";
			/// <summary>Member name VdvProductSeType</summary>
			public static readonly string VdvProductSeType = "VdvProductSeType";
			/// <summary>Member name VdvProductsPriority</summary>
			public static readonly string VdvProductsPriority = "VdvProductsPriority";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvTypeEntity() :base("VdvTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		public VdvTypeEntity(System.Int64 typeID):base("VdvTypeEntity")
		{
			InitClassFetch(typeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvTypeEntity(System.Int64 typeID, IPrefetchPath prefetchPathToUse):base("VdvTypeEntity")
		{
			InitClassFetch(typeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="validator">The custom validator object for this VdvTypeEntity</param>
		public VdvTypeEntity(System.Int64 typeID, IValidator validator):base("VdvTypeEntity")
		{
			InitClassFetch(typeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_vdvProductNmType = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProductNmType", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProductNmType = info.GetBoolean("_alwaysFetchVdvProductNmType");
			_alreadyFetchedVdvProductNmType = info.GetBoolean("_alreadyFetchedVdvProductNmType");

			_vdvProductSeType = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProductSeType", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProductSeType = info.GetBoolean("_alwaysFetchVdvProductSeType");
			_alreadyFetchedVdvProductSeType = info.GetBoolean("_alreadyFetchedVdvProductSeType");

			_vdvProductsPriority = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProductsPriority", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProductsPriority = info.GetBoolean("_alwaysFetchVdvProductsPriority");
			_alreadyFetchedVdvProductsPriority = info.GetBoolean("_alreadyFetchedVdvProductsPriority");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedVdvProductNmType = (_vdvProductNmType.Count > 0);
			_alreadyFetchedVdvProductSeType = (_vdvProductSeType.Count > 0);
			_alreadyFetchedVdvProductsPriority = (_vdvProductsPriority.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "VdvProductNmType":
					toReturn.Add(Relations.VdvProductEntityUsingIssueModeNm);
					break;
				case "VdvProductSeType":
					toReturn.Add(Relations.VdvProductEntityUsingIssueModeSe);
					break;
				case "VdvProductsPriority":
					toReturn.Add(Relations.VdvProductEntityUsingPriorityMode);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_vdvProductNmType", (!this.MarkedForDeletion?_vdvProductNmType:null));
			info.AddValue("_alwaysFetchVdvProductNmType", _alwaysFetchVdvProductNmType);
			info.AddValue("_alreadyFetchedVdvProductNmType", _alreadyFetchedVdvProductNmType);
			info.AddValue("_vdvProductSeType", (!this.MarkedForDeletion?_vdvProductSeType:null));
			info.AddValue("_alwaysFetchVdvProductSeType", _alwaysFetchVdvProductSeType);
			info.AddValue("_alreadyFetchedVdvProductSeType", _alreadyFetchedVdvProductSeType);
			info.AddValue("_vdvProductsPriority", (!this.MarkedForDeletion?_vdvProductsPriority:null));
			info.AddValue("_alwaysFetchVdvProductsPriority", _alwaysFetchVdvProductsPriority);
			info.AddValue("_alreadyFetchedVdvProductsPriority", _alreadyFetchedVdvProductsPriority);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "VdvProductNmType":
					_alreadyFetchedVdvProductNmType = true;
					if(entity!=null)
					{
						this.VdvProductNmType.Add((VdvProductEntity)entity);
					}
					break;
				case "VdvProductSeType":
					_alreadyFetchedVdvProductSeType = true;
					if(entity!=null)
					{
						this.VdvProductSeType.Add((VdvProductEntity)entity);
					}
					break;
				case "VdvProductsPriority":
					_alreadyFetchedVdvProductsPriority = true;
					if(entity!=null)
					{
						this.VdvProductsPriority.Add((VdvProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "VdvProductNmType":
					_vdvProductNmType.Add((VdvProductEntity)relatedEntity);
					break;
				case "VdvProductSeType":
					_vdvProductSeType.Add((VdvProductEntity)relatedEntity);
					break;
				case "VdvProductsPriority":
					_vdvProductsPriority.Add((VdvProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "VdvProductNmType":
					this.PerformRelatedEntityRemoval(_vdvProductNmType, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProductSeType":
					this.PerformRelatedEntityRemoval(_vdvProductSeType, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProductsPriority":
					this.PerformRelatedEntityRemoval(_vdvProductsPriority, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_vdvProductNmType);
			toReturn.Add(_vdvProductSeType);
			toReturn.Add(_vdvProductsPriority);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID)
		{
			return FetchUsingPK(typeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(typeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductNmType(bool forceFetch)
		{
			return GetMultiVdvProductNmType(forceFetch, _vdvProductNmType.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductNmType(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProductNmType(forceFetch, _vdvProductNmType.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductNmType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProductNmType(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductNmType(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProductNmType || forceFetch || _alwaysFetchVdvProductNmType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProductNmType);
				_vdvProductNmType.SuppressClearInGetMulti=!forceFetch;
				_vdvProductNmType.EntityFactoryToUse = entityFactoryToUse;
				_vdvProductNmType.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_vdvProductNmType.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProductNmType = true;
			}
			return _vdvProductNmType;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProductNmType'. These settings will be taken into account
		/// when the property VdvProductNmType is requested or GetMultiVdvProductNmType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProductNmType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProductNmType.SortClauses=sortClauses;
			_vdvProductNmType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductSeType(bool forceFetch)
		{
			return GetMultiVdvProductSeType(forceFetch, _vdvProductSeType.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductSeType(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProductSeType(forceFetch, _vdvProductSeType.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductSeType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProductSeType(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductSeType(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProductSeType || forceFetch || _alwaysFetchVdvProductSeType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProductSeType);
				_vdvProductSeType.SuppressClearInGetMulti=!forceFetch;
				_vdvProductSeType.EntityFactoryToUse = entityFactoryToUse;
				_vdvProductSeType.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_vdvProductSeType.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProductSeType = true;
			}
			return _vdvProductSeType;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProductSeType'. These settings will be taken into account
		/// when the property VdvProductSeType is requested or GetMultiVdvProductSeType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProductSeType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProductSeType.SortClauses=sortClauses;
			_vdvProductSeType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductsPriority(bool forceFetch)
		{
			return GetMultiVdvProductsPriority(forceFetch, _vdvProductsPriority.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductsPriority(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProductsPriority(forceFetch, _vdvProductsPriority.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductsPriority(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProductsPriority(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProductsPriority(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProductsPriority || forceFetch || _alwaysFetchVdvProductsPriority) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProductsPriority);
				_vdvProductsPriority.SuppressClearInGetMulti=!forceFetch;
				_vdvProductsPriority.EntityFactoryToUse = entityFactoryToUse;
				_vdvProductsPriority.GetMultiManyToOne(null, null, null, null, null, null, null, this, filter);
				_vdvProductsPriority.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProductsPriority = true;
			}
			return _vdvProductsPriority;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProductsPriority'. These settings will be taken into account
		/// when the property VdvProductsPriority is requested or GetMultiVdvProductsPriority is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProductsPriority(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProductsPriority.SortClauses=sortClauses;
			_vdvProductsPriority.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("VdvProductNmType", _vdvProductNmType);
			toReturn.Add("VdvProductSeType", _vdvProductSeType);
			toReturn.Add("VdvProductsPriority", _vdvProductsPriority);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="validator">The validator object for this VdvTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 typeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(typeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_vdvProductNmType = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProductNmType.SetContainingEntityInfo(this, "IssueModeNmType");

			_vdvProductSeType = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProductSeType.SetContainingEntityInfo(this, "IssueModeSeType");

			_vdvProductsPriority = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProductsPriority.SetContainingEntityInfo(this, "PriorityModeType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="typeID">PK value for VdvType which data should be fetched into this VdvType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvTypeFieldIndex.TypeID].ForcedCurrentValueWrite(typeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvTypeRelations Relations
		{
			get	{ return new VdvTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProductNmType
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProductNmType")[0], (int)VarioSL.Entities.EntityType.VdvTypeEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProductNmType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProductSeType
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProductSeType")[0], (int)VarioSL.Entities.EntityType.VdvTypeEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProductSeType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProductsPriority
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProductsPriority")[0], (int)VarioSL.Entities.EntityType.VdvTypeEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProductsPriority", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity VdvType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_TYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)VdvTypeFieldIndex.Description, true); }
			set	{ SetValue((int)VdvTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The TypeGroupID property of the Entity VdvType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_TYPE"."TYPEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffVdvTypeGroup TypeGroupID
		{
			get { return (VarioSL.Entities.Enumerations.TariffVdvTypeGroup)GetValue((int)VdvTypeFieldIndex.TypeGroupID, true); }
			set	{ SetValue((int)VdvTypeFieldIndex.TypeGroupID, value, true); }
		}

		/// <summary> The TypeID property of the Entity VdvType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_TYPE"."TYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TypeID
		{
			get { return (System.Int64)GetValue((int)VdvTypeFieldIndex.TypeID, true); }
			set	{ SetValue((int)VdvTypeFieldIndex.TypeID, value, true); }
		}

		/// <summary> The TypeName property of the Entity VdvType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_TYPE"."TYPENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TypeName
		{
			get { return (System.String)GetValue((int)VdvTypeFieldIndex.TypeName, true); }
			set	{ SetValue((int)VdvTypeFieldIndex.TypeName, value, true); }
		}

		/// <summary> The TypeNumber property of the Entity VdvType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_TYPE"."TYPENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TypeNumber
		{
			get { return (System.Int64)GetValue((int)VdvTypeFieldIndex.TypeNumber, true); }
			set	{ SetValue((int)VdvTypeFieldIndex.TypeNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProductNmType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProductNmType
		{
			get	{ return GetMultiVdvProductNmType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProductNmType. When set to true, VdvProductNmType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProductNmType is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProductNmType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProductNmType
		{
			get	{ return _alwaysFetchVdvProductNmType; }
			set	{ _alwaysFetchVdvProductNmType = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProductNmType already has been fetched. Setting this property to false when VdvProductNmType has been fetched
		/// will clear the VdvProductNmType collection well. Setting this property to true while VdvProductNmType hasn't been fetched disables lazy loading for VdvProductNmType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProductNmType
		{
			get { return _alreadyFetchedVdvProductNmType;}
			set 
			{
				if(_alreadyFetchedVdvProductNmType && !value && (_vdvProductNmType != null))
				{
					_vdvProductNmType.Clear();
				}
				_alreadyFetchedVdvProductNmType = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProductSeType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProductSeType
		{
			get	{ return GetMultiVdvProductSeType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProductSeType. When set to true, VdvProductSeType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProductSeType is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProductSeType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProductSeType
		{
			get	{ return _alwaysFetchVdvProductSeType; }
			set	{ _alwaysFetchVdvProductSeType = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProductSeType already has been fetched. Setting this property to false when VdvProductSeType has been fetched
		/// will clear the VdvProductSeType collection well. Setting this property to true while VdvProductSeType hasn't been fetched disables lazy loading for VdvProductSeType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProductSeType
		{
			get { return _alreadyFetchedVdvProductSeType;}
			set 
			{
				if(_alreadyFetchedVdvProductSeType && !value && (_vdvProductSeType != null))
				{
					_vdvProductSeType.Clear();
				}
				_alreadyFetchedVdvProductSeType = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProductsPriority()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProductsPriority
		{
			get	{ return GetMultiVdvProductsPriority(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProductsPriority. When set to true, VdvProductsPriority is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProductsPriority is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProductsPriority(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProductsPriority
		{
			get	{ return _alwaysFetchVdvProductsPriority; }
			set	{ _alwaysFetchVdvProductsPriority = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProductsPriority already has been fetched. Setting this property to false when VdvProductsPriority has been fetched
		/// will clear the VdvProductsPriority collection well. Setting this property to true while VdvProductsPriority hasn't been fetched disables lazy loading for VdvProductsPriority</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProductsPriority
		{
			get { return _alreadyFetchedVdvProductsPriority;}
			set 
			{
				if(_alreadyFetchedVdvProductsPriority && !value && (_vdvProductsPriority != null))
				{
					_vdvProductsPriority.Clear();
				}
				_alreadyFetchedVdvProductsPriority = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
