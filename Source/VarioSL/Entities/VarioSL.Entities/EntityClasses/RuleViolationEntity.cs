﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RuleViolation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RuleViolationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection	_ruleViolationToTransactions;
		private bool	_alwaysFetchRuleViolationToTransactions, _alreadyFetchedRuleViolationToTransactions;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name RuleViolationToTransactions</summary>
			public static readonly string RuleViolationToTransactions = "RuleViolationToTransactions";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RuleViolationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleViolationEntity() :base("RuleViolationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		public RuleViolationEntity(System.Int64 ruleViolationID):base("RuleViolationEntity")
		{
			InitClassFetch(ruleViolationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RuleViolationEntity(System.Int64 ruleViolationID, IPrefetchPath prefetchPathToUse):base("RuleViolationEntity")
		{
			InitClassFetch(ruleViolationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="validator">The custom validator object for this RuleViolationEntity</param>
		public RuleViolationEntity(System.Int64 ruleViolationID, IValidator validator):base("RuleViolationEntity")
		{
			InitClassFetch(ruleViolationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleViolationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ruleViolationToTransactions = (VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection)info.GetValue("_ruleViolationToTransactions", typeof(VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection));
			_alwaysFetchRuleViolationToTransactions = info.GetBoolean("_alwaysFetchRuleViolationToTransactions");
			_alreadyFetchedRuleViolationToTransactions = info.GetBoolean("_alreadyFetchedRuleViolationToTransactions");
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RuleViolationFieldIndex)fieldIndex)
			{
				case RuleViolationFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case RuleViolationFieldIndex.PaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				case RuleViolationFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRuleViolationToTransactions = (_ruleViolationToTransactions.Count > 0);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "RuleViolationToTransactions":
					toReturn.Add(Relations.RuleViolationToTransactionEntityUsingRuleViolationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ruleViolationToTransactions", (!this.MarkedForDeletion?_ruleViolationToTransactions:null));
			info.AddValue("_alwaysFetchRuleViolationToTransactions", _alwaysFetchRuleViolationToTransactions);
			info.AddValue("_alreadyFetchedRuleViolationToTransactions", _alreadyFetchedRuleViolationToTransactions);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "RuleViolationToTransactions":
					_alreadyFetchedRuleViolationToTransactions = true;
					if(entity!=null)
					{
						this.RuleViolationToTransactions.Add((RuleViolationToTransactionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "RuleViolationToTransactions":
					_ruleViolationToTransactions.Add((RuleViolationToTransactionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "RuleViolationToTransactions":
					this.PerformRelatedEntityRemoval(_ruleViolationToTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ruleViolationToTransactions);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationID)
		{
			return FetchUsingPK(ruleViolationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ruleViolationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ruleViolationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleViolationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ruleViolationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RuleViolationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RuleViolationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolationToTransactions || forceFetch || _alwaysFetchRuleViolationToTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolationToTransactions);
				_ruleViolationToTransactions.SuppressClearInGetMulti=!forceFetch;
				_ruleViolationToTransactions.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolationToTransactions.GetMultiManyToOne(null, null, this, null, filter);
				_ruleViolationToTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolationToTransactions = true;
			}
			return _ruleViolationToTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolationToTransactions'. These settings will be taken into account
		/// when the property RuleViolationToTransactions is requested or GetMultiRuleViolationToTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolationToTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolationToTransactions.SortClauses=sortClauses;
			_ruleViolationToTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("PaymentOption", _paymentOption);
			toReturn.Add("Product", _product);
			toReturn.Add("RuleViolationToTransactions", _ruleViolationToTransactions);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="validator">The validator object for this RuleViolationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ruleViolationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ruleViolationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ruleViolationToTransactions = new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection();
			_ruleViolationToTransactions.SetContainingEntityInfo(this, "RuleViolation");
			_cardReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExecutionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsResolved", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationProvider", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationSourceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleViolationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViolationValue", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "RuleViolations", resetFKFields, new int[] { (int)RuleViolationFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, signalRelatedEntity, "RuleViolations", resetFKFields, new int[] { (int)RuleViolationFieldIndex.PaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "RuleViolations", resetFKFields, new int[] { (int)RuleViolationFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticRuleViolationRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ruleViolationID">PK value for RuleViolation which data should be fetched into this RuleViolation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ruleViolationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RuleViolationFieldIndex.RuleViolationID].ForcedCurrentValueWrite(ruleViolationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRuleViolationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RuleViolationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RuleViolationRelations Relations
		{
			get	{ return new RuleViolationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolationToTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolationToTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection(), (IEntityRelation)GetRelationsForField("RuleViolationToTransactions")[0], (int)VarioSL.Entities.EntityType.RuleViolationEntity, (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, 0, null, null, null, "RuleViolationToTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.RuleViolationEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.RuleViolationEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.RuleViolationEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AggregationFrom property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."AGGREGATIONFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationFrom
		{
			get { return (System.DateTime)GetValue((int)RuleViolationFieldIndex.AggregationFrom, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.AggregationFrom, value, true); }
		}

		/// <summary> The AggregationTo property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."AGGREGATIONTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationTo
		{
			get { return (System.DateTime)GetValue((int)RuleViolationFieldIndex.AggregationTo, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.AggregationTo, value, true); }
		}

		/// <summary> The CardID property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationFieldIndex.CardID, false); }
			set	{ SetValue((int)RuleViolationFieldIndex.CardID, value, true); }
		}

		/// <summary> The ExecutionTime property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."EXECUTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ExecutionTime
		{
			get { return (System.DateTime)GetValue((int)RuleViolationFieldIndex.ExecutionTime, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.ExecutionTime, value, true); }
		}

		/// <summary> The IsResolved property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."ISRESOLVED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsResolved
		{
			get { return (System.Boolean)GetValue((int)RuleViolationFieldIndex.IsResolved, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.IsResolved, value, true); }
		}

		/// <summary> The LastModified property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)RuleViolationFieldIndex.LastModified, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)RuleViolationFieldIndex.LastUser, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Message property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."MESSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)RuleViolationFieldIndex.Message, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.Message, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationFieldIndex.PaymentOptionID, false); }
			set	{ SetValue((int)RuleViolationFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The PaymentToken property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."PAYMENTTOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentToken
		{
			get { return (System.String)GetValue((int)RuleViolationFieldIndex.PaymentToken, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.PaymentToken, value, true); }
		}

		/// <summary> The ProductID property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationFieldIndex.ProductID, false); }
			set	{ SetValue((int)RuleViolationFieldIndex.ProductID, value, true); }
		}

		/// <summary> The RuleViolationID property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."RULEVIOLATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RuleViolationID
		{
			get { return (System.Int64)GetValue((int)RuleViolationFieldIndex.RuleViolationID, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.RuleViolationID, value, true); }
		}

		/// <summary> The RuleViolationProvider property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."RULEVIOLATIONPROVIDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.RuleViolationProvider RuleViolationProvider
		{
			get { return (VarioSL.Entities.Enumerations.RuleViolationProvider)GetValue((int)RuleViolationFieldIndex.RuleViolationProvider, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.RuleViolationProvider, value, true); }
		}

		/// <summary> The RuleViolationSourceType property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."RULEVIOLATIONSOURCETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.RuleViolationSourceType RuleViolationSourceType
		{
			get { return (VarioSL.Entities.Enumerations.RuleViolationSourceType)GetValue((int)RuleViolationFieldIndex.RuleViolationSourceType, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.RuleViolationSourceType, value, true); }
		}

		/// <summary> The RuleViolationType property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."RULEVIOLATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.RuleViolationType RuleViolationType
		{
			get { return (VarioSL.Entities.Enumerations.RuleViolationType)GetValue((int)RuleViolationFieldIndex.RuleViolationType, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.RuleViolationType, value, true); }
		}

		/// <summary> The Text property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."TEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)RuleViolationFieldIndex.Text, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.Text, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)RuleViolationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RuleViolationFieldIndex.TransactionJournalID, false); }
			set	{ SetValue((int)RuleViolationFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The ViolationValue property of the Entity RuleViolation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_RULEVIOLATION"."VIOLATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ViolationValue
		{
			get { return (System.String)GetValue((int)RuleViolationFieldIndex.ViolationValue, true); }
			set	{ SetValue((int)RuleViolationFieldIndex.ViolationValue, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolationToTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection RuleViolationToTransactions
		{
			get	{ return GetMultiRuleViolationToTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolationToTransactions. When set to true, RuleViolationToTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolationToTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolationToTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolationToTransactions
		{
			get	{ return _alwaysFetchRuleViolationToTransactions; }
			set	{ _alwaysFetchRuleViolationToTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolationToTransactions already has been fetched. Setting this property to false when RuleViolationToTransactions has been fetched
		/// will clear the RuleViolationToTransactions collection well. Setting this property to true while RuleViolationToTransactions hasn't been fetched disables lazy loading for RuleViolationToTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolationToTransactions
		{
			get { return _alreadyFetchedRuleViolationToTransactions;}
			set 
			{
				if(_alreadyFetchedRuleViolationToTransactions && !value && (_ruleViolationToTransactions != null))
				{
					_ruleViolationToTransactions.Clear();
				}
				_alreadyFetchedRuleViolationToTransactions = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolations", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolations", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleViolations", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RuleViolationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
