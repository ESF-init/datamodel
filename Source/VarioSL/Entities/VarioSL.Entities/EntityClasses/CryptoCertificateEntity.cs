﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CryptoCertificate'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CryptoCertificateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CryptoCertificateCollection	_childCertificates;
		private bool	_alwaysFetchChildCertificates, _alreadyFetchedChildCertificates;
		private VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection	_cryptoOperatorActivationkeys;
		private bool	_alwaysFetchCryptoOperatorActivationkeys, _alreadyFetchedCryptoOperatorActivationkeys;
		private VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection	_cryptoSamSignCertificates;
		private bool	_alwaysFetchCryptoSamSignCertificates, _alreadyFetchedCryptoSamSignCertificates;
		private VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection	_cryptoUicCertificates;
		private bool	_alwaysFetchCryptoUicCertificates, _alreadyFetchedCryptoUicCertificates;
		private CryptoCertificateEntity _parentCertificate;
		private bool	_alwaysFetchParentCertificate, _alreadyFetchedParentCertificate, _parentCertificateReturnsNewIfNotFound;
		private CryptoQArchiveEntity _cryptoQArchive;
		private bool	_alwaysFetchCryptoQArchive, _alreadyFetchedCryptoQArchive, _cryptoQArchiveReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentCertificate</summary>
			public static readonly string ParentCertificate = "ParentCertificate";
			/// <summary>Member name CryptoQArchive</summary>
			public static readonly string CryptoQArchive = "CryptoQArchive";
			/// <summary>Member name ChildCertificates</summary>
			public static readonly string ChildCertificates = "ChildCertificates";
			/// <summary>Member name CryptoOperatorActivationkeys</summary>
			public static readonly string CryptoOperatorActivationkeys = "CryptoOperatorActivationkeys";
			/// <summary>Member name CryptoSamSignCertificates</summary>
			public static readonly string CryptoSamSignCertificates = "CryptoSamSignCertificates";
			/// <summary>Member name CryptoUicCertificates</summary>
			public static readonly string CryptoUicCertificates = "CryptoUicCertificates";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CryptoCertificateEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CryptoCertificateEntity() :base("CryptoCertificateEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		public CryptoCertificateEntity(System.Int64 certificateID):base("CryptoCertificateEntity")
		{
			InitClassFetch(certificateID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CryptoCertificateEntity(System.Int64 certificateID, IPrefetchPath prefetchPathToUse):base("CryptoCertificateEntity")
		{
			InitClassFetch(certificateID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="validator">The custom validator object for this CryptoCertificateEntity</param>
		public CryptoCertificateEntity(System.Int64 certificateID, IValidator validator):base("CryptoCertificateEntity")
		{
			InitClassFetch(certificateID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CryptoCertificateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_childCertificates = (VarioSL.Entities.CollectionClasses.CryptoCertificateCollection)info.GetValue("_childCertificates", typeof(VarioSL.Entities.CollectionClasses.CryptoCertificateCollection));
			_alwaysFetchChildCertificates = info.GetBoolean("_alwaysFetchChildCertificates");
			_alreadyFetchedChildCertificates = info.GetBoolean("_alreadyFetchedChildCertificates");

			_cryptoOperatorActivationkeys = (VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection)info.GetValue("_cryptoOperatorActivationkeys", typeof(VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection));
			_alwaysFetchCryptoOperatorActivationkeys = info.GetBoolean("_alwaysFetchCryptoOperatorActivationkeys");
			_alreadyFetchedCryptoOperatorActivationkeys = info.GetBoolean("_alreadyFetchedCryptoOperatorActivationkeys");

			_cryptoSamSignCertificates = (VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection)info.GetValue("_cryptoSamSignCertificates", typeof(VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection));
			_alwaysFetchCryptoSamSignCertificates = info.GetBoolean("_alwaysFetchCryptoSamSignCertificates");
			_alreadyFetchedCryptoSamSignCertificates = info.GetBoolean("_alreadyFetchedCryptoSamSignCertificates");

			_cryptoUicCertificates = (VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection)info.GetValue("_cryptoUicCertificates", typeof(VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection));
			_alwaysFetchCryptoUicCertificates = info.GetBoolean("_alwaysFetchCryptoUicCertificates");
			_alreadyFetchedCryptoUicCertificates = info.GetBoolean("_alreadyFetchedCryptoUicCertificates");
			_parentCertificate = (CryptoCertificateEntity)info.GetValue("_parentCertificate", typeof(CryptoCertificateEntity));
			if(_parentCertificate!=null)
			{
				_parentCertificate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentCertificateReturnsNewIfNotFound = info.GetBoolean("_parentCertificateReturnsNewIfNotFound");
			_alwaysFetchParentCertificate = info.GetBoolean("_alwaysFetchParentCertificate");
			_alreadyFetchedParentCertificate = info.GetBoolean("_alreadyFetchedParentCertificate");

			_cryptoQArchive = (CryptoQArchiveEntity)info.GetValue("_cryptoQArchive", typeof(CryptoQArchiveEntity));
			if(_cryptoQArchive!=null)
			{
				_cryptoQArchive.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoQArchiveReturnsNewIfNotFound = info.GetBoolean("_cryptoQArchiveReturnsNewIfNotFound");
			_alwaysFetchCryptoQArchive = info.GetBoolean("_alwaysFetchCryptoQArchive");
			_alreadyFetchedCryptoQArchive = info.GetBoolean("_alreadyFetchedCryptoQArchive");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CryptoCertificateFieldIndex)fieldIndex)
			{
				case CryptoCertificateFieldIndex.ParentID:
					DesetupSyncParentCertificate(true, false);
					_alreadyFetchedParentCertificate = false;
					break;
				case CryptoCertificateFieldIndex.QArchiveID:
					DesetupSyncCryptoQArchive(true, false);
					_alreadyFetchedCryptoQArchive = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedChildCertificates = (_childCertificates.Count > 0);
			_alreadyFetchedCryptoOperatorActivationkeys = (_cryptoOperatorActivationkeys.Count > 0);
			_alreadyFetchedCryptoSamSignCertificates = (_cryptoSamSignCertificates.Count > 0);
			_alreadyFetchedCryptoUicCertificates = (_cryptoUicCertificates.Count > 0);
			_alreadyFetchedParentCertificate = (_parentCertificate != null);
			_alreadyFetchedCryptoQArchive = (_cryptoQArchive != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentCertificate":
					toReturn.Add(Relations.CryptoCertificateEntityUsingCertificateIDParentID);
					break;
				case "CryptoQArchive":
					toReturn.Add(Relations.CryptoQArchiveEntityUsingQArchiveID);
					break;
				case "ChildCertificates":
					toReturn.Add(Relations.CryptoCertificateEntityUsingParentID);
					break;
				case "CryptoOperatorActivationkeys":
					toReturn.Add(Relations.CryptoOperatorActivationkeyEntityUsingCertificateID);
					break;
				case "CryptoSamSignCertificates":
					toReturn.Add(Relations.CryptoSamSignCertificateEntityUsingCertificateID);
					break;
				case "CryptoUicCertificates":
					toReturn.Add(Relations.CryptoUicCertificateEntityUsingCertificateID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_childCertificates", (!this.MarkedForDeletion?_childCertificates:null));
			info.AddValue("_alwaysFetchChildCertificates", _alwaysFetchChildCertificates);
			info.AddValue("_alreadyFetchedChildCertificates", _alreadyFetchedChildCertificates);
			info.AddValue("_cryptoOperatorActivationkeys", (!this.MarkedForDeletion?_cryptoOperatorActivationkeys:null));
			info.AddValue("_alwaysFetchCryptoOperatorActivationkeys", _alwaysFetchCryptoOperatorActivationkeys);
			info.AddValue("_alreadyFetchedCryptoOperatorActivationkeys", _alreadyFetchedCryptoOperatorActivationkeys);
			info.AddValue("_cryptoSamSignCertificates", (!this.MarkedForDeletion?_cryptoSamSignCertificates:null));
			info.AddValue("_alwaysFetchCryptoSamSignCertificates", _alwaysFetchCryptoSamSignCertificates);
			info.AddValue("_alreadyFetchedCryptoSamSignCertificates", _alreadyFetchedCryptoSamSignCertificates);
			info.AddValue("_cryptoUicCertificates", (!this.MarkedForDeletion?_cryptoUicCertificates:null));
			info.AddValue("_alwaysFetchCryptoUicCertificates", _alwaysFetchCryptoUicCertificates);
			info.AddValue("_alreadyFetchedCryptoUicCertificates", _alreadyFetchedCryptoUicCertificates);
			info.AddValue("_parentCertificate", (!this.MarkedForDeletion?_parentCertificate:null));
			info.AddValue("_parentCertificateReturnsNewIfNotFound", _parentCertificateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentCertificate", _alwaysFetchParentCertificate);
			info.AddValue("_alreadyFetchedParentCertificate", _alreadyFetchedParentCertificate);
			info.AddValue("_cryptoQArchive", (!this.MarkedForDeletion?_cryptoQArchive:null));
			info.AddValue("_cryptoQArchiveReturnsNewIfNotFound", _cryptoQArchiveReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoQArchive", _alwaysFetchCryptoQArchive);
			info.AddValue("_alreadyFetchedCryptoQArchive", _alreadyFetchedCryptoQArchive);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentCertificate":
					_alreadyFetchedParentCertificate = true;
					this.ParentCertificate = (CryptoCertificateEntity)entity;
					break;
				case "CryptoQArchive":
					_alreadyFetchedCryptoQArchive = true;
					this.CryptoQArchive = (CryptoQArchiveEntity)entity;
					break;
				case "ChildCertificates":
					_alreadyFetchedChildCertificates = true;
					if(entity!=null)
					{
						this.ChildCertificates.Add((CryptoCertificateEntity)entity);
					}
					break;
				case "CryptoOperatorActivationkeys":
					_alreadyFetchedCryptoOperatorActivationkeys = true;
					if(entity!=null)
					{
						this.CryptoOperatorActivationkeys.Add((CryptoOperatorActivationkeyEntity)entity);
					}
					break;
				case "CryptoSamSignCertificates":
					_alreadyFetchedCryptoSamSignCertificates = true;
					if(entity!=null)
					{
						this.CryptoSamSignCertificates.Add((CryptoSamSignCertificateEntity)entity);
					}
					break;
				case "CryptoUicCertificates":
					_alreadyFetchedCryptoUicCertificates = true;
					if(entity!=null)
					{
						this.CryptoUicCertificates.Add((CryptoUicCertificateEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentCertificate":
					SetupSyncParentCertificate(relatedEntity);
					break;
				case "CryptoQArchive":
					SetupSyncCryptoQArchive(relatedEntity);
					break;
				case "ChildCertificates":
					_childCertificates.Add((CryptoCertificateEntity)relatedEntity);
					break;
				case "CryptoOperatorActivationkeys":
					_cryptoOperatorActivationkeys.Add((CryptoOperatorActivationkeyEntity)relatedEntity);
					break;
				case "CryptoSamSignCertificates":
					_cryptoSamSignCertificates.Add((CryptoSamSignCertificateEntity)relatedEntity);
					break;
				case "CryptoUicCertificates":
					_cryptoUicCertificates.Add((CryptoUicCertificateEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentCertificate":
					DesetupSyncParentCertificate(false, true);
					break;
				case "CryptoQArchive":
					DesetupSyncCryptoQArchive(false, true);
					break;
				case "ChildCertificates":
					this.PerformRelatedEntityRemoval(_childCertificates, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoOperatorActivationkeys":
					this.PerformRelatedEntityRemoval(_cryptoOperatorActivationkeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoSamSignCertificates":
					this.PerformRelatedEntityRemoval(_cryptoSamSignCertificates, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CryptoUicCertificates":
					this.PerformRelatedEntityRemoval(_cryptoUicCertificates, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentCertificate!=null)
			{
				toReturn.Add(_parentCertificate);
			}
			if(_cryptoQArchive!=null)
			{
				toReturn.Add(_cryptoQArchive);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_childCertificates);
			toReturn.Add(_cryptoOperatorActivationkeys);
			toReturn.Add(_cryptoSamSignCertificates);
			toReturn.Add(_cryptoUicCertificates);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID)
		{
			return FetchUsingPK(certificateID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(certificateID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(certificateID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(certificateID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CertificateID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CryptoCertificateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CryptoCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCertificateCollection GetMultiChildCertificates(bool forceFetch)
		{
			return GetMultiChildCertificates(forceFetch, _childCertificates.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCertificateCollection GetMultiChildCertificates(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildCertificates(forceFetch, _childCertificates.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoCertificateCollection GetMultiChildCertificates(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildCertificates(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoCertificateCollection GetMultiChildCertificates(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildCertificates || forceFetch || _alwaysFetchChildCertificates) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childCertificates);
				_childCertificates.SuppressClearInGetMulti=!forceFetch;
				_childCertificates.EntityFactoryToUse = entityFactoryToUse;
				_childCertificates.GetMultiManyToOne(this, null, filter);
				_childCertificates.SuppressClearInGetMulti=false;
				_alreadyFetchedChildCertificates = true;
			}
			return _childCertificates;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildCertificates'. These settings will be taken into account
		/// when the property ChildCertificates is requested or GetMultiChildCertificates is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildCertificates(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childCertificates.SortClauses=sortClauses;
			_childCertificates.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoOperatorActivationkeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoOperatorActivationkeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection GetMultiCryptoOperatorActivationkeys(bool forceFetch)
		{
			return GetMultiCryptoOperatorActivationkeys(forceFetch, _cryptoOperatorActivationkeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoOperatorActivationkeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoOperatorActivationkeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection GetMultiCryptoOperatorActivationkeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoOperatorActivationkeys(forceFetch, _cryptoOperatorActivationkeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoOperatorActivationkeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection GetMultiCryptoOperatorActivationkeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoOperatorActivationkeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoOperatorActivationkeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection GetMultiCryptoOperatorActivationkeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoOperatorActivationkeys || forceFetch || _alwaysFetchCryptoOperatorActivationkeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoOperatorActivationkeys);
				_cryptoOperatorActivationkeys.SuppressClearInGetMulti=!forceFetch;
				_cryptoOperatorActivationkeys.EntityFactoryToUse = entityFactoryToUse;
				_cryptoOperatorActivationkeys.GetMultiManyToOne(this, filter);
				_cryptoOperatorActivationkeys.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoOperatorActivationkeys = true;
			}
			return _cryptoOperatorActivationkeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoOperatorActivationkeys'. These settings will be taken into account
		/// when the property CryptoOperatorActivationkeys is requested or GetMultiCryptoOperatorActivationkeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoOperatorActivationkeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoOperatorActivationkeys.SortClauses=sortClauses;
			_cryptoOperatorActivationkeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoSamSignCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoSamSignCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection GetMultiCryptoSamSignCertificates(bool forceFetch)
		{
			return GetMultiCryptoSamSignCertificates(forceFetch, _cryptoSamSignCertificates.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoSamSignCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoSamSignCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection GetMultiCryptoSamSignCertificates(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoSamSignCertificates(forceFetch, _cryptoSamSignCertificates.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoSamSignCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection GetMultiCryptoSamSignCertificates(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoSamSignCertificates(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoSamSignCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection GetMultiCryptoSamSignCertificates(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoSamSignCertificates || forceFetch || _alwaysFetchCryptoSamSignCertificates) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoSamSignCertificates);
				_cryptoSamSignCertificates.SuppressClearInGetMulti=!forceFetch;
				_cryptoSamSignCertificates.EntityFactoryToUse = entityFactoryToUse;
				_cryptoSamSignCertificates.GetMultiManyToOne(this, filter);
				_cryptoSamSignCertificates.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoSamSignCertificates = true;
			}
			return _cryptoSamSignCertificates;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoSamSignCertificates'. These settings will be taken into account
		/// when the property CryptoSamSignCertificates is requested or GetMultiCryptoSamSignCertificates is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoSamSignCertificates(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoSamSignCertificates.SortClauses=sortClauses;
			_cryptoSamSignCertificates.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CryptoUicCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CryptoUicCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection GetMultiCryptoUicCertificates(bool forceFetch)
		{
			return GetMultiCryptoUicCertificates(forceFetch, _cryptoUicCertificates.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoUicCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CryptoUicCertificateEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection GetMultiCryptoUicCertificates(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCryptoUicCertificates(forceFetch, _cryptoUicCertificates.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CryptoUicCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection GetMultiCryptoUicCertificates(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCryptoUicCertificates(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CryptoUicCertificateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection GetMultiCryptoUicCertificates(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCryptoUicCertificates || forceFetch || _alwaysFetchCryptoUicCertificates) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cryptoUicCertificates);
				_cryptoUicCertificates.SuppressClearInGetMulti=!forceFetch;
				_cryptoUicCertificates.EntityFactoryToUse = entityFactoryToUse;
				_cryptoUicCertificates.GetMultiManyToOne(this, filter);
				_cryptoUicCertificates.SuppressClearInGetMulti=false;
				_alreadyFetchedCryptoUicCertificates = true;
			}
			return _cryptoUicCertificates;
		}

		/// <summary> Sets the collection parameters for the collection for 'CryptoUicCertificates'. These settings will be taken into account
		/// when the property CryptoUicCertificates is requested or GetMultiCryptoUicCertificates is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCryptoUicCertificates(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cryptoUicCertificates.SortClauses=sortClauses;
			_cryptoUicCertificates.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CryptoCertificateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoCertificateEntity' which is related to this entity.</returns>
		public CryptoCertificateEntity GetSingleParentCertificate()
		{
			return GetSingleParentCertificate(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoCertificateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoCertificateEntity' which is related to this entity.</returns>
		public virtual CryptoCertificateEntity GetSingleParentCertificate(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentCertificate || forceFetch || _alwaysFetchParentCertificate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoCertificateEntityUsingCertificateIDParentID);
				CryptoCertificateEntity newEntity = new CryptoCertificateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CryptoCertificateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentCertificateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentCertificate = newEntity;
				_alreadyFetchedParentCertificate = fetchResult;
			}
			return _parentCertificate;
		}


		/// <summary> Retrieves the related entity of type 'CryptoQArchiveEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoQArchiveEntity' which is related to this entity.</returns>
		public CryptoQArchiveEntity GetSingleCryptoQArchive()
		{
			return GetSingleCryptoQArchive(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoQArchiveEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoQArchiveEntity' which is related to this entity.</returns>
		public virtual CryptoQArchiveEntity GetSingleCryptoQArchive(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoQArchive || forceFetch || _alwaysFetchCryptoQArchive) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoQArchiveEntityUsingQArchiveID);
				CryptoQArchiveEntity newEntity = new CryptoQArchiveEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.QArchiveID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoQArchiveEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoQArchiveReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoQArchive = newEntity;
				_alreadyFetchedCryptoQArchive = fetchResult;
			}
			return _cryptoQArchive;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentCertificate", _parentCertificate);
			toReturn.Add("CryptoQArchive", _cryptoQArchive);
			toReturn.Add("ChildCertificates", _childCertificates);
			toReturn.Add("CryptoOperatorActivationkeys", _cryptoOperatorActivationkeys);
			toReturn.Add("CryptoSamSignCertificates", _cryptoSamSignCertificates);
			toReturn.Add("CryptoUicCertificates", _cryptoUicCertificates);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="validator">The validator object for this CryptoCertificateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 certificateID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(certificateID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_childCertificates = new VarioSL.Entities.CollectionClasses.CryptoCertificateCollection();
			_childCertificates.SetContainingEntityInfo(this, "ParentCertificate");

			_cryptoOperatorActivationkeys = new VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection();
			_cryptoOperatorActivationkeys.SetContainingEntityInfo(this, "CryptoCertificate");

			_cryptoSamSignCertificates = new VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection();
			_cryptoSamSignCertificates.SetContainingEntityInfo(this, "CryptoCertificate");

			_cryptoUicCertificates = new VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection();
			_cryptoUicCertificates.SetContainingEntityInfo(this, "CryptoCertificate");
			_parentCertificateReturnsNewIfNotFound = false;
			_cryptoQArchiveReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Format", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssuerName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Purpose", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Signature", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubjectName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentCertificate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentCertificate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentCertificate, new PropertyChangedEventHandler( OnParentCertificatePropertyChanged ), "ParentCertificate", VarioSL.Entities.RelationClasses.StaticCryptoCertificateRelations.CryptoCertificateEntityUsingCertificateIDParentIDStatic, true, signalRelatedEntity, "ChildCertificates", resetFKFields, new int[] { (int)CryptoCertificateFieldIndex.ParentID } );		
			_parentCertificate = null;
		}
		
		/// <summary> setups the sync logic for member _parentCertificate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentCertificate(IEntityCore relatedEntity)
		{
			if(_parentCertificate!=relatedEntity)
			{		
				DesetupSyncParentCertificate(true, true);
				_parentCertificate = (CryptoCertificateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentCertificate, new PropertyChangedEventHandler( OnParentCertificatePropertyChanged ), "ParentCertificate", VarioSL.Entities.RelationClasses.StaticCryptoCertificateRelations.CryptoCertificateEntityUsingCertificateIDParentIDStatic, true, ref _alreadyFetchedParentCertificate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentCertificatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cryptoQArchive</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoQArchive(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoQArchive, new PropertyChangedEventHandler( OnCryptoQArchivePropertyChanged ), "CryptoQArchive", VarioSL.Entities.RelationClasses.StaticCryptoCertificateRelations.CryptoQArchiveEntityUsingQArchiveIDStatic, true, signalRelatedEntity, "CryptoCertificates", resetFKFields, new int[] { (int)CryptoCertificateFieldIndex.QArchiveID } );		
			_cryptoQArchive = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoQArchive</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoQArchive(IEntityCore relatedEntity)
		{
			if(_cryptoQArchive!=relatedEntity)
			{		
				DesetupSyncCryptoQArchive(true, true);
				_cryptoQArchive = (CryptoQArchiveEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoQArchive, new PropertyChangedEventHandler( OnCryptoQArchivePropertyChanged ), "CryptoQArchive", VarioSL.Entities.RelationClasses.StaticCryptoCertificateRelations.CryptoQArchiveEntityUsingQArchiveIDStatic, true, ref _alreadyFetchedCryptoQArchive, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoQArchivePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="certificateID">PK value for CryptoCertificate which data should be fetched into this CryptoCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 certificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CryptoCertificateFieldIndex.CertificateID].ForcedCurrentValueWrite(certificateID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCryptoCertificateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CryptoCertificateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CryptoCertificateRelations Relations
		{
			get	{ return new CryptoCertificateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoCertificate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildCertificates
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoCertificateCollection(), (IEntityRelation)GetRelationsForField("ChildCertificates")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, 0, null, null, null, "ChildCertificates", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoOperatorActivationkey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoOperatorActivationkeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection(), (IEntityRelation)GetRelationsForField("CryptoOperatorActivationkeys")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoOperatorActivationkeyEntity, 0, null, null, null, "CryptoOperatorActivationkeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoSamSignCertificate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoSamSignCertificates
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection(), (IEntityRelation)GetRelationsForField("CryptoSamSignCertificates")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoSamSignCertificateEntity, 0, null, null, null, "CryptoSamSignCertificates", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoUicCertificate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoUicCertificates
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection(), (IEntityRelation)GetRelationsForField("CryptoUicCertificates")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoUicCertificateEntity, 0, null, null, null, "CryptoUicCertificates", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoCertificate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentCertificate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoCertificateCollection(), (IEntityRelation)GetRelationsForField("ParentCertificate")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, 0, null, null, null, "ParentCertificate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoQArchive'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoQArchive
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoQArchiveCollection(), (IEntityRelation)GetRelationsForField("CryptoQArchive")[0], (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoQArchiveEntity, 0, null, null, null, "CryptoQArchive", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CertificateID property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."CERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CertificateID
		{
			get { return (System.Int64)GetValue((int)CryptoCertificateFieldIndex.CertificateID, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.CertificateID, value, true); }
		}

		/// <summary> The Description property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.Description, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.Description, value, true); }
		}

		/// <summary> The FileName property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."FILENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String FileName
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.FileName, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.FileName, value, true); }
		}

		/// <summary> The Format property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."FORMAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CertificateFormat Format
		{
			get { return (VarioSL.Entities.Enumerations.CertificateFormat)GetValue((int)CryptoCertificateFieldIndex.Format, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.Format, value, true); }
		}

		/// <summary> The IssuerName property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."ISSUERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String IssuerName
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.IssuerName, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.IssuerName, value, true); }
		}

		/// <summary> The LastModified property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CryptoCertificateFieldIndex.LastModified, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.LastUser, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.Name, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.Name, value, true); }
		}

		/// <summary> The ParentID property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."PARENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParentID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CryptoCertificateFieldIndex.ParentID, false); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.ParentID, value, true); }
		}

		/// <summary> The Purpose property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."PURPOSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CertificatePurpose Purpose
		{
			get { return (VarioSL.Entities.Enumerations.CertificatePurpose)GetValue((int)CryptoCertificateFieldIndex.Purpose, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.Purpose, value, true); }
		}

		/// <summary> The QArchiveID property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."QARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 QArchiveID
		{
			get { return (System.Int64)GetValue((int)CryptoCertificateFieldIndex.QArchiveID, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.QArchiveID, value, true); }
		}

		/// <summary> The RawData property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."RAWDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 4000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RawData
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.RawData, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.RawData, value, true); }
		}

		/// <summary> The Signature property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."SIGNATURE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Signature
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.Signature, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.Signature, value, true); }
		}

		/// <summary> The SubjectName property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."SUBJECTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SubjectName
		{
			get { return (System.String)GetValue((int)CryptoCertificateFieldIndex.SubjectName, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.SubjectName, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CryptoCertificateFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)CryptoCertificateFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity CryptoCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CERTIFICATE"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)CryptoCertificateFieldIndex.ValidTo, true); }
			set	{ SetValue((int)CryptoCertificateFieldIndex.ValidTo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CryptoCertificateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildCertificates()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoCertificateCollection ChildCertificates
		{
			get	{ return GetMultiChildCertificates(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildCertificates. When set to true, ChildCertificates is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildCertificates is accessed. You can always execute/ a forced fetch by calling GetMultiChildCertificates(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildCertificates
		{
			get	{ return _alwaysFetchChildCertificates; }
			set	{ _alwaysFetchChildCertificates = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildCertificates already has been fetched. Setting this property to false when ChildCertificates has been fetched
		/// will clear the ChildCertificates collection well. Setting this property to true while ChildCertificates hasn't been fetched disables lazy loading for ChildCertificates</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildCertificates
		{
			get { return _alreadyFetchedChildCertificates;}
			set 
			{
				if(_alreadyFetchedChildCertificates && !value && (_childCertificates != null))
				{
					_childCertificates.Clear();
				}
				_alreadyFetchedChildCertificates = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoOperatorActivationkeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoOperatorActivationkeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoOperatorActivationkeyCollection CryptoOperatorActivationkeys
		{
			get	{ return GetMultiCryptoOperatorActivationkeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoOperatorActivationkeys. When set to true, CryptoOperatorActivationkeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoOperatorActivationkeys is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoOperatorActivationkeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoOperatorActivationkeys
		{
			get	{ return _alwaysFetchCryptoOperatorActivationkeys; }
			set	{ _alwaysFetchCryptoOperatorActivationkeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoOperatorActivationkeys already has been fetched. Setting this property to false when CryptoOperatorActivationkeys has been fetched
		/// will clear the CryptoOperatorActivationkeys collection well. Setting this property to true while CryptoOperatorActivationkeys hasn't been fetched disables lazy loading for CryptoOperatorActivationkeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoOperatorActivationkeys
		{
			get { return _alreadyFetchedCryptoOperatorActivationkeys;}
			set 
			{
				if(_alreadyFetchedCryptoOperatorActivationkeys && !value && (_cryptoOperatorActivationkeys != null))
				{
					_cryptoOperatorActivationkeys.Clear();
				}
				_alreadyFetchedCryptoOperatorActivationkeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoSamSignCertificateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoSamSignCertificates()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoSamSignCertificateCollection CryptoSamSignCertificates
		{
			get	{ return GetMultiCryptoSamSignCertificates(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoSamSignCertificates. When set to true, CryptoSamSignCertificates is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoSamSignCertificates is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoSamSignCertificates(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoSamSignCertificates
		{
			get	{ return _alwaysFetchCryptoSamSignCertificates; }
			set	{ _alwaysFetchCryptoSamSignCertificates = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoSamSignCertificates already has been fetched. Setting this property to false when CryptoSamSignCertificates has been fetched
		/// will clear the CryptoSamSignCertificates collection well. Setting this property to true while CryptoSamSignCertificates hasn't been fetched disables lazy loading for CryptoSamSignCertificates</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoSamSignCertificates
		{
			get { return _alreadyFetchedCryptoSamSignCertificates;}
			set 
			{
				if(_alreadyFetchedCryptoSamSignCertificates && !value && (_cryptoSamSignCertificates != null))
				{
					_cryptoSamSignCertificates.Clear();
				}
				_alreadyFetchedCryptoSamSignCertificates = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CryptoUicCertificateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCryptoUicCertificates()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CryptoUicCertificateCollection CryptoUicCertificates
		{
			get	{ return GetMultiCryptoUicCertificates(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoUicCertificates. When set to true, CryptoUicCertificates is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoUicCertificates is accessed. You can always execute/ a forced fetch by calling GetMultiCryptoUicCertificates(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoUicCertificates
		{
			get	{ return _alwaysFetchCryptoUicCertificates; }
			set	{ _alwaysFetchCryptoUicCertificates = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoUicCertificates already has been fetched. Setting this property to false when CryptoUicCertificates has been fetched
		/// will clear the CryptoUicCertificates collection well. Setting this property to true while CryptoUicCertificates hasn't been fetched disables lazy loading for CryptoUicCertificates</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoUicCertificates
		{
			get { return _alreadyFetchedCryptoUicCertificates;}
			set 
			{
				if(_alreadyFetchedCryptoUicCertificates && !value && (_cryptoUicCertificates != null))
				{
					_cryptoUicCertificates.Clear();
				}
				_alreadyFetchedCryptoUicCertificates = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CryptoCertificateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentCertificate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoCertificateEntity ParentCertificate
		{
			get	{ return GetSingleParentCertificate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentCertificate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildCertificates", "ParentCertificate", _parentCertificate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentCertificate. When set to true, ParentCertificate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentCertificate is accessed. You can always execute a forced fetch by calling GetSingleParentCertificate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentCertificate
		{
			get	{ return _alwaysFetchParentCertificate; }
			set	{ _alwaysFetchParentCertificate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentCertificate already has been fetched. Setting this property to false when ParentCertificate has been fetched
		/// will set ParentCertificate to null as well. Setting this property to true while ParentCertificate hasn't been fetched disables lazy loading for ParentCertificate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentCertificate
		{
			get { return _alreadyFetchedParentCertificate;}
			set 
			{
				if(_alreadyFetchedParentCertificate && !value)
				{
					this.ParentCertificate = null;
				}
				_alreadyFetchedParentCertificate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentCertificate is not found
		/// in the database. When set to true, ParentCertificate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParentCertificateReturnsNewIfNotFound
		{
			get	{ return _parentCertificateReturnsNewIfNotFound; }
			set { _parentCertificateReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CryptoQArchiveEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoQArchive()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoQArchiveEntity CryptoQArchive
		{
			get	{ return GetSingleCryptoQArchive(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoQArchive(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoCertificates", "CryptoQArchive", _cryptoQArchive, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoQArchive. When set to true, CryptoQArchive is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoQArchive is accessed. You can always execute a forced fetch by calling GetSingleCryptoQArchive(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoQArchive
		{
			get	{ return _alwaysFetchCryptoQArchive; }
			set	{ _alwaysFetchCryptoQArchive = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoQArchive already has been fetched. Setting this property to false when CryptoQArchive has been fetched
		/// will set CryptoQArchive to null as well. Setting this property to true while CryptoQArchive hasn't been fetched disables lazy loading for CryptoQArchive</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoQArchive
		{
			get { return _alreadyFetchedCryptoQArchive;}
			set 
			{
				if(_alreadyFetchedCryptoQArchive && !value)
				{
					this.CryptoQArchive = null;
				}
				_alreadyFetchedCryptoQArchive = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoQArchive is not found
		/// in the database. When set to true, CryptoQArchive will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoQArchiveReturnsNewIfNotFound
		{
			get	{ return _cryptoQArchiveReturnsNewIfNotFound; }
			set { _cryptoQArchiveReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CryptoCertificateEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
