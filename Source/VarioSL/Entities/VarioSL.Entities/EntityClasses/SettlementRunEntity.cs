﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementRun'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementRunEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementRunValueCollection	_settlementRunValues;
		private bool	_alwaysFetchSettlementRunValues, _alreadyFetchedSettlementRunValues;
		private SettlementSetupEntity _settlementSetup;
		private bool	_alwaysFetchSettlementSetup, _alreadyFetchedSettlementSetup, _settlementSetupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SettlementSetup</summary>
			public static readonly string SettlementSetup = "SettlementSetup";
			/// <summary>Member name SettlementRunValues</summary>
			public static readonly string SettlementRunValues = "SettlementRunValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementRunEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementRunEntity() :base("SettlementRunEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		public SettlementRunEntity(System.Int64 settlementRunID):base("SettlementRunEntity")
		{
			InitClassFetch(settlementRunID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementRunEntity(System.Int64 settlementRunID, IPrefetchPath prefetchPathToUse):base("SettlementRunEntity")
		{
			InitClassFetch(settlementRunID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="validator">The custom validator object for this SettlementRunEntity</param>
		public SettlementRunEntity(System.Int64 settlementRunID, IValidator validator):base("SettlementRunEntity")
		{
			InitClassFetch(settlementRunID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementRunEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_settlementRunValues = (VarioSL.Entities.CollectionClasses.SettlementRunValueCollection)info.GetValue("_settlementRunValues", typeof(VarioSL.Entities.CollectionClasses.SettlementRunValueCollection));
			_alwaysFetchSettlementRunValues = info.GetBoolean("_alwaysFetchSettlementRunValues");
			_alreadyFetchedSettlementRunValues = info.GetBoolean("_alreadyFetchedSettlementRunValues");
			_settlementSetup = (SettlementSetupEntity)info.GetValue("_settlementSetup", typeof(SettlementSetupEntity));
			if(_settlementSetup!=null)
			{
				_settlementSetup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_settlementSetupReturnsNewIfNotFound = info.GetBoolean("_settlementSetupReturnsNewIfNotFound");
			_alwaysFetchSettlementSetup = info.GetBoolean("_alwaysFetchSettlementSetup");
			_alreadyFetchedSettlementSetup = info.GetBoolean("_alreadyFetchedSettlementSetup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettlementRunFieldIndex)fieldIndex)
			{
				case SettlementRunFieldIndex.SettlementSetupID:
					DesetupSyncSettlementSetup(true, false);
					_alreadyFetchedSettlementSetup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSettlementRunValues = (_settlementRunValues.Count > 0);
			_alreadyFetchedSettlementSetup = (_settlementSetup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SettlementSetup":
					toReturn.Add(Relations.SettlementSetupEntityUsingSettlementSetupID);
					break;
				case "SettlementRunValues":
					toReturn.Add(Relations.SettlementRunValueEntityUsingSettlementRunID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_settlementRunValues", (!this.MarkedForDeletion?_settlementRunValues:null));
			info.AddValue("_alwaysFetchSettlementRunValues", _alwaysFetchSettlementRunValues);
			info.AddValue("_alreadyFetchedSettlementRunValues", _alreadyFetchedSettlementRunValues);
			info.AddValue("_settlementSetup", (!this.MarkedForDeletion?_settlementSetup:null));
			info.AddValue("_settlementSetupReturnsNewIfNotFound", _settlementSetupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSettlementSetup", _alwaysFetchSettlementSetup);
			info.AddValue("_alreadyFetchedSettlementSetup", _alreadyFetchedSettlementSetup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SettlementSetup":
					_alreadyFetchedSettlementSetup = true;
					this.SettlementSetup = (SettlementSetupEntity)entity;
					break;
				case "SettlementRunValues":
					_alreadyFetchedSettlementRunValues = true;
					if(entity!=null)
					{
						this.SettlementRunValues.Add((SettlementRunValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SettlementSetup":
					SetupSyncSettlementSetup(relatedEntity);
					break;
				case "SettlementRunValues":
					_settlementRunValues.Add((SettlementRunValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SettlementSetup":
					DesetupSyncSettlementSetup(false, true);
					break;
				case "SettlementRunValues":
					this.PerformRelatedEntityRemoval(_settlementRunValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_settlementSetup!=null)
			{
				toReturn.Add(_settlementSetup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_settlementRunValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunID)
		{
			return FetchUsingPK(settlementRunID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementRunID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementRunID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementRunID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementRunID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementRunID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementRunRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch)
		{
			return GetMultiSettlementRunValues(forceFetch, _settlementRunValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementRunValues(forceFetch, _settlementRunValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementRunValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementRunValues || forceFetch || _alwaysFetchSettlementRunValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementRunValues);
				_settlementRunValues.SuppressClearInGetMulti=!forceFetch;
				_settlementRunValues.EntityFactoryToUse = entityFactoryToUse;
				_settlementRunValues.GetMultiManyToOne(this, filter);
				_settlementRunValues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementRunValues = true;
			}
			return _settlementRunValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementRunValues'. These settings will be taken into account
		/// when the property SettlementRunValues is requested or GetMultiSettlementRunValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementRunValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementRunValues.SortClauses=sortClauses;
			_settlementRunValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SettlementSetupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementSetupEntity' which is related to this entity.</returns>
		public SettlementSetupEntity GetSingleSettlementSetup()
		{
			return GetSingleSettlementSetup(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementSetupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementSetupEntity' which is related to this entity.</returns>
		public virtual SettlementSetupEntity GetSingleSettlementSetup(bool forceFetch)
		{
			if( ( !_alreadyFetchedSettlementSetup || forceFetch || _alwaysFetchSettlementSetup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementSetupEntityUsingSettlementSetupID);
				SettlementSetupEntity newEntity = new SettlementSetupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SettlementSetupID);
				}
				if(fetchResult)
				{
					newEntity = (SettlementSetupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_settlementSetupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SettlementSetup = newEntity;
				_alreadyFetchedSettlementSetup = fetchResult;
			}
			return _settlementSetup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SettlementSetup", _settlementSetup);
			toReturn.Add("SettlementRunValues", _settlementRunValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="validator">The validator object for this SettlementRunEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementRunID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementRunID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_settlementRunValues = new VarioSL.Entities.CollectionClasses.SettlementRunValueCollection();
			_settlementRunValues.SetContainingEntityInfo(this, "SettlementRun");
			_settlementSetupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BeginDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementRunID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementSetupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _settlementSetup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSettlementSetup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _settlementSetup, new PropertyChangedEventHandler( OnSettlementSetupPropertyChanged ), "SettlementSetup", VarioSL.Entities.RelationClasses.StaticSettlementRunRelations.SettlementSetupEntityUsingSettlementSetupIDStatic, true, signalRelatedEntity, "SettlementRuns", resetFKFields, new int[] { (int)SettlementRunFieldIndex.SettlementSetupID } );		
			_settlementSetup = null;
		}
		
		/// <summary> setups the sync logic for member _settlementSetup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSettlementSetup(IEntityCore relatedEntity)
		{
			if(_settlementSetup!=relatedEntity)
			{		
				DesetupSyncSettlementSetup(true, true);
				_settlementSetup = (SettlementSetupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _settlementSetup, new PropertyChangedEventHandler( OnSettlementSetupPropertyChanged ), "SettlementSetup", VarioSL.Entities.RelationClasses.StaticSettlementRunRelations.SettlementSetupEntityUsingSettlementSetupIDStatic, true, ref _alreadyFetchedSettlementSetup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSettlementSetupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementRunID">PK value for SettlementRun which data should be fetched into this SettlementRun object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementRunID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementRunFieldIndex.SettlementRunID].ForcedCurrentValueWrite(settlementRunID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementRunDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementRunEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementRunRelations Relations
		{
			get	{ return new SettlementRunRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRunValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRunValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunValueCollection(), (IEntityRelation)GetRelationsForField("SettlementRunValues")[0], (int)VarioSL.Entities.EntityType.SettlementRunEntity, (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, 0, null, null, null, "SettlementRunValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementSetup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementSetup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementSetupCollection(), (IEntityRelation)GetRelationsForField("SettlementSetup")[0], (int)VarioSL.Entities.EntityType.SettlementRunEntity, (int)VarioSL.Entities.EntityType.SettlementSetupEntity, 0, null, null, null, "SettlementSetup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BeginDate property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."BEGINDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime BeginDate
		{
			get { return (System.DateTime)GetValue((int)SettlementRunFieldIndex.BeginDate, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.BeginDate, value, true); }
		}

		/// <summary> The EndDate property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."ENDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EndDate
		{
			get { return (System.DateTime)GetValue((int)SettlementRunFieldIndex.EndDate, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.EndDate, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementRunFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementRunFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SettlementRunID property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."SETTLEMENTRUNID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementRunID
		{
			get { return (System.Int64)GetValue((int)SettlementRunFieldIndex.SettlementRunID, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.SettlementRunID, value, true); }
		}

		/// <summary> The SettlementSetupID property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."SETTLEMENTSETUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SettlementSetupID
		{
			get { return (System.Int64)GetValue((int)SettlementRunFieldIndex.SettlementSetupID, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.SettlementSetupID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementRun<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTRUN"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementRunFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementRunFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementRunValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueCollection SettlementRunValues
		{
			get	{ return GetMultiSettlementRunValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRunValues. When set to true, SettlementRunValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRunValues is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementRunValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRunValues
		{
			get	{ return _alwaysFetchSettlementRunValues; }
			set	{ _alwaysFetchSettlementRunValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRunValues already has been fetched. Setting this property to false when SettlementRunValues has been fetched
		/// will clear the SettlementRunValues collection well. Setting this property to true while SettlementRunValues hasn't been fetched disables lazy loading for SettlementRunValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRunValues
		{
			get { return _alreadyFetchedSettlementRunValues;}
			set 
			{
				if(_alreadyFetchedSettlementRunValues && !value && (_settlementRunValues != null))
				{
					_settlementRunValues.Clear();
				}
				_alreadyFetchedSettlementRunValues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SettlementSetupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSettlementSetup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementSetupEntity SettlementSetup
		{
			get	{ return GetSingleSettlementSetup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSettlementSetup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementRuns", "SettlementSetup", _settlementSetup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementSetup. When set to true, SettlementSetup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementSetup is accessed. You can always execute a forced fetch by calling GetSingleSettlementSetup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementSetup
		{
			get	{ return _alwaysFetchSettlementSetup; }
			set	{ _alwaysFetchSettlementSetup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementSetup already has been fetched. Setting this property to false when SettlementSetup has been fetched
		/// will set SettlementSetup to null as well. Setting this property to true while SettlementSetup hasn't been fetched disables lazy loading for SettlementSetup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementSetup
		{
			get { return _alreadyFetchedSettlementSetup;}
			set 
			{
				if(_alreadyFetchedSettlementSetup && !value)
				{
					this.SettlementSetup = null;
				}
				_alreadyFetchedSettlementSetup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SettlementSetup is not found
		/// in the database. When set to true, SettlementSetup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SettlementSetupReturnsNewIfNotFound
		{
			get	{ return _settlementSetupReturnsNewIfNotFound; }
			set { _settlementSetupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementRunEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
