﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Form'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FormEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardTicketCollection	_cardTicket;
		private bool	_alwaysFetchCardTicket, _alreadyFetchedCardTicket;
		private VarioSL.Entities.CollectionClasses.LayoutCollection	_layouts;
		private bool	_alwaysFetchLayouts, _alreadyFetchedLayouts;
		private VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection	_dunningLevels;
		private bool	_alwaysFetchDunningLevels, _alreadyFetchedDunningLevels;
		private VarioSL.Entities.CollectionClasses.EmailMessageCollection	_emailMessages;
		private bool	_alwaysFetchEmailMessages, _alreadyFetchedEmailMessages;
		private VarioSL.Entities.CollectionClasses.NotificationMessageCollection	_notificationMessages;
		private bool	_alwaysFetchNotificationMessages, _alreadyFetchedNotificationMessages;
		private VarioSL.Entities.CollectionClasses.EmailEventCollection _emailEventCollectionViaEmailMessage;
		private bool	_alwaysFetchEmailEventCollectionViaEmailMessage, _alreadyFetchedEmailEventCollectionViaEmailMessage;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name CardTicket</summary>
			public static readonly string CardTicket = "CardTicket";
			/// <summary>Member name Layouts</summary>
			public static readonly string Layouts = "Layouts";
			/// <summary>Member name DunningLevels</summary>
			public static readonly string DunningLevels = "DunningLevels";
			/// <summary>Member name EmailMessages</summary>
			public static readonly string EmailMessages = "EmailMessages";
			/// <summary>Member name NotificationMessages</summary>
			public static readonly string NotificationMessages = "NotificationMessages";
			/// <summary>Member name EmailEventCollectionViaEmailMessage</summary>
			public static readonly string EmailEventCollectionViaEmailMessage = "EmailEventCollectionViaEmailMessage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FormEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FormEntity() :base("FormEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		public FormEntity(System.Int64 formID):base("FormEntity")
		{
			InitClassFetch(formID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FormEntity(System.Int64 formID, IPrefetchPath prefetchPathToUse):base("FormEntity")
		{
			InitClassFetch(formID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="validator">The custom validator object for this FormEntity</param>
		public FormEntity(System.Int64 formID, IValidator validator):base("FormEntity")
		{
			InitClassFetch(formID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FormEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardTicket = (VarioSL.Entities.CollectionClasses.CardTicketCollection)info.GetValue("_cardTicket", typeof(VarioSL.Entities.CollectionClasses.CardTicketCollection));
			_alwaysFetchCardTicket = info.GetBoolean("_alwaysFetchCardTicket");
			_alreadyFetchedCardTicket = info.GetBoolean("_alreadyFetchedCardTicket");

			_layouts = (VarioSL.Entities.CollectionClasses.LayoutCollection)info.GetValue("_layouts", typeof(VarioSL.Entities.CollectionClasses.LayoutCollection));
			_alwaysFetchLayouts = info.GetBoolean("_alwaysFetchLayouts");
			_alreadyFetchedLayouts = info.GetBoolean("_alreadyFetchedLayouts");

			_dunningLevels = (VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection)info.GetValue("_dunningLevels", typeof(VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection));
			_alwaysFetchDunningLevels = info.GetBoolean("_alwaysFetchDunningLevels");
			_alreadyFetchedDunningLevels = info.GetBoolean("_alreadyFetchedDunningLevels");

			_emailMessages = (VarioSL.Entities.CollectionClasses.EmailMessageCollection)info.GetValue("_emailMessages", typeof(VarioSL.Entities.CollectionClasses.EmailMessageCollection));
			_alwaysFetchEmailMessages = info.GetBoolean("_alwaysFetchEmailMessages");
			_alreadyFetchedEmailMessages = info.GetBoolean("_alreadyFetchedEmailMessages");

			_notificationMessages = (VarioSL.Entities.CollectionClasses.NotificationMessageCollection)info.GetValue("_notificationMessages", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageCollection));
			_alwaysFetchNotificationMessages = info.GetBoolean("_alwaysFetchNotificationMessages");
			_alreadyFetchedNotificationMessages = info.GetBoolean("_alreadyFetchedNotificationMessages");
			_emailEventCollectionViaEmailMessage = (VarioSL.Entities.CollectionClasses.EmailEventCollection)info.GetValue("_emailEventCollectionViaEmailMessage", typeof(VarioSL.Entities.CollectionClasses.EmailEventCollection));
			_alwaysFetchEmailEventCollectionViaEmailMessage = info.GetBoolean("_alwaysFetchEmailEventCollectionViaEmailMessage");
			_alreadyFetchedEmailEventCollectionViaEmailMessage = info.GetBoolean("_alreadyFetchedEmailEventCollectionViaEmailMessage");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FormFieldIndex)fieldIndex)
			{
				case FormFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardTicket = (_cardTicket.Count > 0);
			_alreadyFetchedLayouts = (_layouts.Count > 0);
			_alreadyFetchedDunningLevels = (_dunningLevels.Count > 0);
			_alreadyFetchedEmailMessages = (_emailMessages.Count > 0);
			_alreadyFetchedNotificationMessages = (_notificationMessages.Count > 0);
			_alreadyFetchedEmailEventCollectionViaEmailMessage = (_emailEventCollectionViaEmailMessage.Count > 0);
			_alreadyFetchedClient = (_client != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "CardTicket":
					toReturn.Add(Relations.CardTicketEntityUsingFormID);
					break;
				case "Layouts":
					toReturn.Add(Relations.LayoutEntityUsingFormid);
					break;
				case "DunningLevels":
					toReturn.Add(Relations.DunningLevelConfigurationEntityUsingFormID);
					break;
				case "EmailMessages":
					toReturn.Add(Relations.EmailMessageEntityUsingFormID);
					break;
				case "NotificationMessages":
					toReturn.Add(Relations.NotificationMessageEntityUsingFormID);
					break;
				case "EmailEventCollectionViaEmailMessage":
					toReturn.Add(Relations.EmailMessageEntityUsingFormID, "FormEntity__", "EmailMessage_", JoinHint.None);
					toReturn.Add(EmailMessageEntity.Relations.EmailEventEntityUsingEmailEventID, "EmailMessage_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardTicket", (!this.MarkedForDeletion?_cardTicket:null));
			info.AddValue("_alwaysFetchCardTicket", _alwaysFetchCardTicket);
			info.AddValue("_alreadyFetchedCardTicket", _alreadyFetchedCardTicket);
			info.AddValue("_layouts", (!this.MarkedForDeletion?_layouts:null));
			info.AddValue("_alwaysFetchLayouts", _alwaysFetchLayouts);
			info.AddValue("_alreadyFetchedLayouts", _alreadyFetchedLayouts);
			info.AddValue("_dunningLevels", (!this.MarkedForDeletion?_dunningLevels:null));
			info.AddValue("_alwaysFetchDunningLevels", _alwaysFetchDunningLevels);
			info.AddValue("_alreadyFetchedDunningLevels", _alreadyFetchedDunningLevels);
			info.AddValue("_emailMessages", (!this.MarkedForDeletion?_emailMessages:null));
			info.AddValue("_alwaysFetchEmailMessages", _alwaysFetchEmailMessages);
			info.AddValue("_alreadyFetchedEmailMessages", _alreadyFetchedEmailMessages);
			info.AddValue("_notificationMessages", (!this.MarkedForDeletion?_notificationMessages:null));
			info.AddValue("_alwaysFetchNotificationMessages", _alwaysFetchNotificationMessages);
			info.AddValue("_alreadyFetchedNotificationMessages", _alreadyFetchedNotificationMessages);
			info.AddValue("_emailEventCollectionViaEmailMessage", (!this.MarkedForDeletion?_emailEventCollectionViaEmailMessage:null));
			info.AddValue("_alwaysFetchEmailEventCollectionViaEmailMessage", _alwaysFetchEmailEventCollectionViaEmailMessage);
			info.AddValue("_alreadyFetchedEmailEventCollectionViaEmailMessage", _alreadyFetchedEmailEventCollectionViaEmailMessage);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "CardTicket":
					_alreadyFetchedCardTicket = true;
					if(entity!=null)
					{
						this.CardTicket.Add((CardTicketEntity)entity);
					}
					break;
				case "Layouts":
					_alreadyFetchedLayouts = true;
					if(entity!=null)
					{
						this.Layouts.Add((LayoutEntity)entity);
					}
					break;
				case "DunningLevels":
					_alreadyFetchedDunningLevels = true;
					if(entity!=null)
					{
						this.DunningLevels.Add((DunningLevelConfigurationEntity)entity);
					}
					break;
				case "EmailMessages":
					_alreadyFetchedEmailMessages = true;
					if(entity!=null)
					{
						this.EmailMessages.Add((EmailMessageEntity)entity);
					}
					break;
				case "NotificationMessages":
					_alreadyFetchedNotificationMessages = true;
					if(entity!=null)
					{
						this.NotificationMessages.Add((NotificationMessageEntity)entity);
					}
					break;
				case "EmailEventCollectionViaEmailMessage":
					_alreadyFetchedEmailEventCollectionViaEmailMessage = true;
					if(entity!=null)
					{
						this.EmailEventCollectionViaEmailMessage.Add((EmailEventEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "CardTicket":
					_cardTicket.Add((CardTicketEntity)relatedEntity);
					break;
				case "Layouts":
					_layouts.Add((LayoutEntity)relatedEntity);
					break;
				case "DunningLevels":
					_dunningLevels.Add((DunningLevelConfigurationEntity)relatedEntity);
					break;
				case "EmailMessages":
					_emailMessages.Add((EmailMessageEntity)relatedEntity);
					break;
				case "NotificationMessages":
					_notificationMessages.Add((NotificationMessageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "CardTicket":
					this.PerformRelatedEntityRemoval(_cardTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Layouts":
					this.PerformRelatedEntityRemoval(_layouts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningLevels":
					this.PerformRelatedEntityRemoval(_dunningLevels, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailMessages":
					this.PerformRelatedEntityRemoval(_emailMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NotificationMessages":
					this.PerformRelatedEntityRemoval(_notificationMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardTicket);
			toReturn.Add(_layouts);
			toReturn.Add(_dunningLevels);
			toReturn.Add(_emailMessages);
			toReturn.Add(_notificationMessages);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name)
		{
			return FetchUsingUCName( name, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCName( name, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCName( name, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((FormDAO)CreateDAOInstance()).FetchFormUsingUCName(this, this.Transaction, name, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 formID)
		{
			return FetchUsingPK(formID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 formID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(formID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(formID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(formID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FormID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FormRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch)
		{
			return GetMultiCardTicket(forceFetch, _cardTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardTicket(forceFetch, _cardTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardTicket || forceFetch || _alwaysFetchCardTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardTicket);
				_cardTicket.SuppressClearInGetMulti=!forceFetch;
				_cardTicket.EntityFactoryToUse = entityFactoryToUse;
				_cardTicket.GetMultiManyToOne(null, null, this, filter);
				_cardTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedCardTicket = true;
			}
			return _cardTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardTicket'. These settings will be taken into account
		/// when the property CardTicket is requested or GetMultiCardTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardTicket.SortClauses=sortClauses;
			_cardTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLayouts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLayouts || forceFetch || _alwaysFetchLayouts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_layouts);
				_layouts.SuppressClearInGetMulti=!forceFetch;
				_layouts.EntityFactoryToUse = entityFactoryToUse;
				_layouts.GetMultiManyToOne(null, null, null, this, filter);
				_layouts.SuppressClearInGetMulti=false;
				_alreadyFetchedLayouts = true;
			}
			return _layouts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Layouts'. These settings will be taken into account
		/// when the property Layouts is requested or GetMultiLayouts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLayouts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_layouts.SortClauses=sortClauses;
			_layouts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelConfigurationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiDunningLevels(bool forceFetch)
		{
			return GetMultiDunningLevels(forceFetch, _dunningLevels.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelConfigurationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiDunningLevels(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningLevels(forceFetch, _dunningLevels.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiDunningLevels(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningLevels(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiDunningLevels(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningLevels || forceFetch || _alwaysFetchDunningLevels) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningLevels);
				_dunningLevels.SuppressClearInGetMulti=!forceFetch;
				_dunningLevels.EntityFactoryToUse = entityFactoryToUse;
				_dunningLevels.GetMultiManyToOne(null, this, filter);
				_dunningLevels.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningLevels = true;
			}
			return _dunningLevels;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningLevels'. These settings will be taken into account
		/// when the property DunningLevels is requested or GetMultiDunningLevels is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningLevels(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningLevels.SortClauses=sortClauses;
			_dunningLevels.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailMessages || forceFetch || _alwaysFetchEmailMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailMessages);
				_emailMessages.SuppressClearInGetMulti=!forceFetch;
				_emailMessages.EntityFactoryToUse = entityFactoryToUse;
				_emailMessages.GetMultiManyToOne(null, this, null, filter);
				_emailMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailMessages = true;
			}
			return _emailMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailMessages'. These settings will be taken into account
		/// when the property EmailMessages is requested or GetMultiEmailMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailMessages.SortClauses=sortClauses;
			_emailMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch)
		{
			return GetMultiNotificationMessages(forceFetch, _notificationMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessages(forceFetch, _notificationMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessages || forceFetch || _alwaysFetchNotificationMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessages);
				_notificationMessages.SuppressClearInGetMulti=!forceFetch;
				_notificationMessages.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessages.GetMultiManyToOne(null, this, filter);
				_notificationMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessages = true;
			}
			return _notificationMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessages'. These settings will be taken into account
		/// when the property NotificationMessages is requested or GetMultiNotificationMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessages.SortClauses=sortClauses;
			_notificationMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventCollection GetMultiEmailEventCollectionViaEmailMessage(bool forceFetch)
		{
			return GetMultiEmailEventCollectionViaEmailMessage(forceFetch, _emailEventCollectionViaEmailMessage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventCollection GetMultiEmailEventCollectionViaEmailMessage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEmailEventCollectionViaEmailMessage || forceFetch || _alwaysFetchEmailEventCollectionViaEmailMessage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventCollectionViaEmailMessage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(FormFields.FormID, ComparisonOperator.Equal, this.FormID, "FormEntity__"));
				_emailEventCollectionViaEmailMessage.SuppressClearInGetMulti=!forceFetch;
				_emailEventCollectionViaEmailMessage.EntityFactoryToUse = entityFactoryToUse;
				_emailEventCollectionViaEmailMessage.GetMulti(filter, GetRelationsForField("EmailEventCollectionViaEmailMessage"));
				_emailEventCollectionViaEmailMessage.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventCollectionViaEmailMessage = true;
			}
			return _emailEventCollectionViaEmailMessage;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventCollectionViaEmailMessage'. These settings will be taken into account
		/// when the property EmailEventCollectionViaEmailMessage is requested or GetMultiEmailEventCollectionViaEmailMessage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventCollectionViaEmailMessage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventCollectionViaEmailMessage.SortClauses=sortClauses;
			_emailEventCollectionViaEmailMessage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("CardTicket", _cardTicket);
			toReturn.Add("Layouts", _layouts);
			toReturn.Add("DunningLevels", _dunningLevels);
			toReturn.Add("EmailMessages", _emailMessages);
			toReturn.Add("NotificationMessages", _notificationMessages);
			toReturn.Add("EmailEventCollectionViaEmailMessage", _emailEventCollectionViaEmailMessage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="validator">The validator object for this FormEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 formID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(formID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardTicket = new VarioSL.Entities.CollectionClasses.CardTicketCollection();
			_cardTicket.SetContainingEntityInfo(this, "Form");

			_layouts = new VarioSL.Entities.CollectionClasses.LayoutCollection();
			_layouts.SetContainingEntityInfo(this, "Form");

			_dunningLevels = new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection();
			_dunningLevels.SetContainingEntityInfo(this, "Form");

			_emailMessages = new VarioSL.Entities.CollectionClasses.EmailMessageCollection();
			_emailMessages.SetContainingEntityInfo(this, "Form");

			_notificationMessages = new VarioSL.Entities.CollectionClasses.NotificationMessageCollection();
			_notificationMessages.SetContainingEntityInfo(this, "Form");
			_emailEventCollectionViaEmailMessage = new VarioSL.Entities.CollectionClasses.EmailEventCollection();
			_clientReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Copies", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DocumentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Layout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingScope", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PreviewImage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchemaName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFormRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Forms", resetFKFields, new int[] { (int)FormFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFormRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="formID">PK value for Form which data should be fetched into this Form object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FormFieldIndex.FormID].ForcedCurrentValueWrite(formID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFormDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FormEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FormRelations Relations
		{
			get	{ return new FormRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketCollection(), (IEntityRelation)GetRelationsForField("CardTicket")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.CardTicketEntity, 0, null, null, null, "CardTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayouts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layouts")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layouts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningLevels
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection(), (IEntityRelation)GetRelationsForField("DunningLevels")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, 0, null, null, null, "DunningLevels", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailMessageCollection(), (IEntityRelation)GetRelationsForField("EmailMessages")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.EmailMessageEntity, 0, null, null, null, "EmailMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageCollection(), (IEntityRelation)GetRelationsForField("NotificationMessages")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.NotificationMessageEntity, 0, null, null, null, "NotificationMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEvent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventCollectionViaEmailMessage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.EmailMessageEntityUsingFormID;
				intermediateRelation.SetAliases(string.Empty, "EmailMessage_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.EmailEventEntity, 0, null, null, GetRelationsForField("EmailEventCollectionViaEmailMessage"), "EmailEventCollectionViaEmailMessage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.FormEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FormFieldIndex.ClientID, false); }
			set	{ SetValue((int)FormFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Copies property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."COPIES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Copies
		{
			get { return (System.Int16)GetValue((int)FormFieldIndex.Copies, true); }
			set	{ SetValue((int)FormFieldIndex.Copies, value, true); }
		}

		/// <summary> The Description property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FormFieldIndex.Description, true); }
			set	{ SetValue((int)FormFieldIndex.Description, value, true); }
		}

		/// <summary> The DocumentType property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."DOCUMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.DocumentType> DocumentType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.DocumentType>)GetValue((int)FormFieldIndex.DocumentType, false); }
			set	{ SetValue((int)FormFieldIndex.DocumentType, value, true); }
		}

		/// <summary> The FormID property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FormID
		{
			get { return (System.Int64)GetValue((int)FormFieldIndex.FormID, true); }
			set	{ SetValue((int)FormFieldIndex.FormID, value, true); }
		}

		/// <summary> The LastModified property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FormFieldIndex.LastModified, true); }
			set	{ SetValue((int)FormFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FormFieldIndex.LastUser, true); }
			set	{ SetValue((int)FormFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Layout property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."LAYOUT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Byte[] Layout
		{
			get { return (System.Byte[])GetValue((int)FormFieldIndex.Layout, true); }
			set	{ SetValue((int)FormFieldIndex.Layout, value, true); }
		}

		/// <summary> The Name property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 70<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FormFieldIndex.Name, true); }
			set	{ SetValue((int)FormFieldIndex.Name, value, true); }
		}

		/// <summary> The PostingScope property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."POSTINGSCOPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.PostingScope> PostingScope
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.PostingScope>)GetValue((int)FormFieldIndex.PostingScope, false); }
			set	{ SetValue((int)FormFieldIndex.PostingScope, value, true); }
		}

		/// <summary> The PreviewImage property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."PREVIEWIMAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Drawing.Bitmap PreviewImage
		{
			get { return (System.Drawing.Bitmap)GetValue((int)FormFieldIndex.PreviewImage, true); }
			set	{ SetValue((int)FormFieldIndex.PreviewImage, value, true); }
		}

		/// <summary> The SchemaName property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."SCHEMANAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SchemaName
		{
			get { return (System.String)GetValue((int)FormFieldIndex.SchemaName, true); }
			set	{ SetValue((int)FormFieldIndex.SchemaName, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Form<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FORM"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FormFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FormFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection CardTicket
		{
			get	{ return GetMultiCardTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardTicket. When set to true, CardTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTicket is accessed. You can always execute/ a forced fetch by calling GetMultiCardTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTicket
		{
			get	{ return _alwaysFetchCardTicket; }
			set	{ _alwaysFetchCardTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardTicket already has been fetched. Setting this property to false when CardTicket has been fetched
		/// will clear the CardTicket collection well. Setting this property to true while CardTicket hasn't been fetched disables lazy loading for CardTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTicket
		{
			get { return _alreadyFetchedCardTicket;}
			set 
			{
				if(_alreadyFetchedCardTicket && !value && (_cardTicket != null))
				{
					_cardTicket.Clear();
				}
				_alreadyFetchedCardTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLayouts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection Layouts
		{
			get	{ return GetMultiLayouts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Layouts. When set to true, Layouts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layouts is accessed. You can always execute/ a forced fetch by calling GetMultiLayouts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayouts
		{
			get	{ return _alwaysFetchLayouts; }
			set	{ _alwaysFetchLayouts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layouts already has been fetched. Setting this property to false when Layouts has been fetched
		/// will clear the Layouts collection well. Setting this property to true while Layouts hasn't been fetched disables lazy loading for Layouts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayouts
		{
			get { return _alreadyFetchedLayouts;}
			set 
			{
				if(_alreadyFetchedLayouts && !value && (_layouts != null))
				{
					_layouts.Clear();
				}
				_alreadyFetchedLayouts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningLevels()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection DunningLevels
		{
			get	{ return GetMultiDunningLevels(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningLevels. When set to true, DunningLevels is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningLevels is accessed. You can always execute/ a forced fetch by calling GetMultiDunningLevels(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningLevels
		{
			get	{ return _alwaysFetchDunningLevels; }
			set	{ _alwaysFetchDunningLevels = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningLevels already has been fetched. Setting this property to false when DunningLevels has been fetched
		/// will clear the DunningLevels collection well. Setting this property to true while DunningLevels hasn't been fetched disables lazy loading for DunningLevels</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningLevels
		{
			get { return _alreadyFetchedDunningLevels;}
			set 
			{
				if(_alreadyFetchedDunningLevels && !value && (_dunningLevels != null))
				{
					_dunningLevels.Clear();
				}
				_alreadyFetchedDunningLevels = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection EmailMessages
		{
			get	{ return GetMultiEmailMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailMessages. When set to true, EmailMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailMessages is accessed. You can always execute/ a forced fetch by calling GetMultiEmailMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailMessages
		{
			get	{ return _alwaysFetchEmailMessages; }
			set	{ _alwaysFetchEmailMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailMessages already has been fetched. Setting this property to false when EmailMessages has been fetched
		/// will clear the EmailMessages collection well. Setting this property to true while EmailMessages hasn't been fetched disables lazy loading for EmailMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailMessages
		{
			get { return _alreadyFetchedEmailMessages;}
			set 
			{
				if(_alreadyFetchedEmailMessages && !value && (_emailMessages != null))
				{
					_emailMessages.Clear();
				}
				_alreadyFetchedEmailMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageCollection NotificationMessages
		{
			get	{ return GetMultiNotificationMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessages. When set to true, NotificationMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessages is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessages
		{
			get	{ return _alwaysFetchNotificationMessages; }
			set	{ _alwaysFetchNotificationMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessages already has been fetched. Setting this property to false when NotificationMessages has been fetched
		/// will clear the NotificationMessages collection well. Setting this property to true while NotificationMessages hasn't been fetched disables lazy loading for NotificationMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessages
		{
			get { return _alreadyFetchedNotificationMessages;}
			set 
			{
				if(_alreadyFetchedNotificationMessages && !value && (_notificationMessages != null))
				{
					_notificationMessages.Clear();
				}
				_alreadyFetchedNotificationMessages = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EmailEventEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventCollectionViaEmailMessage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventCollection EmailEventCollectionViaEmailMessage
		{
			get { return GetMultiEmailEventCollectionViaEmailMessage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventCollectionViaEmailMessage. When set to true, EmailEventCollectionViaEmailMessage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventCollectionViaEmailMessage is accessed. You can always execute a forced fetch by calling GetMultiEmailEventCollectionViaEmailMessage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventCollectionViaEmailMessage
		{
			get	{ return _alwaysFetchEmailEventCollectionViaEmailMessage; }
			set	{ _alwaysFetchEmailEventCollectionViaEmailMessage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventCollectionViaEmailMessage already has been fetched. Setting this property to false when EmailEventCollectionViaEmailMessage has been fetched
		/// will clear the EmailEventCollectionViaEmailMessage collection well. Setting this property to true while EmailEventCollectionViaEmailMessage hasn't been fetched disables lazy loading for EmailEventCollectionViaEmailMessage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventCollectionViaEmailMessage
		{
			get { return _alreadyFetchedEmailEventCollectionViaEmailMessage;}
			set 
			{
				if(_alreadyFetchedEmailEventCollectionViaEmailMessage && !value && (_emailEventCollectionViaEmailMessage != null))
				{
					_emailEventCollectionViaEmailMessage.Clear();
				}
				_alreadyFetchedEmailEventCollectionViaEmailMessage = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Forms", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FormEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
