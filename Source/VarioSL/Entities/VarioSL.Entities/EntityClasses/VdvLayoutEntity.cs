﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvLayout'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvLayoutEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection	_vdvLayoutObjects;
		private bool	_alwaysFetchVdvLayoutObjects, _alreadyFetchedVdvLayoutObjects;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProducts;
		private bool	_alwaysFetchVdvProducts, _alreadyFetchedVdvProducts;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name VdvLayoutObjects</summary>
			public static readonly string VdvLayoutObjects = "VdvLayoutObjects";
			/// <summary>Member name VdvProducts</summary>
			public static readonly string VdvProducts = "VdvProducts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvLayoutEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvLayoutEntity() :base("VdvLayoutEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		public VdvLayoutEntity(System.Int64 layoutID):base("VdvLayoutEntity")
		{
			InitClassFetch(layoutID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvLayoutEntity(System.Int64 layoutID, IPrefetchPath prefetchPathToUse):base("VdvLayoutEntity")
		{
			InitClassFetch(layoutID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="validator">The custom validator object for this VdvLayoutEntity</param>
		public VdvLayoutEntity(System.Int64 layoutID, IValidator validator):base("VdvLayoutEntity")
		{
			InitClassFetch(layoutID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvLayoutEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_vdvLayoutObjects = (VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection)info.GetValue("_vdvLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection));
			_alwaysFetchVdvLayoutObjects = info.GetBoolean("_alwaysFetchVdvLayoutObjects");
			_alreadyFetchedVdvLayoutObjects = info.GetBoolean("_alreadyFetchedVdvLayoutObjects");

			_vdvProducts = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProducts", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProducts = info.GetBoolean("_alwaysFetchVdvProducts");
			_alreadyFetchedVdvProducts = info.GetBoolean("_alreadyFetchedVdvProducts");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VdvLayoutFieldIndex)fieldIndex)
			{
				case VdvLayoutFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedVdvLayoutObjects = (_vdvLayoutObjects.Count > 0);
			_alreadyFetchedVdvProducts = (_vdvProducts.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "VdvLayoutObjects":
					toReturn.Add(Relations.VdvLayoutObjectEntityUsingLayoutID);
					break;
				case "VdvProducts":
					toReturn.Add(Relations.VdvProductEntityUsingVdvLayoutID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_vdvLayoutObjects", (!this.MarkedForDeletion?_vdvLayoutObjects:null));
			info.AddValue("_alwaysFetchVdvLayoutObjects", _alwaysFetchVdvLayoutObjects);
			info.AddValue("_alreadyFetchedVdvLayoutObjects", _alreadyFetchedVdvLayoutObjects);
			info.AddValue("_vdvProducts", (!this.MarkedForDeletion?_vdvProducts:null));
			info.AddValue("_alwaysFetchVdvProducts", _alwaysFetchVdvProducts);
			info.AddValue("_alreadyFetchedVdvProducts", _alreadyFetchedVdvProducts);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "VdvLayoutObjects":
					_alreadyFetchedVdvLayoutObjects = true;
					if(entity!=null)
					{
						this.VdvLayoutObjects.Add((VdvLayoutObjectEntity)entity);
					}
					break;
				case "VdvProducts":
					_alreadyFetchedVdvProducts = true;
					if(entity!=null)
					{
						this.VdvProducts.Add((VdvProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "VdvLayoutObjects":
					_vdvLayoutObjects.Add((VdvLayoutObjectEntity)relatedEntity);
					break;
				case "VdvProducts":
					_vdvProducts.Add((VdvProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "VdvLayoutObjects":
					this.PerformRelatedEntityRemoval(_vdvLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProducts":
					this.PerformRelatedEntityRemoval(_vdvProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_vdvLayoutObjects);
			toReturn.Add(_vdvProducts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID)
		{
			return FetchUsingPK(layoutID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvLayoutRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiVdvLayoutObjects(bool forceFetch)
		{
			return GetMultiVdvLayoutObjects(forceFetch, _vdvLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiVdvLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvLayoutObjects(forceFetch, _vdvLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiVdvLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection GetMultiVdvLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvLayoutObjects || forceFetch || _alwaysFetchVdvLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvLayoutObjects);
				_vdvLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_vdvLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_vdvLayoutObjects.GetMultiManyToOne(this, null, null, filter);
				_vdvLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvLayoutObjects = true;
			}
			return _vdvLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvLayoutObjects'. These settings will be taken into account
		/// when the property VdvLayoutObjects is requested or GetMultiVdvLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvLayoutObjects.SortClauses=sortClauses;
			_vdvLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProducts(bool forceFetch)
		{
			return GetMultiVdvProducts(forceFetch, _vdvProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProducts(forceFetch, _vdvProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProducts || forceFetch || _alwaysFetchVdvProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProducts);
				_vdvProducts.SuppressClearInGetMulti=!forceFetch;
				_vdvProducts.EntityFactoryToUse = entityFactoryToUse;
				_vdvProducts.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_vdvProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProducts = true;
			}
			return _vdvProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProducts'. These settings will be taken into account
		/// when the property VdvProducts is requested or GetMultiVdvProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProducts.SortClauses=sortClauses;
			_vdvProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("VdvLayoutObjects", _vdvLayoutObjects);
			toReturn.Add("VdvProducts", _vdvProducts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="validator">The validator object for this VdvLayoutEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_vdvLayoutObjects = new VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection();
			_vdvLayoutObjects.SetContainingEntityInfo(this, "VdvLayout");

			_vdvProducts = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProducts.SetContainingEntityInfo(this, "VdvLayout");
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticVdvLayoutRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "VdvLayout", resetFKFields, new int[] { (int)VdvLayoutFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticVdvLayoutRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutID">PK value for VdvLayout which data should be fetched into this VdvLayout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvLayoutFieldIndex.LayoutID].ForcedCurrentValueWrite(layoutID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvLayoutDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvLayoutEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvLayoutRelations Relations
		{
			get	{ return new VdvLayoutRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("VdvLayoutObjects")[0], (int)VarioSL.Entities.EntityType.VdvLayoutEntity, (int)VarioSL.Entities.EntityType.VdvLayoutObjectEntity, 0, null, null, null, "VdvLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProducts")[0], (int)VarioSL.Entities.EntityType.VdvLayoutEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.VdvLayoutEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LayoutID property of the Entity VdvLayout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)VdvLayoutFieldIndex.LayoutID, true); }
			set	{ SetValue((int)VdvLayoutFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The Description property of the Entity VdvLayout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)VdvLayoutFieldIndex.Description, true); }
			set	{ SetValue((int)VdvLayoutFieldIndex.Description, value, true); }
		}

		/// <summary> The LayoutName property of the Entity VdvLayout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUT"."LAYOUTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LayoutName
		{
			get { return (System.String)GetValue((int)VdvLayoutFieldIndex.LayoutName, true); }
			set	{ SetValue((int)VdvLayoutFieldIndex.LayoutName, value, true); }
		}

		/// <summary> The TariffID property of the Entity VdvLayout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_LAYOUT"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)VdvLayoutFieldIndex.TariffID, true); }
			set	{ SetValue((int)VdvLayoutFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutObjectCollection VdvLayoutObjects
		{
			get	{ return GetMultiVdvLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvLayoutObjects. When set to true, VdvLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiVdvLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvLayoutObjects
		{
			get	{ return _alwaysFetchVdvLayoutObjects; }
			set	{ _alwaysFetchVdvLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvLayoutObjects already has been fetched. Setting this property to false when VdvLayoutObjects has been fetched
		/// will clear the VdvLayoutObjects collection well. Setting this property to true while VdvLayoutObjects hasn't been fetched disables lazy loading for VdvLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvLayoutObjects
		{
			get { return _alreadyFetchedVdvLayoutObjects;}
			set 
			{
				if(_alreadyFetchedVdvLayoutObjects && !value && (_vdvLayoutObjects != null))
				{
					_vdvLayoutObjects.Clear();
				}
				_alreadyFetchedVdvLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProducts
		{
			get	{ return GetMultiVdvProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProducts. When set to true, VdvProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProducts is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProducts
		{
			get	{ return _alwaysFetchVdvProducts; }
			set	{ _alwaysFetchVdvProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProducts already has been fetched. Setting this property to false when VdvProducts has been fetched
		/// will clear the VdvProducts collection well. Setting this property to true while VdvProducts hasn't been fetched disables lazy loading for VdvProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProducts
		{
			get { return _alreadyFetchedVdvProducts;}
			set 
			{
				if(_alreadyFetchedVdvProducts && !value && (_vdvProducts != null))
				{
					_vdvProducts.Clear();
				}
				_alreadyFetchedVdvProducts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvLayout", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvLayoutEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
