﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareStage'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareStageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AreaListElementCollection	_areaListElement;
		private bool	_alwaysFetchAreaListElement, _alreadyFetchedAreaListElement;
		private VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection	_businessRuleResult;
		private bool	_alwaysFetchBusinessRuleResult, _alreadyFetchedBusinessRuleResult;
		private VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection	_externalPacketEfforts;
		private bool	_alwaysFetchExternalPacketEfforts, _alreadyFetchedExternalPacketEfforts;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_fareMatrixEntryBoarding;
		private bool	_alwaysFetchFareMatrixEntryBoarding, _alreadyFetchedFareMatrixEntryBoarding;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_fareMatrixEntryDestination;
		private bool	_alwaysFetchFareMatrixEntryDestination, _alreadyFetchedFareMatrixEntryDestination;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_fareMatrixEntryVia;
		private bool	_alwaysFetchFareMatrixEntryVia, _alreadyFetchedFareMatrixEntryVia;
		private VarioSL.Entities.CollectionClasses.FareStageCollection	_childFareStages;
		private bool	_alwaysFetchChildFareStages, _alreadyFetchedChildFareStages;
		private VarioSL.Entities.CollectionClasses.FareStageAliasCollection	_fareStageAlias;
		private bool	_alwaysFetchFareStageAlias, _alreadyFetchedFareStageAlias;
		private VarioSL.Entities.CollectionClasses.ParameterFareStageCollection	_parameterFareStage;
		private bool	_alwaysFetchParameterFareStage, _alreadyFetchedParameterFareStage;
		private FareStageEntity _parentFareStage;
		private bool	_alwaysFetchParentFareStage, _alreadyFetchedParentFareStage, _parentFareStageReturnsNewIfNotFound;
		private FareStageHierarchieLevelEntity _fareStageHierarchieLevel;
		private bool	_alwaysFetchFareStageHierarchieLevel, _alreadyFetchedFareStageHierarchieLevel, _fareStageHierarchieLevelReturnsNewIfNotFound;
		private FareStageListEntity _fareStageList;
		private bool	_alwaysFetchFareStageList, _alreadyFetchedFareStageList, _fareStageListReturnsNewIfNotFound;
		private FareStageTypeEntity _fareStageType;
		private bool	_alwaysFetchFareStageType, _alreadyFetchedFareStageType, _fareStageTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParentFareStage</summary>
			public static readonly string ParentFareStage = "ParentFareStage";
			/// <summary>Member name FareStageHierarchieLevel</summary>
			public static readonly string FareStageHierarchieLevel = "FareStageHierarchieLevel";
			/// <summary>Member name FareStageList</summary>
			public static readonly string FareStageList = "FareStageList";
			/// <summary>Member name FareStageType</summary>
			public static readonly string FareStageType = "FareStageType";
			/// <summary>Member name AreaListElement</summary>
			public static readonly string AreaListElement = "AreaListElement";
			/// <summary>Member name BusinessRuleResult</summary>
			public static readonly string BusinessRuleResult = "BusinessRuleResult";
			/// <summary>Member name ExternalPacketEfforts</summary>
			public static readonly string ExternalPacketEfforts = "ExternalPacketEfforts";
			/// <summary>Member name FareMatrixEntryBoarding</summary>
			public static readonly string FareMatrixEntryBoarding = "FareMatrixEntryBoarding";
			/// <summary>Member name FareMatrixEntryDestination</summary>
			public static readonly string FareMatrixEntryDestination = "FareMatrixEntryDestination";
			/// <summary>Member name FareMatrixEntryVia</summary>
			public static readonly string FareMatrixEntryVia = "FareMatrixEntryVia";
			/// <summary>Member name ChildFareStages</summary>
			public static readonly string ChildFareStages = "ChildFareStages";
			/// <summary>Member name FareStageAlias</summary>
			public static readonly string FareStageAlias = "FareStageAlias";
			/// <summary>Member name ParameterFareStage</summary>
			public static readonly string ParameterFareStage = "ParameterFareStage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareStageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareStageEntity() :base("FareStageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		public FareStageEntity(System.Int64 fareStageID):base("FareStageEntity")
		{
			InitClassFetch(fareStageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareStageEntity(System.Int64 fareStageID, IPrefetchPath prefetchPathToUse):base("FareStageEntity")
		{
			InitClassFetch(fareStageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="validator">The custom validator object for this FareStageEntity</param>
		public FareStageEntity(System.Int64 fareStageID, IValidator validator):base("FareStageEntity")
		{
			InitClassFetch(fareStageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareStageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaListElement = (VarioSL.Entities.CollectionClasses.AreaListElementCollection)info.GetValue("_areaListElement", typeof(VarioSL.Entities.CollectionClasses.AreaListElementCollection));
			_alwaysFetchAreaListElement = info.GetBoolean("_alwaysFetchAreaListElement");
			_alreadyFetchedAreaListElement = info.GetBoolean("_alreadyFetchedAreaListElement");

			_businessRuleResult = (VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection)info.GetValue("_businessRuleResult", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection));
			_alwaysFetchBusinessRuleResult = info.GetBoolean("_alwaysFetchBusinessRuleResult");
			_alreadyFetchedBusinessRuleResult = info.GetBoolean("_alreadyFetchedBusinessRuleResult");

			_externalPacketEfforts = (VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection)info.GetValue("_externalPacketEfforts", typeof(VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection));
			_alwaysFetchExternalPacketEfforts = info.GetBoolean("_alwaysFetchExternalPacketEfforts");
			_alreadyFetchedExternalPacketEfforts = info.GetBoolean("_alreadyFetchedExternalPacketEfforts");

			_fareMatrixEntryBoarding = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_fareMatrixEntryBoarding", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchFareMatrixEntryBoarding = info.GetBoolean("_alwaysFetchFareMatrixEntryBoarding");
			_alreadyFetchedFareMatrixEntryBoarding = info.GetBoolean("_alreadyFetchedFareMatrixEntryBoarding");

			_fareMatrixEntryDestination = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_fareMatrixEntryDestination", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchFareMatrixEntryDestination = info.GetBoolean("_alwaysFetchFareMatrixEntryDestination");
			_alreadyFetchedFareMatrixEntryDestination = info.GetBoolean("_alreadyFetchedFareMatrixEntryDestination");

			_fareMatrixEntryVia = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_fareMatrixEntryVia", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchFareMatrixEntryVia = info.GetBoolean("_alwaysFetchFareMatrixEntryVia");
			_alreadyFetchedFareMatrixEntryVia = info.GetBoolean("_alreadyFetchedFareMatrixEntryVia");

			_childFareStages = (VarioSL.Entities.CollectionClasses.FareStageCollection)info.GetValue("_childFareStages", typeof(VarioSL.Entities.CollectionClasses.FareStageCollection));
			_alwaysFetchChildFareStages = info.GetBoolean("_alwaysFetchChildFareStages");
			_alreadyFetchedChildFareStages = info.GetBoolean("_alreadyFetchedChildFareStages");

			_fareStageAlias = (VarioSL.Entities.CollectionClasses.FareStageAliasCollection)info.GetValue("_fareStageAlias", typeof(VarioSL.Entities.CollectionClasses.FareStageAliasCollection));
			_alwaysFetchFareStageAlias = info.GetBoolean("_alwaysFetchFareStageAlias");
			_alreadyFetchedFareStageAlias = info.GetBoolean("_alreadyFetchedFareStageAlias");

			_parameterFareStage = (VarioSL.Entities.CollectionClasses.ParameterFareStageCollection)info.GetValue("_parameterFareStage", typeof(VarioSL.Entities.CollectionClasses.ParameterFareStageCollection));
			_alwaysFetchParameterFareStage = info.GetBoolean("_alwaysFetchParameterFareStage");
			_alreadyFetchedParameterFareStage = info.GetBoolean("_alreadyFetchedParameterFareStage");
			_parentFareStage = (FareStageEntity)info.GetValue("_parentFareStage", typeof(FareStageEntity));
			if(_parentFareStage!=null)
			{
				_parentFareStage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentFareStageReturnsNewIfNotFound = info.GetBoolean("_parentFareStageReturnsNewIfNotFound");
			_alwaysFetchParentFareStage = info.GetBoolean("_alwaysFetchParentFareStage");
			_alreadyFetchedParentFareStage = info.GetBoolean("_alreadyFetchedParentFareStage");

			_fareStageHierarchieLevel = (FareStageHierarchieLevelEntity)info.GetValue("_fareStageHierarchieLevel", typeof(FareStageHierarchieLevelEntity));
			if(_fareStageHierarchieLevel!=null)
			{
				_fareStageHierarchieLevel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageHierarchieLevelReturnsNewIfNotFound = info.GetBoolean("_fareStageHierarchieLevelReturnsNewIfNotFound");
			_alwaysFetchFareStageHierarchieLevel = info.GetBoolean("_alwaysFetchFareStageHierarchieLevel");
			_alreadyFetchedFareStageHierarchieLevel = info.GetBoolean("_alreadyFetchedFareStageHierarchieLevel");

			_fareStageList = (FareStageListEntity)info.GetValue("_fareStageList", typeof(FareStageListEntity));
			if(_fareStageList!=null)
			{
				_fareStageList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageListReturnsNewIfNotFound = info.GetBoolean("_fareStageListReturnsNewIfNotFound");
			_alwaysFetchFareStageList = info.GetBoolean("_alwaysFetchFareStageList");
			_alreadyFetchedFareStageList = info.GetBoolean("_alreadyFetchedFareStageList");

			_fareStageType = (FareStageTypeEntity)info.GetValue("_fareStageType", typeof(FareStageTypeEntity));
			if(_fareStageType!=null)
			{
				_fareStageType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageTypeReturnsNewIfNotFound = info.GetBoolean("_fareStageTypeReturnsNewIfNotFound");
			_alwaysFetchFareStageType = info.GetBoolean("_alwaysFetchFareStageType");
			_alreadyFetchedFareStageType = info.GetBoolean("_alreadyFetchedFareStageType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareStageFieldIndex)fieldIndex)
			{
				case FareStageFieldIndex.FareStageListID:
					DesetupSyncFareStageList(true, false);
					_alreadyFetchedFareStageList = false;
					break;
				case FareStageFieldIndex.FareStageTypeID:
					DesetupSyncFareStageType(true, false);
					_alreadyFetchedFareStageType = false;
					break;
				case FareStageFieldIndex.HierarchieLevel:
					DesetupSyncFareStageHierarchieLevel(true, false);
					_alreadyFetchedFareStageHierarchieLevel = false;
					break;
				case FareStageFieldIndex.ParentID:
					DesetupSyncParentFareStage(true, false);
					_alreadyFetchedParentFareStage = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaListElement = (_areaListElement.Count > 0);
			_alreadyFetchedBusinessRuleResult = (_businessRuleResult.Count > 0);
			_alreadyFetchedExternalPacketEfforts = (_externalPacketEfforts.Count > 0);
			_alreadyFetchedFareMatrixEntryBoarding = (_fareMatrixEntryBoarding.Count > 0);
			_alreadyFetchedFareMatrixEntryDestination = (_fareMatrixEntryDestination.Count > 0);
			_alreadyFetchedFareMatrixEntryVia = (_fareMatrixEntryVia.Count > 0);
			_alreadyFetchedChildFareStages = (_childFareStages.Count > 0);
			_alreadyFetchedFareStageAlias = (_fareStageAlias.Count > 0);
			_alreadyFetchedParameterFareStage = (_parameterFareStage.Count > 0);
			_alreadyFetchedParentFareStage = (_parentFareStage != null);
			_alreadyFetchedFareStageHierarchieLevel = (_fareStageHierarchieLevel != null);
			_alreadyFetchedFareStageList = (_fareStageList != null);
			_alreadyFetchedFareStageType = (_fareStageType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParentFareStage":
					toReturn.Add(Relations.FareStageEntityUsingFareStageIDParentID);
					break;
				case "FareStageHierarchieLevel":
					toReturn.Add(Relations.FareStageHierarchieLevelEntityUsingHierarchieLevel);
					break;
				case "FareStageList":
					toReturn.Add(Relations.FareStageListEntityUsingFareStageListID);
					break;
				case "FareStageType":
					toReturn.Add(Relations.FareStageTypeEntityUsingFareStageTypeID);
					break;
				case "AreaListElement":
					toReturn.Add(Relations.AreaListElementEntityUsingFareStageID);
					break;
				case "BusinessRuleResult":
					toReturn.Add(Relations.BusinessRuleResultEntityUsingDestinationFarestageID);
					break;
				case "ExternalPacketEfforts":
					toReturn.Add(Relations.ExternalPacketEffortEntityUsingFareStageID);
					break;
				case "FareMatrixEntryBoarding":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingBoardingID);
					break;
				case "FareMatrixEntryDestination":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingDestinationID);
					break;
				case "FareMatrixEntryVia":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingViaID);
					break;
				case "ChildFareStages":
					toReturn.Add(Relations.FareStageEntityUsingParentID);
					break;
				case "FareStageAlias":
					toReturn.Add(Relations.FareStageAliasEntityUsingFareStageID);
					break;
				case "ParameterFareStage":
					toReturn.Add(Relations.ParameterFareStageEntityUsingFareStageID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaListElement", (!this.MarkedForDeletion?_areaListElement:null));
			info.AddValue("_alwaysFetchAreaListElement", _alwaysFetchAreaListElement);
			info.AddValue("_alreadyFetchedAreaListElement", _alreadyFetchedAreaListElement);
			info.AddValue("_businessRuleResult", (!this.MarkedForDeletion?_businessRuleResult:null));
			info.AddValue("_alwaysFetchBusinessRuleResult", _alwaysFetchBusinessRuleResult);
			info.AddValue("_alreadyFetchedBusinessRuleResult", _alreadyFetchedBusinessRuleResult);
			info.AddValue("_externalPacketEfforts", (!this.MarkedForDeletion?_externalPacketEfforts:null));
			info.AddValue("_alwaysFetchExternalPacketEfforts", _alwaysFetchExternalPacketEfforts);
			info.AddValue("_alreadyFetchedExternalPacketEfforts", _alreadyFetchedExternalPacketEfforts);
			info.AddValue("_fareMatrixEntryBoarding", (!this.MarkedForDeletion?_fareMatrixEntryBoarding:null));
			info.AddValue("_alwaysFetchFareMatrixEntryBoarding", _alwaysFetchFareMatrixEntryBoarding);
			info.AddValue("_alreadyFetchedFareMatrixEntryBoarding", _alreadyFetchedFareMatrixEntryBoarding);
			info.AddValue("_fareMatrixEntryDestination", (!this.MarkedForDeletion?_fareMatrixEntryDestination:null));
			info.AddValue("_alwaysFetchFareMatrixEntryDestination", _alwaysFetchFareMatrixEntryDestination);
			info.AddValue("_alreadyFetchedFareMatrixEntryDestination", _alreadyFetchedFareMatrixEntryDestination);
			info.AddValue("_fareMatrixEntryVia", (!this.MarkedForDeletion?_fareMatrixEntryVia:null));
			info.AddValue("_alwaysFetchFareMatrixEntryVia", _alwaysFetchFareMatrixEntryVia);
			info.AddValue("_alreadyFetchedFareMatrixEntryVia", _alreadyFetchedFareMatrixEntryVia);
			info.AddValue("_childFareStages", (!this.MarkedForDeletion?_childFareStages:null));
			info.AddValue("_alwaysFetchChildFareStages", _alwaysFetchChildFareStages);
			info.AddValue("_alreadyFetchedChildFareStages", _alreadyFetchedChildFareStages);
			info.AddValue("_fareStageAlias", (!this.MarkedForDeletion?_fareStageAlias:null));
			info.AddValue("_alwaysFetchFareStageAlias", _alwaysFetchFareStageAlias);
			info.AddValue("_alreadyFetchedFareStageAlias", _alreadyFetchedFareStageAlias);
			info.AddValue("_parameterFareStage", (!this.MarkedForDeletion?_parameterFareStage:null));
			info.AddValue("_alwaysFetchParameterFareStage", _alwaysFetchParameterFareStage);
			info.AddValue("_alreadyFetchedParameterFareStage", _alreadyFetchedParameterFareStage);
			info.AddValue("_parentFareStage", (!this.MarkedForDeletion?_parentFareStage:null));
			info.AddValue("_parentFareStageReturnsNewIfNotFound", _parentFareStageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentFareStage", _alwaysFetchParentFareStage);
			info.AddValue("_alreadyFetchedParentFareStage", _alreadyFetchedParentFareStage);
			info.AddValue("_fareStageHierarchieLevel", (!this.MarkedForDeletion?_fareStageHierarchieLevel:null));
			info.AddValue("_fareStageHierarchieLevelReturnsNewIfNotFound", _fareStageHierarchieLevelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageHierarchieLevel", _alwaysFetchFareStageHierarchieLevel);
			info.AddValue("_alreadyFetchedFareStageHierarchieLevel", _alreadyFetchedFareStageHierarchieLevel);
			info.AddValue("_fareStageList", (!this.MarkedForDeletion?_fareStageList:null));
			info.AddValue("_fareStageListReturnsNewIfNotFound", _fareStageListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageList", _alwaysFetchFareStageList);
			info.AddValue("_alreadyFetchedFareStageList", _alreadyFetchedFareStageList);
			info.AddValue("_fareStageType", (!this.MarkedForDeletion?_fareStageType:null));
			info.AddValue("_fareStageTypeReturnsNewIfNotFound", _fareStageTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageType", _alwaysFetchFareStageType);
			info.AddValue("_alreadyFetchedFareStageType", _alreadyFetchedFareStageType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParentFareStage":
					_alreadyFetchedParentFareStage = true;
					this.ParentFareStage = (FareStageEntity)entity;
					break;
				case "FareStageHierarchieLevel":
					_alreadyFetchedFareStageHierarchieLevel = true;
					this.FareStageHierarchieLevel = (FareStageHierarchieLevelEntity)entity;
					break;
				case "FareStageList":
					_alreadyFetchedFareStageList = true;
					this.FareStageList = (FareStageListEntity)entity;
					break;
				case "FareStageType":
					_alreadyFetchedFareStageType = true;
					this.FareStageType = (FareStageTypeEntity)entity;
					break;
				case "AreaListElement":
					_alreadyFetchedAreaListElement = true;
					if(entity!=null)
					{
						this.AreaListElement.Add((AreaListElementEntity)entity);
					}
					break;
				case "BusinessRuleResult":
					_alreadyFetchedBusinessRuleResult = true;
					if(entity!=null)
					{
						this.BusinessRuleResult.Add((BusinessRuleResultEntity)entity);
					}
					break;
				case "ExternalPacketEfforts":
					_alreadyFetchedExternalPacketEfforts = true;
					if(entity!=null)
					{
						this.ExternalPacketEfforts.Add((ExternalPacketEffortEntity)entity);
					}
					break;
				case "FareMatrixEntryBoarding":
					_alreadyFetchedFareMatrixEntryBoarding = true;
					if(entity!=null)
					{
						this.FareMatrixEntryBoarding.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "FareMatrixEntryDestination":
					_alreadyFetchedFareMatrixEntryDestination = true;
					if(entity!=null)
					{
						this.FareMatrixEntryDestination.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "FareMatrixEntryVia":
					_alreadyFetchedFareMatrixEntryVia = true;
					if(entity!=null)
					{
						this.FareMatrixEntryVia.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "ChildFareStages":
					_alreadyFetchedChildFareStages = true;
					if(entity!=null)
					{
						this.ChildFareStages.Add((FareStageEntity)entity);
					}
					break;
				case "FareStageAlias":
					_alreadyFetchedFareStageAlias = true;
					if(entity!=null)
					{
						this.FareStageAlias.Add((FareStageAliasEntity)entity);
					}
					break;
				case "ParameterFareStage":
					_alreadyFetchedParameterFareStage = true;
					if(entity!=null)
					{
						this.ParameterFareStage.Add((ParameterFareStageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParentFareStage":
					SetupSyncParentFareStage(relatedEntity);
					break;
				case "FareStageHierarchieLevel":
					SetupSyncFareStageHierarchieLevel(relatedEntity);
					break;
				case "FareStageList":
					SetupSyncFareStageList(relatedEntity);
					break;
				case "FareStageType":
					SetupSyncFareStageType(relatedEntity);
					break;
				case "AreaListElement":
					_areaListElement.Add((AreaListElementEntity)relatedEntity);
					break;
				case "BusinessRuleResult":
					_businessRuleResult.Add((BusinessRuleResultEntity)relatedEntity);
					break;
				case "ExternalPacketEfforts":
					_externalPacketEfforts.Add((ExternalPacketEffortEntity)relatedEntity);
					break;
				case "FareMatrixEntryBoarding":
					_fareMatrixEntryBoarding.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "FareMatrixEntryDestination":
					_fareMatrixEntryDestination.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "FareMatrixEntryVia":
					_fareMatrixEntryVia.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "ChildFareStages":
					_childFareStages.Add((FareStageEntity)relatedEntity);
					break;
				case "FareStageAlias":
					_fareStageAlias.Add((FareStageAliasEntity)relatedEntity);
					break;
				case "ParameterFareStage":
					_parameterFareStage.Add((ParameterFareStageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParentFareStage":
					DesetupSyncParentFareStage(false, true);
					break;
				case "FareStageHierarchieLevel":
					DesetupSyncFareStageHierarchieLevel(false, true);
					break;
				case "FareStageList":
					DesetupSyncFareStageList(false, true);
					break;
				case "FareStageType":
					DesetupSyncFareStageType(false, true);
					break;
				case "AreaListElement":
					this.PerformRelatedEntityRemoval(_areaListElement, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleResult":
					this.PerformRelatedEntityRemoval(_businessRuleResult, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalPacketEfforts":
					this.PerformRelatedEntityRemoval(_externalPacketEfforts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrixEntryBoarding":
					this.PerformRelatedEntityRemoval(_fareMatrixEntryBoarding, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrixEntryDestination":
					this.PerformRelatedEntityRemoval(_fareMatrixEntryDestination, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrixEntryVia":
					this.PerformRelatedEntityRemoval(_fareMatrixEntryVia, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ChildFareStages":
					this.PerformRelatedEntityRemoval(_childFareStages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareStageAlias":
					this.PerformRelatedEntityRemoval(_fareStageAlias, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterFareStage":
					this.PerformRelatedEntityRemoval(_parameterFareStage, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parentFareStage!=null)
			{
				toReturn.Add(_parentFareStage);
			}
			if(_fareStageHierarchieLevel!=null)
			{
				toReturn.Add(_fareStageHierarchieLevel);
			}
			if(_fareStageList!=null)
			{
				toReturn.Add(_fareStageList);
			}
			if(_fareStageType!=null)
			{
				toReturn.Add(_fareStageType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_areaListElement);
			toReturn.Add(_businessRuleResult);
			toReturn.Add(_externalPacketEfforts);
			toReturn.Add(_fareMatrixEntryBoarding);
			toReturn.Add(_fareMatrixEntryDestination);
			toReturn.Add(_fareMatrixEntryVia);
			toReturn.Add(_childFareStages);
			toReturn.Add(_fareStageAlias);
			toReturn.Add(_parameterFareStage);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageID)
		{
			return FetchUsingPK(fareStageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareStageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareStageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareStageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareStageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareStageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch)
		{
			return GetMultiAreaListElement(forceFetch, _areaListElement.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaListElement(forceFetch, _areaListElement.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaListElement(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaListElement || forceFetch || _alwaysFetchAreaListElement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaListElement);
				_areaListElement.SuppressClearInGetMulti=!forceFetch;
				_areaListElement.EntityFactoryToUse = entityFactoryToUse;
				_areaListElement.GetMultiManyToOne(null, null, this, null, filter);
				_areaListElement.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaListElement = true;
			}
			return _areaListElement;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaListElement'. These settings will be taken into account
		/// when the property AreaListElement is requested or GetMultiAreaListElement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaListElement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaListElement.SortClauses=sortClauses;
			_areaListElement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleResult(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleResult || forceFetch || _alwaysFetchBusinessRuleResult) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleResult);
				_businessRuleResult.SuppressClearInGetMulti=!forceFetch;
				_businessRuleResult.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleResult.GetMultiManyToOne(null, null, this, null, null, filter);
				_businessRuleResult.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleResult = true;
			}
			return _businessRuleResult;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleResult'. These settings will be taken into account
		/// when the property BusinessRuleResult is requested or GetMultiBusinessRuleResult is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleResult(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleResult.SortClauses=sortClauses;
			_businessRuleResult.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch)
		{
			return GetMultiExternalPacketEfforts(forceFetch, _externalPacketEfforts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalPacketEfforts(forceFetch, _externalPacketEfforts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalPacketEfforts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketEfforts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalPacketEfforts || forceFetch || _alwaysFetchExternalPacketEfforts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalPacketEfforts);
				_externalPacketEfforts.SuppressClearInGetMulti=!forceFetch;
				_externalPacketEfforts.EntityFactoryToUse = entityFactoryToUse;
				_externalPacketEfforts.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_externalPacketEfforts.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalPacketEfforts = true;
			}
			return _externalPacketEfforts;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalPacketEfforts'. These settings will be taken into account
		/// when the property ExternalPacketEfforts is requested or GetMultiExternalPacketEfforts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalPacketEfforts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalPacketEfforts.SortClauses=sortClauses;
			_externalPacketEfforts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryBoarding(bool forceFetch)
		{
			return GetMultiFareMatrixEntryBoarding(forceFetch, _fareMatrixEntryBoarding.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryBoarding(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixEntryBoarding(forceFetch, _fareMatrixEntryBoarding.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryBoarding(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixEntryBoarding(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryBoarding(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixEntryBoarding || forceFetch || _alwaysFetchFareMatrixEntryBoarding) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixEntryBoarding);
				_fareMatrixEntryBoarding.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixEntryBoarding.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixEntryBoarding.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_fareMatrixEntryBoarding.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixEntryBoarding = true;
			}
			return _fareMatrixEntryBoarding;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixEntryBoarding'. These settings will be taken into account
		/// when the property FareMatrixEntryBoarding is requested or GetMultiFareMatrixEntryBoarding is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixEntryBoarding(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixEntryBoarding.SortClauses=sortClauses;
			_fareMatrixEntryBoarding.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryDestination(bool forceFetch)
		{
			return GetMultiFareMatrixEntryDestination(forceFetch, _fareMatrixEntryDestination.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryDestination(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixEntryDestination(forceFetch, _fareMatrixEntryDestination.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryDestination(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixEntryDestination(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryDestination(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixEntryDestination || forceFetch || _alwaysFetchFareMatrixEntryDestination) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixEntryDestination);
				_fareMatrixEntryDestination.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixEntryDestination.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixEntryDestination.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_fareMatrixEntryDestination.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixEntryDestination = true;
			}
			return _fareMatrixEntryDestination;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixEntryDestination'. These settings will be taken into account
		/// when the property FareMatrixEntryDestination is requested or GetMultiFareMatrixEntryDestination is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixEntryDestination(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixEntryDestination.SortClauses=sortClauses;
			_fareMatrixEntryDestination.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryVia(bool forceFetch)
		{
			return GetMultiFareMatrixEntryVia(forceFetch, _fareMatrixEntryVia.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryVia(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixEntryVia(forceFetch, _fareMatrixEntryVia.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryVia(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixEntryVia(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntryVia(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixEntryVia || forceFetch || _alwaysFetchFareMatrixEntryVia) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixEntryVia);
				_fareMatrixEntryVia.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixEntryVia.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixEntryVia.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_fareMatrixEntryVia.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixEntryVia = true;
			}
			return _fareMatrixEntryVia;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixEntryVia'. These settings will be taken into account
		/// when the property FareMatrixEntryVia is requested or GetMultiFareMatrixEntryVia is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixEntryVia(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixEntryVia.SortClauses=sortClauses;
			_fareMatrixEntryVia.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiChildFareStages(bool forceFetch)
		{
			return GetMultiChildFareStages(forceFetch, _childFareStages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiChildFareStages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildFareStages(forceFetch, _childFareStages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiChildFareStages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildFareStages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiChildFareStages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildFareStages || forceFetch || _alwaysFetchChildFareStages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childFareStages);
				_childFareStages.SuppressClearInGetMulti=!forceFetch;
				_childFareStages.EntityFactoryToUse = entityFactoryToUse;
				_childFareStages.GetMultiManyToOne(this, null, null, null, filter);
				_childFareStages.SuppressClearInGetMulti=false;
				_alreadyFetchedChildFareStages = true;
			}
			return _childFareStages;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildFareStages'. These settings will be taken into account
		/// when the property ChildFareStages is requested or GetMultiChildFareStages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildFareStages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childFareStages.SortClauses=sortClauses;
			_childFareStages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareStageAliasEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareStageAliasEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageAliasCollection GetMultiFareStageAlias(bool forceFetch)
		{
			return GetMultiFareStageAlias(forceFetch, _fareStageAlias.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageAliasEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareStageAliasEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageAliasCollection GetMultiFareStageAlias(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareStageAlias(forceFetch, _fareStageAlias.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareStageAliasEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareStageAliasCollection GetMultiFareStageAlias(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareStageAlias(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageAliasEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareStageAliasCollection GetMultiFareStageAlias(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareStageAlias || forceFetch || _alwaysFetchFareStageAlias) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareStageAlias);
				_fareStageAlias.SuppressClearInGetMulti=!forceFetch;
				_fareStageAlias.EntityFactoryToUse = entityFactoryToUse;
				_fareStageAlias.GetMultiManyToOne(this, null, filter);
				_fareStageAlias.SuppressClearInGetMulti=false;
				_alreadyFetchedFareStageAlias = true;
			}
			return _fareStageAlias;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareStageAlias'. These settings will be taken into account
		/// when the property FareStageAlias is requested or GetMultiFareStageAlias is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareStageAlias(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareStageAlias.SortClauses=sortClauses;
			_fareStageAlias.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterFareStage(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterFareStage || forceFetch || _alwaysFetchParameterFareStage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterFareStage);
				_parameterFareStage.SuppressClearInGetMulti=!forceFetch;
				_parameterFareStage.EntityFactoryToUse = entityFactoryToUse;
				_parameterFareStage.GetMultiManyToOne(this, null, null, filter);
				_parameterFareStage.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterFareStage = true;
			}
			return _parameterFareStage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterFareStage'. These settings will be taken into account
		/// when the property ParameterFareStage is requested or GetMultiParameterFareStage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterFareStage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterFareStage.SortClauses=sortClauses;
			_parameterFareStage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleParentFareStage()
		{
			return GetSingleParentFareStage(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleParentFareStage(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentFareStage || forceFetch || _alwaysFetchParentFareStage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingFareStageIDParentID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentFareStageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentFareStage = newEntity;
				_alreadyFetchedParentFareStage = fetchResult;
			}
			return _parentFareStage;
		}


		/// <summary> Retrieves the related entity of type 'FareStageHierarchieLevelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageHierarchieLevelEntity' which is related to this entity.</returns>
		public FareStageHierarchieLevelEntity GetSingleFareStageHierarchieLevel()
		{
			return GetSingleFareStageHierarchieLevel(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageHierarchieLevelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageHierarchieLevelEntity' which is related to this entity.</returns>
		public virtual FareStageHierarchieLevelEntity GetSingleFareStageHierarchieLevel(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageHierarchieLevel || forceFetch || _alwaysFetchFareStageHierarchieLevel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageHierarchieLevelEntityUsingHierarchieLevel);
				FareStageHierarchieLevelEntity newEntity = new FareStageHierarchieLevelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.HierarchieLevel.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageHierarchieLevelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageHierarchieLevelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageHierarchieLevel = newEntity;
				_alreadyFetchedFareStageHierarchieLevel = fetchResult;
			}
			return _fareStageHierarchieLevel;
		}


		/// <summary> Retrieves the related entity of type 'FareStageListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageListEntity' which is related to this entity.</returns>
		public FareStageListEntity GetSingleFareStageList()
		{
			return GetSingleFareStageList(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageListEntity' which is related to this entity.</returns>
		public virtual FareStageListEntity GetSingleFareStageList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageList || forceFetch || _alwaysFetchFareStageList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageListEntityUsingFareStageListID);
				FareStageListEntity newEntity = new FareStageListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageListID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageList = newEntity;
				_alreadyFetchedFareStageList = fetchResult;
			}
			return _fareStageList;
		}


		/// <summary> Retrieves the related entity of type 'FareStageTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageTypeEntity' which is related to this entity.</returns>
		public FareStageTypeEntity GetSingleFareStageType()
		{
			return GetSingleFareStageType(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageTypeEntity' which is related to this entity.</returns>
		public virtual FareStageTypeEntity GetSingleFareStageType(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageType || forceFetch || _alwaysFetchFareStageType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageTypeEntityUsingFareStageTypeID);
				FareStageTypeEntity newEntity = new FareStageTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageType = newEntity;
				_alreadyFetchedFareStageType = fetchResult;
			}
			return _fareStageType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParentFareStage", _parentFareStage);
			toReturn.Add("FareStageHierarchieLevel", _fareStageHierarchieLevel);
			toReturn.Add("FareStageList", _fareStageList);
			toReturn.Add("FareStageType", _fareStageType);
			toReturn.Add("AreaListElement", _areaListElement);
			toReturn.Add("BusinessRuleResult", _businessRuleResult);
			toReturn.Add("ExternalPacketEfforts", _externalPacketEfforts);
			toReturn.Add("FareMatrixEntryBoarding", _fareMatrixEntryBoarding);
			toReturn.Add("FareMatrixEntryDestination", _fareMatrixEntryDestination);
			toReturn.Add("FareMatrixEntryVia", _fareMatrixEntryVia);
			toReturn.Add("ChildFareStages", _childFareStages);
			toReturn.Add("FareStageAlias", _fareStageAlias);
			toReturn.Add("ParameterFareStage", _parameterFareStage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="validator">The validator object for this FareStageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareStageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareStageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_areaListElement = new VarioSL.Entities.CollectionClasses.AreaListElementCollection();
			_areaListElement.SetContainingEntityInfo(this, "FareStage");

			_businessRuleResult = new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection();
			_businessRuleResult.SetContainingEntityInfo(this, "FareStage");

			_externalPacketEfforts = new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection();
			_externalPacketEfforts.SetContainingEntityInfo(this, "FareStage");

			_fareMatrixEntryBoarding = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_fareMatrixEntryBoarding.SetContainingEntityInfo(this, "FareStageBoarding");

			_fareMatrixEntryDestination = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_fareMatrixEntryDestination.SetContainingEntityInfo(this, "FareStageDestination");

			_fareMatrixEntryVia = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_fareMatrixEntryVia.SetContainingEntityInfo(this, "FareStageVia");

			_childFareStages = new VarioSL.Entities.CollectionClasses.FareStageCollection();
			_childFareStages.SetContainingEntityInfo(this, "ParentFareStage");

			_fareStageAlias = new VarioSL.Entities.CollectionClasses.FareStageAliasCollection();
			_fareStageAlias.SetContainingEntityInfo(this, "FareStage");

			_parameterFareStage = new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection();
			_parameterFareStage.SetContainingEntityInfo(this, "FareStage");
			_parentFareStageReturnsNewIfNotFound = false;
			_fareStageHierarchieLevelReturnsNewIfNotFound = false;
			_fareStageListReturnsNewIfNotFound = false;
			_fareStageTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Externalnumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HierarchieLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Numberoverride", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Selectable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parentFareStage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentFareStage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentFareStage, new PropertyChangedEventHandler( OnParentFareStagePropertyChanged ), "ParentFareStage", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageEntityUsingFareStageIDParentIDStatic, true, signalRelatedEntity, "ChildFareStages", resetFKFields, new int[] { (int)FareStageFieldIndex.ParentID } );		
			_parentFareStage = null;
		}
		
		/// <summary> setups the sync logic for member _parentFareStage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentFareStage(IEntityCore relatedEntity)
		{
			if(_parentFareStage!=relatedEntity)
			{		
				DesetupSyncParentFareStage(true, true);
				_parentFareStage = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentFareStage, new PropertyChangedEventHandler( OnParentFareStagePropertyChanged ), "ParentFareStage", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageEntityUsingFareStageIDParentIDStatic, true, ref _alreadyFetchedParentFareStage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentFareStagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageHierarchieLevel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageHierarchieLevel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageHierarchieLevel, new PropertyChangedEventHandler( OnFareStageHierarchieLevelPropertyChanged ), "FareStageHierarchieLevel", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageHierarchieLevelEntityUsingHierarchieLevelStatic, true, signalRelatedEntity, "FareStages", resetFKFields, new int[] { (int)FareStageFieldIndex.HierarchieLevel } );		
			_fareStageHierarchieLevel = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageHierarchieLevel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageHierarchieLevel(IEntityCore relatedEntity)
		{
			if(_fareStageHierarchieLevel!=relatedEntity)
			{		
				DesetupSyncFareStageHierarchieLevel(true, true);
				_fareStageHierarchieLevel = (FareStageHierarchieLevelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageHierarchieLevel, new PropertyChangedEventHandler( OnFareStageHierarchieLevelPropertyChanged ), "FareStageHierarchieLevel", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageHierarchieLevelEntityUsingHierarchieLevelStatic, true, ref _alreadyFetchedFareStageHierarchieLevel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageHierarchieLevelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageList, new PropertyChangedEventHandler( OnFareStageListPropertyChanged ), "FareStageList", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageListEntityUsingFareStageListIDStatic, true, signalRelatedEntity, "Farestages", resetFKFields, new int[] { (int)FareStageFieldIndex.FareStageListID } );		
			_fareStageList = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageList(IEntityCore relatedEntity)
		{
			if(_fareStageList!=relatedEntity)
			{		
				DesetupSyncFareStageList(true, true);
				_fareStageList = (FareStageListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageList, new PropertyChangedEventHandler( OnFareStageListPropertyChanged ), "FareStageList", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageListEntityUsingFareStageListIDStatic, true, ref _alreadyFetchedFareStageList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageType, new PropertyChangedEventHandler( OnFareStageTypePropertyChanged ), "FareStageType", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageTypeEntityUsingFareStageTypeIDStatic, true, signalRelatedEntity, "FareStages", resetFKFields, new int[] { (int)FareStageFieldIndex.FareStageTypeID } );		
			_fareStageType = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageType(IEntityCore relatedEntity)
		{
			if(_fareStageType!=relatedEntity)
			{		
				DesetupSyncFareStageType(true, true);
				_fareStageType = (FareStageTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageType, new PropertyChangedEventHandler( OnFareStageTypePropertyChanged ), "FareStageType", VarioSL.Entities.RelationClasses.StaticFareStageRelations.FareStageTypeEntityUsingFareStageTypeIDStatic, true, ref _alreadyFetchedFareStageType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareStageID">PK value for FareStage which data should be fetched into this FareStage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareStageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareStageFieldIndex.FareStageID].ForcedCurrentValueWrite(fareStageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareStageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareStageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareStageRelations Relations
		{
			get	{ return new FareStageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaListElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListElement
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListElementCollection(), (IEntityRelation)GetRelationsForField("AreaListElement")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.AreaListElementEntity, 0, null, null, null, "AreaListElement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleResult
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleResult")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, 0, null, null, null, "BusinessRuleResult", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacketEffort' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacketEfforts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalPacketEfforts")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, 0, null, null, null, "ExternalPacketEfforts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixEntryBoarding
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("FareMatrixEntryBoarding")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "FareMatrixEntryBoarding", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixEntryDestination
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("FareMatrixEntryDestination")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "FareMatrixEntryDestination", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixEntryVia
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("FareMatrixEntryVia")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "FareMatrixEntryVia", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildFareStages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("ChildFareStages")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "ChildFareStages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageAlias' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageAlias
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageAliasCollection(), (IEntityRelation)GetRelationsForField("FareStageAlias")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageAliasEntity, 0, null, null, null, "FareStageAlias", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterFareStage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterFareStage
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection(), (IEntityRelation)GetRelationsForField("ParameterFareStage")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.ParameterFareStageEntity, 0, null, null, null, "ParameterFareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentFareStage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("ParentFareStage")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "ParentFareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageHierarchieLevel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageHierarchieLevel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageHierarchieLevelCollection(), (IEntityRelation)GetRelationsForField("FareStageHierarchieLevel")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageHierarchieLevelEntity, 0, null, null, null, "FareStageHierarchieLevel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageListCollection(), (IEntityRelation)GetRelationsForField("FareStageList")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageListEntity, 0, null, null, null, "FareStageList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageTypeCollection(), (IEntityRelation)GetRelationsForField("FareStageType")[0], (int)VarioSL.Entities.EntityType.FareStageEntity, (int)VarioSL.Entities.EntityType.FareStageTypeEntity, 0, null, null, null, "FareStageType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Externalnumber property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Externalnumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.Externalnumber, false); }
			set	{ SetValue((int)FareStageFieldIndex.Externalnumber, value, true); }
		}

		/// <summary> The FareStageClass property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."FARESTAGECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStageClass
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.FareStageClass, false); }
			set	{ SetValue((int)FareStageFieldIndex.FareStageClass, value, true); }
		}

		/// <summary> The FareStageID property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."FARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareStageID
		{
			get { return (System.Int64)GetValue((int)FareStageFieldIndex.FareStageID, true); }
			set	{ SetValue((int)FareStageFieldIndex.FareStageID, value, true); }
		}

		/// <summary> The FareStageListID property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."FARESTAGELISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStageListID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.FareStageListID, false); }
			set	{ SetValue((int)FareStageFieldIndex.FareStageListID, value, true); }
		}

		/// <summary> The FareStageTypeID property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."FARESTAGETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> FareStageTypeID
		{
			get { return (Nullable<System.Decimal>)GetValue((int)FareStageFieldIndex.FareStageTypeID, false); }
			set	{ SetValue((int)FareStageFieldIndex.FareStageTypeID, value, true); }
		}

		/// <summary> The HierarchieLevel property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."HIERARCHIELEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> HierarchieLevel
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.HierarchieLevel, false); }
			set	{ SetValue((int)FareStageFieldIndex.HierarchieLevel, value, true); }
		}

		/// <summary> The Name property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareStageFieldIndex.Name, true); }
			set	{ SetValue((int)FareStageFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.Number, false); }
			set	{ SetValue((int)FareStageFieldIndex.Number, value, true); }
		}

		/// <summary> The Numberoverride property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."NUMBEROVERRIDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Numberoverride
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.Numberoverride, false); }
			set	{ SetValue((int)FareStageFieldIndex.Numberoverride, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrderNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.OrderNumber, false); }
			set	{ SetValue((int)FareStageFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The ParentID property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."PARENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParentID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageFieldIndex.ParentID, false); }
			set	{ SetValue((int)FareStageFieldIndex.ParentID, value, true); }
		}

		/// <summary> The Selectable property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."SELECTABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Selectable
		{
			get { return (Nullable<System.Int16>)GetValue((int)FareStageFieldIndex.Selectable, false); }
			set	{ SetValue((int)FareStageFieldIndex.Selectable, value, true); }
		}

		/// <summary> The ShortName property of the Entity FareStage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGE"."SHORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortName
		{
			get { return (System.String)GetValue((int)FareStageFieldIndex.ShortName, true); }
			set	{ SetValue((int)FareStageFieldIndex.ShortName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaListElement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection AreaListElement
		{
			get	{ return GetMultiAreaListElement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListElement. When set to true, AreaListElement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListElement is accessed. You can always execute/ a forced fetch by calling GetMultiAreaListElement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListElement
		{
			get	{ return _alwaysFetchAreaListElement; }
			set	{ _alwaysFetchAreaListElement = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListElement already has been fetched. Setting this property to false when AreaListElement has been fetched
		/// will clear the AreaListElement collection well. Setting this property to true while AreaListElement hasn't been fetched disables lazy loading for AreaListElement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListElement
		{
			get { return _alreadyFetchedAreaListElement;}
			set 
			{
				if(_alreadyFetchedAreaListElement && !value && (_areaListElement != null))
				{
					_areaListElement.Clear();
				}
				_alreadyFetchedAreaListElement = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleResult()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection BusinessRuleResult
		{
			get	{ return GetMultiBusinessRuleResult(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleResult. When set to true, BusinessRuleResult is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleResult is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleResult(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleResult
		{
			get	{ return _alwaysFetchBusinessRuleResult; }
			set	{ _alwaysFetchBusinessRuleResult = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleResult already has been fetched. Setting this property to false when BusinessRuleResult has been fetched
		/// will clear the BusinessRuleResult collection well. Setting this property to true while BusinessRuleResult hasn't been fetched disables lazy loading for BusinessRuleResult</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleResult
		{
			get { return _alreadyFetchedBusinessRuleResult;}
			set 
			{
				if(_alreadyFetchedBusinessRuleResult && !value && (_businessRuleResult != null))
				{
					_businessRuleResult.Clear();
				}
				_alreadyFetchedBusinessRuleResult = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalPacketEfforts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection ExternalPacketEfforts
		{
			get	{ return GetMultiExternalPacketEfforts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacketEfforts. When set to true, ExternalPacketEfforts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacketEfforts is accessed. You can always execute/ a forced fetch by calling GetMultiExternalPacketEfforts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacketEfforts
		{
			get	{ return _alwaysFetchExternalPacketEfforts; }
			set	{ _alwaysFetchExternalPacketEfforts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacketEfforts already has been fetched. Setting this property to false when ExternalPacketEfforts has been fetched
		/// will clear the ExternalPacketEfforts collection well. Setting this property to true while ExternalPacketEfforts hasn't been fetched disables lazy loading for ExternalPacketEfforts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacketEfforts
		{
			get { return _alreadyFetchedExternalPacketEfforts;}
			set 
			{
				if(_alreadyFetchedExternalPacketEfforts && !value && (_externalPacketEfforts != null))
				{
					_externalPacketEfforts.Clear();
				}
				_alreadyFetchedExternalPacketEfforts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixEntryBoarding()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection FareMatrixEntryBoarding
		{
			get	{ return GetMultiFareMatrixEntryBoarding(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixEntryBoarding. When set to true, FareMatrixEntryBoarding is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixEntryBoarding is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixEntryBoarding(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixEntryBoarding
		{
			get	{ return _alwaysFetchFareMatrixEntryBoarding; }
			set	{ _alwaysFetchFareMatrixEntryBoarding = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixEntryBoarding already has been fetched. Setting this property to false when FareMatrixEntryBoarding has been fetched
		/// will clear the FareMatrixEntryBoarding collection well. Setting this property to true while FareMatrixEntryBoarding hasn't been fetched disables lazy loading for FareMatrixEntryBoarding</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixEntryBoarding
		{
			get { return _alreadyFetchedFareMatrixEntryBoarding;}
			set 
			{
				if(_alreadyFetchedFareMatrixEntryBoarding && !value && (_fareMatrixEntryBoarding != null))
				{
					_fareMatrixEntryBoarding.Clear();
				}
				_alreadyFetchedFareMatrixEntryBoarding = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixEntryDestination()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection FareMatrixEntryDestination
		{
			get	{ return GetMultiFareMatrixEntryDestination(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixEntryDestination. When set to true, FareMatrixEntryDestination is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixEntryDestination is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixEntryDestination(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixEntryDestination
		{
			get	{ return _alwaysFetchFareMatrixEntryDestination; }
			set	{ _alwaysFetchFareMatrixEntryDestination = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixEntryDestination already has been fetched. Setting this property to false when FareMatrixEntryDestination has been fetched
		/// will clear the FareMatrixEntryDestination collection well. Setting this property to true while FareMatrixEntryDestination hasn't been fetched disables lazy loading for FareMatrixEntryDestination</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixEntryDestination
		{
			get { return _alreadyFetchedFareMatrixEntryDestination;}
			set 
			{
				if(_alreadyFetchedFareMatrixEntryDestination && !value && (_fareMatrixEntryDestination != null))
				{
					_fareMatrixEntryDestination.Clear();
				}
				_alreadyFetchedFareMatrixEntryDestination = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixEntryVia()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection FareMatrixEntryVia
		{
			get	{ return GetMultiFareMatrixEntryVia(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixEntryVia. When set to true, FareMatrixEntryVia is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixEntryVia is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixEntryVia(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixEntryVia
		{
			get	{ return _alwaysFetchFareMatrixEntryVia; }
			set	{ _alwaysFetchFareMatrixEntryVia = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixEntryVia already has been fetched. Setting this property to false when FareMatrixEntryVia has been fetched
		/// will clear the FareMatrixEntryVia collection well. Setting this property to true while FareMatrixEntryVia hasn't been fetched disables lazy loading for FareMatrixEntryVia</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixEntryVia
		{
			get { return _alreadyFetchedFareMatrixEntryVia;}
			set 
			{
				if(_alreadyFetchedFareMatrixEntryVia && !value && (_fareMatrixEntryVia != null))
				{
					_fareMatrixEntryVia.Clear();
				}
				_alreadyFetchedFareMatrixEntryVia = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildFareStages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareStageCollection ChildFareStages
		{
			get	{ return GetMultiChildFareStages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildFareStages. When set to true, ChildFareStages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildFareStages is accessed. You can always execute/ a forced fetch by calling GetMultiChildFareStages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildFareStages
		{
			get	{ return _alwaysFetchChildFareStages; }
			set	{ _alwaysFetchChildFareStages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildFareStages already has been fetched. Setting this property to false when ChildFareStages has been fetched
		/// will clear the ChildFareStages collection well. Setting this property to true while ChildFareStages hasn't been fetched disables lazy loading for ChildFareStages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildFareStages
		{
			get { return _alreadyFetchedChildFareStages;}
			set 
			{
				if(_alreadyFetchedChildFareStages && !value && (_childFareStages != null))
				{
					_childFareStages.Clear();
				}
				_alreadyFetchedChildFareStages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareStageAliasEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareStageAlias()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareStageAliasCollection FareStageAlias
		{
			get	{ return GetMultiFareStageAlias(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageAlias. When set to true, FareStageAlias is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageAlias is accessed. You can always execute/ a forced fetch by calling GetMultiFareStageAlias(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageAlias
		{
			get	{ return _alwaysFetchFareStageAlias; }
			set	{ _alwaysFetchFareStageAlias = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageAlias already has been fetched. Setting this property to false when FareStageAlias has been fetched
		/// will clear the FareStageAlias collection well. Setting this property to true while FareStageAlias hasn't been fetched disables lazy loading for FareStageAlias</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageAlias
		{
			get { return _alreadyFetchedFareStageAlias;}
			set 
			{
				if(_alreadyFetchedFareStageAlias && !value && (_fareStageAlias != null))
				{
					_fareStageAlias.Clear();
				}
				_alreadyFetchedFareStageAlias = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection ParameterFareStage
		{
			get	{ return GetMultiParameterFareStage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterFareStage. When set to true, ParameterFareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterFareStage is accessed. You can always execute/ a forced fetch by calling GetMultiParameterFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterFareStage
		{
			get	{ return _alwaysFetchParameterFareStage; }
			set	{ _alwaysFetchParameterFareStage = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterFareStage already has been fetched. Setting this property to false when ParameterFareStage has been fetched
		/// will clear the ParameterFareStage collection well. Setting this property to true while ParameterFareStage hasn't been fetched disables lazy loading for ParameterFareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterFareStage
		{
			get { return _alreadyFetchedParameterFareStage;}
			set 
			{
				if(_alreadyFetchedParameterFareStage && !value && (_parameterFareStage != null))
				{
					_parameterFareStage.Clear();
				}
				_alreadyFetchedParameterFareStage = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity ParentFareStage
		{
			get	{ return GetSingleParentFareStage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentFareStage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildFareStages", "ParentFareStage", _parentFareStage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentFareStage. When set to true, ParentFareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentFareStage is accessed. You can always execute a forced fetch by calling GetSingleParentFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentFareStage
		{
			get	{ return _alwaysFetchParentFareStage; }
			set	{ _alwaysFetchParentFareStage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentFareStage already has been fetched. Setting this property to false when ParentFareStage has been fetched
		/// will set ParentFareStage to null as well. Setting this property to true while ParentFareStage hasn't been fetched disables lazy loading for ParentFareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentFareStage
		{
			get { return _alreadyFetchedParentFareStage;}
			set 
			{
				if(_alreadyFetchedParentFareStage && !value)
				{
					this.ParentFareStage = null;
				}
				_alreadyFetchedParentFareStage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentFareStage is not found
		/// in the database. When set to true, ParentFareStage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParentFareStageReturnsNewIfNotFound
		{
			get	{ return _parentFareStageReturnsNewIfNotFound; }
			set { _parentFareStageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageHierarchieLevelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageHierarchieLevel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageHierarchieLevelEntity FareStageHierarchieLevel
		{
			get	{ return GetSingleFareStageHierarchieLevel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageHierarchieLevel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStages", "FareStageHierarchieLevel", _fareStageHierarchieLevel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageHierarchieLevel. When set to true, FareStageHierarchieLevel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageHierarchieLevel is accessed. You can always execute a forced fetch by calling GetSingleFareStageHierarchieLevel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageHierarchieLevel
		{
			get	{ return _alwaysFetchFareStageHierarchieLevel; }
			set	{ _alwaysFetchFareStageHierarchieLevel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageHierarchieLevel already has been fetched. Setting this property to false when FareStageHierarchieLevel has been fetched
		/// will set FareStageHierarchieLevel to null as well. Setting this property to true while FareStageHierarchieLevel hasn't been fetched disables lazy loading for FareStageHierarchieLevel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageHierarchieLevel
		{
			get { return _alreadyFetchedFareStageHierarchieLevel;}
			set 
			{
				if(_alreadyFetchedFareStageHierarchieLevel && !value)
				{
					this.FareStageHierarchieLevel = null;
				}
				_alreadyFetchedFareStageHierarchieLevel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageHierarchieLevel is not found
		/// in the database. When set to true, FareStageHierarchieLevel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageHierarchieLevelReturnsNewIfNotFound
		{
			get	{ return _fareStageHierarchieLevelReturnsNewIfNotFound; }
			set { _fareStageHierarchieLevelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageListEntity FareStageList
		{
			get	{ return GetSingleFareStageList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Farestages", "FareStageList", _fareStageList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageList. When set to true, FareStageList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageList is accessed. You can always execute a forced fetch by calling GetSingleFareStageList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageList
		{
			get	{ return _alwaysFetchFareStageList; }
			set	{ _alwaysFetchFareStageList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageList already has been fetched. Setting this property to false when FareStageList has been fetched
		/// will set FareStageList to null as well. Setting this property to true while FareStageList hasn't been fetched disables lazy loading for FareStageList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageList
		{
			get { return _alreadyFetchedFareStageList;}
			set 
			{
				if(_alreadyFetchedFareStageList && !value)
				{
					this.FareStageList = null;
				}
				_alreadyFetchedFareStageList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageList is not found
		/// in the database. When set to true, FareStageList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageListReturnsNewIfNotFound
		{
			get	{ return _fareStageListReturnsNewIfNotFound; }
			set { _fareStageListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageTypeEntity FareStageType
		{
			get	{ return GetSingleFareStageType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStages", "FareStageType", _fareStageType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageType. When set to true, FareStageType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageType is accessed. You can always execute a forced fetch by calling GetSingleFareStageType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageType
		{
			get	{ return _alwaysFetchFareStageType; }
			set	{ _alwaysFetchFareStageType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageType already has been fetched. Setting this property to false when FareStageType has been fetched
		/// will set FareStageType to null as well. Setting this property to true while FareStageType hasn't been fetched disables lazy loading for FareStageType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageType
		{
			get { return _alreadyFetchedFareStageType;}
			set 
			{
				if(_alreadyFetchedFareStageType && !value)
				{
					this.FareStageType = null;
				}
				_alreadyFetchedFareStageType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageType is not found
		/// in the database. When set to true, FareStageType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageTypeReturnsNewIfNotFound
		{
			get	{ return _fareStageTypeReturnsNewIfNotFound; }
			set { _fareStageTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareStageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
