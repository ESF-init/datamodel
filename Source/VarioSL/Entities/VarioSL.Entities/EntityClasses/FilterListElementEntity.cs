﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FilterListElement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FilterListElementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FilterValueCollection	_filterValues;
		private bool	_alwaysFetchFilterValues, _alreadyFetchedFilterValues;
		private FilterCategoryEntity _filterCategory;
		private bool	_alwaysFetchFilterCategory, _alreadyFetchedFilterCategory, _filterCategoryReturnsNewIfNotFound;
		private FilterElementEntity _filterElement;
		private bool	_alwaysFetchFilterElement, _alreadyFetchedFilterElement, _filterElementReturnsNewIfNotFound;
		private FilterListEntity _filterList;
		private bool	_alwaysFetchFilterList, _alreadyFetchedFilterList, _filterListReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FilterCategory</summary>
			public static readonly string FilterCategory = "FilterCategory";
			/// <summary>Member name FilterElement</summary>
			public static readonly string FilterElement = "FilterElement";
			/// <summary>Member name FilterList</summary>
			public static readonly string FilterList = "FilterList";
			/// <summary>Member name FilterValues</summary>
			public static readonly string FilterValues = "FilterValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FilterListElementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilterListElementEntity() :base("FilterListElementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		public FilterListElementEntity(System.Int64 filterListElementID):base("FilterListElementEntity")
		{
			InitClassFetch(filterListElementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FilterListElementEntity(System.Int64 filterListElementID, IPrefetchPath prefetchPathToUse):base("FilterListElementEntity")
		{
			InitClassFetch(filterListElementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="validator">The custom validator object for this FilterListElementEntity</param>
		public FilterListElementEntity(System.Int64 filterListElementID, IValidator validator):base("FilterListElementEntity")
		{
			InitClassFetch(filterListElementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilterListElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_filterValues = (VarioSL.Entities.CollectionClasses.FilterValueCollection)info.GetValue("_filterValues", typeof(VarioSL.Entities.CollectionClasses.FilterValueCollection));
			_alwaysFetchFilterValues = info.GetBoolean("_alwaysFetchFilterValues");
			_alreadyFetchedFilterValues = info.GetBoolean("_alreadyFetchedFilterValues");
			_filterCategory = (FilterCategoryEntity)info.GetValue("_filterCategory", typeof(FilterCategoryEntity));
			if(_filterCategory!=null)
			{
				_filterCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterCategoryReturnsNewIfNotFound = info.GetBoolean("_filterCategoryReturnsNewIfNotFound");
			_alwaysFetchFilterCategory = info.GetBoolean("_alwaysFetchFilterCategory");
			_alreadyFetchedFilterCategory = info.GetBoolean("_alreadyFetchedFilterCategory");

			_filterElement = (FilterElementEntity)info.GetValue("_filterElement", typeof(FilterElementEntity));
			if(_filterElement!=null)
			{
				_filterElement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterElementReturnsNewIfNotFound = info.GetBoolean("_filterElementReturnsNewIfNotFound");
			_alwaysFetchFilterElement = info.GetBoolean("_alwaysFetchFilterElement");
			_alreadyFetchedFilterElement = info.GetBoolean("_alreadyFetchedFilterElement");

			_filterList = (FilterListEntity)info.GetValue("_filterList", typeof(FilterListEntity));
			if(_filterList!=null)
			{
				_filterList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterListReturnsNewIfNotFound = info.GetBoolean("_filterListReturnsNewIfNotFound");
			_alwaysFetchFilterList = info.GetBoolean("_alwaysFetchFilterList");
			_alreadyFetchedFilterList = info.GetBoolean("_alreadyFetchedFilterList");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FilterListElementFieldIndex)fieldIndex)
			{
				case FilterListElementFieldIndex.FilterCategoryID:
					DesetupSyncFilterCategory(true, false);
					_alreadyFetchedFilterCategory = false;
					break;
				case FilterListElementFieldIndex.FilterElementID:
					DesetupSyncFilterElement(true, false);
					_alreadyFetchedFilterElement = false;
					break;
				case FilterListElementFieldIndex.FilterListID:
					DesetupSyncFilterList(true, false);
					_alreadyFetchedFilterList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFilterValues = (_filterValues.Count > 0);
			_alreadyFetchedFilterCategory = (_filterCategory != null);
			_alreadyFetchedFilterElement = (_filterElement != null);
			_alreadyFetchedFilterList = (_filterList != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FilterCategory":
					toReturn.Add(Relations.FilterCategoryEntityUsingFilterCategoryID);
					break;
				case "FilterElement":
					toReturn.Add(Relations.FilterElementEntityUsingFilterElementID);
					break;
				case "FilterList":
					toReturn.Add(Relations.FilterListEntityUsingFilterListID);
					break;
				case "FilterValues":
					toReturn.Add(Relations.FilterValueEntityUsingFilterListElementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_filterValues", (!this.MarkedForDeletion?_filterValues:null));
			info.AddValue("_alwaysFetchFilterValues", _alwaysFetchFilterValues);
			info.AddValue("_alreadyFetchedFilterValues", _alreadyFetchedFilterValues);
			info.AddValue("_filterCategory", (!this.MarkedForDeletion?_filterCategory:null));
			info.AddValue("_filterCategoryReturnsNewIfNotFound", _filterCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterCategory", _alwaysFetchFilterCategory);
			info.AddValue("_alreadyFetchedFilterCategory", _alreadyFetchedFilterCategory);
			info.AddValue("_filterElement", (!this.MarkedForDeletion?_filterElement:null));
			info.AddValue("_filterElementReturnsNewIfNotFound", _filterElementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterElement", _alwaysFetchFilterElement);
			info.AddValue("_alreadyFetchedFilterElement", _alreadyFetchedFilterElement);
			info.AddValue("_filterList", (!this.MarkedForDeletion?_filterList:null));
			info.AddValue("_filterListReturnsNewIfNotFound", _filterListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterList", _alwaysFetchFilterList);
			info.AddValue("_alreadyFetchedFilterList", _alreadyFetchedFilterList);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FilterCategory":
					_alreadyFetchedFilterCategory = true;
					this.FilterCategory = (FilterCategoryEntity)entity;
					break;
				case "FilterElement":
					_alreadyFetchedFilterElement = true;
					this.FilterElement = (FilterElementEntity)entity;
					break;
				case "FilterList":
					_alreadyFetchedFilterList = true;
					this.FilterList = (FilterListEntity)entity;
					break;
				case "FilterValues":
					_alreadyFetchedFilterValues = true;
					if(entity!=null)
					{
						this.FilterValues.Add((FilterValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FilterCategory":
					SetupSyncFilterCategory(relatedEntity);
					break;
				case "FilterElement":
					SetupSyncFilterElement(relatedEntity);
					break;
				case "FilterList":
					SetupSyncFilterList(relatedEntity);
					break;
				case "FilterValues":
					_filterValues.Add((FilterValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FilterCategory":
					DesetupSyncFilterCategory(false, true);
					break;
				case "FilterElement":
					DesetupSyncFilterElement(false, true);
					break;
				case "FilterList":
					DesetupSyncFilterList(false, true);
					break;
				case "FilterValues":
					this.PerformRelatedEntityRemoval(_filterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_filterCategory!=null)
			{
				toReturn.Add(_filterCategory);
			}
			if(_filterElement!=null)
			{
				toReturn.Add(_filterElement);
			}
			if(_filterList!=null)
			{
				toReturn.Add(_filterList);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_filterValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListElementID)
		{
			return FetchUsingPK(filterListElementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListElementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(filterListElementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(filterListElementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(filterListElementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FilterListElementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FilterListElementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch)
		{
			return GetMultiFilterValues(forceFetch, _filterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValues(forceFetch, _filterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValues || forceFetch || _alwaysFetchFilterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValues);
				_filterValues.SuppressClearInGetMulti=!forceFetch;
				_filterValues.EntityFactoryToUse = entityFactoryToUse;
				_filterValues.GetMultiManyToOne(this, null, filter);
				_filterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValues = true;
			}
			return _filterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValues'. These settings will be taken into account
		/// when the property FilterValues is requested or GetMultiFilterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValues.SortClauses=sortClauses;
			_filterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'FilterCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterCategoryEntity' which is related to this entity.</returns>
		public FilterCategoryEntity GetSingleFilterCategory()
		{
			return GetSingleFilterCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterCategoryEntity' which is related to this entity.</returns>
		public virtual FilterCategoryEntity GetSingleFilterCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterCategory || forceFetch || _alwaysFetchFilterCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterCategoryEntityUsingFilterCategoryID);
				FilterCategoryEntity newEntity = new FilterCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterCategoryID);
				}
				if(fetchResult)
				{
					newEntity = (FilterCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterCategory = newEntity;
				_alreadyFetchedFilterCategory = fetchResult;
			}
			return _filterCategory;
		}


		/// <summary> Retrieves the related entity of type 'FilterElementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterElementEntity' which is related to this entity.</returns>
		public FilterElementEntity GetSingleFilterElement()
		{
			return GetSingleFilterElement(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterElementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterElementEntity' which is related to this entity.</returns>
		public virtual FilterElementEntity GetSingleFilterElement(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterElement || forceFetch || _alwaysFetchFilterElement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterElementEntityUsingFilterElementID);
				FilterElementEntity newEntity = new FilterElementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterElementID);
				}
				if(fetchResult)
				{
					newEntity = (FilterElementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterElementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterElement = newEntity;
				_alreadyFetchedFilterElement = fetchResult;
			}
			return _filterElement;
		}


		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public FilterListEntity GetSingleFilterList()
		{
			return GetSingleFilterList(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public virtual FilterListEntity GetSingleFilterList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterList || forceFetch || _alwaysFetchFilterList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterListEntityUsingFilterListID);
				FilterListEntity newEntity = new FilterListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterListID);
				}
				if(fetchResult)
				{
					newEntity = (FilterListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterList = newEntity;
				_alreadyFetchedFilterList = fetchResult;
			}
			return _filterList;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FilterCategory", _filterCategory);
			toReturn.Add("FilterElement", _filterElement);
			toReturn.Add("FilterList", _filterList);
			toReturn.Add("FilterValues", _filterValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="validator">The validator object for this FilterListElementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 filterListElementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(filterListElementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_filterValues = new VarioSL.Entities.CollectionClasses.FilterValueCollection();
			_filterValues.SetContainingEntityInfo(this, "FilterListElement");
			_filterCategoryReturnsNewIfNotFound = false;
			_filterElementReturnsNewIfNotFound = false;
			_filterListReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterElementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListElementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsMandatory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsVisible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _filterCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterCategory, new PropertyChangedEventHandler( OnFilterCategoryPropertyChanged ), "FilterCategory", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterCategoryEntityUsingFilterCategoryIDStatic, true, signalRelatedEntity, "FilterListElements", resetFKFields, new int[] { (int)FilterListElementFieldIndex.FilterCategoryID } );		
			_filterCategory = null;
		}
		
		/// <summary> setups the sync logic for member _filterCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterCategory(IEntityCore relatedEntity)
		{
			if(_filterCategory!=relatedEntity)
			{		
				DesetupSyncFilterCategory(true, true);
				_filterCategory = (FilterCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterCategory, new PropertyChangedEventHandler( OnFilterCategoryPropertyChanged ), "FilterCategory", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterCategoryEntityUsingFilterCategoryIDStatic, true, ref _alreadyFetchedFilterCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterElement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterElement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterElement, new PropertyChangedEventHandler( OnFilterElementPropertyChanged ), "FilterElement", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterElementEntityUsingFilterElementIDStatic, true, signalRelatedEntity, "FilterListElements", resetFKFields, new int[] { (int)FilterListElementFieldIndex.FilterElementID } );		
			_filterElement = null;
		}
		
		/// <summary> setups the sync logic for member _filterElement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterElement(IEntityCore relatedEntity)
		{
			if(_filterElement!=relatedEntity)
			{		
				DesetupSyncFilterElement(true, true);
				_filterElement = (FilterElementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterElement, new PropertyChangedEventHandler( OnFilterElementPropertyChanged ), "FilterElement", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterElementEntityUsingFilterElementIDStatic, true, ref _alreadyFetchedFilterElement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterElementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterListEntityUsingFilterListIDStatic, true, signalRelatedEntity, "FilterListElements", resetFKFields, new int[] { (int)FilterListElementFieldIndex.FilterListID } );		
			_filterList = null;
		}
		
		/// <summary> setups the sync logic for member _filterList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterList(IEntityCore relatedEntity)
		{
			if(_filterList!=relatedEntity)
			{		
				DesetupSyncFilterList(true, true);
				_filterList = (FilterListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticFilterListElementRelations.FilterListEntityUsingFilterListIDStatic, true, ref _alreadyFetchedFilterList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="filterListElementID">PK value for FilterListElement which data should be fetched into this FilterListElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 filterListElementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FilterListElementFieldIndex.FilterListElementID].ForcedCurrentValueWrite(filterListElementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFilterListElementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FilterListElementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FilterListElementRelations Relations
		{
			get	{ return new FilterListElementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueCollection(), (IEntityRelation)GetRelationsForField("FilterValues")[0], (int)VarioSL.Entities.EntityType.FilterListElementEntity, (int)VarioSL.Entities.EntityType.FilterValueEntity, 0, null, null, null, "FilterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterCategoryCollection(), (IEntityRelation)GetRelationsForField("FilterCategory")[0], (int)VarioSL.Entities.EntityType.FilterListElementEntity, (int)VarioSL.Entities.EntityType.FilterCategoryEntity, 0, null, null, null, "FilterCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterElement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterElement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterElementCollection(), (IEntityRelation)GetRelationsForField("FilterElement")[0], (int)VarioSL.Entities.EntityType.FilterListElementEntity, (int)VarioSL.Entities.EntityType.FilterElementEntity, 0, null, null, null, "FilterElement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListCollection(), (IEntityRelation)GetRelationsForField("FilterList")[0], (int)VarioSL.Entities.EntityType.FilterListElementEntity, (int)VarioSL.Entities.EntityType.FilterListEntity, 0, null, null, null, "FilterList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DisplayName property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."DISPLAYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayName
		{
			get { return (System.String)GetValue((int)FilterListElementFieldIndex.DisplayName, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.DisplayName, value, true); }
		}

		/// <summary> The DisplayOrder property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."DISPLAYORDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayOrder
		{
			get { return (System.Int32)GetValue((int)FilterListElementFieldIndex.DisplayOrder, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.DisplayOrder, value, true); }
		}

		/// <summary> The FilterCategoryID property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."FILTERCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterCategoryID
		{
			get { return (System.Int64)GetValue((int)FilterListElementFieldIndex.FilterCategoryID, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.FilterCategoryID, value, true); }
		}

		/// <summary> The FilterElementID property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."FILTERELEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterElementID
		{
			get { return (System.Int64)GetValue((int)FilterListElementFieldIndex.FilterElementID, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.FilterElementID, value, true); }
		}

		/// <summary> The FilterListElementID property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."FILTERLISTELEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FilterListElementID
		{
			get { return (System.Int64)GetValue((int)FilterListElementFieldIndex.FilterListElementID, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.FilterListElementID, value, true); }
		}

		/// <summary> The FilterListID property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."FILTERLISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterListID
		{
			get { return (System.Int64)GetValue((int)FilterListElementFieldIndex.FilterListID, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.FilterListID, value, true); }
		}

		/// <summary> The IsActive property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)FilterListElementFieldIndex.IsActive, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.IsActive, value, true); }
		}

		/// <summary> The IsMandatory property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."ISMANDATORY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsMandatory
		{
			get { return (System.Boolean)GetValue((int)FilterListElementFieldIndex.IsMandatory, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.IsMandatory, value, true); }
		}

		/// <summary> The IsVisible property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."ISVISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsVisible
		{
			get { return (System.Boolean)GetValue((int)FilterListElementFieldIndex.IsVisible, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.IsVisible, value, true); }
		}

		/// <summary> The LastModified property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FilterListElementFieldIndex.LastModified, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FilterListElementFieldIndex.LastUser, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FilterListElement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLISTELEMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FilterListElementFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FilterListElementFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueCollection FilterValues
		{
			get	{ return GetMultiFilterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValues. When set to true, FilterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValues is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValues
		{
			get	{ return _alwaysFetchFilterValues; }
			set	{ _alwaysFetchFilterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValues already has been fetched. Setting this property to false when FilterValues has been fetched
		/// will clear the FilterValues collection well. Setting this property to true while FilterValues hasn't been fetched disables lazy loading for FilterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValues
		{
			get { return _alreadyFetchedFilterValues;}
			set 
			{
				if(_alreadyFetchedFilterValues && !value && (_filterValues != null))
				{
					_filterValues.Clear();
				}
				_alreadyFetchedFilterValues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'FilterCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterCategoryEntity FilterCategory
		{
			get	{ return GetSingleFilterCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterListElements", "FilterCategory", _filterCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterCategory. When set to true, FilterCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterCategory is accessed. You can always execute a forced fetch by calling GetSingleFilterCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterCategory
		{
			get	{ return _alwaysFetchFilterCategory; }
			set	{ _alwaysFetchFilterCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterCategory already has been fetched. Setting this property to false when FilterCategory has been fetched
		/// will set FilterCategory to null as well. Setting this property to true while FilterCategory hasn't been fetched disables lazy loading for FilterCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterCategory
		{
			get { return _alreadyFetchedFilterCategory;}
			set 
			{
				if(_alreadyFetchedFilterCategory && !value)
				{
					this.FilterCategory = null;
				}
				_alreadyFetchedFilterCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterCategory is not found
		/// in the database. When set to true, FilterCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterCategoryReturnsNewIfNotFound
		{
			get	{ return _filterCategoryReturnsNewIfNotFound; }
			set { _filterCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterElementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterElement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterElementEntity FilterElement
		{
			get	{ return GetSingleFilterElement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterElement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterListElements", "FilterElement", _filterElement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterElement. When set to true, FilterElement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterElement is accessed. You can always execute a forced fetch by calling GetSingleFilterElement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterElement
		{
			get	{ return _alwaysFetchFilterElement; }
			set	{ _alwaysFetchFilterElement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterElement already has been fetched. Setting this property to false when FilterElement has been fetched
		/// will set FilterElement to null as well. Setting this property to true while FilterElement hasn't been fetched disables lazy loading for FilterElement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterElement
		{
			get { return _alreadyFetchedFilterElement;}
			set 
			{
				if(_alreadyFetchedFilterElement && !value)
				{
					this.FilterElement = null;
				}
				_alreadyFetchedFilterElement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterElement is not found
		/// in the database. When set to true, FilterElement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterElementReturnsNewIfNotFound
		{
			get	{ return _filterElementReturnsNewIfNotFound; }
			set { _filterElementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterListEntity FilterList
		{
			get	{ return GetSingleFilterList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterListElements", "FilterList", _filterList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterList. When set to true, FilterList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterList is accessed. You can always execute a forced fetch by calling GetSingleFilterList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterList
		{
			get	{ return _alwaysFetchFilterList; }
			set	{ _alwaysFetchFilterList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterList already has been fetched. Setting this property to false when FilterList has been fetched
		/// will set FilterList to null as well. Setting this property to true while FilterList hasn't been fetched disables lazy loading for FilterList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterList
		{
			get { return _alreadyFetchedFilterList;}
			set 
			{
				if(_alreadyFetchedFilterList && !value)
				{
					this.FilterList = null;
				}
				_alreadyFetchedFilterList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterList is not found
		/// in the database. When set to true, FilterList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterListReturnsNewIfNotFound
		{
			get	{ return _filterListReturnsNewIfNotFound; }
			set { _filterListReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FilterListElementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
