﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardActionRequest'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardActionRequestEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardActionAttributeCollection	_cardActionAttributes;
		private bool	_alwaysFetchCardActionAttributes, _alreadyFetchedCardActionAttributes;
		private ProductOnCardEntity _productOnCard;
		private bool	_alwaysFetchProductOnCard, _alreadyFetchedProductOnCard, _productOnCardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardActionAttributes</summary>
			public static readonly string CardActionAttributes = "CardActionAttributes";
			/// <summary>Member name ProductOnCard</summary>
			public static readonly string ProductOnCard = "ProductOnCard";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardActionRequestEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardActionRequestEntity() :base("CardActionRequestEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		public CardActionRequestEntity(System.Int64 cardActionRequestID):base("CardActionRequestEntity")
		{
			InitClassFetch(cardActionRequestID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardActionRequestEntity(System.Int64 cardActionRequestID, IPrefetchPath prefetchPathToUse):base("CardActionRequestEntity")
		{
			InitClassFetch(cardActionRequestID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="validator">The custom validator object for this CardActionRequestEntity</param>
		public CardActionRequestEntity(System.Int64 cardActionRequestID, IValidator validator):base("CardActionRequestEntity")
		{
			InitClassFetch(cardActionRequestID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardActionRequestEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardActionAttributes = (VarioSL.Entities.CollectionClasses.CardActionAttributeCollection)info.GetValue("_cardActionAttributes", typeof(VarioSL.Entities.CollectionClasses.CardActionAttributeCollection));
			_alwaysFetchCardActionAttributes = info.GetBoolean("_alwaysFetchCardActionAttributes");
			_alreadyFetchedCardActionAttributes = info.GetBoolean("_alreadyFetchedCardActionAttributes");
			_productOnCard = (ProductOnCardEntity)info.GetValue("_productOnCard", typeof(ProductOnCardEntity));
			if(_productOnCard!=null)
			{
				_productOnCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productOnCardReturnsNewIfNotFound = info.GetBoolean("_productOnCardReturnsNewIfNotFound");
			_alwaysFetchProductOnCard = info.GetBoolean("_alwaysFetchProductOnCard");
			_alreadyFetchedProductOnCard = info.GetBoolean("_alreadyFetchedProductOnCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardActionAttributes = (_cardActionAttributes.Count > 0);
			_alreadyFetchedProductOnCard = (_productOnCard != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardActionAttributes":
					toReturn.Add(Relations.CardActionAttributeEntityUsingCardActionRequestID);
					break;
				case "ProductOnCard":
					toReturn.Add(Relations.ProductOnCardEntityUsingCardActionRequestID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardActionAttributes", (!this.MarkedForDeletion?_cardActionAttributes:null));
			info.AddValue("_alwaysFetchCardActionAttributes", _alwaysFetchCardActionAttributes);
			info.AddValue("_alreadyFetchedCardActionAttributes", _alreadyFetchedCardActionAttributes);

			info.AddValue("_productOnCard", (!this.MarkedForDeletion?_productOnCard:null));
			info.AddValue("_productOnCardReturnsNewIfNotFound", _productOnCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductOnCard", _alwaysFetchProductOnCard);
			info.AddValue("_alreadyFetchedProductOnCard", _alreadyFetchedProductOnCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardActionAttributes":
					_alreadyFetchedCardActionAttributes = true;
					if(entity!=null)
					{
						this.CardActionAttributes.Add((CardActionAttributeEntity)entity);
					}
					break;
				case "ProductOnCard":
					_alreadyFetchedProductOnCard = true;
					this.ProductOnCard = (ProductOnCardEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardActionAttributes":
					_cardActionAttributes.Add((CardActionAttributeEntity)relatedEntity);
					break;
				case "ProductOnCard":
					SetupSyncProductOnCard(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardActionAttributes":
					this.PerformRelatedEntityRemoval(_cardActionAttributes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductOnCard":
					DesetupSyncProductOnCard(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_productOnCard!=null)
			{
				toReturn.Add(_productOnCard);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardActionAttributes);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionRequestID)
		{
			return FetchUsingPK(cardActionRequestID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionRequestID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardActionRequestID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionRequestID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardActionRequestID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardActionRequestID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardActionRequestID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardActionRequestID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardActionRequestRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardActionAttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardActionAttributeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardActionAttributeCollection GetMultiCardActionAttributes(bool forceFetch)
		{
			return GetMultiCardActionAttributes(forceFetch, _cardActionAttributes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardActionAttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardActionAttributeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardActionAttributeCollection GetMultiCardActionAttributes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardActionAttributes(forceFetch, _cardActionAttributes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardActionAttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardActionAttributeCollection GetMultiCardActionAttributes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardActionAttributes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardActionAttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardActionAttributeCollection GetMultiCardActionAttributes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardActionAttributes || forceFetch || _alwaysFetchCardActionAttributes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardActionAttributes);
				_cardActionAttributes.SuppressClearInGetMulti=!forceFetch;
				_cardActionAttributes.EntityFactoryToUse = entityFactoryToUse;
				_cardActionAttributes.GetMultiManyToOne(this, filter);
				_cardActionAttributes.SuppressClearInGetMulti=false;
				_alreadyFetchedCardActionAttributes = true;
			}
			return _cardActionAttributes;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardActionAttributes'. These settings will be taken into account
		/// when the property CardActionAttributes is requested or GetMultiCardActionAttributes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardActionAttributes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardActionAttributes.SortClauses=sortClauses;
			_cardActionAttributes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ProductOnCardEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ProductOnCardEntity' which is related to this entity.</returns>
		public ProductOnCardEntity GetSingleProductOnCard()
		{
			return GetSingleProductOnCard(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ProductOnCardEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductOnCardEntity' which is related to this entity.</returns>
		public virtual ProductOnCardEntity GetSingleProductOnCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductOnCard || forceFetch || _alwaysFetchProductOnCard) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductOnCardEntityUsingCardActionRequestID);
				ProductOnCardEntity newEntity = new ProductOnCardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCCardActionRequestID(this.CardActionRequestID);
				}
				if(fetchResult)
				{
					newEntity = (ProductOnCardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productOnCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductOnCard = newEntity;
				_alreadyFetchedProductOnCard = fetchResult;
			}
			return _productOnCard;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardActionAttributes", _cardActionAttributes);
			toReturn.Add("ProductOnCard", _productOnCard);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="validator">The validator object for this CardActionRequestEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardActionRequestID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardActionRequestID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardActionAttributes = new VarioSL.Entities.CollectionClasses.CardActionAttributeCollection();
			_cardActionAttributes.SetContainingEntityInfo(this, "CardActionRequest");
			_productOnCardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardActionRequestID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargeValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProcessed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Transactionid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _productOnCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductOnCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productOnCard, new PropertyChangedEventHandler( OnProductOnCardPropertyChanged ), "ProductOnCard", VarioSL.Entities.RelationClasses.StaticCardActionRequestRelations.ProductOnCardEntityUsingCardActionRequestIDStatic, false, signalRelatedEntity, "CardActionRequest", false, new int[] { (int)CardActionRequestFieldIndex.CardActionRequestID } );
			_productOnCard = null;
		}
	
		/// <summary> setups the sync logic for member _productOnCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductOnCard(IEntityCore relatedEntity)
		{
			if(_productOnCard!=relatedEntity)
			{
				DesetupSyncProductOnCard(true, true);
				_productOnCard = (ProductOnCardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productOnCard, new PropertyChangedEventHandler( OnProductOnCardPropertyChanged ), "ProductOnCard", VarioSL.Entities.RelationClasses.StaticCardActionRequestRelations.ProductOnCardEntityUsingCardActionRequestIDStatic, false, ref _alreadyFetchedProductOnCard, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductOnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardActionRequestID">PK value for CardActionRequest which data should be fetched into this CardActionRequest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardActionRequestID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardActionRequestFieldIndex.CardActionRequestID].ForcedCurrentValueWrite(cardActionRequestID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardActionRequestDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardActionRequestEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardActionRequestRelations Relations
		{
			get	{ return new CardActionRequestRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardActionAttribute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardActionAttributes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardActionAttributeCollection(), (IEntityRelation)GetRelationsForField("CardActionAttributes")[0], (int)VarioSL.Entities.EntityType.CardActionRequestEntity, (int)VarioSL.Entities.EntityType.CardActionAttributeEntity, 0, null, null, null, "CardActionAttributes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductOnCard'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductOnCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductOnCardCollection(), (IEntityRelation)GetRelationsForField("ProductOnCard")[0], (int)VarioSL.Entities.EntityType.CardActionRequestEntity, (int)VarioSL.Entities.EntityType.ProductOnCardEntity, 0, null, null, null, "ProductOnCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardActionRequestID property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardActionRequestID
		{
			get { return (System.Int64)GetValue((int)CardActionRequestFieldIndex.CardActionRequestID, true); }
			set	{ SetValue((int)CardActionRequestFieldIndex.CardActionRequestID, value, true); }
		}

		/// <summary> The CardNo property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."CARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.CardNo, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.CardNo, value, true); }
		}

		/// <summary> The ChargeValue property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."CHARGEVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ChargeValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.ChargeValue, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ChargeValue, value, true); }
		}

		/// <summary> The ClearingID property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."CLEARINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClearingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.ClearingID, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ClearingID, value, true); }
		}

		/// <summary> The ClientID property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.ClientID, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionRequestFieldIndex.CreationDate, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The IsProcessed property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."ISPROCESSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsProcessed
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CardActionRequestFieldIndex.IsProcessed, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.IsProcessed, value, true); }
		}

		/// <summary> The IsValid property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."VALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsValid
		{
			get { return (System.Boolean)GetValue((int)CardActionRequestFieldIndex.IsValid, true); }
			set	{ SetValue((int)CardActionRequestFieldIndex.IsValid, value, true); }
		}

		/// <summary> The ProcessDate property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."PROCESSDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProcessDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionRequestFieldIndex.ProcessDate, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ProcessDate, value, true); }
		}

		/// <summary> The RequestNo property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."REQUESTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RequestNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.RequestNo, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.RequestNo, value, true); }
		}

		/// <summary> The RequestSource property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."REQUESTSOURCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RequestSource
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.RequestSource, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.RequestSource, value, true); }
		}

		/// <summary> The RequestType property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."REQUESTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RequestType
		{
			get { return (Nullable<System.Int32>)GetValue((int)CardActionRequestFieldIndex.RequestType, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.RequestType, value, true); }
		}

		/// <summary> The Transactionid property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Transactionid
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardActionRequestFieldIndex.Transactionid, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.Transactionid, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionRequestFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity CardActionRequest<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PP_CARDACTIONREQUEST"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardActionRequestFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)CardActionRequestFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardActionAttributeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardActionAttributes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardActionAttributeCollection CardActionAttributes
		{
			get	{ return GetMultiCardActionAttributes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardActionAttributes. When set to true, CardActionAttributes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardActionAttributes is accessed. You can always execute/ a forced fetch by calling GetMultiCardActionAttributes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardActionAttributes
		{
			get	{ return _alwaysFetchCardActionAttributes; }
			set	{ _alwaysFetchCardActionAttributes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardActionAttributes already has been fetched. Setting this property to false when CardActionAttributes has been fetched
		/// will clear the CardActionAttributes collection well. Setting this property to true while CardActionAttributes hasn't been fetched disables lazy loading for CardActionAttributes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardActionAttributes
		{
			get { return _alreadyFetchedCardActionAttributes;}
			set 
			{
				if(_alreadyFetchedCardActionAttributes && !value && (_cardActionAttributes != null))
				{
					_cardActionAttributes.Clear();
				}
				_alreadyFetchedCardActionAttributes = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ProductOnCardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductOnCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductOnCardEntity ProductOnCard
		{
			get	{ return GetSingleProductOnCard(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncProductOnCard(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_productOnCard !=null);
						DesetupSyncProductOnCard(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ProductOnCard");
						}
					}
					else
					{
						if(_productOnCard!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CardActionRequest");
							SetupSyncProductOnCard(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductOnCard. When set to true, ProductOnCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductOnCard is accessed. You can always execute a forced fetch by calling GetSingleProductOnCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductOnCard
		{
			get	{ return _alwaysFetchProductOnCard; }
			set	{ _alwaysFetchProductOnCard = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ProductOnCard already has been fetched. Setting this property to false when ProductOnCard has been fetched
		/// will set ProductOnCard to null as well. Setting this property to true while ProductOnCard hasn't been fetched disables lazy loading for ProductOnCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductOnCard
		{
			get { return _alreadyFetchedProductOnCard;}
			set 
			{
				if(_alreadyFetchedProductOnCard && !value)
				{
					this.ProductOnCard = null;
				}
				_alreadyFetchedProductOnCard = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductOnCard is not found
		/// in the database. When set to true, ProductOnCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductOnCardReturnsNewIfNotFound
		{
			get	{ return _productOnCardReturnsNewIfNotFound; }
			set	{ _productOnCardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardActionRequestEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
