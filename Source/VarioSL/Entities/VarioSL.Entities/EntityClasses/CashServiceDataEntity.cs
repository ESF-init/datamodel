﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CashServiceData'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CashServiceDataEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CashServiceEntity _cashService;
		private bool	_alwaysFetchCashService, _alreadyFetchedCashService, _cashServiceReturnsNewIfNotFound;
		private CashUnitEntity _cashUnit;
		private bool	_alwaysFetchCashUnit, _alreadyFetchedCashUnit, _cashUnitReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CashService</summary>
			public static readonly string CashService = "CashService";
			/// <summary>Member name CashUnit</summary>
			public static readonly string CashUnit = "CashUnit";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CashServiceDataEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CashServiceDataEntity() :base("CashServiceDataEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		public CashServiceDataEntity(System.Int64 cashServiceDataID):base("CashServiceDataEntity")
		{
			InitClassFetch(cashServiceDataID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CashServiceDataEntity(System.Int64 cashServiceDataID, IPrefetchPath prefetchPathToUse):base("CashServiceDataEntity")
		{
			InitClassFetch(cashServiceDataID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="validator">The custom validator object for this CashServiceDataEntity</param>
		public CashServiceDataEntity(System.Int64 cashServiceDataID, IValidator validator):base("CashServiceDataEntity")
		{
			InitClassFetch(cashServiceDataID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CashServiceDataEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cashService = (CashServiceEntity)info.GetValue("_cashService", typeof(CashServiceEntity));
			if(_cashService!=null)
			{
				_cashService.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cashServiceReturnsNewIfNotFound = info.GetBoolean("_cashServiceReturnsNewIfNotFound");
			_alwaysFetchCashService = info.GetBoolean("_alwaysFetchCashService");
			_alreadyFetchedCashService = info.GetBoolean("_alreadyFetchedCashService");

			_cashUnit = (CashUnitEntity)info.GetValue("_cashUnit", typeof(CashUnitEntity));
			if(_cashUnit!=null)
			{
				_cashUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cashUnitReturnsNewIfNotFound = info.GetBoolean("_cashUnitReturnsNewIfNotFound");
			_alwaysFetchCashUnit = info.GetBoolean("_alwaysFetchCashUnit");
			_alreadyFetchedCashUnit = info.GetBoolean("_alreadyFetchedCashUnit");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CashServiceDataFieldIndex)fieldIndex)
			{
				case CashServiceDataFieldIndex.CashServiceID:
					DesetupSyncCashService(true, false);
					_alreadyFetchedCashService = false;
					break;
				case CashServiceDataFieldIndex.CashUnitID:
					DesetupSyncCashUnit(true, false);
					_alreadyFetchedCashUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCashService = (_cashService != null);
			_alreadyFetchedCashUnit = (_cashUnit != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CashService":
					toReturn.Add(Relations.CashServiceEntityUsingCashServiceID);
					break;
				case "CashUnit":
					toReturn.Add(Relations.CashUnitEntityUsingCashUnitID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cashService", (!this.MarkedForDeletion?_cashService:null));
			info.AddValue("_cashServiceReturnsNewIfNotFound", _cashServiceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCashService", _alwaysFetchCashService);
			info.AddValue("_alreadyFetchedCashService", _alreadyFetchedCashService);
			info.AddValue("_cashUnit", (!this.MarkedForDeletion?_cashUnit:null));
			info.AddValue("_cashUnitReturnsNewIfNotFound", _cashUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCashUnit", _alwaysFetchCashUnit);
			info.AddValue("_alreadyFetchedCashUnit", _alreadyFetchedCashUnit);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CashService":
					_alreadyFetchedCashService = true;
					this.CashService = (CashServiceEntity)entity;
					break;
				case "CashUnit":
					_alreadyFetchedCashUnit = true;
					this.CashUnit = (CashUnitEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CashService":
					SetupSyncCashService(relatedEntity);
					break;
				case "CashUnit":
					SetupSyncCashUnit(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CashService":
					DesetupSyncCashService(false, true);
					break;
				case "CashUnit":
					DesetupSyncCashUnit(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cashService!=null)
			{
				toReturn.Add(_cashService);
			}
			if(_cashUnit!=null)
			{
				toReturn.Add(_cashUnit);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceDataID)
		{
			return FetchUsingPK(cashServiceDataID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceDataID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cashServiceDataID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cashServiceDataID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cashServiceDataID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CashServiceDataID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CashServiceDataRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CashServiceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CashServiceEntity' which is related to this entity.</returns>
		public CashServiceEntity GetSingleCashService()
		{
			return GetSingleCashService(false);
		}

		/// <summary> Retrieves the related entity of type 'CashServiceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CashServiceEntity' which is related to this entity.</returns>
		public virtual CashServiceEntity GetSingleCashService(bool forceFetch)
		{
			if( ( !_alreadyFetchedCashService || forceFetch || _alwaysFetchCashService) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CashServiceEntityUsingCashServiceID);
				CashServiceEntity newEntity = new CashServiceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CashServiceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CashServiceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cashServiceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CashService = newEntity;
				_alreadyFetchedCashService = fetchResult;
			}
			return _cashService;
		}


		/// <summary> Retrieves the related entity of type 'CashUnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CashUnitEntity' which is related to this entity.</returns>
		public CashUnitEntity GetSingleCashUnit()
		{
			return GetSingleCashUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'CashUnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CashUnitEntity' which is related to this entity.</returns>
		public virtual CashUnitEntity GetSingleCashUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedCashUnit || forceFetch || _alwaysFetchCashUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CashUnitEntityUsingCashUnitID);
				CashUnitEntity newEntity = new CashUnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CashUnitID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CashUnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cashUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CashUnit = newEntity;
				_alreadyFetchedCashUnit = fetchResult;
			}
			return _cashUnit;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CashService", _cashService);
			toReturn.Add("CashUnit", _cashUnit);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="validator">The validator object for this CashServiceDataEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cashServiceDataID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cashServiceDataID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cashServiceReturnsNewIfNotFound = false;
			_cashUnitReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashDebit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashServiceDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChangeReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentStateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrentValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Slot", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueDate", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cashService</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCashService(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cashService, new PropertyChangedEventHandler( OnCashServicePropertyChanged ), "CashService", VarioSL.Entities.RelationClasses.StaticCashServiceDataRelations.CashServiceEntityUsingCashServiceIDStatic, true, signalRelatedEntity, "AmCashservicedatas", resetFKFields, new int[] { (int)CashServiceDataFieldIndex.CashServiceID } );		
			_cashService = null;
		}
		
		/// <summary> setups the sync logic for member _cashService</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCashService(IEntityCore relatedEntity)
		{
			if(_cashService!=relatedEntity)
			{		
				DesetupSyncCashService(true, true);
				_cashService = (CashServiceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cashService, new PropertyChangedEventHandler( OnCashServicePropertyChanged ), "CashService", VarioSL.Entities.RelationClasses.StaticCashServiceDataRelations.CashServiceEntityUsingCashServiceIDStatic, true, ref _alreadyFetchedCashService, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCashServicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cashUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCashUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cashUnit, new PropertyChangedEventHandler( OnCashUnitPropertyChanged ), "CashUnit", VarioSL.Entities.RelationClasses.StaticCashServiceDataRelations.CashUnitEntityUsingCashUnitIDStatic, true, signalRelatedEntity, "AmCashservicedatas", resetFKFields, new int[] { (int)CashServiceDataFieldIndex.CashUnitID } );		
			_cashUnit = null;
		}
		
		/// <summary> setups the sync logic for member _cashUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCashUnit(IEntityCore relatedEntity)
		{
			if(_cashUnit!=relatedEntity)
			{		
				DesetupSyncCashUnit(true, true);
				_cashUnit = (CashUnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cashUnit, new PropertyChangedEventHandler( OnCashUnitPropertyChanged ), "CashUnit", VarioSL.Entities.RelationClasses.StaticCashServiceDataRelations.CashUnitEntityUsingCashUnitIDStatic, true, ref _alreadyFetchedCashUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCashUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cashServiceDataID">PK value for CashServiceData which data should be fetched into this CashServiceData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cashServiceDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CashServiceDataFieldIndex.CashServiceDataID].ForcedCurrentValueWrite(cashServiceDataID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCashServiceDataDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CashServiceDataEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CashServiceDataRelations Relations
		{
			get	{ return new CashServiceDataRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashService'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCashService
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceCollection(), (IEntityRelation)GetRelationsForField("CashService")[0], (int)VarioSL.Entities.EntityType.CashServiceDataEntity, (int)VarioSL.Entities.EntityType.CashServiceEntity, 0, null, null, null, "CashService", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashUnit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCashUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashUnitCollection(), (IEntityRelation)GetRelationsForField("CashUnit")[0], (int)VarioSL.Entities.EntityType.CashServiceDataEntity, (int)VarioSL.Entities.EntityType.CashUnitEntity, 0, null, null, null, "CashUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CashCredit property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CASHCREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashCredit
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.CashCredit, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CashCredit, value, true); }
		}

		/// <summary> The CashDebit property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CASHDEBIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashDebit
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.CashDebit, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CashDebit, value, true); }
		}

		/// <summary> The CashServiceDataID property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CASHSERVICEDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CashServiceDataID
		{
			get { return (System.Int64)GetValue((int)CashServiceDataFieldIndex.CashServiceDataID, true); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CashServiceDataID, value, true); }
		}

		/// <summary> The CashServiceID property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CASHSERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashServiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.CashServiceID, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CashServiceID, value, true); }
		}

		/// <summary> The CashUnitID property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CASHUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashUnitID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.CashUnitID, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CashUnitID, value, true); }
		}

		/// <summary> The ChangeReason property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CHANGEREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ChangeReason> ChangeReason
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ChangeReason>)GetValue((int)CashServiceDataFieldIndex.ChangeReason, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.ChangeReason, value, true); }
		}

		/// <summary> The ComponentNumber property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."COMPONENTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 14, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.ComponentNumber, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.ComponentNumber, value, true); }
		}

		/// <summary> The ComponentStateID property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."COMPONENTSTATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentStateID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.ComponentStateID, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.ComponentStateID, value, true); }
		}

		/// <summary> The ComponentTypeID property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."COMPONENTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.ComponentTypeID, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.ComponentTypeID, value, true); }
		}

		/// <summary> The CurrentValue property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."CURRENTVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CurrentValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.CurrentValue, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.CurrentValue, value, true); }
		}

		/// <summary> The Slot property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."SLOT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Slot
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceDataFieldIndex.Slot, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.Slot, value, true); }
		}

		/// <summary> The ValueDate property of the Entity CashServiceData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICEDATA"."VALUEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValueDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CashServiceDataFieldIndex.ValueDate, false); }
			set	{ SetValue((int)CashServiceDataFieldIndex.ValueDate, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CashServiceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCashService()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CashServiceEntity CashService
		{
			get	{ return GetSingleCashService(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCashService(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AmCashservicedatas", "CashService", _cashService, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CashService. When set to true, CashService is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CashService is accessed. You can always execute a forced fetch by calling GetSingleCashService(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCashService
		{
			get	{ return _alwaysFetchCashService; }
			set	{ _alwaysFetchCashService = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CashService already has been fetched. Setting this property to false when CashService has been fetched
		/// will set CashService to null as well. Setting this property to true while CashService hasn't been fetched disables lazy loading for CashService</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCashService
		{
			get { return _alreadyFetchedCashService;}
			set 
			{
				if(_alreadyFetchedCashService && !value)
				{
					this.CashService = null;
				}
				_alreadyFetchedCashService = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CashService is not found
		/// in the database. When set to true, CashService will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CashServiceReturnsNewIfNotFound
		{
			get	{ return _cashServiceReturnsNewIfNotFound; }
			set { _cashServiceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CashUnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCashUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CashUnitEntity CashUnit
		{
			get	{ return GetSingleCashUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCashUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AmCashservicedatas", "CashUnit", _cashUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CashUnit. When set to true, CashUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CashUnit is accessed. You can always execute a forced fetch by calling GetSingleCashUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCashUnit
		{
			get	{ return _alwaysFetchCashUnit; }
			set	{ _alwaysFetchCashUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CashUnit already has been fetched. Setting this property to false when CashUnit has been fetched
		/// will set CashUnit to null as well. Setting this property to true while CashUnit hasn't been fetched disables lazy loading for CashUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCashUnit
		{
			get { return _alreadyFetchedCashUnit;}
			set 
			{
				if(_alreadyFetchedCashUnit && !value)
				{
					this.CashUnit = null;
				}
				_alreadyFetchedCashUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CashUnit is not found
		/// in the database. When set to true, CashUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CashUnitReturnsNewIfNotFound
		{
			get	{ return _cashUnitReturnsNewIfNotFound; }
			set { _cashUnitReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CashServiceDataEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
