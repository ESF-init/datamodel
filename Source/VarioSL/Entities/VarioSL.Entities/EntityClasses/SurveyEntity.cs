﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Survey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SurveyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection	_surveyDescriptions;
		private bool	_alwaysFetchSurveyDescriptions, _alreadyFetchedSurveyDescriptions;
		private VarioSL.Entities.CollectionClasses.SurveyQuestionCollection	_surveyQuestions;
		private bool	_alwaysFetchSurveyQuestions, _alreadyFetchedSurveyQuestions;
		private VarioSL.Entities.CollectionClasses.SurveyResponseCollection	_surveyResponses;
		private bool	_alwaysFetchSurveyResponses, _alreadyFetchedSurveyResponses;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SurveyDescriptions</summary>
			public static readonly string SurveyDescriptions = "SurveyDescriptions";
			/// <summary>Member name SurveyQuestions</summary>
			public static readonly string SurveyQuestions = "SurveyQuestions";
			/// <summary>Member name SurveyResponses</summary>
			public static readonly string SurveyResponses = "SurveyResponses";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyEntity() :base("SurveyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		public SurveyEntity(System.Int64 surveyID):base("SurveyEntity")
		{
			InitClassFetch(surveyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyEntity(System.Int64 surveyID, IPrefetchPath prefetchPathToUse):base("SurveyEntity")
		{
			InitClassFetch(surveyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="validator">The custom validator object for this SurveyEntity</param>
		public SurveyEntity(System.Int64 surveyID, IValidator validator):base("SurveyEntity")
		{
			InitClassFetch(surveyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyDescriptions = (VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection)info.GetValue("_surveyDescriptions", typeof(VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection));
			_alwaysFetchSurveyDescriptions = info.GetBoolean("_alwaysFetchSurveyDescriptions");
			_alreadyFetchedSurveyDescriptions = info.GetBoolean("_alreadyFetchedSurveyDescriptions");

			_surveyQuestions = (VarioSL.Entities.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestions", typeof(VarioSL.Entities.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestions = info.GetBoolean("_alwaysFetchSurveyQuestions");
			_alreadyFetchedSurveyQuestions = info.GetBoolean("_alreadyFetchedSurveyQuestions");

			_surveyResponses = (VarioSL.Entities.CollectionClasses.SurveyResponseCollection)info.GetValue("_surveyResponses", typeof(VarioSL.Entities.CollectionClasses.SurveyResponseCollection));
			_alwaysFetchSurveyResponses = info.GetBoolean("_alwaysFetchSurveyResponses");
			_alreadyFetchedSurveyResponses = info.GetBoolean("_alreadyFetchedSurveyResponses");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyDescriptions = (_surveyDescriptions.Count > 0);
			_alreadyFetchedSurveyQuestions = (_surveyQuestions.Count > 0);
			_alreadyFetchedSurveyResponses = (_surveyResponses.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SurveyDescriptions":
					toReturn.Add(Relations.SurveyDescriptionEntityUsingSurveyID);
					break;
				case "SurveyQuestions":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyID);
					break;
				case "SurveyResponses":
					toReturn.Add(Relations.SurveyResponseEntityUsingSurveyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyDescriptions", (!this.MarkedForDeletion?_surveyDescriptions:null));
			info.AddValue("_alwaysFetchSurveyDescriptions", _alwaysFetchSurveyDescriptions);
			info.AddValue("_alreadyFetchedSurveyDescriptions", _alreadyFetchedSurveyDescriptions);
			info.AddValue("_surveyQuestions", (!this.MarkedForDeletion?_surveyQuestions:null));
			info.AddValue("_alwaysFetchSurveyQuestions", _alwaysFetchSurveyQuestions);
			info.AddValue("_alreadyFetchedSurveyQuestions", _alreadyFetchedSurveyQuestions);
			info.AddValue("_surveyResponses", (!this.MarkedForDeletion?_surveyResponses:null));
			info.AddValue("_alwaysFetchSurveyResponses", _alwaysFetchSurveyResponses);
			info.AddValue("_alreadyFetchedSurveyResponses", _alreadyFetchedSurveyResponses);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SurveyDescriptions":
					_alreadyFetchedSurveyDescriptions = true;
					if(entity!=null)
					{
						this.SurveyDescriptions.Add((SurveyDescriptionEntity)entity);
					}
					break;
				case "SurveyQuestions":
					_alreadyFetchedSurveyQuestions = true;
					if(entity!=null)
					{
						this.SurveyQuestions.Add((SurveyQuestionEntity)entity);
					}
					break;
				case "SurveyResponses":
					_alreadyFetchedSurveyResponses = true;
					if(entity!=null)
					{
						this.SurveyResponses.Add((SurveyResponseEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SurveyDescriptions":
					_surveyDescriptions.Add((SurveyDescriptionEntity)relatedEntity);
					break;
				case "SurveyQuestions":
					_surveyQuestions.Add((SurveyQuestionEntity)relatedEntity);
					break;
				case "SurveyResponses":
					_surveyResponses.Add((SurveyResponseEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SurveyDescriptions":
					this.PerformRelatedEntityRemoval(_surveyDescriptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyQuestions":
					this.PerformRelatedEntityRemoval(_surveyQuestions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResponses":
					this.PerformRelatedEntityRemoval(_surveyResponses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyDescriptions);
			toReturn.Add(_surveyQuestions);
			toReturn.Add(_surveyResponses);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyID)
		{
			return FetchUsingPK(surveyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyDescriptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyDescriptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection GetMultiSurveyDescriptions(bool forceFetch)
		{
			return GetMultiSurveyDescriptions(forceFetch, _surveyDescriptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyDescriptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyDescriptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection GetMultiSurveyDescriptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyDescriptions(forceFetch, _surveyDescriptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyDescriptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection GetMultiSurveyDescriptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyDescriptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyDescriptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection GetMultiSurveyDescriptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyDescriptions || forceFetch || _alwaysFetchSurveyDescriptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyDescriptions);
				_surveyDescriptions.SuppressClearInGetMulti=!forceFetch;
				_surveyDescriptions.EntityFactoryToUse = entityFactoryToUse;
				_surveyDescriptions.GetMultiManyToOne(this, filter);
				_surveyDescriptions.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyDescriptions = true;
			}
			return _surveyDescriptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyDescriptions'. These settings will be taken into account
		/// when the property SurveyDescriptions is requested or GetMultiSurveyDescriptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyDescriptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyDescriptions.SortClauses=sortClauses;
			_surveyDescriptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestions(bool forceFetch)
		{
			return GetMultiSurveyQuestions(forceFetch, _surveyQuestions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyQuestions(forceFetch, _surveyQuestions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyQuestions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyQuestions || forceFetch || _alwaysFetchSurveyQuestions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestions);
				_surveyQuestions.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestions.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestions.GetMultiManyToOne(this, filter);
				_surveyQuestions.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestions = true;
			}
			return _surveyQuestions;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestions'. These settings will be taken into account
		/// when the property SurveyQuestions is requested or GetMultiSurveyQuestions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestions.SortClauses=sortClauses;
			_surveyQuestions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch)
		{
			return GetMultiSurveyResponses(forceFetch, _surveyResponses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResponses(forceFetch, _surveyResponses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResponses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResponses || forceFetch || _alwaysFetchSurveyResponses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResponses);
				_surveyResponses.SuppressClearInGetMulti=!forceFetch;
				_surveyResponses.EntityFactoryToUse = entityFactoryToUse;
				_surveyResponses.GetMultiManyToOne(null, this, filter);
				_surveyResponses.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResponses = true;
			}
			return _surveyResponses;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResponses'. These settings will be taken into account
		/// when the property SurveyResponses is requested or GetMultiSurveyResponses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResponses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResponses.SortClauses=sortClauses;
			_surveyResponses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SurveyDescriptions", _surveyDescriptions);
			toReturn.Add("SurveyQuestions", _surveyQuestions);
			toReturn.Add("SurveyResponses", _surveyResponses);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="validator">The validator object for this SurveyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 surveyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyDescriptions = new VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection();
			_surveyDescriptions.SetContainingEntityInfo(this, "Survey");

			_surveyQuestions = new VarioSL.Entities.CollectionClasses.SurveyQuestionCollection();
			_surveyQuestions.SetContainingEntityInfo(this, "Survey");

			_surveyResponses = new VarioSL.Entities.CollectionClasses.SurveyResponseCollection();
			_surveyResponses.SetContainingEntityInfo(this, "Survey");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyID">PK value for Survey which data should be fetched into this Survey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyFieldIndex.SurveyID].ForcedCurrentValueWrite(surveyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyRelations Relations
		{
			get	{ return new SurveyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyDescription' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyDescriptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection(), (IEntityRelation)GetRelationsForField("SurveyDescriptions")[0], (int)VarioSL.Entities.EntityType.SurveyEntity, (int)VarioSL.Entities.EntityType.SurveyDescriptionEntity, 0, null, null, null, "SurveyDescriptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestions")[0], (int)VarioSL.Entities.EntityType.SurveyEntity, (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResponse' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResponses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyResponseCollection(), (IEntityRelation)GetRelationsForField("SurveyResponses")[0], (int)VarioSL.Entities.EntityType.SurveyEntity, (int)VarioSL.Entities.EntityType.SurveyResponseEntity, 0, null, null, null, "SurveyResponses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SurveyFieldIndex.LastModified, true); }
			set	{ SetValue((int)SurveyFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SurveyFieldIndex.LastUser, true); }
			set	{ SetValue((int)SurveyFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SurveyID property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."SURVEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SurveyID
		{
			get { return (System.Int64)GetValue((int)SurveyFieldIndex.SurveyID, true); }
			set	{ SetValue((int)SurveyFieldIndex.SurveyID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SurveyFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SurveyFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)SurveyFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)SurveyFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Survey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEY"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)SurveyFieldIndex.ValidTo, true); }
			set	{ SetValue((int)SurveyFieldIndex.ValidTo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyDescriptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyDescriptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyDescriptionCollection SurveyDescriptions
		{
			get	{ return GetMultiSurveyDescriptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyDescriptions. When set to true, SurveyDescriptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyDescriptions is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyDescriptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyDescriptions
		{
			get	{ return _alwaysFetchSurveyDescriptions; }
			set	{ _alwaysFetchSurveyDescriptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyDescriptions already has been fetched. Setting this property to false when SurveyDescriptions has been fetched
		/// will clear the SurveyDescriptions collection well. Setting this property to true while SurveyDescriptions hasn't been fetched disables lazy loading for SurveyDescriptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyDescriptions
		{
			get { return _alreadyFetchedSurveyDescriptions;}
			set 
			{
				if(_alreadyFetchedSurveyDescriptions && !value && (_surveyDescriptions != null))
				{
					_surveyDescriptions.Clear();
				}
				_alreadyFetchedSurveyDescriptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyQuestionCollection SurveyQuestions
		{
			get	{ return GetMultiSurveyQuestions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestions. When set to true, SurveyQuestions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestions is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyQuestions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestions
		{
			get	{ return _alwaysFetchSurveyQuestions; }
			set	{ _alwaysFetchSurveyQuestions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestions already has been fetched. Setting this property to false when SurveyQuestions has been fetched
		/// will clear the SurveyQuestions collection well. Setting this property to true while SurveyQuestions hasn't been fetched disables lazy loading for SurveyQuestions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestions
		{
			get { return _alreadyFetchedSurveyQuestions;}
			set 
			{
				if(_alreadyFetchedSurveyQuestions && !value && (_surveyQuestions != null))
				{
					_surveyQuestions.Clear();
				}
				_alreadyFetchedSurveyQuestions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResponses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyResponseCollection SurveyResponses
		{
			get	{ return GetMultiSurveyResponses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResponses. When set to true, SurveyResponses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResponses is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResponses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResponses
		{
			get	{ return _alwaysFetchSurveyResponses; }
			set	{ _alwaysFetchSurveyResponses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResponses already has been fetched. Setting this property to false when SurveyResponses has been fetched
		/// will clear the SurveyResponses collection well. Setting this property to true while SurveyResponses hasn't been fetched disables lazy loading for SurveyResponses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResponses
		{
			get { return _alreadyFetchedSurveyResponses;}
			set 
			{
				if(_alreadyFetchedSurveyResponses && !value && (_surveyResponses != null))
				{
					_surveyResponses.Clear();
				}
				_alreadyFetchedSurveyResponses = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SurveyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
