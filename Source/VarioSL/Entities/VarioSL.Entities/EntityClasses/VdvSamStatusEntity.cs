﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvSamStatus'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvSamStatusEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VdvTerminalEntity _vdvTerminal;
		private bool	_alwaysFetchVdvTerminal, _alreadyFetchedVdvTerminal, _vdvTerminalReturnsNewIfNotFound;
		private SamModuleEntity _samModule;
		private bool	_alwaysFetchSamModule, _alreadyFetchedSamModule, _samModuleReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name VdvTerminal</summary>
			public static readonly string VdvTerminal = "VdvTerminal";
			/// <summary>Member name SamModule</summary>
			public static readonly string SamModule = "SamModule";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvSamStatusEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvSamStatusEntity() :base("VdvSamStatusEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		public VdvSamStatusEntity(System.Int64 statusID):base("VdvSamStatusEntity")
		{
			InitClassFetch(statusID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvSamStatusEntity(System.Int64 statusID, IPrefetchPath prefetchPathToUse):base("VdvSamStatusEntity")
		{
			InitClassFetch(statusID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="validator">The custom validator object for this VdvSamStatusEntity</param>
		public VdvSamStatusEntity(System.Int64 statusID, IValidator validator):base("VdvSamStatusEntity")
		{
			InitClassFetch(statusID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvSamStatusEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_vdvTerminal = (VdvTerminalEntity)info.GetValue("_vdvTerminal", typeof(VdvTerminalEntity));
			if(_vdvTerminal!=null)
			{
				_vdvTerminal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vdvTerminalReturnsNewIfNotFound = info.GetBoolean("_vdvTerminalReturnsNewIfNotFound");
			_alwaysFetchVdvTerminal = info.GetBoolean("_alwaysFetchVdvTerminal");
			_alreadyFetchedVdvTerminal = info.GetBoolean("_alreadyFetchedVdvTerminal");

			_samModule = (SamModuleEntity)info.GetValue("_samModule", typeof(SamModuleEntity));
			if(_samModule!=null)
			{
				_samModule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_samModuleReturnsNewIfNotFound = info.GetBoolean("_samModuleReturnsNewIfNotFound");
			_alwaysFetchSamModule = info.GetBoolean("_alwaysFetchSamModule");
			_alreadyFetchedSamModule = info.GetBoolean("_alreadyFetchedSamModule");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VdvSamStatusFieldIndex)fieldIndex)
			{
				case VdvSamStatusFieldIndex.SamID:
					DesetupSyncSamModule(true, false);
					_alreadyFetchedSamModule = false;
					break;
				case VdvSamStatusFieldIndex.TerminalID:
					DesetupSyncVdvTerminal(true, false);
					_alreadyFetchedVdvTerminal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedVdvTerminal = (_vdvTerminal != null);
			_alreadyFetchedSamModule = (_samModule != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "VdvTerminal":
					toReturn.Add(Relations.VdvTerminalEntityUsingTerminalID);
					break;
				case "SamModule":
					toReturn.Add(Relations.SamModuleEntityUsingSamID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_vdvTerminal", (!this.MarkedForDeletion?_vdvTerminal:null));
			info.AddValue("_vdvTerminalReturnsNewIfNotFound", _vdvTerminalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVdvTerminal", _alwaysFetchVdvTerminal);
			info.AddValue("_alreadyFetchedVdvTerminal", _alreadyFetchedVdvTerminal);
			info.AddValue("_samModule", (!this.MarkedForDeletion?_samModule:null));
			info.AddValue("_samModuleReturnsNewIfNotFound", _samModuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSamModule", _alwaysFetchSamModule);
			info.AddValue("_alreadyFetchedSamModule", _alreadyFetchedSamModule);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "VdvTerminal":
					_alreadyFetchedVdvTerminal = true;
					this.VdvTerminal = (VdvTerminalEntity)entity;
					break;
				case "SamModule":
					_alreadyFetchedSamModule = true;
					this.SamModule = (SamModuleEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "VdvTerminal":
					SetupSyncVdvTerminal(relatedEntity);
					break;
				case "SamModule":
					SetupSyncSamModule(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "VdvTerminal":
					DesetupSyncVdvTerminal(false, true);
					break;
				case "SamModule":
					DesetupSyncSamModule(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_vdvTerminal!=null)
			{
				toReturn.Add(_vdvTerminal);
			}
			if(_samModule!=null)
			{
				toReturn.Add(_samModule);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 statusID)
		{
			return FetchUsingPK(statusID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 statusID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(statusID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 statusID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(statusID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 statusID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(statusID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.StatusID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvSamStatusRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'VdvTerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvTerminalEntity' which is related to this entity.</returns>
		public VdvTerminalEntity GetSingleVdvTerminal()
		{
			return GetSingleVdvTerminal(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvTerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvTerminalEntity' which is related to this entity.</returns>
		public virtual VdvTerminalEntity GetSingleVdvTerminal(bool forceFetch)
		{
			if( ( !_alreadyFetchedVdvTerminal || forceFetch || _alwaysFetchVdvTerminal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvTerminalEntityUsingTerminalID);
				VdvTerminalEntity newEntity = new VdvTerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TerminalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VdvTerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vdvTerminalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VdvTerminal = newEntity;
				_alreadyFetchedVdvTerminal = fetchResult;
			}
			return _vdvTerminal;
		}


		/// <summary> Retrieves the related entity of type 'SamModuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SamModuleEntity' which is related to this entity.</returns>
		public SamModuleEntity GetSingleSamModule()
		{
			return GetSingleSamModule(false);
		}

		/// <summary> Retrieves the related entity of type 'SamModuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SamModuleEntity' which is related to this entity.</returns>
		public virtual SamModuleEntity GetSingleSamModule(bool forceFetch)
		{
			if( ( !_alreadyFetchedSamModule || forceFetch || _alwaysFetchSamModule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SamModuleEntityUsingSamID);
				SamModuleEntity newEntity = new SamModuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SamID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SamModuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_samModuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SamModule = newEntity;
				_alreadyFetchedSamModule = fetchResult;
			}
			return _samModule;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("VdvTerminal", _vdvTerminal);
			toReturn.Add("SamModule", _samModule);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="validator">The validator object for this VdvSamStatusEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 statusID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(statusID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_vdvTerminalReturnsNewIfNotFound = false;
			_samModuleReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateEndOfValidity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportSequenceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadKeyCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamOperatorKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamSeqenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _vdvTerminal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVdvTerminal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vdvTerminal, new PropertyChangedEventHandler( OnVdvTerminalPropertyChanged ), "VdvTerminal", VarioSL.Entities.RelationClasses.StaticVdvSamStatusRelations.VdvTerminalEntityUsingTerminalIDStatic, true, signalRelatedEntity, "VdvSamStatuses", resetFKFields, new int[] { (int)VdvSamStatusFieldIndex.TerminalID } );		
			_vdvTerminal = null;
		}
		
		/// <summary> setups the sync logic for member _vdvTerminal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVdvTerminal(IEntityCore relatedEntity)
		{
			if(_vdvTerminal!=relatedEntity)
			{		
				DesetupSyncVdvTerminal(true, true);
				_vdvTerminal = (VdvTerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vdvTerminal, new PropertyChangedEventHandler( OnVdvTerminalPropertyChanged ), "VdvTerminal", VarioSL.Entities.RelationClasses.StaticVdvSamStatusRelations.VdvTerminalEntityUsingTerminalIDStatic, true, ref _alreadyFetchedVdvTerminal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVdvTerminalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _samModule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSamModule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _samModule, new PropertyChangedEventHandler( OnSamModulePropertyChanged ), "SamModule", VarioSL.Entities.RelationClasses.StaticVdvSamStatusRelations.SamModuleEntityUsingSamIDStatic, true, signalRelatedEntity, "VdvSamStatuses", resetFKFields, new int[] { (int)VdvSamStatusFieldIndex.SamID } );		
			_samModule = null;
		}
		
		/// <summary> setups the sync logic for member _samModule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSamModule(IEntityCore relatedEntity)
		{
			if(_samModule!=relatedEntity)
			{		
				DesetupSyncSamModule(true, true);
				_samModule = (SamModuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _samModule, new PropertyChangedEventHandler( OnSamModulePropertyChanged ), "SamModule", VarioSL.Entities.RelationClasses.StaticVdvSamStatusRelations.SamModuleEntityUsingSamIDStatic, true, ref _alreadyFetchedSamModule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSamModulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="statusID">PK value for VdvSamStatus which data should be fetched into this VdvSamStatus object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 statusID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvSamStatusFieldIndex.StatusID].ForcedCurrentValueWrite(statusID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvSamStatusDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvSamStatusEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvSamStatusRelations Relations
		{
			get	{ return new VdvSamStatusRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvTerminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvTerminal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvTerminalCollection(), (IEntityRelation)GetRelationsForField("VdvTerminal")[0], (int)VarioSL.Entities.EntityType.VdvSamStatusEntity, (int)VarioSL.Entities.EntityType.VdvTerminalEntity, 0, null, null, null, "VdvTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SamModule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSamModule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SamModuleCollection(), (IEntityRelation)GetRelationsForField("SamModule")[0], (int)VarioSL.Entities.EntityType.VdvSamStatusEntity, (int)VarioSL.Entities.EntityType.SamModuleEntity, 0, null, null, null, "SamModule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CertificateEndOfValidity property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."CERTEOV"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CertificateEndOfValidity
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VdvSamStatusFieldIndex.CertificateEndOfValidity, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.CertificateEndOfValidity, value, true); }
		}

		/// <summary> The ComponentID property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."COMPONENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ComponentID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.ComponentID, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.ComponentID, value, true); }
		}

		/// <summary> The ExportCounter property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."TMEXPORTCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.ExportCounter, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.ExportCounter, value, true); }
		}

		/// <summary> The ExportId property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."TMEXPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportId
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.ExportId, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.ExportId, value, true); }
		}

		/// <summary> The ExportSequenceId property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."TMEXPORTSEQUENCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportSequenceId
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.ExportSequenceId, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.ExportSequenceId, value, true); }
		}

		/// <summary> The ExportState property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."TMEXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ExportState
		{
			get { return (Nullable<System.Int16>)GetValue((int)VdvSamStatusFieldIndex.ExportState, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.ExportState, value, true); }
		}

		/// <summary> The LastData property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."LASTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastData
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VdvSamStatusFieldIndex.LastData, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.LastData, value, true); }
		}

		/// <summary> The LoadKeyCounter property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."LOADKEYCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LoadKeyCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.LoadKeyCounter, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.LoadKeyCounter, value, true); }
		}

		/// <summary> The SamID property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."SAMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SamID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.SamID, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.SamID, value, true); }
		}

		/// <summary> The SamOperatorKeyID property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."SAMOPKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 36<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SamOperatorKeyID
		{
			get { return (System.String)GetValue((int)VdvSamStatusFieldIndex.SamOperatorKeyID, true); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.SamOperatorKeyID, value, true); }
		}

		/// <summary> The SamSeqenceNumber property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."SAMSEQNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SamSeqenceNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.SamSeqenceNumber, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.SamSeqenceNumber, value, true); }
		}

		/// <summary> The SamVersion property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."SAMVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SamVersion
		{
			get { return (System.String)GetValue((int)VdvSamStatusFieldIndex.SamVersion, true); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.SamVersion, value, true); }
		}

		/// <summary> The StatusID property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."STATUSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 StatusID
		{
			get { return (System.Int64)GetValue((int)VdvSamStatusFieldIndex.StatusID, true); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.StatusID, value, true); }
		}

		/// <summary> The TerminalID property of the Entity VdvSamStatus<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_SAMSTATUS"."TERMINALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TerminalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvSamStatusFieldIndex.TerminalID, false); }
			set	{ SetValue((int)VdvSamStatusFieldIndex.TerminalID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'VdvTerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVdvTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvTerminalEntity VdvTerminal
		{
			get	{ return GetSingleVdvTerminal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVdvTerminal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvSamStatuses", "VdvTerminal", _vdvTerminal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VdvTerminal. When set to true, VdvTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvTerminal is accessed. You can always execute a forced fetch by calling GetSingleVdvTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvTerminal
		{
			get	{ return _alwaysFetchVdvTerminal; }
			set	{ _alwaysFetchVdvTerminal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvTerminal already has been fetched. Setting this property to false when VdvTerminal has been fetched
		/// will set VdvTerminal to null as well. Setting this property to true while VdvTerminal hasn't been fetched disables lazy loading for VdvTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvTerminal
		{
			get { return _alreadyFetchedVdvTerminal;}
			set 
			{
				if(_alreadyFetchedVdvTerminal && !value)
				{
					this.VdvTerminal = null;
				}
				_alreadyFetchedVdvTerminal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VdvTerminal is not found
		/// in the database. When set to true, VdvTerminal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VdvTerminalReturnsNewIfNotFound
		{
			get	{ return _vdvTerminalReturnsNewIfNotFound; }
			set { _vdvTerminalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SamModuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSamModule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SamModuleEntity SamModule
		{
			get	{ return GetSingleSamModule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSamModule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvSamStatuses", "SamModule", _samModule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SamModule. When set to true, SamModule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SamModule is accessed. You can always execute a forced fetch by calling GetSingleSamModule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSamModule
		{
			get	{ return _alwaysFetchSamModule; }
			set	{ _alwaysFetchSamModule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SamModule already has been fetched. Setting this property to false when SamModule has been fetched
		/// will set SamModule to null as well. Setting this property to true while SamModule hasn't been fetched disables lazy loading for SamModule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSamModule
		{
			get { return _alreadyFetchedSamModule;}
			set 
			{
				if(_alreadyFetchedSamModule && !value)
				{
					this.SamModule = null;
				}
				_alreadyFetchedSamModule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SamModule is not found
		/// in the database. When set to true, SamModule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SamModuleReturnsNewIfNotFound
		{
			get	{ return _samModuleReturnsNewIfNotFound; }
			set { _samModuleReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvSamStatusEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
