﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SalesRevenue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SalesRevenueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CloseoutPeriodEntity _closeoutPeriod;
		private bool	_alwaysFetchCloseoutPeriod, _alreadyFetchedCloseoutPeriod, _closeoutPeriodReturnsNewIfNotFound;
		private TransactionJournalEntity _transactionJournal;
		private bool	_alwaysFetchTransactionJournal, _alreadyFetchedTransactionJournal, _transactionJournalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CloseoutPeriod</summary>
			public static readonly string CloseoutPeriod = "CloseoutPeriod";
			/// <summary>Member name TransactionJournal</summary>
			public static readonly string TransactionJournal = "TransactionJournal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SalesRevenueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SalesRevenueEntity() :base("SalesRevenueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		public SalesRevenueEntity(System.Int64 salesRevenueID):base("SalesRevenueEntity")
		{
			InitClassFetch(salesRevenueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SalesRevenueEntity(System.Int64 salesRevenueID, IPrefetchPath prefetchPathToUse):base("SalesRevenueEntity")
		{
			InitClassFetch(salesRevenueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="validator">The custom validator object for this SalesRevenueEntity</param>
		public SalesRevenueEntity(System.Int64 salesRevenueID, IValidator validator):base("SalesRevenueEntity")
		{
			InitClassFetch(salesRevenueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SalesRevenueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_closeoutPeriod = (CloseoutPeriodEntity)info.GetValue("_closeoutPeriod", typeof(CloseoutPeriodEntity));
			if(_closeoutPeriod!=null)
			{
				_closeoutPeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_closeoutPeriodReturnsNewIfNotFound = info.GetBoolean("_closeoutPeriodReturnsNewIfNotFound");
			_alwaysFetchCloseoutPeriod = info.GetBoolean("_alwaysFetchCloseoutPeriod");
			_alreadyFetchedCloseoutPeriod = info.GetBoolean("_alreadyFetchedCloseoutPeriod");
			_transactionJournal = (TransactionJournalEntity)info.GetValue("_transactionJournal", typeof(TransactionJournalEntity));
			if(_transactionJournal!=null)
			{
				_transactionJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionJournalReturnsNewIfNotFound = info.GetBoolean("_transactionJournalReturnsNewIfNotFound");
			_alwaysFetchTransactionJournal = info.GetBoolean("_alwaysFetchTransactionJournal");
			_alreadyFetchedTransactionJournal = info.GetBoolean("_alreadyFetchedTransactionJournal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SalesRevenueFieldIndex)fieldIndex)
			{
				case SalesRevenueFieldIndex.CloseoutPeriodID:
					DesetupSyncCloseoutPeriod(true, false);
					_alreadyFetchedCloseoutPeriod = false;
					break;
				case SalesRevenueFieldIndex.TransactionJournalID:
					DesetupSyncTransactionJournal(true, false);
					_alreadyFetchedTransactionJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCloseoutPeriod = (_closeoutPeriod != null);
			_alreadyFetchedTransactionJournal = (_transactionJournal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CloseoutPeriod":
					toReturn.Add(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
					break;
				case "TransactionJournal":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_closeoutPeriod", (!this.MarkedForDeletion?_closeoutPeriod:null));
			info.AddValue("_closeoutPeriodReturnsNewIfNotFound", _closeoutPeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCloseoutPeriod", _alwaysFetchCloseoutPeriod);
			info.AddValue("_alreadyFetchedCloseoutPeriod", _alreadyFetchedCloseoutPeriod);

			info.AddValue("_transactionJournal", (!this.MarkedForDeletion?_transactionJournal:null));
			info.AddValue("_transactionJournalReturnsNewIfNotFound", _transactionJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionJournal", _alwaysFetchTransactionJournal);
			info.AddValue("_alreadyFetchedTransactionJournal", _alreadyFetchedTransactionJournal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CloseoutPeriod":
					_alreadyFetchedCloseoutPeriod = true;
					this.CloseoutPeriod = (CloseoutPeriodEntity)entity;
					break;
				case "TransactionJournal":
					_alreadyFetchedTransactionJournal = true;
					this.TransactionJournal = (TransactionJournalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					SetupSyncCloseoutPeriod(relatedEntity);
					break;
				case "TransactionJournal":
					SetupSyncTransactionJournal(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					DesetupSyncCloseoutPeriod(false, true);
					break;
				case "TransactionJournal":
					DesetupSyncTransactionJournal(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_closeoutPeriod!=null)
			{
				toReturn.Add(_closeoutPeriod);
			}
			if(_transactionJournal!=null)
			{
				toReturn.Add(_transactionJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="transactionJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTransactionJournalID(System.Int64 transactionJournalID)
		{
			return FetchUsingUCTransactionJournalID( transactionJournalID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="transactionJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTransactionJournalID(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCTransactionJournalID( transactionJournalID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="transactionJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTransactionJournalID(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCTransactionJournalID( transactionJournalID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="transactionJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTransactionJournalID(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((SalesRevenueDAO)CreateDAOInstance()).FetchSalesRevenueUsingUCTransactionJournalID(this, this.Transaction, transactionJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 salesRevenueID)
		{
			return FetchUsingPK(salesRevenueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 salesRevenueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(salesRevenueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 salesRevenueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(salesRevenueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 salesRevenueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(salesRevenueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SalesRevenueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SalesRevenueRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public CloseoutPeriodEntity GetSingleCloseoutPeriod()
		{
			return GetSingleCloseoutPeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public virtual CloseoutPeriodEntity GetSingleCloseoutPeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedCloseoutPeriod || forceFetch || _alwaysFetchCloseoutPeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
				CloseoutPeriodEntity newEntity = new CloseoutPeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CloseoutPeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CloseoutPeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_closeoutPeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CloseoutPeriod = newEntity;
				_alreadyFetchedCloseoutPeriod = fetchResult;
			}
			return _closeoutPeriod;
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleTransactionJournal()
		{
			return GetSingleTransactionJournal(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleTransactionJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionJournal || forceFetch || _alwaysFetchTransactionJournal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalID);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionJournalID);
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionJournal = newEntity;
				_alreadyFetchedTransactionJournal = fetchResult;
			}
			return _transactionJournal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CloseoutPeriod", _closeoutPeriod);
			toReturn.Add("TransactionJournal", _transactionJournal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="validator">The validator object for this SalesRevenueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 salesRevenueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(salesRevenueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_closeoutPeriodReturnsNewIfNotFound = false;
			_transactionJournalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesRevenueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _closeoutPeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCloseoutPeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticSalesRevenueRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, signalRelatedEntity, "SalesRevenues", resetFKFields, new int[] { (int)SalesRevenueFieldIndex.CloseoutPeriodID } );		
			_closeoutPeriod = null;
		}
		
		/// <summary> setups the sync logic for member _closeoutPeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCloseoutPeriod(IEntityCore relatedEntity)
		{
			if(_closeoutPeriod!=relatedEntity)
			{		
				DesetupSyncCloseoutPeriod(true, true);
				_closeoutPeriod = (CloseoutPeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticSalesRevenueRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, ref _alreadyFetchedCloseoutPeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCloseoutPeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticSalesRevenueRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, signalRelatedEntity, "SalesRevenue", resetFKFields, new int[] { (int)SalesRevenueFieldIndex.TransactionJournalID } );
			_transactionJournal = null;
		}
	
		/// <summary> setups the sync logic for member _transactionJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionJournal(IEntityCore relatedEntity)
		{
			if(_transactionJournal!=relatedEntity)
			{
				DesetupSyncTransactionJournal(true, true);
				_transactionJournal = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticSalesRevenueRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, ref _alreadyFetchedTransactionJournal, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="salesRevenueID">PK value for SalesRevenue which data should be fetched into this SalesRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 salesRevenueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SalesRevenueFieldIndex.SalesRevenueID].ForcedCurrentValueWrite(salesRevenueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSalesRevenueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SalesRevenueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SalesRevenueRelations Relations
		{
			get	{ return new SalesRevenueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloseoutPeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloseoutPeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CloseoutPeriodCollection(), (IEntityRelation)GetRelationsForField("CloseoutPeriod")[0], (int)VarioSL.Entities.EntityType.SalesRevenueEntity, (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, 0, null, null, null, "CloseoutPeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournal")[0], (int)VarioSL.Entities.EntityType.SalesRevenueEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)SalesRevenueFieldIndex.Amount, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.Amount, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."CLOSEOUTPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SalesRevenueFieldIndex.CloseoutPeriodID, false); }
			set	{ SetValue((int)SalesRevenueFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."CREDITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)SalesRevenueFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."DEBITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)SalesRevenueFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SalesRevenueFieldIndex.LastModified, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SalesRevenueFieldIndex.LastUser, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PostingDate property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."POSTINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SalesRevenueFieldIndex.PostingDate, false); }
			set	{ SetValue((int)SalesRevenueFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."POSTINGREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)SalesRevenueFieldIndex.PostingReference, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The SalesRevenueID property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."SALESREVENUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SalesRevenueID
		{
			get { return (System.Int64)GetValue((int)SalesRevenueFieldIndex.SalesRevenueID, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.SalesRevenueID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SalesRevenueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity SalesRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SALESREVENUE"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionJournalID
		{
			get { return (System.Int64)GetValue((int)SalesRevenueFieldIndex.TransactionJournalID, true); }
			set	{ SetValue((int)SalesRevenueFieldIndex.TransactionJournalID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CloseoutPeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCloseoutPeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CloseoutPeriodEntity CloseoutPeriod
		{
			get	{ return GetSingleCloseoutPeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCloseoutPeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SalesRevenues", "CloseoutPeriod", _closeoutPeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CloseoutPeriod. When set to true, CloseoutPeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloseoutPeriod is accessed. You can always execute a forced fetch by calling GetSingleCloseoutPeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloseoutPeriod
		{
			get	{ return _alwaysFetchCloseoutPeriod; }
			set	{ _alwaysFetchCloseoutPeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloseoutPeriod already has been fetched. Setting this property to false when CloseoutPeriod has been fetched
		/// will set CloseoutPeriod to null as well. Setting this property to true while CloseoutPeriod hasn't been fetched disables lazy loading for CloseoutPeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloseoutPeriod
		{
			get { return _alreadyFetchedCloseoutPeriod;}
			set 
			{
				if(_alreadyFetchedCloseoutPeriod && !value)
				{
					this.CloseoutPeriod = null;
				}
				_alreadyFetchedCloseoutPeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CloseoutPeriod is not found
		/// in the database. When set to true, CloseoutPeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CloseoutPeriodReturnsNewIfNotFound
		{
			get	{ return _closeoutPeriodReturnsNewIfNotFound; }
			set { _closeoutPeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity TransactionJournal
		{
			get	{ return GetSingleTransactionJournal(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncTransactionJournal(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_transactionJournal !=null);
						DesetupSyncTransactionJournal(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("TransactionJournal");
						}
					}
					else
					{
						if(_transactionJournal!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "SalesRevenue");
							SetupSyncTransactionJournal(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournal. When set to true, TransactionJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournal is accessed. You can always execute a forced fetch by calling GetSingleTransactionJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournal
		{
			get	{ return _alwaysFetchTransactionJournal; }
			set	{ _alwaysFetchTransactionJournal = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournal already has been fetched. Setting this property to false when TransactionJournal has been fetched
		/// will set TransactionJournal to null as well. Setting this property to true while TransactionJournal hasn't been fetched disables lazy loading for TransactionJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournal
		{
			get { return _alreadyFetchedTransactionJournal;}
			set 
			{
				if(_alreadyFetchedTransactionJournal && !value)
				{
					this.TransactionJournal = null;
				}
				_alreadyFetchedTransactionJournal = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionJournal is not found
		/// in the database. When set to true, TransactionJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionJournalReturnsNewIfNotFound
		{
			get	{ return _transactionJournalReturnsNewIfNotFound; }
			set	{ _transactionJournalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SalesRevenueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
