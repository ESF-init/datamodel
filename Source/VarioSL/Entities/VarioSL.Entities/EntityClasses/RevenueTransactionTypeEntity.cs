﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RevenueTransactionType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RevenueTransactionTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AbortedTransactionCollection	_abortedTransactions;
		private bool	_alwaysFetchAbortedTransactions, _alreadyFetchedAbortedTransactions;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResults;
		private bool	_alwaysFetchClearingResults, _alreadyFetchedClearingResults;
		private VarioSL.Entities.CollectionClasses.TransactionCollection	_transactions;
		private bool	_alwaysFetchTransactions, _alreadyFetchedTransactions;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AbortedTransactions</summary>
			public static readonly string AbortedTransactions = "AbortedTransactions";
			/// <summary>Member name ClearingResults</summary>
			public static readonly string ClearingResults = "ClearingResults";
			/// <summary>Member name Transactions</summary>
			public static readonly string Transactions = "Transactions";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RevenueTransactionTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RevenueTransactionTypeEntity() :base("RevenueTransactionTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		public RevenueTransactionTypeEntity(System.Int64 typeID):base("RevenueTransactionTypeEntity")
		{
			InitClassFetch(typeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RevenueTransactionTypeEntity(System.Int64 typeID, IPrefetchPath prefetchPathToUse):base("RevenueTransactionTypeEntity")
		{
			InitClassFetch(typeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="validator">The custom validator object for this RevenueTransactionTypeEntity</param>
		public RevenueTransactionTypeEntity(System.Int64 typeID, IValidator validator):base("RevenueTransactionTypeEntity")
		{
			InitClassFetch(typeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RevenueTransactionTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_abortedTransactions = (VarioSL.Entities.CollectionClasses.AbortedTransactionCollection)info.GetValue("_abortedTransactions", typeof(VarioSL.Entities.CollectionClasses.AbortedTransactionCollection));
			_alwaysFetchAbortedTransactions = info.GetBoolean("_alwaysFetchAbortedTransactions");
			_alreadyFetchedAbortedTransactions = info.GetBoolean("_alreadyFetchedAbortedTransactions");

			_clearingResults = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResults", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResults = info.GetBoolean("_alwaysFetchClearingResults");
			_alreadyFetchedClearingResults = info.GetBoolean("_alreadyFetchedClearingResults");

			_transactions = (VarioSL.Entities.CollectionClasses.TransactionCollection)info.GetValue("_transactions", typeof(VarioSL.Entities.CollectionClasses.TransactionCollection));
			_alwaysFetchTransactions = info.GetBoolean("_alwaysFetchTransactions");
			_alreadyFetchedTransactions = info.GetBoolean("_alreadyFetchedTransactions");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAbortedTransactions = (_abortedTransactions.Count > 0);
			_alreadyFetchedClearingResults = (_clearingResults.Count > 0);
			_alreadyFetchedTransactions = (_transactions.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AbortedTransactions":
					toReturn.Add(Relations.AbortedTransactionEntityUsingTypeID);
					break;
				case "ClearingResults":
					toReturn.Add(Relations.ClearingResultEntityUsingTransactionTypeID);
					break;
				case "Transactions":
					toReturn.Add(Relations.TransactionEntityUsingTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_abortedTransactions", (!this.MarkedForDeletion?_abortedTransactions:null));
			info.AddValue("_alwaysFetchAbortedTransactions", _alwaysFetchAbortedTransactions);
			info.AddValue("_alreadyFetchedAbortedTransactions", _alreadyFetchedAbortedTransactions);
			info.AddValue("_clearingResults", (!this.MarkedForDeletion?_clearingResults:null));
			info.AddValue("_alwaysFetchClearingResults", _alwaysFetchClearingResults);
			info.AddValue("_alreadyFetchedClearingResults", _alreadyFetchedClearingResults);
			info.AddValue("_transactions", (!this.MarkedForDeletion?_transactions:null));
			info.AddValue("_alwaysFetchTransactions", _alwaysFetchTransactions);
			info.AddValue("_alreadyFetchedTransactions", _alreadyFetchedTransactions);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AbortedTransactions":
					_alreadyFetchedAbortedTransactions = true;
					if(entity!=null)
					{
						this.AbortedTransactions.Add((AbortedTransactionEntity)entity);
					}
					break;
				case "ClearingResults":
					_alreadyFetchedClearingResults = true;
					if(entity!=null)
					{
						this.ClearingResults.Add((ClearingResultEntity)entity);
					}
					break;
				case "Transactions":
					_alreadyFetchedTransactions = true;
					if(entity!=null)
					{
						this.Transactions.Add((TransactionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AbortedTransactions":
					_abortedTransactions.Add((AbortedTransactionEntity)relatedEntity);
					break;
				case "ClearingResults":
					_clearingResults.Add((ClearingResultEntity)relatedEntity);
					break;
				case "Transactions":
					_transactions.Add((TransactionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AbortedTransactions":
					this.PerformRelatedEntityRemoval(_abortedTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResults":
					this.PerformRelatedEntityRemoval(_clearingResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Transactions":
					this.PerformRelatedEntityRemoval(_transactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_abortedTransactions);
			toReturn.Add(_clearingResults);
			toReturn.Add(_transactions);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID)
		{
			return FetchUsingPK(typeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(typeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RevenueTransactionTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AbortedTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AbortedTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AbortedTransactionCollection GetMultiAbortedTransactions(bool forceFetch)
		{
			return GetMultiAbortedTransactions(forceFetch, _abortedTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AbortedTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AbortedTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AbortedTransactionCollection GetMultiAbortedTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAbortedTransactions(forceFetch, _abortedTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AbortedTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AbortedTransactionCollection GetMultiAbortedTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAbortedTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AbortedTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AbortedTransactionCollection GetMultiAbortedTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAbortedTransactions || forceFetch || _alwaysFetchAbortedTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_abortedTransactions);
				_abortedTransactions.SuppressClearInGetMulti=!forceFetch;
				_abortedTransactions.EntityFactoryToUse = entityFactoryToUse;
				_abortedTransactions.GetMultiManyToOne(this, filter);
				_abortedTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedAbortedTransactions = true;
			}
			return _abortedTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'AbortedTransactions'. These settings will be taken into account
		/// when the property AbortedTransactions is requested or GetMultiAbortedTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAbortedTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_abortedTransactions.SortClauses=sortClauses;
			_abortedTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResults || forceFetch || _alwaysFetchClearingResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResults);
				_clearingResults.SuppressClearInGetMulti=!forceFetch;
				_clearingResults.EntityFactoryToUse = entityFactoryToUse;
				_clearingResults.GetMultiManyToOne(null, null, null, null, null, null, null, this, filter);
				_clearingResults.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResults = true;
			}
			return _clearingResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResults'. These settings will be taken into account
		/// when the property ClearingResults is requested or GetMultiClearingResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResults.SortClauses=sortClauses;
			_clearingResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch)
		{
			return GetMultiTransactions(forceFetch, _transactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactions(forceFetch, _transactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactions || forceFetch || _alwaysFetchTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactions);
				_transactions.SuppressClearInGetMulti=!forceFetch;
				_transactions.EntityFactoryToUse = entityFactoryToUse;
				_transactions.GetMultiManyToOne(null, this, null, filter);
				_transactions.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactions = true;
			}
			return _transactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'Transactions'. These settings will be taken into account
		/// when the property Transactions is requested or GetMultiTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactions.SortClauses=sortClauses;
			_transactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AbortedTransactions", _abortedTransactions);
			toReturn.Add("ClearingResults", _clearingResults);
			toReturn.Add("Transactions", _transactions);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="validator">The validator object for this RevenueTransactionTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 typeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(typeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_abortedTransactions = new VarioSL.Entities.CollectionClasses.AbortedTransactionCollection();
			_abortedTransactions.SetContainingEntityInfo(this, "RevenueTransactionType");

			_clearingResults = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResults.SetContainingEntityInfo(this, "RevenueTransactionType");

			_transactions = new VarioSL.Entities.CollectionClasses.TransactionCollection();
			_transactions.SetContainingEntityInfo(this, "TransactionType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateAccountEntry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyAutomatic", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyManual", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="typeID">PK value for RevenueTransactionType which data should be fetched into this RevenueTransactionType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RevenueTransactionTypeFieldIndex.TypeID].ForcedCurrentValueWrite(typeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRevenueTransactionTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RevenueTransactionTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RevenueTransactionTypeRelations Relations
		{
			get	{ return new RevenueTransactionTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AbortedTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAbortedTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AbortedTransactionCollection(), (IEntityRelation)GetRelationsForField("AbortedTransactions")[0], (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, (int)VarioSL.Entities.EntityType.AbortedTransactionEntity, 0, null, null, null, "AbortedTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResults")[0], (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("Transactions")[0], (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "Transactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreateAccountEntry property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."CREATEACCOUNTENTRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> CreateAccountEntry
		{
			get { return (Nullable<System.Int16>)GetValue((int)RevenueTransactionTypeFieldIndex.CreateAccountEntry, false); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.CreateAccountEntry, value, true); }
		}

		/// <summary> The PostingKeyAutomatic property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."POSTINGKEYAUTOMATIC"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> PostingKeyAutomatic
		{
			get { return (Nullable<System.Int16>)GetValue((int)RevenueTransactionTypeFieldIndex.PostingKeyAutomatic, false); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.PostingKeyAutomatic, value, true); }
		}

		/// <summary> The PostingKeyManual property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."POSTINGKEYMANUAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> PostingKeyManual
		{
			get { return (Nullable<System.Int16>)GetValue((int)RevenueTransactionTypeFieldIndex.PostingKeyManual, false); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.PostingKeyManual, value, true); }
		}

		/// <summary> The TypeID property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."TYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TypeID
		{
			get { return (System.Int64)GetValue((int)RevenueTransactionTypeFieldIndex.TypeID, true); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.TypeID, value, true); }
		}

		/// <summary> The TypeName property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."TYPENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeName
		{
			get { return (System.String)GetValue((int)RevenueTransactionTypeFieldIndex.TypeName, true); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.TypeName, value, true); }
		}

		/// <summary> The Visible property of the Entity RevenueTransactionType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_TRANSACTIONTYPE"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Visible
		{
			get { return (Nullable<System.Int16>)GetValue((int)RevenueTransactionTypeFieldIndex.Visible, false); }
			set	{ SetValue((int)RevenueTransactionTypeFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AbortedTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAbortedTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AbortedTransactionCollection AbortedTransactions
		{
			get	{ return GetMultiAbortedTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AbortedTransactions. When set to true, AbortedTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AbortedTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiAbortedTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAbortedTransactions
		{
			get	{ return _alwaysFetchAbortedTransactions; }
			set	{ _alwaysFetchAbortedTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AbortedTransactions already has been fetched. Setting this property to false when AbortedTransactions has been fetched
		/// will clear the AbortedTransactions collection well. Setting this property to true while AbortedTransactions hasn't been fetched disables lazy loading for AbortedTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAbortedTransactions
		{
			get { return _alreadyFetchedAbortedTransactions;}
			set 
			{
				if(_alreadyFetchedAbortedTransactions && !value && (_abortedTransactions != null))
				{
					_abortedTransactions.Clear();
				}
				_alreadyFetchedAbortedTransactions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResults
		{
			get	{ return GetMultiClearingResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResults. When set to true, ClearingResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResults is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResults
		{
			get	{ return _alwaysFetchClearingResults; }
			set	{ _alwaysFetchClearingResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResults already has been fetched. Setting this property to false when ClearingResults has been fetched
		/// will clear the ClearingResults collection well. Setting this property to true while ClearingResults hasn't been fetched disables lazy loading for ClearingResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResults
		{
			get { return _alreadyFetchedClearingResults;}
			set 
			{
				if(_alreadyFetchedClearingResults && !value && (_clearingResults != null))
				{
					_clearingResults.Clear();
				}
				_alreadyFetchedClearingResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection Transactions
		{
			get	{ return GetMultiTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Transactions. When set to true, Transactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Transactions is accessed. You can always execute/ a forced fetch by calling GetMultiTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactions
		{
			get	{ return _alwaysFetchTransactions; }
			set	{ _alwaysFetchTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Transactions already has been fetched. Setting this property to false when Transactions has been fetched
		/// will clear the Transactions collection well. Setting this property to true while Transactions hasn't been fetched disables lazy loading for Transactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactions
		{
			get { return _alreadyFetchedTransactions;}
			set 
			{
				if(_alreadyFetchedTransactions && !value && (_transactions != null))
				{
					_transactions.Clear();
				}
				_alreadyFetchedTransactions = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
