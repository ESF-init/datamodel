﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ReportJob'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ReportJobEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ReportJobResultCollection	_reportJobResults;
		private bool	_alwaysFetchReportJobResults, _alreadyFetchedReportJobResults;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private FilterValueSetEntity _filterValueSet;
		private bool	_alwaysFetchFilterValueSet, _alreadyFetchedFilterValueSet, _filterValueSetReturnsNewIfNotFound;
		private ReportEntity _report;
		private bool	_alwaysFetchReport, _alreadyFetchedReport, _reportReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name FilterValueSet</summary>
			public static readonly string FilterValueSet = "FilterValueSet";
			/// <summary>Member name Report</summary>
			public static readonly string Report = "Report";
			/// <summary>Member name ReportJobResults</summary>
			public static readonly string ReportJobResults = "ReportJobResults";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportJobEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ReportJobEntity() :base("ReportJobEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		public ReportJobEntity(System.Int64 reportJobID):base("ReportJobEntity")
		{
			InitClassFetch(reportJobID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReportJobEntity(System.Int64 reportJobID, IPrefetchPath prefetchPathToUse):base("ReportJobEntity")
		{
			InitClassFetch(reportJobID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="validator">The custom validator object for this ReportJobEntity</param>
		public ReportJobEntity(System.Int64 reportJobID, IValidator validator):base("ReportJobEntity")
		{
			InitClassFetch(reportJobID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportJobEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_reportJobResults = (VarioSL.Entities.CollectionClasses.ReportJobResultCollection)info.GetValue("_reportJobResults", typeof(VarioSL.Entities.CollectionClasses.ReportJobResultCollection));
			_alwaysFetchReportJobResults = info.GetBoolean("_alwaysFetchReportJobResults");
			_alreadyFetchedReportJobResults = info.GetBoolean("_alreadyFetchedReportJobResults");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_filterValueSet = (FilterValueSetEntity)info.GetValue("_filterValueSet", typeof(FilterValueSetEntity));
			if(_filterValueSet!=null)
			{
				_filterValueSet.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterValueSetReturnsNewIfNotFound = info.GetBoolean("_filterValueSetReturnsNewIfNotFound");
			_alwaysFetchFilterValueSet = info.GetBoolean("_alwaysFetchFilterValueSet");
			_alreadyFetchedFilterValueSet = info.GetBoolean("_alreadyFetchedFilterValueSet");

			_report = (ReportEntity)info.GetValue("_report", typeof(ReportEntity));
			if(_report!=null)
			{
				_report.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportReturnsNewIfNotFound = info.GetBoolean("_reportReturnsNewIfNotFound");
			_alwaysFetchReport = info.GetBoolean("_alwaysFetchReport");
			_alreadyFetchedReport = info.GetBoolean("_alreadyFetchedReport");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportJobFieldIndex)fieldIndex)
			{
				case ReportJobFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ReportJobFieldIndex.FilterValueSetID:
					DesetupSyncFilterValueSet(true, false);
					_alreadyFetchedFilterValueSet = false;
					break;
				case ReportJobFieldIndex.ReportID:
					DesetupSyncReport(true, false);
					_alreadyFetchedReport = false;
					break;
				case ReportJobFieldIndex.UserID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReportJobResults = (_reportJobResults.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedFilterValueSet = (_filterValueSet != null);
			_alreadyFetchedReport = (_report != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "FilterValueSet":
					toReturn.Add(Relations.FilterValueSetEntityUsingFilterValueSetID);
					break;
				case "Report":
					toReturn.Add(Relations.ReportEntityUsingReportID);
					break;
				case "ReportJobResults":
					toReturn.Add(Relations.ReportJobResultEntityUsingReportJobID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_reportJobResults", (!this.MarkedForDeletion?_reportJobResults:null));
			info.AddValue("_alwaysFetchReportJobResults", _alwaysFetchReportJobResults);
			info.AddValue("_alreadyFetchedReportJobResults", _alreadyFetchedReportJobResults);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_filterValueSet", (!this.MarkedForDeletion?_filterValueSet:null));
			info.AddValue("_filterValueSetReturnsNewIfNotFound", _filterValueSetReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterValueSet", _alwaysFetchFilterValueSet);
			info.AddValue("_alreadyFetchedFilterValueSet", _alreadyFetchedFilterValueSet);
			info.AddValue("_report", (!this.MarkedForDeletion?_report:null));
			info.AddValue("_reportReturnsNewIfNotFound", _reportReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReport", _alwaysFetchReport);
			info.AddValue("_alreadyFetchedReport", _alreadyFetchedReport);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "FilterValueSet":
					_alreadyFetchedFilterValueSet = true;
					this.FilterValueSet = (FilterValueSetEntity)entity;
					break;
				case "Report":
					_alreadyFetchedReport = true;
					this.Report = (ReportEntity)entity;
					break;
				case "ReportJobResults":
					_alreadyFetchedReportJobResults = true;
					if(entity!=null)
					{
						this.ReportJobResults.Add((ReportJobResultEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "FilterValueSet":
					SetupSyncFilterValueSet(relatedEntity);
					break;
				case "Report":
					SetupSyncReport(relatedEntity);
					break;
				case "ReportJobResults":
					_reportJobResults.Add((ReportJobResultEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "FilterValueSet":
					DesetupSyncFilterValueSet(false, true);
					break;
				case "Report":
					DesetupSyncReport(false, true);
					break;
				case "ReportJobResults":
					this.PerformRelatedEntityRemoval(_reportJobResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_filterValueSet!=null)
			{
				toReturn.Add(_filterValueSet);
			}
			if(_report!=null)
			{
				toReturn.Add(_report);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_reportJobResults);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobID)
		{
			return FetchUsingPK(reportJobID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportJobID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportJobID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportJobID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportJobID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportJobRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobResults(forceFetch, _reportJobResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection GetMultiReportJobResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobResults || forceFetch || _alwaysFetchReportJobResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobResults);
				_reportJobResults.SuppressClearInGetMulti=!forceFetch;
				_reportJobResults.EntityFactoryToUse = entityFactoryToUse;
				_reportJobResults.GetMultiManyToOne(null, null, this, filter);
				_reportJobResults.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobResults = true;
			}
			return _reportJobResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobResults'. These settings will be taken into account
		/// when the property ReportJobResults is requested or GetMultiReportJobResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobResults.SortClauses=sortClauses;
			_reportJobResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'FilterValueSetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterValueSetEntity' which is related to this entity.</returns>
		public FilterValueSetEntity GetSingleFilterValueSet()
		{
			return GetSingleFilterValueSet(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterValueSetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterValueSetEntity' which is related to this entity.</returns>
		public virtual FilterValueSetEntity GetSingleFilterValueSet(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterValueSet || forceFetch || _alwaysFetchFilterValueSet) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterValueSetEntityUsingFilterValueSetID);
				FilterValueSetEntity newEntity = new FilterValueSetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterValueSetID);
				}
				if(fetchResult)
				{
					newEntity = (FilterValueSetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterValueSetReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterValueSet = newEntity;
				_alreadyFetchedFilterValueSet = fetchResult;
			}
			return _filterValueSet;
		}


		/// <summary> Retrieves the related entity of type 'ReportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportEntity' which is related to this entity.</returns>
		public ReportEntity GetSingleReport()
		{
			return GetSingleReport(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportEntity' which is related to this entity.</returns>
		public virtual ReportEntity GetSingleReport(bool forceFetch)
		{
			if( ( !_alreadyFetchedReport || forceFetch || _alwaysFetchReport) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportEntityUsingReportID);
				ReportEntity newEntity = new ReportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportID);
				}
				if(fetchResult)
				{
					newEntity = (ReportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Report = newEntity;
				_alreadyFetchedReport = fetchResult;
			}
			return _report;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("UserList", _userList);
			toReturn.Add("FilterValueSet", _filterValueSet);
			toReturn.Add("Report", _report);
			toReturn.Add("ReportJobResults", _reportJobResults);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="validator">The validator object for this ReportJobEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 reportJobID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportJobID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_reportJobResults = new VarioSL.Entities.CollectionClasses.ReportJobResultCollection();
			_reportJobResults.SetContainingEntityInfo(this, "ReportJob");
			_clientReturnsNewIfNotFound = false;
			_userListReturnsNewIfNotFound = false;
			_filterValueSetReturnsNewIfNotFound = false;
			_reportReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailRecipient", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnableNotification", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterValueSetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastRun", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NextRun", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportJobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduleRfc5545", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticReportJobRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ReportJobs", resetFKFields, new int[] { (int)ReportJobFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticReportJobRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticReportJobRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "ReportJobs", resetFKFields, new int[] { (int)ReportJobFieldIndex.UserID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticReportJobRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterValueSet</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterValueSet(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterValueSet, new PropertyChangedEventHandler( OnFilterValueSetPropertyChanged ), "FilterValueSet", VarioSL.Entities.RelationClasses.StaticReportJobRelations.FilterValueSetEntityUsingFilterValueSetIDStatic, true, signalRelatedEntity, "ReportJobs", resetFKFields, new int[] { (int)ReportJobFieldIndex.FilterValueSetID } );		
			_filterValueSet = null;
		}
		
		/// <summary> setups the sync logic for member _filterValueSet</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterValueSet(IEntityCore relatedEntity)
		{
			if(_filterValueSet!=relatedEntity)
			{		
				DesetupSyncFilterValueSet(true, true);
				_filterValueSet = (FilterValueSetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterValueSet, new PropertyChangedEventHandler( OnFilterValueSetPropertyChanged ), "FilterValueSet", VarioSL.Entities.RelationClasses.StaticReportJobRelations.FilterValueSetEntityUsingFilterValueSetIDStatic, true, ref _alreadyFetchedFilterValueSet, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterValueSetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _report</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReport(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _report, new PropertyChangedEventHandler( OnReportPropertyChanged ), "Report", VarioSL.Entities.RelationClasses.StaticReportJobRelations.ReportEntityUsingReportIDStatic, true, signalRelatedEntity, "ReportJobs", resetFKFields, new int[] { (int)ReportJobFieldIndex.ReportID } );		
			_report = null;
		}
		
		/// <summary> setups the sync logic for member _report</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReport(IEntityCore relatedEntity)
		{
			if(_report!=relatedEntity)
			{		
				DesetupSyncReport(true, true);
				_report = (ReportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _report, new PropertyChangedEventHandler( OnReportPropertyChanged ), "Report", VarioSL.Entities.RelationClasses.StaticReportJobRelations.ReportEntityUsingReportIDStatic, true, ref _alreadyFetchedReport, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportJobID">PK value for ReportJob which data should be fetched into this ReportJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 reportJobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportJobFieldIndex.ReportJobID].ForcedCurrentValueWrite(reportJobID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportJobDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportJobEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportJobRelations Relations
		{
			get	{ return new ReportJobRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJobResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobResultCollection(), (IEntityRelation)GetRelationsForField("ReportJobResults")[0], (int)VarioSL.Entities.EntityType.ReportJobEntity, (int)VarioSL.Entities.EntityType.ReportJobResultEntity, 0, null, null, null, "ReportJobResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ReportJobEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.ReportJobEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSet
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSet")[0], (int)VarioSL.Entities.EntityType.ReportJobEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSet", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReport
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), (IEntityRelation)GetRelationsForField("Report")[0], (int)VarioSL.Entities.EntityType.ReportJobEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, null, "Report", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Active property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."ACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Active
		{
			get { return (System.Boolean)GetValue((int)ReportJobFieldIndex.Active, true); }
			set	{ SetValue((int)ReportJobFieldIndex.Active, value, true); }
		}

		/// <summary> The ClientID property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)ReportJobFieldIndex.ClientID, true); }
			set	{ SetValue((int)ReportJobFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreationTime property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."CREATIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportJobFieldIndex.CreationTime, false); }
			set	{ SetValue((int)ReportJobFieldIndex.CreationTime, value, true); }
		}

		/// <summary> The Description property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ReportJobFieldIndex.Description, true); }
			set	{ SetValue((int)ReportJobFieldIndex.Description, value, true); }
		}

		/// <summary> The EmailRecipient property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."EMAILRECIPIENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EmailRecipient
		{
			get { return (System.String)GetValue((int)ReportJobFieldIndex.EmailRecipient, true); }
			set	{ SetValue((int)ReportJobFieldIndex.EmailRecipient, value, true); }
		}

		/// <summary> The EnableNotification property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."ENABLENOTIFICATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> EnableNotification
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ReportJobFieldIndex.EnableNotification, false); }
			set	{ SetValue((int)ReportJobFieldIndex.EnableNotification, value, true); }
		}

		/// <summary> The FileType property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."FILETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.JobFileType FileType
		{
			get { return (VarioSL.Entities.Enumerations.JobFileType)GetValue((int)ReportJobFieldIndex.FileType, true); }
			set	{ SetValue((int)ReportJobFieldIndex.FileType, value, true); }
		}

		/// <summary> The FilterValueSetID property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."FILTERVALUESETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterValueSetID
		{
			get { return (System.Int64)GetValue((int)ReportJobFieldIndex.FilterValueSetID, true); }
			set	{ SetValue((int)ReportJobFieldIndex.FilterValueSetID, value, true); }
		}

		/// <summary> The LastModified property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ReportJobFieldIndex.LastModified, true); }
			set	{ SetValue((int)ReportJobFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastRun property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."LASTRUN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastRun
		{
			get { return (System.DateTime)GetValue((int)ReportJobFieldIndex.LastRun, true); }
			set	{ SetValue((int)ReportJobFieldIndex.LastRun, value, true); }
		}

		/// <summary> The LastUser property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ReportJobFieldIndex.LastUser, true); }
			set	{ SetValue((int)ReportJobFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReportJobFieldIndex.Name, true); }
			set	{ SetValue((int)ReportJobFieldIndex.Name, value, true); }
		}

		/// <summary> The NextRun property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."NEXTRUN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime NextRun
		{
			get { return (System.DateTime)GetValue((int)ReportJobFieldIndex.NextRun, true); }
			set	{ SetValue((int)ReportJobFieldIndex.NextRun, value, true); }
		}

		/// <summary> The ReportID property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."REPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ReportID
		{
			get { return (System.Int64)GetValue((int)ReportJobFieldIndex.ReportID, true); }
			set	{ SetValue((int)ReportJobFieldIndex.ReportID, value, true); }
		}

		/// <summary> The ReportJobID property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."REPORTJOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ReportJobID
		{
			get { return (System.Int64)GetValue((int)ReportJobFieldIndex.ReportJobID, true); }
			set	{ SetValue((int)ReportJobFieldIndex.ReportJobID, value, true); }
		}

		/// <summary> The ScheduleRfc5545 property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."SCHEDULERFC5545"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ScheduleRfc5545
		{
			get { return (System.String)GetValue((int)ReportJobFieldIndex.ScheduleRfc5545, true); }
			set	{ SetValue((int)ReportJobFieldIndex.ScheduleRfc5545, value, true); }
		}

		/// <summary> The State property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.JobState State
		{
			get { return (VarioSL.Entities.Enumerations.JobState)GetValue((int)ReportJobFieldIndex.State, true); }
			set	{ SetValue((int)ReportJobFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ReportJobFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ReportJobFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserID property of the Entity ReportJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOB"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportJobFieldIndex.UserID, false); }
			set	{ SetValue((int)ReportJobFieldIndex.UserID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ReportJobResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobResultCollection ReportJobResults
		{
			get	{ return GetMultiReportJobResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobResults. When set to true, ReportJobResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobResults is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobResults
		{
			get	{ return _alwaysFetchReportJobResults; }
			set	{ _alwaysFetchReportJobResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobResults already has been fetched. Setting this property to false when ReportJobResults has been fetched
		/// will clear the ReportJobResults collection well. Setting this property to true while ReportJobResults hasn't been fetched disables lazy loading for ReportJobResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobResults
		{
			get { return _alreadyFetchedReportJobResults;}
			set 
			{
				if(_alreadyFetchedReportJobResults && !value && (_reportJobResults != null))
				{
					_reportJobResults.Clear();
				}
				_alreadyFetchedReportJobResults = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobs", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobs", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterValueSetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterValueSet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterValueSetEntity FilterValueSet
		{
			get	{ return GetSingleFilterValueSet(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterValueSet(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobs", "FilterValueSet", _filterValueSet, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSet. When set to true, FilterValueSet is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSet is accessed. You can always execute a forced fetch by calling GetSingleFilterValueSet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSet
		{
			get	{ return _alwaysFetchFilterValueSet; }
			set	{ _alwaysFetchFilterValueSet = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSet already has been fetched. Setting this property to false when FilterValueSet has been fetched
		/// will set FilterValueSet to null as well. Setting this property to true while FilterValueSet hasn't been fetched disables lazy loading for FilterValueSet</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSet
		{
			get { return _alreadyFetchedFilterValueSet;}
			set 
			{
				if(_alreadyFetchedFilterValueSet && !value)
				{
					this.FilterValueSet = null;
				}
				_alreadyFetchedFilterValueSet = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterValueSet is not found
		/// in the database. When set to true, FilterValueSet will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterValueSetReturnsNewIfNotFound
		{
			get	{ return _filterValueSetReturnsNewIfNotFound; }
			set { _filterValueSetReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReport()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportEntity Report
		{
			get	{ return GetSingleReport(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReport(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobs", "Report", _report, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Report. When set to true, Report is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Report is accessed. You can always execute a forced fetch by calling GetSingleReport(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReport
		{
			get	{ return _alwaysFetchReport; }
			set	{ _alwaysFetchReport = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Report already has been fetched. Setting this property to false when Report has been fetched
		/// will set Report to null as well. Setting this property to true while Report hasn't been fetched disables lazy loading for Report</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReport
		{
			get { return _alreadyFetchedReport;}
			set 
			{
				if(_alreadyFetchedReport && !value)
				{
					this.Report = null;
				}
				_alreadyFetchedReport = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Report is not found
		/// in the database. When set to true, Report will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReportReturnsNewIfNotFound
		{
			get	{ return _reportReturnsNewIfNotFound; }
			set { _reportReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ReportJobEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
