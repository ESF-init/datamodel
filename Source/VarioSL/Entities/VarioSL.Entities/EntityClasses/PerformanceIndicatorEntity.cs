﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PerformanceIndicator'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PerformanceIndicatorEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PerformanceCollection	_performances;
		private bool	_alwaysFetchPerformances, _alreadyFetchedPerformances;
		private VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection	_aggregations;
		private bool	_alwaysFetchAggregations, _alreadyFetchedAggregations;
		private PerformanceComponentEntity _performanceComponent;
		private bool	_alwaysFetchPerformanceComponent, _alreadyFetchedPerformanceComponent, _performanceComponentReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PerformanceComponent</summary>
			public static readonly string PerformanceComponent = "PerformanceComponent";
			/// <summary>Member name Performances</summary>
			public static readonly string Performances = "Performances";
			/// <summary>Member name Aggregations</summary>
			public static readonly string Aggregations = "Aggregations";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PerformanceIndicatorEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PerformanceIndicatorEntity() :base("PerformanceIndicatorEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		public PerformanceIndicatorEntity(System.Int64 indicatorID):base("PerformanceIndicatorEntity")
		{
			InitClassFetch(indicatorID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PerformanceIndicatorEntity(System.Int64 indicatorID, IPrefetchPath prefetchPathToUse):base("PerformanceIndicatorEntity")
		{
			InitClassFetch(indicatorID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="validator">The custom validator object for this PerformanceIndicatorEntity</param>
		public PerformanceIndicatorEntity(System.Int64 indicatorID, IValidator validator):base("PerformanceIndicatorEntity")
		{
			InitClassFetch(indicatorID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PerformanceIndicatorEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_performances = (VarioSL.Entities.CollectionClasses.PerformanceCollection)info.GetValue("_performances", typeof(VarioSL.Entities.CollectionClasses.PerformanceCollection));
			_alwaysFetchPerformances = info.GetBoolean("_alwaysFetchPerformances");
			_alreadyFetchedPerformances = info.GetBoolean("_alreadyFetchedPerformances");

			_aggregations = (VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection)info.GetValue("_aggregations", typeof(VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection));
			_alwaysFetchAggregations = info.GetBoolean("_alwaysFetchAggregations");
			_alreadyFetchedAggregations = info.GetBoolean("_alreadyFetchedAggregations");
			_performanceComponent = (PerformanceComponentEntity)info.GetValue("_performanceComponent", typeof(PerformanceComponentEntity));
			if(_performanceComponent!=null)
			{
				_performanceComponent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_performanceComponentReturnsNewIfNotFound = info.GetBoolean("_performanceComponentReturnsNewIfNotFound");
			_alwaysFetchPerformanceComponent = info.GetBoolean("_alwaysFetchPerformanceComponent");
			_alreadyFetchedPerformanceComponent = info.GetBoolean("_alreadyFetchedPerformanceComponent");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PerformanceIndicatorFieldIndex)fieldIndex)
			{
				case PerformanceIndicatorFieldIndex.ComponentID:
					DesetupSyncPerformanceComponent(true, false);
					_alreadyFetchedPerformanceComponent = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPerformances = (_performances.Count > 0);
			_alreadyFetchedAggregations = (_aggregations.Count > 0);
			_alreadyFetchedPerformanceComponent = (_performanceComponent != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PerformanceComponent":
					toReturn.Add(Relations.PerformanceComponentEntityUsingComponentID);
					break;
				case "Performances":
					toReturn.Add(Relations.PerformanceEntityUsingIndicatorID);
					break;
				case "Aggregations":
					toReturn.Add(Relations.PerformanceAggregationEntityUsingIndicatorID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_performances", (!this.MarkedForDeletion?_performances:null));
			info.AddValue("_alwaysFetchPerformances", _alwaysFetchPerformances);
			info.AddValue("_alreadyFetchedPerformances", _alreadyFetchedPerformances);
			info.AddValue("_aggregations", (!this.MarkedForDeletion?_aggregations:null));
			info.AddValue("_alwaysFetchAggregations", _alwaysFetchAggregations);
			info.AddValue("_alreadyFetchedAggregations", _alreadyFetchedAggregations);
			info.AddValue("_performanceComponent", (!this.MarkedForDeletion?_performanceComponent:null));
			info.AddValue("_performanceComponentReturnsNewIfNotFound", _performanceComponentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerformanceComponent", _alwaysFetchPerformanceComponent);
			info.AddValue("_alreadyFetchedPerformanceComponent", _alreadyFetchedPerformanceComponent);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PerformanceComponent":
					_alreadyFetchedPerformanceComponent = true;
					this.PerformanceComponent = (PerformanceComponentEntity)entity;
					break;
				case "Performances":
					_alreadyFetchedPerformances = true;
					if(entity!=null)
					{
						this.Performances.Add((PerformanceEntity)entity);
					}
					break;
				case "Aggregations":
					_alreadyFetchedAggregations = true;
					if(entity!=null)
					{
						this.Aggregations.Add((PerformanceAggregationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PerformanceComponent":
					SetupSyncPerformanceComponent(relatedEntity);
					break;
				case "Performances":
					_performances.Add((PerformanceEntity)relatedEntity);
					break;
				case "Aggregations":
					_aggregations.Add((PerformanceAggregationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PerformanceComponent":
					DesetupSyncPerformanceComponent(false, true);
					break;
				case "Performances":
					this.PerformRelatedEntityRemoval(_performances, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Aggregations":
					this.PerformRelatedEntityRemoval(_aggregations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_performanceComponent!=null)
			{
				toReturn.Add(_performanceComponent);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_performances);
			toReturn.Add(_aggregations);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 indicatorID)
		{
			return FetchUsingPK(indicatorID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 indicatorID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(indicatorID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 indicatorID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(indicatorID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 indicatorID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(indicatorID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.IndicatorID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PerformanceIndicatorRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PerformanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PerformanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceCollection GetMultiPerformances(bool forceFetch)
		{
			return GetMultiPerformances(forceFetch, _performances.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PerformanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceCollection GetMultiPerformances(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPerformances(forceFetch, _performances.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceCollection GetMultiPerformances(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPerformances(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PerformanceCollection GetMultiPerformances(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPerformances || forceFetch || _alwaysFetchPerformances) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_performances);
				_performances.SuppressClearInGetMulti=!forceFetch;
				_performances.EntityFactoryToUse = entityFactoryToUse;
				_performances.GetMultiManyToOne(this, filter);
				_performances.SuppressClearInGetMulti=false;
				_alreadyFetchedPerformances = true;
			}
			return _performances;
		}

		/// <summary> Sets the collection parameters for the collection for 'Performances'. These settings will be taken into account
		/// when the property Performances is requested or GetMultiPerformances is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPerformances(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_performances.SortClauses=sortClauses;
			_performances.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PerformanceAggregationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PerformanceAggregationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection GetMultiAggregations(bool forceFetch)
		{
			return GetMultiAggregations(forceFetch, _aggregations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceAggregationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PerformanceAggregationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection GetMultiAggregations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAggregations(forceFetch, _aggregations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceAggregationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection GetMultiAggregations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAggregations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PerformanceAggregationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection GetMultiAggregations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAggregations || forceFetch || _alwaysFetchAggregations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_aggregations);
				_aggregations.SuppressClearInGetMulti=!forceFetch;
				_aggregations.EntityFactoryToUse = entityFactoryToUse;
				_aggregations.GetMultiManyToOne(this, filter);
				_aggregations.SuppressClearInGetMulti=false;
				_alreadyFetchedAggregations = true;
			}
			return _aggregations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Aggregations'. These settings will be taken into account
		/// when the property Aggregations is requested or GetMultiAggregations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAggregations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_aggregations.SortClauses=sortClauses;
			_aggregations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PerformanceComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PerformanceComponentEntity' which is related to this entity.</returns>
		public PerformanceComponentEntity GetSinglePerformanceComponent()
		{
			return GetSinglePerformanceComponent(false);
		}

		/// <summary> Retrieves the related entity of type 'PerformanceComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PerformanceComponentEntity' which is related to this entity.</returns>
		public virtual PerformanceComponentEntity GetSinglePerformanceComponent(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerformanceComponent || forceFetch || _alwaysFetchPerformanceComponent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PerformanceComponentEntityUsingComponentID);
				PerformanceComponentEntity newEntity = new PerformanceComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ComponentID);
				}
				if(fetchResult)
				{
					newEntity = (PerformanceComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_performanceComponentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PerformanceComponent = newEntity;
				_alreadyFetchedPerformanceComponent = fetchResult;
			}
			return _performanceComponent;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PerformanceComponent", _performanceComponent);
			toReturn.Add("Performances", _performances);
			toReturn.Add("Aggregations", _aggregations);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="validator">The validator object for this PerformanceIndicatorEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 indicatorID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(indicatorID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_performances = new VarioSL.Entities.CollectionClasses.PerformanceCollection();
			_performances.SetContainingEntityInfo(this, "PerformanceIndicator");

			_aggregations = new VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection();
			_aggregations.SetContainingEntityInfo(this, "PerformanceIndicator");
			_performanceComponentReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IndicatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _performanceComponent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerformanceComponent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _performanceComponent, new PropertyChangedEventHandler( OnPerformanceComponentPropertyChanged ), "PerformanceComponent", VarioSL.Entities.RelationClasses.StaticPerformanceIndicatorRelations.PerformanceComponentEntityUsingComponentIDStatic, true, signalRelatedEntity, "PerformanceIndicators", resetFKFields, new int[] { (int)PerformanceIndicatorFieldIndex.ComponentID } );		
			_performanceComponent = null;
		}
		
		/// <summary> setups the sync logic for member _performanceComponent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerformanceComponent(IEntityCore relatedEntity)
		{
			if(_performanceComponent!=relatedEntity)
			{		
				DesetupSyncPerformanceComponent(true, true);
				_performanceComponent = (PerformanceComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _performanceComponent, new PropertyChangedEventHandler( OnPerformanceComponentPropertyChanged ), "PerformanceComponent", VarioSL.Entities.RelationClasses.StaticPerformanceIndicatorRelations.PerformanceComponentEntityUsingComponentIDStatic, true, ref _alreadyFetchedPerformanceComponent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPerformanceComponentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="indicatorID">PK value for PerformanceIndicator which data should be fetched into this PerformanceIndicator object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 indicatorID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PerformanceIndicatorFieldIndex.IndicatorID].ForcedCurrentValueWrite(indicatorID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePerformanceIndicatorDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PerformanceIndicatorEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PerformanceIndicatorRelations Relations
		{
			get	{ return new PerformanceIndicatorRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Performance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerformances
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PerformanceCollection(), (IEntityRelation)GetRelationsForField("Performances")[0], (int)VarioSL.Entities.EntityType.PerformanceIndicatorEntity, (int)VarioSL.Entities.EntityType.PerformanceEntity, 0, null, null, null, "Performances", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PerformanceAggregation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAggregations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection(), (IEntityRelation)GetRelationsForField("Aggregations")[0], (int)VarioSL.Entities.EntityType.PerformanceIndicatorEntity, (int)VarioSL.Entities.EntityType.PerformanceAggregationEntity, 0, null, null, null, "Aggregations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PerformanceComponent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerformanceComponent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PerformanceComponentCollection(), (IEntityRelation)GetRelationsForField("PerformanceComponent")[0], (int)VarioSL.Entities.EntityType.PerformanceIndicatorEntity, (int)VarioSL.Entities.EntityType.PerformanceComponentEntity, 0, null, null, null, "PerformanceComponent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ComponentID property of the Entity PerformanceIndicator<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_INDICATOR"."COMPONENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ComponentID
		{
			get { return (System.Int64)GetValue((int)PerformanceIndicatorFieldIndex.ComponentID, true); }
			set	{ SetValue((int)PerformanceIndicatorFieldIndex.ComponentID, value, true); }
		}

		/// <summary> The IndicatorID property of the Entity PerformanceIndicator<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_INDICATOR"."INDICATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 IndicatorID
		{
			get { return (System.Int64)GetValue((int)PerformanceIndicatorFieldIndex.IndicatorID, true); }
			set	{ SetValue((int)PerformanceIndicatorFieldIndex.IndicatorID, value, true); }
		}

		/// <summary> The Name property of the Entity PerformanceIndicator<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_INDICATOR"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)PerformanceIndicatorFieldIndex.Name, true); }
			set	{ SetValue((int)PerformanceIndicatorFieldIndex.Name, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PerformanceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPerformances()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PerformanceCollection Performances
		{
			get	{ return GetMultiPerformances(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Performances. When set to true, Performances is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Performances is accessed. You can always execute/ a forced fetch by calling GetMultiPerformances(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerformances
		{
			get	{ return _alwaysFetchPerformances; }
			set	{ _alwaysFetchPerformances = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Performances already has been fetched. Setting this property to false when Performances has been fetched
		/// will clear the Performances collection well. Setting this property to true while Performances hasn't been fetched disables lazy loading for Performances</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerformances
		{
			get { return _alreadyFetchedPerformances;}
			set 
			{
				if(_alreadyFetchedPerformances && !value && (_performances != null))
				{
					_performances.Clear();
				}
				_alreadyFetchedPerformances = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PerformanceAggregationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAggregations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PerformanceAggregationCollection Aggregations
		{
			get	{ return GetMultiAggregations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Aggregations. When set to true, Aggregations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Aggregations is accessed. You can always execute/ a forced fetch by calling GetMultiAggregations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAggregations
		{
			get	{ return _alwaysFetchAggregations; }
			set	{ _alwaysFetchAggregations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Aggregations already has been fetched. Setting this property to false when Aggregations has been fetched
		/// will clear the Aggregations collection well. Setting this property to true while Aggregations hasn't been fetched disables lazy loading for Aggregations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAggregations
		{
			get { return _alreadyFetchedAggregations;}
			set 
			{
				if(_alreadyFetchedAggregations && !value && (_aggregations != null))
				{
					_aggregations.Clear();
				}
				_alreadyFetchedAggregations = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PerformanceComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerformanceComponent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PerformanceComponentEntity PerformanceComponent
		{
			get	{ return GetSinglePerformanceComponent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerformanceComponent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PerformanceIndicators", "PerformanceComponent", _performanceComponent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PerformanceComponent. When set to true, PerformanceComponent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PerformanceComponent is accessed. You can always execute a forced fetch by calling GetSinglePerformanceComponent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerformanceComponent
		{
			get	{ return _alwaysFetchPerformanceComponent; }
			set	{ _alwaysFetchPerformanceComponent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PerformanceComponent already has been fetched. Setting this property to false when PerformanceComponent has been fetched
		/// will set PerformanceComponent to null as well. Setting this property to true while PerformanceComponent hasn't been fetched disables lazy loading for PerformanceComponent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerformanceComponent
		{
			get { return _alreadyFetchedPerformanceComponent;}
			set 
			{
				if(_alreadyFetchedPerformanceComponent && !value)
				{
					this.PerformanceComponent = null;
				}
				_alreadyFetchedPerformanceComponent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PerformanceComponent is not found
		/// in the database. When set to true, PerformanceComponent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PerformanceComponentReturnsNewIfNotFound
		{
			get	{ return _performanceComponentReturnsNewIfNotFound; }
			set { _performanceComponentReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PerformanceIndicatorEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
