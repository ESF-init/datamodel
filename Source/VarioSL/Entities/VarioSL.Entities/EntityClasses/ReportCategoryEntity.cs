﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ReportCategory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ReportCategoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ReportCollection	_reports;
		private bool	_alwaysFetchReports, _alreadyFetchedReports;
		private VarioSL.Entities.CollectionClasses.ReportCategoryCollection	_subReportCategories;
		private bool	_alwaysFetchSubReportCategories, _alreadyFetchedSubReportCategories;
		private UserResourceEntity _userResource;
		private bool	_alwaysFetchUserResource, _alreadyFetchedUserResource, _userResourceReturnsNewIfNotFound;
		private FilterListEntity _filterList;
		private bool	_alwaysFetchFilterList, _alreadyFetchedFilterList, _filterListReturnsNewIfNotFound;
		private ReportCategoryEntity _masterReportCategory;
		private bool	_alwaysFetchMasterReportCategory, _alreadyFetchedMasterReportCategory, _masterReportCategoryReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserResource</summary>
			public static readonly string UserResource = "UserResource";
			/// <summary>Member name FilterList</summary>
			public static readonly string FilterList = "FilterList";
			/// <summary>Member name MasterReportCategory</summary>
			public static readonly string MasterReportCategory = "MasterReportCategory";
			/// <summary>Member name Reports</summary>
			public static readonly string Reports = "Reports";
			/// <summary>Member name SubReportCategories</summary>
			public static readonly string SubReportCategories = "SubReportCategories";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportCategoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ReportCategoryEntity() :base("ReportCategoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		public ReportCategoryEntity(System.Int64 reportCategoryID):base("ReportCategoryEntity")
		{
			InitClassFetch(reportCategoryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReportCategoryEntity(System.Int64 reportCategoryID, IPrefetchPath prefetchPathToUse):base("ReportCategoryEntity")
		{
			InitClassFetch(reportCategoryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="validator">The custom validator object for this ReportCategoryEntity</param>
		public ReportCategoryEntity(System.Int64 reportCategoryID, IValidator validator):base("ReportCategoryEntity")
		{
			InitClassFetch(reportCategoryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_reports = (VarioSL.Entities.CollectionClasses.ReportCollection)info.GetValue("_reports", typeof(VarioSL.Entities.CollectionClasses.ReportCollection));
			_alwaysFetchReports = info.GetBoolean("_alwaysFetchReports");
			_alreadyFetchedReports = info.GetBoolean("_alreadyFetchedReports");

			_subReportCategories = (VarioSL.Entities.CollectionClasses.ReportCategoryCollection)info.GetValue("_subReportCategories", typeof(VarioSL.Entities.CollectionClasses.ReportCategoryCollection));
			_alwaysFetchSubReportCategories = info.GetBoolean("_alwaysFetchSubReportCategories");
			_alreadyFetchedSubReportCategories = info.GetBoolean("_alreadyFetchedSubReportCategories");
			_userResource = (UserResourceEntity)info.GetValue("_userResource", typeof(UserResourceEntity));
			if(_userResource!=null)
			{
				_userResource.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userResourceReturnsNewIfNotFound = info.GetBoolean("_userResourceReturnsNewIfNotFound");
			_alwaysFetchUserResource = info.GetBoolean("_alwaysFetchUserResource");
			_alreadyFetchedUserResource = info.GetBoolean("_alreadyFetchedUserResource");

			_filterList = (FilterListEntity)info.GetValue("_filterList", typeof(FilterListEntity));
			if(_filterList!=null)
			{
				_filterList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterListReturnsNewIfNotFound = info.GetBoolean("_filterListReturnsNewIfNotFound");
			_alwaysFetchFilterList = info.GetBoolean("_alwaysFetchFilterList");
			_alreadyFetchedFilterList = info.GetBoolean("_alreadyFetchedFilterList");

			_masterReportCategory = (ReportCategoryEntity)info.GetValue("_masterReportCategory", typeof(ReportCategoryEntity));
			if(_masterReportCategory!=null)
			{
				_masterReportCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_masterReportCategoryReturnsNewIfNotFound = info.GetBoolean("_masterReportCategoryReturnsNewIfNotFound");
			_alwaysFetchMasterReportCategory = info.GetBoolean("_alwaysFetchMasterReportCategory");
			_alreadyFetchedMasterReportCategory = info.GetBoolean("_alreadyFetchedMasterReportCategory");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportCategoryFieldIndex)fieldIndex)
			{
				case ReportCategoryFieldIndex.CategoryFilterListID:
					DesetupSyncFilterList(true, false);
					_alreadyFetchedFilterList = false;
					break;
				case ReportCategoryFieldIndex.MasterCategoryID:
					DesetupSyncMasterReportCategory(true, false);
					_alreadyFetchedMasterReportCategory = false;
					break;
				case ReportCategoryFieldIndex.ResourceID:
					DesetupSyncUserResource(true, false);
					_alreadyFetchedUserResource = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedReports = (_reports.Count > 0);
			_alreadyFetchedSubReportCategories = (_subReportCategories.Count > 0);
			_alreadyFetchedUserResource = (_userResource != null);
			_alreadyFetchedFilterList = (_filterList != null);
			_alreadyFetchedMasterReportCategory = (_masterReportCategory != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserResource":
					toReturn.Add(Relations.UserResourceEntityUsingResourceID);
					break;
				case "FilterList":
					toReturn.Add(Relations.FilterListEntityUsingCategoryFilterListID);
					break;
				case "MasterReportCategory":
					toReturn.Add(Relations.ReportCategoryEntityUsingReportCategoryIDMasterCategoryID);
					break;
				case "Reports":
					toReturn.Add(Relations.ReportEntityUsingCategoryID);
					break;
				case "SubReportCategories":
					toReturn.Add(Relations.ReportCategoryEntityUsingMasterCategoryID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_reports", (!this.MarkedForDeletion?_reports:null));
			info.AddValue("_alwaysFetchReports", _alwaysFetchReports);
			info.AddValue("_alreadyFetchedReports", _alreadyFetchedReports);
			info.AddValue("_subReportCategories", (!this.MarkedForDeletion?_subReportCategories:null));
			info.AddValue("_alwaysFetchSubReportCategories", _alwaysFetchSubReportCategories);
			info.AddValue("_alreadyFetchedSubReportCategories", _alreadyFetchedSubReportCategories);
			info.AddValue("_userResource", (!this.MarkedForDeletion?_userResource:null));
			info.AddValue("_userResourceReturnsNewIfNotFound", _userResourceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserResource", _alwaysFetchUserResource);
			info.AddValue("_alreadyFetchedUserResource", _alreadyFetchedUserResource);
			info.AddValue("_filterList", (!this.MarkedForDeletion?_filterList:null));
			info.AddValue("_filterListReturnsNewIfNotFound", _filterListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterList", _alwaysFetchFilterList);
			info.AddValue("_alreadyFetchedFilterList", _alreadyFetchedFilterList);
			info.AddValue("_masterReportCategory", (!this.MarkedForDeletion?_masterReportCategory:null));
			info.AddValue("_masterReportCategoryReturnsNewIfNotFound", _masterReportCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMasterReportCategory", _alwaysFetchMasterReportCategory);
			info.AddValue("_alreadyFetchedMasterReportCategory", _alreadyFetchedMasterReportCategory);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserResource":
					_alreadyFetchedUserResource = true;
					this.UserResource = (UserResourceEntity)entity;
					break;
				case "FilterList":
					_alreadyFetchedFilterList = true;
					this.FilterList = (FilterListEntity)entity;
					break;
				case "MasterReportCategory":
					_alreadyFetchedMasterReportCategory = true;
					this.MasterReportCategory = (ReportCategoryEntity)entity;
					break;
				case "Reports":
					_alreadyFetchedReports = true;
					if(entity!=null)
					{
						this.Reports.Add((ReportEntity)entity);
					}
					break;
				case "SubReportCategories":
					_alreadyFetchedSubReportCategories = true;
					if(entity!=null)
					{
						this.SubReportCategories.Add((ReportCategoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserResource":
					SetupSyncUserResource(relatedEntity);
					break;
				case "FilterList":
					SetupSyncFilterList(relatedEntity);
					break;
				case "MasterReportCategory":
					SetupSyncMasterReportCategory(relatedEntity);
					break;
				case "Reports":
					_reports.Add((ReportEntity)relatedEntity);
					break;
				case "SubReportCategories":
					_subReportCategories.Add((ReportCategoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserResource":
					DesetupSyncUserResource(false, true);
					break;
				case "FilterList":
					DesetupSyncFilterList(false, true);
					break;
				case "MasterReportCategory":
					DesetupSyncMasterReportCategory(false, true);
					break;
				case "Reports":
					this.PerformRelatedEntityRemoval(_reports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SubReportCategories":
					this.PerformRelatedEntityRemoval(_subReportCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userResource!=null)
			{
				toReturn.Add(_userResource);
			}
			if(_filterList!=null)
			{
				toReturn.Add(_filterList);
			}
			if(_masterReportCategory!=null)
			{
				toReturn.Add(_masterReportCategory);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_reports);
			toReturn.Add(_subReportCategories);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportCategoryID)
		{
			return FetchUsingPK(reportCategoryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportCategoryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportCategoryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportCategoryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportCategoryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportCategoryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReports || forceFetch || _alwaysFetchReports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reports);
				_reports.SuppressClearInGetMulti=!forceFetch;
				_reports.EntityFactoryToUse = entityFactoryToUse;
				_reports.GetMultiManyToOne(null, null, this, null, filter);
				_reports.SuppressClearInGetMulti=false;
				_alreadyFetchedReports = true;
			}
			return _reports;
		}

		/// <summary> Sets the collection parameters for the collection for 'Reports'. These settings will be taken into account
		/// when the property Reports is requested or GetMultiReports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reports.SortClauses=sortClauses;
			_reports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiSubReportCategories(bool forceFetch)
		{
			return GetMultiSubReportCategories(forceFetch, _subReportCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiSubReportCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSubReportCategories(forceFetch, _subReportCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiSubReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSubReportCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiSubReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSubReportCategories || forceFetch || _alwaysFetchSubReportCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_subReportCategories);
				_subReportCategories.SuppressClearInGetMulti=!forceFetch;
				_subReportCategories.EntityFactoryToUse = entityFactoryToUse;
				_subReportCategories.GetMultiManyToOne(null, null, this, filter);
				_subReportCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedSubReportCategories = true;
			}
			return _subReportCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'SubReportCategories'. These settings will be taken into account
		/// when the property SubReportCategories is requested or GetMultiSubReportCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSubReportCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_subReportCategories.SortClauses=sortClauses;
			_subReportCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public UserResourceEntity GetSingleUserResource()
		{
			return GetSingleUserResource(false);
		}

		/// <summary> Retrieves the related entity of type 'UserResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserResourceEntity' which is related to this entity.</returns>
		public virtual UserResourceEntity GetSingleUserResource(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserResource || forceFetch || _alwaysFetchUserResource) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserResourceEntityUsingResourceID);
				UserResourceEntity newEntity = new UserResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ResourceID);
				}
				if(fetchResult)
				{
					newEntity = (UserResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userResourceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserResource = newEntity;
				_alreadyFetchedUserResource = fetchResult;
			}
			return _userResource;
		}


		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public FilterListEntity GetSingleFilterList()
		{
			return GetSingleFilterList(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public virtual FilterListEntity GetSingleFilterList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterList || forceFetch || _alwaysFetchFilterList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterListEntityUsingCategoryFilterListID);
				FilterListEntity newEntity = new FilterListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryFilterListID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FilterListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterList = newEntity;
				_alreadyFetchedFilterList = fetchResult;
			}
			return _filterList;
		}


		/// <summary> Retrieves the related entity of type 'ReportCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportCategoryEntity' which is related to this entity.</returns>
		public ReportCategoryEntity GetSingleMasterReportCategory()
		{
			return GetSingleMasterReportCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportCategoryEntity' which is related to this entity.</returns>
		public virtual ReportCategoryEntity GetSingleMasterReportCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedMasterReportCategory || forceFetch || _alwaysFetchMasterReportCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportCategoryEntityUsingReportCategoryIDMasterCategoryID);
				ReportCategoryEntity newEntity = new ReportCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MasterCategoryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_masterReportCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MasterReportCategory = newEntity;
				_alreadyFetchedMasterReportCategory = fetchResult;
			}
			return _masterReportCategory;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserResource", _userResource);
			toReturn.Add("FilterList", _filterList);
			toReturn.Add("MasterReportCategory", _masterReportCategory);
			toReturn.Add("Reports", _reports);
			toReturn.Add("SubReportCategories", _subReportCategories);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="validator">The validator object for this ReportCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 reportCategoryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportCategoryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_reports = new VarioSL.Entities.CollectionClasses.ReportCollection();
			_reports.SetContainingEntityInfo(this, "ReportCategory");

			_subReportCategories = new VarioSL.Entities.CollectionClasses.ReportCategoryCollection();
			_subReportCategories.SetContainingEntityInfo(this, "MasterReportCategory");
			_userResourceReturnsNewIfNotFound = false;
			_filterListReturnsNewIfNotFound = false;
			_masterReportCategoryReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryFilterListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userResource</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserResource(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.UserResourceEntityUsingResourceIDStatic, true, signalRelatedEntity, "ReportCategories", resetFKFields, new int[] { (int)ReportCategoryFieldIndex.ResourceID } );		
			_userResource = null;
		}
		
		/// <summary> setups the sync logic for member _userResource</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserResource(IEntityCore relatedEntity)
		{
			if(_userResource!=relatedEntity)
			{		
				DesetupSyncUserResource(true, true);
				_userResource = (UserResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userResource, new PropertyChangedEventHandler( OnUserResourcePropertyChanged ), "UserResource", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.UserResourceEntityUsingResourceIDStatic, true, ref _alreadyFetchedUserResource, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserResourcePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.FilterListEntityUsingCategoryFilterListIDStatic, true, signalRelatedEntity, "ReportCategories", resetFKFields, new int[] { (int)ReportCategoryFieldIndex.CategoryFilterListID } );		
			_filterList = null;
		}
		
		/// <summary> setups the sync logic for member _filterList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterList(IEntityCore relatedEntity)
		{
			if(_filterList!=relatedEntity)
			{		
				DesetupSyncFilterList(true, true);
				_filterList = (FilterListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.FilterListEntityUsingCategoryFilterListIDStatic, true, ref _alreadyFetchedFilterList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _masterReportCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMasterReportCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _masterReportCategory, new PropertyChangedEventHandler( OnMasterReportCategoryPropertyChanged ), "MasterReportCategory", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.ReportCategoryEntityUsingReportCategoryIDMasterCategoryIDStatic, true, signalRelatedEntity, "SubReportCategories", resetFKFields, new int[] { (int)ReportCategoryFieldIndex.MasterCategoryID } );		
			_masterReportCategory = null;
		}
		
		/// <summary> setups the sync logic for member _masterReportCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMasterReportCategory(IEntityCore relatedEntity)
		{
			if(_masterReportCategory!=relatedEntity)
			{		
				DesetupSyncMasterReportCategory(true, true);
				_masterReportCategory = (ReportCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _masterReportCategory, new PropertyChangedEventHandler( OnMasterReportCategoryPropertyChanged ), "MasterReportCategory", VarioSL.Entities.RelationClasses.StaticReportCategoryRelations.ReportCategoryEntityUsingReportCategoryIDMasterCategoryIDStatic, true, ref _alreadyFetchedMasterReportCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMasterReportCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportCategoryID">PK value for ReportCategory which data should be fetched into this ReportCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 reportCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportCategoryFieldIndex.ReportCategoryID].ForcedCurrentValueWrite(reportCategoryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportCategoryRelations Relations
		{
			get	{ return new ReportCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), (IEntityRelation)GetRelationsForField("Reports")[0], (int)VarioSL.Entities.EntityType.ReportCategoryEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, null, "Reports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSubReportCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCategoryCollection(), (IEntityRelation)GetRelationsForField("SubReportCategories")[0], (int)VarioSL.Entities.EntityType.ReportCategoryEntity, (int)VarioSL.Entities.EntityType.ReportCategoryEntity, 0, null, null, null, "SubReportCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserResource
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserResourceCollection(), (IEntityRelation)GetRelationsForField("UserResource")[0], (int)VarioSL.Entities.EntityType.ReportCategoryEntity, (int)VarioSL.Entities.EntityType.UserResourceEntity, 0, null, null, null, "UserResource", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListCollection(), (IEntityRelation)GetRelationsForField("FilterList")[0], (int)VarioSL.Entities.EntityType.ReportCategoryEntity, (int)VarioSL.Entities.EntityType.FilterListEntity, 0, null, null, null, "FilterList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMasterReportCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCategoryCollection(), (IEntityRelation)GetRelationsForField("MasterReportCategory")[0], (int)VarioSL.Entities.EntityType.ReportCategoryEntity, (int)VarioSL.Entities.EntityType.ReportCategoryEntity, 0, null, null, null, "MasterReportCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CategoryFilterListID property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."CATEGORYFILTERLISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CategoryFilterListID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportCategoryFieldIndex.CategoryFilterListID, false); }
			set	{ SetValue((int)ReportCategoryFieldIndex.CategoryFilterListID, value, true); }
		}

		/// <summary> The Description property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ReportCategoryFieldIndex.Description, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.Description, value, true); }
		}

		/// <summary> The DisplayOrder property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."DISPLAYORDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DisplayOrder
		{
			get { return (System.Int32)GetValue((int)ReportCategoryFieldIndex.DisplayOrder, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.DisplayOrder, value, true); }
		}

		/// <summary> The LastModified property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ReportCategoryFieldIndex.LastModified, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ReportCategoryFieldIndex.LastUser, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MasterCategoryID property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."MASTERCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MasterCategoryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportCategoryFieldIndex.MasterCategoryID, false); }
			set	{ SetValue((int)ReportCategoryFieldIndex.MasterCategoryID, value, true); }
		}

		/// <summary> The Name property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ReportCategoryFieldIndex.Name, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The ReportCategoryID property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."REPORTCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ReportCategoryID
		{
			get { return (System.Int64)GetValue((int)ReportCategoryFieldIndex.ReportCategoryID, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.ReportCategoryID, value, true); }
		}

		/// <summary> The ResourceID property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."RESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResourceID
		{
			get { return (System.String)GetValue((int)ReportCategoryFieldIndex.ResourceID, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.ResourceID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ReportCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTCATEGORY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ReportCategoryFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ReportCategoryFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection Reports
		{
			get	{ return GetMultiReports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Reports. When set to true, Reports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Reports is accessed. You can always execute/ a forced fetch by calling GetMultiReports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReports
		{
			get	{ return _alwaysFetchReports; }
			set	{ _alwaysFetchReports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Reports already has been fetched. Setting this property to false when Reports has been fetched
		/// will clear the Reports collection well. Setting this property to true while Reports hasn't been fetched disables lazy loading for Reports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReports
		{
			get { return _alreadyFetchedReports;}
			set 
			{
				if(_alreadyFetchedReports && !value && (_reports != null))
				{
					_reports.Clear();
				}
				_alreadyFetchedReports = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSubReportCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection SubReportCategories
		{
			get	{ return GetMultiSubReportCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SubReportCategories. When set to true, SubReportCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SubReportCategories is accessed. You can always execute/ a forced fetch by calling GetMultiSubReportCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSubReportCategories
		{
			get	{ return _alwaysFetchSubReportCategories; }
			set	{ _alwaysFetchSubReportCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SubReportCategories already has been fetched. Setting this property to false when SubReportCategories has been fetched
		/// will clear the SubReportCategories collection well. Setting this property to true while SubReportCategories hasn't been fetched disables lazy loading for SubReportCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSubReportCategories
		{
			get { return _alreadyFetchedSubReportCategories;}
			set 
			{
				if(_alreadyFetchedSubReportCategories && !value && (_subReportCategories != null))
				{
					_subReportCategories.Clear();
				}
				_alreadyFetchedSubReportCategories = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserResource()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserResourceEntity UserResource
		{
			get	{ return GetSingleUserResource(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserResource(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportCategories", "UserResource", _userResource, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserResource. When set to true, UserResource is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserResource is accessed. You can always execute a forced fetch by calling GetSingleUserResource(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserResource
		{
			get	{ return _alwaysFetchUserResource; }
			set	{ _alwaysFetchUserResource = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserResource already has been fetched. Setting this property to false when UserResource has been fetched
		/// will set UserResource to null as well. Setting this property to true while UserResource hasn't been fetched disables lazy loading for UserResource</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserResource
		{
			get { return _alreadyFetchedUserResource;}
			set 
			{
				if(_alreadyFetchedUserResource && !value)
				{
					this.UserResource = null;
				}
				_alreadyFetchedUserResource = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserResource is not found
		/// in the database. When set to true, UserResource will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserResourceReturnsNewIfNotFound
		{
			get	{ return _userResourceReturnsNewIfNotFound; }
			set { _userResourceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterListEntity FilterList
		{
			get	{ return GetSingleFilterList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportCategories", "FilterList", _filterList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterList. When set to true, FilterList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterList is accessed. You can always execute a forced fetch by calling GetSingleFilterList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterList
		{
			get	{ return _alwaysFetchFilterList; }
			set	{ _alwaysFetchFilterList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterList already has been fetched. Setting this property to false when FilterList has been fetched
		/// will set FilterList to null as well. Setting this property to true while FilterList hasn't been fetched disables lazy loading for FilterList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterList
		{
			get { return _alreadyFetchedFilterList;}
			set 
			{
				if(_alreadyFetchedFilterList && !value)
				{
					this.FilterList = null;
				}
				_alreadyFetchedFilterList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterList is not found
		/// in the database. When set to true, FilterList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterListReturnsNewIfNotFound
		{
			get	{ return _filterListReturnsNewIfNotFound; }
			set { _filterListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMasterReportCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportCategoryEntity MasterReportCategory
		{
			get	{ return GetSingleMasterReportCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMasterReportCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SubReportCategories", "MasterReportCategory", _masterReportCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MasterReportCategory. When set to true, MasterReportCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MasterReportCategory is accessed. You can always execute a forced fetch by calling GetSingleMasterReportCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMasterReportCategory
		{
			get	{ return _alwaysFetchMasterReportCategory; }
			set	{ _alwaysFetchMasterReportCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MasterReportCategory already has been fetched. Setting this property to false when MasterReportCategory has been fetched
		/// will set MasterReportCategory to null as well. Setting this property to true while MasterReportCategory hasn't been fetched disables lazy loading for MasterReportCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMasterReportCategory
		{
			get { return _alreadyFetchedMasterReportCategory;}
			set 
			{
				if(_alreadyFetchedMasterReportCategory && !value)
				{
					this.MasterReportCategory = null;
				}
				_alreadyFetchedMasterReportCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MasterReportCategory is not found
		/// in the database. When set to true, MasterReportCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MasterReportCategoryReturnsNewIfNotFound
		{
			get	{ return _masterReportCategoryReturnsNewIfNotFound; }
			set { _masterReportCategoryReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ReportCategoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
