﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomAttributeValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomAttributeValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection	_customAttributeValueToPeople;
		private bool	_alwaysFetchCustomAttributeValueToPeople, _alreadyFetchedCustomAttributeValueToPeople;
		private CustomAttributeEntity _customAttribute;
		private bool	_alwaysFetchCustomAttribute, _alreadyFetchedCustomAttribute, _customAttributeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomAttribute</summary>
			public static readonly string CustomAttribute = "CustomAttribute";
			/// <summary>Member name CustomAttributeValueToPeople</summary>
			public static readonly string CustomAttributeValueToPeople = "CustomAttributeValueToPeople";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomAttributeValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomAttributeValueEntity() :base("CustomAttributeValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		public CustomAttributeValueEntity(System.Int64 customAttributeValueID):base("CustomAttributeValueEntity")
		{
			InitClassFetch(customAttributeValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomAttributeValueEntity(System.Int64 customAttributeValueID, IPrefetchPath prefetchPathToUse):base("CustomAttributeValueEntity")
		{
			InitClassFetch(customAttributeValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="validator">The custom validator object for this CustomAttributeValueEntity</param>
		public CustomAttributeValueEntity(System.Int64 customAttributeValueID, IValidator validator):base("CustomAttributeValueEntity")
		{
			InitClassFetch(customAttributeValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomAttributeValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customAttributeValueToPeople = (VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection)info.GetValue("_customAttributeValueToPeople", typeof(VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection));
			_alwaysFetchCustomAttributeValueToPeople = info.GetBoolean("_alwaysFetchCustomAttributeValueToPeople");
			_alreadyFetchedCustomAttributeValueToPeople = info.GetBoolean("_alreadyFetchedCustomAttributeValueToPeople");
			_customAttribute = (CustomAttributeEntity)info.GetValue("_customAttribute", typeof(CustomAttributeEntity));
			if(_customAttribute!=null)
			{
				_customAttribute.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customAttributeReturnsNewIfNotFound = info.GetBoolean("_customAttributeReturnsNewIfNotFound");
			_alwaysFetchCustomAttribute = info.GetBoolean("_alwaysFetchCustomAttribute");
			_alreadyFetchedCustomAttribute = info.GetBoolean("_alreadyFetchedCustomAttribute");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomAttributeValueFieldIndex)fieldIndex)
			{
				case CustomAttributeValueFieldIndex.CustomAttributeID:
					DesetupSyncCustomAttribute(true, false);
					_alreadyFetchedCustomAttribute = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomAttributeValueToPeople = (_customAttributeValueToPeople.Count > 0);
			_alreadyFetchedCustomAttribute = (_customAttribute != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomAttribute":
					toReturn.Add(Relations.CustomAttributeEntityUsingCustomAttributeID);
					break;
				case "CustomAttributeValueToPeople":
					toReturn.Add(Relations.CustomAttributeValueToPersonEntityUsingCustomAttributeValueID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customAttributeValueToPeople", (!this.MarkedForDeletion?_customAttributeValueToPeople:null));
			info.AddValue("_alwaysFetchCustomAttributeValueToPeople", _alwaysFetchCustomAttributeValueToPeople);
			info.AddValue("_alreadyFetchedCustomAttributeValueToPeople", _alreadyFetchedCustomAttributeValueToPeople);
			info.AddValue("_customAttribute", (!this.MarkedForDeletion?_customAttribute:null));
			info.AddValue("_customAttributeReturnsNewIfNotFound", _customAttributeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomAttribute", _alwaysFetchCustomAttribute);
			info.AddValue("_alreadyFetchedCustomAttribute", _alreadyFetchedCustomAttribute);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomAttribute":
					_alreadyFetchedCustomAttribute = true;
					this.CustomAttribute = (CustomAttributeEntity)entity;
					break;
				case "CustomAttributeValueToPeople":
					_alreadyFetchedCustomAttributeValueToPeople = true;
					if(entity!=null)
					{
						this.CustomAttributeValueToPeople.Add((CustomAttributeValueToPersonEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomAttribute":
					SetupSyncCustomAttribute(relatedEntity);
					break;
				case "CustomAttributeValueToPeople":
					_customAttributeValueToPeople.Add((CustomAttributeValueToPersonEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomAttribute":
					DesetupSyncCustomAttribute(false, true);
					break;
				case "CustomAttributeValueToPeople":
					this.PerformRelatedEntityRemoval(_customAttributeValueToPeople, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customAttribute!=null)
			{
				toReturn.Add(_customAttribute);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customAttributeValueToPeople);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID)
		{
			return FetchUsingPK(customAttributeValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customAttributeValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customAttributeValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customAttributeValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomAttributeValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomAttributeValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueToPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, _customAttributeValueToPeople.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueToPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, _customAttributeValueToPeople.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomAttributeValueToPeople || forceFetch || _alwaysFetchCustomAttributeValueToPeople) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customAttributeValueToPeople);
				_customAttributeValueToPeople.SuppressClearInGetMulti=!forceFetch;
				_customAttributeValueToPeople.EntityFactoryToUse = entityFactoryToUse;
				_customAttributeValueToPeople.GetMultiManyToOne(this, null, filter);
				_customAttributeValueToPeople.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomAttributeValueToPeople = true;
			}
			return _customAttributeValueToPeople;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomAttributeValueToPeople'. These settings will be taken into account
		/// when the property CustomAttributeValueToPeople is requested or GetMultiCustomAttributeValueToPeople is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomAttributeValueToPeople(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customAttributeValueToPeople.SortClauses=sortClauses;
			_customAttributeValueToPeople.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomAttributeEntity' which is related to this entity.</returns>
		public CustomAttributeEntity GetSingleCustomAttribute()
		{
			return GetSingleCustomAttribute(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomAttributeEntity' which is related to this entity.</returns>
		public virtual CustomAttributeEntity GetSingleCustomAttribute(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomAttribute || forceFetch || _alwaysFetchCustomAttribute) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomAttributeEntityUsingCustomAttributeID);
				CustomAttributeEntity newEntity = new CustomAttributeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomAttributeID);
				}
				if(fetchResult)
				{
					newEntity = (CustomAttributeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customAttributeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomAttribute = newEntity;
				_alreadyFetchedCustomAttribute = fetchResult;
			}
			return _customAttribute;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomAttribute", _customAttribute);
			toReturn.Add("CustomAttributeValueToPeople", _customAttributeValueToPeople);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="validator">The validator object for this CustomAttributeValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customAttributeValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customAttributeValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customAttributeValueToPeople = new VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection();
			_customAttributeValueToPeople.SetContainingEntityInfo(this, "CustomAttributeValue");
			_customAttributeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Value", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customAttribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customAttribute, new PropertyChangedEventHandler( OnCustomAttributePropertyChanged ), "CustomAttribute", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueRelations.CustomAttributeEntityUsingCustomAttributeIDStatic, true, signalRelatedEntity, "CustomAttributeValues", resetFKFields, new int[] { (int)CustomAttributeValueFieldIndex.CustomAttributeID } );		
			_customAttribute = null;
		}
		
		/// <summary> setups the sync logic for member _customAttribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomAttribute(IEntityCore relatedEntity)
		{
			if(_customAttribute!=relatedEntity)
			{		
				DesetupSyncCustomAttribute(true, true);
				_customAttribute = (CustomAttributeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customAttribute, new PropertyChangedEventHandler( OnCustomAttributePropertyChanged ), "CustomAttribute", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueRelations.CustomAttributeEntityUsingCustomAttributeIDStatic, true, ref _alreadyFetchedCustomAttribute, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValue which data should be fetched into this CustomAttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customAttributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomAttributeValueFieldIndex.CustomAttributeValueID].ForcedCurrentValueWrite(customAttributeValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomAttributeValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomAttributeValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomAttributeValueRelations Relations
		{
			get	{ return new CustomAttributeValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttributeValueToPerson' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttributeValueToPeople
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection(), (IEntityRelation)GetRelationsForField("CustomAttributeValueToPeople")[0], (int)VarioSL.Entities.EntityType.CustomAttributeValueEntity, (int)VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity, 0, null, null, null, "CustomAttributeValueToPeople", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttribute'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttribute
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeCollection(), (IEntityRelation)GetRelationsForField("CustomAttribute")[0], (int)VarioSL.Entities.EntityType.CustomAttributeValueEntity, (int)VarioSL.Entities.EntityType.CustomAttributeEntity, 0, null, null, null, "CustomAttribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomAttributeID property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CustomAttributeID
		{
			get { return (System.Int64)GetValue((int)CustomAttributeValueFieldIndex.CustomAttributeID, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.CustomAttributeID, value, true); }
		}

		/// <summary> The CustomAttributeValueID property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CustomAttributeValueID
		{
			get { return (System.Int64)GetValue((int)CustomAttributeValueFieldIndex.CustomAttributeValueID, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.CustomAttributeValueID, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModified
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomAttributeValueFieldIndex.LastModified, false); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomAttributeValueFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderNumber
		{
			get { return (System.Int32)GetValue((int)CustomAttributeValueFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomAttributeValueFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The Value property of the Entity CustomAttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUE"."VALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)CustomAttributeValueFieldIndex.Value, true); }
			set	{ SetValue((int)CustomAttributeValueFieldIndex.Value, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomAttributeValueToPeople()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection CustomAttributeValueToPeople
		{
			get	{ return GetMultiCustomAttributeValueToPeople(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttributeValueToPeople. When set to true, CustomAttributeValueToPeople is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttributeValueToPeople is accessed. You can always execute/ a forced fetch by calling GetMultiCustomAttributeValueToPeople(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttributeValueToPeople
		{
			get	{ return _alwaysFetchCustomAttributeValueToPeople; }
			set	{ _alwaysFetchCustomAttributeValueToPeople = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttributeValueToPeople already has been fetched. Setting this property to false when CustomAttributeValueToPeople has been fetched
		/// will clear the CustomAttributeValueToPeople collection well. Setting this property to true while CustomAttributeValueToPeople hasn't been fetched disables lazy loading for CustomAttributeValueToPeople</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttributeValueToPeople
		{
			get { return _alreadyFetchedCustomAttributeValueToPeople;}
			set 
			{
				if(_alreadyFetchedCustomAttributeValueToPeople && !value && (_customAttributeValueToPeople != null))
				{
					_customAttributeValueToPeople.Clear();
				}
				_alreadyFetchedCustomAttributeValueToPeople = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CustomAttributeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomAttribute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomAttributeEntity CustomAttribute
		{
			get	{ return GetSingleCustomAttribute(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomAttribute(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomAttributeValues", "CustomAttribute", _customAttribute, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttribute. When set to true, CustomAttribute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttribute is accessed. You can always execute a forced fetch by calling GetSingleCustomAttribute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttribute
		{
			get	{ return _alwaysFetchCustomAttribute; }
			set	{ _alwaysFetchCustomAttribute = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttribute already has been fetched. Setting this property to false when CustomAttribute has been fetched
		/// will set CustomAttribute to null as well. Setting this property to true while CustomAttribute hasn't been fetched disables lazy loading for CustomAttribute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttribute
		{
			get { return _alreadyFetchedCustomAttribute;}
			set 
			{
				if(_alreadyFetchedCustomAttribute && !value)
				{
					this.CustomAttribute = null;
				}
				_alreadyFetchedCustomAttribute = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomAttribute is not found
		/// in the database. When set to true, CustomAttribute will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomAttributeReturnsNewIfNotFound
		{
			get	{ return _customAttributeReturnsNewIfNotFound; }
			set { _customAttributeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomAttributeValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
