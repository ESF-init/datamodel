﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyDescription'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SurveyDescriptionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private SurveyEntity _survey;
		private bool	_alwaysFetchSurvey, _alreadyFetchedSurvey, _surveyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Survey</summary>
			public static readonly string Survey = "Survey";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyDescriptionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyDescriptionEntity() :base("SurveyDescriptionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		public SurveyDescriptionEntity(System.String language, System.Int64 surveyID):base("SurveyDescriptionEntity")
		{
			InitClassFetch(language, surveyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyDescriptionEntity(System.String language, System.Int64 surveyID, IPrefetchPath prefetchPathToUse):base("SurveyDescriptionEntity")
		{
			InitClassFetch(language, surveyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="validator">The custom validator object for this SurveyDescriptionEntity</param>
		public SurveyDescriptionEntity(System.String language, System.Int64 surveyID, IValidator validator):base("SurveyDescriptionEntity")
		{
			InitClassFetch(language, surveyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyDescriptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_survey = (SurveyEntity)info.GetValue("_survey", typeof(SurveyEntity));
			if(_survey!=null)
			{
				_survey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyReturnsNewIfNotFound = info.GetBoolean("_surveyReturnsNewIfNotFound");
			_alwaysFetchSurvey = info.GetBoolean("_alwaysFetchSurvey");
			_alreadyFetchedSurvey = info.GetBoolean("_alreadyFetchedSurvey");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyDescriptionFieldIndex)fieldIndex)
			{
				case SurveyDescriptionFieldIndex.SurveyID:
					DesetupSyncSurvey(true, false);
					_alreadyFetchedSurvey = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurvey = (_survey != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Survey":
					toReturn.Add(Relations.SurveyEntityUsingSurveyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_survey", (!this.MarkedForDeletion?_survey:null));
			info.AddValue("_surveyReturnsNewIfNotFound", _surveyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurvey", _alwaysFetchSurvey);
			info.AddValue("_alreadyFetchedSurvey", _alreadyFetchedSurvey);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Survey":
					_alreadyFetchedSurvey = true;
					this.Survey = (SurveyEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Survey":
					SetupSyncSurvey(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Survey":
					DesetupSyncSurvey(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_survey!=null)
			{
				toReturn.Add(_survey);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String language, System.Int64 surveyID)
		{
			return FetchUsingPK(language, surveyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String language, System.Int64 surveyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(language, surveyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String language, System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(language, surveyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String language, System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(language, surveyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.Language, this.SurveyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyDescriptionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public SurveyEntity GetSingleSurvey()
		{
			return GetSingleSurvey(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public virtual SurveyEntity GetSingleSurvey(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurvey || forceFetch || _alwaysFetchSurvey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyEntityUsingSurveyID);
				SurveyEntity newEntity = new SurveyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyID);
				}
				if(fetchResult)
				{
					newEntity = (SurveyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Survey = newEntity;
				_alreadyFetchedSurvey = fetchResult;
			}
			return _survey;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Survey", _survey);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="validator">The validator object for this SurveyDescriptionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.String language, System.Int64 surveyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(language, surveyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_surveyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Language", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _survey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurvey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyDescriptionRelations.SurveyEntityUsingSurveyIDStatic, true, signalRelatedEntity, "SurveyDescriptions", resetFKFields, new int[] { (int)SurveyDescriptionFieldIndex.SurveyID } );		
			_survey = null;
		}
		
		/// <summary> setups the sync logic for member _survey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurvey(IEntityCore relatedEntity)
		{
			if(_survey!=relatedEntity)
			{		
				DesetupSyncSurvey(true, true);
				_survey = (SurveyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyDescriptionRelations.SurveyEntityUsingSurveyIDStatic, true, ref _alreadyFetchedSurvey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="language">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="surveyID">PK value for SurveyDescription which data should be fetched into this SurveyDescription object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.String language, System.Int64 surveyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyDescriptionFieldIndex.Language].ForcedCurrentValueWrite(language);
				this.Fields[(int)SurveyDescriptionFieldIndex.SurveyID].ForcedCurrentValueWrite(surveyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyDescriptionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyDescriptionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyDescriptionRelations Relations
		{
			get	{ return new SurveyDescriptionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurvey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyCollection(), (IEntityRelation)GetRelationsForField("Survey")[0], (int)VarioSL.Entities.EntityType.SurveyDescriptionEntity, (int)VarioSL.Entities.EntityType.SurveyEntity, 0, null, null, null, "Survey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SurveyDescriptionFieldIndex.Description, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.Description, value, true); }
		}

		/// <summary> The Language property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."LANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String Language
		{
			get { return (System.String)GetValue((int)SurveyDescriptionFieldIndex.Language, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.Language, value, true); }
		}

		/// <summary> The LastModified property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SurveyDescriptionFieldIndex.LastModified, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SurveyDescriptionFieldIndex.LastUser, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SurveyDescriptionFieldIndex.Name, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.Name, value, true); }
		}

		/// <summary> The SurveyID property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."SURVEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 SurveyID
		{
			get { return (System.Int64)GetValue((int)SurveyDescriptionFieldIndex.SurveyID, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.SurveyID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SurveyDescription<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYDESCRIPTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SurveyDescriptionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SurveyDescriptionFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'SurveyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurvey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SurveyEntity Survey
		{
			get	{ return GetSingleSurvey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurvey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyDescriptions", "Survey", _survey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Survey. When set to true, Survey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Survey is accessed. You can always execute a forced fetch by calling GetSingleSurvey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurvey
		{
			get	{ return _alwaysFetchSurvey; }
			set	{ _alwaysFetchSurvey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Survey already has been fetched. Setting this property to false when Survey has been fetched
		/// will set Survey to null as well. Setting this property to true while Survey hasn't been fetched disables lazy loading for Survey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurvey
		{
			get { return _alreadyFetchedSurvey;}
			set 
			{
				if(_alreadyFetchedSurvey && !value)
				{
					this.Survey = null;
				}
				_alreadyFetchedSurvey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Survey is not found
		/// in the database. When set to true, Survey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SurveyReturnsNewIfNotFound
		{
			get	{ return _surveyReturnsNewIfNotFound; }
			set { _surveyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SurveyDescriptionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
