﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProtocolAction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProtocolActionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ProtocolMessageCollection	_protocolMessages;
		private bool	_alwaysFetchProtocolMessages, _alreadyFetchedProtocolMessages;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProtocolMessages</summary>
			public static readonly string ProtocolMessages = "ProtocolMessages";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProtocolActionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProtocolActionEntity() :base("ProtocolActionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		public ProtocolActionEntity(System.Int64 protocolActionID):base("ProtocolActionEntity")
		{
			InitClassFetch(protocolActionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProtocolActionEntity(System.Int64 protocolActionID, IPrefetchPath prefetchPathToUse):base("ProtocolActionEntity")
		{
			InitClassFetch(protocolActionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="validator">The custom validator object for this ProtocolActionEntity</param>
		public ProtocolActionEntity(System.Int64 protocolActionID, IValidator validator):base("ProtocolActionEntity")
		{
			InitClassFetch(protocolActionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProtocolActionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_protocolMessages = (VarioSL.Entities.CollectionClasses.ProtocolMessageCollection)info.GetValue("_protocolMessages", typeof(VarioSL.Entities.CollectionClasses.ProtocolMessageCollection));
			_alwaysFetchProtocolMessages = info.GetBoolean("_alwaysFetchProtocolMessages");
			_alreadyFetchedProtocolMessages = info.GetBoolean("_alreadyFetchedProtocolMessages");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProtocolMessages = (_protocolMessages.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ProtocolMessages":
					toReturn.Add(Relations.ProtocolMessageEntityUsingActionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_protocolMessages", (!this.MarkedForDeletion?_protocolMessages:null));
			info.AddValue("_alwaysFetchProtocolMessages", _alwaysFetchProtocolMessages);
			info.AddValue("_alreadyFetchedProtocolMessages", _alreadyFetchedProtocolMessages);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ProtocolMessages":
					_alreadyFetchedProtocolMessages = true;
					if(entity!=null)
					{
						this.ProtocolMessages.Add((ProtocolMessageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ProtocolMessages":
					_protocolMessages.Add((ProtocolMessageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ProtocolMessages":
					this.PerformRelatedEntityRemoval(_protocolMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_protocolMessages);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolActionID)
		{
			return FetchUsingPK(protocolActionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolActionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(protocolActionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolActionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(protocolActionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolActionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(protocolActionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProtocolActionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProtocolActionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtocolMessages(bool forceFetch)
		{
			return GetMultiProtocolMessages(forceFetch, _protocolMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtocolMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProtocolMessages(forceFetch, _protocolMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtocolMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProtocolMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtocolMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProtocolMessages || forceFetch || _alwaysFetchProtocolMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_protocolMessages);
				_protocolMessages.SuppressClearInGetMulti=!forceFetch;
				_protocolMessages.EntityFactoryToUse = entityFactoryToUse;
				_protocolMessages.GetMultiManyToOne(null, this, filter);
				_protocolMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedProtocolMessages = true;
			}
			return _protocolMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProtocolMessages'. These settings will be taken into account
		/// when the property ProtocolMessages is requested or GetMultiProtocolMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProtocolMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_protocolMessages.SortClauses=sortClauses;
			_protocolMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ProtocolMessages", _protocolMessages);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="validator">The validator object for this ProtocolActionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 protocolActionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(protocolActionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_protocolMessages = new VarioSL.Entities.CollectionClasses.ProtocolMessageCollection();
			_protocolMessages.SetContainingEntityInfo(this, "ProtocolAction");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayrollRelevant", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriorityCorrection", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProtocolActionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeToLive", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="protocolActionID">PK value for ProtocolAction which data should be fetched into this ProtocolAction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 protocolActionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProtocolActionFieldIndex.ProtocolActionID].ForcedCurrentValueWrite(protocolActionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProtocolActionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProtocolActionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProtocolActionRelations Relations
		{
			get	{ return new ProtocolActionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProtocolMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocolMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolMessageCollection(), (IEntityRelation)GetRelationsForField("ProtocolMessages")[0], (int)VarioSL.Entities.EntityType.ProtocolActionEntity, (int)VarioSL.Entities.EntityType.ProtocolMessageEntity, 0, null, null, null, "ProtocolMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ActionName property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."ACTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionName
		{
			get { return (System.String)GetValue((int)ProtocolActionFieldIndex.ActionName, true); }
			set	{ SetValue((int)ProtocolActionFieldIndex.ActionName, value, true); }
		}

		/// <summary> The ActionText property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."ACTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ActionText
		{
			get { return (System.String)GetValue((int)ProtocolActionFieldIndex.ActionText, true); }
			set	{ SetValue((int)ProtocolActionFieldIndex.ActionText, value, true); }
		}

		/// <summary> The LanguageID property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."LANGUAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LanguageID
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProtocolActionFieldIndex.LanguageID, false); }
			set	{ SetValue((int)ProtocolActionFieldIndex.LanguageID, value, true); }
		}

		/// <summary> The PayrollRelevant property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."ACTPAYROLLRELEVANCY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> PayrollRelevant
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProtocolActionFieldIndex.PayrollRelevant, false); }
			set	{ SetValue((int)ProtocolActionFieldIndex.PayrollRelevant, value, true); }
		}

		/// <summary> The PriorityCorrection property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."ACTPRIOCORRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> PriorityCorrection
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolActionFieldIndex.PriorityCorrection, false); }
			set	{ SetValue((int)ProtocolActionFieldIndex.PriorityCorrection, value, true); }
		}

		/// <summary> The ProtocolActionID property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."PROTACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ProtocolActionID
		{
			get { return (System.Int64)GetValue((int)ProtocolActionFieldIndex.ProtocolActionID, true); }
			set	{ SetValue((int)ProtocolActionFieldIndex.ProtocolActionID, value, true); }
		}

		/// <summary> The TimeToLive property of the Entity ProtocolAction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_ACTION"."TIMETOLIFE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 7, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeToLive
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProtocolActionFieldIndex.TimeToLive, false); }
			set	{ SetValue((int)ProtocolActionFieldIndex.TimeToLive, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProtocolMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolMessageCollection ProtocolMessages
		{
			get	{ return GetMultiProtocolMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProtocolMessages. When set to true, ProtocolMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProtocolMessages is accessed. You can always execute/ a forced fetch by calling GetMultiProtocolMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocolMessages
		{
			get	{ return _alwaysFetchProtocolMessages; }
			set	{ _alwaysFetchProtocolMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProtocolMessages already has been fetched. Setting this property to false when ProtocolMessages has been fetched
		/// will clear the ProtocolMessages collection well. Setting this property to true while ProtocolMessages hasn't been fetched disables lazy loading for ProtocolMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocolMessages
		{
			get { return _alreadyFetchedProtocolMessages;}
			set 
			{
				if(_alreadyFetchedProtocolMessages && !value && (_protocolMessages != null))
				{
					_protocolMessages.Clear();
				}
				_alreadyFetchedProtocolMessages = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProtocolActionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
