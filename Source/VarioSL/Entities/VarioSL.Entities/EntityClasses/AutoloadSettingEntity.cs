﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AutoloadSetting'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AutoloadSettingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection	_autoloadToPaymentOptions;
		private bool	_alwaysFetchAutoloadToPaymentOptions, _alreadyFetchedAutoloadToPaymentOptions;
		private VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection	_emailEventResendLocks;
		private bool	_alwaysFetchEmailEventResendLocks, _alreadyFetchedEmailEventResendLocks;
		private PaymentOptionEntity _altPaymentOption;
		private bool	_alwaysFetchAltPaymentOption, _alreadyFetchedAltPaymentOption, _altPaymentOptionReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AltPaymentOption</summary>
			public static readonly string AltPaymentOption = "AltPaymentOption";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name AutoloadToPaymentOptions</summary>
			public static readonly string AutoloadToPaymentOptions = "AutoloadToPaymentOptions";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AutoloadSettingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AutoloadSettingEntity() :base("AutoloadSettingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		public AutoloadSettingEntity(System.Int64 autoloadSettingID):base("AutoloadSettingEntity")
		{
			InitClassFetch(autoloadSettingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AutoloadSettingEntity(System.Int64 autoloadSettingID, IPrefetchPath prefetchPathToUse):base("AutoloadSettingEntity")
		{
			InitClassFetch(autoloadSettingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="validator">The custom validator object for this AutoloadSettingEntity</param>
		public AutoloadSettingEntity(System.Int64 autoloadSettingID, IValidator validator):base("AutoloadSettingEntity")
		{
			InitClassFetch(autoloadSettingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AutoloadSettingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_autoloadToPaymentOptions = (VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection)info.GetValue("_autoloadToPaymentOptions", typeof(VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection));
			_alwaysFetchAutoloadToPaymentOptions = info.GetBoolean("_alwaysFetchAutoloadToPaymentOptions");
			_alreadyFetchedAutoloadToPaymentOptions = info.GetBoolean("_alreadyFetchedAutoloadToPaymentOptions");

			_emailEventResendLocks = (VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection)info.GetValue("_emailEventResendLocks", typeof(VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection));
			_alwaysFetchEmailEventResendLocks = info.GetBoolean("_alwaysFetchEmailEventResendLocks");
			_alreadyFetchedEmailEventResendLocks = info.GetBoolean("_alreadyFetchedEmailEventResendLocks");
			_altPaymentOption = (PaymentOptionEntity)info.GetValue("_altPaymentOption", typeof(PaymentOptionEntity));
			if(_altPaymentOption!=null)
			{
				_altPaymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_altPaymentOptionReturnsNewIfNotFound = info.GetBoolean("_altPaymentOptionReturnsNewIfNotFound");
			_alwaysFetchAltPaymentOption = info.GetBoolean("_alwaysFetchAltPaymentOption");
			_alreadyFetchedAltPaymentOption = info.GetBoolean("_alreadyFetchedAltPaymentOption");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AutoloadSettingFieldIndex)fieldIndex)
			{
				case AutoloadSettingFieldIndex.AlternativePaymentOptionId:
					DesetupSyncAltPaymentOption(true, false);
					_alreadyFetchedAltPaymentOption = false;
					break;
				case AutoloadSettingFieldIndex.PaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				case AutoloadSettingFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutoloadToPaymentOptions = (_autoloadToPaymentOptions.Count > 0);
			_alreadyFetchedEmailEventResendLocks = (_emailEventResendLocks.Count > 0);
			_alreadyFetchedAltPaymentOption = (_altPaymentOption != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AltPaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingAlternativePaymentOptionId);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "AutoloadToPaymentOptions":
					toReturn.Add(Relations.AutoloadToPaymentOptionEntityUsingAutoloadSettingID);
					break;
				case "EmailEventResendLocks":
					toReturn.Add(Relations.EmailEventResendLockEntityUsingAutoloadSettingID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_autoloadToPaymentOptions", (!this.MarkedForDeletion?_autoloadToPaymentOptions:null));
			info.AddValue("_alwaysFetchAutoloadToPaymentOptions", _alwaysFetchAutoloadToPaymentOptions);
			info.AddValue("_alreadyFetchedAutoloadToPaymentOptions", _alreadyFetchedAutoloadToPaymentOptions);
			info.AddValue("_emailEventResendLocks", (!this.MarkedForDeletion?_emailEventResendLocks:null));
			info.AddValue("_alwaysFetchEmailEventResendLocks", _alwaysFetchEmailEventResendLocks);
			info.AddValue("_alreadyFetchedEmailEventResendLocks", _alreadyFetchedEmailEventResendLocks);
			info.AddValue("_altPaymentOption", (!this.MarkedForDeletion?_altPaymentOption:null));
			info.AddValue("_altPaymentOptionReturnsNewIfNotFound", _altPaymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAltPaymentOption", _alwaysFetchAltPaymentOption);
			info.AddValue("_alreadyFetchedAltPaymentOption", _alreadyFetchedAltPaymentOption);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AltPaymentOption":
					_alreadyFetchedAltPaymentOption = true;
					this.AltPaymentOption = (PaymentOptionEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "AutoloadToPaymentOptions":
					_alreadyFetchedAutoloadToPaymentOptions = true;
					if(entity!=null)
					{
						this.AutoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)entity);
					}
					break;
				case "EmailEventResendLocks":
					_alreadyFetchedEmailEventResendLocks = true;
					if(entity!=null)
					{
						this.EmailEventResendLocks.Add((EmailEventResendLockEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AltPaymentOption":
					SetupSyncAltPaymentOption(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "AutoloadToPaymentOptions":
					_autoloadToPaymentOptions.Add((AutoloadToPaymentOptionEntity)relatedEntity);
					break;
				case "EmailEventResendLocks":
					_emailEventResendLocks.Add((EmailEventResendLockEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AltPaymentOption":
					DesetupSyncAltPaymentOption(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "AutoloadToPaymentOptions":
					this.PerformRelatedEntityRemoval(_autoloadToPaymentOptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailEventResendLocks":
					this.PerformRelatedEntityRemoval(_emailEventResendLocks, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_altPaymentOption!=null)
			{
				toReturn.Add(_altPaymentOption);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_autoloadToPaymentOptions);
			toReturn.Add(_emailEventResendLocks);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadSettingID)
		{
			return FetchUsingPK(autoloadSettingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadSettingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(autoloadSettingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadSettingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(autoloadSettingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadSettingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(autoloadSettingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AutoloadSettingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AutoloadSettingRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, _autoloadToPaymentOptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadToPaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, _autoloadToPaymentOptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAutoloadToPaymentOptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection GetMultiAutoloadToPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAutoloadToPaymentOptions || forceFetch || _alwaysFetchAutoloadToPaymentOptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_autoloadToPaymentOptions);
				_autoloadToPaymentOptions.SuppressClearInGetMulti=!forceFetch;
				_autoloadToPaymentOptions.EntityFactoryToUse = entityFactoryToUse;
				_autoloadToPaymentOptions.GetMultiManyToOne(this, null, null, filter);
				_autoloadToPaymentOptions.SuppressClearInGetMulti=false;
				_alreadyFetchedAutoloadToPaymentOptions = true;
			}
			return _autoloadToPaymentOptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutoloadToPaymentOptions'. These settings will be taken into account
		/// when the property AutoloadToPaymentOptions is requested or GetMultiAutoloadToPaymentOptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutoloadToPaymentOptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_autoloadToPaymentOptions.SortClauses=sortClauses;
			_autoloadToPaymentOptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailEventResendLocks(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailEventResendLocks || forceFetch || _alwaysFetchEmailEventResendLocks) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventResendLocks);
				_emailEventResendLocks.SuppressClearInGetMulti=!forceFetch;
				_emailEventResendLocks.EntityFactoryToUse = entityFactoryToUse;
				_emailEventResendLocks.GetMultiManyToOne(this, null, null, null, null, filter);
				_emailEventResendLocks.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventResendLocks = true;
			}
			return _emailEventResendLocks;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventResendLocks'. These settings will be taken into account
		/// when the property EmailEventResendLocks is requested or GetMultiEmailEventResendLocks is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventResendLocks(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventResendLocks.SortClauses=sortClauses;
			_emailEventResendLocks.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSingleAltPaymentOption()
		{
			return GetSingleAltPaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSingleAltPaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedAltPaymentOption || forceFetch || _alwaysFetchAltPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingAlternativePaymentOptionId);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlternativePaymentOptionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_altPaymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AltPaymentOption = newEntity;
				_alreadyFetchedAltPaymentOption = fetchResult;
			}
			return _altPaymentOption;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AltPaymentOption", _altPaymentOption);
			toReturn.Add("PaymentOption", _paymentOption);
			toReturn.Add("Product", _product);
			toReturn.Add("AutoloadToPaymentOptions", _autoloadToPaymentOptions);
			toReturn.Add("EmailEventResendLocks", _emailEventResendLocks);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="validator">The validator object for this AutoloadSettingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 autoloadSettingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(autoloadSettingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_autoloadToPaymentOptions = new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection();
			_autoloadToPaymentOptions.SetContainingEntityInfo(this, "AutoloadSetting");

			_emailEventResendLocks = new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection();
			_emailEventResendLocks.SetContainingEntityInfo(this, "AutoloadSetting");
			_altPaymentOptionReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlternativePaymentOptionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoloadSettingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Condition", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodDayOfMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodRunDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SuspendedFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SuspendedTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ThresholdValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TriggerType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _altPaymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAltPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _altPaymentOption, new PropertyChangedEventHandler( OnAltPaymentOptionPropertyChanged ), "AltPaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.PaymentOptionEntityUsingAlternativePaymentOptionIdStatic, true, signalRelatedEntity, "AutoloadSettingsAlternative", resetFKFields, new int[] { (int)AutoloadSettingFieldIndex.AlternativePaymentOptionId } );		
			_altPaymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _altPaymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAltPaymentOption(IEntityCore relatedEntity)
		{
			if(_altPaymentOption!=relatedEntity)
			{		
				DesetupSyncAltPaymentOption(true, true);
				_altPaymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _altPaymentOption, new PropertyChangedEventHandler( OnAltPaymentOptionPropertyChanged ), "AltPaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.PaymentOptionEntityUsingAlternativePaymentOptionIdStatic, true, ref _alreadyFetchedAltPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAltPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, signalRelatedEntity, "AutoloadSettings", resetFKFields, new int[] { (int)AutoloadSettingFieldIndex.PaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "AutoloadSettings", resetFKFields, new int[] { (int)AutoloadSettingFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticAutoloadSettingRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="autoloadSettingID">PK value for AutoloadSetting which data should be fetched into this AutoloadSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 autoloadSettingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AutoloadSettingFieldIndex.AutoloadSettingID].ForcedCurrentValueWrite(autoloadSettingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAutoloadSettingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AutoloadSettingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AutoloadSettingRelations Relations
		{
			get	{ return new AutoloadSettingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadToPaymentOption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadToPaymentOptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection(), (IEntityRelation)GetRelationsForField("AutoloadToPaymentOptions")[0], (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, 0, null, null, null, "AutoloadToPaymentOptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEventResendLock' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventResendLocks
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection(), (IEntityRelation)GetRelationsForField("EmailEventResendLocks")[0], (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, 0, null, null, null, "EmailEventResendLocks", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAltPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("AltPaymentOption")[0], (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "AltPaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlternativePaymentOptionId property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."ALTERNATIVEPAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AlternativePaymentOptionId
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutoloadSettingFieldIndex.AlternativePaymentOptionId, false); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.AlternativePaymentOptionId, value, true); }
		}

		/// <summary> The Amount property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Amount
		{
			get { return (System.Int32)GetValue((int)AutoloadSettingFieldIndex.Amount, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.Amount, value, true); }
		}

		/// <summary> The AutoloadSettingID property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."AUTOLOADSETTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AutoloadSettingID
		{
			get { return (System.Int64)GetValue((int)AutoloadSettingFieldIndex.AutoloadSettingID, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.AutoloadSettingID, value, true); }
		}

		/// <summary> The Condition property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."CONDITION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.Frequency Condition
		{
			get { return (VarioSL.Entities.Enumerations.Frequency)GetValue((int)AutoloadSettingFieldIndex.Condition, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.Condition, value, true); }
		}

		/// <summary> The LastModified property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AutoloadSettingFieldIndex.LastModified, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)AutoloadSettingFieldIndex.LastUser, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutoloadSettingFieldIndex.PaymentOptionID, false); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The PeriodDayOfMonth property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."PERIODDAYOFMONTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PeriodDayOfMonth
		{
			get { return (Nullable<System.Int32>)GetValue((int)AutoloadSettingFieldIndex.PeriodDayOfMonth, false); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.PeriodDayOfMonth, value, true); }
		}

		/// <summary> The PeriodRunDate property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."PERIODRUNDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodRunDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AutoloadSettingFieldIndex.PeriodRunDate, false); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.PeriodRunDate, value, true); }
		}

		/// <summary> The ProductID property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductID
		{
			get { return (System.Int64)GetValue((int)AutoloadSettingFieldIndex.ProductID, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.ProductID, value, true); }
		}

		/// <summary> The SuspendedFrom property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."SUSPENDEDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SuspendedFrom
		{
			get { return (System.DateTime)GetValue((int)AutoloadSettingFieldIndex.SuspendedFrom, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.SuspendedFrom, value, true); }
		}

		/// <summary> The SuspendedTo property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."SUSPENDEDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SuspendedTo
		{
			get { return (System.DateTime)GetValue((int)AutoloadSettingFieldIndex.SuspendedTo, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.SuspendedTo, value, true); }
		}

		/// <summary> The ThresholdValue property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."THRESHOLDVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ThresholdValue
		{
			get { return (System.Int32)GetValue((int)AutoloadSettingFieldIndex.ThresholdValue, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.ThresholdValue, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)AutoloadSettingFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TriggerType property of the Entity AutoloadSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADSETTING"."TRIGGERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TriggerType TriggerType
		{
			get { return (VarioSL.Entities.Enumerations.TriggerType)GetValue((int)AutoloadSettingFieldIndex.TriggerType, true); }
			set	{ SetValue((int)AutoloadSettingFieldIndex.TriggerType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AutoloadToPaymentOptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutoloadToPaymentOptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadToPaymentOptionCollection AutoloadToPaymentOptions
		{
			get	{ return GetMultiAutoloadToPaymentOptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadToPaymentOptions. When set to true, AutoloadToPaymentOptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadToPaymentOptions is accessed. You can always execute/ a forced fetch by calling GetMultiAutoloadToPaymentOptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadToPaymentOptions
		{
			get	{ return _alwaysFetchAutoloadToPaymentOptions; }
			set	{ _alwaysFetchAutoloadToPaymentOptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadToPaymentOptions already has been fetched. Setting this property to false when AutoloadToPaymentOptions has been fetched
		/// will clear the AutoloadToPaymentOptions collection well. Setting this property to true while AutoloadToPaymentOptions hasn't been fetched disables lazy loading for AutoloadToPaymentOptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadToPaymentOptions
		{
			get { return _alreadyFetchedAutoloadToPaymentOptions;}
			set 
			{
				if(_alreadyFetchedAutoloadToPaymentOptions && !value && (_autoloadToPaymentOptions != null))
				{
					_autoloadToPaymentOptions.Clear();
				}
				_alreadyFetchedAutoloadToPaymentOptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventResendLocks()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection EmailEventResendLocks
		{
			get	{ return GetMultiEmailEventResendLocks(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventResendLocks. When set to true, EmailEventResendLocks is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventResendLocks is accessed. You can always execute/ a forced fetch by calling GetMultiEmailEventResendLocks(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventResendLocks
		{
			get	{ return _alwaysFetchEmailEventResendLocks; }
			set	{ _alwaysFetchEmailEventResendLocks = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventResendLocks already has been fetched. Setting this property to false when EmailEventResendLocks has been fetched
		/// will clear the EmailEventResendLocks collection well. Setting this property to true while EmailEventResendLocks hasn't been fetched disables lazy loading for EmailEventResendLocks</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventResendLocks
		{
			get { return _alreadyFetchedEmailEventResendLocks;}
			set 
			{
				if(_alreadyFetchedEmailEventResendLocks && !value && (_emailEventResendLocks != null))
				{
					_emailEventResendLocks.Clear();
				}
				_alreadyFetchedEmailEventResendLocks = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAltPaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity AltPaymentOption
		{
			get	{ return GetSingleAltPaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAltPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AutoloadSettingsAlternative", "AltPaymentOption", _altPaymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AltPaymentOption. When set to true, AltPaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AltPaymentOption is accessed. You can always execute a forced fetch by calling GetSingleAltPaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAltPaymentOption
		{
			get	{ return _alwaysFetchAltPaymentOption; }
			set	{ _alwaysFetchAltPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AltPaymentOption already has been fetched. Setting this property to false when AltPaymentOption has been fetched
		/// will set AltPaymentOption to null as well. Setting this property to true while AltPaymentOption hasn't been fetched disables lazy loading for AltPaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAltPaymentOption
		{
			get { return _alreadyFetchedAltPaymentOption;}
			set 
			{
				if(_alreadyFetchedAltPaymentOption && !value)
				{
					this.AltPaymentOption = null;
				}
				_alreadyFetchedAltPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AltPaymentOption is not found
		/// in the database. When set to true, AltPaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AltPaymentOptionReturnsNewIfNotFound
		{
			get	{ return _altPaymentOptionReturnsNewIfNotFound; }
			set { _altPaymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AutoloadSettings", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AutoloadSettings", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AutoloadSettingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
