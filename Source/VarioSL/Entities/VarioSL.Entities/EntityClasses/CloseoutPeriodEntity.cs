﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CloseoutPeriod'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CloseoutPeriodEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ApportionCollection	_apportions;
		private bool	_alwaysFetchApportions, _alreadyFetchedApportions;
		private VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection	_paymentRecognitions;
		private bool	_alwaysFetchPaymentRecognitions, _alreadyFetchedPaymentRecognitions;
		private VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection	_paymentReconciliations;
		private bool	_alwaysFetchPaymentReconciliations, _alreadyFetchedPaymentReconciliations;
		private VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection	_revenueRecognitions;
		private bool	_alwaysFetchRevenueRecognitions, _alreadyFetchedRevenueRecognitions;
		private VarioSL.Entities.CollectionClasses.RevenueSettlementCollection	_revenueSettlements;
		private bool	_alwaysFetchRevenueSettlements, _alreadyFetchedRevenueSettlements;
		private VarioSL.Entities.CollectionClasses.SalesRevenueCollection	_salesRevenues;
		private bool	_alwaysFetchSalesRevenues, _alreadyFetchedSalesRevenues;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Apportions</summary>
			public static readonly string Apportions = "Apportions";
			/// <summary>Member name PaymentRecognitions</summary>
			public static readonly string PaymentRecognitions = "PaymentRecognitions";
			/// <summary>Member name PaymentReconciliations</summary>
			public static readonly string PaymentReconciliations = "PaymentReconciliations";
			/// <summary>Member name RevenueRecognitions</summary>
			public static readonly string RevenueRecognitions = "RevenueRecognitions";
			/// <summary>Member name RevenueSettlements</summary>
			public static readonly string RevenueSettlements = "RevenueSettlements";
			/// <summary>Member name SalesRevenues</summary>
			public static readonly string SalesRevenues = "SalesRevenues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CloseoutPeriodEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CloseoutPeriodEntity() :base("CloseoutPeriodEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		public CloseoutPeriodEntity(System.Int64 closeoutPeriodID):base("CloseoutPeriodEntity")
		{
			InitClassFetch(closeoutPeriodID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CloseoutPeriodEntity(System.Int64 closeoutPeriodID, IPrefetchPath prefetchPathToUse):base("CloseoutPeriodEntity")
		{
			InitClassFetch(closeoutPeriodID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="validator">The custom validator object for this CloseoutPeriodEntity</param>
		public CloseoutPeriodEntity(System.Int64 closeoutPeriodID, IValidator validator):base("CloseoutPeriodEntity")
		{
			InitClassFetch(closeoutPeriodID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CloseoutPeriodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportions = (VarioSL.Entities.CollectionClasses.ApportionCollection)info.GetValue("_apportions", typeof(VarioSL.Entities.CollectionClasses.ApportionCollection));
			_alwaysFetchApportions = info.GetBoolean("_alwaysFetchApportions");
			_alreadyFetchedApportions = info.GetBoolean("_alreadyFetchedApportions");

			_paymentRecognitions = (VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection)info.GetValue("_paymentRecognitions", typeof(VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection));
			_alwaysFetchPaymentRecognitions = info.GetBoolean("_alwaysFetchPaymentRecognitions");
			_alreadyFetchedPaymentRecognitions = info.GetBoolean("_alreadyFetchedPaymentRecognitions");

			_paymentReconciliations = (VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection)info.GetValue("_paymentReconciliations", typeof(VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection));
			_alwaysFetchPaymentReconciliations = info.GetBoolean("_alwaysFetchPaymentReconciliations");
			_alreadyFetchedPaymentReconciliations = info.GetBoolean("_alreadyFetchedPaymentReconciliations");

			_revenueRecognitions = (VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection)info.GetValue("_revenueRecognitions", typeof(VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection));
			_alwaysFetchRevenueRecognitions = info.GetBoolean("_alwaysFetchRevenueRecognitions");
			_alreadyFetchedRevenueRecognitions = info.GetBoolean("_alreadyFetchedRevenueRecognitions");

			_revenueSettlements = (VarioSL.Entities.CollectionClasses.RevenueSettlementCollection)info.GetValue("_revenueSettlements", typeof(VarioSL.Entities.CollectionClasses.RevenueSettlementCollection));
			_alwaysFetchRevenueSettlements = info.GetBoolean("_alwaysFetchRevenueSettlements");
			_alreadyFetchedRevenueSettlements = info.GetBoolean("_alreadyFetchedRevenueSettlements");

			_salesRevenues = (VarioSL.Entities.CollectionClasses.SalesRevenueCollection)info.GetValue("_salesRevenues", typeof(VarioSL.Entities.CollectionClasses.SalesRevenueCollection));
			_alwaysFetchSalesRevenues = info.GetBoolean("_alwaysFetchSalesRevenues");
			_alreadyFetchedSalesRevenues = info.GetBoolean("_alreadyFetchedSalesRevenues");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportions = (_apportions.Count > 0);
			_alreadyFetchedPaymentRecognitions = (_paymentRecognitions.Count > 0);
			_alreadyFetchedPaymentReconciliations = (_paymentReconciliations.Count > 0);
			_alreadyFetchedRevenueRecognitions = (_revenueRecognitions.Count > 0);
			_alreadyFetchedRevenueSettlements = (_revenueSettlements.Count > 0);
			_alreadyFetchedSalesRevenues = (_salesRevenues.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Apportions":
					toReturn.Add(Relations.ApportionEntityUsingCloseoutPeriodID);
					break;
				case "PaymentRecognitions":
					toReturn.Add(Relations.PaymentRecognitionEntityUsingCloseoutPeriodID);
					break;
				case "PaymentReconciliations":
					toReturn.Add(Relations.PaymentReconciliationEntityUsingCloseoutPeriodID);
					break;
				case "RevenueRecognitions":
					toReturn.Add(Relations.RevenueRecognitionEntityUsingCloseoutPeriodID);
					break;
				case "RevenueSettlements":
					toReturn.Add(Relations.RevenueSettlementEntityUsingCloseoutPeriodID);
					break;
				case "SalesRevenues":
					toReturn.Add(Relations.SalesRevenueEntityUsingCloseoutPeriodID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportions", (!this.MarkedForDeletion?_apportions:null));
			info.AddValue("_alwaysFetchApportions", _alwaysFetchApportions);
			info.AddValue("_alreadyFetchedApportions", _alreadyFetchedApportions);
			info.AddValue("_paymentRecognitions", (!this.MarkedForDeletion?_paymentRecognitions:null));
			info.AddValue("_alwaysFetchPaymentRecognitions", _alwaysFetchPaymentRecognitions);
			info.AddValue("_alreadyFetchedPaymentRecognitions", _alreadyFetchedPaymentRecognitions);
			info.AddValue("_paymentReconciliations", (!this.MarkedForDeletion?_paymentReconciliations:null));
			info.AddValue("_alwaysFetchPaymentReconciliations", _alwaysFetchPaymentReconciliations);
			info.AddValue("_alreadyFetchedPaymentReconciliations", _alreadyFetchedPaymentReconciliations);
			info.AddValue("_revenueRecognitions", (!this.MarkedForDeletion?_revenueRecognitions:null));
			info.AddValue("_alwaysFetchRevenueRecognitions", _alwaysFetchRevenueRecognitions);
			info.AddValue("_alreadyFetchedRevenueRecognitions", _alreadyFetchedRevenueRecognitions);
			info.AddValue("_revenueSettlements", (!this.MarkedForDeletion?_revenueSettlements:null));
			info.AddValue("_alwaysFetchRevenueSettlements", _alwaysFetchRevenueSettlements);
			info.AddValue("_alreadyFetchedRevenueSettlements", _alreadyFetchedRevenueSettlements);
			info.AddValue("_salesRevenues", (!this.MarkedForDeletion?_salesRevenues:null));
			info.AddValue("_alwaysFetchSalesRevenues", _alwaysFetchSalesRevenues);
			info.AddValue("_alreadyFetchedSalesRevenues", _alreadyFetchedSalesRevenues);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Apportions":
					_alreadyFetchedApportions = true;
					if(entity!=null)
					{
						this.Apportions.Add((ApportionEntity)entity);
					}
					break;
				case "PaymentRecognitions":
					_alreadyFetchedPaymentRecognitions = true;
					if(entity!=null)
					{
						this.PaymentRecognitions.Add((PaymentRecognitionEntity)entity);
					}
					break;
				case "PaymentReconciliations":
					_alreadyFetchedPaymentReconciliations = true;
					if(entity!=null)
					{
						this.PaymentReconciliations.Add((PaymentReconciliationEntity)entity);
					}
					break;
				case "RevenueRecognitions":
					_alreadyFetchedRevenueRecognitions = true;
					if(entity!=null)
					{
						this.RevenueRecognitions.Add((RevenueRecognitionEntity)entity);
					}
					break;
				case "RevenueSettlements":
					_alreadyFetchedRevenueSettlements = true;
					if(entity!=null)
					{
						this.RevenueSettlements.Add((RevenueSettlementEntity)entity);
					}
					break;
				case "SalesRevenues":
					_alreadyFetchedSalesRevenues = true;
					if(entity!=null)
					{
						this.SalesRevenues.Add((SalesRevenueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Apportions":
					_apportions.Add((ApportionEntity)relatedEntity);
					break;
				case "PaymentRecognitions":
					_paymentRecognitions.Add((PaymentRecognitionEntity)relatedEntity);
					break;
				case "PaymentReconciliations":
					_paymentReconciliations.Add((PaymentReconciliationEntity)relatedEntity);
					break;
				case "RevenueRecognitions":
					_revenueRecognitions.Add((RevenueRecognitionEntity)relatedEntity);
					break;
				case "RevenueSettlements":
					_revenueSettlements.Add((RevenueSettlementEntity)relatedEntity);
					break;
				case "SalesRevenues":
					_salesRevenues.Add((SalesRevenueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Apportions":
					this.PerformRelatedEntityRemoval(_apportions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentRecognitions":
					this.PerformRelatedEntityRemoval(_paymentRecognitions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentReconciliations":
					this.PerformRelatedEntityRemoval(_paymentReconciliations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevenueRecognitions":
					this.PerformRelatedEntityRemoval(_revenueRecognitions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevenueSettlements":
					this.PerformRelatedEntityRemoval(_revenueSettlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SalesRevenues":
					this.PerformRelatedEntityRemoval(_salesRevenues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_apportions);
			toReturn.Add(_paymentRecognitions);
			toReturn.Add(_paymentReconciliations);
			toReturn.Add(_revenueRecognitions);
			toReturn.Add(_revenueSettlements);
			toReturn.Add(_salesRevenues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 closeoutPeriodID)
		{
			return FetchUsingPK(closeoutPeriodID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 closeoutPeriodID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(closeoutPeriodID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 closeoutPeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(closeoutPeriodID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 closeoutPeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(closeoutPeriodID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CloseoutPeriodID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CloseoutPeriodRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportions || forceFetch || _alwaysFetchApportions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportions);
				_apportions.SuppressClearInGetMulti=!forceFetch;
				_apportions.EntityFactoryToUse = entityFactoryToUse;
				_apportions.GetMultiManyToOne(this, null, null, filter);
				_apportions.SuppressClearInGetMulti=false;
				_alreadyFetchedApportions = true;
			}
			return _apportions;
		}

		/// <summary> Sets the collection parameters for the collection for 'Apportions'. These settings will be taken into account
		/// when the property Apportions is requested or GetMultiApportions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportions.SortClauses=sortClauses;
			_apportions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection GetMultiPaymentRecognitions(bool forceFetch)
		{
			return GetMultiPaymentRecognitions(forceFetch, _paymentRecognitions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection GetMultiPaymentRecognitions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentRecognitions(forceFetch, _paymentRecognitions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection GetMultiPaymentRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentRecognitions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection GetMultiPaymentRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentRecognitions || forceFetch || _alwaysFetchPaymentRecognitions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentRecognitions);
				_paymentRecognitions.SuppressClearInGetMulti=!forceFetch;
				_paymentRecognitions.EntityFactoryToUse = entityFactoryToUse;
				_paymentRecognitions.GetMultiManyToOne(this, filter);
				_paymentRecognitions.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentRecognitions = true;
			}
			return _paymentRecognitions;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentRecognitions'. These settings will be taken into account
		/// when the property PaymentRecognitions is requested or GetMultiPaymentRecognitions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentRecognitions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentRecognitions.SortClauses=sortClauses;
			_paymentRecognitions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentReconciliationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentReconciliationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection GetMultiPaymentReconciliations(bool forceFetch)
		{
			return GetMultiPaymentReconciliations(forceFetch, _paymentReconciliations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentReconciliationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentReconciliationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection GetMultiPaymentReconciliations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentReconciliations(forceFetch, _paymentReconciliations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentReconciliationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection GetMultiPaymentReconciliations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentReconciliations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentReconciliationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection GetMultiPaymentReconciliations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentReconciliations || forceFetch || _alwaysFetchPaymentReconciliations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentReconciliations);
				_paymentReconciliations.SuppressClearInGetMulti=!forceFetch;
				_paymentReconciliations.EntityFactoryToUse = entityFactoryToUse;
				_paymentReconciliations.GetMultiManyToOne(this, filter);
				_paymentReconciliations.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentReconciliations = true;
			}
			return _paymentReconciliations;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentReconciliations'. These settings will be taken into account
		/// when the property PaymentReconciliations is requested or GetMultiPaymentReconciliations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentReconciliations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentReconciliations.SortClauses=sortClauses;
			_paymentReconciliations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch)
		{
			return GetMultiRevenueRecognitions(forceFetch, _revenueRecognitions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueRecognitions(forceFetch, _revenueRecognitions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueRecognitions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueRecognitions || forceFetch || _alwaysFetchRevenueRecognitions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueRecognitions);
				_revenueRecognitions.SuppressClearInGetMulti=!forceFetch;
				_revenueRecognitions.EntityFactoryToUse = entityFactoryToUse;
				_revenueRecognitions.GetMultiManyToOne(this, null, null, filter);
				_revenueRecognitions.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueRecognitions = true;
			}
			return _revenueRecognitions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueRecognitions'. These settings will be taken into account
		/// when the property RevenueRecognitions is requested or GetMultiRevenueRecognitions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueRecognitions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueRecognitions.SortClauses=sortClauses;
			_revenueRecognitions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueSettlements || forceFetch || _alwaysFetchRevenueSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueSettlements);
				_revenueSettlements.SuppressClearInGetMulti=!forceFetch;
				_revenueSettlements.EntityFactoryToUse = entityFactoryToUse;
				_revenueSettlements.GetMultiManyToOne(null, this, null, filter);
				_revenueSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueSettlements = true;
			}
			return _revenueSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueSettlements'. These settings will be taken into account
		/// when the property RevenueSettlements is requested or GetMultiRevenueSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueSettlements.SortClauses=sortClauses;
			_revenueSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SalesRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SalesRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesRevenueCollection GetMultiSalesRevenues(bool forceFetch)
		{
			return GetMultiSalesRevenues(forceFetch, _salesRevenues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SalesRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesRevenueCollection GetMultiSalesRevenues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSalesRevenues(forceFetch, _salesRevenues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SalesRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SalesRevenueCollection GetMultiSalesRevenues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSalesRevenues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SalesRevenueCollection GetMultiSalesRevenues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSalesRevenues || forceFetch || _alwaysFetchSalesRevenues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_salesRevenues);
				_salesRevenues.SuppressClearInGetMulti=!forceFetch;
				_salesRevenues.EntityFactoryToUse = entityFactoryToUse;
				_salesRevenues.GetMultiManyToOne(this, filter);
				_salesRevenues.SuppressClearInGetMulti=false;
				_alreadyFetchedSalesRevenues = true;
			}
			return _salesRevenues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SalesRevenues'. These settings will be taken into account
		/// when the property SalesRevenues is requested or GetMultiSalesRevenues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSalesRevenues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_salesRevenues.SortClauses=sortClauses;
			_salesRevenues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Apportions", _apportions);
			toReturn.Add("PaymentRecognitions", _paymentRecognitions);
			toReturn.Add("PaymentReconciliations", _paymentReconciliations);
			toReturn.Add("RevenueRecognitions", _revenueRecognitions);
			toReturn.Add("RevenueSettlements", _revenueSettlements);
			toReturn.Add("SalesRevenues", _salesRevenues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="validator">The validator object for this CloseoutPeriodEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 closeoutPeriodID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(closeoutPeriodID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_apportions = new VarioSL.Entities.CollectionClasses.ApportionCollection();
			_apportions.SetContainingEntityInfo(this, "CloseoutPeriod");

			_paymentRecognitions = new VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection();
			_paymentRecognitions.SetContainingEntityInfo(this, "CloseoutPeriod");

			_paymentReconciliations = new VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection();
			_paymentReconciliations.SetContainingEntityInfo(this, "CloseoutPeriod");

			_revenueRecognitions = new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection();
			_revenueRecognitions.SetContainingEntityInfo(this, "CloseoutPeriod");

			_revenueSettlements = new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection();
			_revenueSettlements.SetContainingEntityInfo(this, "CloseoutPeriod");

			_salesRevenues = new VarioSL.Entities.CollectionClasses.SalesRevenueCollection();
			_salesRevenues.SetContainingEntityInfo(this, "CloseoutPeriod");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="closeoutPeriodID">PK value for CloseoutPeriod which data should be fetched into this CloseoutPeriod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 closeoutPeriodID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CloseoutPeriodFieldIndex.CloseoutPeriodID].ForcedCurrentValueWrite(closeoutPeriodID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCloseoutPeriodDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CloseoutPeriodEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CloseoutPeriodRelations Relations
		{
			get	{ return new CloseoutPeriodRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Apportion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionCollection(), (IEntityRelation)GetRelationsForField("Apportions")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.ApportionEntity, 0, null, null, null, "Apportions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentRecognition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentRecognitions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection(), (IEntityRelation)GetRelationsForField("PaymentRecognitions")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.PaymentRecognitionEntity, 0, null, null, null, "PaymentRecognitions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentReconciliation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentReconciliations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection(), (IEntityRelation)GetRelationsForField("PaymentReconciliations")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.PaymentReconciliationEntity, 0, null, null, null, "PaymentReconciliations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueRecognition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueRecognitions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection(), (IEntityRelation)GetRelationsForField("RevenueRecognitions")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, 0, null, null, null, "RevenueRecognitions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection(), (IEntityRelation)GetRelationsForField("RevenueSettlements")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, 0, null, null, null, "RevenueSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SalesRevenue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSalesRevenues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SalesRevenueCollection(), (IEntityRelation)GetRelationsForField("SalesRevenues")[0], (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, (int)VarioSL.Entities.EntityType.SalesRevenueEntity, 0, null, null, null, "SalesRevenues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CloseoutDate property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."CLOSEOUTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CloseoutDate
		{
			get { return (System.DateTime)GetValue((int)CloseoutPeriodFieldIndex.CloseoutDate, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.CloseoutDate, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."CLOSEOUTPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CloseoutPeriodID
		{
			get { return (System.Int64)GetValue((int)CloseoutPeriodFieldIndex.CloseoutPeriodID, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CloseoutType property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."CLOSEOUTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CloseoutType CloseoutType
		{
			get { return (VarioSL.Entities.Enumerations.CloseoutType)GetValue((int)CloseoutPeriodFieldIndex.CloseoutType, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.CloseoutType, value, true); }
		}

		/// <summary> The LastModified property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CloseoutPeriodFieldIndex.LastModified, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CloseoutPeriodFieldIndex.LastUser, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PeriodFrom property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."PERIODFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloseoutPeriodFieldIndex.PeriodFrom, false); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.PeriodFrom, value, true); }
		}

		/// <summary> The PeriodTo property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."PERIODTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CloseoutPeriodFieldIndex.PeriodTo, false); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.PeriodTo, value, true); }
		}

		/// <summary> The State property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CloseoutState State
		{
			get { return (VarioSL.Entities.Enumerations.CloseoutState)GetValue((int)CloseoutPeriodFieldIndex.State, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CloseoutPeriod<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_CLOSEOUTPERIOD"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CloseoutPeriodFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CloseoutPeriodFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection Apportions
		{
			get	{ return GetMultiApportions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Apportions. When set to true, Apportions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Apportions is accessed. You can always execute/ a forced fetch by calling GetMultiApportions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportions
		{
			get	{ return _alwaysFetchApportions; }
			set	{ _alwaysFetchApportions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Apportions already has been fetched. Setting this property to false when Apportions has been fetched
		/// will clear the Apportions collection well. Setting this property to true while Apportions hasn't been fetched disables lazy loading for Apportions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportions
		{
			get { return _alreadyFetchedApportions;}
			set 
			{
				if(_alreadyFetchedApportions && !value && (_apportions != null))
				{
					_apportions.Clear();
				}
				_alreadyFetchedApportions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentRecognitionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentRecognitions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentRecognitionCollection PaymentRecognitions
		{
			get	{ return GetMultiPaymentRecognitions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentRecognitions. When set to true, PaymentRecognitions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentRecognitions is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentRecognitions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentRecognitions
		{
			get	{ return _alwaysFetchPaymentRecognitions; }
			set	{ _alwaysFetchPaymentRecognitions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentRecognitions already has been fetched. Setting this property to false when PaymentRecognitions has been fetched
		/// will clear the PaymentRecognitions collection well. Setting this property to true while PaymentRecognitions hasn't been fetched disables lazy loading for PaymentRecognitions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentRecognitions
		{
			get { return _alreadyFetchedPaymentRecognitions;}
			set 
			{
				if(_alreadyFetchedPaymentRecognitions && !value && (_paymentRecognitions != null))
				{
					_paymentRecognitions.Clear();
				}
				_alreadyFetchedPaymentRecognitions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentReconciliationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentReconciliations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentReconciliationCollection PaymentReconciliations
		{
			get	{ return GetMultiPaymentReconciliations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentReconciliations. When set to true, PaymentReconciliations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentReconciliations is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentReconciliations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentReconciliations
		{
			get	{ return _alwaysFetchPaymentReconciliations; }
			set	{ _alwaysFetchPaymentReconciliations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentReconciliations already has been fetched. Setting this property to false when PaymentReconciliations has been fetched
		/// will clear the PaymentReconciliations collection well. Setting this property to true while PaymentReconciliations hasn't been fetched disables lazy loading for PaymentReconciliations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentReconciliations
		{
			get { return _alreadyFetchedPaymentReconciliations;}
			set 
			{
				if(_alreadyFetchedPaymentReconciliations && !value && (_paymentReconciliations != null))
				{
					_paymentReconciliations.Clear();
				}
				_alreadyFetchedPaymentReconciliations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueRecognitions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection RevenueRecognitions
		{
			get	{ return GetMultiRevenueRecognitions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueRecognitions. When set to true, RevenueRecognitions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueRecognitions is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueRecognitions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueRecognitions
		{
			get	{ return _alwaysFetchRevenueRecognitions; }
			set	{ _alwaysFetchRevenueRecognitions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueRecognitions already has been fetched. Setting this property to false when RevenueRecognitions has been fetched
		/// will clear the RevenueRecognitions collection well. Setting this property to true while RevenueRecognitions hasn't been fetched disables lazy loading for RevenueRecognitions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueRecognitions
		{
			get { return _alreadyFetchedRevenueRecognitions;}
			set 
			{
				if(_alreadyFetchedRevenueRecognitions && !value && (_revenueRecognitions != null))
				{
					_revenueRecognitions.Clear();
				}
				_alreadyFetchedRevenueRecognitions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection RevenueSettlements
		{
			get	{ return GetMultiRevenueSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueSettlements. When set to true, RevenueSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueSettlements is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueSettlements
		{
			get	{ return _alwaysFetchRevenueSettlements; }
			set	{ _alwaysFetchRevenueSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueSettlements already has been fetched. Setting this property to false when RevenueSettlements has been fetched
		/// will clear the RevenueSettlements collection well. Setting this property to true while RevenueSettlements hasn't been fetched disables lazy loading for RevenueSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueSettlements
		{
			get { return _alreadyFetchedRevenueSettlements;}
			set 
			{
				if(_alreadyFetchedRevenueSettlements && !value && (_revenueSettlements != null))
				{
					_revenueSettlements.Clear();
				}
				_alreadyFetchedRevenueSettlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SalesRevenueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSalesRevenues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SalesRevenueCollection SalesRevenues
		{
			get	{ return GetMultiSalesRevenues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SalesRevenues. When set to true, SalesRevenues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SalesRevenues is accessed. You can always execute/ a forced fetch by calling GetMultiSalesRevenues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSalesRevenues
		{
			get	{ return _alwaysFetchSalesRevenues; }
			set	{ _alwaysFetchSalesRevenues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SalesRevenues already has been fetched. Setting this property to false when SalesRevenues has been fetched
		/// will clear the SalesRevenues collection well. Setting this property to true while SalesRevenues hasn't been fetched disables lazy loading for SalesRevenues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSalesRevenues
		{
			get { return _alreadyFetchedSalesRevenues;}
			set 
			{
				if(_alreadyFetchedSalesRevenues && !value && (_salesRevenues != null))
				{
					_salesRevenues.Clear();
				}
				_alreadyFetchedSalesRevenues = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
