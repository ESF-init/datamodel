﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PredefinedKey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PredefinedKeyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection	_keyAttributeTransform;
		private bool	_alwaysFetchKeyAttributeTransform, _alreadyFetchedKeyAttributeTransform;
		private VarioSL.Entities.CollectionClasses.PrimalKeyCollection	_primalKeys;
		private bool	_alwaysFetchPrimalKeys, _alreadyFetchedPrimalKeys;
		private PanelEntity _panel;
		private bool	_alwaysFetchPanel, _alreadyFetchedPanel, _panelReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Panel</summary>
			public static readonly string Panel = "Panel";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name KeyAttributeTransform</summary>
			public static readonly string KeyAttributeTransform = "KeyAttributeTransform";
			/// <summary>Member name PrimalKeys</summary>
			public static readonly string PrimalKeys = "PrimalKeys";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PredefinedKeyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PredefinedKeyEntity() :base("PredefinedKeyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		public PredefinedKeyEntity(System.Int64 keyID):base("PredefinedKeyEntity")
		{
			InitClassFetch(keyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PredefinedKeyEntity(System.Int64 keyID, IPrefetchPath prefetchPathToUse):base("PredefinedKeyEntity")
		{
			InitClassFetch(keyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="validator">The custom validator object for this PredefinedKeyEntity</param>
		public PredefinedKeyEntity(System.Int64 keyID, IValidator validator):base("PredefinedKeyEntity")
		{
			InitClassFetch(keyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PredefinedKeyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_keyAttributeTransform = (VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection)info.GetValue("_keyAttributeTransform", typeof(VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection));
			_alwaysFetchKeyAttributeTransform = info.GetBoolean("_alwaysFetchKeyAttributeTransform");
			_alreadyFetchedKeyAttributeTransform = info.GetBoolean("_alreadyFetchedKeyAttributeTransform");

			_primalKeys = (VarioSL.Entities.CollectionClasses.PrimalKeyCollection)info.GetValue("_primalKeys", typeof(VarioSL.Entities.CollectionClasses.PrimalKeyCollection));
			_alwaysFetchPrimalKeys = info.GetBoolean("_alwaysFetchPrimalKeys");
			_alreadyFetchedPrimalKeys = info.GetBoolean("_alreadyFetchedPrimalKeys");
			_panel = (PanelEntity)info.GetValue("_panel", typeof(PanelEntity));
			if(_panel!=null)
			{
				_panel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_panelReturnsNewIfNotFound = info.GetBoolean("_panelReturnsNewIfNotFound");
			_alwaysFetchPanel = info.GetBoolean("_alwaysFetchPanel");
			_alreadyFetchedPanel = info.GetBoolean("_alreadyFetchedPanel");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PredefinedKeyFieldIndex)fieldIndex)
			{
				case PredefinedKeyFieldIndex.PanelID:
					DesetupSyncPanel(true, false);
					_alreadyFetchedPanel = false;
					break;
				case PredefinedKeyFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedKeyAttributeTransform = (_keyAttributeTransform.Count > 0);
			_alreadyFetchedPrimalKeys = (_primalKeys.Count > 0);
			_alreadyFetchedPanel = (_panel != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Panel":
					toReturn.Add(Relations.PanelEntityUsingPanelID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "KeyAttributeTransform":
					toReturn.Add(Relations.KeyAttributeTransfromEntityUsingKeyID);
					break;
				case "PrimalKeys":
					toReturn.Add(Relations.PrimalKeyEntityUsingKeyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_keyAttributeTransform", (!this.MarkedForDeletion?_keyAttributeTransform:null));
			info.AddValue("_alwaysFetchKeyAttributeTransform", _alwaysFetchKeyAttributeTransform);
			info.AddValue("_alreadyFetchedKeyAttributeTransform", _alreadyFetchedKeyAttributeTransform);
			info.AddValue("_primalKeys", (!this.MarkedForDeletion?_primalKeys:null));
			info.AddValue("_alwaysFetchPrimalKeys", _alwaysFetchPrimalKeys);
			info.AddValue("_alreadyFetchedPrimalKeys", _alreadyFetchedPrimalKeys);
			info.AddValue("_panel", (!this.MarkedForDeletion?_panel:null));
			info.AddValue("_panelReturnsNewIfNotFound", _panelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPanel", _alwaysFetchPanel);
			info.AddValue("_alreadyFetchedPanel", _alreadyFetchedPanel);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Panel":
					_alreadyFetchedPanel = true;
					this.Panel = (PanelEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "KeyAttributeTransform":
					_alreadyFetchedKeyAttributeTransform = true;
					if(entity!=null)
					{
						this.KeyAttributeTransform.Add((KeyAttributeTransfromEntity)entity);
					}
					break;
				case "PrimalKeys":
					_alreadyFetchedPrimalKeys = true;
					if(entity!=null)
					{
						this.PrimalKeys.Add((PrimalKeyEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Panel":
					SetupSyncPanel(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "KeyAttributeTransform":
					_keyAttributeTransform.Add((KeyAttributeTransfromEntity)relatedEntity);
					break;
				case "PrimalKeys":
					_primalKeys.Add((PrimalKeyEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Panel":
					DesetupSyncPanel(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "KeyAttributeTransform":
					this.PerformRelatedEntityRemoval(_keyAttributeTransform, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PrimalKeys":
					this.PerformRelatedEntityRemoval(_primalKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_panel!=null)
			{
				toReturn.Add(_panel);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_keyAttributeTransform);
			toReturn.Add(_primalKeys);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID)
		{
			return FetchUsingPK(keyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(keyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(keyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(keyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.KeyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PredefinedKeyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransform(bool forceFetch)
		{
			return GetMultiKeyAttributeTransform(forceFetch, _keyAttributeTransform.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransform(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiKeyAttributeTransform(forceFetch, _keyAttributeTransform.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransform(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiKeyAttributeTransform(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransform(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedKeyAttributeTransform || forceFetch || _alwaysFetchKeyAttributeTransform) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_keyAttributeTransform);
				_keyAttributeTransform.SuppressClearInGetMulti=!forceFetch;
				_keyAttributeTransform.EntityFactoryToUse = entityFactoryToUse;
				_keyAttributeTransform.GetMultiManyToOne(null, null, null, this, null, filter);
				_keyAttributeTransform.SuppressClearInGetMulti=false;
				_alreadyFetchedKeyAttributeTransform = true;
			}
			return _keyAttributeTransform;
		}

		/// <summary> Sets the collection parameters for the collection for 'KeyAttributeTransform'. These settings will be taken into account
		/// when the property KeyAttributeTransform is requested or GetMultiKeyAttributeTransform is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersKeyAttributeTransform(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_keyAttributeTransform.SortClauses=sortClauses;
			_keyAttributeTransform.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPrimalKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPrimalKeys || forceFetch || _alwaysFetchPrimalKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_primalKeys);
				_primalKeys.SuppressClearInGetMulti=!forceFetch;
				_primalKeys.EntityFactoryToUse = entityFactoryToUse;
				_primalKeys.GetMultiManyToOne(this, null, null, filter);
				_primalKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPrimalKeys = true;
			}
			return _primalKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PrimalKeys'. These settings will be taken into account
		/// when the property PrimalKeys is requested or GetMultiPrimalKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPrimalKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_primalKeys.SortClauses=sortClauses;
			_primalKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public PanelEntity GetSinglePanel()
		{
			return GetSinglePanel(false);
		}

		/// <summary> Retrieves the related entity of type 'PanelEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PanelEntity' which is related to this entity.</returns>
		public virtual PanelEntity GetSinglePanel(bool forceFetch)
		{
			if( ( !_alreadyFetchedPanel || forceFetch || _alwaysFetchPanel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PanelEntityUsingPanelID);
				PanelEntity newEntity = new PanelEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PanelID);
				}
				if(fetchResult)
				{
					newEntity = (PanelEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_panelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Panel = newEntity;
				_alreadyFetchedPanel = fetchResult;
			}
			return _panel;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Panel", _panel);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("KeyAttributeTransform", _keyAttributeTransform);
			toReturn.Add("PrimalKeys", _primalKeys);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="validator">The validator object for this PredefinedKeyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 keyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(keyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_keyAttributeTransform = new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection();
			_keyAttributeTransform.SetContainingEntityInfo(this, "PredefinedKey");

			_primalKeys = new VarioSL.Entities.CollectionClasses.PrimalKeyCollection();
			_primalKeys.SetContainingEntityInfo(this, "PredefinedKey");
			_panelReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PanelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffKey", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _panel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPanel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _panel, new PropertyChangedEventHandler( OnPanelPropertyChanged ), "Panel", VarioSL.Entities.RelationClasses.StaticPredefinedKeyRelations.PanelEntityUsingPanelIDStatic, true, signalRelatedEntity, "PredefinedKeys", resetFKFields, new int[] { (int)PredefinedKeyFieldIndex.PanelID } );		
			_panel = null;
		}
		
		/// <summary> setups the sync logic for member _panel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPanel(IEntityCore relatedEntity)
		{
			if(_panel!=relatedEntity)
			{		
				DesetupSyncPanel(true, true);
				_panel = (PanelEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _panel, new PropertyChangedEventHandler( OnPanelPropertyChanged ), "Panel", VarioSL.Entities.RelationClasses.StaticPredefinedKeyRelations.PanelEntityUsingPanelIDStatic, true, ref _alreadyFetchedPanel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPanelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticPredefinedKeyRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "PredefinedKeys", resetFKFields, new int[] { (int)PredefinedKeyFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticPredefinedKeyRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="keyID">PK value for PredefinedKey which data should be fetched into this PredefinedKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 keyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PredefinedKeyFieldIndex.KeyID].ForcedCurrentValueWrite(keyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePredefinedKeyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PredefinedKeyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PredefinedKeyRelations Relations
		{
			get	{ return new PredefinedKeyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeyAttributeTransfrom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKeyAttributeTransform
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection(), (IEntityRelation)GetRelationsForField("KeyAttributeTransform")[0], (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, 0, null, null, null, "KeyAttributeTransform", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrimalKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrimalKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrimalKeyCollection(), (IEntityRelation)GetRelationsForField("PrimalKeys")[0], (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, (int)VarioSL.Entities.EntityType.PrimalKeyEntity, 0, null, null, null, "PrimalKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Panel'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPanel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PanelCollection(), (IEntityRelation)GetRelationsForField("Panel")[0], (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, (int)VarioSL.Entities.EntityType.PanelEntity, 0, null, null, null, "Panel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Caption property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."CAPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)PredefinedKeyFieldIndex.Caption, true); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.Caption, value, true); }
		}

		/// <summary> The Description property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PredefinedKeyFieldIndex.Description, true); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.Description, value, true); }
		}

		/// <summary> The KeyID property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."KEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 KeyID
		{
			get { return (System.Int64)GetValue((int)PredefinedKeyFieldIndex.KeyID, true); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.KeyID, value, true); }
		}

		/// <summary> The KeyNumber property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."KEYNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyNumber
		{
			get { return (System.Int64)GetValue((int)PredefinedKeyFieldIndex.KeyNumber, true); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.KeyNumber, value, true); }
		}

		/// <summary> The PanelID property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."PANELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PanelID
		{
			get { return (System.Int64)GetValue((int)PredefinedKeyFieldIndex.PanelID, true); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.PanelID, value, true); }
		}

		/// <summary> The TariffID property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PredefinedKeyFieldIndex.TariffID, false); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TariffKey property of the Entity PredefinedKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PREDEFINEDKEYS"."TARIFFKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TariffKey
		{
			get { return (Nullable<System.Int16>)GetValue((int)PredefinedKeyFieldIndex.TariffKey, false); }
			set	{ SetValue((int)PredefinedKeyFieldIndex.TariffKey, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiKeyAttributeTransform()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection KeyAttributeTransform
		{
			get	{ return GetMultiKeyAttributeTransform(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for KeyAttributeTransform. When set to true, KeyAttributeTransform is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time KeyAttributeTransform is accessed. You can always execute/ a forced fetch by calling GetMultiKeyAttributeTransform(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKeyAttributeTransform
		{
			get	{ return _alwaysFetchKeyAttributeTransform; }
			set	{ _alwaysFetchKeyAttributeTransform = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property KeyAttributeTransform already has been fetched. Setting this property to false when KeyAttributeTransform has been fetched
		/// will clear the KeyAttributeTransform collection well. Setting this property to true while KeyAttributeTransform hasn't been fetched disables lazy loading for KeyAttributeTransform</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKeyAttributeTransform
		{
			get { return _alreadyFetchedKeyAttributeTransform;}
			set 
			{
				if(_alreadyFetchedKeyAttributeTransform && !value && (_keyAttributeTransform != null))
				{
					_keyAttributeTransform.Clear();
				}
				_alreadyFetchedKeyAttributeTransform = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPrimalKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection PrimalKeys
		{
			get	{ return GetMultiPrimalKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PrimalKeys. When set to true, PrimalKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrimalKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPrimalKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrimalKeys
		{
			get	{ return _alwaysFetchPrimalKeys; }
			set	{ _alwaysFetchPrimalKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrimalKeys already has been fetched. Setting this property to false when PrimalKeys has been fetched
		/// will clear the PrimalKeys collection well. Setting this property to true while PrimalKeys hasn't been fetched disables lazy loading for PrimalKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrimalKeys
		{
			get { return _alreadyFetchedPrimalKeys;}
			set 
			{
				if(_alreadyFetchedPrimalKeys && !value && (_primalKeys != null))
				{
					_primalKeys.Clear();
				}
				_alreadyFetchedPrimalKeys = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'PanelEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePanel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PanelEntity Panel
		{
			get	{ return GetSinglePanel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPanel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PredefinedKeys", "Panel", _panel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Panel. When set to true, Panel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Panel is accessed. You can always execute a forced fetch by calling GetSinglePanel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPanel
		{
			get	{ return _alwaysFetchPanel; }
			set	{ _alwaysFetchPanel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Panel already has been fetched. Setting this property to false when Panel has been fetched
		/// will set Panel to null as well. Setting this property to true while Panel hasn't been fetched disables lazy loading for Panel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPanel
		{
			get { return _alreadyFetchedPanel;}
			set 
			{
				if(_alreadyFetchedPanel && !value)
				{
					this.Panel = null;
				}
				_alreadyFetchedPanel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Panel is not found
		/// in the database. When set to true, Panel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PanelReturnsNewIfNotFound
		{
			get	{ return _panelReturnsNewIfNotFound; }
			set { _panelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PredefinedKeys", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PredefinedKeyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
