﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentDetail'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PaymentDetailEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TransactionEntity _salesTransaction;
		private bool	_alwaysFetchSalesTransaction, _alreadyFetchedSalesTransaction, _salesTransactionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SalesTransaction</summary>
			public static readonly string SalesTransaction = "SalesTransaction";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentDetailEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentDetailEntity() :base("PaymentDetailEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		public PaymentDetailEntity(System.Int64 paymentDetailID):base("PaymentDetailEntity")
		{
			InitClassFetch(paymentDetailID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentDetailEntity(System.Int64 paymentDetailID, IPrefetchPath prefetchPathToUse):base("PaymentDetailEntity")
		{
			InitClassFetch(paymentDetailID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="validator">The custom validator object for this PaymentDetailEntity</param>
		public PaymentDetailEntity(System.Int64 paymentDetailID, IValidator validator):base("PaymentDetailEntity")
		{
			InitClassFetch(paymentDetailID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentDetailEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_salesTransaction = (TransactionEntity)info.GetValue("_salesTransaction", typeof(TransactionEntity));
			if(_salesTransaction!=null)
			{
				_salesTransaction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_salesTransactionReturnsNewIfNotFound = info.GetBoolean("_salesTransactionReturnsNewIfNotFound");
			_alwaysFetchSalesTransaction = info.GetBoolean("_alwaysFetchSalesTransaction");
			_alreadyFetchedSalesTransaction = info.GetBoolean("_alreadyFetchedSalesTransaction");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentDetailFieldIndex)fieldIndex)
			{
				case PaymentDetailFieldIndex.TransactionID:
					DesetupSyncSalesTransaction(true, false);
					_alreadyFetchedSalesTransaction = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSalesTransaction = (_salesTransaction != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SalesTransaction":
					toReturn.Add(Relations.TransactionEntityUsingTransactionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_salesTransaction", (!this.MarkedForDeletion?_salesTransaction:null));
			info.AddValue("_salesTransactionReturnsNewIfNotFound", _salesTransactionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSalesTransaction", _alwaysFetchSalesTransaction);
			info.AddValue("_alreadyFetchedSalesTransaction", _alreadyFetchedSalesTransaction);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SalesTransaction":
					_alreadyFetchedSalesTransaction = true;
					this.SalesTransaction = (TransactionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SalesTransaction":
					SetupSyncSalesTransaction(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SalesTransaction":
					DesetupSyncSalesTransaction(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_salesTransaction!=null)
			{
				toReturn.Add(_salesTransaction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentDetailID)
		{
			return FetchUsingPK(paymentDetailID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentDetailID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentDetailID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentDetailID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentDetailRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public TransactionEntity GetSingleSalesTransaction()
		{
			return GetSingleSalesTransaction(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public virtual TransactionEntity GetSingleSalesTransaction(bool forceFetch)
		{
			if( ( !_alreadyFetchedSalesTransaction || forceFetch || _alwaysFetchSalesTransaction) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionEntityUsingTransactionID);
				TransactionEntity newEntity = new TransactionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionID);
				}
				if(fetchResult)
				{
					newEntity = (TransactionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_salesTransactionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SalesTransaction = newEntity;
				_alreadyFetchedSalesTransaction = fetchResult;
			}
			return _salesTransaction;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SalesTransaction", _salesTransaction);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="validator">The validator object for this PaymentDetailEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 paymentDetailID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentDetailID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_salesTransactionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuthenticationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Issuer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaskedPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Resultcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SequenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TerminalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationMode", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _salesTransaction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSalesTransaction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _salesTransaction, new PropertyChangedEventHandler( OnSalesTransactionPropertyChanged ), "SalesTransaction", VarioSL.Entities.RelationClasses.StaticPaymentDetailRelations.TransactionEntityUsingTransactionIDStatic, true, signalRelatedEntity, "PaymentDetails", resetFKFields, new int[] { (int)PaymentDetailFieldIndex.TransactionID } );		
			_salesTransaction = null;
		}
		
		/// <summary> setups the sync logic for member _salesTransaction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSalesTransaction(IEntityCore relatedEntity)
		{
			if(_salesTransaction!=relatedEntity)
			{		
				DesetupSyncSalesTransaction(true, true);
				_salesTransaction = (TransactionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _salesTransaction, new PropertyChangedEventHandler( OnSalesTransactionPropertyChanged ), "SalesTransaction", VarioSL.Entities.RelationClasses.StaticPaymentDetailRelations.TransactionEntityUsingTransactionIDStatic, true, ref _alreadyFetchedSalesTransaction, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalesTransactionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentDetailID">PK value for PaymentDetail which data should be fetched into this PaymentDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 paymentDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentDetailFieldIndex.PaymentDetailID].ForcedCurrentValueWrite(paymentDetailID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentDetailDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentDetailEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentDetailRelations Relations
		{
			get	{ return new PaymentDetailRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSalesTransaction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("SalesTransaction")[0], (int)VarioSL.Entities.EntityType.PaymentDetailEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "SalesTransaction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AuthenticationCode property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."AUTHCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AuthenticationCode
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.AuthenticationCode, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.AuthenticationCode, value, true); }
		}

		/// <summary> The Expiry property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."EXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Expiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentDetailFieldIndex.Expiry, false); }
			set	{ SetValue((int)PaymentDetailFieldIndex.Expiry, value, true); }
		}

		/// <summary> The Issuer property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."ISSUER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Issuer
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.Issuer, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.Issuer, value, true); }
		}

		/// <summary> The MaskedPan property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."MASKEDPAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MaskedPan
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.MaskedPan, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.MaskedPan, value, true); }
		}

		/// <summary> The PaymentDetailID property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."PAYMENTDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PaymentDetailID
		{
			get { return (System.Int64)GetValue((int)PaymentDetailFieldIndex.PaymentDetailID, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.PaymentDetailID, value, true); }
		}

		/// <summary> The PaymentType property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."PAYMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentType
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentDetailFieldIndex.PaymentType, false); }
			set	{ SetValue((int)PaymentDetailFieldIndex.PaymentType, value, true); }
		}

		/// <summary> The ReferenceNumber property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."REFERENCENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReferenceNumber
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.ReferenceNumber, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.ReferenceNumber, value, true); }
		}

		/// <summary> The Resultcode property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."RESULTCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Resultcode
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentDetailFieldIndex.Resultcode, false); }
			set	{ SetValue((int)PaymentDetailFieldIndex.Resultcode, value, true); }
		}

		/// <summary> The SequenceNumber property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."SEQUENCENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SequenceNumber
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.SequenceNumber, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.SequenceNumber, value, true); }
		}

		/// <summary> The TerminalID property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."TERMINALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TerminalID
		{
			get { return (System.String)GetValue((int)PaymentDetailFieldIndex.TerminalID, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.TerminalID, value, true); }
		}

		/// <summary> The TransactionID property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionID
		{
			get { return (System.Int64)GetValue((int)PaymentDetailFieldIndex.TransactionID, true); }
			set	{ SetValue((int)PaymentDetailFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The ValidationMode property of the Entity PaymentDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_PAYMENTDETAILS"."VALIDATIONMODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ValidationMode
		{
			get { return (Nullable<System.Int16>)GetValue((int)PaymentDetailFieldIndex.ValidationMode, false); }
			set	{ SetValue((int)PaymentDetailFieldIndex.ValidationMode, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TransactionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSalesTransaction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionEntity SalesTransaction
		{
			get	{ return GetSingleSalesTransaction(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSalesTransaction(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentDetails", "SalesTransaction", _salesTransaction, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SalesTransaction. When set to true, SalesTransaction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SalesTransaction is accessed. You can always execute a forced fetch by calling GetSingleSalesTransaction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSalesTransaction
		{
			get	{ return _alwaysFetchSalesTransaction; }
			set	{ _alwaysFetchSalesTransaction = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SalesTransaction already has been fetched. Setting this property to false when SalesTransaction has been fetched
		/// will set SalesTransaction to null as well. Setting this property to true while SalesTransaction hasn't been fetched disables lazy loading for SalesTransaction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSalesTransaction
		{
			get { return _alreadyFetchedSalesTransaction;}
			set 
			{
				if(_alreadyFetchedSalesTransaction && !value)
				{
					this.SalesTransaction = null;
				}
				_alreadyFetchedSalesTransaction = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SalesTransaction is not found
		/// in the database. When set to true, SalesTransaction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SalesTransactionReturnsNewIfNotFound
		{
			get	{ return _salesTransactionReturnsNewIfNotFound; }
			set { _salesTransactionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PaymentDetailEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
