﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Shift'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ShiftEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TransactionCollection	_transactions;
		private bool	_alwaysFetchTransactions, _alreadyFetchedTransactions;
		private ShiftStateEntity _shiftState;
		private bool	_alwaysFetchShiftState, _alreadyFetchedShiftState, _shiftStateReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ShiftState</summary>
			public static readonly string ShiftState = "ShiftState";
			/// <summary>Member name Transactions</summary>
			public static readonly string Transactions = "Transactions";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ShiftEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ShiftEntity() :base("ShiftEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		public ShiftEntity(System.Int64 shiftID):base("ShiftEntity")
		{
			InitClassFetch(shiftID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ShiftEntity(System.Int64 shiftID, IPrefetchPath prefetchPathToUse):base("ShiftEntity")
		{
			InitClassFetch(shiftID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="validator">The custom validator object for this ShiftEntity</param>
		public ShiftEntity(System.Int64 shiftID, IValidator validator):base("ShiftEntity")
		{
			InitClassFetch(shiftID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ShiftEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_transactions = (VarioSL.Entities.CollectionClasses.TransactionCollection)info.GetValue("_transactions", typeof(VarioSL.Entities.CollectionClasses.TransactionCollection));
			_alwaysFetchTransactions = info.GetBoolean("_alwaysFetchTransactions");
			_alreadyFetchedTransactions = info.GetBoolean("_alreadyFetchedTransactions");
			_shiftState = (ShiftStateEntity)info.GetValue("_shiftState", typeof(ShiftStateEntity));
			if(_shiftState!=null)
			{
				_shiftState.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_shiftStateReturnsNewIfNotFound = info.GetBoolean("_shiftStateReturnsNewIfNotFound");
			_alwaysFetchShiftState = info.GetBoolean("_alwaysFetchShiftState");
			_alreadyFetchedShiftState = info.GetBoolean("_alreadyFetchedShiftState");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ShiftFieldIndex)fieldIndex)
			{
				case ShiftFieldIndex.ShiftStateID:
					DesetupSyncShiftState(true, false);
					_alreadyFetchedShiftState = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTransactions = (_transactions.Count > 0);
			_alreadyFetchedShiftState = (_shiftState != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ShiftState":
					toReturn.Add(Relations.ShiftStateEntityUsingShiftStateID);
					break;
				case "Transactions":
					toReturn.Add(Relations.TransactionEntityUsingShiftID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_transactions", (!this.MarkedForDeletion?_transactions:null));
			info.AddValue("_alwaysFetchTransactions", _alwaysFetchTransactions);
			info.AddValue("_alreadyFetchedTransactions", _alreadyFetchedTransactions);
			info.AddValue("_shiftState", (!this.MarkedForDeletion?_shiftState:null));
			info.AddValue("_shiftStateReturnsNewIfNotFound", _shiftStateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchShiftState", _alwaysFetchShiftState);
			info.AddValue("_alreadyFetchedShiftState", _alreadyFetchedShiftState);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ShiftState":
					_alreadyFetchedShiftState = true;
					this.ShiftState = (ShiftStateEntity)entity;
					break;
				case "Transactions":
					_alreadyFetchedTransactions = true;
					if(entity!=null)
					{
						this.Transactions.Add((TransactionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ShiftState":
					SetupSyncShiftState(relatedEntity);
					break;
				case "Transactions":
					_transactions.Add((TransactionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ShiftState":
					DesetupSyncShiftState(false, true);
					break;
				case "Transactions":
					this.PerformRelatedEntityRemoval(_transactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_shiftState!=null)
			{
				toReturn.Add(_shiftState);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_transactions);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 shiftID)
		{
			return FetchUsingPK(shiftID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 shiftID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(shiftID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 shiftID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(shiftID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 shiftID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(shiftID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ShiftID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ShiftRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch)
		{
			return GetMultiTransactions(forceFetch, _transactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactions(forceFetch, _transactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection GetMultiTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactions || forceFetch || _alwaysFetchTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactions);
				_transactions.SuppressClearInGetMulti=!forceFetch;
				_transactions.EntityFactoryToUse = entityFactoryToUse;
				_transactions.GetMultiManyToOne(null, null, this, filter);
				_transactions.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactions = true;
			}
			return _transactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'Transactions'. These settings will be taken into account
		/// when the property Transactions is requested or GetMultiTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactions.SortClauses=sortClauses;
			_transactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ShiftStateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ShiftStateEntity' which is related to this entity.</returns>
		public ShiftStateEntity GetSingleShiftState()
		{
			return GetSingleShiftState(false);
		}

		/// <summary> Retrieves the related entity of type 'ShiftStateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ShiftStateEntity' which is related to this entity.</returns>
		public virtual ShiftStateEntity GetSingleShiftState(bool forceFetch)
		{
			if( ( !_alreadyFetchedShiftState || forceFetch || _alwaysFetchShiftState) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ShiftStateEntityUsingShiftStateID);
				ShiftStateEntity newEntity = new ShiftStateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ShiftStateID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ShiftStateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_shiftStateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ShiftState = newEntity;
				_alreadyFetchedShiftState = fetchResult;
			}
			return _shiftState;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ShiftState", _shiftState);
			toReturn.Add("Transactions", _transactions);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="validator">The validator object for this ShiftEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 shiftID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(shiftID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_transactions = new VarioSL.Entities.CollectionClasses.TransactionCollection();
			_transactions.SetContainingEntityInfo(this, "Shift");
			_shiftStateReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BalanceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankCardRevenue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashContentBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashContentEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashReceipt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CourseNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerCardRevenue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepositNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstTicketNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HashValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImportDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBroken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDst", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastTicketNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogOffDebtor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PosPaymentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceReduction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesVolume", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftStateID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _shiftState</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncShiftState(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _shiftState, new PropertyChangedEventHandler( OnShiftStatePropertyChanged ), "ShiftState", VarioSL.Entities.RelationClasses.StaticShiftRelations.ShiftStateEntityUsingShiftStateIDStatic, true, signalRelatedEntity, "Shifts", resetFKFields, new int[] { (int)ShiftFieldIndex.ShiftStateID } );		
			_shiftState = null;
		}
		
		/// <summary> setups the sync logic for member _shiftState</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncShiftState(IEntityCore relatedEntity)
		{
			if(_shiftState!=relatedEntity)
			{		
				DesetupSyncShiftState(true, true);
				_shiftState = (ShiftStateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _shiftState, new PropertyChangedEventHandler( OnShiftStatePropertyChanged ), "ShiftState", VarioSL.Entities.RelationClasses.StaticShiftRelations.ShiftStateEntityUsingShiftStateIDStatic, true, ref _alreadyFetchedShiftState, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnShiftStatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="shiftID">PK value for Shift which data should be fetched into this Shift object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 shiftID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ShiftFieldIndex.ShiftID].ForcedCurrentValueWrite(shiftID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateShiftDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ShiftEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ShiftRelations Relations
		{
			get	{ return new ShiftRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("Transactions")[0], (int)VarioSL.Entities.EntityType.ShiftEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "Transactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ShiftState'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShiftState
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ShiftStateCollection(), (IEntityRelation)GetRelationsForField("ShiftState")[0], (int)VarioSL.Entities.EntityType.ShiftEntity, (int)VarioSL.Entities.EntityType.ShiftStateEntity, 0, null, null, null, "ShiftState", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BalanceNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."BALANCENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 14, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BalanceNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.BalanceNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.BalanceNo, value, true); }
		}

		/// <summary> The BankCardRevenue property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."BANKCARDREVENUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BankCardRevenue
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.BankCardRevenue, false); }
			set	{ SetValue((int)ShiftFieldIndex.BankCardRevenue, value, true); }
		}

		/// <summary> The CancellationAmount property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CANCELLATIONAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationAmount
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CancellationAmount, false); }
			set	{ SetValue((int)ShiftFieldIndex.CancellationAmount, value, true); }
		}

		/// <summary> The CancellationCount property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CANCELLATIONCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CancellationCount, false); }
			set	{ SetValue((int)ShiftFieldIndex.CancellationCount, value, true); }
		}

		/// <summary> The CardNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CardNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.CardNo, value, true); }
		}

		/// <summary> The CashContentBegin property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CASHCONTENTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashContentBegin
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CashContentBegin, false); }
			set	{ SetValue((int)ShiftFieldIndex.CashContentBegin, value, true); }
		}

		/// <summary> The CashContentEnd property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CASHCONTENTEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashContentEnd
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CashContentEnd, false); }
			set	{ SetValue((int)ShiftFieldIndex.CashContentEnd, value, true); }
		}

		/// <summary> The CashReceipt property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CASHRECEIPT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashReceipt
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CashReceipt, false); }
			set	{ SetValue((int)ShiftFieldIndex.CashReceipt, value, true); }
		}

		/// <summary> The ClientID property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ClientID
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftFieldIndex.ClientID, false); }
			set	{ SetValue((int)ShiftFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CourseNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."COURSENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CourseNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CourseNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.CourseNo, value, true); }
		}

		/// <summary> The CustomerCardRevenue property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."CUSTOMERCARDREVENUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerCardRevenue
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.CustomerCardRevenue, false); }
			set	{ SetValue((int)ShiftFieldIndex.CustomerCardRevenue, value, true); }
		}

		/// <summary> The DebitAccount property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."DEBITACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebitAccount
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.DebitAccount, false); }
			set	{ SetValue((int)ShiftFieldIndex.DebitAccount, value, true); }
		}

		/// <summary> The DebtorNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."DEBTORNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.DebtorNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.DebtorNo, value, true); }
		}

		/// <summary> The DepositNumber property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."DEPOSITNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DepositNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.DepositNumber, false); }
			set	{ SetValue((int)ShiftFieldIndex.DepositNumber, value, true); }
		}

		/// <summary> The DeviceNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.DeviceNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.DeviceNo, value, true); }
		}

		/// <summary> The DeviceType property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."DEVICETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceType
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.DeviceType, false); }
			set	{ SetValue((int)ShiftFieldIndex.DeviceType, value, true); }
		}

		/// <summary> The ExportDate property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."EXPORTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExportDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftFieldIndex.ExportDate, false); }
			set	{ SetValue((int)ShiftFieldIndex.ExportDate, value, true); }
		}

		/// <summary> The ExportState property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."EXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportState
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.ExportState, false); }
			set	{ SetValue((int)ShiftFieldIndex.ExportState, value, true); }
		}

		/// <summary> The FirstTicketNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."FIRSTTICKETNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FirstTicketNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.FirstTicketNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.FirstTicketNo, value, true); }
		}

		/// <summary> The HashValue property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."HASHVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HashValue
		{
			get { return (System.String)GetValue((int)ShiftFieldIndex.HashValue, true); }
			set	{ SetValue((int)ShiftFieldIndex.HashValue, value, true); }
		}

		/// <summary> The ImportDate property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."IMPORTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ImportDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftFieldIndex.ImportDate, false); }
			set	{ SetValue((int)ShiftFieldIndex.ImportDate, value, true); }
		}

		/// <summary> The IsBroken property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."ISBROKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsBroken
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ShiftFieldIndex.IsBroken, false); }
			set	{ SetValue((int)ShiftFieldIndex.IsBroken, value, true); }
		}

		/// <summary> The IsDst property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."ISDST"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDst
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftFieldIndex.IsDst, false); }
			set	{ SetValue((int)ShiftFieldIndex.IsDst, value, true); }
		}

		/// <summary> The LastTicketNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."LASTTICKETNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastTicketNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.LastTicketNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.LastTicketNo, value, true); }
		}

		/// <summary> The LineNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."LINENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.LineNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.LineNo, value, true); }
		}

		/// <summary> The LocationNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."LOCATIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LocationNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.LocationNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.LocationNo, value, true); }
		}

		/// <summary> The LogOffDebtor property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."LOGOFFDEBTOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LogOffDebtor
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.LogOffDebtor, false); }
			set	{ SetValue((int)ShiftFieldIndex.LogOffDebtor, value, true); }
		}

		/// <summary> The MountingPlateNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."MOUNTINGPLATENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MountingPlateNo
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftFieldIndex.MountingPlateNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.MountingPlateNo, value, true); }
		}

		/// <summary> The PosPaymentID property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."POSPAYMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PosPaymentID
		{
			get { return (System.Int64)GetValue((int)ShiftFieldIndex.PosPaymentID, true); }
			set	{ SetValue((int)ShiftFieldIndex.PosPaymentID, value, true); }
		}

		/// <summary> The PriceReduction property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."PRICEREDUCTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PriceReduction
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.PriceReduction, false); }
			set	{ SetValue((int)ShiftFieldIndex.PriceReduction, value, true); }
		}

		/// <summary> The RouteNo property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."ROUTENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RouteNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.RouteNo, false); }
			set	{ SetValue((int)ShiftFieldIndex.RouteNo, value, true); }
		}

		/// <summary> The SalesVolume property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SALESVOLUME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesVolume
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.SalesVolume, false); }
			set	{ SetValue((int)ShiftFieldIndex.SalesVolume, value, true); }
		}

		/// <summary> The SettlementID property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.SettlementID, false); }
			set	{ SetValue((int)ShiftFieldIndex.SettlementID, value, true); }
		}

		/// <summary> The ShiftBegin property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftBegin
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftFieldIndex.ShiftBegin, false); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftBegin, value, true); }
		}

		/// <summary> The ShiftCounter property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShiftCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.ShiftCounter, false); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftCounter, value, true); }
		}

		/// <summary> The ShiftEnd property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ShiftFieldIndex.ShiftEnd, false); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftEnd, value, true); }
		}

		/// <summary> The ShiftID property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ShiftID
		{
			get { return (System.Int64)GetValue((int)ShiftFieldIndex.ShiftID, true); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftID, value, true); }
		}

		/// <summary> The ShiftNumber property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShiftNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ShiftFieldIndex.ShiftNumber, false); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftNumber, value, true); }
		}

		/// <summary> The ShiftStateID property of the Entity Shift<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_SHIFT"."SHIFTSTATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ShiftStateID
		{
			get { return (Nullable<System.Int16>)GetValue((int)ShiftFieldIndex.ShiftStateID, false); }
			set	{ SetValue((int)ShiftFieldIndex.ShiftStateID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionCollection Transactions
		{
			get	{ return GetMultiTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Transactions. When set to true, Transactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Transactions is accessed. You can always execute/ a forced fetch by calling GetMultiTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactions
		{
			get	{ return _alwaysFetchTransactions; }
			set	{ _alwaysFetchTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Transactions already has been fetched. Setting this property to false when Transactions has been fetched
		/// will clear the Transactions collection well. Setting this property to true while Transactions hasn't been fetched disables lazy loading for Transactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactions
		{
			get { return _alreadyFetchedTransactions;}
			set 
			{
				if(_alreadyFetchedTransactions && !value && (_transactions != null))
				{
					_transactions.Clear();
				}
				_alreadyFetchedTransactions = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ShiftStateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleShiftState()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ShiftStateEntity ShiftState
		{
			get	{ return GetSingleShiftState(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncShiftState(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Shifts", "ShiftState", _shiftState, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ShiftState. When set to true, ShiftState is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ShiftState is accessed. You can always execute a forced fetch by calling GetSingleShiftState(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShiftState
		{
			get	{ return _alwaysFetchShiftState; }
			set	{ _alwaysFetchShiftState = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ShiftState already has been fetched. Setting this property to false when ShiftState has been fetched
		/// will set ShiftState to null as well. Setting this property to true while ShiftState hasn't been fetched disables lazy loading for ShiftState</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShiftState
		{
			get { return _alreadyFetchedShiftState;}
			set 
			{
				if(_alreadyFetchedShiftState && !value)
				{
					this.ShiftState = null;
				}
				_alreadyFetchedShiftState = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ShiftState is not found
		/// in the database. When set to true, ShiftState will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ShiftStateReturnsNewIfNotFound
		{
			get	{ return _shiftStateReturnsNewIfNotFound; }
			set { _shiftStateReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ShiftEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
