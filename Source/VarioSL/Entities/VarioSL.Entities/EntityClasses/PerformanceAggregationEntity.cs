﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PerformanceAggregation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PerformanceAggregationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private PerformanceIndicatorEntity _performanceIndicator;
		private bool	_alwaysFetchPerformanceIndicator, _alreadyFetchedPerformanceIndicator, _performanceIndicatorReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PerformanceIndicator</summary>
			public static readonly string PerformanceIndicator = "PerformanceIndicator";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PerformanceAggregationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PerformanceAggregationEntity() :base("PerformanceAggregationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		public PerformanceAggregationEntity(System.Int64 aggregationID):base("PerformanceAggregationEntity")
		{
			InitClassFetch(aggregationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PerformanceAggregationEntity(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse):base("PerformanceAggregationEntity")
		{
			InitClassFetch(aggregationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="validator">The custom validator object for this PerformanceAggregationEntity</param>
		public PerformanceAggregationEntity(System.Int64 aggregationID, IValidator validator):base("PerformanceAggregationEntity")
		{
			InitClassFetch(aggregationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PerformanceAggregationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_performanceIndicator = (PerformanceIndicatorEntity)info.GetValue("_performanceIndicator", typeof(PerformanceIndicatorEntity));
			if(_performanceIndicator!=null)
			{
				_performanceIndicator.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_performanceIndicatorReturnsNewIfNotFound = info.GetBoolean("_performanceIndicatorReturnsNewIfNotFound");
			_alwaysFetchPerformanceIndicator = info.GetBoolean("_alwaysFetchPerformanceIndicator");
			_alreadyFetchedPerformanceIndicator = info.GetBoolean("_alreadyFetchedPerformanceIndicator");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PerformanceAggregationFieldIndex)fieldIndex)
			{
				case PerformanceAggregationFieldIndex.IndicatorID:
					DesetupSyncPerformanceIndicator(true, false);
					_alreadyFetchedPerformanceIndicator = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPerformanceIndicator = (_performanceIndicator != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PerformanceIndicator":
					toReturn.Add(Relations.PerformanceIndicatorEntityUsingIndicatorID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_performanceIndicator", (!this.MarkedForDeletion?_performanceIndicator:null));
			info.AddValue("_performanceIndicatorReturnsNewIfNotFound", _performanceIndicatorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerformanceIndicator", _alwaysFetchPerformanceIndicator);
			info.AddValue("_alreadyFetchedPerformanceIndicator", _alreadyFetchedPerformanceIndicator);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PerformanceIndicator":
					_alreadyFetchedPerformanceIndicator = true;
					this.PerformanceIndicator = (PerformanceIndicatorEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PerformanceIndicator":
					SetupSyncPerformanceIndicator(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PerformanceIndicator":
					DesetupSyncPerformanceIndicator(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_performanceIndicator!=null)
			{
				toReturn.Add(_performanceIndicator);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID)
		{
			return FetchUsingPK(aggregationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(aggregationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(aggregationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(aggregationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AggregationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PerformanceAggregationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PerformanceIndicatorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PerformanceIndicatorEntity' which is related to this entity.</returns>
		public PerformanceIndicatorEntity GetSinglePerformanceIndicator()
		{
			return GetSinglePerformanceIndicator(false);
		}

		/// <summary> Retrieves the related entity of type 'PerformanceIndicatorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PerformanceIndicatorEntity' which is related to this entity.</returns>
		public virtual PerformanceIndicatorEntity GetSinglePerformanceIndicator(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerformanceIndicator || forceFetch || _alwaysFetchPerformanceIndicator) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PerformanceIndicatorEntityUsingIndicatorID);
				PerformanceIndicatorEntity newEntity = new PerformanceIndicatorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IndicatorID);
				}
				if(fetchResult)
				{
					newEntity = (PerformanceIndicatorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_performanceIndicatorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PerformanceIndicator = newEntity;
				_alreadyFetchedPerformanceIndicator = fetchResult;
			}
			return _performanceIndicator;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PerformanceIndicator", _performanceIndicator);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="validator">The validator object for this PerformanceAggregationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 aggregationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(aggregationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_performanceIndicatorReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationFunction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IndicatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _performanceIndicator</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerformanceIndicator(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _performanceIndicator, new PropertyChangedEventHandler( OnPerformanceIndicatorPropertyChanged ), "PerformanceIndicator", VarioSL.Entities.RelationClasses.StaticPerformanceAggregationRelations.PerformanceIndicatorEntityUsingIndicatorIDStatic, true, signalRelatedEntity, "Aggregations", resetFKFields, new int[] { (int)PerformanceAggregationFieldIndex.IndicatorID } );		
			_performanceIndicator = null;
		}
		
		/// <summary> setups the sync logic for member _performanceIndicator</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerformanceIndicator(IEntityCore relatedEntity)
		{
			if(_performanceIndicator!=relatedEntity)
			{		
				DesetupSyncPerformanceIndicator(true, true);
				_performanceIndicator = (PerformanceIndicatorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _performanceIndicator, new PropertyChangedEventHandler( OnPerformanceIndicatorPropertyChanged ), "PerformanceIndicator", VarioSL.Entities.RelationClasses.StaticPerformanceAggregationRelations.PerformanceIndicatorEntityUsingIndicatorIDStatic, true, ref _alreadyFetchedPerformanceIndicator, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPerformanceIndicatorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="aggregationID">PK value for PerformanceAggregation which data should be fetched into this PerformanceAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PerformanceAggregationFieldIndex.AggregationID].ForcedCurrentValueWrite(aggregationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePerformanceAggregationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PerformanceAggregationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PerformanceAggregationRelations Relations
		{
			get	{ return new PerformanceAggregationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PerformanceIndicator'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerformanceIndicator
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PerformanceIndicatorCollection(), (IEntityRelation)GetRelationsForField("PerformanceIndicator")[0], (int)VarioSL.Entities.EntityType.PerformanceAggregationEntity, (int)VarioSL.Entities.EntityType.PerformanceIndicatorEntity, 0, null, null, null, "PerformanceIndicator", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AggregationFrom property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."AGGREGATIONFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationFrom
		{
			get { return (System.DateTime)GetValue((int)PerformanceAggregationFieldIndex.AggregationFrom, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.AggregationFrom, value, true); }
		}

		/// <summary> The AggregationFunction property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."AGGREGATIONFUNCTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.AggregationFunction AggregationFunction
		{
			get { return (VarioSL.Entities.Enumerations.AggregationFunction)GetValue((int)PerformanceAggregationFieldIndex.AggregationFunction, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.AggregationFunction, value, true); }
		}

		/// <summary> The AggregationID property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."AGGREGATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AggregationID
		{
			get { return (System.Int64)GetValue((int)PerformanceAggregationFieldIndex.AggregationID, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.AggregationID, value, true); }
		}

		/// <summary> The AggregationTo property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."AGGREGATIONTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationTo
		{
			get { return (System.DateTime)GetValue((int)PerformanceAggregationFieldIndex.AggregationTo, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.AggregationTo, value, true); }
		}

		/// <summary> The AggregationValue property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."AGGREGATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal AggregationValue
		{
			get { return (System.Decimal)GetValue((int)PerformanceAggregationFieldIndex.AggregationValue, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.AggregationValue, value, true); }
		}

		/// <summary> The Description property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PerformanceAggregationFieldIndex.Description, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.Description, value, true); }
		}

		/// <summary> The IndicatorID property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."INDICATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 IndicatorID
		{
			get { return (System.Int64)GetValue((int)PerformanceAggregationFieldIndex.IndicatorID, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.IndicatorID, value, true); }
		}

		/// <summary> The LastModified property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PerformanceAggregationFieldIndex.LastModified, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PerformanceAggregationFieldIndex.LastUser, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PerformanceAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_AGGREGATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PerformanceAggregationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PerformanceAggregationFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PerformanceIndicatorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerformanceIndicator()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PerformanceIndicatorEntity PerformanceIndicator
		{
			get	{ return GetSinglePerformanceIndicator(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerformanceIndicator(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Aggregations", "PerformanceIndicator", _performanceIndicator, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PerformanceIndicator. When set to true, PerformanceIndicator is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PerformanceIndicator is accessed. You can always execute a forced fetch by calling GetSinglePerformanceIndicator(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerformanceIndicator
		{
			get	{ return _alwaysFetchPerformanceIndicator; }
			set	{ _alwaysFetchPerformanceIndicator = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PerformanceIndicator already has been fetched. Setting this property to false when PerformanceIndicator has been fetched
		/// will set PerformanceIndicator to null as well. Setting this property to true while PerformanceIndicator hasn't been fetched disables lazy loading for PerformanceIndicator</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerformanceIndicator
		{
			get { return _alreadyFetchedPerformanceIndicator;}
			set 
			{
				if(_alreadyFetchedPerformanceIndicator && !value)
				{
					this.PerformanceIndicator = null;
				}
				_alreadyFetchedPerformanceIndicator = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PerformanceIndicator is not found
		/// in the database. When set to true, PerformanceIndicator will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PerformanceIndicatorReturnsNewIfNotFound
		{
			get	{ return _performanceIndicatorReturnsNewIfNotFound; }
			set { _performanceIndicatorReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PerformanceAggregationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
