﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ComponentFilling'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ComponentFillingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AutomatEntity _automat;
		private bool	_alwaysFetchAutomat, _alreadyFetchedAutomat, _automatReturnsNewIfNotFound;
		private CashUnitEntity _cashUnit;
		private bool	_alwaysFetchCashUnit, _alreadyFetchedCashUnit, _cashUnitReturnsNewIfNotFound;
		private ComponentEntity _component;
		private bool	_alwaysFetchComponent, _alreadyFetchedComponent, _componentReturnsNewIfNotFound;
		private DebtorEntity _maintenanceStaff;
		private bool	_alwaysFetchMaintenanceStaff, _alreadyFetchedMaintenanceStaff, _maintenanceStaffReturnsNewIfNotFound;
		private DebtorEntity _personnelFilling;
		private bool	_alwaysFetchPersonnelFilling, _alreadyFetchedPersonnelFilling, _personnelFillingReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Automat</summary>
			public static readonly string Automat = "Automat";
			/// <summary>Member name CashUnit</summary>
			public static readonly string CashUnit = "CashUnit";
			/// <summary>Member name Component</summary>
			public static readonly string Component = "Component";
			/// <summary>Member name MaintenanceStaff</summary>
			public static readonly string MaintenanceStaff = "MaintenanceStaff";
			/// <summary>Member name PersonnelFilling</summary>
			public static readonly string PersonnelFilling = "PersonnelFilling";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ComponentFillingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ComponentFillingEntity() :base("ComponentFillingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		public ComponentFillingEntity(System.Int64 componentFillingID):base("ComponentFillingEntity")
		{
			InitClassFetch(componentFillingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ComponentFillingEntity(System.Int64 componentFillingID, IPrefetchPath prefetchPathToUse):base("ComponentFillingEntity")
		{
			InitClassFetch(componentFillingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="validator">The custom validator object for this ComponentFillingEntity</param>
		public ComponentFillingEntity(System.Int64 componentFillingID, IValidator validator):base("ComponentFillingEntity")
		{
			InitClassFetch(componentFillingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ComponentFillingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_automat = (AutomatEntity)info.GetValue("_automat", typeof(AutomatEntity));
			if(_automat!=null)
			{
				_automat.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_automatReturnsNewIfNotFound = info.GetBoolean("_automatReturnsNewIfNotFound");
			_alwaysFetchAutomat = info.GetBoolean("_alwaysFetchAutomat");
			_alreadyFetchedAutomat = info.GetBoolean("_alreadyFetchedAutomat");

			_cashUnit = (CashUnitEntity)info.GetValue("_cashUnit", typeof(CashUnitEntity));
			if(_cashUnit!=null)
			{
				_cashUnit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cashUnitReturnsNewIfNotFound = info.GetBoolean("_cashUnitReturnsNewIfNotFound");
			_alwaysFetchCashUnit = info.GetBoolean("_alwaysFetchCashUnit");
			_alreadyFetchedCashUnit = info.GetBoolean("_alreadyFetchedCashUnit");

			_component = (ComponentEntity)info.GetValue("_component", typeof(ComponentEntity));
			if(_component!=null)
			{
				_component.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_componentReturnsNewIfNotFound = info.GetBoolean("_componentReturnsNewIfNotFound");
			_alwaysFetchComponent = info.GetBoolean("_alwaysFetchComponent");
			_alreadyFetchedComponent = info.GetBoolean("_alreadyFetchedComponent");

			_maintenanceStaff = (DebtorEntity)info.GetValue("_maintenanceStaff", typeof(DebtorEntity));
			if(_maintenanceStaff!=null)
			{
				_maintenanceStaff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_maintenanceStaffReturnsNewIfNotFound = info.GetBoolean("_maintenanceStaffReturnsNewIfNotFound");
			_alwaysFetchMaintenanceStaff = info.GetBoolean("_alwaysFetchMaintenanceStaff");
			_alreadyFetchedMaintenanceStaff = info.GetBoolean("_alreadyFetchedMaintenanceStaff");

			_personnelFilling = (DebtorEntity)info.GetValue("_personnelFilling", typeof(DebtorEntity));
			if(_personnelFilling!=null)
			{
				_personnelFilling.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personnelFillingReturnsNewIfNotFound = info.GetBoolean("_personnelFillingReturnsNewIfNotFound");
			_alwaysFetchPersonnelFilling = info.GetBoolean("_alwaysFetchPersonnelFilling");
			_alreadyFetchedPersonnelFilling = info.GetBoolean("_alreadyFetchedPersonnelFilling");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ComponentFillingFieldIndex)fieldIndex)
			{
				case ComponentFillingFieldIndex.AutomatID:
					DesetupSyncAutomat(true, false);
					_alreadyFetchedAutomat = false;
					break;
				case ComponentFillingFieldIndex.CashUnitID:
					DesetupSyncCashUnit(true, false);
					_alreadyFetchedCashUnit = false;
					break;
				case ComponentFillingFieldIndex.ComponentID:
					DesetupSyncComponent(true, false);
					_alreadyFetchedComponent = false;
					break;
				case ComponentFillingFieldIndex.MaintenanceStaffID:
					DesetupSyncMaintenanceStaff(true, false);
					_alreadyFetchedMaintenanceStaff = false;
					break;
				case ComponentFillingFieldIndex.PersonnelFillingID:
					DesetupSyncPersonnelFilling(true, false);
					_alreadyFetchedPersonnelFilling = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutomat = (_automat != null);
			_alreadyFetchedCashUnit = (_cashUnit != null);
			_alreadyFetchedComponent = (_component != null);
			_alreadyFetchedMaintenanceStaff = (_maintenanceStaff != null);
			_alreadyFetchedPersonnelFilling = (_personnelFilling != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Automat":
					toReturn.Add(Relations.AutomatEntityUsingAutomatID);
					break;
				case "CashUnit":
					toReturn.Add(Relations.CashUnitEntityUsingCashUnitID);
					break;
				case "Component":
					toReturn.Add(Relations.ComponentEntityUsingComponentID);
					break;
				case "MaintenanceStaff":
					toReturn.Add(Relations.DebtorEntityUsingMaintenanceStaffID);
					break;
				case "PersonnelFilling":
					toReturn.Add(Relations.DebtorEntityUsingPersonnelFillingID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_automat", (!this.MarkedForDeletion?_automat:null));
			info.AddValue("_automatReturnsNewIfNotFound", _automatReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutomat", _alwaysFetchAutomat);
			info.AddValue("_alreadyFetchedAutomat", _alreadyFetchedAutomat);
			info.AddValue("_cashUnit", (!this.MarkedForDeletion?_cashUnit:null));
			info.AddValue("_cashUnitReturnsNewIfNotFound", _cashUnitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCashUnit", _alwaysFetchCashUnit);
			info.AddValue("_alreadyFetchedCashUnit", _alreadyFetchedCashUnit);
			info.AddValue("_component", (!this.MarkedForDeletion?_component:null));
			info.AddValue("_componentReturnsNewIfNotFound", _componentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchComponent", _alwaysFetchComponent);
			info.AddValue("_alreadyFetchedComponent", _alreadyFetchedComponent);
			info.AddValue("_maintenanceStaff", (!this.MarkedForDeletion?_maintenanceStaff:null));
			info.AddValue("_maintenanceStaffReturnsNewIfNotFound", _maintenanceStaffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMaintenanceStaff", _alwaysFetchMaintenanceStaff);
			info.AddValue("_alreadyFetchedMaintenanceStaff", _alreadyFetchedMaintenanceStaff);
			info.AddValue("_personnelFilling", (!this.MarkedForDeletion?_personnelFilling:null));
			info.AddValue("_personnelFillingReturnsNewIfNotFound", _personnelFillingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPersonnelFilling", _alwaysFetchPersonnelFilling);
			info.AddValue("_alreadyFetchedPersonnelFilling", _alreadyFetchedPersonnelFilling);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Automat":
					_alreadyFetchedAutomat = true;
					this.Automat = (AutomatEntity)entity;
					break;
				case "CashUnit":
					_alreadyFetchedCashUnit = true;
					this.CashUnit = (CashUnitEntity)entity;
					break;
				case "Component":
					_alreadyFetchedComponent = true;
					this.Component = (ComponentEntity)entity;
					break;
				case "MaintenanceStaff":
					_alreadyFetchedMaintenanceStaff = true;
					this.MaintenanceStaff = (DebtorEntity)entity;
					break;
				case "PersonnelFilling":
					_alreadyFetchedPersonnelFilling = true;
					this.PersonnelFilling = (DebtorEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Automat":
					SetupSyncAutomat(relatedEntity);
					break;
				case "CashUnit":
					SetupSyncCashUnit(relatedEntity);
					break;
				case "Component":
					SetupSyncComponent(relatedEntity);
					break;
				case "MaintenanceStaff":
					SetupSyncMaintenanceStaff(relatedEntity);
					break;
				case "PersonnelFilling":
					SetupSyncPersonnelFilling(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Automat":
					DesetupSyncAutomat(false, true);
					break;
				case "CashUnit":
					DesetupSyncCashUnit(false, true);
					break;
				case "Component":
					DesetupSyncComponent(false, true);
					break;
				case "MaintenanceStaff":
					DesetupSyncMaintenanceStaff(false, true);
					break;
				case "PersonnelFilling":
					DesetupSyncPersonnelFilling(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_automat!=null)
			{
				toReturn.Add(_automat);
			}
			if(_cashUnit!=null)
			{
				toReturn.Add(_cashUnit);
			}
			if(_component!=null)
			{
				toReturn.Add(_component);
			}
			if(_maintenanceStaff!=null)
			{
				toReturn.Add(_maintenanceStaff);
			}
			if(_personnelFilling!=null)
			{
				toReturn.Add(_personnelFilling);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentFillingID)
		{
			return FetchUsingPK(componentFillingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentFillingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(componentFillingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentFillingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(componentFillingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentFillingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(componentFillingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ComponentFillingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ComponentFillingRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public AutomatEntity GetSingleAutomat()
		{
			return GetSingleAutomat(false);
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public virtual AutomatEntity GetSingleAutomat(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutomat || forceFetch || _alwaysFetchAutomat) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutomatEntityUsingAutomatID);
				AutomatEntity newEntity = new AutomatEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutomatID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AutomatEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_automatReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Automat = newEntity;
				_alreadyFetchedAutomat = fetchResult;
			}
			return _automat;
		}


		/// <summary> Retrieves the related entity of type 'CashUnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CashUnitEntity' which is related to this entity.</returns>
		public CashUnitEntity GetSingleCashUnit()
		{
			return GetSingleCashUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'CashUnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CashUnitEntity' which is related to this entity.</returns>
		public virtual CashUnitEntity GetSingleCashUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedCashUnit || forceFetch || _alwaysFetchCashUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CashUnitEntityUsingCashUnitID);
				CashUnitEntity newEntity = new CashUnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CashUnitID);
				}
				if(fetchResult)
				{
					newEntity = (CashUnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cashUnitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CashUnit = newEntity;
				_alreadyFetchedCashUnit = fetchResult;
			}
			return _cashUnit;
		}


		/// <summary> Retrieves the related entity of type 'ComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ComponentEntity' which is related to this entity.</returns>
		public ComponentEntity GetSingleComponent()
		{
			return GetSingleComponent(false);
		}

		/// <summary> Retrieves the related entity of type 'ComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ComponentEntity' which is related to this entity.</returns>
		public virtual ComponentEntity GetSingleComponent(bool forceFetch)
		{
			if( ( !_alreadyFetchedComponent || forceFetch || _alwaysFetchComponent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ComponentEntityUsingComponentID);
				ComponentEntity newEntity = new ComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ComponentID);
				}
				if(fetchResult)
				{
					newEntity = (ComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_componentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Component = newEntity;
				_alreadyFetchedComponent = fetchResult;
			}
			return _component;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleMaintenanceStaff()
		{
			return GetSingleMaintenanceStaff(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleMaintenanceStaff(bool forceFetch)
		{
			if( ( !_alreadyFetchedMaintenanceStaff || forceFetch || _alwaysFetchMaintenanceStaff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingMaintenanceStaffID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MaintenanceStaffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_maintenanceStaffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MaintenanceStaff = newEntity;
				_alreadyFetchedMaintenanceStaff = fetchResult;
			}
			return _maintenanceStaff;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSinglePersonnelFilling()
		{
			return GetSinglePersonnelFilling(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSinglePersonnelFilling(bool forceFetch)
		{
			if( ( !_alreadyFetchedPersonnelFilling || forceFetch || _alwaysFetchPersonnelFilling) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingPersonnelFillingID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PersonnelFillingID);
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personnelFillingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PersonnelFilling = newEntity;
				_alreadyFetchedPersonnelFilling = fetchResult;
			}
			return _personnelFilling;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Automat", _automat);
			toReturn.Add("CashUnit", _cashUnit);
			toReturn.Add("Component", _component);
			toReturn.Add("MaintenanceStaff", _maintenanceStaff);
			toReturn.Add("PersonnelFilling", _personnelFilling);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="validator">The validator object for this ComponentFillingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 componentFillingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(componentFillingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_automatReturnsNewIfNotFound = false;
			_cashUnitReturnsNewIfNotFound = false;
			_componentReturnsNewIfNotFound = false;
			_maintenanceStaffReturnsNewIfNotFound = false;
			_personnelFillingReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentFillingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FeedDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FillingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBooked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationMark", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoginDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaintenanceStaffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfCoins", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonnelFillingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageMediumName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageMediumNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _automat</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutomat(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.AutomatEntityUsingAutomatIDStatic, true, signalRelatedEntity, "ComponentFillings", resetFKFields, new int[] { (int)ComponentFillingFieldIndex.AutomatID } );		
			_automat = null;
		}
		
		/// <summary> setups the sync logic for member _automat</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutomat(IEntityCore relatedEntity)
		{
			if(_automat!=relatedEntity)
			{		
				DesetupSyncAutomat(true, true);
				_automat = (AutomatEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.AutomatEntityUsingAutomatIDStatic, true, ref _alreadyFetchedAutomat, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutomatPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cashUnit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCashUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cashUnit, new PropertyChangedEventHandler( OnCashUnitPropertyChanged ), "CashUnit", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.CashUnitEntityUsingCashUnitIDStatic, true, signalRelatedEntity, "ComponentFillings", resetFKFields, new int[] { (int)ComponentFillingFieldIndex.CashUnitID } );		
			_cashUnit = null;
		}
		
		/// <summary> setups the sync logic for member _cashUnit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCashUnit(IEntityCore relatedEntity)
		{
			if(_cashUnit!=relatedEntity)
			{		
				DesetupSyncCashUnit(true, true);
				_cashUnit = (CashUnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cashUnit, new PropertyChangedEventHandler( OnCashUnitPropertyChanged ), "CashUnit", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.CashUnitEntityUsingCashUnitIDStatic, true, ref _alreadyFetchedCashUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCashUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _component</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncComponent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _component, new PropertyChangedEventHandler( OnComponentPropertyChanged ), "Component", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.ComponentEntityUsingComponentIDStatic, true, signalRelatedEntity, "ComponentFillings", resetFKFields, new int[] { (int)ComponentFillingFieldIndex.ComponentID } );		
			_component = null;
		}
		
		/// <summary> setups the sync logic for member _component</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncComponent(IEntityCore relatedEntity)
		{
			if(_component!=relatedEntity)
			{		
				DesetupSyncComponent(true, true);
				_component = (ComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _component, new PropertyChangedEventHandler( OnComponentPropertyChanged ), "Component", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.ComponentEntityUsingComponentIDStatic, true, ref _alreadyFetchedComponent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnComponentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _maintenanceStaff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMaintenanceStaff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _maintenanceStaff, new PropertyChangedEventHandler( OnMaintenanceStaffPropertyChanged ), "MaintenanceStaff", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.DebtorEntityUsingMaintenanceStaffIDStatic, true, signalRelatedEntity, "ComponentFillingsByMaintenanceStaff", resetFKFields, new int[] { (int)ComponentFillingFieldIndex.MaintenanceStaffID } );		
			_maintenanceStaff = null;
		}
		
		/// <summary> setups the sync logic for member _maintenanceStaff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMaintenanceStaff(IEntityCore relatedEntity)
		{
			if(_maintenanceStaff!=relatedEntity)
			{		
				DesetupSyncMaintenanceStaff(true, true);
				_maintenanceStaff = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _maintenanceStaff, new PropertyChangedEventHandler( OnMaintenanceStaffPropertyChanged ), "MaintenanceStaff", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.DebtorEntityUsingMaintenanceStaffIDStatic, true, ref _alreadyFetchedMaintenanceStaff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMaintenanceStaffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _personnelFilling</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPersonnelFilling(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _personnelFilling, new PropertyChangedEventHandler( OnPersonnelFillingPropertyChanged ), "PersonnelFilling", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.DebtorEntityUsingPersonnelFillingIDStatic, true, signalRelatedEntity, "ComponentFillingsByPersonnelFilling", resetFKFields, new int[] { (int)ComponentFillingFieldIndex.PersonnelFillingID } );		
			_personnelFilling = null;
		}
		
		/// <summary> setups the sync logic for member _personnelFilling</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPersonnelFilling(IEntityCore relatedEntity)
		{
			if(_personnelFilling!=relatedEntity)
			{		
				DesetupSyncPersonnelFilling(true, true);
				_personnelFilling = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _personnelFilling, new PropertyChangedEventHandler( OnPersonnelFillingPropertyChanged ), "PersonnelFilling", VarioSL.Entities.RelationClasses.StaticComponentFillingRelations.DebtorEntityUsingPersonnelFillingIDStatic, true, ref _alreadyFetchedPersonnelFilling, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonnelFillingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="componentFillingID">PK value for ComponentFilling which data should be fetched into this ComponentFilling object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 componentFillingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ComponentFillingFieldIndex.ComponentFillingID].ForcedCurrentValueWrite(componentFillingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateComponentFillingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ComponentFillingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ComponentFillingRelations Relations
		{
			get	{ return new ComponentFillingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Automat'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomat
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutomatCollection(), (IEntityRelation)GetRelationsForField("Automat")[0], (int)VarioSL.Entities.EntityType.ComponentFillingEntity, (int)VarioSL.Entities.EntityType.AutomatEntity, 0, null, null, null, "Automat", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashUnit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCashUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashUnitCollection(), (IEntityRelation)GetRelationsForField("CashUnit")[0], (int)VarioSL.Entities.EntityType.ComponentFillingEntity, (int)VarioSL.Entities.EntityType.CashUnitEntity, 0, null, null, null, "CashUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Component'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentCollection(), (IEntityRelation)GetRelationsForField("Component")[0], (int)VarioSL.Entities.EntityType.ComponentFillingEntity, (int)VarioSL.Entities.EntityType.ComponentEntity, 0, null, null, null, "Component", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMaintenanceStaff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("MaintenanceStaff")[0], (int)VarioSL.Entities.EntityType.ComponentFillingEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "MaintenanceStaff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPersonnelFilling
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("PersonnelFilling")[0], (int)VarioSL.Entities.EntityType.ComponentFillingEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "PersonnelFilling", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutomatID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."AUTOMATID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AutomatID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFillingFieldIndex.AutomatID, false); }
			set	{ SetValue((int)ComponentFillingFieldIndex.AutomatID, value, true); }
		}

		/// <summary> The BookingNo property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."BOOKINGNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BookingNo
		{
			get { return (System.Int64)GetValue((int)ComponentFillingFieldIndex.BookingNo, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.BookingNo, value, true); }
		}

		/// <summary> The CashUnitID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."CASHUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CashUnitID
		{
			get { return (System.Int64)GetValue((int)ComponentFillingFieldIndex.CashUnitID, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.CashUnitID, value, true); }
		}

		/// <summary> The ComponentFillingID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."COMPONENTFILLINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ComponentFillingID
		{
			get { return (System.Int64)GetValue((int)ComponentFillingFieldIndex.ComponentFillingID, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.ComponentFillingID, value, true); }
		}

		/// <summary> The ComponentID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."COMPONENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ComponentID
		{
			get { return (System.Int64)GetValue((int)ComponentFillingFieldIndex.ComponentID, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.ComponentID, value, true); }
		}

		/// <summary> The FeedDate property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."FEEDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FeedDate
		{
			get { return (System.DateTime)GetValue((int)ComponentFillingFieldIndex.FeedDate, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.FeedDate, value, true); }
		}

		/// <summary> The FillingDate property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."FILLINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FillingDate
		{
			get { return (System.DateTime)GetValue((int)ComponentFillingFieldIndex.FillingDate, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.FillingDate, value, true); }
		}

		/// <summary> The IsBooked property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."ISBOOKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBooked
		{
			get { return (System.Boolean)GetValue((int)ComponentFillingFieldIndex.IsBooked, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.IsBooked, value, true); }
		}

		/// <summary> The LocationMark property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."LOCATIONMARK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LocationMark
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentFillingFieldIndex.LocationMark, false); }
			set	{ SetValue((int)ComponentFillingFieldIndex.LocationMark, value, true); }
		}

		/// <summary> The LoginDate property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."LOGINDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LoginDate
		{
			get { return (System.DateTime)GetValue((int)ComponentFillingFieldIndex.LoginDate, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.LoginDate, value, true); }
		}

		/// <summary> The MaintenanceStaffID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."MAINTENANCESTAFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaintenanceStaffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentFillingFieldIndex.MaintenanceStaffID, false); }
			set	{ SetValue((int)ComponentFillingFieldIndex.MaintenanceStaffID, value, true); }
		}

		/// <summary> The MountingPlateNumber property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."MOUNTINGPLATENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MountingPlateNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentFillingFieldIndex.MountingPlateNumber, false); }
			set	{ SetValue((int)ComponentFillingFieldIndex.MountingPlateNumber, value, true); }
		}

		/// <summary> The NumberOfCoins property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."NUMBEROFCOINS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NumberOfCoins
		{
			get { return (System.Int32)GetValue((int)ComponentFillingFieldIndex.NumberOfCoins, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.NumberOfCoins, value, true); }
		}

		/// <summary> The PersonnelFillingID property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."PERSONNELFILLINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonnelFillingID
		{
			get { return (System.Int64)GetValue((int)ComponentFillingFieldIndex.PersonnelFillingID, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.PersonnelFillingID, value, true); }
		}

		/// <summary> The StorageMediumName property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."STORAGEMEDIUMNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StorageMediumName
		{
			get { return (System.String)GetValue((int)ComponentFillingFieldIndex.StorageMediumName, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.StorageMediumName, value, true); }
		}

		/// <summary> The StorageMediumNo property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."STORAGEMEDIUMNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StorageMediumNo
		{
			get { return (System.String)GetValue((int)ComponentFillingFieldIndex.StorageMediumNo, true); }
			set	{ SetValue((int)ComponentFillingFieldIndex.StorageMediumNo, value, true); }
		}

		/// <summary> The VehicleNo property of the Entity ComponentFilling<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTFILLING"."VEHICLENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VehicleNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentFillingFieldIndex.VehicleNo, false); }
			set	{ SetValue((int)ComponentFillingFieldIndex.VehicleNo, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AutomatEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutomat()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutomatEntity Automat
		{
			get	{ return GetSingleAutomat(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutomat(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentFillings", "Automat", _automat, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Automat. When set to true, Automat is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Automat is accessed. You can always execute a forced fetch by calling GetSingleAutomat(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomat
		{
			get	{ return _alwaysFetchAutomat; }
			set	{ _alwaysFetchAutomat = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Automat already has been fetched. Setting this property to false when Automat has been fetched
		/// will set Automat to null as well. Setting this property to true while Automat hasn't been fetched disables lazy loading for Automat</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomat
		{
			get { return _alreadyFetchedAutomat;}
			set 
			{
				if(_alreadyFetchedAutomat && !value)
				{
					this.Automat = null;
				}
				_alreadyFetchedAutomat = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Automat is not found
		/// in the database. When set to true, Automat will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutomatReturnsNewIfNotFound
		{
			get	{ return _automatReturnsNewIfNotFound; }
			set { _automatReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CashUnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCashUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CashUnitEntity CashUnit
		{
			get	{ return GetSingleCashUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCashUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentFillings", "CashUnit", _cashUnit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CashUnit. When set to true, CashUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CashUnit is accessed. You can always execute a forced fetch by calling GetSingleCashUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCashUnit
		{
			get	{ return _alwaysFetchCashUnit; }
			set	{ _alwaysFetchCashUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CashUnit already has been fetched. Setting this property to false when CashUnit has been fetched
		/// will set CashUnit to null as well. Setting this property to true while CashUnit hasn't been fetched disables lazy loading for CashUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCashUnit
		{
			get { return _alreadyFetchedCashUnit;}
			set 
			{
				if(_alreadyFetchedCashUnit && !value)
				{
					this.CashUnit = null;
				}
				_alreadyFetchedCashUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CashUnit is not found
		/// in the database. When set to true, CashUnit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CashUnitReturnsNewIfNotFound
		{
			get	{ return _cashUnitReturnsNewIfNotFound; }
			set { _cashUnitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleComponent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ComponentEntity Component
		{
			get	{ return GetSingleComponent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncComponent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentFillings", "Component", _component, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Component. When set to true, Component is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Component is accessed. You can always execute a forced fetch by calling GetSingleComponent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponent
		{
			get	{ return _alwaysFetchComponent; }
			set	{ _alwaysFetchComponent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Component already has been fetched. Setting this property to false when Component has been fetched
		/// will set Component to null as well. Setting this property to true while Component hasn't been fetched disables lazy loading for Component</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponent
		{
			get { return _alreadyFetchedComponent;}
			set 
			{
				if(_alreadyFetchedComponent && !value)
				{
					this.Component = null;
				}
				_alreadyFetchedComponent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Component is not found
		/// in the database. When set to true, Component will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ComponentReturnsNewIfNotFound
		{
			get	{ return _componentReturnsNewIfNotFound; }
			set { _componentReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMaintenanceStaff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity MaintenanceStaff
		{
			get	{ return GetSingleMaintenanceStaff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMaintenanceStaff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentFillingsByMaintenanceStaff", "MaintenanceStaff", _maintenanceStaff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MaintenanceStaff. When set to true, MaintenanceStaff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MaintenanceStaff is accessed. You can always execute a forced fetch by calling GetSingleMaintenanceStaff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMaintenanceStaff
		{
			get	{ return _alwaysFetchMaintenanceStaff; }
			set	{ _alwaysFetchMaintenanceStaff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MaintenanceStaff already has been fetched. Setting this property to false when MaintenanceStaff has been fetched
		/// will set MaintenanceStaff to null as well. Setting this property to true while MaintenanceStaff hasn't been fetched disables lazy loading for MaintenanceStaff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMaintenanceStaff
		{
			get { return _alreadyFetchedMaintenanceStaff;}
			set 
			{
				if(_alreadyFetchedMaintenanceStaff && !value)
				{
					this.MaintenanceStaff = null;
				}
				_alreadyFetchedMaintenanceStaff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MaintenanceStaff is not found
		/// in the database. When set to true, MaintenanceStaff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MaintenanceStaffReturnsNewIfNotFound
		{
			get	{ return _maintenanceStaffReturnsNewIfNotFound; }
			set { _maintenanceStaffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePersonnelFilling()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity PersonnelFilling
		{
			get	{ return GetSinglePersonnelFilling(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPersonnelFilling(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentFillingsByPersonnelFilling", "PersonnelFilling", _personnelFilling, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PersonnelFilling. When set to true, PersonnelFilling is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PersonnelFilling is accessed. You can always execute a forced fetch by calling GetSinglePersonnelFilling(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPersonnelFilling
		{
			get	{ return _alwaysFetchPersonnelFilling; }
			set	{ _alwaysFetchPersonnelFilling = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PersonnelFilling already has been fetched. Setting this property to false when PersonnelFilling has been fetched
		/// will set PersonnelFilling to null as well. Setting this property to true while PersonnelFilling hasn't been fetched disables lazy loading for PersonnelFilling</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPersonnelFilling
		{
			get { return _alreadyFetchedPersonnelFilling;}
			set 
			{
				if(_alreadyFetchedPersonnelFilling && !value)
				{
					this.PersonnelFilling = null;
				}
				_alreadyFetchedPersonnelFilling = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PersonnelFilling is not found
		/// in the database. When set to true, PersonnelFilling will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonnelFillingReturnsNewIfNotFound
		{
			get	{ return _personnelFillingReturnsNewIfNotFound; }
			set { _personnelFillingReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ComponentFillingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
