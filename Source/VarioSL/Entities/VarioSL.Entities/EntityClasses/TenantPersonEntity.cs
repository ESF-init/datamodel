﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TenantPerson'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TenantPersonEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TenantHistoryCollection	_tenantHistories;
		private bool	_alwaysFetchTenantHistories, _alreadyFetchedTenantHistories;
		private TenantEntity _tenant;
		private bool	_alwaysFetchTenant, _alreadyFetchedTenant, _tenantReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tenant</summary>
			public static readonly string Tenant = "Tenant";
			/// <summary>Member name TenantHistories</summary>
			public static readonly string TenantHistories = "TenantHistories";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TenantPersonEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TenantPersonEntity() :base("TenantPersonEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		public TenantPersonEntity(System.Int64 tenantPersonID):base("TenantPersonEntity")
		{
			InitClassFetch(tenantPersonID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TenantPersonEntity(System.Int64 tenantPersonID, IPrefetchPath prefetchPathToUse):base("TenantPersonEntity")
		{
			InitClassFetch(tenantPersonID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="validator">The custom validator object for this TenantPersonEntity</param>
		public TenantPersonEntity(System.Int64 tenantPersonID, IValidator validator):base("TenantPersonEntity")
		{
			InitClassFetch(tenantPersonID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TenantPersonEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_tenantHistories = (VarioSL.Entities.CollectionClasses.TenantHistoryCollection)info.GetValue("_tenantHistories", typeof(VarioSL.Entities.CollectionClasses.TenantHistoryCollection));
			_alwaysFetchTenantHistories = info.GetBoolean("_alwaysFetchTenantHistories");
			_alreadyFetchedTenantHistories = info.GetBoolean("_alreadyFetchedTenantHistories");
			_tenant = (TenantEntity)info.GetValue("_tenant", typeof(TenantEntity));
			if(_tenant!=null)
			{
				_tenant.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tenantReturnsNewIfNotFound = info.GetBoolean("_tenantReturnsNewIfNotFound");
			_alwaysFetchTenant = info.GetBoolean("_alwaysFetchTenant");
			_alreadyFetchedTenant = info.GetBoolean("_alreadyFetchedTenant");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TenantPersonFieldIndex)fieldIndex)
			{
				case TenantPersonFieldIndex.TenantID:
					DesetupSyncTenant(true, false);
					_alreadyFetchedTenant = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTenantHistories = (_tenantHistories.Count > 0);
			_alreadyFetchedTenant = (_tenant != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tenant":
					toReturn.Add(Relations.TenantEntityUsingTenantID);
					break;
				case "TenantHistories":
					toReturn.Add(Relations.TenantHistoryEntityUsingTenantPersonID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_tenantHistories", (!this.MarkedForDeletion?_tenantHistories:null));
			info.AddValue("_alwaysFetchTenantHistories", _alwaysFetchTenantHistories);
			info.AddValue("_alreadyFetchedTenantHistories", _alreadyFetchedTenantHistories);
			info.AddValue("_tenant", (!this.MarkedForDeletion?_tenant:null));
			info.AddValue("_tenantReturnsNewIfNotFound", _tenantReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTenant", _alwaysFetchTenant);
			info.AddValue("_alreadyFetchedTenant", _alreadyFetchedTenant);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tenant":
					_alreadyFetchedTenant = true;
					this.Tenant = (TenantEntity)entity;
					break;
				case "TenantHistories":
					_alreadyFetchedTenantHistories = true;
					if(entity!=null)
					{
						this.TenantHistories.Add((TenantHistoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tenant":
					SetupSyncTenant(relatedEntity);
					break;
				case "TenantHistories":
					_tenantHistories.Add((TenantHistoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tenant":
					DesetupSyncTenant(false, true);
					break;
				case "TenantHistories":
					this.PerformRelatedEntityRemoval(_tenantHistories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tenant!=null)
			{
				toReturn.Add(_tenant);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_tenantHistories);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantPersonID)
		{
			return FetchUsingPK(tenantPersonID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantPersonID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tenantPersonID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantPersonID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tenantPersonID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tenantPersonID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tenantPersonID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TenantPersonID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TenantPersonRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TenantHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch)
		{
			return GetMultiTenantHistories(forceFetch, _tenantHistories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TenantHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTenantHistories(forceFetch, _tenantHistories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTenantHistories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TenantHistoryCollection GetMultiTenantHistories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTenantHistories || forceFetch || _alwaysFetchTenantHistories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tenantHistories);
				_tenantHistories.SuppressClearInGetMulti=!forceFetch;
				_tenantHistories.EntityFactoryToUse = entityFactoryToUse;
				_tenantHistories.GetMultiManyToOne(null, this, filter);
				_tenantHistories.SuppressClearInGetMulti=false;
				_alreadyFetchedTenantHistories = true;
			}
			return _tenantHistories;
		}

		/// <summary> Sets the collection parameters for the collection for 'TenantHistories'. These settings will be taken into account
		/// when the property TenantHistories is requested or GetMultiTenantHistories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTenantHistories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tenantHistories.SortClauses=sortClauses;
			_tenantHistories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TenantEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TenantEntity' which is related to this entity.</returns>
		public TenantEntity GetSingleTenant()
		{
			return GetSingleTenant(false);
		}

		/// <summary> Retrieves the related entity of type 'TenantEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TenantEntity' which is related to this entity.</returns>
		public virtual TenantEntity GetSingleTenant(bool forceFetch)
		{
			if( ( !_alreadyFetchedTenant || forceFetch || _alwaysFetchTenant) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TenantEntityUsingTenantID);
				TenantEntity newEntity = new TenantEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TenantID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TenantEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tenantReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tenant = newEntity;
				_alreadyFetchedTenant = fetchResult;
			}
			return _tenant;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tenant", _tenant);
			toReturn.Add("TenantHistories", _tenantHistories);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="validator">The validator object for this TenantPersonEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 tenantPersonID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tenantPersonID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_tenantHistories = new VarioSL.Entities.CollectionClasses.TenantHistoryCollection();
			_tenantHistories.SetContainingEntityInfo(this, "TenantPerson");
			_tenantReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CellPhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FaxNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Password", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TenantPersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tenant</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTenant(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tenant, new PropertyChangedEventHandler( OnTenantPropertyChanged ), "Tenant", VarioSL.Entities.RelationClasses.StaticTenantPersonRelations.TenantEntityUsingTenantIDStatic, true, signalRelatedEntity, "TenantPeople", resetFKFields, new int[] { (int)TenantPersonFieldIndex.TenantID } );		
			_tenant = null;
		}
		
		/// <summary> setups the sync logic for member _tenant</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTenant(IEntityCore relatedEntity)
		{
			if(_tenant!=relatedEntity)
			{		
				DesetupSyncTenant(true, true);
				_tenant = (TenantEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tenant, new PropertyChangedEventHandler( OnTenantPropertyChanged ), "Tenant", VarioSL.Entities.RelationClasses.StaticTenantPersonRelations.TenantEntityUsingTenantIDStatic, true, ref _alreadyFetchedTenant, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTenantPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tenantPersonID">PK value for TenantPerson which data should be fetched into this TenantPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 tenantPersonID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TenantPersonFieldIndex.TenantPersonID].ForcedCurrentValueWrite(tenantPersonID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTenantPersonDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TenantPersonEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TenantPersonRelations Relations
		{
			get	{ return new TenantPersonRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TenantHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenantHistories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantHistoryCollection(), (IEntityRelation)GetRelationsForField("TenantHistories")[0], (int)VarioSL.Entities.EntityType.TenantPersonEntity, (int)VarioSL.Entities.EntityType.TenantHistoryEntity, 0, null, null, null, "TenantHistories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tenant'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTenant
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TenantCollection(), (IEntityRelation)GetRelationsForField("Tenant")[0], (int)VarioSL.Entities.EntityType.TenantPersonEntity, (int)VarioSL.Entities.EntityType.TenantEntity, 0, null, null, null, "Tenant", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CellPhoneNumber property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."CELLPHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CellPhoneNumber
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.CellPhoneNumber, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.CellPhoneNumber, value, true); }
		}

		/// <summary> The Email property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.Email, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.Email, value, true); }
		}

		/// <summary> The FaxNumber property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."FAXNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FaxNumber
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.FaxNumber, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.FaxNumber, value, true); }
		}

		/// <summary> The FirstName property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."FIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.FirstName, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.FirstName, value, true); }
		}

		/// <summary> The LastModified property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)TenantPersonFieldIndex.LastModified, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastName property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."LASTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastName
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.LastName, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.LastName, value, true); }
		}

		/// <summary> The LastUser property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.LastUser, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Password property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."PASSWORD"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Password
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.Password, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.Password, value, true); }
		}

		/// <summary> The TenantID property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."TENANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TenantID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TenantPersonFieldIndex.TenantID, false); }
			set	{ SetValue((int)TenantPersonFieldIndex.TenantID, value, true); }
		}

		/// <summary> The TenantPersonID property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."TENANTPERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TenantPersonID
		{
			get { return (System.Int64)GetValue((int)TenantPersonFieldIndex.TenantPersonID, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.TenantPersonID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)TenantPersonFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserName property of the Entity TenantPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TENANTPERSON"."USERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String UserName
		{
			get { return (System.String)GetValue((int)TenantPersonFieldIndex.UserName, true); }
			set	{ SetValue((int)TenantPersonFieldIndex.UserName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TenantHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTenantHistories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TenantHistoryCollection TenantHistories
		{
			get	{ return GetMultiTenantHistories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TenantHistories. When set to true, TenantHistories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TenantHistories is accessed. You can always execute/ a forced fetch by calling GetMultiTenantHistories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenantHistories
		{
			get	{ return _alwaysFetchTenantHistories; }
			set	{ _alwaysFetchTenantHistories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TenantHistories already has been fetched. Setting this property to false when TenantHistories has been fetched
		/// will clear the TenantHistories collection well. Setting this property to true while TenantHistories hasn't been fetched disables lazy loading for TenantHistories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenantHistories
		{
			get { return _alreadyFetchedTenantHistories;}
			set 
			{
				if(_alreadyFetchedTenantHistories && !value && (_tenantHistories != null))
				{
					_tenantHistories.Clear();
				}
				_alreadyFetchedTenantHistories = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TenantEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTenant()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TenantEntity Tenant
		{
			get	{ return GetSingleTenant(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTenant(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TenantPeople", "Tenant", _tenant, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tenant. When set to true, Tenant is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tenant is accessed. You can always execute a forced fetch by calling GetSingleTenant(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTenant
		{
			get	{ return _alwaysFetchTenant; }
			set	{ _alwaysFetchTenant = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tenant already has been fetched. Setting this property to false when Tenant has been fetched
		/// will set Tenant to null as well. Setting this property to true while Tenant hasn't been fetched disables lazy loading for Tenant</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTenant
		{
			get { return _alreadyFetchedTenant;}
			set 
			{
				if(_alreadyFetchedTenant && !value)
				{
					this.Tenant = null;
				}
				_alreadyFetchedTenant = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tenant is not found
		/// in the database. When set to true, Tenant will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TenantReturnsNewIfNotFound
		{
			get	{ return _tenantReturnsNewIfNotFound; }
			set { _tenantReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TenantPersonEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
