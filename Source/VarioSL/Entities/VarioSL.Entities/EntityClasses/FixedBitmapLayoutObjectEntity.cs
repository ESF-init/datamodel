﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FixedBitmapLayoutObject'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FixedBitmapLayoutObjectEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private LayoutEntity _layout;
		private bool	_alwaysFetchLayout, _alreadyFetchedLayout, _layoutReturnsNewIfNotFound;
		private LogoEntity _logo;
		private bool	_alwaysFetchLogo, _alreadyFetchedLogo, _logoReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Layout</summary>
			public static readonly string Layout = "Layout";
			/// <summary>Member name Logo</summary>
			public static readonly string Logo = "Logo";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FixedBitmapLayoutObjectEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FixedBitmapLayoutObjectEntity() :base("FixedBitmapLayoutObjectEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		public FixedBitmapLayoutObjectEntity(System.Int64 layoutObjectID):base("FixedBitmapLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FixedBitmapLayoutObjectEntity(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse):base("FixedBitmapLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="validator">The custom validator object for this FixedBitmapLayoutObjectEntity</param>
		public FixedBitmapLayoutObjectEntity(System.Int64 layoutObjectID, IValidator validator):base("FixedBitmapLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FixedBitmapLayoutObjectEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_layout = (LayoutEntity)info.GetValue("_layout", typeof(LayoutEntity));
			if(_layout!=null)
			{
				_layout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_layoutReturnsNewIfNotFound = info.GetBoolean("_layoutReturnsNewIfNotFound");
			_alwaysFetchLayout = info.GetBoolean("_alwaysFetchLayout");
			_alreadyFetchedLayout = info.GetBoolean("_alreadyFetchedLayout");

			_logo = (LogoEntity)info.GetValue("_logo", typeof(LogoEntity));
			if(_logo!=null)
			{
				_logo.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_logoReturnsNewIfNotFound = info.GetBoolean("_logoReturnsNewIfNotFound");
			_alwaysFetchLogo = info.GetBoolean("_alwaysFetchLogo");
			_alreadyFetchedLogo = info.GetBoolean("_alreadyFetchedLogo");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FixedBitmapLayoutObjectFieldIndex)fieldIndex)
			{
				case FixedBitmapLayoutObjectFieldIndex.LayoutID:
					DesetupSyncLayout(true, false);
					_alreadyFetchedLayout = false;
					break;
				case FixedBitmapLayoutObjectFieldIndex.LogoID:
					DesetupSyncLogo(true, false);
					_alreadyFetchedLogo = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLayout = (_layout != null);
			_alreadyFetchedLogo = (_logo != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Layout":
					toReturn.Add(Relations.LayoutEntityUsingLayoutID);
					break;
				case "Logo":
					toReturn.Add(Relations.LogoEntityUsingLogoID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_layout", (!this.MarkedForDeletion?_layout:null));
			info.AddValue("_layoutReturnsNewIfNotFound", _layoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLayout", _alwaysFetchLayout);
			info.AddValue("_alreadyFetchedLayout", _alreadyFetchedLayout);
			info.AddValue("_logo", (!this.MarkedForDeletion?_logo:null));
			info.AddValue("_logoReturnsNewIfNotFound", _logoReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLogo", _alwaysFetchLogo);
			info.AddValue("_alreadyFetchedLogo", _alreadyFetchedLogo);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Layout":
					_alreadyFetchedLayout = true;
					this.Layout = (LayoutEntity)entity;
					break;
				case "Logo":
					_alreadyFetchedLogo = true;
					this.Logo = (LogoEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Layout":
					SetupSyncLayout(relatedEntity);
					break;
				case "Logo":
					SetupSyncLogo(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Layout":
					DesetupSyncLayout(false, true);
					break;
				case "Logo":
					DesetupSyncLogo(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_layout!=null)
			{
				toReturn.Add(_layout);
			}
			if(_logo!=null)
			{
				toReturn.Add(_logo);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID)
		{
			return FetchUsingPK(layoutObjectID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutObjectID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutObjectID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FixedBitmapLayoutObjectRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleLayout()
		{
			return GetSingleLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedLayout || forceFetch || _alwaysFetchLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_layoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Layout = newEntity;
				_alreadyFetchedLayout = fetchResult;
			}
			return _layout;
		}


		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public LogoEntity GetSingleLogo()
		{
			return GetSingleLogo(false);
		}

		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public virtual LogoEntity GetSingleLogo(bool forceFetch)
		{
			if( ( !_alreadyFetchedLogo || forceFetch || _alwaysFetchLogo) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LogoEntityUsingLogoID);
				LogoEntity newEntity = new LogoEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LogoID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LogoEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_logoReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Logo = newEntity;
				_alreadyFetchedLogo = fetchResult;
			}
			return _logo;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Layout", _layout);
			toReturn.Add("Logo", _logo);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="validator">The validator object for this FixedBitmapLayoutObjectEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutObjectID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutObjectID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_layoutReturnsNewIfNotFound = false;
			_logoReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientAdaptable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Column", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogoID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Row", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _layout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticFixedBitmapLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, signalRelatedEntity, "FixedBitmapLayoutObjects", resetFKFields, new int[] { (int)FixedBitmapLayoutObjectFieldIndex.LayoutID } );		
			_layout = null;
		}
		
		/// <summary> setups the sync logic for member _layout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLayout(IEntityCore relatedEntity)
		{
			if(_layout!=relatedEntity)
			{		
				DesetupSyncLayout(true, true);
				_layout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticFixedBitmapLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, ref _alreadyFetchedLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _logo</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLogo(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _logo, new PropertyChangedEventHandler( OnLogoPropertyChanged ), "Logo", VarioSL.Entities.RelationClasses.StaticFixedBitmapLayoutObjectRelations.LogoEntityUsingLogoIDStatic, true, signalRelatedEntity, "FixedBitmapLayoutObjects", resetFKFields, new int[] { (int)FixedBitmapLayoutObjectFieldIndex.LogoID } );		
			_logo = null;
		}
		
		/// <summary> setups the sync logic for member _logo</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLogo(IEntityCore relatedEntity)
		{
			if(_logo!=relatedEntity)
			{		
				DesetupSyncLogo(true, true);
				_logo = (LogoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _logo, new PropertyChangedEventHandler( OnLogoPropertyChanged ), "Logo", VarioSL.Entities.RelationClasses.StaticFixedBitmapLayoutObjectRelations.LogoEntityUsingLogoIDStatic, true, ref _alreadyFetchedLogo, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLogoPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutObjectID">PK value for FixedBitmapLayoutObject which data should be fetched into this FixedBitmapLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FixedBitmapLayoutObjectFieldIndex.LayoutObjectID].ForcedCurrentValueWrite(layoutObjectID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFixedBitmapLayoutObjectDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FixedBitmapLayoutObjectEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FixedBitmapLayoutObjectRelations Relations
		{
			get	{ return new FixedBitmapLayoutObjectRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layout")[0], (int)VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Logo'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLogo
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LogoCollection(), (IEntityRelation)GetRelationsForField("Logo")[0], (int)VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LogoEntity, 0, null, null, null, "Logo", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientAdaptable property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."CLIENTADAPTABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ClientAdaptable
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedBitmapLayoutObjectFieldIndex.ClientAdaptable, false); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.ClientAdaptable, value, true); }
		}

		/// <summary> The Column property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."COLUMN_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Column
		{
			get { return (System.Double)GetValue((int)FixedBitmapLayoutObjectFieldIndex.Column, true); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.Column, value, true); }
		}

		/// <summary> The LayoutID property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FixedBitmapLayoutObjectFieldIndex.LayoutID, false); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The LayoutObjectID property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."LAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutObjectID
		{
			get { return (System.Int64)GetValue((int)FixedBitmapLayoutObjectFieldIndex.LayoutObjectID, true); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.LayoutObjectID, value, true); }
		}

		/// <summary> The LogoID property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."LOGOID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LogoID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FixedBitmapLayoutObjectFieldIndex.LogoID, false); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.LogoID, value, true); }
		}

		/// <summary> The Number property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)FixedBitmapLayoutObjectFieldIndex.Number, false); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.Number, value, true); }
		}

		/// <summary> The Row property of the Entity FixedBitmapLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDBITMAPLAYOUTOBJECT"."ROW_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Row
		{
			get { return (System.Double)GetValue((int)FixedBitmapLayoutObjectFieldIndex.Row, true); }
			set	{ SetValue((int)FixedBitmapLayoutObjectFieldIndex.Row, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity Layout
		{
			get	{ return GetSingleLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FixedBitmapLayoutObjects", "Layout", _layout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Layout. When set to true, Layout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layout is accessed. You can always execute a forced fetch by calling GetSingleLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayout
		{
			get	{ return _alwaysFetchLayout; }
			set	{ _alwaysFetchLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layout already has been fetched. Setting this property to false when Layout has been fetched
		/// will set Layout to null as well. Setting this property to true while Layout hasn't been fetched disables lazy loading for Layout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayout
		{
			get { return _alreadyFetchedLayout;}
			set 
			{
				if(_alreadyFetchedLayout && !value)
				{
					this.Layout = null;
				}
				_alreadyFetchedLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Layout is not found
		/// in the database. When set to true, Layout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LayoutReturnsNewIfNotFound
		{
			get	{ return _layoutReturnsNewIfNotFound; }
			set { _layoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LogoEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLogo()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LogoEntity Logo
		{
			get	{ return GetSingleLogo(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLogo(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FixedBitmapLayoutObjects", "Logo", _logo, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Logo. When set to true, Logo is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Logo is accessed. You can always execute a forced fetch by calling GetSingleLogo(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLogo
		{
			get	{ return _alwaysFetchLogo; }
			set	{ _alwaysFetchLogo = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Logo already has been fetched. Setting this property to false when Logo has been fetched
		/// will set Logo to null as well. Setting this property to true while Logo hasn't been fetched disables lazy loading for Logo</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLogo
		{
			get { return _alreadyFetchedLogo;}
			set 
			{
				if(_alreadyFetchedLogo && !value)
				{
					this.Logo = null;
				}
				_alreadyFetchedLogo = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Logo is not found
		/// in the database. When set to true, Logo will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LogoReturnsNewIfNotFound
		{
			get	{ return _logoReturnsNewIfNotFound; }
			set { _logoReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
