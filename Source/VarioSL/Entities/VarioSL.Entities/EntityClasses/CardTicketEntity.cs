﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardTicket'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardTicketEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection	_cardTicketToTicket;
		private bool	_alwaysFetchCardTicketToTicket, _alreadyFetchedCardTicketToTicket;
		private VarioSL.Entities.CollectionClasses.TicketCollection _assignedPasses;
		private bool	_alwaysFetchAssignedPasses, _alreadyFetchedAssignedPasses;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;
		private CardPhysicalDetailEntity _cardPhysicalDetail;
		private bool	_alwaysFetchCardPhysicalDetail, _alreadyFetchedCardPhysicalDetail, _cardPhysicalDetailReturnsNewIfNotFound;
		private FormEntity _form;
		private bool	_alwaysFetchForm, _alreadyFetchedForm, _formReturnsNewIfNotFound;
		private TicketEntity _assignedCardTicket;
		private bool	_alwaysFetchAssignedCardTicket, _alreadyFetchedAssignedCardTicket, _assignedCardTicketReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name CardPhysicalDetail</summary>
			public static readonly string CardPhysicalDetail = "CardPhysicalDetail";
			/// <summary>Member name Form</summary>
			public static readonly string Form = "Form";
			/// <summary>Member name CardTicketToTicket</summary>
			public static readonly string CardTicketToTicket = "CardTicketToTicket";
			/// <summary>Member name AssignedPasses</summary>
			public static readonly string AssignedPasses = "AssignedPasses";
			/// <summary>Member name AssignedCardTicket</summary>
			public static readonly string AssignedCardTicket = "AssignedCardTicket";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardTicketEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardTicketEntity() :base("CardTicketEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		public CardTicketEntity(System.Int64 cardTicketID):base("CardTicketEntity")
		{
			InitClassFetch(cardTicketID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardTicketEntity(System.Int64 cardTicketID, IPrefetchPath prefetchPathToUse):base("CardTicketEntity")
		{
			InitClassFetch(cardTicketID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="validator">The custom validator object for this CardTicketEntity</param>
		public CardTicketEntity(System.Int64 cardTicketID, IValidator validator):base("CardTicketEntity")
		{
			InitClassFetch(cardTicketID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardTicketEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardTicketToTicket = (VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection)info.GetValue("_cardTicketToTicket", typeof(VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection));
			_alwaysFetchCardTicketToTicket = info.GetBoolean("_alwaysFetchCardTicketToTicket");
			_alreadyFetchedCardTicketToTicket = info.GetBoolean("_alreadyFetchedCardTicketToTicket");
			_assignedPasses = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_assignedPasses", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchAssignedPasses = info.GetBoolean("_alwaysFetchAssignedPasses");
			_alreadyFetchedAssignedPasses = info.GetBoolean("_alreadyFetchedAssignedPasses");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");

			_cardPhysicalDetail = (CardPhysicalDetailEntity)info.GetValue("_cardPhysicalDetail", typeof(CardPhysicalDetailEntity));
			if(_cardPhysicalDetail!=null)
			{
				_cardPhysicalDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardPhysicalDetailReturnsNewIfNotFound = info.GetBoolean("_cardPhysicalDetailReturnsNewIfNotFound");
			_alwaysFetchCardPhysicalDetail = info.GetBoolean("_alwaysFetchCardPhysicalDetail");
			_alreadyFetchedCardPhysicalDetail = info.GetBoolean("_alreadyFetchedCardPhysicalDetail");

			_form = (FormEntity)info.GetValue("_form", typeof(FormEntity));
			if(_form!=null)
			{
				_form.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_formReturnsNewIfNotFound = info.GetBoolean("_formReturnsNewIfNotFound");
			_alwaysFetchForm = info.GetBoolean("_alwaysFetchForm");
			_alreadyFetchedForm = info.GetBoolean("_alreadyFetchedForm");
			_assignedCardTicket = (TicketEntity)info.GetValue("_assignedCardTicket", typeof(TicketEntity));
			if(_assignedCardTicket!=null)
			{
				_assignedCardTicket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_assignedCardTicketReturnsNewIfNotFound = info.GetBoolean("_assignedCardTicketReturnsNewIfNotFound");
			_alwaysFetchAssignedCardTicket = info.GetBoolean("_alwaysFetchAssignedCardTicket");
			_alreadyFetchedAssignedCardTicket = info.GetBoolean("_alreadyFetchedAssignedCardTicket");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardTicketFieldIndex)fieldIndex)
			{
				case CardTicketFieldIndex.CardPhysicalDetailID:
					DesetupSyncCardPhysicalDetail(true, false);
					_alreadyFetchedCardPhysicalDetail = false;
					break;
				case CardTicketFieldIndex.FormID:
					DesetupSyncForm(true, false);
					_alreadyFetchedForm = false;
					break;
				case CardTicketFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case CardTicketFieldIndex.TicketID:
					DesetupSyncAssignedCardTicket(true, false);
					_alreadyFetchedAssignedCardTicket = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardTicketToTicket = (_cardTicketToTicket.Count > 0);
			_alreadyFetchedAssignedPasses = (_assignedPasses.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
			_alreadyFetchedCardPhysicalDetail = (_cardPhysicalDetail != null);
			_alreadyFetchedForm = (_form != null);
			_alreadyFetchedAssignedCardTicket = (_assignedCardTicket != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "CardPhysicalDetail":
					toReturn.Add(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
					break;
				case "Form":
					toReturn.Add(Relations.FormEntityUsingFormID);
					break;
				case "CardTicketToTicket":
					toReturn.Add(Relations.CardTicketToTicketEntityUsingCardTicketID);
					break;
				case "AssignedPasses":
					toReturn.Add(Relations.CardTicketToTicketEntityUsingCardTicketID, "CardTicketEntity__", "CardTicketToTicket_", JoinHint.None);
					toReturn.Add(CardTicketToTicketEntity.Relations.TicketEntityUsingTicketID, "CardTicketToTicket_", string.Empty, JoinHint.None);
					break;
				case "AssignedCardTicket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardTicketToTicket", (!this.MarkedForDeletion?_cardTicketToTicket:null));
			info.AddValue("_alwaysFetchCardTicketToTicket", _alwaysFetchCardTicketToTicket);
			info.AddValue("_alreadyFetchedCardTicketToTicket", _alreadyFetchedCardTicketToTicket);
			info.AddValue("_assignedPasses", (!this.MarkedForDeletion?_assignedPasses:null));
			info.AddValue("_alwaysFetchAssignedPasses", _alwaysFetchAssignedPasses);
			info.AddValue("_alreadyFetchedAssignedPasses", _alreadyFetchedAssignedPasses);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);
			info.AddValue("_cardPhysicalDetail", (!this.MarkedForDeletion?_cardPhysicalDetail:null));
			info.AddValue("_cardPhysicalDetailReturnsNewIfNotFound", _cardPhysicalDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardPhysicalDetail", _alwaysFetchCardPhysicalDetail);
			info.AddValue("_alreadyFetchedCardPhysicalDetail", _alreadyFetchedCardPhysicalDetail);
			info.AddValue("_form", (!this.MarkedForDeletion?_form:null));
			info.AddValue("_formReturnsNewIfNotFound", _formReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForm", _alwaysFetchForm);
			info.AddValue("_alreadyFetchedForm", _alreadyFetchedForm);

			info.AddValue("_assignedCardTicket", (!this.MarkedForDeletion?_assignedCardTicket:null));
			info.AddValue("_assignedCardTicketReturnsNewIfNotFound", _assignedCardTicketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAssignedCardTicket", _alwaysFetchAssignedCardTicket);
			info.AddValue("_alreadyFetchedAssignedCardTicket", _alreadyFetchedAssignedCardTicket);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "CardPhysicalDetail":
					_alreadyFetchedCardPhysicalDetail = true;
					this.CardPhysicalDetail = (CardPhysicalDetailEntity)entity;
					break;
				case "Form":
					_alreadyFetchedForm = true;
					this.Form = (FormEntity)entity;
					break;
				case "CardTicketToTicket":
					_alreadyFetchedCardTicketToTicket = true;
					if(entity!=null)
					{
						this.CardTicketToTicket.Add((CardTicketToTicketEntity)entity);
					}
					break;
				case "AssignedPasses":
					_alreadyFetchedAssignedPasses = true;
					if(entity!=null)
					{
						this.AssignedPasses.Add((TicketEntity)entity);
					}
					break;
				case "AssignedCardTicket":
					_alreadyFetchedAssignedCardTicket = true;
					this.AssignedCardTicket = (TicketEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "CardPhysicalDetail":
					SetupSyncCardPhysicalDetail(relatedEntity);
					break;
				case "Form":
					SetupSyncForm(relatedEntity);
					break;
				case "CardTicketToTicket":
					_cardTicketToTicket.Add((CardTicketToTicketEntity)relatedEntity);
					break;
				case "AssignedCardTicket":
					SetupSyncAssignedCardTicket(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "CardPhysicalDetail":
					DesetupSyncCardPhysicalDetail(false, true);
					break;
				case "Form":
					DesetupSyncForm(false, true);
					break;
				case "CardTicketToTicket":
					this.PerformRelatedEntityRemoval(_cardTicketToTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AssignedCardTicket":
					DesetupSyncAssignedCardTicket(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			if(_cardPhysicalDetail!=null)
			{
				toReturn.Add(_cardPhysicalDetail);
			}
			if(_form!=null)
			{
				toReturn.Add(_form);
			}
			if(_assignedCardTicket!=null)
			{
				toReturn.Add(_assignedCardTicket);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardTicketToTicket);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="ticketID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTicketID(System.Int64 ticketID)
		{
			return FetchUsingUCTicketID( ticketID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="ticketID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTicketID(System.Int64 ticketID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCTicketID( ticketID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="ticketID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTicketID(System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCTicketID( ticketID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="ticketID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCTicketID(System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CardTicketDAO)CreateDAOInstance()).FetchCardTicketUsingUCTicketID(this, this.Transaction, ticketID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardTicketID)
		{
			return FetchUsingPK(cardTicketID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardTicketID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardTicketID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardTicketID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardTicketID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardTicketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardTicketID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardTicketID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardTicketRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch)
		{
			return GetMultiCardTicketToTicket(forceFetch, _cardTicketToTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardTicketToTicket(forceFetch, _cardTicketToTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardTicketToTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection GetMultiCardTicketToTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardTicketToTicket || forceFetch || _alwaysFetchCardTicketToTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardTicketToTicket);
				_cardTicketToTicket.SuppressClearInGetMulti=!forceFetch;
				_cardTicketToTicket.EntityFactoryToUse = entityFactoryToUse;
				_cardTicketToTicket.GetMultiManyToOne(this, null, filter);
				_cardTicketToTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedCardTicketToTicket = true;
			}
			return _cardTicketToTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardTicketToTicket'. These settings will be taken into account
		/// when the property CardTicketToTicket is requested or GetMultiCardTicketToTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardTicketToTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardTicketToTicket.SortClauses=sortClauses;
			_cardTicketToTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiAssignedPasses(bool forceFetch)
		{
			return GetMultiAssignedPasses(forceFetch, _assignedPasses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiAssignedPasses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAssignedPasses || forceFetch || _alwaysFetchAssignedPasses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_assignedPasses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CardTicketFields.CardTicketID, ComparisonOperator.Equal, this.CardTicketID, "CardTicketEntity__"));
				_assignedPasses.SuppressClearInGetMulti=!forceFetch;
				_assignedPasses.EntityFactoryToUse = entityFactoryToUse;
				_assignedPasses.GetMulti(filter, GetRelationsForField("AssignedPasses"));
				_assignedPasses.SuppressClearInGetMulti=false;
				_alreadyFetchedAssignedPasses = true;
			}
			return _assignedPasses;
		}

		/// <summary> Sets the collection parameters for the collection for 'AssignedPasses'. These settings will be taken into account
		/// when the property AssignedPasses is requested or GetMultiAssignedPasses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAssignedPasses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_assignedPasses.SortClauses=sortClauses;
			_assignedPasses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public CardPhysicalDetailEntity GetSingleCardPhysicalDetail()
		{
			return GetSingleCardPhysicalDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public virtual CardPhysicalDetailEntity GetSingleCardPhysicalDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardPhysicalDetail || forceFetch || _alwaysFetchCardPhysicalDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
				CardPhysicalDetailEntity newEntity = new CardPhysicalDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardPhysicalDetailID);
				}
				if(fetchResult)
				{
					newEntity = (CardPhysicalDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardPhysicalDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardPhysicalDetail = newEntity;
				_alreadyFetchedCardPhysicalDetail = fetchResult;
			}
			return _cardPhysicalDetail;
		}


		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public FormEntity GetSingleForm()
		{
			return GetSingleForm(false);
		}

		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public virtual FormEntity GetSingleForm(bool forceFetch)
		{
			if( ( !_alreadyFetchedForm || forceFetch || _alwaysFetchForm) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FormEntityUsingFormID);
				FormEntity newEntity = new FormEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FormID);
				}
				if(fetchResult)
				{
					newEntity = (FormEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_formReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Form = newEntity;
				_alreadyFetchedForm = fetchResult;
			}
			return _form;
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleAssignedCardTicket()
		{
			return GetSingleAssignedCardTicket(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleAssignedCardTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedAssignedCardTicket || forceFetch || _alwaysFetchAssignedCardTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_assignedCardTicketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AssignedCardTicket = newEntity;
				_alreadyFetchedAssignedCardTicket = fetchResult;
			}
			return _assignedCardTicket;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("CardPhysicalDetail", _cardPhysicalDetail);
			toReturn.Add("Form", _form);
			toReturn.Add("CardTicketToTicket", _cardTicketToTicket);
			toReturn.Add("AssignedPasses", _assignedPasses);
			toReturn.Add("AssignedCardTicket", _assignedCardTicket);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="validator">The validator object for this CardTicketEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardTicketID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardTicketID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardTicketToTicket = new VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection();
			_cardTicketToTicket.SetContainingEntityInfo(this, "CardTicket");
			_assignedPasses = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tariffReturnsNewIfNotFound = false;
			_cardPhysicalDetailReturnsNewIfNotFound = false;
			_formReturnsNewIfNotFound = false;
			_assignedCardTicketReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTicketName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "CardTickets", resetFKFields, new int[] { (int)CardTicketFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardPhysicalDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, signalRelatedEntity, "CardTicket", resetFKFields, new int[] { (int)CardTicketFieldIndex.CardPhysicalDetailID } );		
			_cardPhysicalDetail = null;
		}
		
		/// <summary> setups the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardPhysicalDetail(IEntityCore relatedEntity)
		{
			if(_cardPhysicalDetail!=relatedEntity)
			{		
				DesetupSyncCardPhysicalDetail(true, true);
				_cardPhysicalDetail = (CardPhysicalDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, ref _alreadyFetchedCardPhysicalDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPhysicalDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _form</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForm(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.FormEntityUsingFormIDStatic, true, signalRelatedEntity, "CardTicket", resetFKFields, new int[] { (int)CardTicketFieldIndex.FormID } );		
			_form = null;
		}
		
		/// <summary> setups the sync logic for member _form</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForm(IEntityCore relatedEntity)
		{
			if(_form!=relatedEntity)
			{		
				DesetupSyncForm(true, true);
				_form = (FormEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.FormEntityUsingFormIDStatic, true, ref _alreadyFetchedForm, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _assignedCardTicket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAssignedCardTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _assignedCardTicket, new PropertyChangedEventHandler( OnAssignedCardTicketPropertyChanged ), "AssignedCardTicket", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "CardTicketAddition", resetFKFields, new int[] { (int)CardTicketFieldIndex.TicketID } );
			_assignedCardTicket = null;
		}
	
		/// <summary> setups the sync logic for member _assignedCardTicket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAssignedCardTicket(IEntityCore relatedEntity)
		{
			if(_assignedCardTicket!=relatedEntity)
			{
				DesetupSyncAssignedCardTicket(true, true);
				_assignedCardTicket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _assignedCardTicket, new PropertyChangedEventHandler( OnAssignedCardTicketPropertyChanged ), "AssignedCardTicket", VarioSL.Entities.RelationClasses.StaticCardTicketRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedAssignedCardTicket, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAssignedCardTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardTicketID">PK value for CardTicket which data should be fetched into this CardTicket object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardTicketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardTicketFieldIndex.CardTicketID].ForcedCurrentValueWrite(cardTicketID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardTicketDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardTicketEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardTicketRelations Relations
		{
			get	{ return new CardTicketRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicketToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTicketToTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection(), (IEntityRelation)GetRelationsForField("CardTicketToTicket")[0], (int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.CardTicketToTicketEntity, 0, null, null, null, "CardTicketToTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAssignedPasses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardTicketToTicketEntityUsingCardTicketID;
				intermediateRelation.SetAliases(string.Empty, "CardTicketToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("AssignedPasses"), "AssignedPasses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardPhysicalDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardPhysicalDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection(), (IEntityRelation)GetRelationsForField("CardPhysicalDetail")[0], (int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, 0, null, null, null, "CardPhysicalDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForm
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Form")[0], (int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Form", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAssignedCardTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("AssignedCardTicket")[0], (int)VarioSL.Entities.EntityType.CardTicketEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "AssignedCardTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardPhysicalDetailID property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."CARDPHYSICALDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardPhysicalDetailID
		{
			get { return (System.Int64)GetValue((int)CardTicketFieldIndex.CardPhysicalDetailID, true); }
			set	{ SetValue((int)CardTicketFieldIndex.CardPhysicalDetailID, value, true); }
		}

		/// <summary> The CardTicketID property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."CARDTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CardTicketID
		{
			get { return (System.Int64)GetValue((int)CardTicketFieldIndex.CardTicketID, true); }
			set	{ SetValue((int)CardTicketFieldIndex.CardTicketID, value, true); }
		}

		/// <summary> The CardTicketName property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."CARDTICKETNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CardTicketName
		{
			get { return (System.String)GetValue((int)CardTicketFieldIndex.CardTicketName, true); }
			set	{ SetValue((int)CardTicketFieldIndex.CardTicketName, value, true); }
		}

		/// <summary> The FormID property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FormID
		{
			get { return (System.Int64)GetValue((int)CardTicketFieldIndex.FormID, true); }
			set	{ SetValue((int)CardTicketFieldIndex.FormID, value, true); }
		}

		/// <summary> The TariffID property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)CardTicketFieldIndex.TariffID, true); }
			set	{ SetValue((int)CardTicketFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TicketID property of the Entity CardTicket<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_CARDTICKET"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)CardTicketFieldIndex.TicketID, true); }
			set	{ SetValue((int)CardTicketFieldIndex.TicketID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardTicketToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardTicketToTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketToTicketCollection CardTicketToTicket
		{
			get	{ return GetMultiCardTicketToTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardTicketToTicket. When set to true, CardTicketToTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTicketToTicket is accessed. You can always execute/ a forced fetch by calling GetMultiCardTicketToTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTicketToTicket
		{
			get	{ return _alwaysFetchCardTicketToTicket; }
			set	{ _alwaysFetchCardTicketToTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardTicketToTicket already has been fetched. Setting this property to false when CardTicketToTicket has been fetched
		/// will clear the CardTicketToTicket collection well. Setting this property to true while CardTicketToTicket hasn't been fetched disables lazy loading for CardTicketToTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTicketToTicket
		{
			get { return _alreadyFetchedCardTicketToTicket;}
			set 
			{
				if(_alreadyFetchedCardTicketToTicket && !value && (_cardTicketToTicket != null))
				{
					_cardTicketToTicket.Clear();
				}
				_alreadyFetchedCardTicketToTicket = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAssignedPasses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection AssignedPasses
		{
			get { return GetMultiAssignedPasses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AssignedPasses. When set to true, AssignedPasses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AssignedPasses is accessed. You can always execute a forced fetch by calling GetMultiAssignedPasses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAssignedPasses
		{
			get	{ return _alwaysFetchAssignedPasses; }
			set	{ _alwaysFetchAssignedPasses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AssignedPasses already has been fetched. Setting this property to false when AssignedPasses has been fetched
		/// will clear the AssignedPasses collection well. Setting this property to true while AssignedPasses hasn't been fetched disables lazy loading for AssignedPasses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAssignedPasses
		{
			get { return _alreadyFetchedAssignedPasses;}
			set 
			{
				if(_alreadyFetchedAssignedPasses && !value && (_assignedPasses != null))
				{
					_assignedPasses.Clear();
				}
				_alreadyFetchedAssignedPasses = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardTickets", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardPhysicalDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardPhysicalDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardPhysicalDetailEntity CardPhysicalDetail
		{
			get	{ return GetSingleCardPhysicalDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardPhysicalDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardTicket", "CardPhysicalDetail", _cardPhysicalDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardPhysicalDetail. When set to true, CardPhysicalDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardPhysicalDetail is accessed. You can always execute a forced fetch by calling GetSingleCardPhysicalDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardPhysicalDetail
		{
			get	{ return _alwaysFetchCardPhysicalDetail; }
			set	{ _alwaysFetchCardPhysicalDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardPhysicalDetail already has been fetched. Setting this property to false when CardPhysicalDetail has been fetched
		/// will set CardPhysicalDetail to null as well. Setting this property to true while CardPhysicalDetail hasn't been fetched disables lazy loading for CardPhysicalDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardPhysicalDetail
		{
			get { return _alreadyFetchedCardPhysicalDetail;}
			set 
			{
				if(_alreadyFetchedCardPhysicalDetail && !value)
				{
					this.CardPhysicalDetail = null;
				}
				_alreadyFetchedCardPhysicalDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardPhysicalDetail is not found
		/// in the database. When set to true, CardPhysicalDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardPhysicalDetailReturnsNewIfNotFound
		{
			get	{ return _cardPhysicalDetailReturnsNewIfNotFound; }
			set { _cardPhysicalDetailReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FormEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForm()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FormEntity Form
		{
			get	{ return GetSingleForm(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForm(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardTicket", "Form", _form, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Form. When set to true, Form is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Form is accessed. You can always execute a forced fetch by calling GetSingleForm(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForm
		{
			get	{ return _alwaysFetchForm; }
			set	{ _alwaysFetchForm = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Form already has been fetched. Setting this property to false when Form has been fetched
		/// will set Form to null as well. Setting this property to true while Form hasn't been fetched disables lazy loading for Form</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForm
		{
			get { return _alreadyFetchedForm;}
			set 
			{
				if(_alreadyFetchedForm && !value)
				{
					this.Form = null;
				}
				_alreadyFetchedForm = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Form is not found
		/// in the database. When set to true, Form will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FormReturnsNewIfNotFound
		{
			get	{ return _formReturnsNewIfNotFound; }
			set { _formReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAssignedCardTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity AssignedCardTicket
		{
			get	{ return GetSingleAssignedCardTicket(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAssignedCardTicket(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_assignedCardTicket !=null);
						DesetupSyncAssignedCardTicket(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("AssignedCardTicket");
						}
					}
					else
					{
						if(_assignedCardTicket!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CardTicketAddition");
							SetupSyncAssignedCardTicket(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AssignedCardTicket. When set to true, AssignedCardTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AssignedCardTicket is accessed. You can always execute a forced fetch by calling GetSingleAssignedCardTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAssignedCardTicket
		{
			get	{ return _alwaysFetchAssignedCardTicket; }
			set	{ _alwaysFetchAssignedCardTicket = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property AssignedCardTicket already has been fetched. Setting this property to false when AssignedCardTicket has been fetched
		/// will set AssignedCardTicket to null as well. Setting this property to true while AssignedCardTicket hasn't been fetched disables lazy loading for AssignedCardTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAssignedCardTicket
		{
			get { return _alreadyFetchedAssignedCardTicket;}
			set 
			{
				if(_alreadyFetchedAssignedCardTicket && !value)
				{
					this.AssignedCardTicket = null;
				}
				_alreadyFetchedAssignedCardTicket = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AssignedCardTicket is not found
		/// in the database. When set to true, AssignedCardTicket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AssignedCardTicketReturnsNewIfNotFound
		{
			get	{ return _assignedCardTicketReturnsNewIfNotFound; }
			set	{ _assignedCardTicketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardTicketEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
