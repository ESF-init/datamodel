﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ExternalPacketEffort'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ExternalPacketEffortEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ExternalEffortEntity _externalEffort;
		private bool	_alwaysFetchExternalEffort, _alreadyFetchedExternalEffort, _externalEffortReturnsNewIfNotFound;
		private ExternalPacketEntity _externalPacket;
		private bool	_alwaysFetchExternalPacket, _alreadyFetchedExternalPacket, _externalPacketReturnsNewIfNotFound;
		private FareStageEntity _fareStage;
		private bool	_alwaysFetchFareStage, _alreadyFetchedFareStage, _fareStageReturnsNewIfNotFound;
		private LineEntity _line;
		private bool	_alwaysFetchLine, _alreadyFetchedLine, _lineReturnsNewIfNotFound;
		private OperatorEntity _operator;
		private bool	_alwaysFetchOperator, _alreadyFetchedOperator, _operatorReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;
		private TypeOfCardEntity _typeOfCard;
		private bool	_alwaysFetchTypeOfCard, _alreadyFetchedTypeOfCard, _typeOfCardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ExternalEffort</summary>
			public static readonly string ExternalEffort = "ExternalEffort";
			/// <summary>Member name ExternalPacket</summary>
			public static readonly string ExternalPacket = "ExternalPacket";
			/// <summary>Member name FareStage</summary>
			public static readonly string FareStage = "FareStage";
			/// <summary>Member name Line</summary>
			public static readonly string Line = "Line";
			/// <summary>Member name Operator</summary>
			public static readonly string Operator = "Operator";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name TypeOfCard</summary>
			public static readonly string TypeOfCard = "TypeOfCard";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ExternalPacketEffortEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ExternalPacketEffortEntity() :base("ExternalPacketEffortEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		public ExternalPacketEffortEntity(System.Int64 packetEffortID):base("ExternalPacketEffortEntity")
		{
			InitClassFetch(packetEffortID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ExternalPacketEffortEntity(System.Int64 packetEffortID, IPrefetchPath prefetchPathToUse):base("ExternalPacketEffortEntity")
		{
			InitClassFetch(packetEffortID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="validator">The custom validator object for this ExternalPacketEffortEntity</param>
		public ExternalPacketEffortEntity(System.Int64 packetEffortID, IValidator validator):base("ExternalPacketEffortEntity")
		{
			InitClassFetch(packetEffortID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalPacketEffortEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_externalEffort = (ExternalEffortEntity)info.GetValue("_externalEffort", typeof(ExternalEffortEntity));
			if(_externalEffort!=null)
			{
				_externalEffort.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalEffortReturnsNewIfNotFound = info.GetBoolean("_externalEffortReturnsNewIfNotFound");
			_alwaysFetchExternalEffort = info.GetBoolean("_alwaysFetchExternalEffort");
			_alreadyFetchedExternalEffort = info.GetBoolean("_alreadyFetchedExternalEffort");

			_externalPacket = (ExternalPacketEntity)info.GetValue("_externalPacket", typeof(ExternalPacketEntity));
			if(_externalPacket!=null)
			{
				_externalPacket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_externalPacketReturnsNewIfNotFound = info.GetBoolean("_externalPacketReturnsNewIfNotFound");
			_alwaysFetchExternalPacket = info.GetBoolean("_alwaysFetchExternalPacket");
			_alreadyFetchedExternalPacket = info.GetBoolean("_alreadyFetchedExternalPacket");

			_fareStage = (FareStageEntity)info.GetValue("_fareStage", typeof(FareStageEntity));
			if(_fareStage!=null)
			{
				_fareStage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageReturnsNewIfNotFound = info.GetBoolean("_fareStageReturnsNewIfNotFound");
			_alwaysFetchFareStage = info.GetBoolean("_alwaysFetchFareStage");
			_alreadyFetchedFareStage = info.GetBoolean("_alreadyFetchedFareStage");

			_line = (LineEntity)info.GetValue("_line", typeof(LineEntity));
			if(_line!=null)
			{
				_line.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_lineReturnsNewIfNotFound = info.GetBoolean("_lineReturnsNewIfNotFound");
			_alwaysFetchLine = info.GetBoolean("_alwaysFetchLine");
			_alreadyFetchedLine = info.GetBoolean("_alreadyFetchedLine");

			_operator = (OperatorEntity)info.GetValue("_operator", typeof(OperatorEntity));
			if(_operator!=null)
			{
				_operator.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_operatorReturnsNewIfNotFound = info.GetBoolean("_operatorReturnsNewIfNotFound");
			_alwaysFetchOperator = info.GetBoolean("_alwaysFetchOperator");
			_alreadyFetchedOperator = info.GetBoolean("_alreadyFetchedOperator");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");

			_typeOfCard = (TypeOfCardEntity)info.GetValue("_typeOfCard", typeof(TypeOfCardEntity));
			if(_typeOfCard!=null)
			{
				_typeOfCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_typeOfCardReturnsNewIfNotFound = info.GetBoolean("_typeOfCardReturnsNewIfNotFound");
			_alwaysFetchTypeOfCard = info.GetBoolean("_alwaysFetchTypeOfCard");
			_alreadyFetchedTypeOfCard = info.GetBoolean("_alreadyFetchedTypeOfCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ExternalPacketEffortFieldIndex)fieldIndex)
			{
				case ExternalPacketEffortFieldIndex.CardTypeId:
					DesetupSyncTypeOfCard(true, false);
					_alreadyFetchedTypeOfCard = false;
					break;
				case ExternalPacketEffortFieldIndex.EffortID:
					DesetupSyncExternalEffort(true, false);
					_alreadyFetchedExternalEffort = false;
					break;
				case ExternalPacketEffortFieldIndex.FareStageID:
					DesetupSyncFareStage(true, false);
					_alreadyFetchedFareStage = false;
					break;
				case ExternalPacketEffortFieldIndex.LineID:
					DesetupSyncLine(true, false);
					_alreadyFetchedLine = false;
					break;
				case ExternalPacketEffortFieldIndex.OperatorID:
					DesetupSyncOperator(true, false);
					_alreadyFetchedOperator = false;
					break;
				case ExternalPacketEffortFieldIndex.PacketID:
					DesetupSyncExternalPacket(true, false);
					_alreadyFetchedExternalPacket = false;
					break;
				case ExternalPacketEffortFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedExternalEffort = (_externalEffort != null);
			_alreadyFetchedExternalPacket = (_externalPacket != null);
			_alreadyFetchedFareStage = (_fareStage != null);
			_alreadyFetchedLine = (_line != null);
			_alreadyFetchedOperator = (_operator != null);
			_alreadyFetchedTariff = (_tariff != null);
			_alreadyFetchedTypeOfCard = (_typeOfCard != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ExternalEffort":
					toReturn.Add(Relations.ExternalEffortEntityUsingEffortID);
					break;
				case "ExternalPacket":
					toReturn.Add(Relations.ExternalPacketEntityUsingPacketID);
					break;
				case "FareStage":
					toReturn.Add(Relations.FareStageEntityUsingFareStageID);
					break;
				case "Line":
					toReturn.Add(Relations.LineEntityUsingLineID);
					break;
				case "Operator":
					toReturn.Add(Relations.OperatorEntityUsingOperatorID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "TypeOfCard":
					toReturn.Add(Relations.TypeOfCardEntityUsingCardTypeId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_externalEffort", (!this.MarkedForDeletion?_externalEffort:null));
			info.AddValue("_externalEffortReturnsNewIfNotFound", _externalEffortReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalEffort", _alwaysFetchExternalEffort);
			info.AddValue("_alreadyFetchedExternalEffort", _alreadyFetchedExternalEffort);
			info.AddValue("_externalPacket", (!this.MarkedForDeletion?_externalPacket:null));
			info.AddValue("_externalPacketReturnsNewIfNotFound", _externalPacketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchExternalPacket", _alwaysFetchExternalPacket);
			info.AddValue("_alreadyFetchedExternalPacket", _alreadyFetchedExternalPacket);
			info.AddValue("_fareStage", (!this.MarkedForDeletion?_fareStage:null));
			info.AddValue("_fareStageReturnsNewIfNotFound", _fareStageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStage", _alwaysFetchFareStage);
			info.AddValue("_alreadyFetchedFareStage", _alreadyFetchedFareStage);
			info.AddValue("_line", (!this.MarkedForDeletion?_line:null));
			info.AddValue("_lineReturnsNewIfNotFound", _lineReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLine", _alwaysFetchLine);
			info.AddValue("_alreadyFetchedLine", _alreadyFetchedLine);
			info.AddValue("_operator", (!this.MarkedForDeletion?_operator:null));
			info.AddValue("_operatorReturnsNewIfNotFound", _operatorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOperator", _alwaysFetchOperator);
			info.AddValue("_alreadyFetchedOperator", _alreadyFetchedOperator);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);
			info.AddValue("_typeOfCard", (!this.MarkedForDeletion?_typeOfCard:null));
			info.AddValue("_typeOfCardReturnsNewIfNotFound", _typeOfCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTypeOfCard", _alwaysFetchTypeOfCard);
			info.AddValue("_alreadyFetchedTypeOfCard", _alreadyFetchedTypeOfCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ExternalEffort":
					_alreadyFetchedExternalEffort = true;
					this.ExternalEffort = (ExternalEffortEntity)entity;
					break;
				case "ExternalPacket":
					_alreadyFetchedExternalPacket = true;
					this.ExternalPacket = (ExternalPacketEntity)entity;
					break;
				case "FareStage":
					_alreadyFetchedFareStage = true;
					this.FareStage = (FareStageEntity)entity;
					break;
				case "Line":
					_alreadyFetchedLine = true;
					this.Line = (LineEntity)entity;
					break;
				case "Operator":
					_alreadyFetchedOperator = true;
					this.Operator = (OperatorEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "TypeOfCard":
					_alreadyFetchedTypeOfCard = true;
					this.TypeOfCard = (TypeOfCardEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ExternalEffort":
					SetupSyncExternalEffort(relatedEntity);
					break;
				case "ExternalPacket":
					SetupSyncExternalPacket(relatedEntity);
					break;
				case "FareStage":
					SetupSyncFareStage(relatedEntity);
					break;
				case "Line":
					SetupSyncLine(relatedEntity);
					break;
				case "Operator":
					SetupSyncOperator(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "TypeOfCard":
					SetupSyncTypeOfCard(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ExternalEffort":
					DesetupSyncExternalEffort(false, true);
					break;
				case "ExternalPacket":
					DesetupSyncExternalPacket(false, true);
					break;
				case "FareStage":
					DesetupSyncFareStage(false, true);
					break;
				case "Line":
					DesetupSyncLine(false, true);
					break;
				case "Operator":
					DesetupSyncOperator(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "TypeOfCard":
					DesetupSyncTypeOfCard(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_externalEffort!=null)
			{
				toReturn.Add(_externalEffort);
			}
			if(_externalPacket!=null)
			{
				toReturn.Add(_externalPacket);
			}
			if(_fareStage!=null)
			{
				toReturn.Add(_fareStage);
			}
			if(_line!=null)
			{
				toReturn.Add(_line);
			}
			if(_operator!=null)
			{
				toReturn.Add(_operator);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			if(_typeOfCard!=null)
			{
				toReturn.Add(_typeOfCard);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 packetEffortID)
		{
			return FetchUsingPK(packetEffortID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 packetEffortID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(packetEffortID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 packetEffortID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(packetEffortID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 packetEffortID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(packetEffortID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PacketEffortID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ExternalPacketEffortRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ExternalEffortEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalEffortEntity' which is related to this entity.</returns>
		public ExternalEffortEntity GetSingleExternalEffort()
		{
			return GetSingleExternalEffort(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalEffortEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalEffortEntity' which is related to this entity.</returns>
		public virtual ExternalEffortEntity GetSingleExternalEffort(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalEffort || forceFetch || _alwaysFetchExternalEffort) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalEffortEntityUsingEffortID);
				ExternalEffortEntity newEntity = new ExternalEffortEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EffortID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalEffortEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalEffortReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalEffort = newEntity;
				_alreadyFetchedExternalEffort = fetchResult;
			}
			return _externalEffort;
		}


		/// <summary> Retrieves the related entity of type 'ExternalPacketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ExternalPacketEntity' which is related to this entity.</returns>
		public ExternalPacketEntity GetSingleExternalPacket()
		{
			return GetSingleExternalPacket(false);
		}

		/// <summary> Retrieves the related entity of type 'ExternalPacketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ExternalPacketEntity' which is related to this entity.</returns>
		public virtual ExternalPacketEntity GetSingleExternalPacket(bool forceFetch)
		{
			if( ( !_alreadyFetchedExternalPacket || forceFetch || _alwaysFetchExternalPacket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ExternalPacketEntityUsingPacketID);
				ExternalPacketEntity newEntity = new ExternalPacketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PacketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ExternalPacketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_externalPacketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ExternalPacket = newEntity;
				_alreadyFetchedExternalPacket = fetchResult;
			}
			return _externalPacket;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStage()
		{
			return GetSingleFareStage(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStage(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStage || forceFetch || _alwaysFetchFareStage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingFareStageID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStage = newEntity;
				_alreadyFetchedFareStage = fetchResult;
			}
			return _fareStage;
		}


		/// <summary> Retrieves the related entity of type 'LineEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LineEntity' which is related to this entity.</returns>
		public LineEntity GetSingleLine()
		{
			return GetSingleLine(false);
		}

		/// <summary> Retrieves the related entity of type 'LineEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LineEntity' which is related to this entity.</returns>
		public virtual LineEntity GetSingleLine(bool forceFetch)
		{
			if( ( !_alreadyFetchedLine || forceFetch || _alwaysFetchLine) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LineEntityUsingLineID);
				LineEntity newEntity = new LineEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LineID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LineEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_lineReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Line = newEntity;
				_alreadyFetchedLine = fetchResult;
			}
			return _line;
		}


		/// <summary> Retrieves the related entity of type 'OperatorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OperatorEntity' which is related to this entity.</returns>
		public OperatorEntity GetSingleOperator()
		{
			return GetSingleOperator(false);
		}

		/// <summary> Retrieves the related entity of type 'OperatorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OperatorEntity' which is related to this entity.</returns>
		public virtual OperatorEntity GetSingleOperator(bool forceFetch)
		{
			if( ( !_alreadyFetchedOperator || forceFetch || _alwaysFetchOperator) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OperatorEntityUsingOperatorID);
				OperatorEntity newEntity = new OperatorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OperatorID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OperatorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_operatorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Operator = newEntity;
				_alreadyFetchedOperator = fetchResult;
			}
			return _operator;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary> Retrieves the related entity of type 'TypeOfCardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TypeOfCardEntity' which is related to this entity.</returns>
		public TypeOfCardEntity GetSingleTypeOfCard()
		{
			return GetSingleTypeOfCard(false);
		}

		/// <summary> Retrieves the related entity of type 'TypeOfCardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TypeOfCardEntity' which is related to this entity.</returns>
		public virtual TypeOfCardEntity GetSingleTypeOfCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedTypeOfCard || forceFetch || _alwaysFetchTypeOfCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TypeOfCardEntityUsingCardTypeId);
				TypeOfCardEntity newEntity = new TypeOfCardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardTypeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TypeOfCardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_typeOfCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TypeOfCard = newEntity;
				_alreadyFetchedTypeOfCard = fetchResult;
			}
			return _typeOfCard;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ExternalEffort", _externalEffort);
			toReturn.Add("ExternalPacket", _externalPacket);
			toReturn.Add("FareStage", _fareStage);
			toReturn.Add("Line", _line);
			toReturn.Add("Operator", _operator);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("TypeOfCard", _typeOfCard);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="validator">The validator object for this ExternalPacketEffortEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 packetEffortID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(packetEffortID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_externalEffortReturnsNewIfNotFound = false;
			_externalPacketReturnsNewIfNotFound = false;
			_fareStageReturnsNewIfNotFound = false;
			_lineReturnsNewIfNotFound = false;
			_operatorReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			_typeOfCardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EffortID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PacketEffortID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PacketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _externalEffort</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalEffort(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalEffort, new PropertyChangedEventHandler( OnExternalEffortPropertyChanged ), "ExternalEffort", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.ExternalEffortEntityUsingEffortIDStatic, true, signalRelatedEntity, "ExternalPacketEfforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.EffortID } );		
			_externalEffort = null;
		}
		
		/// <summary> setups the sync logic for member _externalEffort</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalEffort(IEntityCore relatedEntity)
		{
			if(_externalEffort!=relatedEntity)
			{		
				DesetupSyncExternalEffort(true, true);
				_externalEffort = (ExternalEffortEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalEffort, new PropertyChangedEventHandler( OnExternalEffortPropertyChanged ), "ExternalEffort", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.ExternalEffortEntityUsingEffortIDStatic, true, ref _alreadyFetchedExternalEffort, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalEffortPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _externalPacket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncExternalPacket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _externalPacket, new PropertyChangedEventHandler( OnExternalPacketPropertyChanged ), "ExternalPacket", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.ExternalPacketEntityUsingPacketIDStatic, true, signalRelatedEntity, "ExternalPacketEfforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.PacketID } );		
			_externalPacket = null;
		}
		
		/// <summary> setups the sync logic for member _externalPacket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncExternalPacket(IEntityCore relatedEntity)
		{
			if(_externalPacket!=relatedEntity)
			{		
				DesetupSyncExternalPacket(true, true);
				_externalPacket = (ExternalPacketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _externalPacket, new PropertyChangedEventHandler( OnExternalPacketPropertyChanged ), "ExternalPacket", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.ExternalPacketEntityUsingPacketIDStatic, true, ref _alreadyFetchedExternalPacket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnExternalPacketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.FareStageEntityUsingFareStageIDStatic, true, signalRelatedEntity, "ExternalPacketEfforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.FareStageID } );		
			_fareStage = null;
		}
		
		/// <summary> setups the sync logic for member _fareStage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStage(IEntityCore relatedEntity)
		{
			if(_fareStage!=relatedEntity)
			{		
				DesetupSyncFareStage(true, true);
				_fareStage = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.FareStageEntityUsingFareStageIDStatic, true, ref _alreadyFetchedFareStage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _line</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLine(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _line, new PropertyChangedEventHandler( OnLinePropertyChanged ), "Line", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.LineEntityUsingLineIDStatic, true, signalRelatedEntity, "ExternalPacketEfforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.LineID } );		
			_line = null;
		}
		
		/// <summary> setups the sync logic for member _line</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLine(IEntityCore relatedEntity)
		{
			if(_line!=relatedEntity)
			{		
				DesetupSyncLine(true, true);
				_line = (LineEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _line, new PropertyChangedEventHandler( OnLinePropertyChanged ), "Line", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.LineEntityUsingLineIDStatic, true, ref _alreadyFetchedLine, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLinePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _operator</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOperator(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _operator, new PropertyChangedEventHandler( OnOperatorPropertyChanged ), "Operator", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.OperatorEntityUsingOperatorIDStatic, true, signalRelatedEntity, "ExternalPacketEfforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.OperatorID } );		
			_operator = null;
		}
		
		/// <summary> setups the sync logic for member _operator</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOperator(IEntityCore relatedEntity)
		{
			if(_operator!=relatedEntity)
			{		
				DesetupSyncOperator(true, true);
				_operator = (OperatorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _operator, new PropertyChangedEventHandler( OnOperatorPropertyChanged ), "Operator", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.OperatorEntityUsingOperatorIDStatic, true, ref _alreadyFetchedOperator, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOperatorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "ExternalPacketefforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _typeOfCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTypeOfCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _typeOfCard, new PropertyChangedEventHandler( OnTypeOfCardPropertyChanged ), "TypeOfCard", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.TypeOfCardEntityUsingCardTypeIdStatic, true, signalRelatedEntity, "ExternalPacketefforts", resetFKFields, new int[] { (int)ExternalPacketEffortFieldIndex.CardTypeId } );		
			_typeOfCard = null;
		}
		
		/// <summary> setups the sync logic for member _typeOfCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTypeOfCard(IEntityCore relatedEntity)
		{
			if(_typeOfCard!=relatedEntity)
			{		
				DesetupSyncTypeOfCard(true, true);
				_typeOfCard = (TypeOfCardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _typeOfCard, new PropertyChangedEventHandler( OnTypeOfCardPropertyChanged ), "TypeOfCard", VarioSL.Entities.RelationClasses.StaticExternalPacketEffortRelations.TypeOfCardEntityUsingCardTypeIdStatic, true, ref _alreadyFetchedTypeOfCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTypeOfCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="packetEffortID">PK value for ExternalPacketEffort which data should be fetched into this ExternalPacketEffort object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 packetEffortID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ExternalPacketEffortFieldIndex.PacketEffortID].ForcedCurrentValueWrite(packetEffortID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateExternalPacketEffortDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ExternalPacketEffortEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ExternalPacketEffortRelations Relations
		{
			get	{ return new ExternalPacketEffortRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalEffort'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalEffort
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalEffort")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.ExternalEffortEntity, 0, null, null, null, "ExternalEffort", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketCollection(), (IEntityRelation)GetRelationsForField("ExternalPacket")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEntity, 0, null, null, null, "ExternalPacket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStage")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Line'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLine
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineCollection(), (IEntityRelation)GetRelationsForField("Line")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.LineEntity, 0, null, null, null, "Line", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Operator'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOperator
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OperatorCollection(), (IEntityRelation)GetRelationsForField("Operator")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.OperatorEntity, 0, null, null, null, "Operator", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TypeOfCard'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTypeOfCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TypeOfCardCollection(), (IEntityRelation)GetRelationsForField("TypeOfCard")[0], (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, (int)VarioSL.Entities.EntityType.TypeOfCardEntity, 0, null, null, null, "TypeOfCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardTypeId property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."CARDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardTypeId
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.CardTypeId, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.CardTypeId, value, true); }
		}

		/// <summary> The EffortID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."EFFORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EffortID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.EffortID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.EffortID, value, true); }
		}

		/// <summary> The FareStageID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."FARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.FareStageID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.FareStageID, value, true); }
		}

		/// <summary> The IsDefault property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."ISDEFAULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDefault
		{
			get { return (Nullable<System.Int16>)GetValue((int)ExternalPacketEffortFieldIndex.IsDefault, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.IsDefault, value, true); }
		}

		/// <summary> The LineID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."LINEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.LineID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.LineID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OperatorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.OperatorID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The PacketEffortID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."PACKETEFFORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PacketEffortID
		{
			get { return (System.Int64)GetValue((int)ExternalPacketEffortFieldIndex.PacketEffortID, true); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.PacketEffortID, value, true); }
		}

		/// <summary> The PacketID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."PACKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PacketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.PacketID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.PacketID, value, true); }
		}

		/// <summary> The ServiceID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."SERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.ServiceID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.ServiceID, value, true); }
		}

		/// <summary> The TariffID property of the Entity ExternalPacketEffort<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_EXTERNAL_PACKETEFFORT"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ExternalPacketEffortFieldIndex.TariffID, false); }
			set	{ SetValue((int)ExternalPacketEffortFieldIndex.TariffID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ExternalEffortEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalEffort()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalEffortEntity ExternalEffort
		{
			get	{ return GetSingleExternalEffort(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalEffort(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketEfforts", "ExternalEffort", _externalEffort, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalEffort. When set to true, ExternalEffort is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalEffort is accessed. You can always execute a forced fetch by calling GetSingleExternalEffort(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalEffort
		{
			get	{ return _alwaysFetchExternalEffort; }
			set	{ _alwaysFetchExternalEffort = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalEffort already has been fetched. Setting this property to false when ExternalEffort has been fetched
		/// will set ExternalEffort to null as well. Setting this property to true while ExternalEffort hasn't been fetched disables lazy loading for ExternalEffort</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalEffort
		{
			get { return _alreadyFetchedExternalEffort;}
			set 
			{
				if(_alreadyFetchedExternalEffort && !value)
				{
					this.ExternalEffort = null;
				}
				_alreadyFetchedExternalEffort = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalEffort is not found
		/// in the database. When set to true, ExternalEffort will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalEffortReturnsNewIfNotFound
		{
			get	{ return _externalEffortReturnsNewIfNotFound; }
			set { _externalEffortReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ExternalPacketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleExternalPacket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ExternalPacketEntity ExternalPacket
		{
			get	{ return GetSingleExternalPacket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncExternalPacket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketEfforts", "ExternalPacket", _externalPacket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacket. When set to true, ExternalPacket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacket is accessed. You can always execute a forced fetch by calling GetSingleExternalPacket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacket
		{
			get	{ return _alwaysFetchExternalPacket; }
			set	{ _alwaysFetchExternalPacket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacket already has been fetched. Setting this property to false when ExternalPacket has been fetched
		/// will set ExternalPacket to null as well. Setting this property to true while ExternalPacket hasn't been fetched disables lazy loading for ExternalPacket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacket
		{
			get { return _alreadyFetchedExternalPacket;}
			set 
			{
				if(_alreadyFetchedExternalPacket && !value)
				{
					this.ExternalPacket = null;
				}
				_alreadyFetchedExternalPacket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ExternalPacket is not found
		/// in the database. When set to true, ExternalPacket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ExternalPacketReturnsNewIfNotFound
		{
			get	{ return _externalPacketReturnsNewIfNotFound; }
			set { _externalPacketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStage
		{
			get	{ return GetSingleFareStage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketEfforts", "FareStage", _fareStage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStage. When set to true, FareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStage is accessed. You can always execute a forced fetch by calling GetSingleFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStage
		{
			get	{ return _alwaysFetchFareStage; }
			set	{ _alwaysFetchFareStage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStage already has been fetched. Setting this property to false when FareStage has been fetched
		/// will set FareStage to null as well. Setting this property to true while FareStage hasn't been fetched disables lazy loading for FareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStage
		{
			get { return _alreadyFetchedFareStage;}
			set 
			{
				if(_alreadyFetchedFareStage && !value)
				{
					this.FareStage = null;
				}
				_alreadyFetchedFareStage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStage is not found
		/// in the database. When set to true, FareStage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageReturnsNewIfNotFound
		{
			get	{ return _fareStageReturnsNewIfNotFound; }
			set { _fareStageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LineEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLine()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LineEntity Line
		{
			get	{ return GetSingleLine(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLine(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketEfforts", "Line", _line, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Line. When set to true, Line is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Line is accessed. You can always execute a forced fetch by calling GetSingleLine(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLine
		{
			get	{ return _alwaysFetchLine; }
			set	{ _alwaysFetchLine = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Line already has been fetched. Setting this property to false when Line has been fetched
		/// will set Line to null as well. Setting this property to true while Line hasn't been fetched disables lazy loading for Line</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLine
		{
			get { return _alreadyFetchedLine;}
			set 
			{
				if(_alreadyFetchedLine && !value)
				{
					this.Line = null;
				}
				_alreadyFetchedLine = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Line is not found
		/// in the database. When set to true, Line will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LineReturnsNewIfNotFound
		{
			get	{ return _lineReturnsNewIfNotFound; }
			set { _lineReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OperatorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOperator()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OperatorEntity Operator
		{
			get	{ return GetSingleOperator(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOperator(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketEfforts", "Operator", _operator, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Operator. When set to true, Operator is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Operator is accessed. You can always execute a forced fetch by calling GetSingleOperator(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOperator
		{
			get	{ return _alwaysFetchOperator; }
			set	{ _alwaysFetchOperator = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Operator already has been fetched. Setting this property to false when Operator has been fetched
		/// will set Operator to null as well. Setting this property to true while Operator hasn't been fetched disables lazy loading for Operator</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOperator
		{
			get { return _alreadyFetchedOperator;}
			set 
			{
				if(_alreadyFetchedOperator && !value)
				{
					this.Operator = null;
				}
				_alreadyFetchedOperator = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Operator is not found
		/// in the database. When set to true, Operator will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OperatorReturnsNewIfNotFound
		{
			get	{ return _operatorReturnsNewIfNotFound; }
			set { _operatorReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketefforts", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TypeOfCardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTypeOfCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TypeOfCardEntity TypeOfCard
		{
			get	{ return GetSingleTypeOfCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTypeOfCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ExternalPacketefforts", "TypeOfCard", _typeOfCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TypeOfCard. When set to true, TypeOfCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TypeOfCard is accessed. You can always execute a forced fetch by calling GetSingleTypeOfCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTypeOfCard
		{
			get	{ return _alwaysFetchTypeOfCard; }
			set	{ _alwaysFetchTypeOfCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TypeOfCard already has been fetched. Setting this property to false when TypeOfCard has been fetched
		/// will set TypeOfCard to null as well. Setting this property to true while TypeOfCard hasn't been fetched disables lazy loading for TypeOfCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTypeOfCard
		{
			get { return _alreadyFetchedTypeOfCard;}
			set 
			{
				if(_alreadyFetchedTypeOfCard && !value)
				{
					this.TypeOfCard = null;
				}
				_alreadyFetchedTypeOfCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TypeOfCard is not found
		/// in the database. When set to true, TypeOfCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TypeOfCardReturnsNewIfNotFound
		{
			get	{ return _typeOfCardReturnsNewIfNotFound; }
			set { _typeOfCardReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
