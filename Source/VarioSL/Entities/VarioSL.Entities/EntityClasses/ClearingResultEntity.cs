﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ClearingResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ClearingResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ClientEntity _cardIssuer;
		private bool	_alwaysFetchCardIssuer, _alreadyFetchedCardIssuer, _cardIssuerReturnsNewIfNotFound;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private ClientEntity _clientFrom;
		private bool	_alwaysFetchClientFrom, _alreadyFetchedClientFrom, _clientFromReturnsNewIfNotFound;
		private ClientEntity _clientTo;
		private bool	_alwaysFetchClientTo, _alreadyFetchedClientTo, _clientToReturnsNewIfNotFound;
		private DebtorEntity _debtor;
		private bool	_alwaysFetchDebtor, _alreadyFetchedDebtor, _debtorReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private DevicePaymentMethodEntity _devicePaymentMethod;
		private bool	_alwaysFetchDevicePaymentMethod, _alreadyFetchedDevicePaymentMethod, _devicePaymentMethodReturnsNewIfNotFound;
		private RevenueTransactionTypeEntity _revenueTransactionType;
		private bool	_alwaysFetchRevenueTransactionType, _alreadyFetchedRevenueTransactionType, _revenueTransactionTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardIssuer</summary>
			public static readonly string CardIssuer = "CardIssuer";
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ClientFrom</summary>
			public static readonly string ClientFrom = "ClientFrom";
			/// <summary>Member name ClientTo</summary>
			public static readonly string ClientTo = "ClientTo";
			/// <summary>Member name Debtor</summary>
			public static readonly string Debtor = "Debtor";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name DevicePaymentMethod</summary>
			public static readonly string DevicePaymentMethod = "DevicePaymentMethod";
			/// <summary>Member name RevenueTransactionType</summary>
			public static readonly string RevenueTransactionType = "RevenueTransactionType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClearingResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ClearingResultEntity() :base("ClearingResultEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		public ClearingResultEntity(System.Int64 clearingResultID):base("ClearingResultEntity")
		{
			InitClassFetch(clearingResultID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ClearingResultEntity(System.Int64 clearingResultID, IPrefetchPath prefetchPathToUse):base("ClearingResultEntity")
		{
			InitClassFetch(clearingResultID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="validator">The custom validator object for this ClearingResultEntity</param>
		public ClearingResultEntity(System.Int64 clearingResultID, IValidator validator):base("ClearingResultEntity")
		{
			InitClassFetch(clearingResultID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClearingResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardIssuer = (ClientEntity)info.GetValue("_cardIssuer", typeof(ClientEntity));
			if(_cardIssuer!=null)
			{
				_cardIssuer.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardIssuerReturnsNewIfNotFound = info.GetBoolean("_cardIssuerReturnsNewIfNotFound");
			_alwaysFetchCardIssuer = info.GetBoolean("_alwaysFetchCardIssuer");
			_alreadyFetchedCardIssuer = info.GetBoolean("_alreadyFetchedCardIssuer");

			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_clientFrom = (ClientEntity)info.GetValue("_clientFrom", typeof(ClientEntity));
			if(_clientFrom!=null)
			{
				_clientFrom.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientFromReturnsNewIfNotFound = info.GetBoolean("_clientFromReturnsNewIfNotFound");
			_alwaysFetchClientFrom = info.GetBoolean("_alwaysFetchClientFrom");
			_alreadyFetchedClientFrom = info.GetBoolean("_alreadyFetchedClientFrom");

			_clientTo = (ClientEntity)info.GetValue("_clientTo", typeof(ClientEntity));
			if(_clientTo!=null)
			{
				_clientTo.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientToReturnsNewIfNotFound = info.GetBoolean("_clientToReturnsNewIfNotFound");
			_alwaysFetchClientTo = info.GetBoolean("_alwaysFetchClientTo");
			_alreadyFetchedClientTo = info.GetBoolean("_alreadyFetchedClientTo");

			_debtor = (DebtorEntity)info.GetValue("_debtor", typeof(DebtorEntity));
			if(_debtor!=null)
			{
				_debtor.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_debtorReturnsNewIfNotFound = info.GetBoolean("_debtorReturnsNewIfNotFound");
			_alwaysFetchDebtor = info.GetBoolean("_alwaysFetchDebtor");
			_alreadyFetchedDebtor = info.GetBoolean("_alreadyFetchedDebtor");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_devicePaymentMethod = (DevicePaymentMethodEntity)info.GetValue("_devicePaymentMethod", typeof(DevicePaymentMethodEntity));
			if(_devicePaymentMethod!=null)
			{
				_devicePaymentMethod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_devicePaymentMethodReturnsNewIfNotFound = info.GetBoolean("_devicePaymentMethodReturnsNewIfNotFound");
			_alwaysFetchDevicePaymentMethod = info.GetBoolean("_alwaysFetchDevicePaymentMethod");
			_alreadyFetchedDevicePaymentMethod = info.GetBoolean("_alreadyFetchedDevicePaymentMethod");

			_revenueTransactionType = (RevenueTransactionTypeEntity)info.GetValue("_revenueTransactionType", typeof(RevenueTransactionTypeEntity));
			if(_revenueTransactionType!=null)
			{
				_revenueTransactionType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueTransactionTypeReturnsNewIfNotFound = info.GetBoolean("_revenueTransactionTypeReturnsNewIfNotFound");
			_alwaysFetchRevenueTransactionType = info.GetBoolean("_alwaysFetchRevenueTransactionType");
			_alreadyFetchedRevenueTransactionType = info.GetBoolean("_alreadyFetchedRevenueTransactionType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClearingResultFieldIndex)fieldIndex)
			{
				case ClearingResultFieldIndex.CardIssuerID:
					DesetupSyncCardIssuer(true, false);
					_alreadyFetchedCardIssuer = false;
					break;
				case ClearingResultFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ClearingResultFieldIndex.DebtorID:
					DesetupSyncDebtor(true, false);
					_alreadyFetchedDebtor = false;
					break;
				case ClearingResultFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case ClearingResultFieldIndex.DevicePaymentMethodID:
					DesetupSyncDevicePaymentMethod(true, false);
					_alreadyFetchedDevicePaymentMethod = false;
					break;
				case ClearingResultFieldIndex.FromClient:
					DesetupSyncClientFrom(true, false);
					_alreadyFetchedClientFrom = false;
					break;
				case ClearingResultFieldIndex.ToClient:
					DesetupSyncClientTo(true, false);
					_alreadyFetchedClientTo = false;
					break;
				case ClearingResultFieldIndex.TransactionTypeID:
					DesetupSyncRevenueTransactionType(true, false);
					_alreadyFetchedRevenueTransactionType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardIssuer = (_cardIssuer != null);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedClientFrom = (_clientFrom != null);
			_alreadyFetchedClientTo = (_clientTo != null);
			_alreadyFetchedDebtor = (_debtor != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedDevicePaymentMethod = (_devicePaymentMethod != null);
			_alreadyFetchedRevenueTransactionType = (_revenueTransactionType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardIssuer":
					toReturn.Add(Relations.ClientEntityUsingCardIssuerID);
					break;
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "ClientFrom":
					toReturn.Add(Relations.ClientEntityUsingFromClient);
					break;
				case "ClientTo":
					toReturn.Add(Relations.ClientEntityUsingToClient);
					break;
				case "Debtor":
					toReturn.Add(Relations.DebtorEntityUsingDebtorID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "DevicePaymentMethod":
					toReturn.Add(Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
					break;
				case "RevenueTransactionType":
					toReturn.Add(Relations.RevenueTransactionTypeEntityUsingTransactionTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardIssuer", (!this.MarkedForDeletion?_cardIssuer:null));
			info.AddValue("_cardIssuerReturnsNewIfNotFound", _cardIssuerReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardIssuer", _alwaysFetchCardIssuer);
			info.AddValue("_alreadyFetchedCardIssuer", _alreadyFetchedCardIssuer);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_clientFrom", (!this.MarkedForDeletion?_clientFrom:null));
			info.AddValue("_clientFromReturnsNewIfNotFound", _clientFromReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientFrom", _alwaysFetchClientFrom);
			info.AddValue("_alreadyFetchedClientFrom", _alreadyFetchedClientFrom);
			info.AddValue("_clientTo", (!this.MarkedForDeletion?_clientTo:null));
			info.AddValue("_clientToReturnsNewIfNotFound", _clientToReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientTo", _alwaysFetchClientTo);
			info.AddValue("_alreadyFetchedClientTo", _alreadyFetchedClientTo);
			info.AddValue("_debtor", (!this.MarkedForDeletion?_debtor:null));
			info.AddValue("_debtorReturnsNewIfNotFound", _debtorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDebtor", _alwaysFetchDebtor);
			info.AddValue("_alreadyFetchedDebtor", _alreadyFetchedDebtor);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_devicePaymentMethod", (!this.MarkedForDeletion?_devicePaymentMethod:null));
			info.AddValue("_devicePaymentMethodReturnsNewIfNotFound", _devicePaymentMethodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDevicePaymentMethod", _alwaysFetchDevicePaymentMethod);
			info.AddValue("_alreadyFetchedDevicePaymentMethod", _alreadyFetchedDevicePaymentMethod);
			info.AddValue("_revenueTransactionType", (!this.MarkedForDeletion?_revenueTransactionType:null));
			info.AddValue("_revenueTransactionTypeReturnsNewIfNotFound", _revenueTransactionTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueTransactionType", _alwaysFetchRevenueTransactionType);
			info.AddValue("_alreadyFetchedRevenueTransactionType", _alreadyFetchedRevenueTransactionType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardIssuer":
					_alreadyFetchedCardIssuer = true;
					this.CardIssuer = (ClientEntity)entity;
					break;
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "ClientFrom":
					_alreadyFetchedClientFrom = true;
					this.ClientFrom = (ClientEntity)entity;
					break;
				case "ClientTo":
					_alreadyFetchedClientTo = true;
					this.ClientTo = (ClientEntity)entity;
					break;
				case "Debtor":
					_alreadyFetchedDebtor = true;
					this.Debtor = (DebtorEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "DevicePaymentMethod":
					_alreadyFetchedDevicePaymentMethod = true;
					this.DevicePaymentMethod = (DevicePaymentMethodEntity)entity;
					break;
				case "RevenueTransactionType":
					_alreadyFetchedRevenueTransactionType = true;
					this.RevenueTransactionType = (RevenueTransactionTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardIssuer":
					SetupSyncCardIssuer(relatedEntity);
					break;
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "ClientFrom":
					SetupSyncClientFrom(relatedEntity);
					break;
				case "ClientTo":
					SetupSyncClientTo(relatedEntity);
					break;
				case "Debtor":
					SetupSyncDebtor(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "DevicePaymentMethod":
					SetupSyncDevicePaymentMethod(relatedEntity);
					break;
				case "RevenueTransactionType":
					SetupSyncRevenueTransactionType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardIssuer":
					DesetupSyncCardIssuer(false, true);
					break;
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "ClientFrom":
					DesetupSyncClientFrom(false, true);
					break;
				case "ClientTo":
					DesetupSyncClientTo(false, true);
					break;
				case "Debtor":
					DesetupSyncDebtor(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "DevicePaymentMethod":
					DesetupSyncDevicePaymentMethod(false, true);
					break;
				case "RevenueTransactionType":
					DesetupSyncRevenueTransactionType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardIssuer!=null)
			{
				toReturn.Add(_cardIssuer);
			}
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_clientFrom!=null)
			{
				toReturn.Add(_clientFrom);
			}
			if(_clientTo!=null)
			{
				toReturn.Add(_clientTo);
			}
			if(_debtor!=null)
			{
				toReturn.Add(_debtor);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_devicePaymentMethod!=null)
			{
				toReturn.Add(_devicePaymentMethod);
			}
			if(_revenueTransactionType!=null)
			{
				toReturn.Add(_revenueTransactionType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clearingResultID)
		{
			return FetchUsingPK(clearingResultID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clearingResultID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(clearingResultID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clearingResultID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(clearingResultID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 clearingResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(clearingResultID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ClearingResultID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ClearingResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleCardIssuer()
		{
			return GetSingleCardIssuer(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleCardIssuer(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardIssuer || forceFetch || _alwaysFetchCardIssuer) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingCardIssuerID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardIssuerID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardIssuerReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardIssuer = newEntity;
				_alreadyFetchedCardIssuer = fetchResult;
			}
			return _cardIssuer;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientFrom()
		{
			return GetSingleClientFrom(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientFrom(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientFrom || forceFetch || _alwaysFetchClientFrom) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingFromClient);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FromClient.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientFromReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientFrom = newEntity;
				_alreadyFetchedClientFrom = fetchResult;
			}
			return _clientFrom;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientTo()
		{
			return GetSingleClientTo(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientTo(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientTo || forceFetch || _alwaysFetchClientTo) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingToClient);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ToClient.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientToReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientTo = newEntity;
				_alreadyFetchedClientTo = fetchResult;
			}
			return _clientTo;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleDebtor()
		{
			return GetSingleDebtor(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleDebtor(bool forceFetch)
		{
			if( ( !_alreadyFetchedDebtor || forceFetch || _alwaysFetchDebtor) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingDebtorID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DebtorID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_debtorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Debtor = newEntity;
				_alreadyFetchedDebtor = fetchResult;
			}
			return _debtor;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'DevicePaymentMethodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DevicePaymentMethodEntity' which is related to this entity.</returns>
		public DevicePaymentMethodEntity GetSingleDevicePaymentMethod()
		{
			return GetSingleDevicePaymentMethod(false);
		}

		/// <summary> Retrieves the related entity of type 'DevicePaymentMethodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DevicePaymentMethodEntity' which is related to this entity.</returns>
		public virtual DevicePaymentMethodEntity GetSingleDevicePaymentMethod(bool forceFetch)
		{
			if( ( !_alreadyFetchedDevicePaymentMethod || forceFetch || _alwaysFetchDevicePaymentMethod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
				DevicePaymentMethodEntity newEntity = new DevicePaymentMethodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DevicePaymentMethodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DevicePaymentMethodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_devicePaymentMethodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DevicePaymentMethod = newEntity;
				_alreadyFetchedDevicePaymentMethod = fetchResult;
			}
			return _devicePaymentMethod;
		}


		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public RevenueTransactionTypeEntity GetSingleRevenueTransactionType()
		{
			return GetSingleRevenueTransactionType(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public virtual RevenueTransactionTypeEntity GetSingleRevenueTransactionType(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueTransactionType || forceFetch || _alwaysFetchRevenueTransactionType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueTransactionTypeEntityUsingTransactionTypeID);
				RevenueTransactionTypeEntity newEntity = new RevenueTransactionTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RevenueTransactionTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueTransactionTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueTransactionType = newEntity;
				_alreadyFetchedRevenueTransactionType = fetchResult;
			}
			return _revenueTransactionType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardIssuer", _cardIssuer);
			toReturn.Add("Client", _client);
			toReturn.Add("ClientFrom", _clientFrom);
			toReturn.Add("ClientTo", _clientTo);
			toReturn.Add("Debtor", _debtor);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("DevicePaymentMethod", _devicePaymentMethod);
			toReturn.Add("RevenueTransactionType", _revenueTransactionType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="validator">The validator object for this ClearingResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 clearingResultID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(clearingResultID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardIssuerReturnsNewIfNotFound = false;
			_clientReturnsNewIfNotFound = false;
			_clientFromReturnsNewIfNotFound = false;
			_clientToReturnsNewIfNotFound = false;
			_debtorReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_devicePaymentMethodReturnsNewIfNotFound = false;
			_revenueTransactionTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BadAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BadAmountTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BadCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardIssuerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingResultLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedAmountTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromClient", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoodAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoodAmountTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GoodCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCancellation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PtomUnit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuarantinedAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuarantinedAmountTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("QuarantinedCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResponsibleClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToClient", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationAmountTaxed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationCount", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardIssuer</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardIssuer(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardIssuer, new PropertyChangedEventHandler( OnCardIssuerPropertyChanged ), "CardIssuer", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingCardIssuerIDStatic, true, signalRelatedEntity, "ClearingResultsByCardIssuer", resetFKFields, new int[] { (int)ClearingResultFieldIndex.CardIssuerID } );		
			_cardIssuer = null;
		}
		
		/// <summary> setups the sync logic for member _cardIssuer</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardIssuer(IEntityCore relatedEntity)
		{
			if(_cardIssuer!=relatedEntity)
			{		
				DesetupSyncCardIssuer(true, true);
				_cardIssuer = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardIssuer, new PropertyChangedEventHandler( OnCardIssuerPropertyChanged ), "CardIssuer", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingCardIssuerIDStatic, true, ref _alreadyFetchedCardIssuer, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardIssuerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ClearingResults", resetFKFields, new int[] { (int)ClearingResultFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientFrom</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientFrom(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientFrom, new PropertyChangedEventHandler( OnClientFromPropertyChanged ), "ClientFrom", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingFromClientStatic, true, signalRelatedEntity, "ClearingResultsByClientFrom", resetFKFields, new int[] { (int)ClearingResultFieldIndex.FromClient } );		
			_clientFrom = null;
		}
		
		/// <summary> setups the sync logic for member _clientFrom</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientFrom(IEntityCore relatedEntity)
		{
			if(_clientFrom!=relatedEntity)
			{		
				DesetupSyncClientFrom(true, true);
				_clientFrom = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientFrom, new PropertyChangedEventHandler( OnClientFromPropertyChanged ), "ClientFrom", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingFromClientStatic, true, ref _alreadyFetchedClientFrom, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientFromPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _clientTo</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientTo(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientTo, new PropertyChangedEventHandler( OnClientToPropertyChanged ), "ClientTo", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingToClientStatic, true, signalRelatedEntity, "ClearingResultsByClientTo", resetFKFields, new int[] { (int)ClearingResultFieldIndex.ToClient } );		
			_clientTo = null;
		}
		
		/// <summary> setups the sync logic for member _clientTo</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientTo(IEntityCore relatedEntity)
		{
			if(_clientTo!=relatedEntity)
			{		
				DesetupSyncClientTo(true, true);
				_clientTo = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientTo, new PropertyChangedEventHandler( OnClientToPropertyChanged ), "ClientTo", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.ClientEntityUsingToClientStatic, true, ref _alreadyFetchedClientTo, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientToPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _debtor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDebtor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DebtorEntityUsingDebtorIDStatic, true, signalRelatedEntity, "ClearingResults", resetFKFields, new int[] { (int)ClearingResultFieldIndex.DebtorID } );		
			_debtor = null;
		}
		
		/// <summary> setups the sync logic for member _debtor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDebtor(IEntityCore relatedEntity)
		{
			if(_debtor!=relatedEntity)
			{		
				DesetupSyncDebtor(true, true);
				_debtor = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DebtorEntityUsingDebtorIDStatic, true, ref _alreadyFetchedDebtor, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDebtorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "ClearingResults", resetFKFields, new int[] { (int)ClearingResultFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _devicePaymentMethod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDevicePaymentMethod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _devicePaymentMethod, new PropertyChangedEventHandler( OnDevicePaymentMethodPropertyChanged ), "DevicePaymentMethod", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic, true, signalRelatedEntity, "ClearingResults", resetFKFields, new int[] { (int)ClearingResultFieldIndex.DevicePaymentMethodID } );		
			_devicePaymentMethod = null;
		}
		
		/// <summary> setups the sync logic for member _devicePaymentMethod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDevicePaymentMethod(IEntityCore relatedEntity)
		{
			if(_devicePaymentMethod!=relatedEntity)
			{		
				DesetupSyncDevicePaymentMethod(true, true);
				_devicePaymentMethod = (DevicePaymentMethodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _devicePaymentMethod, new PropertyChangedEventHandler( OnDevicePaymentMethodPropertyChanged ), "DevicePaymentMethod", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic, true, ref _alreadyFetchedDevicePaymentMethod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDevicePaymentMethodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _revenueTransactionType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueTransactionType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueTransactionType, new PropertyChangedEventHandler( OnRevenueTransactionTypePropertyChanged ), "RevenueTransactionType", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.RevenueTransactionTypeEntityUsingTransactionTypeIDStatic, true, signalRelatedEntity, "ClearingResults", resetFKFields, new int[] { (int)ClearingResultFieldIndex.TransactionTypeID } );		
			_revenueTransactionType = null;
		}
		
		/// <summary> setups the sync logic for member _revenueTransactionType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueTransactionType(IEntityCore relatedEntity)
		{
			if(_revenueTransactionType!=relatedEntity)
			{		
				DesetupSyncRevenueTransactionType(true, true);
				_revenueTransactionType = (RevenueTransactionTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueTransactionType, new PropertyChangedEventHandler( OnRevenueTransactionTypePropertyChanged ), "RevenueTransactionType", VarioSL.Entities.RelationClasses.StaticClearingResultRelations.RevenueTransactionTypeEntityUsingTransactionTypeIDStatic, true, ref _alreadyFetchedRevenueTransactionType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueTransactionTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="clearingResultID">PK value for ClearingResult which data should be fetched into this ClearingResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 clearingResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ClearingResultFieldIndex.ClearingResultID].ForcedCurrentValueWrite(clearingResultID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateClearingResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ClearingResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClearingResultRelations Relations
		{
			get	{ return new ClearingResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardIssuer
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("CardIssuer")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "CardIssuer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientFrom
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientFrom")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "ClientFrom", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientTo
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientTo")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "ClientTo", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtor
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtor")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DevicePaymentMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevicePaymentMethod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection(), (IEntityRelation)GetRelationsForField("DevicePaymentMethod")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, 0, null, null, null, "DevicePaymentMethod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueTransactionType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueTransactionType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueTransactionTypeCollection(), (IEntityRelation)GetRelationsForField("RevenueTransactionType")[0], (int)VarioSL.Entities.EntityType.ClearingResultEntity, (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, 0, null, null, null, "RevenueTransactionType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BadAmount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."BADAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BadAmount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.BadAmount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.BadAmount, value, true); }
		}

		/// <summary> The BadAmountTaxed property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."BADAMOUNTTAXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> BadAmountTaxed
		{
			get { return (Nullable<System.Double>)GetValue((int)ClearingResultFieldIndex.BadAmountTaxed, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.BadAmountTaxed, value, true); }
		}

		/// <summary> The BadCount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."BADCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BadCount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.BadCount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.BadCount, value, true); }
		}

		/// <summary> The CardIssuerID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CARDISSUERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardIssuerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.CardIssuerID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.CardIssuerID, value, true); }
		}

		/// <summary> The ClearingID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CLEARINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClearingID
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.ClearingID, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.ClearingID, value, true); }
		}

		/// <summary> The ClearingResultID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CLEARINGRESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ClearingResultID
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.ClearingResultID, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.ClearingResultID, value, true); }
		}

		/// <summary> The ClearingResultLevel property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CLEARINGRESULTLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ClearingResultLevel ClearingResultLevel
		{
			get { return (VarioSL.Entities.Enumerations.ClearingResultLevel)GetValue((int)ClearingResultFieldIndex.ClearingResultLevel, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.ClearingResultLevel, value, true); }
		}

		/// <summary> The ClientID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.ClientID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractorID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CONTRACTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.ContractorID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.ContractorID, value, true); }
		}

		/// <summary> The CorrectedAmount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CORRECTEDAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CorrectedAmount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.CorrectedAmount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.CorrectedAmount, value, true); }
		}

		/// <summary> The CorrectedAmountTaxed property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CORRECTEDAMOUNTTAXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> CorrectedAmountTaxed
		{
			get { return (Nullable<System.Double>)GetValue((int)ClearingResultFieldIndex.CorrectedAmountTaxed, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.CorrectedAmountTaxed, value, true); }
		}

		/// <summary> The CorrectedCount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CORRECTEDCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CorrectedCount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.CorrectedCount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.CorrectedCount, value, true); }
		}

		/// <summary> The CustomerGroupID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."CUSTOMERGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CustomerGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.CustomerGroupID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.CustomerGroupID, value, true); }
		}

		/// <summary> The DebtorID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.DebtorID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DevicePaymentMethodID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."DEVICEPAYMENTMETHODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DevicePaymentMethodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.DevicePaymentMethodID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.DevicePaymentMethodID, value, true); }
		}

		/// <summary> The FromClient property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."FROMCLIENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromClient
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.FromClient, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.FromClient, value, true); }
		}

		/// <summary> The GoodAmount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."GOODAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 GoodAmount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.GoodAmount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.GoodAmount, value, true); }
		}

		/// <summary> The GoodAmountTaxed property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."GOODAMOUNTTAXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> GoodAmountTaxed
		{
			get { return (Nullable<System.Double>)GetValue((int)ClearingResultFieldIndex.GoodAmountTaxed, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.GoodAmountTaxed, value, true); }
		}

		/// <summary> The GoodCount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."GOODCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 GoodCount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.GoodCount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.GoodCount, value, true); }
		}

		/// <summary> The IsCancellation property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."ISCANCELLATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsCancellation
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ClearingResultFieldIndex.IsCancellation, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.IsCancellation, value, true); }
		}

		/// <summary> The LastModified property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ClearingResultFieldIndex.LastModified, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ClearingResultFieldIndex.LastUser, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."LINEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The LineID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."LINEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.LineID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.LineID, value, true); }
		}

		/// <summary> The PtomUnit property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."PTOMUNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PtomUnit
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.PtomUnit, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.PtomUnit, value, true); }
		}

		/// <summary> The QuarantinedAmount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."QUARANTINEDAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 QuarantinedAmount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.QuarantinedAmount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.QuarantinedAmount, value, true); }
		}

		/// <summary> The QuarantinedAmountTaxed property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."QUARANTINEDAMOUNTTAXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> QuarantinedAmountTaxed
		{
			get { return (Nullable<System.Double>)GetValue((int)ClearingResultFieldIndex.QuarantinedAmountTaxed, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.QuarantinedAmountTaxed, value, true); }
		}

		/// <summary> The QuarantinedCount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."QUARANTINEDCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 QuarantinedCount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.QuarantinedCount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.QuarantinedCount, value, true); }
		}

		/// <summary> The ResponsibleClientID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."RESPONSIBLECLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ResponsibleClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.ResponsibleClientID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.ResponsibleClientID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketType property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TICKETTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TicketType> TicketType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TicketType>)GetValue((int)ClearingResultFieldIndex.TicketType, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.TicketType, value, true); }
		}

		/// <summary> The ToClient property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TOCLIENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ToClient
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.ToClient, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.ToClient, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ClearingResultFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionDate property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TRANSACTIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TransactionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ClearingResultFieldIndex.TransactionDate, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.TransactionDate, value, true); }
		}

		/// <summary> The TransactionTypeID property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TRANSACTIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ClearingResultFieldIndex.TransactionTypeID, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.TransactionTypeID, value, true); }
		}

		/// <summary> The TripCode property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."TRIPCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TripCode
		{
			get { return (System.String)GetValue((int)ClearingResultFieldIndex.TripCode, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.TripCode, value, true); }
		}

		/// <summary> The ValidationAmount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."VALIDATIONAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationAmount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.ValidationAmount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.ValidationAmount, value, true); }
		}

		/// <summary> The ValidationAmountTaxed property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."VALIDATIONAMOUNTTAXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> ValidationAmountTaxed
		{
			get { return (Nullable<System.Double>)GetValue((int)ClearingResultFieldIndex.ValidationAmountTaxed, false); }
			set	{ SetValue((int)ClearingResultFieldIndex.ValidationAmountTaxed, value, true); }
		}

		/// <summary> The ValidationCount property of the Entity ClearingResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_CLEARINGRESULT"."VALIDATIONCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ValidationCount
		{
			get { return (System.Int64)GetValue((int)ClearingResultFieldIndex.ValidationCount, true); }
			set	{ SetValue((int)ClearingResultFieldIndex.ValidationCount, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardIssuer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity CardIssuer
		{
			get	{ return GetSingleCardIssuer(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardIssuer(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResultsByCardIssuer", "CardIssuer", _cardIssuer, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardIssuer. When set to true, CardIssuer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardIssuer is accessed. You can always execute a forced fetch by calling GetSingleCardIssuer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardIssuer
		{
			get	{ return _alwaysFetchCardIssuer; }
			set	{ _alwaysFetchCardIssuer = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardIssuer already has been fetched. Setting this property to false when CardIssuer has been fetched
		/// will set CardIssuer to null as well. Setting this property to true while CardIssuer hasn't been fetched disables lazy loading for CardIssuer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardIssuer
		{
			get { return _alreadyFetchedCardIssuer;}
			set 
			{
				if(_alreadyFetchedCardIssuer && !value)
				{
					this.CardIssuer = null;
				}
				_alreadyFetchedCardIssuer = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardIssuer is not found
		/// in the database. When set to true, CardIssuer will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardIssuerReturnsNewIfNotFound
		{
			get	{ return _cardIssuerReturnsNewIfNotFound; }
			set { _cardIssuerReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResults", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientFrom()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity ClientFrom
		{
			get	{ return GetSingleClientFrom(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientFrom(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResultsByClientFrom", "ClientFrom", _clientFrom, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientFrom. When set to true, ClientFrom is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientFrom is accessed. You can always execute a forced fetch by calling GetSingleClientFrom(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientFrom
		{
			get	{ return _alwaysFetchClientFrom; }
			set	{ _alwaysFetchClientFrom = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientFrom already has been fetched. Setting this property to false when ClientFrom has been fetched
		/// will set ClientFrom to null as well. Setting this property to true while ClientFrom hasn't been fetched disables lazy loading for ClientFrom</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientFrom
		{
			get { return _alreadyFetchedClientFrom;}
			set 
			{
				if(_alreadyFetchedClientFrom && !value)
				{
					this.ClientFrom = null;
				}
				_alreadyFetchedClientFrom = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientFrom is not found
		/// in the database. When set to true, ClientFrom will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientFromReturnsNewIfNotFound
		{
			get	{ return _clientFromReturnsNewIfNotFound; }
			set { _clientFromReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientTo()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity ClientTo
		{
			get	{ return GetSingleClientTo(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientTo(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResultsByClientTo", "ClientTo", _clientTo, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientTo. When set to true, ClientTo is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientTo is accessed. You can always execute a forced fetch by calling GetSingleClientTo(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientTo
		{
			get	{ return _alwaysFetchClientTo; }
			set	{ _alwaysFetchClientTo = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientTo already has been fetched. Setting this property to false when ClientTo has been fetched
		/// will set ClientTo to null as well. Setting this property to true while ClientTo hasn't been fetched disables lazy loading for ClientTo</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientTo
		{
			get { return _alreadyFetchedClientTo;}
			set 
			{
				if(_alreadyFetchedClientTo && !value)
				{
					this.ClientTo = null;
				}
				_alreadyFetchedClientTo = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientTo is not found
		/// in the database. When set to true, ClientTo will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientToReturnsNewIfNotFound
		{
			get	{ return _clientToReturnsNewIfNotFound; }
			set { _clientToReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDebtor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity Debtor
		{
			get	{ return GetSingleDebtor(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDebtor(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResults", "Debtor", _debtor, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Debtor. When set to true, Debtor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtor is accessed. You can always execute a forced fetch by calling GetSingleDebtor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtor
		{
			get	{ return _alwaysFetchDebtor; }
			set	{ _alwaysFetchDebtor = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtor already has been fetched. Setting this property to false when Debtor has been fetched
		/// will set Debtor to null as well. Setting this property to true while Debtor hasn't been fetched disables lazy loading for Debtor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtor
		{
			get { return _alreadyFetchedDebtor;}
			set 
			{
				if(_alreadyFetchedDebtor && !value)
				{
					this.Debtor = null;
				}
				_alreadyFetchedDebtor = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Debtor is not found
		/// in the database. When set to true, Debtor will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DebtorReturnsNewIfNotFound
		{
			get	{ return _debtorReturnsNewIfNotFound; }
			set { _debtorReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResults", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DevicePaymentMethodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDevicePaymentMethod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DevicePaymentMethodEntity DevicePaymentMethod
		{
			get	{ return GetSingleDevicePaymentMethod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDevicePaymentMethod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResults", "DevicePaymentMethod", _devicePaymentMethod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DevicePaymentMethod. When set to true, DevicePaymentMethod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DevicePaymentMethod is accessed. You can always execute a forced fetch by calling GetSingleDevicePaymentMethod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevicePaymentMethod
		{
			get	{ return _alwaysFetchDevicePaymentMethod; }
			set	{ _alwaysFetchDevicePaymentMethod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DevicePaymentMethod already has been fetched. Setting this property to false when DevicePaymentMethod has been fetched
		/// will set DevicePaymentMethod to null as well. Setting this property to true while DevicePaymentMethod hasn't been fetched disables lazy loading for DevicePaymentMethod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevicePaymentMethod
		{
			get { return _alreadyFetchedDevicePaymentMethod;}
			set 
			{
				if(_alreadyFetchedDevicePaymentMethod && !value)
				{
					this.DevicePaymentMethod = null;
				}
				_alreadyFetchedDevicePaymentMethod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DevicePaymentMethod is not found
		/// in the database. When set to true, DevicePaymentMethod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DevicePaymentMethodReturnsNewIfNotFound
		{
			get	{ return _devicePaymentMethodReturnsNewIfNotFound; }
			set { _devicePaymentMethodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RevenueTransactionTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueTransactionType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueTransactionTypeEntity RevenueTransactionType
		{
			get	{ return GetSingleRevenueTransactionType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueTransactionType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClearingResults", "RevenueTransactionType", _revenueTransactionType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueTransactionType. When set to true, RevenueTransactionType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueTransactionType is accessed. You can always execute a forced fetch by calling GetSingleRevenueTransactionType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueTransactionType
		{
			get	{ return _alwaysFetchRevenueTransactionType; }
			set	{ _alwaysFetchRevenueTransactionType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueTransactionType already has been fetched. Setting this property to false when RevenueTransactionType has been fetched
		/// will set RevenueTransactionType to null as well. Setting this property to true while RevenueTransactionType hasn't been fetched disables lazy loading for RevenueTransactionType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueTransactionType
		{
			get { return _alreadyFetchedRevenueTransactionType;}
			set 
			{
				if(_alreadyFetchedRevenueTransactionType && !value)
				{
					this.RevenueTransactionType = null;
				}
				_alreadyFetchedRevenueTransactionType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueTransactionType is not found
		/// in the database. When set to true, RevenueTransactionType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueTransactionTypeReturnsNewIfNotFound
		{
			get	{ return _revenueTransactionTypeReturnsNewIfNotFound; }
			set { _revenueTransactionTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ClearingResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
