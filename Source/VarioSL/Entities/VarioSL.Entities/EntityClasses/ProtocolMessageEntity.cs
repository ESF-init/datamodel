﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProtocolMessage'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProtocolMessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ProtocolEntity _protocol;
		private bool	_alwaysFetchProtocol, _alreadyFetchedProtocol, _protocolReturnsNewIfNotFound;
		private ProtocolActionEntity _protocolAction;
		private bool	_alwaysFetchProtocolAction, _alreadyFetchedProtocolAction, _protocolActionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Protocol</summary>
			public static readonly string Protocol = "Protocol";
			/// <summary>Member name ProtocolAction</summary>
			public static readonly string ProtocolAction = "ProtocolAction";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProtocolMessageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProtocolMessageEntity() :base("ProtocolMessageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		public ProtocolMessageEntity(System.Int64 protocolMessageID):base("ProtocolMessageEntity")
		{
			InitClassFetch(protocolMessageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProtocolMessageEntity(System.Int64 protocolMessageID, IPrefetchPath prefetchPathToUse):base("ProtocolMessageEntity")
		{
			InitClassFetch(protocolMessageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="validator">The custom validator object for this ProtocolMessageEntity</param>
		public ProtocolMessageEntity(System.Int64 protocolMessageID, IValidator validator):base("ProtocolMessageEntity")
		{
			InitClassFetch(protocolMessageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProtocolMessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_protocol = (ProtocolEntity)info.GetValue("_protocol", typeof(ProtocolEntity));
			if(_protocol!=null)
			{
				_protocol.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_protocolReturnsNewIfNotFound = info.GetBoolean("_protocolReturnsNewIfNotFound");
			_alwaysFetchProtocol = info.GetBoolean("_alwaysFetchProtocol");
			_alreadyFetchedProtocol = info.GetBoolean("_alreadyFetchedProtocol");

			_protocolAction = (ProtocolActionEntity)info.GetValue("_protocolAction", typeof(ProtocolActionEntity));
			if(_protocolAction!=null)
			{
				_protocolAction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_protocolActionReturnsNewIfNotFound = info.GetBoolean("_protocolActionReturnsNewIfNotFound");
			_alwaysFetchProtocolAction = info.GetBoolean("_alwaysFetchProtocolAction");
			_alreadyFetchedProtocolAction = info.GetBoolean("_alreadyFetchedProtocolAction");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProtocolMessageFieldIndex)fieldIndex)
			{
				case ProtocolMessageFieldIndex.ActionID:
					DesetupSyncProtocolAction(true, false);
					_alreadyFetchedProtocolAction = false;
					break;
				case ProtocolMessageFieldIndex.ProtocolID:
					DesetupSyncProtocol(true, false);
					_alreadyFetchedProtocol = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProtocol = (_protocol != null);
			_alreadyFetchedProtocolAction = (_protocolAction != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Protocol":
					toReturn.Add(Relations.ProtocolEntityUsingProtocolID);
					break;
				case "ProtocolAction":
					toReturn.Add(Relations.ProtocolActionEntityUsingActionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_protocol", (!this.MarkedForDeletion?_protocol:null));
			info.AddValue("_protocolReturnsNewIfNotFound", _protocolReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProtocol", _alwaysFetchProtocol);
			info.AddValue("_alreadyFetchedProtocol", _alreadyFetchedProtocol);
			info.AddValue("_protocolAction", (!this.MarkedForDeletion?_protocolAction:null));
			info.AddValue("_protocolActionReturnsNewIfNotFound", _protocolActionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProtocolAction", _alwaysFetchProtocolAction);
			info.AddValue("_alreadyFetchedProtocolAction", _alreadyFetchedProtocolAction);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Protocol":
					_alreadyFetchedProtocol = true;
					this.Protocol = (ProtocolEntity)entity;
					break;
				case "ProtocolAction":
					_alreadyFetchedProtocolAction = true;
					this.ProtocolAction = (ProtocolActionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Protocol":
					SetupSyncProtocol(relatedEntity);
					break;
				case "ProtocolAction":
					SetupSyncProtocolAction(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Protocol":
					DesetupSyncProtocol(false, true);
					break;
				case "ProtocolAction":
					DesetupSyncProtocolAction(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_protocol!=null)
			{
				toReturn.Add(_protocol);
			}
			if(_protocolAction!=null)
			{
				toReturn.Add(_protocolAction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolMessageID)
		{
			return FetchUsingPK(protocolMessageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolMessageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(protocolMessageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(protocolMessageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(protocolMessageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProtocolMessageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProtocolMessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ProtocolEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProtocolEntity' which is related to this entity.</returns>
		public ProtocolEntity GetSingleProtocol()
		{
			return GetSingleProtocol(false);
		}

		/// <summary> Retrieves the related entity of type 'ProtocolEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProtocolEntity' which is related to this entity.</returns>
		public virtual ProtocolEntity GetSingleProtocol(bool forceFetch)
		{
			if( ( !_alreadyFetchedProtocol || forceFetch || _alwaysFetchProtocol) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProtocolEntityUsingProtocolID);
				ProtocolEntity newEntity = new ProtocolEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProtocolID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProtocolEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_protocolReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Protocol = newEntity;
				_alreadyFetchedProtocol = fetchResult;
			}
			return _protocol;
		}


		/// <summary> Retrieves the related entity of type 'ProtocolActionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProtocolActionEntity' which is related to this entity.</returns>
		public ProtocolActionEntity GetSingleProtocolAction()
		{
			return GetSingleProtocolAction(false);
		}

		/// <summary> Retrieves the related entity of type 'ProtocolActionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProtocolActionEntity' which is related to this entity.</returns>
		public virtual ProtocolActionEntity GetSingleProtocolAction(bool forceFetch)
		{
			if( ( !_alreadyFetchedProtocolAction || forceFetch || _alwaysFetchProtocolAction) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProtocolActionEntityUsingActionID);
				ProtocolActionEntity newEntity = new ProtocolActionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ActionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProtocolActionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_protocolActionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProtocolAction = newEntity;
				_alreadyFetchedProtocolAction = fetchResult;
			}
			return _protocolAction;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Protocol", _protocol);
			toReturn.Add("ProtocolAction", _protocolAction);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="validator">The validator object for this ProtocolMessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 protocolMessageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(protocolMessageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_protocolReturnsNewIfNotFound = false;
			_protocolActionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProtocolID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProtocolMessageID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _protocol</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProtocol(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _protocol, new PropertyChangedEventHandler( OnProtocolPropertyChanged ), "Protocol", VarioSL.Entities.RelationClasses.StaticProtocolMessageRelations.ProtocolEntityUsingProtocolIDStatic, true, signalRelatedEntity, "ProtMessages", resetFKFields, new int[] { (int)ProtocolMessageFieldIndex.ProtocolID } );		
			_protocol = null;
		}
		
		/// <summary> setups the sync logic for member _protocol</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProtocol(IEntityCore relatedEntity)
		{
			if(_protocol!=relatedEntity)
			{		
				DesetupSyncProtocol(true, true);
				_protocol = (ProtocolEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _protocol, new PropertyChangedEventHandler( OnProtocolPropertyChanged ), "Protocol", VarioSL.Entities.RelationClasses.StaticProtocolMessageRelations.ProtocolEntityUsingProtocolIDStatic, true, ref _alreadyFetchedProtocol, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProtocolPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _protocolAction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProtocolAction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _protocolAction, new PropertyChangedEventHandler( OnProtocolActionPropertyChanged ), "ProtocolAction", VarioSL.Entities.RelationClasses.StaticProtocolMessageRelations.ProtocolActionEntityUsingActionIDStatic, true, signalRelatedEntity, "ProtocolMessages", resetFKFields, new int[] { (int)ProtocolMessageFieldIndex.ActionID } );		
			_protocolAction = null;
		}
		
		/// <summary> setups the sync logic for member _protocolAction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProtocolAction(IEntityCore relatedEntity)
		{
			if(_protocolAction!=relatedEntity)
			{		
				DesetupSyncProtocolAction(true, true);
				_protocolAction = (ProtocolActionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _protocolAction, new PropertyChangedEventHandler( OnProtocolActionPropertyChanged ), "ProtocolAction", VarioSL.Entities.RelationClasses.StaticProtocolMessageRelations.ProtocolActionEntityUsingActionIDStatic, true, ref _alreadyFetchedProtocolAction, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProtocolActionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="protocolMessageID">PK value for ProtocolMessage which data should be fetched into this ProtocolMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 protocolMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProtocolMessageFieldIndex.ProtocolMessageID].ForcedCurrentValueWrite(protocolMessageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProtocolMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProtocolMessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProtocolMessageRelations Relations
		{
			get	{ return new ProtocolMessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Protocol'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocol
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolCollection(), (IEntityRelation)GetRelationsForField("Protocol")[0], (int)VarioSL.Entities.EntityType.ProtocolMessageEntity, (int)VarioSL.Entities.EntityType.ProtocolEntity, 0, null, null, null, "Protocol", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProtocolAction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocolAction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolActionCollection(), (IEntityRelation)GetRelationsForField("ProtocolAction")[0], (int)VarioSL.Entities.EntityType.ProtocolMessageEntity, (int)VarioSL.Entities.EntityType.ProtocolActionEntity, 0, null, null, null, "ProtocolAction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ActionID property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ActionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProtocolMessageFieldIndex.ActionID, false); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.ActionID, value, true); }
		}

		/// <summary> The MessageDate property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTMESSAGEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime MessageDate
		{
			get { return (System.DateTime)GetValue((int)ProtocolMessageFieldIndex.MessageDate, true); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.MessageDate, value, true); }
		}

		/// <summary> The MessageText property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTMESSAGETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1800<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageText
		{
			get { return (System.String)GetValue((int)ProtocolMessageFieldIndex.MessageText, true); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.MessageText, value, true); }
		}

		/// <summary> The Priority property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTMESSAGEPRIO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Priority
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolMessageFieldIndex.Priority, false); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.Priority, value, true); }
		}

		/// <summary> The ProtocolID property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 15, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProtocolID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProtocolMessageFieldIndex.ProtocolID, false); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.ProtocolID, value, true); }
		}

		/// <summary> The ProtocolMessageID property of the Entity ProtocolMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROT_MESSAGE"."PROTMESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 15, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ProtocolMessageID
		{
			get { return (System.Int64)GetValue((int)ProtocolMessageFieldIndex.ProtocolMessageID, true); }
			set	{ SetValue((int)ProtocolMessageFieldIndex.ProtocolMessageID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ProtocolEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProtocol()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProtocolEntity Protocol
		{
			get	{ return GetSingleProtocol(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProtocol(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProtMessages", "Protocol", _protocol, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Protocol. When set to true, Protocol is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Protocol is accessed. You can always execute a forced fetch by calling GetSingleProtocol(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocol
		{
			get	{ return _alwaysFetchProtocol; }
			set	{ _alwaysFetchProtocol = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Protocol already has been fetched. Setting this property to false when Protocol has been fetched
		/// will set Protocol to null as well. Setting this property to true while Protocol hasn't been fetched disables lazy loading for Protocol</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocol
		{
			get { return _alreadyFetchedProtocol;}
			set 
			{
				if(_alreadyFetchedProtocol && !value)
				{
					this.Protocol = null;
				}
				_alreadyFetchedProtocol = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Protocol is not found
		/// in the database. When set to true, Protocol will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProtocolReturnsNewIfNotFound
		{
			get	{ return _protocolReturnsNewIfNotFound; }
			set { _protocolReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProtocolActionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProtocolAction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProtocolActionEntity ProtocolAction
		{
			get	{ return GetSingleProtocolAction(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProtocolAction(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ProtocolMessages", "ProtocolAction", _protocolAction, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProtocolAction. When set to true, ProtocolAction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProtocolAction is accessed. You can always execute a forced fetch by calling GetSingleProtocolAction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocolAction
		{
			get	{ return _alwaysFetchProtocolAction; }
			set	{ _alwaysFetchProtocolAction = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProtocolAction already has been fetched. Setting this property to false when ProtocolAction has been fetched
		/// will set ProtocolAction to null as well. Setting this property to true while ProtocolAction hasn't been fetched disables lazy loading for ProtocolAction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocolAction
		{
			get { return _alreadyFetchedProtocolAction;}
			set 
			{
				if(_alreadyFetchedProtocolAction && !value)
				{
					this.ProtocolAction = null;
				}
				_alreadyFetchedProtocolAction = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProtocolAction is not found
		/// in the database. When set to true, ProtocolAction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProtocolActionReturnsNewIfNotFound
		{
			get	{ return _protocolActionReturnsNewIfNotFound; }
			set { _protocolActionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProtocolMessageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
