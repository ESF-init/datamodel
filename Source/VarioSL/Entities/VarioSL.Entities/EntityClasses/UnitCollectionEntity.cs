﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UnitCollection'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UnitCollectionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ParameterValueCollection	_parameterValues;
		private bool	_alwaysFetchParameterValues, _alreadyFetchedParameterValues;
		private VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection	_unitCollectionToUnits;
		private bool	_alwaysFetchUnitCollectionToUnits, _alreadyFetchedUnitCollectionToUnits;
		private VarioSL.Entities.CollectionClasses.UnitCollection _units;
		private bool	_alwaysFetchUnits, _alreadyFetchedUnits;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private ParameterArchiveEntity _parameterArchive;
		private bool	_alwaysFetchParameterArchive, _alreadyFetchedParameterArchive, _parameterArchiveReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name ParameterArchive</summary>
			public static readonly string ParameterArchive = "ParameterArchive";
			/// <summary>Member name ParameterValues</summary>
			public static readonly string ParameterValues = "ParameterValues";
			/// <summary>Member name UnitCollectionToUnits</summary>
			public static readonly string UnitCollectionToUnits = "UnitCollectionToUnits";
			/// <summary>Member name Units</summary>
			public static readonly string Units = "Units";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UnitCollectionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UnitCollectionEntity() :base("UnitCollectionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		public UnitCollectionEntity(System.Int64 unitCollectionID):base("UnitCollectionEntity")
		{
			InitClassFetch(unitCollectionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UnitCollectionEntity(System.Int64 unitCollectionID, IPrefetchPath prefetchPathToUse):base("UnitCollectionEntity")
		{
			InitClassFetch(unitCollectionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="validator">The custom validator object for this UnitCollectionEntity</param>
		public UnitCollectionEntity(System.Int64 unitCollectionID, IValidator validator):base("UnitCollectionEntity")
		{
			InitClassFetch(unitCollectionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UnitCollectionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parameterValues = (VarioSL.Entities.CollectionClasses.ParameterValueCollection)info.GetValue("_parameterValues", typeof(VarioSL.Entities.CollectionClasses.ParameterValueCollection));
			_alwaysFetchParameterValues = info.GetBoolean("_alwaysFetchParameterValues");
			_alreadyFetchedParameterValues = info.GetBoolean("_alreadyFetchedParameterValues");

			_unitCollectionToUnits = (VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection)info.GetValue("_unitCollectionToUnits", typeof(VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection));
			_alwaysFetchUnitCollectionToUnits = info.GetBoolean("_alwaysFetchUnitCollectionToUnits");
			_alreadyFetchedUnitCollectionToUnits = info.GetBoolean("_alreadyFetchedUnitCollectionToUnits");
			_units = (VarioSL.Entities.CollectionClasses.UnitCollection)info.GetValue("_units", typeof(VarioSL.Entities.CollectionClasses.UnitCollection));
			_alwaysFetchUnits = info.GetBoolean("_alwaysFetchUnits");
			_alreadyFetchedUnits = info.GetBoolean("_alreadyFetchedUnits");
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_parameterArchive = (ParameterArchiveEntity)info.GetValue("_parameterArchive", typeof(ParameterArchiveEntity));
			if(_parameterArchive!=null)
			{
				_parameterArchive.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parameterArchiveReturnsNewIfNotFound = info.GetBoolean("_parameterArchiveReturnsNewIfNotFound");
			_alwaysFetchParameterArchive = info.GetBoolean("_alwaysFetchParameterArchive");
			_alreadyFetchedParameterArchive = info.GetBoolean("_alreadyFetchedParameterArchive");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UnitCollectionFieldIndex)fieldIndex)
			{
				case UnitCollectionFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case UnitCollectionFieldIndex.ParameterArchiveID:
					DesetupSyncParameterArchive(true, false);
					_alreadyFetchedParameterArchive = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParameterValues = (_parameterValues.Count > 0);
			_alreadyFetchedUnitCollectionToUnits = (_unitCollectionToUnits.Count > 0);
			_alreadyFetchedUnits = (_units.Count > 0);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedParameterArchive = (_parameterArchive != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "ParameterArchive":
					toReturn.Add(Relations.ParameterArchiveEntityUsingParameterArchiveID);
					break;
				case "ParameterValues":
					toReturn.Add(Relations.ParameterValueEntityUsingUnitCollectionID);
					break;
				case "UnitCollectionToUnits":
					toReturn.Add(Relations.UnitCollectionToUnitEntityUsingUnitCollectionID);
					break;
				case "Units":
					toReturn.Add(Relations.UnitCollectionToUnitEntityUsingUnitCollectionID, "UnitCollectionEntity__", "UnitCollectionToUnit_", JoinHint.None);
					toReturn.Add(UnitCollectionToUnitEntity.Relations.UnitEntityUsingUnitID, "UnitCollectionToUnit_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parameterValues", (!this.MarkedForDeletion?_parameterValues:null));
			info.AddValue("_alwaysFetchParameterValues", _alwaysFetchParameterValues);
			info.AddValue("_alreadyFetchedParameterValues", _alreadyFetchedParameterValues);
			info.AddValue("_unitCollectionToUnits", (!this.MarkedForDeletion?_unitCollectionToUnits:null));
			info.AddValue("_alwaysFetchUnitCollectionToUnits", _alwaysFetchUnitCollectionToUnits);
			info.AddValue("_alreadyFetchedUnitCollectionToUnits", _alreadyFetchedUnitCollectionToUnits);
			info.AddValue("_units", (!this.MarkedForDeletion?_units:null));
			info.AddValue("_alwaysFetchUnits", _alwaysFetchUnits);
			info.AddValue("_alreadyFetchedUnits", _alreadyFetchedUnits);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_parameterArchive", (!this.MarkedForDeletion?_parameterArchive:null));
			info.AddValue("_parameterArchiveReturnsNewIfNotFound", _parameterArchiveReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParameterArchive", _alwaysFetchParameterArchive);
			info.AddValue("_alreadyFetchedParameterArchive", _alreadyFetchedParameterArchive);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "ParameterArchive":
					_alreadyFetchedParameterArchive = true;
					this.ParameterArchive = (ParameterArchiveEntity)entity;
					break;
				case "ParameterValues":
					_alreadyFetchedParameterValues = true;
					if(entity!=null)
					{
						this.ParameterValues.Add((ParameterValueEntity)entity);
					}
					break;
				case "UnitCollectionToUnits":
					_alreadyFetchedUnitCollectionToUnits = true;
					if(entity!=null)
					{
						this.UnitCollectionToUnits.Add((UnitCollectionToUnitEntity)entity);
					}
					break;
				case "Units":
					_alreadyFetchedUnits = true;
					if(entity!=null)
					{
						this.Units.Add((UnitEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "ParameterArchive":
					SetupSyncParameterArchive(relatedEntity);
					break;
				case "ParameterValues":
					_parameterValues.Add((ParameterValueEntity)relatedEntity);
					break;
				case "UnitCollectionToUnits":
					_unitCollectionToUnits.Add((UnitCollectionToUnitEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "ParameterArchive":
					DesetupSyncParameterArchive(false, true);
					break;
				case "ParameterValues":
					this.PerformRelatedEntityRemoval(_parameterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UnitCollectionToUnits":
					this.PerformRelatedEntityRemoval(_unitCollectionToUnits, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_parameterArchive!=null)
			{
				toReturn.Add(_parameterArchive);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_parameterValues);
			toReturn.Add(_unitCollectionToUnits);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID)
		{
			return FetchUsingPK(unitCollectionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(unitCollectionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(unitCollectionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(unitCollectionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UnitCollectionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UnitCollectionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterValues || forceFetch || _alwaysFetchParameterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterValues);
				_parameterValues.SuppressClearInGetMulti=!forceFetch;
				_parameterValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterValues.GetMultiManyToOne(null, null, null, null, this, filter);
				_parameterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterValues = true;
			}
			return _parameterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterValues'. These settings will be taken into account
		/// when the property ParameterValues is requested or GetMultiParameterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterValues.SortClauses=sortClauses;
			_parameterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, _unitCollectionToUnits.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionToUnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, _unitCollectionToUnits.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUnitCollectionToUnits(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection GetMultiUnitCollectionToUnits(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUnitCollectionToUnits || forceFetch || _alwaysFetchUnitCollectionToUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_unitCollectionToUnits);
				_unitCollectionToUnits.SuppressClearInGetMulti=!forceFetch;
				_unitCollectionToUnits.EntityFactoryToUse = entityFactoryToUse;
				_unitCollectionToUnits.GetMultiManyToOne(null, this, filter);
				_unitCollectionToUnits.SuppressClearInGetMulti=false;
				_alreadyFetchedUnitCollectionToUnits = true;
			}
			return _unitCollectionToUnits;
		}

		/// <summary> Sets the collection parameters for the collection for 'UnitCollectionToUnits'. These settings will be taken into account
		/// when the property UnitCollectionToUnits is requested or GetMultiUnitCollectionToUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnitCollectionToUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_unitCollectionToUnits.SortClauses=sortClauses;
			_unitCollectionToUnits.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch)
		{
			return GetMultiUnits(forceFetch, _units.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollection GetMultiUnits(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUnits || forceFetch || _alwaysFetchUnits) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_units);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UnitCollectionFields.UnitCollectionID, ComparisonOperator.Equal, this.UnitCollectionID, "UnitCollectionEntity__"));
				_units.SuppressClearInGetMulti=!forceFetch;
				_units.EntityFactoryToUse = entityFactoryToUse;
				_units.GetMulti(filter, GetRelationsForField("Units"));
				_units.SuppressClearInGetMulti=false;
				_alreadyFetchedUnits = true;
			}
			return _units;
		}

		/// <summary> Sets the collection parameters for the collection for 'Units'. These settings will be taken into account
		/// when the property Units is requested or GetMultiUnits is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnits(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_units.SortClauses=sortClauses;
			_units.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'ParameterArchiveEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParameterArchiveEntity' which is related to this entity.</returns>
		public ParameterArchiveEntity GetSingleParameterArchive()
		{
			return GetSingleParameterArchive(false);
		}

		/// <summary> Retrieves the related entity of type 'ParameterArchiveEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParameterArchiveEntity' which is related to this entity.</returns>
		public virtual ParameterArchiveEntity GetSingleParameterArchive(bool forceFetch)
		{
			if( ( !_alreadyFetchedParameterArchive || forceFetch || _alwaysFetchParameterArchive) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParameterArchiveEntityUsingParameterArchiveID);
				ParameterArchiveEntity newEntity = new ParameterArchiveEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParameterArchiveID);
				}
				if(fetchResult)
				{
					newEntity = (ParameterArchiveEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parameterArchiveReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParameterArchive = newEntity;
				_alreadyFetchedParameterArchive = fetchResult;
			}
			return _parameterArchive;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("ParameterArchive", _parameterArchive);
			toReturn.Add("ParameterValues", _parameterValues);
			toReturn.Add("UnitCollectionToUnits", _unitCollectionToUnits);
			toReturn.Add("Units", _units);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="validator">The validator object for this UnitCollectionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 unitCollectionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(unitCollectionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_parameterValues = new VarioSL.Entities.CollectionClasses.ParameterValueCollection();
			_parameterValues.SetContainingEntityInfo(this, "UnitCollection");

			_unitCollectionToUnits = new VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection();
			_unitCollectionToUnits.SetContainingEntityInfo(this, "UnitCollection");
			_units = new VarioSL.Entities.CollectionClasses.UnitCollection();
			_deviceClassReturnsNewIfNotFound = false;
			_parameterArchiveReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Abbreviation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefaultForDeviceClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsGlobal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitCollectionID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticUnitCollectionRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "UnitCollections", resetFKFields, new int[] { (int)UnitCollectionFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticUnitCollectionRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parameterArchive</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParameterArchive(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parameterArchive, new PropertyChangedEventHandler( OnParameterArchivePropertyChanged ), "ParameterArchive", VarioSL.Entities.RelationClasses.StaticUnitCollectionRelations.ParameterArchiveEntityUsingParameterArchiveIDStatic, true, signalRelatedEntity, "UnitCollections", resetFKFields, new int[] { (int)UnitCollectionFieldIndex.ParameterArchiveID } );		
			_parameterArchive = null;
		}
		
		/// <summary> setups the sync logic for member _parameterArchive</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParameterArchive(IEntityCore relatedEntity)
		{
			if(_parameterArchive!=relatedEntity)
			{		
				DesetupSyncParameterArchive(true, true);
				_parameterArchive = (ParameterArchiveEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parameterArchive, new PropertyChangedEventHandler( OnParameterArchivePropertyChanged ), "ParameterArchive", VarioSL.Entities.RelationClasses.StaticUnitCollectionRelations.ParameterArchiveEntityUsingParameterArchiveIDStatic, true, ref _alreadyFetchedParameterArchive, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParameterArchivePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="unitCollectionID">PK value for UnitCollection which data should be fetched into this UnitCollection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 unitCollectionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UnitCollectionFieldIndex.UnitCollectionID].ForcedCurrentValueWrite(unitCollectionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUnitCollectionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UnitCollectionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UnitCollectionRelations Relations
		{
			get	{ return new UnitCollectionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterValueCollection(), (IEntityRelation)GetRelationsForField("ParameterValues")[0], (int)VarioSL.Entities.EntityType.UnitCollectionEntity, (int)VarioSL.Entities.EntityType.ParameterValueEntity, 0, null, null, null, "ParameterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollectionToUnit' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollectionToUnits
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection(), (IEntityRelation)GetRelationsForField("UnitCollectionToUnits")[0], (int)VarioSL.Entities.EntityType.UnitCollectionEntity, (int)VarioSL.Entities.EntityType.UnitCollectionToUnitEntity, 0, null, null, null, "UnitCollectionToUnits", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnits
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UnitCollectionToUnitEntityUsingUnitCollectionID;
				intermediateRelation.SetAliases(string.Empty, "UnitCollectionToUnit_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.UnitCollectionEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, GetRelationsForField("Units"), "Units", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.UnitCollectionEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterArchive'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterArchive
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterArchiveCollection(), (IEntityRelation)GetRelationsForField("ParameterArchive")[0], (int)VarioSL.Entities.EntityType.UnitCollectionEntity, (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, 0, null, null, null, "ParameterArchive", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Abbreviation property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."ABBREVIATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Abbreviation
		{
			get { return (System.String)GetValue((int)UnitCollectionFieldIndex.Abbreviation, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.Abbreviation, value, true); }
		}

		/// <summary> The Description property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)UnitCollectionFieldIndex.Description, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UnitCollectionFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)UnitCollectionFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The IsDefaultForDeviceClass property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."ISDEFAULTFORDEVICECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDefaultForDeviceClass
		{
			get { return (System.Boolean)GetValue((int)UnitCollectionFieldIndex.IsDefaultForDeviceClass, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.IsDefaultForDeviceClass, value, true); }
		}

		/// <summary> The IsGlobal property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."ISGLOBAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsGlobal
		{
			get { return (System.Boolean)GetValue((int)UnitCollectionFieldIndex.IsGlobal, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.IsGlobal, value, true); }
		}

		/// <summary> The LastModified property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)UnitCollectionFieldIndex.LastModified, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)UnitCollectionFieldIndex.LastUser, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UnitCollectionFieldIndex.Name, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.Name, value, true); }
		}

		/// <summary> The ParameterArchiveID property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."PARAMETERARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ParameterArchiveID
		{
			get { return (System.Int64)GetValue((int)UnitCollectionFieldIndex.ParameterArchiveID, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.ParameterArchiveID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)UnitCollectionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UnitCollectionID property of the Entity UnitCollection<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTION"."UNITCOLLECTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 UnitCollectionID
		{
			get { return (System.Int64)GetValue((int)UnitCollectionFieldIndex.UnitCollectionID, true); }
			set	{ SetValue((int)UnitCollectionFieldIndex.UnitCollectionID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection ParameterValues
		{
			get	{ return GetMultiParameterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterValues. When set to true, ParameterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterValues
		{
			get	{ return _alwaysFetchParameterValues; }
			set	{ _alwaysFetchParameterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterValues already has been fetched. Setting this property to false when ParameterValues has been fetched
		/// will clear the ParameterValues collection well. Setting this property to true while ParameterValues hasn't been fetched disables lazy loading for ParameterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterValues
		{
			get { return _alreadyFetchedParameterValues;}
			set 
			{
				if(_alreadyFetchedParameterValues && !value && (_parameterValues != null))
				{
					_parameterValues.Clear();
				}
				_alreadyFetchedParameterValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitCollectionToUnitEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnitCollectionToUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionToUnitCollection UnitCollectionToUnits
		{
			get	{ return GetMultiUnitCollectionToUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollectionToUnits. When set to true, UnitCollectionToUnits is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollectionToUnits is accessed. You can always execute/ a forced fetch by calling GetMultiUnitCollectionToUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollectionToUnits
		{
			get	{ return _alwaysFetchUnitCollectionToUnits; }
			set	{ _alwaysFetchUnitCollectionToUnits = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollectionToUnits already has been fetched. Setting this property to false when UnitCollectionToUnits has been fetched
		/// will clear the UnitCollectionToUnits collection well. Setting this property to true while UnitCollectionToUnits hasn't been fetched disables lazy loading for UnitCollectionToUnits</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollectionToUnits
		{
			get { return _alreadyFetchedUnitCollectionToUnits;}
			set 
			{
				if(_alreadyFetchedUnitCollectionToUnits && !value && (_unitCollectionToUnits != null))
				{
					_unitCollectionToUnits.Clear();
				}
				_alreadyFetchedUnitCollectionToUnits = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UnitEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnits()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollection Units
		{
			get { return GetMultiUnits(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Units. When set to true, Units is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Units is accessed. You can always execute a forced fetch by calling GetMultiUnits(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnits
		{
			get	{ return _alwaysFetchUnits; }
			set	{ _alwaysFetchUnits = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Units already has been fetched. Setting this property to false when Units has been fetched
		/// will clear the Units collection well. Setting this property to true while Units hasn't been fetched disables lazy loading for Units</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnits
		{
			get { return _alreadyFetchedUnits;}
			set 
			{
				if(_alreadyFetchedUnits && !value && (_units != null))
				{
					_units.Clear();
				}
				_alreadyFetchedUnits = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UnitCollections", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ParameterArchiveEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParameterArchive()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParameterArchiveEntity ParameterArchive
		{
			get	{ return GetSingleParameterArchive(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParameterArchive(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UnitCollections", "ParameterArchive", _parameterArchive, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterArchive. When set to true, ParameterArchive is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterArchive is accessed. You can always execute a forced fetch by calling GetSingleParameterArchive(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterArchive
		{
			get	{ return _alwaysFetchParameterArchive; }
			set	{ _alwaysFetchParameterArchive = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterArchive already has been fetched. Setting this property to false when ParameterArchive has been fetched
		/// will set ParameterArchive to null as well. Setting this property to true while ParameterArchive hasn't been fetched disables lazy loading for ParameterArchive</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterArchive
		{
			get { return _alreadyFetchedParameterArchive;}
			set 
			{
				if(_alreadyFetchedParameterArchive && !value)
				{
					this.ParameterArchive = null;
				}
				_alreadyFetchedParameterArchive = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParameterArchive is not found
		/// in the database. When set to true, ParameterArchive will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParameterArchiveReturnsNewIfNotFound
		{
			get	{ return _parameterArchiveReturnsNewIfNotFound; }
			set { _parameterArchiveReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UnitCollectionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
