﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BusinessRuleResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BusinessRuleResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection	_businessRuleClause;
		private bool	_alwaysFetchBusinessRuleClause, _alreadyFetchedBusinessRuleClause;
		private AttributeValueEntity _userGroup;
		private bool	_alwaysFetchUserGroup, _alreadyFetchedUserGroup, _userGroupReturnsNewIfNotFound;
		private BusinessRuleTypeEntity _businessRuleType;
		private bool	_alwaysFetchBusinessRuleType, _alreadyFetchedBusinessRuleType, _businessRuleTypeReturnsNewIfNotFound;
		private FareStageEntity _fareStage;
		private bool	_alwaysFetchFareStage, _alreadyFetchedFareStage, _fareStageReturnsNewIfNotFound;
		private KeySelectionModeEntity _keySelectionMode;
		private bool	_alwaysFetchKeySelectionMode, _alreadyFetchedKeySelectionMode, _keySelectionModeReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserGroup</summary>
			public static readonly string UserGroup = "UserGroup";
			/// <summary>Member name BusinessRuleType</summary>
			public static readonly string BusinessRuleType = "BusinessRuleType";
			/// <summary>Member name FareStage</summary>
			public static readonly string FareStage = "FareStage";
			/// <summary>Member name KeySelectionMode</summary>
			public static readonly string KeySelectionMode = "KeySelectionMode";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name BusinessRuleClause</summary>
			public static readonly string BusinessRuleClause = "BusinessRuleClause";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BusinessRuleResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BusinessRuleResultEntity() :base("BusinessRuleResultEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		public BusinessRuleResultEntity(System.Int64 businessRuleResultID):base("BusinessRuleResultEntity")
		{
			InitClassFetch(businessRuleResultID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BusinessRuleResultEntity(System.Int64 businessRuleResultID, IPrefetchPath prefetchPathToUse):base("BusinessRuleResultEntity")
		{
			InitClassFetch(businessRuleResultID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="validator">The custom validator object for this BusinessRuleResultEntity</param>
		public BusinessRuleResultEntity(System.Int64 businessRuleResultID, IValidator validator):base("BusinessRuleResultEntity")
		{
			InitClassFetch(businessRuleResultID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinessRuleResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_businessRuleClause = (VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection)info.GetValue("_businessRuleClause", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection));
			_alwaysFetchBusinessRuleClause = info.GetBoolean("_alwaysFetchBusinessRuleClause");
			_alreadyFetchedBusinessRuleClause = info.GetBoolean("_alreadyFetchedBusinessRuleClause");
			_userGroup = (AttributeValueEntity)info.GetValue("_userGroup", typeof(AttributeValueEntity));
			if(_userGroup!=null)
			{
				_userGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userGroupReturnsNewIfNotFound = info.GetBoolean("_userGroupReturnsNewIfNotFound");
			_alwaysFetchUserGroup = info.GetBoolean("_alwaysFetchUserGroup");
			_alreadyFetchedUserGroup = info.GetBoolean("_alreadyFetchedUserGroup");

			_businessRuleType = (BusinessRuleTypeEntity)info.GetValue("_businessRuleType", typeof(BusinessRuleTypeEntity));
			if(_businessRuleType!=null)
			{
				_businessRuleType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleTypeReturnsNewIfNotFound = info.GetBoolean("_businessRuleTypeReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleType = info.GetBoolean("_alwaysFetchBusinessRuleType");
			_alreadyFetchedBusinessRuleType = info.GetBoolean("_alreadyFetchedBusinessRuleType");

			_fareStage = (FareStageEntity)info.GetValue("_fareStage", typeof(FareStageEntity));
			if(_fareStage!=null)
			{
				_fareStage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageReturnsNewIfNotFound = info.GetBoolean("_fareStageReturnsNewIfNotFound");
			_alwaysFetchFareStage = info.GetBoolean("_alwaysFetchFareStage");
			_alreadyFetchedFareStage = info.GetBoolean("_alreadyFetchedFareStage");

			_keySelectionMode = (KeySelectionModeEntity)info.GetValue("_keySelectionMode", typeof(KeySelectionModeEntity));
			if(_keySelectionMode!=null)
			{
				_keySelectionMode.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_keySelectionModeReturnsNewIfNotFound = info.GetBoolean("_keySelectionModeReturnsNewIfNotFound");
			_alwaysFetchKeySelectionMode = info.GetBoolean("_alwaysFetchKeySelectionMode");
			_alreadyFetchedKeySelectionMode = info.GetBoolean("_alreadyFetchedKeySelectionMode");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BusinessRuleResultFieldIndex)fieldIndex)
			{
				case BusinessRuleResultFieldIndex.BusinessRuleTypeID:
					DesetupSyncBusinessRuleType(true, false);
					_alreadyFetchedBusinessRuleType = false;
					break;
				case BusinessRuleResultFieldIndex.DestinationFarestageID:
					DesetupSyncFareStage(true, false);
					_alreadyFetchedFareStage = false;
					break;
				case BusinessRuleResultFieldIndex.KeySelectionModeID:
					DesetupSyncKeySelectionMode(true, false);
					_alreadyFetchedKeySelectionMode = false;
					break;
				case BusinessRuleResultFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case BusinessRuleResultFieldIndex.UserGroupAttributeValue:
					DesetupSyncUserGroup(true, false);
					_alreadyFetchedUserGroup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBusinessRuleClause = (_businessRuleClause.Count > 0);
			_alreadyFetchedUserGroup = (_userGroup != null);
			_alreadyFetchedBusinessRuleType = (_businessRuleType != null);
			_alreadyFetchedFareStage = (_fareStage != null);
			_alreadyFetchedKeySelectionMode = (_keySelectionMode != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserGroup":
					toReturn.Add(Relations.AttributeValueEntityUsingUserGroupAttributeValue);
					break;
				case "BusinessRuleType":
					toReturn.Add(Relations.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
					break;
				case "FareStage":
					toReturn.Add(Relations.FareStageEntityUsingDestinationFarestageID);
					break;
				case "KeySelectionMode":
					toReturn.Add(Relations.KeySelectionModeEntityUsingKeySelectionModeID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "BusinessRuleClause":
					toReturn.Add(Relations.BusinessRuleClauseEntityUsingBusinessruleResultID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_businessRuleClause", (!this.MarkedForDeletion?_businessRuleClause:null));
			info.AddValue("_alwaysFetchBusinessRuleClause", _alwaysFetchBusinessRuleClause);
			info.AddValue("_alreadyFetchedBusinessRuleClause", _alreadyFetchedBusinessRuleClause);
			info.AddValue("_userGroup", (!this.MarkedForDeletion?_userGroup:null));
			info.AddValue("_userGroupReturnsNewIfNotFound", _userGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserGroup", _alwaysFetchUserGroup);
			info.AddValue("_alreadyFetchedUserGroup", _alreadyFetchedUserGroup);
			info.AddValue("_businessRuleType", (!this.MarkedForDeletion?_businessRuleType:null));
			info.AddValue("_businessRuleTypeReturnsNewIfNotFound", _businessRuleTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleType", _alwaysFetchBusinessRuleType);
			info.AddValue("_alreadyFetchedBusinessRuleType", _alreadyFetchedBusinessRuleType);
			info.AddValue("_fareStage", (!this.MarkedForDeletion?_fareStage:null));
			info.AddValue("_fareStageReturnsNewIfNotFound", _fareStageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStage", _alwaysFetchFareStage);
			info.AddValue("_alreadyFetchedFareStage", _alreadyFetchedFareStage);
			info.AddValue("_keySelectionMode", (!this.MarkedForDeletion?_keySelectionMode:null));
			info.AddValue("_keySelectionModeReturnsNewIfNotFound", _keySelectionModeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchKeySelectionMode", _alwaysFetchKeySelectionMode);
			info.AddValue("_alreadyFetchedKeySelectionMode", _alreadyFetchedKeySelectionMode);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserGroup":
					_alreadyFetchedUserGroup = true;
					this.UserGroup = (AttributeValueEntity)entity;
					break;
				case "BusinessRuleType":
					_alreadyFetchedBusinessRuleType = true;
					this.BusinessRuleType = (BusinessRuleTypeEntity)entity;
					break;
				case "FareStage":
					_alreadyFetchedFareStage = true;
					this.FareStage = (FareStageEntity)entity;
					break;
				case "KeySelectionMode":
					_alreadyFetchedKeySelectionMode = true;
					this.KeySelectionMode = (KeySelectionModeEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "BusinessRuleClause":
					_alreadyFetchedBusinessRuleClause = true;
					if(entity!=null)
					{
						this.BusinessRuleClause.Add((BusinessRuleClauseEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserGroup":
					SetupSyncUserGroup(relatedEntity);
					break;
				case "BusinessRuleType":
					SetupSyncBusinessRuleType(relatedEntity);
					break;
				case "FareStage":
					SetupSyncFareStage(relatedEntity);
					break;
				case "KeySelectionMode":
					SetupSyncKeySelectionMode(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "BusinessRuleClause":
					_businessRuleClause.Add((BusinessRuleClauseEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserGroup":
					DesetupSyncUserGroup(false, true);
					break;
				case "BusinessRuleType":
					DesetupSyncBusinessRuleType(false, true);
					break;
				case "FareStage":
					DesetupSyncFareStage(false, true);
					break;
				case "KeySelectionMode":
					DesetupSyncKeySelectionMode(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "BusinessRuleClause":
					this.PerformRelatedEntityRemoval(_businessRuleClause, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userGroup!=null)
			{
				toReturn.Add(_userGroup);
			}
			if(_businessRuleType!=null)
			{
				toReturn.Add(_businessRuleType);
			}
			if(_fareStage!=null)
			{
				toReturn.Add(_fareStage);
			}
			if(_keySelectionMode!=null)
			{
				toReturn.Add(_keySelectionMode);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_businessRuleClause);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleResultID)
		{
			return FetchUsingPK(businessRuleResultID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleResultID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(businessRuleResultID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleResultID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(businessRuleResultID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(businessRuleResultID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BusinessRuleResultID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BusinessRuleResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleClauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleClauseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection GetMultiBusinessRuleClause(bool forceFetch)
		{
			return GetMultiBusinessRuleClause(forceFetch, _businessRuleClause.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleClauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleClauseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection GetMultiBusinessRuleClause(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleClause(forceFetch, _businessRuleClause.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleClauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection GetMultiBusinessRuleClause(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleClause(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleClauseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection GetMultiBusinessRuleClause(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleClause || forceFetch || _alwaysFetchBusinessRuleClause) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleClause);
				_businessRuleClause.SuppressClearInGetMulti=!forceFetch;
				_businessRuleClause.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleClause.GetMultiManyToOne(null, null, this, filter);
				_businessRuleClause.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleClause = true;
			}
			return _businessRuleClause;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleClause'. These settings will be taken into account
		/// when the property BusinessRuleClause is requested or GetMultiBusinessRuleClause is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleClause(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleClause.SortClauses=sortClauses;
			_businessRuleClause.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleUserGroup()
		{
			return GetSingleUserGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleUserGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserGroup || forceFetch || _alwaysFetchUserGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingUserGroupAttributeValue);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserGroupAttributeValue.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserGroup = newEntity;
				_alreadyFetchedUserGroup = fetchResult;
			}
			return _userGroup;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleTypeEntity' which is related to this entity.</returns>
		public BusinessRuleTypeEntity GetSingleBusinessRuleType()
		{
			return GetSingleBusinessRuleType(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleTypeEntity' which is related to this entity.</returns>
		public virtual BusinessRuleTypeEntity GetSingleBusinessRuleType(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleType || forceFetch || _alwaysFetchBusinessRuleType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
				BusinessRuleTypeEntity newEntity = new BusinessRuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessRuleTypeID);
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleType = newEntity;
				_alreadyFetchedBusinessRuleType = fetchResult;
			}
			return _businessRuleType;
		}


		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStage()
		{
			return GetSingleFareStage(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStage(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStage || forceFetch || _alwaysFetchFareStage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingDestinationFarestageID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DestinationFarestageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStage = newEntity;
				_alreadyFetchedFareStage = fetchResult;
			}
			return _fareStage;
		}


		/// <summary> Retrieves the related entity of type 'KeySelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'KeySelectionModeEntity' which is related to this entity.</returns>
		public KeySelectionModeEntity GetSingleKeySelectionMode()
		{
			return GetSingleKeySelectionMode(false);
		}

		/// <summary> Retrieves the related entity of type 'KeySelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'KeySelectionModeEntity' which is related to this entity.</returns>
		public virtual KeySelectionModeEntity GetSingleKeySelectionMode(bool forceFetch)
		{
			if( ( !_alreadyFetchedKeySelectionMode || forceFetch || _alwaysFetchKeySelectionMode) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.KeySelectionModeEntityUsingKeySelectionModeID);
				KeySelectionModeEntity newEntity = new KeySelectionModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.KeySelectionModeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (KeySelectionModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_keySelectionModeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.KeySelectionMode = newEntity;
				_alreadyFetchedKeySelectionMode = fetchResult;
			}
			return _keySelectionMode;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserGroup", _userGroup);
			toReturn.Add("BusinessRuleType", _businessRuleType);
			toReturn.Add("FareStage", _fareStage);
			toReturn.Add("KeySelectionMode", _keySelectionMode);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("BusinessRuleClause", _businessRuleClause);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="validator">The validator object for this BusinessRuleResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 businessRuleResultID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(businessRuleResultID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_businessRuleClause = new VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection();
			_businessRuleClause.SetContainingEntityInfo(this, "BusinessRuleResult");
			_userGroupReturnsNewIfNotFound = false;
			_businessRuleTypeReturnsNewIfNotFound = false;
			_fareStageReturnsNewIfNotFound = false;
			_keySelectionModeReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationFarestageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTicketName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GuiPanelName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InfoText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeySelectionModeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NxErrorCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OfflineCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodCalculationModeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceCalculationModeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupplementTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Surcharge", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ticket", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransferTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupAttributeValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Valid", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userGroup, new PropertyChangedEventHandler( OnUserGroupPropertyChanged ), "UserGroup", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.AttributeValueEntityUsingUserGroupAttributeValueStatic, true, signalRelatedEntity, "BusinessRuleResultUserGroup", resetFKFields, new int[] { (int)BusinessRuleResultFieldIndex.UserGroupAttributeValue } );		
			_userGroup = null;
		}
		
		/// <summary> setups the sync logic for member _userGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserGroup(IEntityCore relatedEntity)
		{
			if(_userGroup!=relatedEntity)
			{		
				DesetupSyncUserGroup(true, true);
				_userGroup = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userGroup, new PropertyChangedEventHandler( OnUserGroupPropertyChanged ), "UserGroup", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.AttributeValueEntityUsingUserGroupAttributeValueStatic, true, ref _alreadyFetchedUserGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleType, new PropertyChangedEventHandler( OnBusinessRuleTypePropertyChanged ), "BusinessRuleType", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic, true, signalRelatedEntity, "BusinessRuleResult", resetFKFields, new int[] { (int)BusinessRuleResultFieldIndex.BusinessRuleTypeID } );		
			_businessRuleType = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleType(IEntityCore relatedEntity)
		{
			if(_businessRuleType!=relatedEntity)
			{		
				DesetupSyncBusinessRuleType(true, true);
				_businessRuleType = (BusinessRuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleType, new PropertyChangedEventHandler( OnBusinessRuleTypePropertyChanged ), "BusinessRuleType", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic, true, ref _alreadyFetchedBusinessRuleType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.FareStageEntityUsingDestinationFarestageIDStatic, true, signalRelatedEntity, "BusinessRuleResult", resetFKFields, new int[] { (int)BusinessRuleResultFieldIndex.DestinationFarestageID } );		
			_fareStage = null;
		}
		
		/// <summary> setups the sync logic for member _fareStage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStage(IEntityCore relatedEntity)
		{
			if(_fareStage!=relatedEntity)
			{		
				DesetupSyncFareStage(true, true);
				_fareStage = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.FareStageEntityUsingDestinationFarestageIDStatic, true, ref _alreadyFetchedFareStage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _keySelectionMode</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncKeySelectionMode(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _keySelectionMode, new PropertyChangedEventHandler( OnKeySelectionModePropertyChanged ), "KeySelectionMode", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.KeySelectionModeEntityUsingKeySelectionModeIDStatic, true, signalRelatedEntity, "BusinessRuleResult", resetFKFields, new int[] { (int)BusinessRuleResultFieldIndex.KeySelectionModeID } );		
			_keySelectionMode = null;
		}
		
		/// <summary> setups the sync logic for member _keySelectionMode</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncKeySelectionMode(IEntityCore relatedEntity)
		{
			if(_keySelectionMode!=relatedEntity)
			{		
				DesetupSyncKeySelectionMode(true, true);
				_keySelectionMode = (KeySelectionModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _keySelectionMode, new PropertyChangedEventHandler( OnKeySelectionModePropertyChanged ), "KeySelectionMode", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.KeySelectionModeEntityUsingKeySelectionModeIDStatic, true, ref _alreadyFetchedKeySelectionMode, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnKeySelectionModePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "BusinessRuleResult", resetFKFields, new int[] { (int)BusinessRuleResultFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticBusinessRuleResultRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="businessRuleResultID">PK value for BusinessRuleResult which data should be fetched into this BusinessRuleResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 businessRuleResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BusinessRuleResultFieldIndex.BusinessRuleResultID].ForcedCurrentValueWrite(businessRuleResultID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBusinessRuleResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BusinessRuleResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BusinessRuleResultRelations Relations
		{
			get	{ return new BusinessRuleResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleClause' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleClause
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleClause")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.BusinessRuleClauseEntity, 0, null, null, null, "BusinessRuleClause", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("UserGroup")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "UserGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleTypeCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleType")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, 0, null, null, null, "BusinessRuleType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStage")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeySelectionMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKeySelectionMode
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeySelectionModeCollection(), (IEntityRelation)GetRelationsForField("KeySelectionMode")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.KeySelectionModeEntity, 0, null, null, null, "KeySelectionMode", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BusinessRuleResultID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."BUSINESSRULERESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 BusinessRuleResultID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleResultFieldIndex.BusinessRuleResultID, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.BusinessRuleResultID, value, true); }
		}

		/// <summary> The BusinessRuleTypeID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."BUSINESSRULETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BusinessRuleTypeID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleResultFieldIndex.BusinessRuleTypeID, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.BusinessRuleTypeID, value, true); }
		}

		/// <summary> The DestinationFarestageID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."DESTINATIONFARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DestinationFarestageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.DestinationFarestageID, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.DestinationFarestageID, value, true); }
		}

		/// <summary> The ExternalTicketName property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."EXTERNALTICKETNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalTicketName
		{
			get { return (System.String)GetValue((int)BusinessRuleResultFieldIndex.ExternalTicketName, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.ExternalTicketName, value, true); }
		}

		/// <summary> The GuiPanelName property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."GUIPANELNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String GuiPanelName
		{
			get { return (System.String)GetValue((int)BusinessRuleResultFieldIndex.GuiPanelName, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.GuiPanelName, value, true); }
		}

		/// <summary> The InfoText property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."INFOTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InfoText
		{
			get { return (System.String)GetValue((int)BusinessRuleResultFieldIndex.InfoText, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.InfoText, value, true); }
		}

		/// <summary> The KeySelectionModeID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."KEYSELECTIONMODEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Nullable<System.Int64> KeySelectionModeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.KeySelectionModeID, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.KeySelectionModeID, value, true); }
		}

		/// <summary> The LineNo property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."LINENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.LineNo, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.LineNo, value, true); }
		}

		/// <summary> The NxErrorCode property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."NXERRORCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NxErrorCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.NxErrorCode, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.NxErrorCode, value, true); }
		}

		/// <summary> The OfflineCredit property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."OFFLINECREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OfflineCredit
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.OfflineCredit, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.OfflineCredit, value, true); }
		}

		/// <summary> The PeriodCalculationModeID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."PERIODCALCULATIONMODEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TariffPeriodCalculationMode> PeriodCalculationModeID
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TariffPeriodCalculationMode>)GetValue((int)BusinessRuleResultFieldIndex.PeriodCalculationModeID, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.PeriodCalculationModeID, value, true); }
		}

		/// <summary> The PriceCalculationModeID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."PRICECALCULATIONMODEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PriceCalculationModeID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleResultFieldIndex.PriceCalculationModeID, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.PriceCalculationModeID, value, true); }
		}

		/// <summary> The ResultName property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."RESULTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResultName
		{
			get { return (System.String)GetValue((int)BusinessRuleResultFieldIndex.ResultName, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.ResultName, value, true); }
		}

		/// <summary> The SupplementTicketInternalNumber property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."SUPPLEMENTTICKET"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SupplementTicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.SupplementTicketInternalNumber, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.SupplementTicketInternalNumber, value, true); }
		}

		/// <summary> The Surcharge property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."SURCHARGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Surcharge
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.Surcharge, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.Surcharge, value, true); }
		}

		/// <summary> The TariffID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleResultFieldIndex.TariffID, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.TariffID, value, true); }
		}

		/// <summary> The Ticket property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."TICKET"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Ticket
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.Ticket, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.Ticket, value, true); }
		}

		/// <summary> The TransactionTypeID property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."TRANSACTIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.TransactionTypeID, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.TransactionTypeID, value, true); }
		}

		/// <summary> The TransferTime property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."TRANSFERTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransferTime
		{
			get { return (System.Int64)GetValue((int)BusinessRuleResultFieldIndex.TransferTime, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.TransferTime, value, true); }
		}

		/// <summary> The UserGroupAttributeValue property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."USERGROUPATTRIBUTEVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserGroupAttributeValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleResultFieldIndex.UserGroupAttributeValue, false); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.UserGroupAttributeValue, value, true); }
		}

		/// <summary> The Valid property of the Entity BusinessRuleResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULERESULT"."VALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Valid
		{
			get { return (System.Boolean)GetValue((int)BusinessRuleResultFieldIndex.Valid, true); }
			set	{ SetValue((int)BusinessRuleResultFieldIndex.Valid, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleClauseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleClause()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleClauseCollection BusinessRuleClause
		{
			get	{ return GetMultiBusinessRuleClause(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleClause. When set to true, BusinessRuleClause is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleClause is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleClause(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleClause
		{
			get	{ return _alwaysFetchBusinessRuleClause; }
			set	{ _alwaysFetchBusinessRuleClause = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleClause already has been fetched. Setting this property to false when BusinessRuleClause has been fetched
		/// will clear the BusinessRuleClause collection well. Setting this property to true while BusinessRuleClause hasn't been fetched disables lazy loading for BusinessRuleClause</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleClause
		{
			get { return _alreadyFetchedBusinessRuleClause;}
			set 
			{
				if(_alreadyFetchedBusinessRuleClause && !value && (_businessRuleClause != null))
				{
					_businessRuleClause.Clear();
				}
				_alreadyFetchedBusinessRuleClause = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity UserGroup
		{
			get	{ return GetSingleUserGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleResultUserGroup", "UserGroup", _userGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserGroup. When set to true, UserGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserGroup is accessed. You can always execute a forced fetch by calling GetSingleUserGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserGroup
		{
			get	{ return _alwaysFetchUserGroup; }
			set	{ _alwaysFetchUserGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserGroup already has been fetched. Setting this property to false when UserGroup has been fetched
		/// will set UserGroup to null as well. Setting this property to true while UserGroup hasn't been fetched disables lazy loading for UserGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserGroup
		{
			get { return _alreadyFetchedUserGroup;}
			set 
			{
				if(_alreadyFetchedUserGroup && !value)
				{
					this.UserGroup = null;
				}
				_alreadyFetchedUserGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserGroup is not found
		/// in the database. When set to true, UserGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserGroupReturnsNewIfNotFound
		{
			get	{ return _userGroupReturnsNewIfNotFound; }
			set { _userGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleTypeEntity BusinessRuleType
		{
			get	{ return GetSingleBusinessRuleType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleResult", "BusinessRuleType", _businessRuleType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleType. When set to true, BusinessRuleType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleType is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleType
		{
			get	{ return _alwaysFetchBusinessRuleType; }
			set	{ _alwaysFetchBusinessRuleType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleType already has been fetched. Setting this property to false when BusinessRuleType has been fetched
		/// will set BusinessRuleType to null as well. Setting this property to true while BusinessRuleType hasn't been fetched disables lazy loading for BusinessRuleType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleType
		{
			get { return _alreadyFetchedBusinessRuleType;}
			set 
			{
				if(_alreadyFetchedBusinessRuleType && !value)
				{
					this.BusinessRuleType = null;
				}
				_alreadyFetchedBusinessRuleType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleType is not found
		/// in the database. When set to true, BusinessRuleType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleTypeReturnsNewIfNotFound
		{
			get	{ return _businessRuleTypeReturnsNewIfNotFound; }
			set { _businessRuleTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStage
		{
			get	{ return GetSingleFareStage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleResult", "FareStage", _fareStage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStage. When set to true, FareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStage is accessed. You can always execute a forced fetch by calling GetSingleFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStage
		{
			get	{ return _alwaysFetchFareStage; }
			set	{ _alwaysFetchFareStage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStage already has been fetched. Setting this property to false when FareStage has been fetched
		/// will set FareStage to null as well. Setting this property to true while FareStage hasn't been fetched disables lazy loading for FareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStage
		{
			get { return _alreadyFetchedFareStage;}
			set 
			{
				if(_alreadyFetchedFareStage && !value)
				{
					this.FareStage = null;
				}
				_alreadyFetchedFareStage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStage is not found
		/// in the database. When set to true, FareStage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageReturnsNewIfNotFound
		{
			get	{ return _fareStageReturnsNewIfNotFound; }
			set { _fareStageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'KeySelectionModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleKeySelectionMode()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual KeySelectionModeEntity KeySelectionMode
		{
			get	{ return GetSingleKeySelectionMode(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncKeySelectionMode(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleResult", "KeySelectionMode", _keySelectionMode, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for KeySelectionMode. When set to true, KeySelectionMode is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time KeySelectionMode is accessed. You can always execute a forced fetch by calling GetSingleKeySelectionMode(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKeySelectionMode
		{
			get	{ return _alwaysFetchKeySelectionMode; }
			set	{ _alwaysFetchKeySelectionMode = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property KeySelectionMode already has been fetched. Setting this property to false when KeySelectionMode has been fetched
		/// will set KeySelectionMode to null as well. Setting this property to true while KeySelectionMode hasn't been fetched disables lazy loading for KeySelectionMode</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKeySelectionMode
		{
			get { return _alreadyFetchedKeySelectionMode;}
			set 
			{
				if(_alreadyFetchedKeySelectionMode && !value)
				{
					this.KeySelectionMode = null;
				}
				_alreadyFetchedKeySelectionMode = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property KeySelectionMode is not found
		/// in the database. When set to true, KeySelectionMode will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool KeySelectionModeReturnsNewIfNotFound
		{
			get	{ return _keySelectionModeReturnsNewIfNotFound; }
			set { _keySelectionModeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleResult", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
