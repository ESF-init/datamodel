﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WorkItem'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class WorkItemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection	_workItemHistory;
		private bool	_alwaysFetchWorkItemHistory, _alreadyFetchedWorkItemHistory;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private WorkItemSubjectEntity _workItemSubject;
		private bool	_alwaysFetchWorkItemSubject, _alreadyFetchedWorkItemSubject, _workItemSubjectReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name WorkItemSubject</summary>
			public static readonly string WorkItemSubject = "WorkItemSubject";
			/// <summary>Member name WorkItemHistory</summary>
			public static readonly string WorkItemHistory = "WorkItemHistory";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WorkItemEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public WorkItemEntity() :base("WorkItemEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		public WorkItemEntity(System.Int64 workItemID):base("WorkItemEntity")
		{
			InitClassFetch(workItemID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WorkItemEntity(System.Int64 workItemID, IPrefetchPath prefetchPathToUse):base("WorkItemEntity")
		{
			InitClassFetch(workItemID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="validator">The custom validator object for this WorkItemEntity</param>
		public WorkItemEntity(System.Int64 workItemID, IValidator validator):base("WorkItemEntity")
		{
			InitClassFetch(workItemID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WorkItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_workItemHistory = (VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection)info.GetValue("_workItemHistory", typeof(VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection));
			_alwaysFetchWorkItemHistory = info.GetBoolean("_alwaysFetchWorkItemHistory");
			_alreadyFetchedWorkItemHistory = info.GetBoolean("_alreadyFetchedWorkItemHistory");
			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_workItemSubject = (WorkItemSubjectEntity)info.GetValue("_workItemSubject", typeof(WorkItemSubjectEntity));
			if(_workItemSubject!=null)
			{
				_workItemSubject.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_workItemSubjectReturnsNewIfNotFound = info.GetBoolean("_workItemSubjectReturnsNewIfNotFound");
			_alwaysFetchWorkItemSubject = info.GetBoolean("_alwaysFetchWorkItemSubject");
			_alreadyFetchedWorkItemSubject = info.GetBoolean("_alreadyFetchedWorkItemSubject");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((WorkItemFieldIndex)fieldIndex)
			{
				case WorkItemFieldIndex.Assignee:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				case WorkItemFieldIndex.WorkItemSubjectID:
					DesetupSyncWorkItemSubject(true, false);
					_alreadyFetchedWorkItemSubject = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedWorkItemHistory = (_workItemHistory.Count > 0);
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedWorkItemSubject = (_workItemSubject != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingAssignee);
					break;
				case "WorkItemSubject":
					toReturn.Add(Relations.WorkItemSubjectEntityUsingWorkItemSubjectID);
					break;
				case "WorkItemHistory":
					toReturn.Add(Relations.WorkItemHistoryEntityUsingWorkItemID);
					break;
				default:
					break;				
			}
			return toReturn;
		}

		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter()
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WorkItemEntity", false);
		}
		
		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter(bool negate)
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("WorkItemEntity", negate);
		}

		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_workItemHistory", (!this.MarkedForDeletion?_workItemHistory:null));
			info.AddValue("_alwaysFetchWorkItemHistory", _alwaysFetchWorkItemHistory);
			info.AddValue("_alreadyFetchedWorkItemHistory", _alreadyFetchedWorkItemHistory);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_workItemSubject", (!this.MarkedForDeletion?_workItemSubject:null));
			info.AddValue("_workItemSubjectReturnsNewIfNotFound", _workItemSubjectReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWorkItemSubject", _alwaysFetchWorkItemSubject);
			info.AddValue("_alreadyFetchedWorkItemSubject", _alreadyFetchedWorkItemSubject);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "WorkItemSubject":
					_alreadyFetchedWorkItemSubject = true;
					this.WorkItemSubject = (WorkItemSubjectEntity)entity;
					break;
				case "WorkItemHistory":
					_alreadyFetchedWorkItemHistory = true;
					if(entity!=null)
					{
						this.WorkItemHistory.Add((WorkItemHistoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "WorkItemSubject":
					SetupSyncWorkItemSubject(relatedEntity);
					break;
				case "WorkItemHistory":
					_workItemHistory.Add((WorkItemHistoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "WorkItemSubject":
					DesetupSyncWorkItemSubject(false, true);
					break;
				case "WorkItemHistory":
					this.PerformRelatedEntityRemoval(_workItemHistory, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_workItemSubject!=null)
			{
				toReturn.Add(_workItemSubject);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_workItemHistory);

			return toReturn;
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  WorkItemEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 workItemID, Context contextToUse)
		{
			return FetchPolymorphic(transactionToUse, workItemID, contextToUse, null);
		}
				
		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  WorkItemEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 workItemID, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.WorkItemEntity);
			fields.ForcedValueWrite((int)WorkItemFieldIndex.WorkItemID, workItemID);
			return (WorkItemEntity)new WorkItemDAO().FetchExistingPolymorphic(transactionToUse, fields, contextToUse, excludedIncludedFields);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemID)
		{
			return FetchUsingPK(workItemID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(workItemID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(workItemID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(workItemID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.WorkItemID, null, null, null);
		}

		/// <summary>Determines whether this entity is a subType of the entity represented by the passed in enum value, which represents a value in the VarioSL.Entities.EntityType enum</summary>
		/// <param name="typeOfEntity">Type of entity.</param>
		/// <returns>true if the passed in type is a supertype of this entity, otherwise false</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckIfIsSubTypeOf(int typeOfEntity)
		{
			return InheritanceInfoProviderSingleton.GetInstance().CheckIfIsSubTypeOf("WorkItemEntity", ((VarioSL.Entities.EntityType)typeOfEntity).ToString());
		}
				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WorkItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'WorkItemHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'WorkItemHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection GetMultiWorkItemHistory(bool forceFetch)
		{
			return GetMultiWorkItemHistory(forceFetch, _workItemHistory.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'WorkItemHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection GetMultiWorkItemHistory(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkItemHistory(forceFetch, _workItemHistory.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection GetMultiWorkItemHistory(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkItemHistory(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'WorkItemHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection GetMultiWorkItemHistory(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkItemHistory || forceFetch || _alwaysFetchWorkItemHistory) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workItemHistory);
				_workItemHistory.SuppressClearInGetMulti=!forceFetch;
				_workItemHistory.EntityFactoryToUse = entityFactoryToUse;
				_workItemHistory.GetMultiManyToOne(this, filter);
				_workItemHistory.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkItemHistory = true;
			}
			return _workItemHistory;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkItemHistory'. These settings will be taken into account
		/// when the property WorkItemHistory is requested or GetMultiWorkItemHistory is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkItemHistory(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workItemHistory.SortClauses=sortClauses;
			_workItemHistory.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingAssignee);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Assignee.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'WorkItemSubjectEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WorkItemSubjectEntity' which is related to this entity.</returns>
		public WorkItemSubjectEntity GetSingleWorkItemSubject()
		{
			return GetSingleWorkItemSubject(false);
		}

		/// <summary> Retrieves the related entity of type 'WorkItemSubjectEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WorkItemSubjectEntity' which is related to this entity.</returns>
		public virtual WorkItemSubjectEntity GetSingleWorkItemSubject(bool forceFetch)
		{
			if( ( !_alreadyFetchedWorkItemSubject || forceFetch || _alwaysFetchWorkItemSubject) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WorkItemSubjectEntityUsingWorkItemSubjectID);
				WorkItemSubjectEntity newEntity = new WorkItemSubjectEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.WorkItemSubjectID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (WorkItemSubjectEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_workItemSubjectReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WorkItemSubject = newEntity;
				_alreadyFetchedWorkItemSubject = fetchResult;
			}
			return _workItemSubject;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserList", _userList);
			toReturn.Add("WorkItemSubject", _workItemSubject);
			toReturn.Add("WorkItemHistory", _workItemHistory);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)WorkItemFieldIndex.TypeDiscriminator, "WorkItem");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="validator">The validator object for this WorkItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 workItemID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(workItemID, prefetchPathToUse, null, null);
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)WorkItemFieldIndex.TypeDiscriminator, "WorkItem");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_workItemHistory = new VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection();
			_workItemHistory.SetContainingEntityInfo(this, "WorkItem");
			_userListReturnsNewIfNotFound = false;
			_workItemSubjectReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Assigned", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Assignee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Executed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Memo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Source", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeDiscriminator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemSubjectID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticWorkItemRelations.UserListEntityUsingAssigneeStatic, true, signalRelatedEntity, "WorkItems", resetFKFields, new int[] { (int)WorkItemFieldIndex.Assignee } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticWorkItemRelations.UserListEntityUsingAssigneeStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _workItemSubject</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWorkItemSubject(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _workItemSubject, new PropertyChangedEventHandler( OnWorkItemSubjectPropertyChanged ), "WorkItemSubject", VarioSL.Entities.RelationClasses.StaticWorkItemRelations.WorkItemSubjectEntityUsingWorkItemSubjectIDStatic, true, signalRelatedEntity, "WorkItems", resetFKFields, new int[] { (int)WorkItemFieldIndex.WorkItemSubjectID } );		
			_workItemSubject = null;
		}
		
		/// <summary> setups the sync logic for member _workItemSubject</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWorkItemSubject(IEntityCore relatedEntity)
		{
			if(_workItemSubject!=relatedEntity)
			{		
				DesetupSyncWorkItemSubject(true, true);
				_workItemSubject = (WorkItemSubjectEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _workItemSubject, new PropertyChangedEventHandler( OnWorkItemSubjectPropertyChanged ), "WorkItemSubject", VarioSL.Entities.RelationClasses.StaticWorkItemRelations.WorkItemSubjectEntityUsingWorkItemSubjectIDStatic, true, ref _alreadyFetchedWorkItemSubject, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWorkItemSubjectPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="workItemID">PK value for WorkItem which data should be fetched into this WorkItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 workItemID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)WorkItemFieldIndex.WorkItemID].ForcedCurrentValueWrite(workItemID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWorkItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WorkItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static WorkItemRelations Relations
		{
			get	{ return new WorkItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WorkItemHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItemHistory
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection(), (IEntityRelation)GetRelationsForField("WorkItemHistory")[0], (int)VarioSL.Entities.EntityType.WorkItemEntity, (int)VarioSL.Entities.EntityType.WorkItemHistoryEntity, 0, null, null, null, "WorkItemHistory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.WorkItemEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WorkItemSubject'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItemSubject
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkItemSubjectCollection(), (IEntityRelation)GetRelationsForField("WorkItemSubject")[0], (int)VarioSL.Entities.EntityType.WorkItemEntity, (int)VarioSL.Entities.EntityType.WorkItemSubjectEntity, 0, null, null, null, "WorkItemSubject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Assigned property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."ASSIGNED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Assigned
		{
			get { return (System.DateTime)GetValue((int)WorkItemFieldIndex.Assigned, true); }
			set	{ SetValue((int)WorkItemFieldIndex.Assigned, value, true); }
		}

		/// <summary> The Assignee property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."ASSIGNEE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Assignee
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemFieldIndex.Assignee, false); }
			set	{ SetValue((int)WorkItemFieldIndex.Assignee, value, true); }
		}

		/// <summary> The Created property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."CREATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Created
		{
			get { return (System.DateTime)GetValue((int)WorkItemFieldIndex.Created, true); }
			set	{ SetValue((int)WorkItemFieldIndex.Created, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."CREATEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CreatedBy
		{
			get { return (System.String)GetValue((int)WorkItemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)WorkItemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Executed property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."EXECUTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Executed
		{
			get { return (System.DateTime)GetValue((int)WorkItemFieldIndex.Executed, true); }
			set	{ SetValue((int)WorkItemFieldIndex.Executed, value, true); }
		}

		/// <summary> The LastModified property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)WorkItemFieldIndex.LastModified, true); }
			set	{ SetValue((int)WorkItemFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)WorkItemFieldIndex.LastUser, true); }
			set	{ SetValue((int)WorkItemFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Memo property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."MEMO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Memo
		{
			get { return (System.String)GetValue((int)WorkItemFieldIndex.Memo, true); }
			set	{ SetValue((int)WorkItemFieldIndex.Memo, value, true); }
		}

		/// <summary> The Source property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."SOURCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.Source> Source
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.Source>)GetValue((int)WorkItemFieldIndex.Source, false); }
			set	{ SetValue((int)WorkItemFieldIndex.Source, value, true); }
		}

		/// <summary> The State property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.WorkItemState> State
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.WorkItemState>)GetValue((int)WorkItemFieldIndex.State, false); }
			set	{ SetValue((int)WorkItemFieldIndex.State, value, true); }
		}

		/// <summary> The Title property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."TITLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)WorkItemFieldIndex.Title, true); }
			set	{ SetValue((int)WorkItemFieldIndex.Title, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)WorkItemFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)WorkItemFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TypeDiscriminator property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."TYPEDISCRIMINATOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TypeDiscriminator
		{
			get { return (System.String)GetValue((int)WorkItemFieldIndex.TypeDiscriminator, true); }

		}

		/// <summary> The WorkItemID property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."WORKITEMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 WorkItemID
		{
			get { return (System.Int64)GetValue((int)WorkItemFieldIndex.WorkItemID, true); }
			set	{ SetValue((int)WorkItemFieldIndex.WorkItemID, value, true); }
		}

		/// <summary> The WorkItemSubjectID property of the Entity WorkItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEM"."WORKITEMSUBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> WorkItemSubjectID
		{
			get { return (Nullable<System.Int64>)GetValue((int)WorkItemFieldIndex.WorkItemSubjectID, false); }
			set	{ SetValue((int)WorkItemFieldIndex.WorkItemSubjectID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'WorkItemHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkItemHistory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.WorkItemHistoryCollection WorkItemHistory
		{
			get	{ return GetMultiWorkItemHistory(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItemHistory. When set to true, WorkItemHistory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItemHistory is accessed. You can always execute/ a forced fetch by calling GetMultiWorkItemHistory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItemHistory
		{
			get	{ return _alwaysFetchWorkItemHistory; }
			set	{ _alwaysFetchWorkItemHistory = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItemHistory already has been fetched. Setting this property to false when WorkItemHistory has been fetched
		/// will clear the WorkItemHistory collection well. Setting this property to true while WorkItemHistory hasn't been fetched disables lazy loading for WorkItemHistory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItemHistory
		{
			get { return _alreadyFetchedWorkItemHistory;}
			set 
			{
				if(_alreadyFetchedWorkItemHistory && !value && (_workItemHistory != null))
				{
					_workItemHistory.Clear();
				}
				_alreadyFetchedWorkItemHistory = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WorkItems", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'WorkItemSubjectEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWorkItemSubject()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual WorkItemSubjectEntity WorkItemSubject
		{
			get	{ return GetSingleWorkItemSubject(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWorkItemSubject(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WorkItems", "WorkItemSubject", _workItemSubject, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItemSubject. When set to true, WorkItemSubject is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItemSubject is accessed. You can always execute a forced fetch by calling GetSingleWorkItemSubject(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItemSubject
		{
			get	{ return _alwaysFetchWorkItemSubject; }
			set	{ _alwaysFetchWorkItemSubject = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItemSubject already has been fetched. Setting this property to false when WorkItemSubject has been fetched
		/// will set WorkItemSubject to null as well. Setting this property to true while WorkItemSubject hasn't been fetched disables lazy loading for WorkItemSubject</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItemSubject
		{
			get { return _alreadyFetchedWorkItemSubject;}
			set 
			{
				if(_alreadyFetchedWorkItemSubject && !value)
				{
					this.WorkItemSubject = null;
				}
				_alreadyFetchedWorkItemSubject = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WorkItemSubject is not found
		/// in the database. When set to true, WorkItemSubject will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool WorkItemSubjectReturnsNewIfNotFound
		{
			get	{ return _workItemSubjectReturnsNewIfNotFound; }
			set { _workItemSubjectReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.TargetPerEntityHierarchy;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.WorkItemEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
