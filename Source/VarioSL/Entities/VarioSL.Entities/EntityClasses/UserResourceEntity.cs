﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserResource'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UserResourceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.UserGroupRightCollection	_userGroupRights;
		private bool	_alwaysFetchUserGroupRights, _alreadyFetchedUserGroupRights;
		private VarioSL.Entities.CollectionClasses.ClaimCollection	_claims;
		private bool	_alwaysFetchClaims, _alreadyFetchedClaims;
		private VarioSL.Entities.CollectionClasses.ReportCollection	_reports;
		private bool	_alwaysFetchReports, _alreadyFetchedReports;
		private VarioSL.Entities.CollectionClasses.ReportCategoryCollection	_reportCategories;
		private bool	_alwaysFetchReportCategories, _alreadyFetchedReportCategories;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserGroupRights</summary>
			public static readonly string UserGroupRights = "UserGroupRights";
			/// <summary>Member name Claims</summary>
			public static readonly string Claims = "Claims";
			/// <summary>Member name Reports</summary>
			public static readonly string Reports = "Reports";
			/// <summary>Member name ReportCategories</summary>
			public static readonly string ReportCategories = "ReportCategories";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserResourceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UserResourceEntity() :base("UserResourceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		public UserResourceEntity(System.String resourceID):base("UserResourceEntity")
		{
			InitClassFetch(resourceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserResourceEntity(System.String resourceID, IPrefetchPath prefetchPathToUse):base("UserResourceEntity")
		{
			InitClassFetch(resourceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="validator">The custom validator object for this UserResourceEntity</param>
		public UserResourceEntity(System.String resourceID, IValidator validator):base("UserResourceEntity")
		{
			InitClassFetch(resourceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserResourceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userGroupRights = (VarioSL.Entities.CollectionClasses.UserGroupRightCollection)info.GetValue("_userGroupRights", typeof(VarioSL.Entities.CollectionClasses.UserGroupRightCollection));
			_alwaysFetchUserGroupRights = info.GetBoolean("_alwaysFetchUserGroupRights");
			_alreadyFetchedUserGroupRights = info.GetBoolean("_alreadyFetchedUserGroupRights");

			_claims = (VarioSL.Entities.CollectionClasses.ClaimCollection)info.GetValue("_claims", typeof(VarioSL.Entities.CollectionClasses.ClaimCollection));
			_alwaysFetchClaims = info.GetBoolean("_alwaysFetchClaims");
			_alreadyFetchedClaims = info.GetBoolean("_alreadyFetchedClaims");

			_reports = (VarioSL.Entities.CollectionClasses.ReportCollection)info.GetValue("_reports", typeof(VarioSL.Entities.CollectionClasses.ReportCollection));
			_alwaysFetchReports = info.GetBoolean("_alwaysFetchReports");
			_alreadyFetchedReports = info.GetBoolean("_alreadyFetchedReports");

			_reportCategories = (VarioSL.Entities.CollectionClasses.ReportCategoryCollection)info.GetValue("_reportCategories", typeof(VarioSL.Entities.CollectionClasses.ReportCategoryCollection));
			_alwaysFetchReportCategories = info.GetBoolean("_alwaysFetchReportCategories");
			_alreadyFetchedReportCategories = info.GetBoolean("_alreadyFetchedReportCategories");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserGroupRights = (_userGroupRights.Count > 0);
			_alreadyFetchedClaims = (_claims.Count > 0);
			_alreadyFetchedReports = (_reports.Count > 0);
			_alreadyFetchedReportCategories = (_reportCategories.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserGroupRights":
					toReturn.Add(Relations.UserGroupRightEntityUsingResourceID);
					break;
				case "Claims":
					toReturn.Add(Relations.ClaimEntityUsingResourceID);
					break;
				case "Reports":
					toReturn.Add(Relations.ReportEntityUsingResourceID);
					break;
				case "ReportCategories":
					toReturn.Add(Relations.ReportCategoryEntityUsingResourceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userGroupRights", (!this.MarkedForDeletion?_userGroupRights:null));
			info.AddValue("_alwaysFetchUserGroupRights", _alwaysFetchUserGroupRights);
			info.AddValue("_alreadyFetchedUserGroupRights", _alreadyFetchedUserGroupRights);
			info.AddValue("_claims", (!this.MarkedForDeletion?_claims:null));
			info.AddValue("_alwaysFetchClaims", _alwaysFetchClaims);
			info.AddValue("_alreadyFetchedClaims", _alreadyFetchedClaims);
			info.AddValue("_reports", (!this.MarkedForDeletion?_reports:null));
			info.AddValue("_alwaysFetchReports", _alwaysFetchReports);
			info.AddValue("_alreadyFetchedReports", _alreadyFetchedReports);
			info.AddValue("_reportCategories", (!this.MarkedForDeletion?_reportCategories:null));
			info.AddValue("_alwaysFetchReportCategories", _alwaysFetchReportCategories);
			info.AddValue("_alreadyFetchedReportCategories", _alreadyFetchedReportCategories);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserGroupRights":
					_alreadyFetchedUserGroupRights = true;
					if(entity!=null)
					{
						this.UserGroupRights.Add((UserGroupRightEntity)entity);
					}
					break;
				case "Claims":
					_alreadyFetchedClaims = true;
					if(entity!=null)
					{
						this.Claims.Add((ClaimEntity)entity);
					}
					break;
				case "Reports":
					_alreadyFetchedReports = true;
					if(entity!=null)
					{
						this.Reports.Add((ReportEntity)entity);
					}
					break;
				case "ReportCategories":
					_alreadyFetchedReportCategories = true;
					if(entity!=null)
					{
						this.ReportCategories.Add((ReportCategoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserGroupRights":
					_userGroupRights.Add((UserGroupRightEntity)relatedEntity);
					break;
				case "Claims":
					_claims.Add((ClaimEntity)relatedEntity);
					break;
				case "Reports":
					_reports.Add((ReportEntity)relatedEntity);
					break;
				case "ReportCategories":
					_reportCategories.Add((ReportCategoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserGroupRights":
					this.PerformRelatedEntityRemoval(_userGroupRights, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Claims":
					this.PerformRelatedEntityRemoval(_claims, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Reports":
					this.PerformRelatedEntityRemoval(_reports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportCategories":
					this.PerformRelatedEntityRemoval(_reportCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_userGroupRights);
			toReturn.Add(_claims);
			toReturn.Add(_reports);
			toReturn.Add(_reportCategories);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String resourceID)
		{
			return FetchUsingPK(resourceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String resourceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(resourceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(resourceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.String resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(resourceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ResourceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserResourceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserGroupRightEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch)
		{
			return GetMultiUserGroupRights(forceFetch, _userGroupRights.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserGroupRightEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserGroupRights(forceFetch, _userGroupRights.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserGroupRights(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserGroupRights || forceFetch || _alwaysFetchUserGroupRights) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userGroupRights);
				_userGroupRights.SuppressClearInGetMulti=!forceFetch;
				_userGroupRights.EntityFactoryToUse = entityFactoryToUse;
				_userGroupRights.GetMultiManyToOne(null, this, filter);
				_userGroupRights.SuppressClearInGetMulti=false;
				_alreadyFetchedUserGroupRights = true;
			}
			return _userGroupRights;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserGroupRights'. These settings will be taken into account
		/// when the property UserGroupRights is requested or GetMultiUserGroupRights is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserGroupRights(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userGroupRights.SortClauses=sortClauses;
			_userGroupRights.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClaimEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClaimEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClaimCollection GetMultiClaims(bool forceFetch)
		{
			return GetMultiClaims(forceFetch, _claims.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClaimEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClaimEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClaimCollection GetMultiClaims(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClaims(forceFetch, _claims.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClaimEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClaimCollection GetMultiClaims(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClaims(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClaimEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClaimCollection GetMultiClaims(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClaims || forceFetch || _alwaysFetchClaims) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_claims);
				_claims.SuppressClearInGetMulti=!forceFetch;
				_claims.EntityFactoryToUse = entityFactoryToUse;
				_claims.GetMultiManyToOne(this, filter);
				_claims.SuppressClearInGetMulti=false;
				_alreadyFetchedClaims = true;
			}
			return _claims;
		}

		/// <summary> Sets the collection parameters for the collection for 'Claims'. These settings will be taken into account
		/// when the property Claims is requested or GetMultiClaims is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClaims(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_claims.SortClauses=sortClauses;
			_claims.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReports || forceFetch || _alwaysFetchReports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reports);
				_reports.SuppressClearInGetMulti=!forceFetch;
				_reports.EntityFactoryToUse = entityFactoryToUse;
				_reports.GetMultiManyToOne(this, null, null, null, filter);
				_reports.SuppressClearInGetMulti=false;
				_alreadyFetchedReports = true;
			}
			return _reports;
		}

		/// <summary> Sets the collection parameters for the collection for 'Reports'. These settings will be taken into account
		/// when the property Reports is requested or GetMultiReports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reports.SortClauses=sortClauses;
			_reports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch)
		{
			return GetMultiReportCategories(forceFetch, _reportCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportCategories(forceFetch, _reportCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportCategories || forceFetch || _alwaysFetchReportCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportCategories);
				_reportCategories.SuppressClearInGetMulti=!forceFetch;
				_reportCategories.EntityFactoryToUse = entityFactoryToUse;
				_reportCategories.GetMultiManyToOne(this, null, null, filter);
				_reportCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedReportCategories = true;
			}
			return _reportCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportCategories'. These settings will be taken into account
		/// when the property ReportCategories is requested or GetMultiReportCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportCategories.SortClauses=sortClauses;
			_reportCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserGroupRights", _userGroupRights);
			toReturn.Add("Claims", _claims);
			toReturn.Add("Reports", _reports);
			toReturn.Add("ReportCategories", _reportCategories);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="validator">The validator object for this UserResourceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.String resourceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(resourceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_userGroupRights = new VarioSL.Entities.CollectionClasses.UserGroupRightCollection();
			_userGroupRights.SetContainingEntityInfo(this, "UserResource");

			_claims = new VarioSL.Entities.CollectionClasses.ClaimCollection();
			_claims.SetContainingEntityInfo(this, "UserResource");

			_reports = new VarioSL.Entities.CollectionClasses.ReportCollection();
			_reports.SetContainingEntityInfo(this, "UserResource");

			_reportCategories = new VarioSL.Entities.CollectionClasses.ReportCategoryCollection();
			_reportCategories.SetContainingEntityInfo(this, "UserResource");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LanguageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResourceType", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="resourceID">PK value for UserResource which data should be fetched into this UserResource object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.String resourceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserResourceFieldIndex.ResourceID].ForcedCurrentValueWrite(resourceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserResourceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserResourceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserResourceRelations Relations
		{
			get	{ return new UserResourceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserGroupRight' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserGroupRights
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserGroupRightCollection(), (IEntityRelation)GetRelationsForField("UserGroupRights")[0], (int)VarioSL.Entities.EntityType.UserResourceEntity, (int)VarioSL.Entities.EntityType.UserGroupRightEntity, 0, null, null, null, "UserGroupRights", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Claim' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClaims
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClaimCollection(), (IEntityRelation)GetRelationsForField("Claims")[0], (int)VarioSL.Entities.EntityType.UserResourceEntity, (int)VarioSL.Entities.EntityType.ClaimEntity, 0, null, null, null, "Claims", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), (IEntityRelation)GetRelationsForField("Reports")[0], (int)VarioSL.Entities.EntityType.UserResourceEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, null, "Reports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCategoryCollection(), (IEntityRelation)GetRelationsForField("ReportCategories")[0], (int)VarioSL.Entities.EntityType.UserResourceEntity, (int)VarioSL.Entities.EntityType.ReportCategoryEntity, 0, null, null, null, "ReportCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LanguageID property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."LANGUAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 LanguageID
		{
			get { return (System.Int16)GetValue((int)UserResourceFieldIndex.LanguageID, true); }
			set	{ SetValue((int)UserResourceFieldIndex.LanguageID, value, true); }
		}

		/// <summary> The ResourceDescription property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."RESOURCEDESCR"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResourceDescription
		{
			get { return (System.String)GetValue((int)UserResourceFieldIndex.ResourceDescription, true); }
			set	{ SetValue((int)UserResourceFieldIndex.ResourceDescription, value, true); }
		}

		/// <summary> The ResourceGroup property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."RESOURCEGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResourceGroup
		{
			get { return (System.String)GetValue((int)UserResourceFieldIndex.ResourceGroup, true); }
			set	{ SetValue((int)UserResourceFieldIndex.ResourceGroup, value, true); }
		}

		/// <summary> The ResourceID property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."RESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.String ResourceID
		{
			get { return (System.String)GetValue((int)UserResourceFieldIndex.ResourceID, true); }
			set	{ SetValue((int)UserResourceFieldIndex.ResourceID, value, true); }
		}

		/// <summary> The ResourceName property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."RESOURCENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ResourceName
		{
			get { return (System.String)GetValue((int)UserResourceFieldIndex.ResourceName, true); }
			set	{ SetValue((int)UserResourceFieldIndex.ResourceName, value, true); }
		}

		/// <summary> The ResourceType property of the Entity UserResource<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_RESOURCE"."RESOURCETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ResourceType
		{
			get { return (Nullable<System.Int16>)GetValue((int)UserResourceFieldIndex.ResourceType, false); }
			set	{ SetValue((int)UserResourceFieldIndex.ResourceType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserGroupRights()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserGroupRightCollection UserGroupRights
		{
			get	{ return GetMultiUserGroupRights(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserGroupRights. When set to true, UserGroupRights is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserGroupRights is accessed. You can always execute/ a forced fetch by calling GetMultiUserGroupRights(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserGroupRights
		{
			get	{ return _alwaysFetchUserGroupRights; }
			set	{ _alwaysFetchUserGroupRights = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserGroupRights already has been fetched. Setting this property to false when UserGroupRights has been fetched
		/// will clear the UserGroupRights collection well. Setting this property to true while UserGroupRights hasn't been fetched disables lazy loading for UserGroupRights</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserGroupRights
		{
			get { return _alreadyFetchedUserGroupRights;}
			set 
			{
				if(_alreadyFetchedUserGroupRights && !value && (_userGroupRights != null))
				{
					_userGroupRights.Clear();
				}
				_alreadyFetchedUserGroupRights = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClaimEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClaims()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClaimCollection Claims
		{
			get	{ return GetMultiClaims(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Claims. When set to true, Claims is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Claims is accessed. You can always execute/ a forced fetch by calling GetMultiClaims(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClaims
		{
			get	{ return _alwaysFetchClaims; }
			set	{ _alwaysFetchClaims = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Claims already has been fetched. Setting this property to false when Claims has been fetched
		/// will clear the Claims collection well. Setting this property to true while Claims hasn't been fetched disables lazy loading for Claims</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClaims
		{
			get { return _alreadyFetchedClaims;}
			set 
			{
				if(_alreadyFetchedClaims && !value && (_claims != null))
				{
					_claims.Clear();
				}
				_alreadyFetchedClaims = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection Reports
		{
			get	{ return GetMultiReports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Reports. When set to true, Reports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Reports is accessed. You can always execute/ a forced fetch by calling GetMultiReports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReports
		{
			get	{ return _alwaysFetchReports; }
			set	{ _alwaysFetchReports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Reports already has been fetched. Setting this property to false when Reports has been fetched
		/// will clear the Reports collection well. Setting this property to true while Reports hasn't been fetched disables lazy loading for Reports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReports
		{
			get { return _alreadyFetchedReports;}
			set 
			{
				if(_alreadyFetchedReports && !value && (_reports != null))
				{
					_reports.Clear();
				}
				_alreadyFetchedReports = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection ReportCategories
		{
			get	{ return GetMultiReportCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportCategories. When set to true, ReportCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportCategories is accessed. You can always execute/ a forced fetch by calling GetMultiReportCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportCategories
		{
			get	{ return _alwaysFetchReportCategories; }
			set	{ _alwaysFetchReportCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportCategories already has been fetched. Setting this property to false when ReportCategories has been fetched
		/// will clear the ReportCategories collection well. Setting this property to true while ReportCategories hasn't been fetched disables lazy loading for ReportCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportCategories
		{
			get { return _alreadyFetchedReportCategories;}
			set 
			{
				if(_alreadyFetchedReportCategories && !value && (_reportCategories != null))
				{
					_reportCategories.Clear();
				}
				_alreadyFetchedReportCategories = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UserResourceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
