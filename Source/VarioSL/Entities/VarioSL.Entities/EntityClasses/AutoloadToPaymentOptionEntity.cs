﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AutoloadToPaymentOption'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AutoloadToPaymentOptionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AutoloadSettingEntity _autoloadSetting;
		private bool	_alwaysFetchAutoloadSetting, _alreadyFetchedAutoloadSetting, _autoloadSettingReturnsNewIfNotFound;
		private PaymentOptionEntity _backupPaymentOption;
		private bool	_alwaysFetchBackupPaymentOption, _alreadyFetchedBackupPaymentOption, _backupPaymentOptionReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AutoloadSetting</summary>
			public static readonly string AutoloadSetting = "AutoloadSetting";
			/// <summary>Member name BackupPaymentOption</summary>
			public static readonly string BackupPaymentOption = "BackupPaymentOption";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AutoloadToPaymentOptionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AutoloadToPaymentOptionEntity() :base("AutoloadToPaymentOptionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		public AutoloadToPaymentOptionEntity(System.Int64 autoloadToPaymentOptionID):base("AutoloadToPaymentOptionEntity")
		{
			InitClassFetch(autoloadToPaymentOptionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AutoloadToPaymentOptionEntity(System.Int64 autoloadToPaymentOptionID, IPrefetchPath prefetchPathToUse):base("AutoloadToPaymentOptionEntity")
		{
			InitClassFetch(autoloadToPaymentOptionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="validator">The custom validator object for this AutoloadToPaymentOptionEntity</param>
		public AutoloadToPaymentOptionEntity(System.Int64 autoloadToPaymentOptionID, IValidator validator):base("AutoloadToPaymentOptionEntity")
		{
			InitClassFetch(autoloadToPaymentOptionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AutoloadToPaymentOptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_autoloadSetting = (AutoloadSettingEntity)info.GetValue("_autoloadSetting", typeof(AutoloadSettingEntity));
			if(_autoloadSetting!=null)
			{
				_autoloadSetting.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_autoloadSettingReturnsNewIfNotFound = info.GetBoolean("_autoloadSettingReturnsNewIfNotFound");
			_alwaysFetchAutoloadSetting = info.GetBoolean("_alwaysFetchAutoloadSetting");
			_alreadyFetchedAutoloadSetting = info.GetBoolean("_alreadyFetchedAutoloadSetting");

			_backupPaymentOption = (PaymentOptionEntity)info.GetValue("_backupPaymentOption", typeof(PaymentOptionEntity));
			if(_backupPaymentOption!=null)
			{
				_backupPaymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_backupPaymentOptionReturnsNewIfNotFound = info.GetBoolean("_backupPaymentOptionReturnsNewIfNotFound");
			_alwaysFetchBackupPaymentOption = info.GetBoolean("_alwaysFetchBackupPaymentOption");
			_alreadyFetchedBackupPaymentOption = info.GetBoolean("_alreadyFetchedBackupPaymentOption");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AutoloadToPaymentOptionFieldIndex)fieldIndex)
			{
				case AutoloadToPaymentOptionFieldIndex.AutoloadSettingID:
					DesetupSyncAutoloadSetting(true, false);
					_alreadyFetchedAutoloadSetting = false;
					break;
				case AutoloadToPaymentOptionFieldIndex.BackupPaymentOptionID:
					DesetupSyncBackupPaymentOption(true, false);
					_alreadyFetchedBackupPaymentOption = false;
					break;
				case AutoloadToPaymentOptionFieldIndex.PaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutoloadSetting = (_autoloadSetting != null);
			_alreadyFetchedBackupPaymentOption = (_backupPaymentOption != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AutoloadSetting":
					toReturn.Add(Relations.AutoloadSettingEntityUsingAutoloadSettingID);
					break;
				case "BackupPaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingBackupPaymentOptionID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_autoloadSetting", (!this.MarkedForDeletion?_autoloadSetting:null));
			info.AddValue("_autoloadSettingReturnsNewIfNotFound", _autoloadSettingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutoloadSetting", _alwaysFetchAutoloadSetting);
			info.AddValue("_alreadyFetchedAutoloadSetting", _alreadyFetchedAutoloadSetting);
			info.AddValue("_backupPaymentOption", (!this.MarkedForDeletion?_backupPaymentOption:null));
			info.AddValue("_backupPaymentOptionReturnsNewIfNotFound", _backupPaymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBackupPaymentOption", _alwaysFetchBackupPaymentOption);
			info.AddValue("_alreadyFetchedBackupPaymentOption", _alreadyFetchedBackupPaymentOption);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AutoloadSetting":
					_alreadyFetchedAutoloadSetting = true;
					this.AutoloadSetting = (AutoloadSettingEntity)entity;
					break;
				case "BackupPaymentOption":
					_alreadyFetchedBackupPaymentOption = true;
					this.BackupPaymentOption = (PaymentOptionEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AutoloadSetting":
					SetupSyncAutoloadSetting(relatedEntity);
					break;
				case "BackupPaymentOption":
					SetupSyncBackupPaymentOption(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AutoloadSetting":
					DesetupSyncAutoloadSetting(false, true);
					break;
				case "BackupPaymentOption":
					DesetupSyncBackupPaymentOption(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_autoloadSetting!=null)
			{
				toReturn.Add(_autoloadSetting);
			}
			if(_backupPaymentOption!=null)
			{
				toReturn.Add(_backupPaymentOption);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadToPaymentOptionID)
		{
			return FetchUsingPK(autoloadToPaymentOptionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadToPaymentOptionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(autoloadToPaymentOptionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadToPaymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(autoloadToPaymentOptionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 autoloadToPaymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(autoloadToPaymentOptionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AutoloadToPaymentOptionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AutoloadToPaymentOptionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AutoloadSettingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutoloadSettingEntity' which is related to this entity.</returns>
		public AutoloadSettingEntity GetSingleAutoloadSetting()
		{
			return GetSingleAutoloadSetting(false);
		}

		/// <summary> Retrieves the related entity of type 'AutoloadSettingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutoloadSettingEntity' which is related to this entity.</returns>
		public virtual AutoloadSettingEntity GetSingleAutoloadSetting(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutoloadSetting || forceFetch || _alwaysFetchAutoloadSetting) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutoloadSettingEntityUsingAutoloadSettingID);
				AutoloadSettingEntity newEntity = new AutoloadSettingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutoloadSettingID);
				}
				if(fetchResult)
				{
					newEntity = (AutoloadSettingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_autoloadSettingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AutoloadSetting = newEntity;
				_alreadyFetchedAutoloadSetting = fetchResult;
			}
			return _autoloadSetting;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSingleBackupPaymentOption()
		{
			return GetSingleBackupPaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSingleBackupPaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedBackupPaymentOption || forceFetch || _alwaysFetchBackupPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingBackupPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BackupPaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_backupPaymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BackupPaymentOption = newEntity;
				_alreadyFetchedBackupPaymentOption = fetchResult;
			}
			return _backupPaymentOption;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentOptionID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AutoloadSetting", _autoloadSetting);
			toReturn.Add("BackupPaymentOption", _backupPaymentOption);
			toReturn.Add("PaymentOption", _paymentOption);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="validator">The validator object for this AutoloadToPaymentOptionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 autoloadToPaymentOptionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(autoloadToPaymentOptionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_autoloadSettingReturnsNewIfNotFound = false;
			_backupPaymentOptionReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoloadSettingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoloadToPaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BackupPaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _autoloadSetting</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutoloadSetting(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _autoloadSetting, new PropertyChangedEventHandler( OnAutoloadSettingPropertyChanged ), "AutoloadSetting", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.AutoloadSettingEntityUsingAutoloadSettingIDStatic, true, signalRelatedEntity, "AutoloadToPaymentOptions", resetFKFields, new int[] { (int)AutoloadToPaymentOptionFieldIndex.AutoloadSettingID } );		
			_autoloadSetting = null;
		}
		
		/// <summary> setups the sync logic for member _autoloadSetting</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutoloadSetting(IEntityCore relatedEntity)
		{
			if(_autoloadSetting!=relatedEntity)
			{		
				DesetupSyncAutoloadSetting(true, true);
				_autoloadSetting = (AutoloadSettingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _autoloadSetting, new PropertyChangedEventHandler( OnAutoloadSettingPropertyChanged ), "AutoloadSetting", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.AutoloadSettingEntityUsingAutoloadSettingIDStatic, true, ref _alreadyFetchedAutoloadSetting, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutoloadSettingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _backupPaymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBackupPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _backupPaymentOption, new PropertyChangedEventHandler( OnBackupPaymentOptionPropertyChanged ), "BackupPaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.PaymentOptionEntityUsingBackupPaymentOptionIDStatic, true, signalRelatedEntity, "BackupAutoloadToPaymentOptions", resetFKFields, new int[] { (int)AutoloadToPaymentOptionFieldIndex.BackupPaymentOptionID } );		
			_backupPaymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _backupPaymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBackupPaymentOption(IEntityCore relatedEntity)
		{
			if(_backupPaymentOption!=relatedEntity)
			{		
				DesetupSyncBackupPaymentOption(true, true);
				_backupPaymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _backupPaymentOption, new PropertyChangedEventHandler( OnBackupPaymentOptionPropertyChanged ), "BackupPaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.PaymentOptionEntityUsingBackupPaymentOptionIDStatic, true, ref _alreadyFetchedBackupPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBackupPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, signalRelatedEntity, "AutoloadToPaymentOptions", resetFKFields, new int[] { (int)AutoloadToPaymentOptionFieldIndex.PaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticAutoloadToPaymentOptionRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="autoloadToPaymentOptionID">PK value for AutoloadToPaymentOption which data should be fetched into this AutoloadToPaymentOption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 autoloadToPaymentOptionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AutoloadToPaymentOptionFieldIndex.AutoloadToPaymentOptionID].ForcedCurrentValueWrite(autoloadToPaymentOptionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAutoloadToPaymentOptionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AutoloadToPaymentOptionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AutoloadToPaymentOptionRelations Relations
		{
			get	{ return new AutoloadToPaymentOptionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadSetting'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadSetting
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection(), (IEntityRelation)GetRelationsForField("AutoloadSetting")[0], (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, 0, null, null, null, "AutoloadSetting", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBackupPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("BackupPaymentOption")[0], (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "BackupPaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)AutoloadToPaymentOptionFieldIndex.Amount, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.Amount, value, true); }
		}

		/// <summary> The AutoloadSettingID property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."AUTOLOADSETTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AutoloadSettingID
		{
			get { return (System.Int64)GetValue((int)AutoloadToPaymentOptionFieldIndex.AutoloadSettingID, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.AutoloadSettingID, value, true); }
		}

		/// <summary> The AutoloadToPaymentOptionID property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."AUTOLOADTOPAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AutoloadToPaymentOptionID
		{
			get { return (System.Int64)GetValue((int)AutoloadToPaymentOptionFieldIndex.AutoloadToPaymentOptionID, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.AutoloadToPaymentOptionID, value, true); }
		}

		/// <summary> The BackupPaymentOptionID property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."BACKUPPAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BackupPaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutoloadToPaymentOptionFieldIndex.BackupPaymentOptionID, false); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.BackupPaymentOptionID, value, true); }
		}

		/// <summary> The LastModified property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AutoloadToPaymentOptionFieldIndex.LastModified, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)AutoloadToPaymentOptionFieldIndex.LastUser, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PaymentOptionID
		{
			get { return (System.Int64)GetValue((int)AutoloadToPaymentOptionFieldIndex.PaymentOptionID, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity AutoloadToPaymentOption<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUTOLOADTOPAYMENTOPTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)AutoloadToPaymentOptionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)AutoloadToPaymentOptionFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AutoloadSettingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutoloadSetting()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutoloadSettingEntity AutoloadSetting
		{
			get	{ return GetSingleAutoloadSetting(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutoloadSetting(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AutoloadToPaymentOptions", "AutoloadSetting", _autoloadSetting, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadSetting. When set to true, AutoloadSetting is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadSetting is accessed. You can always execute a forced fetch by calling GetSingleAutoloadSetting(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadSetting
		{
			get	{ return _alwaysFetchAutoloadSetting; }
			set	{ _alwaysFetchAutoloadSetting = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadSetting already has been fetched. Setting this property to false when AutoloadSetting has been fetched
		/// will set AutoloadSetting to null as well. Setting this property to true while AutoloadSetting hasn't been fetched disables lazy loading for AutoloadSetting</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadSetting
		{
			get { return _alreadyFetchedAutoloadSetting;}
			set 
			{
				if(_alreadyFetchedAutoloadSetting && !value)
				{
					this.AutoloadSetting = null;
				}
				_alreadyFetchedAutoloadSetting = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AutoloadSetting is not found
		/// in the database. When set to true, AutoloadSetting will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutoloadSettingReturnsNewIfNotFound
		{
			get	{ return _autoloadSettingReturnsNewIfNotFound; }
			set { _autoloadSettingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBackupPaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity BackupPaymentOption
		{
			get	{ return GetSingleBackupPaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBackupPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BackupAutoloadToPaymentOptions", "BackupPaymentOption", _backupPaymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BackupPaymentOption. When set to true, BackupPaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BackupPaymentOption is accessed. You can always execute a forced fetch by calling GetSingleBackupPaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBackupPaymentOption
		{
			get	{ return _alwaysFetchBackupPaymentOption; }
			set	{ _alwaysFetchBackupPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BackupPaymentOption already has been fetched. Setting this property to false when BackupPaymentOption has been fetched
		/// will set BackupPaymentOption to null as well. Setting this property to true while BackupPaymentOption hasn't been fetched disables lazy loading for BackupPaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBackupPaymentOption
		{
			get { return _alreadyFetchedBackupPaymentOption;}
			set 
			{
				if(_alreadyFetchedBackupPaymentOption && !value)
				{
					this.BackupPaymentOption = null;
				}
				_alreadyFetchedBackupPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BackupPaymentOption is not found
		/// in the database. When set to true, BackupPaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BackupPaymentOptionReturnsNewIfNotFound
		{
			get	{ return _backupPaymentOptionReturnsNewIfNotFound; }
			set { _backupPaymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AutoloadToPaymentOptions", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
