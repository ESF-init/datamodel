﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareStageList'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareStageListEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareMatrixCollection	_farematrices;
		private bool	_alwaysFetchFarematrices, _alreadyFetchedFarematrices;
		private VarioSL.Entities.CollectionClasses.FareStageCollection	_farestages;
		private bool	_alwaysFetchFarestages, _alreadyFetchedFarestages;
		private AttributeValueEntity _attributevalue;
		private bool	_alwaysFetchAttributevalue, _alreadyFetchedAttributevalue, _attributevalueReturnsNewIfNotFound;
		private TariffEntity _tarif;
		private bool	_alwaysFetchTarif, _alreadyFetchedTarif, _tarifReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Attributevalue</summary>
			public static readonly string Attributevalue = "Attributevalue";
			/// <summary>Member name Tarif</summary>
			public static readonly string Tarif = "Tarif";
			/// <summary>Member name Farematrices</summary>
			public static readonly string Farematrices = "Farematrices";
			/// <summary>Member name Farestages</summary>
			public static readonly string Farestages = "Farestages";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareStageListEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareStageListEntity() :base("FareStageListEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		public FareStageListEntity(System.Int64 fareStageListID):base("FareStageListEntity")
		{
			InitClassFetch(fareStageListID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareStageListEntity(System.Int64 fareStageListID, IPrefetchPath prefetchPathToUse):base("FareStageListEntity")
		{
			InitClassFetch(fareStageListID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="validator">The custom validator object for this FareStageListEntity</param>
		public FareStageListEntity(System.Int64 fareStageListID, IValidator validator):base("FareStageListEntity")
		{
			InitClassFetch(fareStageListID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareStageListEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_farematrices = (VarioSL.Entities.CollectionClasses.FareMatrixCollection)info.GetValue("_farematrices", typeof(VarioSL.Entities.CollectionClasses.FareMatrixCollection));
			_alwaysFetchFarematrices = info.GetBoolean("_alwaysFetchFarematrices");
			_alreadyFetchedFarematrices = info.GetBoolean("_alreadyFetchedFarematrices");

			_farestages = (VarioSL.Entities.CollectionClasses.FareStageCollection)info.GetValue("_farestages", typeof(VarioSL.Entities.CollectionClasses.FareStageCollection));
			_alwaysFetchFarestages = info.GetBoolean("_alwaysFetchFarestages");
			_alreadyFetchedFarestages = info.GetBoolean("_alreadyFetchedFarestages");
			_attributevalue = (AttributeValueEntity)info.GetValue("_attributevalue", typeof(AttributeValueEntity));
			if(_attributevalue!=null)
			{
				_attributevalue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributevalueReturnsNewIfNotFound = info.GetBoolean("_attributevalueReturnsNewIfNotFound");
			_alwaysFetchAttributevalue = info.GetBoolean("_alwaysFetchAttributevalue");
			_alreadyFetchedAttributevalue = info.GetBoolean("_alreadyFetchedAttributevalue");

			_tarif = (TariffEntity)info.GetValue("_tarif", typeof(TariffEntity));
			if(_tarif!=null)
			{
				_tarif.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tarifReturnsNewIfNotFound = info.GetBoolean("_tarifReturnsNewIfNotFound");
			_alwaysFetchTarif = info.GetBoolean("_alwaysFetchTarif");
			_alreadyFetchedTarif = info.GetBoolean("_alreadyFetchedTarif");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareStageListFieldIndex)fieldIndex)
			{
				case FareStageListFieldIndex.AttributeValueID:
					DesetupSyncAttributevalue(true, false);
					_alreadyFetchedAttributevalue = false;
					break;
				case FareStageListFieldIndex.TariffID:
					DesetupSyncTarif(true, false);
					_alreadyFetchedTarif = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFarematrices = (_farematrices.Count > 0);
			_alreadyFetchedFarestages = (_farestages.Count > 0);
			_alreadyFetchedAttributevalue = (_attributevalue != null);
			_alreadyFetchedTarif = (_tarif != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Attributevalue":
					toReturn.Add(Relations.AttributeValueEntityUsingAttributeValueID);
					break;
				case "Tarif":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "Farematrices":
					toReturn.Add(Relations.FareMatrixEntityUsingFareStageListID);
					break;
				case "Farestages":
					toReturn.Add(Relations.FareStageEntityUsingFareStageListID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_farematrices", (!this.MarkedForDeletion?_farematrices:null));
			info.AddValue("_alwaysFetchFarematrices", _alwaysFetchFarematrices);
			info.AddValue("_alreadyFetchedFarematrices", _alreadyFetchedFarematrices);
			info.AddValue("_farestages", (!this.MarkedForDeletion?_farestages:null));
			info.AddValue("_alwaysFetchFarestages", _alwaysFetchFarestages);
			info.AddValue("_alreadyFetchedFarestages", _alreadyFetchedFarestages);
			info.AddValue("_attributevalue", (!this.MarkedForDeletion?_attributevalue:null));
			info.AddValue("_attributevalueReturnsNewIfNotFound", _attributevalueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributevalue", _alwaysFetchAttributevalue);
			info.AddValue("_alreadyFetchedAttributevalue", _alreadyFetchedAttributevalue);
			info.AddValue("_tarif", (!this.MarkedForDeletion?_tarif:null));
			info.AddValue("_tarifReturnsNewIfNotFound", _tarifReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTarif", _alwaysFetchTarif);
			info.AddValue("_alreadyFetchedTarif", _alreadyFetchedTarif);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Attributevalue":
					_alreadyFetchedAttributevalue = true;
					this.Attributevalue = (AttributeValueEntity)entity;
					break;
				case "Tarif":
					_alreadyFetchedTarif = true;
					this.Tarif = (TariffEntity)entity;
					break;
				case "Farematrices":
					_alreadyFetchedFarematrices = true;
					if(entity!=null)
					{
						this.Farematrices.Add((FareMatrixEntity)entity);
					}
					break;
				case "Farestages":
					_alreadyFetchedFarestages = true;
					if(entity!=null)
					{
						this.Farestages.Add((FareStageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Attributevalue":
					SetupSyncAttributevalue(relatedEntity);
					break;
				case "Tarif":
					SetupSyncTarif(relatedEntity);
					break;
				case "Farematrices":
					_farematrices.Add((FareMatrixEntity)relatedEntity);
					break;
				case "Farestages":
					_farestages.Add((FareStageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Attributevalue":
					DesetupSyncAttributevalue(false, true);
					break;
				case "Tarif":
					DesetupSyncTarif(false, true);
					break;
				case "Farematrices":
					this.PerformRelatedEntityRemoval(_farematrices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Farestages":
					this.PerformRelatedEntityRemoval(_farestages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attributevalue!=null)
			{
				toReturn.Add(_attributevalue);
			}
			if(_tarif!=null)
			{
				toReturn.Add(_tarif);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_farematrices);
			toReturn.Add(_farestages);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageListID)
		{
			return FetchUsingPK(fareStageListID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageListID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareStageListID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageListID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareStageListID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareStageListID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareStageListID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareStageListRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFarematrices(bool forceFetch)
		{
			return GetMultiFarematrices(forceFetch, _farematrices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFarematrices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFarematrices(forceFetch, _farematrices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFarematrices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFarematrices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFarematrices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFarematrices || forceFetch || _alwaysFetchFarematrices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_farematrices);
				_farematrices.SuppressClearInGetMulti=!forceFetch;
				_farematrices.EntityFactoryToUse = entityFactoryToUse;
				_farematrices.GetMultiManyToOne(null, null, this, null, filter);
				_farematrices.SuppressClearInGetMulti=false;
				_alreadyFetchedFarematrices = true;
			}
			return _farematrices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Farematrices'. These settings will be taken into account
		/// when the property Farematrices is requested or GetMultiFarematrices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFarematrices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_farematrices.SortClauses=sortClauses;
			_farematrices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiFarestages(bool forceFetch)
		{
			return GetMultiFarestages(forceFetch, _farestages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiFarestages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFarestages(forceFetch, _farestages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiFarestages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFarestages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareStageCollection GetMultiFarestages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFarestages || forceFetch || _alwaysFetchFarestages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_farestages);
				_farestages.SuppressClearInGetMulti=!forceFetch;
				_farestages.EntityFactoryToUse = entityFactoryToUse;
				_farestages.GetMultiManyToOne(null, null, this, null, filter);
				_farestages.SuppressClearInGetMulti=false;
				_alreadyFetchedFarestages = true;
			}
			return _farestages;
		}

		/// <summary> Sets the collection parameters for the collection for 'Farestages'. These settings will be taken into account
		/// when the property Farestages is requested or GetMultiFarestages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFarestages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_farestages.SortClauses=sortClauses;
			_farestages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributevalue()
		{
			return GetSingleAttributevalue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributevalue(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributevalue || forceFetch || _alwaysFetchAttributevalue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingAttributeValueID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeValueID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributevalueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Attributevalue = newEntity;
				_alreadyFetchedAttributevalue = fetchResult;
			}
			return _attributevalue;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTarif()
		{
			return GetSingleTarif(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTarif(bool forceFetch)
		{
			if( ( !_alreadyFetchedTarif || forceFetch || _alwaysFetchTarif) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tarifReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tarif = newEntity;
				_alreadyFetchedTarif = fetchResult;
			}
			return _tarif;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Attributevalue", _attributevalue);
			toReturn.Add("Tarif", _tarif);
			toReturn.Add("Farematrices", _farematrices);
			toReturn.Add("Farestages", _farestages);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="validator">The validator object for this FareStageListEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareStageListID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareStageListID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_farematrices = new VarioSL.Entities.CollectionClasses.FareMatrixCollection();
			_farematrices.SetContainingEntityInfo(this, "FareStageList");

			_farestages = new VarioSL.Entities.CollectionClasses.FareStageCollection();
			_farestages.SetContainingEntityInfo(this, "FareStageList");
			_attributevalueReturnsNewIfNotFound = false;
			_tarifReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attributevalue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributevalue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributevalue, new PropertyChangedEventHandler( OnAttributevaluePropertyChanged ), "Attributevalue", VarioSL.Entities.RelationClasses.StaticFareStageListRelations.AttributeValueEntityUsingAttributeValueIDStatic, true, signalRelatedEntity, "FareStageLists", resetFKFields, new int[] { (int)FareStageListFieldIndex.AttributeValueID } );		
			_attributevalue = null;
		}
		
		/// <summary> setups the sync logic for member _attributevalue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributevalue(IEntityCore relatedEntity)
		{
			if(_attributevalue!=relatedEntity)
			{		
				DesetupSyncAttributevalue(true, true);
				_attributevalue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributevalue, new PropertyChangedEventHandler( OnAttributevaluePropertyChanged ), "Attributevalue", VarioSL.Entities.RelationClasses.StaticFareStageListRelations.AttributeValueEntityUsingAttributeValueIDStatic, true, ref _alreadyFetchedAttributevalue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributevaluePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tarif</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTarif(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tarif, new PropertyChangedEventHandler( OnTarifPropertyChanged ), "Tarif", VarioSL.Entities.RelationClasses.StaticFareStageListRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "FareStageLists", resetFKFields, new int[] { (int)FareStageListFieldIndex.TariffID } );		
			_tarif = null;
		}
		
		/// <summary> setups the sync logic for member _tarif</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTarif(IEntityCore relatedEntity)
		{
			if(_tarif!=relatedEntity)
			{		
				DesetupSyncTarif(true, true);
				_tarif = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tarif, new PropertyChangedEventHandler( OnTarifPropertyChanged ), "Tarif", VarioSL.Entities.RelationClasses.StaticFareStageListRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTarif, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTarifPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareStageListID">PK value for FareStageList which data should be fetched into this FareStageList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareStageListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareStageListFieldIndex.FareStageListID].ForcedCurrentValueWrite(fareStageListID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareStageListDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareStageListEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareStageListRelations Relations
		{
			get	{ return new FareStageListRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFarematrices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("Farematrices")[0], (int)VarioSL.Entities.EntityType.FareStageListEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "Farematrices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFarestages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("Farestages")[0], (int)VarioSL.Entities.EntityType.FareStageListEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "Farestages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributevalue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("Attributevalue")[0], (int)VarioSL.Entities.EntityType.FareStageListEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "Attributevalue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTarif
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tarif")[0], (int)VarioSL.Entities.EntityType.FareStageListEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tarif", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeValueID property of the Entity FareStageList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGELIST"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageListFieldIndex.AttributeValueID, false); }
			set	{ SetValue((int)FareStageListFieldIndex.AttributeValueID, value, true); }
		}

		/// <summary> The Description property of the Entity FareStageList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGELIST"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FareStageListFieldIndex.Description, true); }
			set	{ SetValue((int)FareStageListFieldIndex.Description, value, true); }
		}

		/// <summary> The FareStageListID property of the Entity FareStageList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGELIST"."FARESTAGELISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareStageListID
		{
			get { return (System.Int64)GetValue((int)FareStageListFieldIndex.FareStageListID, true); }
			set	{ SetValue((int)FareStageListFieldIndex.FareStageListID, value, true); }
		}

		/// <summary> The Name property of the Entity FareStageList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGELIST"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareStageListFieldIndex.Name, true); }
			set	{ SetValue((int)FareStageListFieldIndex.Name, value, true); }
		}

		/// <summary> The TariffID property of the Entity FareStageList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGELIST"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageListFieldIndex.TariffID, false); }
			set	{ SetValue((int)FareStageListFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFarematrices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection Farematrices
		{
			get	{ return GetMultiFarematrices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Farematrices. When set to true, Farematrices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Farematrices is accessed. You can always execute/ a forced fetch by calling GetMultiFarematrices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFarematrices
		{
			get	{ return _alwaysFetchFarematrices; }
			set	{ _alwaysFetchFarematrices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Farematrices already has been fetched. Setting this property to false when Farematrices has been fetched
		/// will clear the Farematrices collection well. Setting this property to true while Farematrices hasn't been fetched disables lazy loading for Farematrices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFarematrices
		{
			get { return _alreadyFetchedFarematrices;}
			set 
			{
				if(_alreadyFetchedFarematrices && !value && (_farematrices != null))
				{
					_farematrices.Clear();
				}
				_alreadyFetchedFarematrices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareStageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFarestages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareStageCollection Farestages
		{
			get	{ return GetMultiFarestages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Farestages. When set to true, Farestages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Farestages is accessed. You can always execute/ a forced fetch by calling GetMultiFarestages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFarestages
		{
			get	{ return _alwaysFetchFarestages; }
			set	{ _alwaysFetchFarestages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Farestages already has been fetched. Setting this property to false when Farestages has been fetched
		/// will clear the Farestages collection well. Setting this property to true while Farestages hasn't been fetched disables lazy loading for Farestages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFarestages
		{
			get { return _alreadyFetchedFarestages;}
			set 
			{
				if(_alreadyFetchedFarestages && !value && (_farestages != null))
				{
					_farestages.Clear();
				}
				_alreadyFetchedFarestages = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributevalue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity Attributevalue
		{
			get	{ return GetSingleAttributevalue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributevalue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStageLists", "Attributevalue", _attributevalue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Attributevalue. When set to true, Attributevalue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Attributevalue is accessed. You can always execute a forced fetch by calling GetSingleAttributevalue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributevalue
		{
			get	{ return _alwaysFetchAttributevalue; }
			set	{ _alwaysFetchAttributevalue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Attributevalue already has been fetched. Setting this property to false when Attributevalue has been fetched
		/// will set Attributevalue to null as well. Setting this property to true while Attributevalue hasn't been fetched disables lazy loading for Attributevalue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributevalue
		{
			get { return _alreadyFetchedAttributevalue;}
			set 
			{
				if(_alreadyFetchedAttributevalue && !value)
				{
					this.Attributevalue = null;
				}
				_alreadyFetchedAttributevalue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Attributevalue is not found
		/// in the database. When set to true, Attributevalue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributevalueReturnsNewIfNotFound
		{
			get	{ return _attributevalueReturnsNewIfNotFound; }
			set { _attributevalueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTarif()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tarif
		{
			get	{ return GetSingleTarif(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTarif(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStageLists", "Tarif", _tarif, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tarif. When set to true, Tarif is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tarif is accessed. You can always execute a forced fetch by calling GetSingleTarif(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTarif
		{
			get	{ return _alwaysFetchTarif; }
			set	{ _alwaysFetchTarif = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tarif already has been fetched. Setting this property to false when Tarif has been fetched
		/// will set Tarif to null as well. Setting this property to true while Tarif hasn't been fetched disables lazy loading for Tarif</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTarif
		{
			get { return _alreadyFetchedTarif;}
			set 
			{
				if(_alreadyFetchedTarif && !value)
				{
					this.Tarif = null;
				}
				_alreadyFetchedTarif = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tarif is not found
		/// in the database. When set to true, Tarif will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TarifReturnsNewIfNotFound
		{
			get	{ return _tarifReturnsNewIfNotFound; }
			set { _tarifReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareStageListEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
