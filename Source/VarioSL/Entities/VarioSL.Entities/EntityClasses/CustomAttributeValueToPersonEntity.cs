﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomAttributeValueToPerson'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomAttributeValueToPersonEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CustomAttributeValueEntity _customAttributeValue;
		private bool	_alwaysFetchCustomAttributeValue, _alreadyFetchedCustomAttributeValue, _customAttributeValueReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomAttributeValue</summary>
			public static readonly string CustomAttributeValue = "CustomAttributeValue";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomAttributeValueToPersonEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomAttributeValueToPersonEntity() :base("CustomAttributeValueToPersonEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		public CustomAttributeValueToPersonEntity(System.Int64 customAttributeValueID, System.Int64 personID):base("CustomAttributeValueToPersonEntity")
		{
			InitClassFetch(customAttributeValueID, personID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomAttributeValueToPersonEntity(System.Int64 customAttributeValueID, System.Int64 personID, IPrefetchPath prefetchPathToUse):base("CustomAttributeValueToPersonEntity")
		{
			InitClassFetch(customAttributeValueID, personID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="validator">The custom validator object for this CustomAttributeValueToPersonEntity</param>
		public CustomAttributeValueToPersonEntity(System.Int64 customAttributeValueID, System.Int64 personID, IValidator validator):base("CustomAttributeValueToPersonEntity")
		{
			InitClassFetch(customAttributeValueID, personID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomAttributeValueToPersonEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customAttributeValue = (CustomAttributeValueEntity)info.GetValue("_customAttributeValue", typeof(CustomAttributeValueEntity));
			if(_customAttributeValue!=null)
			{
				_customAttributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customAttributeValueReturnsNewIfNotFound = info.GetBoolean("_customAttributeValueReturnsNewIfNotFound");
			_alwaysFetchCustomAttributeValue = info.GetBoolean("_alwaysFetchCustomAttributeValue");
			_alreadyFetchedCustomAttributeValue = info.GetBoolean("_alreadyFetchedCustomAttributeValue");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomAttributeValueToPersonFieldIndex)fieldIndex)
			{
				case CustomAttributeValueToPersonFieldIndex.CustomAttributeValueID:
					DesetupSyncCustomAttributeValue(true, false);
					_alreadyFetchedCustomAttributeValue = false;
					break;
				case CustomAttributeValueToPersonFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomAttributeValue = (_customAttributeValue != null);
			_alreadyFetchedPerson = (_person != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomAttributeValue":
					toReturn.Add(Relations.CustomAttributeValueEntityUsingCustomAttributeValueID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customAttributeValue", (!this.MarkedForDeletion?_customAttributeValue:null));
			info.AddValue("_customAttributeValueReturnsNewIfNotFound", _customAttributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomAttributeValue", _alwaysFetchCustomAttributeValue);
			info.AddValue("_alreadyFetchedCustomAttributeValue", _alreadyFetchedCustomAttributeValue);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomAttributeValue":
					_alreadyFetchedCustomAttributeValue = true;
					this.CustomAttributeValue = (CustomAttributeValueEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomAttributeValue":
					SetupSyncCustomAttributeValue(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomAttributeValue":
					DesetupSyncCustomAttributeValue(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customAttributeValue!=null)
			{
				toReturn.Add(_customAttributeValue);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, System.Int64 personID)
		{
			return FetchUsingPK(customAttributeValueID, personID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, System.Int64 personID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customAttributeValueID, personID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customAttributeValueID, personID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeValueID, System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customAttributeValueID, personID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomAttributeValueID, this.PersonID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomAttributeValueToPersonRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomAttributeValueEntity' which is related to this entity.</returns>
		public CustomAttributeValueEntity GetSingleCustomAttributeValue()
		{
			return GetSingleCustomAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomAttributeValueEntity' which is related to this entity.</returns>
		public virtual CustomAttributeValueEntity GetSingleCustomAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomAttributeValue || forceFetch || _alwaysFetchCustomAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomAttributeValueEntityUsingCustomAttributeValueID);
				CustomAttributeValueEntity newEntity = new CustomAttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomAttributeValueID);
				}
				if(fetchResult)
				{
					newEntity = (CustomAttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customAttributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomAttributeValue = newEntity;
				_alreadyFetchedCustomAttributeValue = fetchResult;
			}
			return _customAttributeValue;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomAttributeValue", _customAttributeValue);
			toReturn.Add("Person", _person);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="validator">The validator object for this CustomAttributeValueToPersonEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customAttributeValueID, System.Int64 personID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customAttributeValueID, personID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_customAttributeValueReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customAttributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customAttributeValue, new PropertyChangedEventHandler( OnCustomAttributeValuePropertyChanged ), "CustomAttributeValue", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueToPersonRelations.CustomAttributeValueEntityUsingCustomAttributeValueIDStatic, true, signalRelatedEntity, "CustomAttributeValueToPeople", resetFKFields, new int[] { (int)CustomAttributeValueToPersonFieldIndex.CustomAttributeValueID } );		
			_customAttributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _customAttributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomAttributeValue(IEntityCore relatedEntity)
		{
			if(_customAttributeValue!=relatedEntity)
			{		
				DesetupSyncCustomAttributeValue(true, true);
				_customAttributeValue = (CustomAttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customAttributeValue, new PropertyChangedEventHandler( OnCustomAttributeValuePropertyChanged ), "CustomAttributeValue", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueToPersonRelations.CustomAttributeValueEntityUsingCustomAttributeValueIDStatic, true, ref _alreadyFetchedCustomAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueToPersonRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "CustomAttributeValueToPeople", resetFKFields, new int[] { (int)CustomAttributeValueToPersonFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticCustomAttributeValueToPersonRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customAttributeValueID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="personID">PK value for CustomAttributeValueToPerson which data should be fetched into this CustomAttributeValueToPerson object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customAttributeValueID, System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomAttributeValueToPersonFieldIndex.CustomAttributeValueID].ForcedCurrentValueWrite(customAttributeValueID);
				this.Fields[(int)CustomAttributeValueToPersonFieldIndex.PersonID].ForcedCurrentValueWrite(personID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomAttributeValueToPersonDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomAttributeValueToPersonEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomAttributeValueToPersonRelations Relations
		{
			get	{ return new CustomAttributeValueToPersonRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection(), (IEntityRelation)GetRelationsForField("CustomAttributeValue")[0], (int)VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity, (int)VarioSL.Entities.EntityType.CustomAttributeValueEntity, 0, null, null, null, "CustomAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomAttributeValueID property of the Entity CustomAttributeValueToPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUETOPERSON"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 CustomAttributeValueID
		{
			get { return (System.Int64)GetValue((int)CustomAttributeValueToPersonFieldIndex.CustomAttributeValueID, true); }
			set	{ SetValue((int)CustomAttributeValueToPersonFieldIndex.CustomAttributeValueID, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomAttributeValueToPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUETOPERSON"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModified
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomAttributeValueToPersonFieldIndex.LastModified, false); }
			set	{ SetValue((int)CustomAttributeValueToPersonFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomAttributeValueToPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUETOPERSON"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomAttributeValueToPersonFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomAttributeValueToPersonFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PersonID property of the Entity CustomAttributeValueToPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUETOPERSON"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)CustomAttributeValueToPersonFieldIndex.PersonID, true); }
			set	{ SetValue((int)CustomAttributeValueToPersonFieldIndex.PersonID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomAttributeValueToPerson<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTEVALUETOPERSON"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomAttributeValueToPersonFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomAttributeValueToPersonFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CustomAttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomAttributeValueEntity CustomAttributeValue
		{
			get	{ return GetSingleCustomAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomAttributeValueToPeople", "CustomAttributeValue", _customAttributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttributeValue. When set to true, CustomAttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttributeValue is accessed. You can always execute a forced fetch by calling GetSingleCustomAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttributeValue
		{
			get	{ return _alwaysFetchCustomAttributeValue; }
			set	{ _alwaysFetchCustomAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttributeValue already has been fetched. Setting this property to false when CustomAttributeValue has been fetched
		/// will set CustomAttributeValue to null as well. Setting this property to true while CustomAttributeValue hasn't been fetched disables lazy loading for CustomAttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttributeValue
		{
			get { return _alreadyFetchedCustomAttributeValue;}
			set 
			{
				if(_alreadyFetchedCustomAttributeValue && !value)
				{
					this.CustomAttributeValue = null;
				}
				_alreadyFetchedCustomAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomAttributeValue is not found
		/// in the database. When set to true, CustomAttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomAttributeValueReturnsNewIfNotFound
		{
			get	{ return _customAttributeValueReturnsNewIfNotFound; }
			set { _customAttributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomAttributeValueToPeople", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
