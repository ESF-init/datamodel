﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Order'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection	_orderDetails;
		private bool	_alwaysFetchOrderDetails, _alreadyFetchedOrderDetails;
		private VarioSL.Entities.CollectionClasses.OrderHistoryCollection	_orderHistories;
		private bool	_alwaysFetchOrderHistories, _alreadyFetchedOrderHistories;
		private VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection	_orderProcessingCleanups;
		private bool	_alwaysFetchOrderProcessingCleanups, _alreadyFetchedOrderProcessingCleanups;
		private VarioSL.Entities.CollectionClasses.OrderWorkItemCollection	_workItems;
		private bool	_alwaysFetchWorkItems, _alreadyFetchedWorkItems;
		private UserListEntity _assignedUser;
		private bool	_alwaysFetchAssignedUser, _alreadyFetchedAssignedUser, _assignedUserReturnsNewIfNotFound;
		private AddressEntity _invoiceAddress;
		private bool	_alwaysFetchInvoiceAddress, _alreadyFetchedInvoiceAddress, _invoiceAddressReturnsNewIfNotFound;
		private AddressEntity _shippingAddress;
		private bool	_alwaysFetchShippingAddress, _alreadyFetchedShippingAddress, _shippingAddressReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;
		private SchoolYearEntity _schoolYear;
		private bool	_alwaysFetchSchoolYear, _alreadyFetchedSchoolYear, _schoolYearReturnsNewIfNotFound;
		private SaleEntity _sale;
		private bool	_alwaysFetchSale, _alreadyFetchedSale, _saleReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AssignedUser</summary>
			public static readonly string AssignedUser = "AssignedUser";
			/// <summary>Member name InvoiceAddress</summary>
			public static readonly string InvoiceAddress = "InvoiceAddress";
			/// <summary>Member name ShippingAddress</summary>
			public static readonly string ShippingAddress = "ShippingAddress";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
			/// <summary>Member name SchoolYear</summary>
			public static readonly string SchoolYear = "SchoolYear";
			/// <summary>Member name OrderDetails</summary>
			public static readonly string OrderDetails = "OrderDetails";
			/// <summary>Member name OrderHistories</summary>
			public static readonly string OrderHistories = "OrderHistories";
			/// <summary>Member name OrderProcessingCleanups</summary>
			public static readonly string OrderProcessingCleanups = "OrderProcessingCleanups";
			/// <summary>Member name WorkItems</summary>
			public static readonly string WorkItems = "WorkItems";
			/// <summary>Member name Sale</summary>
			public static readonly string Sale = "Sale";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrderEntity() :base("OrderEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		public OrderEntity(System.Int64 orderID):base("OrderEntity")
		{
			InitClassFetch(orderID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderEntity(System.Int64 orderID, IPrefetchPath prefetchPathToUse):base("OrderEntity")
		{
			InitClassFetch(orderID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The custom validator object for this OrderEntity</param>
		public OrderEntity(System.Int64 orderID, IValidator validator):base("OrderEntity")
		{
			InitClassFetch(orderID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_orderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchOrderDetails = info.GetBoolean("_alwaysFetchOrderDetails");
			_alreadyFetchedOrderDetails = info.GetBoolean("_alreadyFetchedOrderDetails");

			_orderHistories = (VarioSL.Entities.CollectionClasses.OrderHistoryCollection)info.GetValue("_orderHistories", typeof(VarioSL.Entities.CollectionClasses.OrderHistoryCollection));
			_alwaysFetchOrderHistories = info.GetBoolean("_alwaysFetchOrderHistories");
			_alreadyFetchedOrderHistories = info.GetBoolean("_alreadyFetchedOrderHistories");

			_orderProcessingCleanups = (VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection)info.GetValue("_orderProcessingCleanups", typeof(VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection));
			_alwaysFetchOrderProcessingCleanups = info.GetBoolean("_alwaysFetchOrderProcessingCleanups");
			_alreadyFetchedOrderProcessingCleanups = info.GetBoolean("_alreadyFetchedOrderProcessingCleanups");

			_workItems = (VarioSL.Entities.CollectionClasses.OrderWorkItemCollection)info.GetValue("_workItems", typeof(VarioSL.Entities.CollectionClasses.OrderWorkItemCollection));
			_alwaysFetchWorkItems = info.GetBoolean("_alwaysFetchWorkItems");
			_alreadyFetchedWorkItems = info.GetBoolean("_alreadyFetchedWorkItems");
			_assignedUser = (UserListEntity)info.GetValue("_assignedUser", typeof(UserListEntity));
			if(_assignedUser!=null)
			{
				_assignedUser.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_assignedUserReturnsNewIfNotFound = info.GetBoolean("_assignedUserReturnsNewIfNotFound");
			_alwaysFetchAssignedUser = info.GetBoolean("_alwaysFetchAssignedUser");
			_alreadyFetchedAssignedUser = info.GetBoolean("_alreadyFetchedAssignedUser");

			_invoiceAddress = (AddressEntity)info.GetValue("_invoiceAddress", typeof(AddressEntity));
			if(_invoiceAddress!=null)
			{
				_invoiceAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoiceAddressReturnsNewIfNotFound = info.GetBoolean("_invoiceAddressReturnsNewIfNotFound");
			_alwaysFetchInvoiceAddress = info.GetBoolean("_alwaysFetchInvoiceAddress");
			_alreadyFetchedInvoiceAddress = info.GetBoolean("_alreadyFetchedInvoiceAddress");

			_shippingAddress = (AddressEntity)info.GetValue("_shippingAddress", typeof(AddressEntity));
			if(_shippingAddress!=null)
			{
				_shippingAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_shippingAddressReturnsNewIfNotFound = info.GetBoolean("_shippingAddressReturnsNewIfNotFound");
			_alwaysFetchShippingAddress = info.GetBoolean("_alwaysFetchShippingAddress");
			_alreadyFetchedShippingAddress = info.GetBoolean("_alreadyFetchedShippingAddress");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");

			_schoolYear = (SchoolYearEntity)info.GetValue("_schoolYear", typeof(SchoolYearEntity));
			if(_schoolYear!=null)
			{
				_schoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_schoolYearReturnsNewIfNotFound = info.GetBoolean("_schoolYearReturnsNewIfNotFound");
			_alwaysFetchSchoolYear = info.GetBoolean("_alwaysFetchSchoolYear");
			_alreadyFetchedSchoolYear = info.GetBoolean("_alreadyFetchedSchoolYear");
			_sale = (SaleEntity)info.GetValue("_sale", typeof(SaleEntity));
			if(_sale!=null)
			{
				_sale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_saleReturnsNewIfNotFound = info.GetBoolean("_saleReturnsNewIfNotFound");
			_alwaysFetchSale = info.GetBoolean("_alwaysFetchSale");
			_alreadyFetchedSale = info.GetBoolean("_alreadyFetchedSale");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderFieldIndex)fieldIndex)
			{
				case OrderFieldIndex.Assignee:
					DesetupSyncAssignedUser(true, false);
					_alreadyFetchedAssignedUser = false;
					break;
				case OrderFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case OrderFieldIndex.InvoiceAddressID:
					DesetupSyncInvoiceAddress(true, false);
					_alreadyFetchedInvoiceAddress = false;
					break;
				case OrderFieldIndex.PaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				case OrderFieldIndex.SchoolYearID:
					DesetupSyncSchoolYear(true, false);
					_alreadyFetchedSchoolYear = false;
					break;
				case OrderFieldIndex.ShippingAddressID:
					DesetupSyncShippingAddress(true, false);
					_alreadyFetchedShippingAddress = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderDetails = (_orderDetails.Count > 0);
			_alreadyFetchedOrderHistories = (_orderHistories.Count > 0);
			_alreadyFetchedOrderProcessingCleanups = (_orderProcessingCleanups.Count > 0);
			_alreadyFetchedWorkItems = (_workItems.Count > 0);
			_alreadyFetchedAssignedUser = (_assignedUser != null);
			_alreadyFetchedInvoiceAddress = (_invoiceAddress != null);
			_alreadyFetchedShippingAddress = (_shippingAddress != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
			_alreadyFetchedSchoolYear = (_schoolYear != null);
			_alreadyFetchedSale = (_sale != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AssignedUser":
					toReturn.Add(Relations.UserListEntityUsingAssignee);
					break;
				case "InvoiceAddress":
					toReturn.Add(Relations.AddressEntityUsingInvoiceAddressID);
					break;
				case "ShippingAddress":
					toReturn.Add(Relations.AddressEntityUsingShippingAddressID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingPaymentOptionID);
					break;
				case "SchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingSchoolYearID);
					break;
				case "OrderDetails":
					toReturn.Add(Relations.OrderDetailEntityUsingOrderID);
					break;
				case "OrderHistories":
					toReturn.Add(Relations.OrderHistoryEntityUsingOrderID);
					break;
				case "OrderProcessingCleanups":
					toReturn.Add(Relations.OrderProcessingCleanupEntityUsingOrderID);
					break;
				case "WorkItems":
					toReturn.Add(Relations.OrderWorkItemEntityUsingOrderID);
					break;
				case "Sale":
					toReturn.Add(Relations.SaleEntityUsingOrderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderDetails", (!this.MarkedForDeletion?_orderDetails:null));
			info.AddValue("_alwaysFetchOrderDetails", _alwaysFetchOrderDetails);
			info.AddValue("_alreadyFetchedOrderDetails", _alreadyFetchedOrderDetails);
			info.AddValue("_orderHistories", (!this.MarkedForDeletion?_orderHistories:null));
			info.AddValue("_alwaysFetchOrderHistories", _alwaysFetchOrderHistories);
			info.AddValue("_alreadyFetchedOrderHistories", _alreadyFetchedOrderHistories);
			info.AddValue("_orderProcessingCleanups", (!this.MarkedForDeletion?_orderProcessingCleanups:null));
			info.AddValue("_alwaysFetchOrderProcessingCleanups", _alwaysFetchOrderProcessingCleanups);
			info.AddValue("_alreadyFetchedOrderProcessingCleanups", _alreadyFetchedOrderProcessingCleanups);
			info.AddValue("_workItems", (!this.MarkedForDeletion?_workItems:null));
			info.AddValue("_alwaysFetchWorkItems", _alwaysFetchWorkItems);
			info.AddValue("_alreadyFetchedWorkItems", _alreadyFetchedWorkItems);
			info.AddValue("_assignedUser", (!this.MarkedForDeletion?_assignedUser:null));
			info.AddValue("_assignedUserReturnsNewIfNotFound", _assignedUserReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAssignedUser", _alwaysFetchAssignedUser);
			info.AddValue("_alreadyFetchedAssignedUser", _alreadyFetchedAssignedUser);
			info.AddValue("_invoiceAddress", (!this.MarkedForDeletion?_invoiceAddress:null));
			info.AddValue("_invoiceAddressReturnsNewIfNotFound", _invoiceAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoiceAddress", _alwaysFetchInvoiceAddress);
			info.AddValue("_alreadyFetchedInvoiceAddress", _alreadyFetchedInvoiceAddress);
			info.AddValue("_shippingAddress", (!this.MarkedForDeletion?_shippingAddress:null));
			info.AddValue("_shippingAddressReturnsNewIfNotFound", _shippingAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchShippingAddress", _alwaysFetchShippingAddress);
			info.AddValue("_alreadyFetchedShippingAddress", _alreadyFetchedShippingAddress);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);
			info.AddValue("_schoolYear", (!this.MarkedForDeletion?_schoolYear:null));
			info.AddValue("_schoolYearReturnsNewIfNotFound", _schoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSchoolYear", _alwaysFetchSchoolYear);
			info.AddValue("_alreadyFetchedSchoolYear", _alreadyFetchedSchoolYear);

			info.AddValue("_sale", (!this.MarkedForDeletion?_sale:null));
			info.AddValue("_saleReturnsNewIfNotFound", _saleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSale", _alwaysFetchSale);
			info.AddValue("_alreadyFetchedSale", _alreadyFetchedSale);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AssignedUser":
					_alreadyFetchedAssignedUser = true;
					this.AssignedUser = (UserListEntity)entity;
					break;
				case "InvoiceAddress":
					_alreadyFetchedInvoiceAddress = true;
					this.InvoiceAddress = (AddressEntity)entity;
					break;
				case "ShippingAddress":
					_alreadyFetchedShippingAddress = true;
					this.ShippingAddress = (AddressEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				case "SchoolYear":
					_alreadyFetchedSchoolYear = true;
					this.SchoolYear = (SchoolYearEntity)entity;
					break;
				case "OrderDetails":
					_alreadyFetchedOrderDetails = true;
					if(entity!=null)
					{
						this.OrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "OrderHistories":
					_alreadyFetchedOrderHistories = true;
					if(entity!=null)
					{
						this.OrderHistories.Add((OrderHistoryEntity)entity);
					}
					break;
				case "OrderProcessingCleanups":
					_alreadyFetchedOrderProcessingCleanups = true;
					if(entity!=null)
					{
						this.OrderProcessingCleanups.Add((OrderProcessingCleanupEntity)entity);
					}
					break;
				case "WorkItems":
					_alreadyFetchedWorkItems = true;
					if(entity!=null)
					{
						this.WorkItems.Add((OrderWorkItemEntity)entity);
					}
					break;
				case "Sale":
					_alreadyFetchedSale = true;
					this.Sale = (SaleEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AssignedUser":
					SetupSyncAssignedUser(relatedEntity);
					break;
				case "InvoiceAddress":
					SetupSyncInvoiceAddress(relatedEntity);
					break;
				case "ShippingAddress":
					SetupSyncShippingAddress(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				case "SchoolYear":
					SetupSyncSchoolYear(relatedEntity);
					break;
				case "OrderDetails":
					_orderDetails.Add((OrderDetailEntity)relatedEntity);
					break;
				case "OrderHistories":
					_orderHistories.Add((OrderHistoryEntity)relatedEntity);
					break;
				case "OrderProcessingCleanups":
					_orderProcessingCleanups.Add((OrderProcessingCleanupEntity)relatedEntity);
					break;
				case "WorkItems":
					_workItems.Add((OrderWorkItemEntity)relatedEntity);
					break;
				case "Sale":
					SetupSyncSale(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AssignedUser":
					DesetupSyncAssignedUser(false, true);
					break;
				case "InvoiceAddress":
					DesetupSyncInvoiceAddress(false, true);
					break;
				case "ShippingAddress":
					DesetupSyncShippingAddress(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				case "SchoolYear":
					DesetupSyncSchoolYear(false, true);
					break;
				case "OrderDetails":
					this.PerformRelatedEntityRemoval(_orderDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderHistories":
					this.PerformRelatedEntityRemoval(_orderHistories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderProcessingCleanups":
					this.PerformRelatedEntityRemoval(_orderProcessingCleanups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WorkItems":
					this.PerformRelatedEntityRemoval(_workItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sale":
					DesetupSyncSale(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_sale!=null)
			{
				toReturn.Add(_sale);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_assignedUser!=null)
			{
				toReturn.Add(_assignedUser);
			}
			if(_invoiceAddress!=null)
			{
				toReturn.Add(_invoiceAddress);
			}
			if(_shippingAddress!=null)
			{
				toReturn.Add(_shippingAddress);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			if(_schoolYear!=null)
			{
				toReturn.Add(_schoolYear);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_orderDetails);
			toReturn.Add(_orderHistories);
			toReturn.Add(_orderProcessingCleanups);
			toReturn.Add(_workItems);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderID)
		{
			return FetchUsingPK(orderID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetails || forceFetch || _alwaysFetchOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetails);
				_orderDetails.SuppressClearInGetMulti=!forceFetch;
				_orderDetails.EntityFactoryToUse = entityFactoryToUse;
				_orderDetails.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_orderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetails = true;
			}
			return _orderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetails'. These settings will be taken into account
		/// when the property OrderDetails is requested or GetMultiOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetails.SortClauses=sortClauses;
			_orderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderHistoryCollection GetMultiOrderHistories(bool forceFetch)
		{
			return GetMultiOrderHistories(forceFetch, _orderHistories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderHistoryCollection GetMultiOrderHistories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderHistories(forceFetch, _orderHistories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderHistoryCollection GetMultiOrderHistories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderHistories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderHistoryCollection GetMultiOrderHistories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderHistories || forceFetch || _alwaysFetchOrderHistories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderHistories);
				_orderHistories.SuppressClearInGetMulti=!forceFetch;
				_orderHistories.EntityFactoryToUse = entityFactoryToUse;
				_orderHistories.GetMultiManyToOne(this, filter);
				_orderHistories.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderHistories = true;
			}
			return _orderHistories;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderHistories'. These settings will be taken into account
		/// when the property OrderHistories is requested or GetMultiOrderHistories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderHistories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderHistories.SortClauses=sortClauses;
			_orderHistories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderProcessingCleanupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderProcessingCleanupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection GetMultiOrderProcessingCleanups(bool forceFetch)
		{
			return GetMultiOrderProcessingCleanups(forceFetch, _orderProcessingCleanups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderProcessingCleanupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderProcessingCleanupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection GetMultiOrderProcessingCleanups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderProcessingCleanups(forceFetch, _orderProcessingCleanups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderProcessingCleanupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection GetMultiOrderProcessingCleanups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderProcessingCleanups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderProcessingCleanupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection GetMultiOrderProcessingCleanups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderProcessingCleanups || forceFetch || _alwaysFetchOrderProcessingCleanups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderProcessingCleanups);
				_orderProcessingCleanups.SuppressClearInGetMulti=!forceFetch;
				_orderProcessingCleanups.EntityFactoryToUse = entityFactoryToUse;
				_orderProcessingCleanups.GetMultiManyToOne(this, filter);
				_orderProcessingCleanups.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderProcessingCleanups = true;
			}
			return _orderProcessingCleanups;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderProcessingCleanups'. These settings will be taken into account
		/// when the property OrderProcessingCleanups is requested or GetMultiOrderProcessingCleanups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderProcessingCleanups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderProcessingCleanups.SortClauses=sortClauses;
			_orderProcessingCleanups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderWorkItemCollection GetMultiWorkItems(bool forceFetch)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderWorkItemCollection GetMultiWorkItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkItems || forceFetch || _alwaysFetchWorkItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workItems);
				_workItems.SuppressClearInGetMulti=!forceFetch;
				_workItems.EntityFactoryToUse = entityFactoryToUse;
				_workItems.GetMultiManyToOne(null, this, null, filter);
				_workItems.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkItems = true;
			}
			return _workItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkItems'. These settings will be taken into account
		/// when the property WorkItems is requested or GetMultiWorkItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workItems.SortClauses=sortClauses;
			_workItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleAssignedUser()
		{
			return GetSingleAssignedUser(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleAssignedUser(bool forceFetch)
		{
			if( ( !_alreadyFetchedAssignedUser || forceFetch || _alwaysFetchAssignedUser) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingAssignee);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Assignee.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_assignedUserReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AssignedUser = newEntity;
				_alreadyFetchedAssignedUser = fetchResult;
			}
			return _assignedUser;
		}


		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleInvoiceAddress()
		{
			return GetSingleInvoiceAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleInvoiceAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoiceAddress || forceFetch || _alwaysFetchInvoiceAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingInvoiceAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoiceAddressID);
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoiceAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InvoiceAddress = newEntity;
				_alreadyFetchedInvoiceAddress = fetchResult;
			}
			return _invoiceAddress;
		}


		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleShippingAddress()
		{
			return GetSingleShippingAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleShippingAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedShippingAddress || forceFetch || _alwaysFetchShippingAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingShippingAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ShippingAddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_shippingAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ShippingAddress = newEntity;
				_alreadyFetchedShippingAddress = fetchResult;
			}
			return _shippingAddress;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSingleSchoolYear()
		{
			return GetSingleSchoolYear(false);
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSingleSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedSchoolYear || forceFetch || _alwaysFetchSchoolYear) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingSchoolYearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SchoolYearID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_schoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SchoolYear = newEntity;
				_alreadyFetchedSchoolYear = fetchResult;
			}
			return _schoolYear;
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleSale()
		{
			return GetSingleSale(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedSale || forceFetch || _alwaysFetchSale) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingOrderID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCOrderID(this.OrderID);
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_saleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Sale = newEntity;
				_alreadyFetchedSale = fetchResult;
			}
			return _sale;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AssignedUser", _assignedUser);
			toReturn.Add("InvoiceAddress", _invoiceAddress);
			toReturn.Add("ShippingAddress", _shippingAddress);
			toReturn.Add("Contract", _contract);
			toReturn.Add("PaymentOption", _paymentOption);
			toReturn.Add("SchoolYear", _schoolYear);
			toReturn.Add("OrderDetails", _orderDetails);
			toReturn.Add("OrderHistories", _orderHistories);
			toReturn.Add("OrderProcessingCleanups", _orderProcessingCleanups);
			toReturn.Add("WorkItems", _workItems);
			toReturn.Add("Sale", _sale);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The validator object for this OrderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 orderID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_orderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_orderDetails.SetContainingEntityInfo(this, "Order");

			_orderHistories = new VarioSL.Entities.CollectionClasses.OrderHistoryCollection();
			_orderHistories.SetContainingEntityInfo(this, "Order");

			_orderProcessingCleanups = new VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection();
			_orderProcessingCleanups.SetContainingEntityInfo(this, "Order");

			_workItems = new VarioSL.Entities.CollectionClasses.OrderWorkItemCollection();
			_workItems.SetContainingEntityInfo(this, "Order");
			_assignedUserReturnsNewIfNotFound = false;
			_invoiceAddressReturnsNewIfNotFound = false;
			_shippingAddressReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			_schoolYearReturnsNewIfNotFound = false;
			_saleReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Assignee", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FulfillDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceAddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsClosed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsMail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Ordertype", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentProviderToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShippingAddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoucherNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _assignedUser</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAssignedUser(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _assignedUser, new PropertyChangedEventHandler( OnAssignedUserPropertyChanged ), "AssignedUser", VarioSL.Entities.RelationClasses.StaticOrderRelations.UserListEntityUsingAssigneeStatic, true, signalRelatedEntity, "Orders", resetFKFields, new int[] { (int)OrderFieldIndex.Assignee } );		
			_assignedUser = null;
		}
		
		/// <summary> setups the sync logic for member _assignedUser</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAssignedUser(IEntityCore relatedEntity)
		{
			if(_assignedUser!=relatedEntity)
			{		
				DesetupSyncAssignedUser(true, true);
				_assignedUser = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _assignedUser, new PropertyChangedEventHandler( OnAssignedUserPropertyChanged ), "AssignedUser", VarioSL.Entities.RelationClasses.StaticOrderRelations.UserListEntityUsingAssigneeStatic, true, ref _alreadyFetchedAssignedUser, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAssignedUserPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoiceAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoiceAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoiceAddress, new PropertyChangedEventHandler( OnInvoiceAddressPropertyChanged ), "InvoiceAddress", VarioSL.Entities.RelationClasses.StaticOrderRelations.AddressEntityUsingInvoiceAddressIDStatic, true, signalRelatedEntity, "InvoiceAddressOrders", resetFKFields, new int[] { (int)OrderFieldIndex.InvoiceAddressID } );		
			_invoiceAddress = null;
		}
		
		/// <summary> setups the sync logic for member _invoiceAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoiceAddress(IEntityCore relatedEntity)
		{
			if(_invoiceAddress!=relatedEntity)
			{		
				DesetupSyncInvoiceAddress(true, true);
				_invoiceAddress = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoiceAddress, new PropertyChangedEventHandler( OnInvoiceAddressPropertyChanged ), "InvoiceAddress", VarioSL.Entities.RelationClasses.StaticOrderRelations.AddressEntityUsingInvoiceAddressIDStatic, true, ref _alreadyFetchedInvoiceAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoiceAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _shippingAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncShippingAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _shippingAddress, new PropertyChangedEventHandler( OnShippingAddressPropertyChanged ), "ShippingAddress", VarioSL.Entities.RelationClasses.StaticOrderRelations.AddressEntityUsingShippingAddressIDStatic, true, signalRelatedEntity, "ShippingAddressOrders", resetFKFields, new int[] { (int)OrderFieldIndex.ShippingAddressID } );		
			_shippingAddress = null;
		}
		
		/// <summary> setups the sync logic for member _shippingAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncShippingAddress(IEntityCore relatedEntity)
		{
			if(_shippingAddress!=relatedEntity)
			{		
				DesetupSyncShippingAddress(true, true);
				_shippingAddress = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _shippingAddress, new PropertyChangedEventHandler( OnShippingAddressPropertyChanged ), "ShippingAddress", VarioSL.Entities.RelationClasses.StaticOrderRelations.AddressEntityUsingShippingAddressIDStatic, true, ref _alreadyFetchedShippingAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnShippingAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticOrderRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Orders", resetFKFields, new int[] { (int)OrderFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticOrderRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticOrderRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, signalRelatedEntity, "Orders", resetFKFields, new int[] { (int)OrderFieldIndex.PaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticOrderRelations.PaymentOptionEntityUsingPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _schoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticOrderRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, signalRelatedEntity, "Orders", resetFKFields, new int[] { (int)OrderFieldIndex.SchoolYearID } );		
			_schoolYear = null;
		}
		
		/// <summary> setups the sync logic for member _schoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSchoolYear(IEntityCore relatedEntity)
		{
			if(_schoolYear!=relatedEntity)
			{		
				DesetupSyncSchoolYear(true, true);
				_schoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticOrderRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, ref _alreadyFetchedSchoolYear, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _sale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticOrderRelations.SaleEntityUsingOrderIDStatic, false, signalRelatedEntity, "Order", false, new int[] { (int)OrderFieldIndex.OrderID } );
			_sale = null;
		}
	
		/// <summary> setups the sync logic for member _sale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSale(IEntityCore relatedEntity)
		{
			if(_sale!=relatedEntity)
			{
				DesetupSyncSale(true, true);
				_sale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticOrderRelations.SaleEntityUsingOrderIDStatic, false, ref _alreadyFetchedSale, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderID">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 orderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderFieldIndex.OrderID].ForcedCurrentValueWrite(orderID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderRelations Relations
		{
			get	{ return new OrderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetails")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderHistories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderHistoryCollection(), (IEntityRelation)GetRelationsForField("OrderHistories")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.OrderHistoryEntity, 0, null, null, null, "OrderHistories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderProcessingCleanup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderProcessingCleanups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection(), (IEntityRelation)GetRelationsForField("OrderProcessingCleanups")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.OrderProcessingCleanupEntity, 0, null, null, null, "OrderProcessingCleanups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderWorkItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderWorkItemCollection(), (IEntityRelation)GetRelationsForField("WorkItems")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.OrderWorkItemEntity, 0, null, null, null, "WorkItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAssignedUser
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("AssignedUser")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "AssignedUser", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoiceAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("InvoiceAddress")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "InvoiceAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShippingAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("ShippingAddress")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "ShippingAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("SchoolYear")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "SchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sale")[0], (int)VarioSL.Entities.EntityType.OrderEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Assignee property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ASSIGNEE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Assignee
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderFieldIndex.Assignee, false); }
			set	{ SetValue((int)OrderFieldIndex.Assignee, value, true); }
		}

		/// <summary> The ContractID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderFieldIndex.ContractID, false); }
			set	{ SetValue((int)OrderFieldIndex.ContractID, value, true); }
		}

		/// <summary> The FulfillDate property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."FULFILLDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FulfillDate
		{
			get { return (System.DateTime)GetValue((int)OrderFieldIndex.FulfillDate, true); }
			set	{ SetValue((int)OrderFieldIndex.FulfillDate, value, true); }
		}

		/// <summary> The InvoiceAddressID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."INVOICEADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InvoiceAddressID
		{
			get { return (System.Int64)GetValue((int)OrderFieldIndex.InvoiceAddressID, true); }
			set	{ SetValue((int)OrderFieldIndex.InvoiceAddressID, value, true); }
		}

		/// <summary> The IsClosed property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ISCLOSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsClosed
		{
			get { return (Nullable<System.Boolean>)GetValue((int)OrderFieldIndex.IsClosed, false); }
			set	{ SetValue((int)OrderFieldIndex.IsClosed, value, true); }
		}

		/// <summary> The IsMail property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ISMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsMail
		{
			get { return (System.Boolean)GetValue((int)OrderFieldIndex.IsMail, true); }
			set	{ SetValue((int)OrderFieldIndex.IsMail, value, true); }
		}

		/// <summary> The LastModified property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)OrderFieldIndex.LastModified, true); }
			set	{ SetValue((int)OrderFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.LastUser, true); }
			set	{ SetValue((int)OrderFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrderDate property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ORDERDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime OrderDate
		{
			get { return (System.DateTime)GetValue((int)OrderFieldIndex.OrderDate, true); }
			set	{ SetValue((int)OrderFieldIndex.OrderDate, value, true); }
		}

		/// <summary> The OrderID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ORDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrderID
		{
			get { return (System.Int64)GetValue((int)OrderFieldIndex.OrderID, true); }
			set	{ SetValue((int)OrderFieldIndex.OrderID, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String OrderNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The OrderSource property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ORDERSOURCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.Source OrderSource
		{
			get { return (VarioSL.Entities.Enumerations.Source)GetValue((int)OrderFieldIndex.OrderSource, true); }
			set	{ SetValue((int)OrderFieldIndex.OrderSource, value, true); }
		}

		/// <summary> The Ordertype property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."ORDERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Ordertype
		{
			get { return (System.Int32)GetValue((int)OrderFieldIndex.Ordertype, true); }
			set	{ SetValue((int)OrderFieldIndex.Ordertype, value, true); }
		}

		/// <summary> The PaymentOptionID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."PAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderFieldIndex.PaymentOptionID, false); }
			set	{ SetValue((int)OrderFieldIndex.PaymentOptionID, value, true); }
		}

		/// <summary> The PaymentProviderToken property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."PAYMENTPROVIDERTOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentProviderToken
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.PaymentProviderToken, true); }
			set	{ SetValue((int)OrderFieldIndex.PaymentProviderToken, value, true); }
		}

		/// <summary> The Priority property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."PRIORITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Priority
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderFieldIndex.Priority, false); }
			set	{ SetValue((int)OrderFieldIndex.Priority, value, true); }
		}

		/// <summary> The SchoolYearID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."SCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SchoolYearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderFieldIndex.SchoolYearID, false); }
			set	{ SetValue((int)OrderFieldIndex.SchoolYearID, value, true); }
		}

		/// <summary> The ShippingAddressID property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."SHIPPINGADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShippingAddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderFieldIndex.ShippingAddressID, false); }
			set	{ SetValue((int)OrderFieldIndex.ShippingAddressID, value, true); }
		}

		/// <summary> The State property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.OrderState State
		{
			get { return (VarioSL.Entities.Enumerations.OrderState)GetValue((int)OrderFieldIndex.State, true); }
			set	{ SetValue((int)OrderFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)OrderFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)OrderFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VoucherNumber property of the Entity Order<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDER"."VOUCHERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VoucherNumber
		{
			get { return (System.String)GetValue((int)OrderFieldIndex.VoucherNumber, true); }
			set	{ SetValue((int)OrderFieldIndex.VoucherNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection OrderDetails
		{
			get	{ return GetMultiOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetails. When set to true, OrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetails is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetails
		{
			get	{ return _alwaysFetchOrderDetails; }
			set	{ _alwaysFetchOrderDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetails already has been fetched. Setting this property to false when OrderDetails has been fetched
		/// will clear the OrderDetails collection well. Setting this property to true while OrderDetails hasn't been fetched disables lazy loading for OrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetails
		{
			get { return _alreadyFetchedOrderDetails;}
			set 
			{
				if(_alreadyFetchedOrderDetails && !value && (_orderDetails != null))
				{
					_orderDetails.Clear();
				}
				_alreadyFetchedOrderDetails = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderHistories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderHistoryCollection OrderHistories
		{
			get	{ return GetMultiOrderHistories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderHistories. When set to true, OrderHistories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderHistories is accessed. You can always execute/ a forced fetch by calling GetMultiOrderHistories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderHistories
		{
			get	{ return _alwaysFetchOrderHistories; }
			set	{ _alwaysFetchOrderHistories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderHistories already has been fetched. Setting this property to false when OrderHistories has been fetched
		/// will clear the OrderHistories collection well. Setting this property to true while OrderHistories hasn't been fetched disables lazy loading for OrderHistories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderHistories
		{
			get { return _alreadyFetchedOrderHistories;}
			set 
			{
				if(_alreadyFetchedOrderHistories && !value && (_orderHistories != null))
				{
					_orderHistories.Clear();
				}
				_alreadyFetchedOrderHistories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderProcessingCleanupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderProcessingCleanups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderProcessingCleanupCollection OrderProcessingCleanups
		{
			get	{ return GetMultiOrderProcessingCleanups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderProcessingCleanups. When set to true, OrderProcessingCleanups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderProcessingCleanups is accessed. You can always execute/ a forced fetch by calling GetMultiOrderProcessingCleanups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderProcessingCleanups
		{
			get	{ return _alwaysFetchOrderProcessingCleanups; }
			set	{ _alwaysFetchOrderProcessingCleanups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderProcessingCleanups already has been fetched. Setting this property to false when OrderProcessingCleanups has been fetched
		/// will clear the OrderProcessingCleanups collection well. Setting this property to true while OrderProcessingCleanups hasn't been fetched disables lazy loading for OrderProcessingCleanups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderProcessingCleanups
		{
			get { return _alreadyFetchedOrderProcessingCleanups;}
			set 
			{
				if(_alreadyFetchedOrderProcessingCleanups && !value && (_orderProcessingCleanups != null))
				{
					_orderProcessingCleanups.Clear();
				}
				_alreadyFetchedOrderProcessingCleanups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderWorkItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderWorkItemCollection WorkItems
		{
			get	{ return GetMultiWorkItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItems. When set to true, WorkItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItems is accessed. You can always execute/ a forced fetch by calling GetMultiWorkItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItems
		{
			get	{ return _alwaysFetchWorkItems; }
			set	{ _alwaysFetchWorkItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItems already has been fetched. Setting this property to false when WorkItems has been fetched
		/// will clear the WorkItems collection well. Setting this property to true while WorkItems hasn't been fetched disables lazy loading for WorkItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItems
		{
			get { return _alreadyFetchedWorkItems;}
			set 
			{
				if(_alreadyFetchedWorkItems && !value && (_workItems != null))
				{
					_workItems.Clear();
				}
				_alreadyFetchedWorkItems = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAssignedUser()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity AssignedUser
		{
			get	{ return GetSingleAssignedUser(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAssignedUser(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Orders", "AssignedUser", _assignedUser, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AssignedUser. When set to true, AssignedUser is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AssignedUser is accessed. You can always execute a forced fetch by calling GetSingleAssignedUser(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAssignedUser
		{
			get	{ return _alwaysFetchAssignedUser; }
			set	{ _alwaysFetchAssignedUser = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AssignedUser already has been fetched. Setting this property to false when AssignedUser has been fetched
		/// will set AssignedUser to null as well. Setting this property to true while AssignedUser hasn't been fetched disables lazy loading for AssignedUser</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAssignedUser
		{
			get { return _alreadyFetchedAssignedUser;}
			set 
			{
				if(_alreadyFetchedAssignedUser && !value)
				{
					this.AssignedUser = null;
				}
				_alreadyFetchedAssignedUser = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AssignedUser is not found
		/// in the database. When set to true, AssignedUser will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AssignedUserReturnsNewIfNotFound
		{
			get	{ return _assignedUserReturnsNewIfNotFound; }
			set { _assignedUserReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoiceAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity InvoiceAddress
		{
			get	{ return GetSingleInvoiceAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoiceAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "InvoiceAddressOrders", "InvoiceAddress", _invoiceAddress, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InvoiceAddress. When set to true, InvoiceAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InvoiceAddress is accessed. You can always execute a forced fetch by calling GetSingleInvoiceAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoiceAddress
		{
			get	{ return _alwaysFetchInvoiceAddress; }
			set	{ _alwaysFetchInvoiceAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InvoiceAddress already has been fetched. Setting this property to false when InvoiceAddress has been fetched
		/// will set InvoiceAddress to null as well. Setting this property to true while InvoiceAddress hasn't been fetched disables lazy loading for InvoiceAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoiceAddress
		{
			get { return _alreadyFetchedInvoiceAddress;}
			set 
			{
				if(_alreadyFetchedInvoiceAddress && !value)
				{
					this.InvoiceAddress = null;
				}
				_alreadyFetchedInvoiceAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InvoiceAddress is not found
		/// in the database. When set to true, InvoiceAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoiceAddressReturnsNewIfNotFound
		{
			get	{ return _invoiceAddressReturnsNewIfNotFound; }
			set { _invoiceAddressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleShippingAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity ShippingAddress
		{
			get	{ return GetSingleShippingAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncShippingAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ShippingAddressOrders", "ShippingAddress", _shippingAddress, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ShippingAddress. When set to true, ShippingAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ShippingAddress is accessed. You can always execute a forced fetch by calling GetSingleShippingAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShippingAddress
		{
			get	{ return _alwaysFetchShippingAddress; }
			set	{ _alwaysFetchShippingAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ShippingAddress already has been fetched. Setting this property to false when ShippingAddress has been fetched
		/// will set ShippingAddress to null as well. Setting this property to true while ShippingAddress hasn't been fetched disables lazy loading for ShippingAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShippingAddress
		{
			get { return _alreadyFetchedShippingAddress;}
			set 
			{
				if(_alreadyFetchedShippingAddress && !value)
				{
					this.ShippingAddress = null;
				}
				_alreadyFetchedShippingAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ShippingAddress is not found
		/// in the database. When set to true, ShippingAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ShippingAddressReturnsNewIfNotFound
		{
			get	{ return _shippingAddressReturnsNewIfNotFound; }
			set { _shippingAddressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Orders", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Orders", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity SchoolYear
		{
			get	{ return GetSingleSchoolYear(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSchoolYear(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Orders", "SchoolYear", _schoolYear, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SchoolYear. When set to true, SchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SchoolYear is accessed. You can always execute a forced fetch by calling GetSingleSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSchoolYear
		{
			get	{ return _alwaysFetchSchoolYear; }
			set	{ _alwaysFetchSchoolYear = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SchoolYear already has been fetched. Setting this property to false when SchoolYear has been fetched
		/// will set SchoolYear to null as well. Setting this property to true while SchoolYear hasn't been fetched disables lazy loading for SchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSchoolYear
		{
			get { return _alreadyFetchedSchoolYear;}
			set 
			{
				if(_alreadyFetchedSchoolYear && !value)
				{
					this.SchoolYear = null;
				}
				_alreadyFetchedSchoolYear = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SchoolYear is not found
		/// in the database. When set to true, SchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SchoolYearReturnsNewIfNotFound
		{
			get	{ return _schoolYearReturnsNewIfNotFound; }
			set { _schoolYearReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity Sale
		{
			get	{ return GetSingleSale(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSale(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_sale !=null);
						DesetupSyncSale(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Sale");
						}
					}
					else
					{
						if(_sale!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Order");
							SetupSyncSale(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Sale. When set to true, Sale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sale is accessed. You can always execute a forced fetch by calling GetSingleSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSale
		{
			get	{ return _alwaysFetchSale; }
			set	{ _alwaysFetchSale = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Sale already has been fetched. Setting this property to false when Sale has been fetched
		/// will set Sale to null as well. Setting this property to true while Sale hasn't been fetched disables lazy loading for Sale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSale
		{
			get { return _alreadyFetchedSale;}
			set 
			{
				if(_alreadyFetchedSale && !value)
				{
					this.Sale = null;
				}
				_alreadyFetchedSale = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Sale is not found
		/// in the database. When set to true, Sale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SaleReturnsNewIfNotFound
		{
			get	{ return _saleReturnsNewIfNotFound; }
			set	{ _saleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
