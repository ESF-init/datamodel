﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Entitlement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class EntitlementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.EntitlementToProductCollection	_entitlementToProducts;
		private bool	_alwaysFetchEntitlementToProducts, _alreadyFetchedEntitlementToProducts;
		private VarioSL.Entities.CollectionClasses.TicketAssignmentCollection	_ticketAssignments;
		private bool	_alwaysFetchTicketAssignments, _alreadyFetchedTicketAssignments;
		private EntitlementTypeEntity _entitlementType;
		private bool	_alwaysFetchEntitlementType, _alreadyFetchedEntitlementType, _entitlementTypeReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EntitlementType</summary>
			public static readonly string EntitlementType = "EntitlementType";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name EntitlementToProducts</summary>
			public static readonly string EntitlementToProducts = "EntitlementToProducts";
			/// <summary>Member name TicketAssignments</summary>
			public static readonly string TicketAssignments = "TicketAssignments";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntitlementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public EntitlementEntity() :base("EntitlementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		public EntitlementEntity(System.Int64 entitlementID):base("EntitlementEntity")
		{
			InitClassFetch(entitlementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EntitlementEntity(System.Int64 entitlementID, IPrefetchPath prefetchPathToUse):base("EntitlementEntity")
		{
			InitClassFetch(entitlementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="validator">The custom validator object for this EntitlementEntity</param>
		public EntitlementEntity(System.Int64 entitlementID, IValidator validator):base("EntitlementEntity")
		{
			InitClassFetch(entitlementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntitlementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_entitlementToProducts = (VarioSL.Entities.CollectionClasses.EntitlementToProductCollection)info.GetValue("_entitlementToProducts", typeof(VarioSL.Entities.CollectionClasses.EntitlementToProductCollection));
			_alwaysFetchEntitlementToProducts = info.GetBoolean("_alwaysFetchEntitlementToProducts");
			_alreadyFetchedEntitlementToProducts = info.GetBoolean("_alreadyFetchedEntitlementToProducts");

			_ticketAssignments = (VarioSL.Entities.CollectionClasses.TicketAssignmentCollection)info.GetValue("_ticketAssignments", typeof(VarioSL.Entities.CollectionClasses.TicketAssignmentCollection));
			_alwaysFetchTicketAssignments = info.GetBoolean("_alwaysFetchTicketAssignments");
			_alreadyFetchedTicketAssignments = info.GetBoolean("_alreadyFetchedTicketAssignments");
			_entitlementType = (EntitlementTypeEntity)info.GetValue("_entitlementType", typeof(EntitlementTypeEntity));
			if(_entitlementType!=null)
			{
				_entitlementType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entitlementTypeReturnsNewIfNotFound = info.GetBoolean("_entitlementTypeReturnsNewIfNotFound");
			_alwaysFetchEntitlementType = info.GetBoolean("_alwaysFetchEntitlementType");
			_alreadyFetchedEntitlementType = info.GetBoolean("_alreadyFetchedEntitlementType");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EntitlementFieldIndex)fieldIndex)
			{
				case EntitlementFieldIndex.EntitlementTypeID:
					DesetupSyncEntitlementType(true, false);
					_alreadyFetchedEntitlementType = false;
					break;
				case EntitlementFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEntitlementToProducts = (_entitlementToProducts.Count > 0);
			_alreadyFetchedTicketAssignments = (_ticketAssignments.Count > 0);
			_alreadyFetchedEntitlementType = (_entitlementType != null);
			_alreadyFetchedPerson = (_person != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EntitlementType":
					toReturn.Add(Relations.EntitlementTypeEntityUsingEntitlementTypeID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "EntitlementToProducts":
					toReturn.Add(Relations.EntitlementToProductEntityUsingEntitlementID);
					break;
				case "TicketAssignments":
					toReturn.Add(Relations.TicketAssignmentEntityUsingEntitlementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_entitlementToProducts", (!this.MarkedForDeletion?_entitlementToProducts:null));
			info.AddValue("_alwaysFetchEntitlementToProducts", _alwaysFetchEntitlementToProducts);
			info.AddValue("_alreadyFetchedEntitlementToProducts", _alreadyFetchedEntitlementToProducts);
			info.AddValue("_ticketAssignments", (!this.MarkedForDeletion?_ticketAssignments:null));
			info.AddValue("_alwaysFetchTicketAssignments", _alwaysFetchTicketAssignments);
			info.AddValue("_alreadyFetchedTicketAssignments", _alreadyFetchedTicketAssignments);
			info.AddValue("_entitlementType", (!this.MarkedForDeletion?_entitlementType:null));
			info.AddValue("_entitlementTypeReturnsNewIfNotFound", _entitlementTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntitlementType", _alwaysFetchEntitlementType);
			info.AddValue("_alreadyFetchedEntitlementType", _alreadyFetchedEntitlementType);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EntitlementType":
					_alreadyFetchedEntitlementType = true;
					this.EntitlementType = (EntitlementTypeEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "EntitlementToProducts":
					_alreadyFetchedEntitlementToProducts = true;
					if(entity!=null)
					{
						this.EntitlementToProducts.Add((EntitlementToProductEntity)entity);
					}
					break;
				case "TicketAssignments":
					_alreadyFetchedTicketAssignments = true;
					if(entity!=null)
					{
						this.TicketAssignments.Add((TicketAssignmentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EntitlementType":
					SetupSyncEntitlementType(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "EntitlementToProducts":
					_entitlementToProducts.Add((EntitlementToProductEntity)relatedEntity);
					break;
				case "TicketAssignments":
					_ticketAssignments.Add((TicketAssignmentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EntitlementType":
					DesetupSyncEntitlementType(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "EntitlementToProducts":
					this.PerformRelatedEntityRemoval(_entitlementToProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketAssignments":
					this.PerformRelatedEntityRemoval(_ticketAssignments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_entitlementType!=null)
			{
				toReturn.Add(_entitlementType);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_entitlementToProducts);
			toReturn.Add(_ticketAssignments);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entitlementID)
		{
			return FetchUsingPK(entitlementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entitlementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entitlementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entitlementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entitlementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 entitlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entitlementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntitlementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntitlementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch)
		{
			return GetMultiEntitlementToProducts(forceFetch, _entitlementToProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntitlementToProducts(forceFetch, _entitlementToProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntitlementToProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntitlementToProducts || forceFetch || _alwaysFetchEntitlementToProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entitlementToProducts);
				_entitlementToProducts.SuppressClearInGetMulti=!forceFetch;
				_entitlementToProducts.EntityFactoryToUse = entityFactoryToUse;
				_entitlementToProducts.GetMultiManyToOne(this, null, filter);
				_entitlementToProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedEntitlementToProducts = true;
			}
			return _entitlementToProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntitlementToProducts'. These settings will be taken into account
		/// when the property EntitlementToProducts is requested or GetMultiEntitlementToProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntitlementToProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entitlementToProducts.SortClauses=sortClauses;
			_entitlementToProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketAssignments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketAssignments || forceFetch || _alwaysFetchTicketAssignments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketAssignments);
				_ticketAssignments.SuppressClearInGetMulti=!forceFetch;
				_ticketAssignments.EntityFactoryToUse = entityFactoryToUse;
				_ticketAssignments.GetMultiManyToOne(null, null, null, this, null, filter);
				_ticketAssignments.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketAssignments = true;
			}
			return _ticketAssignments;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketAssignments'. These settings will be taken into account
		/// when the property TicketAssignments is requested or GetMultiTicketAssignments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketAssignments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketAssignments.SortClauses=sortClauses;
			_ticketAssignments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'EntitlementTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntitlementTypeEntity' which is related to this entity.</returns>
		public EntitlementTypeEntity GetSingleEntitlementType()
		{
			return GetSingleEntitlementType(false);
		}

		/// <summary> Retrieves the related entity of type 'EntitlementTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntitlementTypeEntity' which is related to this entity.</returns>
		public virtual EntitlementTypeEntity GetSingleEntitlementType(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntitlementType || forceFetch || _alwaysFetchEntitlementType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntitlementTypeEntityUsingEntitlementTypeID);
				EntitlementTypeEntity newEntity = new EntitlementTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntitlementTypeID);
				}
				if(fetchResult)
				{
					newEntity = (EntitlementTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entitlementTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntitlementType = newEntity;
				_alreadyFetchedEntitlementType = fetchResult;
			}
			return _entitlementType;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EntitlementType", _entitlementType);
			toReturn.Add("Person", _person);
			toReturn.Add("EntitlementToProducts", _entitlementToProducts);
			toReturn.Add("TicketAssignments", _ticketAssignments);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="validator">The validator object for this EntitlementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 entitlementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entitlementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_entitlementToProducts = new VarioSL.Entities.CollectionClasses.EntitlementToProductCollection();
			_entitlementToProducts.SetContainingEntityInfo(this, "Entitlement");

			_ticketAssignments = new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection();
			_ticketAssignments.SetContainingEntityInfo(this, "Entitlement");
			_entitlementTypeReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Document", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntitlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntitlementTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _entitlementType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntitlementType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entitlementType, new PropertyChangedEventHandler( OnEntitlementTypePropertyChanged ), "EntitlementType", VarioSL.Entities.RelationClasses.StaticEntitlementRelations.EntitlementTypeEntityUsingEntitlementTypeIDStatic, true, signalRelatedEntity, "Entitlements", resetFKFields, new int[] { (int)EntitlementFieldIndex.EntitlementTypeID } );		
			_entitlementType = null;
		}
		
		/// <summary> setups the sync logic for member _entitlementType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntitlementType(IEntityCore relatedEntity)
		{
			if(_entitlementType!=relatedEntity)
			{		
				DesetupSyncEntitlementType(true, true);
				_entitlementType = (EntitlementTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entitlementType, new PropertyChangedEventHandler( OnEntitlementTypePropertyChanged ), "EntitlementType", VarioSL.Entities.RelationClasses.StaticEntitlementRelations.EntitlementTypeEntityUsingEntitlementTypeIDStatic, true, ref _alreadyFetchedEntitlementType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntitlementTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticEntitlementRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "Entitlements", resetFKFields, new int[] { (int)EntitlementFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticEntitlementRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entitlementID">PK value for Entitlement which data should be fetched into this Entitlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 entitlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntitlementFieldIndex.EntitlementID].ForcedCurrentValueWrite(entitlementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntitlementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntitlementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntitlementRelations Relations
		{
			get	{ return new EntitlementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntitlementToProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntitlementToProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EntitlementToProductCollection(), (IEntityRelation)GetRelationsForField("EntitlementToProducts")[0], (int)VarioSL.Entities.EntityType.EntitlementEntity, (int)VarioSL.Entities.EntityType.EntitlementToProductEntity, 0, null, null, null, "EntitlementToProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketAssignment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketAssignments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection(), (IEntityRelation)GetRelationsForField("TicketAssignments")[0], (int)VarioSL.Entities.EntityType.EntitlementEntity, (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, 0, null, null, null, "TicketAssignments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntitlementType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntitlementType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EntitlementTypeCollection(), (IEntityRelation)GetRelationsForField("EntitlementType")[0], (int)VarioSL.Entities.EntityType.EntitlementEntity, (int)VarioSL.Entities.EntityType.EntitlementTypeEntity, 0, null, null, null, "EntitlementType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.EntitlementEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)EntitlementFieldIndex.ClientID, false); }
			set	{ SetValue((int)EntitlementFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Document property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."DOCUMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Document
		{
			get { return (System.String)GetValue((int)EntitlementFieldIndex.Document, true); }
			set	{ SetValue((int)EntitlementFieldIndex.Document, value, true); }
		}

		/// <summary> The EntitlementID property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."ENTITLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 EntitlementID
		{
			get { return (System.Int64)GetValue((int)EntitlementFieldIndex.EntitlementID, true); }
			set	{ SetValue((int)EntitlementFieldIndex.EntitlementID, value, true); }
		}

		/// <summary> The EntitlementTypeID property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."ENTITLEMENTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 EntitlementTypeID
		{
			get { return (System.Int64)GetValue((int)EntitlementFieldIndex.EntitlementTypeID, true); }
			set	{ SetValue((int)EntitlementFieldIndex.EntitlementTypeID, value, true); }
		}

		/// <summary> The ExternalID property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."EXTERNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalID
		{
			get { return (System.String)GetValue((int)EntitlementFieldIndex.ExternalID, true); }
			set	{ SetValue((int)EntitlementFieldIndex.ExternalID, value, true); }
		}

		/// <summary> The LastModified property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)EntitlementFieldIndex.LastModified, true); }
			set	{ SetValue((int)EntitlementFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)EntitlementFieldIndex.LastUser, true); }
			set	{ SetValue((int)EntitlementFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PersonID property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)EntitlementFieldIndex.PersonID, true); }
			set	{ SetValue((int)EntitlementFieldIndex.PersonID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)EntitlementFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)EntitlementFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntitlementFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)EntitlementFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Entitlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ENTITLEMENT"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntitlementFieldIndex.ValidTo, false); }
			set	{ SetValue((int)EntitlementFieldIndex.ValidTo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntitlementToProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementToProductCollection EntitlementToProducts
		{
			get	{ return GetMultiEntitlementToProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntitlementToProducts. When set to true, EntitlementToProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntitlementToProducts is accessed. You can always execute/ a forced fetch by calling GetMultiEntitlementToProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntitlementToProducts
		{
			get	{ return _alwaysFetchEntitlementToProducts; }
			set	{ _alwaysFetchEntitlementToProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntitlementToProducts already has been fetched. Setting this property to false when EntitlementToProducts has been fetched
		/// will clear the EntitlementToProducts collection well. Setting this property to true while EntitlementToProducts hasn't been fetched disables lazy loading for EntitlementToProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntitlementToProducts
		{
			get { return _alreadyFetchedEntitlementToProducts;}
			set 
			{
				if(_alreadyFetchedEntitlementToProducts && !value && (_entitlementToProducts != null))
				{
					_entitlementToProducts.Clear();
				}
				_alreadyFetchedEntitlementToProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketAssignments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection TicketAssignments
		{
			get	{ return GetMultiTicketAssignments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketAssignments. When set to true, TicketAssignments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketAssignments is accessed. You can always execute/ a forced fetch by calling GetMultiTicketAssignments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketAssignments
		{
			get	{ return _alwaysFetchTicketAssignments; }
			set	{ _alwaysFetchTicketAssignments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketAssignments already has been fetched. Setting this property to false when TicketAssignments has been fetched
		/// will clear the TicketAssignments collection well. Setting this property to true while TicketAssignments hasn't been fetched disables lazy loading for TicketAssignments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketAssignments
		{
			get { return _alreadyFetchedTicketAssignments;}
			set 
			{
				if(_alreadyFetchedTicketAssignments && !value && (_ticketAssignments != null))
				{
					_ticketAssignments.Clear();
				}
				_alreadyFetchedTicketAssignments = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'EntitlementTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntitlementType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EntitlementTypeEntity EntitlementType
		{
			get	{ return GetSingleEntitlementType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntitlementType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Entitlements", "EntitlementType", _entitlementType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntitlementType. When set to true, EntitlementType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntitlementType is accessed. You can always execute a forced fetch by calling GetSingleEntitlementType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntitlementType
		{
			get	{ return _alwaysFetchEntitlementType; }
			set	{ _alwaysFetchEntitlementType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntitlementType already has been fetched. Setting this property to false when EntitlementType has been fetched
		/// will set EntitlementType to null as well. Setting this property to true while EntitlementType hasn't been fetched disables lazy loading for EntitlementType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntitlementType
		{
			get { return _alreadyFetchedEntitlementType;}
			set 
			{
				if(_alreadyFetchedEntitlementType && !value)
				{
					this.EntitlementType = null;
				}
				_alreadyFetchedEntitlementType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntitlementType is not found
		/// in the database. When set to true, EntitlementType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EntitlementTypeReturnsNewIfNotFound
		{
			get	{ return _entitlementTypeReturnsNewIfNotFound; }
			set { _entitlementTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Entitlements", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.EntitlementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
