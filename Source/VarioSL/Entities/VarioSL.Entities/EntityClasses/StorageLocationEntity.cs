﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'StorageLocation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class StorageLocationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardCollection	_cards;
		private bool	_alwaysFetchCards, _alreadyFetchedCards;
		private VarioSL.Entities.CollectionClasses.JobCardImportCollection	_jobCardImports;
		private bool	_alwaysFetchJobCardImports, _alreadyFetchedJobCardImports;
		private VarioSL.Entities.CollectionClasses.StockTransferCollection	_newLocationStockTransfers;
		private bool	_alwaysFetchNewLocationStockTransfers, _alreadyFetchedNewLocationStockTransfers;
		private VarioSL.Entities.CollectionClasses.StockTransferCollection	_oldLocationStockTransfers;
		private bool	_alwaysFetchOldLocationStockTransfers, _alreadyFetchedOldLocationStockTransfers;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private AddressEntity _address;
		private bool	_alwaysFetchAddress, _alreadyFetchedAddress, _addressReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
			/// <summary>Member name Cards</summary>
			public static readonly string Cards = "Cards";
			/// <summary>Member name JobCardImports</summary>
			public static readonly string JobCardImports = "JobCardImports";
			/// <summary>Member name NewLocationStockTransfers</summary>
			public static readonly string NewLocationStockTransfers = "NewLocationStockTransfers";
			/// <summary>Member name OldLocationStockTransfers</summary>
			public static readonly string OldLocationStockTransfers = "OldLocationStockTransfers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static StorageLocationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public StorageLocationEntity() :base("StorageLocationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		public StorageLocationEntity(System.Int64 storageLocationID):base("StorageLocationEntity")
		{
			InitClassFetch(storageLocationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public StorageLocationEntity(System.Int64 storageLocationID, IPrefetchPath prefetchPathToUse):base("StorageLocationEntity")
		{
			InitClassFetch(storageLocationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="validator">The custom validator object for this StorageLocationEntity</param>
		public StorageLocationEntity(System.Int64 storageLocationID, IValidator validator):base("StorageLocationEntity")
		{
			InitClassFetch(storageLocationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StorageLocationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCards = info.GetBoolean("_alwaysFetchCards");
			_alreadyFetchedCards = info.GetBoolean("_alreadyFetchedCards");

			_jobCardImports = (VarioSL.Entities.CollectionClasses.JobCardImportCollection)info.GetValue("_jobCardImports", typeof(VarioSL.Entities.CollectionClasses.JobCardImportCollection));
			_alwaysFetchJobCardImports = info.GetBoolean("_alwaysFetchJobCardImports");
			_alreadyFetchedJobCardImports = info.GetBoolean("_alreadyFetchedJobCardImports");

			_newLocationStockTransfers = (VarioSL.Entities.CollectionClasses.StockTransferCollection)info.GetValue("_newLocationStockTransfers", typeof(VarioSL.Entities.CollectionClasses.StockTransferCollection));
			_alwaysFetchNewLocationStockTransfers = info.GetBoolean("_alwaysFetchNewLocationStockTransfers");
			_alreadyFetchedNewLocationStockTransfers = info.GetBoolean("_alreadyFetchedNewLocationStockTransfers");

			_oldLocationStockTransfers = (VarioSL.Entities.CollectionClasses.StockTransferCollection)info.GetValue("_oldLocationStockTransfers", typeof(VarioSL.Entities.CollectionClasses.StockTransferCollection));
			_alwaysFetchOldLocationStockTransfers = info.GetBoolean("_alwaysFetchOldLocationStockTransfers");
			_alreadyFetchedOldLocationStockTransfers = info.GetBoolean("_alreadyFetchedOldLocationStockTransfers");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_address = (AddressEntity)info.GetValue("_address", typeof(AddressEntity));
			if(_address!=null)
			{
				_address.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressReturnsNewIfNotFound = info.GetBoolean("_addressReturnsNewIfNotFound");
			_alwaysFetchAddress = info.GetBoolean("_alwaysFetchAddress");
			_alreadyFetchedAddress = info.GetBoolean("_alreadyFetchedAddress");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((StorageLocationFieldIndex)fieldIndex)
			{
				case StorageLocationFieldIndex.AddressID:
					DesetupSyncAddress(true, false);
					_alreadyFetchedAddress = false;
					break;
				case StorageLocationFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCards = (_cards.Count > 0);
			_alreadyFetchedJobCardImports = (_jobCardImports.Count > 0);
			_alreadyFetchedNewLocationStockTransfers = (_newLocationStockTransfers.Count > 0);
			_alreadyFetchedOldLocationStockTransfers = (_oldLocationStockTransfers.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedAddress = (_address != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Address":
					toReturn.Add(Relations.AddressEntityUsingAddressID);
					break;
				case "Cards":
					toReturn.Add(Relations.CardEntityUsingStorageLocationID);
					break;
				case "JobCardImports":
					toReturn.Add(Relations.JobCardImportEntityUsingStorageLocationID);
					break;
				case "NewLocationStockTransfers":
					toReturn.Add(Relations.StockTransferEntityUsingNewStorageLocationID);
					break;
				case "OldLocationStockTransfers":
					toReturn.Add(Relations.StockTransferEntityUsingOldStorageLocationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cards", (!this.MarkedForDeletion?_cards:null));
			info.AddValue("_alwaysFetchCards", _alwaysFetchCards);
			info.AddValue("_alreadyFetchedCards", _alreadyFetchedCards);
			info.AddValue("_jobCardImports", (!this.MarkedForDeletion?_jobCardImports:null));
			info.AddValue("_alwaysFetchJobCardImports", _alwaysFetchJobCardImports);
			info.AddValue("_alreadyFetchedJobCardImports", _alreadyFetchedJobCardImports);
			info.AddValue("_newLocationStockTransfers", (!this.MarkedForDeletion?_newLocationStockTransfers:null));
			info.AddValue("_alwaysFetchNewLocationStockTransfers", _alwaysFetchNewLocationStockTransfers);
			info.AddValue("_alreadyFetchedNewLocationStockTransfers", _alreadyFetchedNewLocationStockTransfers);
			info.AddValue("_oldLocationStockTransfers", (!this.MarkedForDeletion?_oldLocationStockTransfers:null));
			info.AddValue("_alwaysFetchOldLocationStockTransfers", _alwaysFetchOldLocationStockTransfers);
			info.AddValue("_alreadyFetchedOldLocationStockTransfers", _alreadyFetchedOldLocationStockTransfers);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_address", (!this.MarkedForDeletion?_address:null));
			info.AddValue("_addressReturnsNewIfNotFound", _addressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddress", _alwaysFetchAddress);
			info.AddValue("_alreadyFetchedAddress", _alreadyFetchedAddress);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Address":
					_alreadyFetchedAddress = true;
					this.Address = (AddressEntity)entity;
					break;
				case "Cards":
					_alreadyFetchedCards = true;
					if(entity!=null)
					{
						this.Cards.Add((CardEntity)entity);
					}
					break;
				case "JobCardImports":
					_alreadyFetchedJobCardImports = true;
					if(entity!=null)
					{
						this.JobCardImports.Add((JobCardImportEntity)entity);
					}
					break;
				case "NewLocationStockTransfers":
					_alreadyFetchedNewLocationStockTransfers = true;
					if(entity!=null)
					{
						this.NewLocationStockTransfers.Add((StockTransferEntity)entity);
					}
					break;
				case "OldLocationStockTransfers":
					_alreadyFetchedOldLocationStockTransfers = true;
					if(entity!=null)
					{
						this.OldLocationStockTransfers.Add((StockTransferEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Address":
					SetupSyncAddress(relatedEntity);
					break;
				case "Cards":
					_cards.Add((CardEntity)relatedEntity);
					break;
				case "JobCardImports":
					_jobCardImports.Add((JobCardImportEntity)relatedEntity);
					break;
				case "NewLocationStockTransfers":
					_newLocationStockTransfers.Add((StockTransferEntity)relatedEntity);
					break;
				case "OldLocationStockTransfers":
					_oldLocationStockTransfers.Add((StockTransferEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Address":
					DesetupSyncAddress(false, true);
					break;
				case "Cards":
					this.PerformRelatedEntityRemoval(_cards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "JobCardImports":
					this.PerformRelatedEntityRemoval(_jobCardImports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NewLocationStockTransfers":
					this.PerformRelatedEntityRemoval(_newLocationStockTransfers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OldLocationStockTransfers":
					this.PerformRelatedEntityRemoval(_oldLocationStockTransfers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_address!=null)
			{
				toReturn.Add(_address);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cards);
			toReturn.Add(_jobCardImports);
			toReturn.Add(_newLocationStockTransfers);
			toReturn.Add(_oldLocationStockTransfers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 storageLocationID)
		{
			return FetchUsingPK(storageLocationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 storageLocationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(storageLocationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 storageLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(storageLocationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 storageLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(storageLocationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.StorageLocationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new StorageLocationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCards || forceFetch || _alwaysFetchCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cards);
				_cards.SuppressClearInGetMulti=!forceFetch;
				_cards.EntityFactoryToUse = entityFactoryToUse;
				_cards.GetMultiManyToOne(null, null, null, null, null, null, this, filter);
				_cards.SuppressClearInGetMulti=false;
				_alreadyFetchedCards = true;
			}
			return _cards;
		}

		/// <summary> Sets the collection parameters for the collection for 'Cards'. These settings will be taken into account
		/// when the property Cards is requested or GetMultiCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cards.SortClauses=sortClauses;
			_cards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobCardImports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobCardImports || forceFetch || _alwaysFetchJobCardImports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobCardImports);
				_jobCardImports.SuppressClearInGetMulti=!forceFetch;
				_jobCardImports.EntityFactoryToUse = entityFactoryToUse;
				_jobCardImports.GetMultiManyToOne(null, null, this, filter);
				_jobCardImports.SuppressClearInGetMulti=false;
				_alreadyFetchedJobCardImports = true;
			}
			return _jobCardImports;
		}

		/// <summary> Sets the collection parameters for the collection for 'JobCardImports'. These settings will be taken into account
		/// when the property JobCardImports is requested or GetMultiJobCardImports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobCardImports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobCardImports.SortClauses=sortClauses;
			_jobCardImports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiNewLocationStockTransfers(bool forceFetch)
		{
			return GetMultiNewLocationStockTransfers(forceFetch, _newLocationStockTransfers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiNewLocationStockTransfers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNewLocationStockTransfers(forceFetch, _newLocationStockTransfers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiNewLocationStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNewLocationStockTransfers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiNewLocationStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNewLocationStockTransfers || forceFetch || _alwaysFetchNewLocationStockTransfers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_newLocationStockTransfers);
				_newLocationStockTransfers.SuppressClearInGetMulti=!forceFetch;
				_newLocationStockTransfers.EntityFactoryToUse = entityFactoryToUse;
				_newLocationStockTransfers.GetMultiManyToOne(this, null, filter);
				_newLocationStockTransfers.SuppressClearInGetMulti=false;
				_alreadyFetchedNewLocationStockTransfers = true;
			}
			return _newLocationStockTransfers;
		}

		/// <summary> Sets the collection parameters for the collection for 'NewLocationStockTransfers'. These settings will be taken into account
		/// when the property NewLocationStockTransfers is requested or GetMultiNewLocationStockTransfers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNewLocationStockTransfers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_newLocationStockTransfers.SortClauses=sortClauses;
			_newLocationStockTransfers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiOldLocationStockTransfers(bool forceFetch)
		{
			return GetMultiOldLocationStockTransfers(forceFetch, _oldLocationStockTransfers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiOldLocationStockTransfers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOldLocationStockTransfers(forceFetch, _oldLocationStockTransfers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiOldLocationStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOldLocationStockTransfers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StockTransferCollection GetMultiOldLocationStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOldLocationStockTransfers || forceFetch || _alwaysFetchOldLocationStockTransfers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_oldLocationStockTransfers);
				_oldLocationStockTransfers.SuppressClearInGetMulti=!forceFetch;
				_oldLocationStockTransfers.EntityFactoryToUse = entityFactoryToUse;
				_oldLocationStockTransfers.GetMultiManyToOne(null, this, filter);
				_oldLocationStockTransfers.SuppressClearInGetMulti=false;
				_alreadyFetchedOldLocationStockTransfers = true;
			}
			return _oldLocationStockTransfers;
		}

		/// <summary> Sets the collection parameters for the collection for 'OldLocationStockTransfers'. These settings will be taken into account
		/// when the property OldLocationStockTransfers is requested or GetMultiOldLocationStockTransfers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOldLocationStockTransfers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_oldLocationStockTransfers.SortClauses=sortClauses;
			_oldLocationStockTransfers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleAddress()
		{
			return GetSingleAddress(false);
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddress || forceFetch || _alwaysFetchAddress) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID);
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Address = newEntity;
				_alreadyFetchedAddress = fetchResult;
			}
			return _address;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Address", _address);
			toReturn.Add("Cards", _cards);
			toReturn.Add("JobCardImports", _jobCardImports);
			toReturn.Add("NewLocationStockTransfers", _newLocationStockTransfers);
			toReturn.Add("OldLocationStockTransfers", _oldLocationStockTransfers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="validator">The validator object for this StorageLocationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 storageLocationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(storageLocationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_cards.SetContainingEntityInfo(this, "StorageLocation");

			_jobCardImports = new VarioSL.Entities.CollectionClasses.JobCardImportCollection();
			_jobCardImports.SetContainingEntityInfo(this, "StorageLocation");

			_newLocationStockTransfers = new VarioSL.Entities.CollectionClasses.StockTransferCollection();
			_newLocationStockTransfers.SetContainingEntityInfo(this, "NewStorageLocation");

			_oldLocationStockTransfers = new VarioSL.Entities.CollectionClasses.StockTransferCollection();
			_oldLocationStockTransfers.SetContainingEntityInfo(this, "OldStorageLocation");
			_clientReturnsNewIfNotFound = false;
			_addressReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Abbreviation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticStorageLocationRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "StorageLocations", resetFKFields, new int[] { (int)StorageLocationFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticStorageLocationRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _address</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticStorageLocationRelations.AddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "StorageLocations", resetFKFields, new int[] { (int)StorageLocationFieldIndex.AddressID } );		
			_address = null;
		}
		
		/// <summary> setups the sync logic for member _address</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddress(IEntityCore relatedEntity)
		{
			if(_address!=relatedEntity)
			{		
				DesetupSyncAddress(true, true);
				_address = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticStorageLocationRelations.AddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedAddress, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="storageLocationID">PK value for StorageLocation which data should be fetched into this StorageLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 storageLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)StorageLocationFieldIndex.StorageLocationID].ForcedCurrentValueWrite(storageLocationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateStorageLocationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new StorageLocationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static StorageLocationRelations Relations
		{
			get	{ return new StorageLocationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Cards")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Cards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'JobCardImport' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobCardImports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCardImportCollection(), (IEntityRelation)GetRelationsForField("JobCardImports")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.JobCardImportEntity, 0, null, null, null, "JobCardImports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StockTransfer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNewLocationStockTransfers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StockTransferCollection(), (IEntityRelation)GetRelationsForField("NewLocationStockTransfers")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.StockTransferEntity, 0, null, null, null, "NewLocationStockTransfers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StockTransfer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOldLocationStockTransfers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StockTransferCollection(), (IEntityRelation)GetRelationsForField("OldLocationStockTransfers")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.StockTransferEntity, 0, null, null, null, "OldLocationStockTransfers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("Address")[0], (int)VarioSL.Entities.EntityType.StorageLocationEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "Address", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Abbreviation property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."ABBREVIATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Abbreviation
		{
			get { return (System.String)GetValue((int)StorageLocationFieldIndex.Abbreviation, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.Abbreviation, value, true); }
		}

		/// <summary> The AddressID property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AddressID
		{
			get { return (System.Int64)GetValue((int)StorageLocationFieldIndex.AddressID, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.AddressID, value, true); }
		}

		/// <summary> The ClientID property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)StorageLocationFieldIndex.ClientID, false); }
			set	{ SetValue((int)StorageLocationFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)StorageLocationFieldIndex.Description, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)StorageLocationFieldIndex.LastModified, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)StorageLocationFieldIndex.LastUser, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)StorageLocationFieldIndex.Name, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.Name, value, true); }
		}

		/// <summary> The StorageLocationID property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."STORAGELOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 StorageLocationID
		{
			get { return (System.Int64)GetValue((int)StorageLocationFieldIndex.StorageLocationID, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.StorageLocationID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity StorageLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STORAGELOCATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)StorageLocationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)StorageLocationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection Cards
		{
			get	{ return GetMultiCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Cards. When set to true, Cards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Cards is accessed. You can always execute/ a forced fetch by calling GetMultiCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCards
		{
			get	{ return _alwaysFetchCards; }
			set	{ _alwaysFetchCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Cards already has been fetched. Setting this property to false when Cards has been fetched
		/// will clear the Cards collection well. Setting this property to true while Cards hasn't been fetched disables lazy loading for Cards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCards
		{
			get { return _alreadyFetchedCards;}
			set 
			{
				if(_alreadyFetchedCards && !value && (_cards != null))
				{
					_cards.Clear();
				}
				_alreadyFetchedCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobCardImports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection JobCardImports
		{
			get	{ return GetMultiJobCardImports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for JobCardImports. When set to true, JobCardImports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time JobCardImports is accessed. You can always execute/ a forced fetch by calling GetMultiJobCardImports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobCardImports
		{
			get	{ return _alwaysFetchJobCardImports; }
			set	{ _alwaysFetchJobCardImports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property JobCardImports already has been fetched. Setting this property to false when JobCardImports has been fetched
		/// will clear the JobCardImports collection well. Setting this property to true while JobCardImports hasn't been fetched disables lazy loading for JobCardImports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobCardImports
		{
			get { return _alreadyFetchedJobCardImports;}
			set 
			{
				if(_alreadyFetchedJobCardImports && !value && (_jobCardImports != null))
				{
					_jobCardImports.Clear();
				}
				_alreadyFetchedJobCardImports = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNewLocationStockTransfers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StockTransferCollection NewLocationStockTransfers
		{
			get	{ return GetMultiNewLocationStockTransfers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NewLocationStockTransfers. When set to true, NewLocationStockTransfers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NewLocationStockTransfers is accessed. You can always execute/ a forced fetch by calling GetMultiNewLocationStockTransfers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNewLocationStockTransfers
		{
			get	{ return _alwaysFetchNewLocationStockTransfers; }
			set	{ _alwaysFetchNewLocationStockTransfers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NewLocationStockTransfers already has been fetched. Setting this property to false when NewLocationStockTransfers has been fetched
		/// will clear the NewLocationStockTransfers collection well. Setting this property to true while NewLocationStockTransfers hasn't been fetched disables lazy loading for NewLocationStockTransfers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNewLocationStockTransfers
		{
			get { return _alreadyFetchedNewLocationStockTransfers;}
			set 
			{
				if(_alreadyFetchedNewLocationStockTransfers && !value && (_newLocationStockTransfers != null))
				{
					_newLocationStockTransfers.Clear();
				}
				_alreadyFetchedNewLocationStockTransfers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StockTransferEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOldLocationStockTransfers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StockTransferCollection OldLocationStockTransfers
		{
			get	{ return GetMultiOldLocationStockTransfers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OldLocationStockTransfers. When set to true, OldLocationStockTransfers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OldLocationStockTransfers is accessed. You can always execute/ a forced fetch by calling GetMultiOldLocationStockTransfers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOldLocationStockTransfers
		{
			get	{ return _alwaysFetchOldLocationStockTransfers; }
			set	{ _alwaysFetchOldLocationStockTransfers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OldLocationStockTransfers already has been fetched. Setting this property to false when OldLocationStockTransfers has been fetched
		/// will clear the OldLocationStockTransfers collection well. Setting this property to true while OldLocationStockTransfers hasn't been fetched disables lazy loading for OldLocationStockTransfers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOldLocationStockTransfers
		{
			get { return _alreadyFetchedOldLocationStockTransfers;}
			set 
			{
				if(_alreadyFetchedOldLocationStockTransfers && !value && (_oldLocationStockTransfers != null))
				{
					_oldLocationStockTransfers.Clear();
				}
				_alreadyFetchedOldLocationStockTransfers = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "StorageLocations", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity Address
		{
			get	{ return GetSingleAddress(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAddress(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "StorageLocations", "Address", _address, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Address. When set to true, Address is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Address is accessed. You can always execute a forced fetch by calling GetSingleAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddress
		{
			get	{ return _alwaysFetchAddress; }
			set	{ _alwaysFetchAddress = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Address already has been fetched. Setting this property to false when Address has been fetched
		/// will set Address to null as well. Setting this property to true while Address hasn't been fetched disables lazy loading for Address</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddress
		{
			get { return _alreadyFetchedAddress;}
			set 
			{
				if(_alreadyFetchedAddress && !value)
				{
					this.Address = null;
				}
				_alreadyFetchedAddress = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Address is not found
		/// in the database. When set to true, Address will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AddressReturnsNewIfNotFound
		{
			get	{ return _addressReturnsNewIfNotFound; }
			set { _addressReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.StorageLocationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
