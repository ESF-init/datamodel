﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RuleType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RuleTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RuleCappingCollection	_tmRuleCappings;
		private bool	_alwaysFetchTmRuleCappings, _alreadyFetchedTmRuleCappings;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriodPeriodUnit;
		private bool	_alwaysFetchRulePeriodPeriodUnit, _alreadyFetchedRulePeriodPeriodUnit;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriodValidTimeUnit;
		private bool	_alwaysFetchRulePeriodValidTimeUnit, _alreadyFetchedRulePeriodValidTimeUnit;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriodsPeriodType;
		private bool	_alwaysFetchRulePeriodsPeriodType, _alreadyFetchedRulePeriodsPeriodType;
		private VarioSL.Entities.CollectionClasses.RulePeriodCollection	_rulePeriodExtTimeUnit;
		private bool	_alwaysFetchRulePeriodExtTimeUnit, _alreadyFetchedRulePeriodExtTimeUnit;
		private VarioSL.Entities.CollectionClasses.TicketAssignmentCollection	_ticketAssignments;
		private bool	_alwaysFetchTicketAssignments, _alreadyFetchedTicketAssignments;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TmRuleCappings</summary>
			public static readonly string TmRuleCappings = "TmRuleCappings";
			/// <summary>Member name RulePeriodPeriodUnit</summary>
			public static readonly string RulePeriodPeriodUnit = "RulePeriodPeriodUnit";
			/// <summary>Member name RulePeriodValidTimeUnit</summary>
			public static readonly string RulePeriodValidTimeUnit = "RulePeriodValidTimeUnit";
			/// <summary>Member name RulePeriodsPeriodType</summary>
			public static readonly string RulePeriodsPeriodType = "RulePeriodsPeriodType";
			/// <summary>Member name RulePeriodExtTimeUnit</summary>
			public static readonly string RulePeriodExtTimeUnit = "RulePeriodExtTimeUnit";
			/// <summary>Member name TicketAssignments</summary>
			public static readonly string TicketAssignments = "TicketAssignments";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RuleTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleTypeEntity() :base("RuleTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		public RuleTypeEntity(System.Int64 ruleTypeID):base("RuleTypeEntity")
		{
			InitClassFetch(ruleTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RuleTypeEntity(System.Int64 ruleTypeID, IPrefetchPath prefetchPathToUse):base("RuleTypeEntity")
		{
			InitClassFetch(ruleTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="validator">The custom validator object for this RuleTypeEntity</param>
		public RuleTypeEntity(System.Int64 ruleTypeID, IValidator validator):base("RuleTypeEntity")
		{
			InitClassFetch(ruleTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_tmRuleCappings = (VarioSL.Entities.CollectionClasses.RuleCappingCollection)info.GetValue("_tmRuleCappings", typeof(VarioSL.Entities.CollectionClasses.RuleCappingCollection));
			_alwaysFetchTmRuleCappings = info.GetBoolean("_alwaysFetchTmRuleCappings");
			_alreadyFetchedTmRuleCappings = info.GetBoolean("_alreadyFetchedTmRuleCappings");

			_rulePeriodPeriodUnit = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriodPeriodUnit", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriodPeriodUnit = info.GetBoolean("_alwaysFetchRulePeriodPeriodUnit");
			_alreadyFetchedRulePeriodPeriodUnit = info.GetBoolean("_alreadyFetchedRulePeriodPeriodUnit");

			_rulePeriodValidTimeUnit = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriodValidTimeUnit", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriodValidTimeUnit = info.GetBoolean("_alwaysFetchRulePeriodValidTimeUnit");
			_alreadyFetchedRulePeriodValidTimeUnit = info.GetBoolean("_alreadyFetchedRulePeriodValidTimeUnit");

			_rulePeriodsPeriodType = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriodsPeriodType", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriodsPeriodType = info.GetBoolean("_alwaysFetchRulePeriodsPeriodType");
			_alreadyFetchedRulePeriodsPeriodType = info.GetBoolean("_alreadyFetchedRulePeriodsPeriodType");

			_rulePeriodExtTimeUnit = (VarioSL.Entities.CollectionClasses.RulePeriodCollection)info.GetValue("_rulePeriodExtTimeUnit", typeof(VarioSL.Entities.CollectionClasses.RulePeriodCollection));
			_alwaysFetchRulePeriodExtTimeUnit = info.GetBoolean("_alwaysFetchRulePeriodExtTimeUnit");
			_alreadyFetchedRulePeriodExtTimeUnit = info.GetBoolean("_alreadyFetchedRulePeriodExtTimeUnit");

			_ticketAssignments = (VarioSL.Entities.CollectionClasses.TicketAssignmentCollection)info.GetValue("_ticketAssignments", typeof(VarioSL.Entities.CollectionClasses.TicketAssignmentCollection));
			_alwaysFetchTicketAssignments = info.GetBoolean("_alwaysFetchTicketAssignments");
			_alreadyFetchedTicketAssignments = info.GetBoolean("_alreadyFetchedTicketAssignments");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTmRuleCappings = (_tmRuleCappings.Count > 0);
			_alreadyFetchedRulePeriodPeriodUnit = (_rulePeriodPeriodUnit.Count > 0);
			_alreadyFetchedRulePeriodValidTimeUnit = (_rulePeriodValidTimeUnit.Count > 0);
			_alreadyFetchedRulePeriodsPeriodType = (_rulePeriodsPeriodType.Count > 0);
			_alreadyFetchedRulePeriodExtTimeUnit = (_rulePeriodExtTimeUnit.Count > 0);
			_alreadyFetchedTicketAssignments = (_ticketAssignments.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TmRuleCappings":
					toReturn.Add(Relations.RuleCappingEntityUsingRuleTypeID);
					break;
				case "RulePeriodPeriodUnit":
					toReturn.Add(Relations.RulePeriodEntityUsingPeriodUnit);
					break;
				case "RulePeriodValidTimeUnit":
					toReturn.Add(Relations.RulePeriodEntityUsingValidTimeUnit);
					break;
				case "RulePeriodsPeriodType":
					toReturn.Add(Relations.RulePeriodEntityUsingValidationType);
					break;
				case "RulePeriodExtTimeUnit":
					toReturn.Add(Relations.RulePeriodEntityUsingExtTimeUnit);
					break;
				case "TicketAssignments":
					toReturn.Add(Relations.TicketAssignmentEntityUsingPeriodicTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_tmRuleCappings", (!this.MarkedForDeletion?_tmRuleCappings:null));
			info.AddValue("_alwaysFetchTmRuleCappings", _alwaysFetchTmRuleCappings);
			info.AddValue("_alreadyFetchedTmRuleCappings", _alreadyFetchedTmRuleCappings);
			info.AddValue("_rulePeriodPeriodUnit", (!this.MarkedForDeletion?_rulePeriodPeriodUnit:null));
			info.AddValue("_alwaysFetchRulePeriodPeriodUnit", _alwaysFetchRulePeriodPeriodUnit);
			info.AddValue("_alreadyFetchedRulePeriodPeriodUnit", _alreadyFetchedRulePeriodPeriodUnit);
			info.AddValue("_rulePeriodValidTimeUnit", (!this.MarkedForDeletion?_rulePeriodValidTimeUnit:null));
			info.AddValue("_alwaysFetchRulePeriodValidTimeUnit", _alwaysFetchRulePeriodValidTimeUnit);
			info.AddValue("_alreadyFetchedRulePeriodValidTimeUnit", _alreadyFetchedRulePeriodValidTimeUnit);
			info.AddValue("_rulePeriodsPeriodType", (!this.MarkedForDeletion?_rulePeriodsPeriodType:null));
			info.AddValue("_alwaysFetchRulePeriodsPeriodType", _alwaysFetchRulePeriodsPeriodType);
			info.AddValue("_alreadyFetchedRulePeriodsPeriodType", _alreadyFetchedRulePeriodsPeriodType);
			info.AddValue("_rulePeriodExtTimeUnit", (!this.MarkedForDeletion?_rulePeriodExtTimeUnit:null));
			info.AddValue("_alwaysFetchRulePeriodExtTimeUnit", _alwaysFetchRulePeriodExtTimeUnit);
			info.AddValue("_alreadyFetchedRulePeriodExtTimeUnit", _alreadyFetchedRulePeriodExtTimeUnit);
			info.AddValue("_ticketAssignments", (!this.MarkedForDeletion?_ticketAssignments:null));
			info.AddValue("_alwaysFetchTicketAssignments", _alwaysFetchTicketAssignments);
			info.AddValue("_alreadyFetchedTicketAssignments", _alreadyFetchedTicketAssignments);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TmRuleCappings":
					_alreadyFetchedTmRuleCappings = true;
					if(entity!=null)
					{
						this.TmRuleCappings.Add((RuleCappingEntity)entity);
					}
					break;
				case "RulePeriodPeriodUnit":
					_alreadyFetchedRulePeriodPeriodUnit = true;
					if(entity!=null)
					{
						this.RulePeriodPeriodUnit.Add((RulePeriodEntity)entity);
					}
					break;
				case "RulePeriodValidTimeUnit":
					_alreadyFetchedRulePeriodValidTimeUnit = true;
					if(entity!=null)
					{
						this.RulePeriodValidTimeUnit.Add((RulePeriodEntity)entity);
					}
					break;
				case "RulePeriodsPeriodType":
					_alreadyFetchedRulePeriodsPeriodType = true;
					if(entity!=null)
					{
						this.RulePeriodsPeriodType.Add((RulePeriodEntity)entity);
					}
					break;
				case "RulePeriodExtTimeUnit":
					_alreadyFetchedRulePeriodExtTimeUnit = true;
					if(entity!=null)
					{
						this.RulePeriodExtTimeUnit.Add((RulePeriodEntity)entity);
					}
					break;
				case "TicketAssignments":
					_alreadyFetchedTicketAssignments = true;
					if(entity!=null)
					{
						this.TicketAssignments.Add((TicketAssignmentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TmRuleCappings":
					_tmRuleCappings.Add((RuleCappingEntity)relatedEntity);
					break;
				case "RulePeriodPeriodUnit":
					_rulePeriodPeriodUnit.Add((RulePeriodEntity)relatedEntity);
					break;
				case "RulePeriodValidTimeUnit":
					_rulePeriodValidTimeUnit.Add((RulePeriodEntity)relatedEntity);
					break;
				case "RulePeriodsPeriodType":
					_rulePeriodsPeriodType.Add((RulePeriodEntity)relatedEntity);
					break;
				case "RulePeriodExtTimeUnit":
					_rulePeriodExtTimeUnit.Add((RulePeriodEntity)relatedEntity);
					break;
				case "TicketAssignments":
					_ticketAssignments.Add((TicketAssignmentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TmRuleCappings":
					this.PerformRelatedEntityRemoval(_tmRuleCappings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriodPeriodUnit":
					this.PerformRelatedEntityRemoval(_rulePeriodPeriodUnit, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriodValidTimeUnit":
					this.PerformRelatedEntityRemoval(_rulePeriodValidTimeUnit, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriodsPeriodType":
					this.PerformRelatedEntityRemoval(_rulePeriodsPeriodType, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RulePeriodExtTimeUnit":
					this.PerformRelatedEntityRemoval(_rulePeriodExtTimeUnit, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketAssignments":
					this.PerformRelatedEntityRemoval(_ticketAssignments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_tmRuleCappings);
			toReturn.Add(_rulePeriodPeriodUnit);
			toReturn.Add(_rulePeriodValidTimeUnit);
			toReturn.Add(_rulePeriodsPeriodType);
			toReturn.Add(_rulePeriodExtTimeUnit);
			toReturn.Add(_ticketAssignments);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleTypeID)
		{
			return FetchUsingPK(ruleTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ruleTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ruleTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ruleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ruleTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RuleTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RuleTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiTmRuleCappings(bool forceFetch)
		{
			return GetMultiTmRuleCappings(forceFetch, _tmRuleCappings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiTmRuleCappings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTmRuleCappings(forceFetch, _tmRuleCappings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiTmRuleCappings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTmRuleCappings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiTmRuleCappings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTmRuleCappings || forceFetch || _alwaysFetchTmRuleCappings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tmRuleCappings);
				_tmRuleCappings.SuppressClearInGetMulti=!forceFetch;
				_tmRuleCappings.EntityFactoryToUse = entityFactoryToUse;
				_tmRuleCappings.GetMultiManyToOne(null, null, this, null, filter);
				_tmRuleCappings.SuppressClearInGetMulti=false;
				_alreadyFetchedTmRuleCappings = true;
			}
			return _tmRuleCappings;
		}

		/// <summary> Sets the collection parameters for the collection for 'TmRuleCappings'. These settings will be taken into account
		/// when the property TmRuleCappings is requested or GetMultiTmRuleCappings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTmRuleCappings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tmRuleCappings.SortClauses=sortClauses;
			_tmRuleCappings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodPeriodUnit(bool forceFetch)
		{
			return GetMultiRulePeriodPeriodUnit(forceFetch, _rulePeriodPeriodUnit.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodPeriodUnit(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriodPeriodUnit(forceFetch, _rulePeriodPeriodUnit.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodPeriodUnit(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriodPeriodUnit(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodPeriodUnit(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriodPeriodUnit || forceFetch || _alwaysFetchRulePeriodPeriodUnit) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriodPeriodUnit);
				_rulePeriodPeriodUnit.SuppressClearInGetMulti=!forceFetch;
				_rulePeriodPeriodUnit.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriodPeriodUnit.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_rulePeriodPeriodUnit.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriodPeriodUnit = true;
			}
			return _rulePeriodPeriodUnit;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriodPeriodUnit'. These settings will be taken into account
		/// when the property RulePeriodPeriodUnit is requested or GetMultiRulePeriodPeriodUnit is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriodPeriodUnit(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriodPeriodUnit.SortClauses=sortClauses;
			_rulePeriodPeriodUnit.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodValidTimeUnit(bool forceFetch)
		{
			return GetMultiRulePeriodValidTimeUnit(forceFetch, _rulePeriodValidTimeUnit.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodValidTimeUnit(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriodValidTimeUnit(forceFetch, _rulePeriodValidTimeUnit.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodValidTimeUnit(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriodValidTimeUnit(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodValidTimeUnit(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriodValidTimeUnit || forceFetch || _alwaysFetchRulePeriodValidTimeUnit) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriodValidTimeUnit);
				_rulePeriodValidTimeUnit.SuppressClearInGetMulti=!forceFetch;
				_rulePeriodValidTimeUnit.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriodValidTimeUnit.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_rulePeriodValidTimeUnit.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriodValidTimeUnit = true;
			}
			return _rulePeriodValidTimeUnit;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriodValidTimeUnit'. These settings will be taken into account
		/// when the property RulePeriodValidTimeUnit is requested or GetMultiRulePeriodValidTimeUnit is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriodValidTimeUnit(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriodValidTimeUnit.SortClauses=sortClauses;
			_rulePeriodValidTimeUnit.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodsPeriodType(bool forceFetch)
		{
			return GetMultiRulePeriodsPeriodType(forceFetch, _rulePeriodsPeriodType.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodsPeriodType(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriodsPeriodType(forceFetch, _rulePeriodsPeriodType.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodsPeriodType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriodsPeriodType(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodsPeriodType(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriodsPeriodType || forceFetch || _alwaysFetchRulePeriodsPeriodType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriodsPeriodType);
				_rulePeriodsPeriodType.SuppressClearInGetMulti=!forceFetch;
				_rulePeriodsPeriodType.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriodsPeriodType.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_rulePeriodsPeriodType.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriodsPeriodType = true;
			}
			return _rulePeriodsPeriodType;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriodsPeriodType'. These settings will be taken into account
		/// when the property RulePeriodsPeriodType is requested or GetMultiRulePeriodsPeriodType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriodsPeriodType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriodsPeriodType.SortClauses=sortClauses;
			_rulePeriodsPeriodType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodExtTimeUnit(bool forceFetch)
		{
			return GetMultiRulePeriodExtTimeUnit(forceFetch, _rulePeriodExtTimeUnit.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RulePeriodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodExtTimeUnit(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRulePeriodExtTimeUnit(forceFetch, _rulePeriodExtTimeUnit.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodExtTimeUnit(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRulePeriodExtTimeUnit(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection GetMultiRulePeriodExtTimeUnit(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRulePeriodExtTimeUnit || forceFetch || _alwaysFetchRulePeriodExtTimeUnit) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_rulePeriodExtTimeUnit);
				_rulePeriodExtTimeUnit.SuppressClearInGetMulti=!forceFetch;
				_rulePeriodExtTimeUnit.EntityFactoryToUse = entityFactoryToUse;
				_rulePeriodExtTimeUnit.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_rulePeriodExtTimeUnit.SuppressClearInGetMulti=false;
				_alreadyFetchedRulePeriodExtTimeUnit = true;
			}
			return _rulePeriodExtTimeUnit;
		}

		/// <summary> Sets the collection parameters for the collection for 'RulePeriodExtTimeUnit'. These settings will be taken into account
		/// when the property RulePeriodExtTimeUnit is requested or GetMultiRulePeriodExtTimeUnit is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRulePeriodExtTimeUnit(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_rulePeriodExtTimeUnit.SortClauses=sortClauses;
			_rulePeriodExtTimeUnit.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketAssignments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketAssignments || forceFetch || _alwaysFetchTicketAssignments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketAssignments);
				_ticketAssignments.SuppressClearInGetMulti=!forceFetch;
				_ticketAssignments.EntityFactoryToUse = entityFactoryToUse;
				_ticketAssignments.GetMultiManyToOne(this, null, null, null, null, filter);
				_ticketAssignments.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketAssignments = true;
			}
			return _ticketAssignments;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketAssignments'. These settings will be taken into account
		/// when the property TicketAssignments is requested or GetMultiTicketAssignments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketAssignments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketAssignments.SortClauses=sortClauses;
			_ticketAssignments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TmRuleCappings", _tmRuleCappings);
			toReturn.Add("RulePeriodPeriodUnit", _rulePeriodPeriodUnit);
			toReturn.Add("RulePeriodValidTimeUnit", _rulePeriodValidTimeUnit);
			toReturn.Add("RulePeriodsPeriodType", _rulePeriodsPeriodType);
			toReturn.Add("RulePeriodExtTimeUnit", _rulePeriodExtTimeUnit);
			toReturn.Add("TicketAssignments", _ticketAssignments);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="validator">The validator object for this RuleTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ruleTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ruleTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_tmRuleCappings = new VarioSL.Entities.CollectionClasses.RuleCappingCollection();
			_tmRuleCappings.SetContainingEntityInfo(this, "TmRuleType");

			_rulePeriodPeriodUnit = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriodPeriodUnit.SetContainingEntityInfo(this, "RuleTypePeriodUnit");

			_rulePeriodValidTimeUnit = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriodValidTimeUnit.SetContainingEntityInfo(this, "RuleTypesValidTimeUnit");

			_rulePeriodsPeriodType = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriodsPeriodType.SetContainingEntityInfo(this, "RuleTypeValidationType");

			_rulePeriodExtTimeUnit = new VarioSL.Entities.CollectionClasses.RulePeriodCollection();
			_rulePeriodExtTimeUnit.SetContainingEntityInfo(this, "RuleTypeExtTimeUnit");

			_ticketAssignments = new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection();
			_ticketAssignments.SetContainingEntityInfo(this, "RuleType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleTypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleTypeGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleTypeNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ruleTypeID">PK value for RuleType which data should be fetched into this RuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ruleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RuleTypeFieldIndex.RuleTypeID].ForcedCurrentValueWrite(ruleTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRuleTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RuleTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RuleTypeRelations Relations
		{
			get	{ return new RuleTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCapping' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTmRuleCappings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingCollection(), (IEntityRelation)GetRelationsForField("TmRuleCappings")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.RuleCappingEntity, 0, null, null, null, "TmRuleCappings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriodPeriodUnit
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriodPeriodUnit")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriodPeriodUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriodValidTimeUnit
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriodValidTimeUnit")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriodValidTimeUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriodsPeriodType
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriodsPeriodType")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriodsPeriodType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriodExtTimeUnit
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriodExtTimeUnit")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriodExtTimeUnit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketAssignment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketAssignments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection(), (IEntityRelation)GetRelationsForField("TicketAssignments")[0], (int)VarioSL.Entities.EntityType.RuleTypeEntity, (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, 0, null, null, null, "TicketAssignments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RuleTypeID property of the Entity RuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_TYPE"."RULETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RuleTypeID
		{
			get { return (System.Int64)GetValue((int)RuleTypeFieldIndex.RuleTypeID, true); }
			set	{ SetValue((int)RuleTypeFieldIndex.RuleTypeID, value, true); }
		}

		/// <summary> The RuleTypeName property of the Entity RuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_TYPE"."RULETYPENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RuleTypeName
		{
			get { return (System.String)GetValue((int)RuleTypeFieldIndex.RuleTypeName, true); }
			set	{ SetValue((int)RuleTypeFieldIndex.RuleTypeName, value, true); }
		}

		/// <summary> The Description property of the Entity RuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_TYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)RuleTypeFieldIndex.Description, true); }
			set	{ SetValue((int)RuleTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The RuleTypeGroupID property of the Entity RuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_TYPE"."RULETYPEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RuleTypeGroupID
		{
			get { return (System.Int64)GetValue((int)RuleTypeFieldIndex.RuleTypeGroupID, true); }
			set	{ SetValue((int)RuleTypeFieldIndex.RuleTypeGroupID, value, true); }
		}

		/// <summary> The RuleTypeNumber property of the Entity RuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_TYPE"."RULETYPENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RuleTypeNumber
		{
			get { return (System.Int32)GetValue((int)RuleTypeFieldIndex.RuleTypeNumber, true); }
			set	{ SetValue((int)RuleTypeFieldIndex.RuleTypeNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTmRuleCappings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection TmRuleCappings
		{
			get	{ return GetMultiTmRuleCappings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TmRuleCappings. When set to true, TmRuleCappings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TmRuleCappings is accessed. You can always execute/ a forced fetch by calling GetMultiTmRuleCappings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTmRuleCappings
		{
			get	{ return _alwaysFetchTmRuleCappings; }
			set	{ _alwaysFetchTmRuleCappings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TmRuleCappings already has been fetched. Setting this property to false when TmRuleCappings has been fetched
		/// will clear the TmRuleCappings collection well. Setting this property to true while TmRuleCappings hasn't been fetched disables lazy loading for TmRuleCappings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTmRuleCappings
		{
			get { return _alreadyFetchedTmRuleCappings;}
			set 
			{
				if(_alreadyFetchedTmRuleCappings && !value && (_tmRuleCappings != null))
				{
					_tmRuleCappings.Clear();
				}
				_alreadyFetchedTmRuleCappings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriodPeriodUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriodPeriodUnit
		{
			get	{ return GetMultiRulePeriodPeriodUnit(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriodPeriodUnit. When set to true, RulePeriodPeriodUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriodPeriodUnit is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriodPeriodUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriodPeriodUnit
		{
			get	{ return _alwaysFetchRulePeriodPeriodUnit; }
			set	{ _alwaysFetchRulePeriodPeriodUnit = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriodPeriodUnit already has been fetched. Setting this property to false when RulePeriodPeriodUnit has been fetched
		/// will clear the RulePeriodPeriodUnit collection well. Setting this property to true while RulePeriodPeriodUnit hasn't been fetched disables lazy loading for RulePeriodPeriodUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriodPeriodUnit
		{
			get { return _alreadyFetchedRulePeriodPeriodUnit;}
			set 
			{
				if(_alreadyFetchedRulePeriodPeriodUnit && !value && (_rulePeriodPeriodUnit != null))
				{
					_rulePeriodPeriodUnit.Clear();
				}
				_alreadyFetchedRulePeriodPeriodUnit = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriodValidTimeUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriodValidTimeUnit
		{
			get	{ return GetMultiRulePeriodValidTimeUnit(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriodValidTimeUnit. When set to true, RulePeriodValidTimeUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriodValidTimeUnit is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriodValidTimeUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriodValidTimeUnit
		{
			get	{ return _alwaysFetchRulePeriodValidTimeUnit; }
			set	{ _alwaysFetchRulePeriodValidTimeUnit = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriodValidTimeUnit already has been fetched. Setting this property to false when RulePeriodValidTimeUnit has been fetched
		/// will clear the RulePeriodValidTimeUnit collection well. Setting this property to true while RulePeriodValidTimeUnit hasn't been fetched disables lazy loading for RulePeriodValidTimeUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriodValidTimeUnit
		{
			get { return _alreadyFetchedRulePeriodValidTimeUnit;}
			set 
			{
				if(_alreadyFetchedRulePeriodValidTimeUnit && !value && (_rulePeriodValidTimeUnit != null))
				{
					_rulePeriodValidTimeUnit.Clear();
				}
				_alreadyFetchedRulePeriodValidTimeUnit = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriodsPeriodType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriodsPeriodType
		{
			get	{ return GetMultiRulePeriodsPeriodType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriodsPeriodType. When set to true, RulePeriodsPeriodType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriodsPeriodType is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriodsPeriodType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriodsPeriodType
		{
			get	{ return _alwaysFetchRulePeriodsPeriodType; }
			set	{ _alwaysFetchRulePeriodsPeriodType = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriodsPeriodType already has been fetched. Setting this property to false when RulePeriodsPeriodType has been fetched
		/// will clear the RulePeriodsPeriodType collection well. Setting this property to true while RulePeriodsPeriodType hasn't been fetched disables lazy loading for RulePeriodsPeriodType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriodsPeriodType
		{
			get { return _alreadyFetchedRulePeriodsPeriodType;}
			set 
			{
				if(_alreadyFetchedRulePeriodsPeriodType && !value && (_rulePeriodsPeriodType != null))
				{
					_rulePeriodsPeriodType.Clear();
				}
				_alreadyFetchedRulePeriodsPeriodType = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RulePeriodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRulePeriodExtTimeUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RulePeriodCollection RulePeriodExtTimeUnit
		{
			get	{ return GetMultiRulePeriodExtTimeUnit(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriodExtTimeUnit. When set to true, RulePeriodExtTimeUnit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriodExtTimeUnit is accessed. You can always execute/ a forced fetch by calling GetMultiRulePeriodExtTimeUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriodExtTimeUnit
		{
			get	{ return _alwaysFetchRulePeriodExtTimeUnit; }
			set	{ _alwaysFetchRulePeriodExtTimeUnit = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriodExtTimeUnit already has been fetched. Setting this property to false when RulePeriodExtTimeUnit has been fetched
		/// will clear the RulePeriodExtTimeUnit collection well. Setting this property to true while RulePeriodExtTimeUnit hasn't been fetched disables lazy loading for RulePeriodExtTimeUnit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriodExtTimeUnit
		{
			get { return _alreadyFetchedRulePeriodExtTimeUnit;}
			set 
			{
				if(_alreadyFetchedRulePeriodExtTimeUnit && !value && (_rulePeriodExtTimeUnit != null))
				{
					_rulePeriodExtTimeUnit.Clear();
				}
				_alreadyFetchedRulePeriodExtTimeUnit = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketAssignments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection TicketAssignments
		{
			get	{ return GetMultiTicketAssignments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketAssignments. When set to true, TicketAssignments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketAssignments is accessed. You can always execute/ a forced fetch by calling GetMultiTicketAssignments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketAssignments
		{
			get	{ return _alwaysFetchTicketAssignments; }
			set	{ _alwaysFetchTicketAssignments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketAssignments already has been fetched. Setting this property to false when TicketAssignments has been fetched
		/// will clear the TicketAssignments collection well. Setting this property to true while TicketAssignments hasn't been fetched disables lazy loading for TicketAssignments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketAssignments
		{
			get { return _alreadyFetchedTicketAssignments;}
			set 
			{
				if(_alreadyFetchedTicketAssignments && !value && (_ticketAssignments != null))
				{
					_ticketAssignments.Clear();
				}
				_alreadyFetchedTicketAssignments = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RuleTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
