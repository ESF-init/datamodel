﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Automat'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AutomatEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CashServiceCollection	_amCashservices;
		private bool	_alwaysFetchAmCashservices, _alreadyFetchedAmCashservices;
		private VarioSL.Entities.CollectionClasses.ComponentCollection	_components;
		private bool	_alwaysFetchComponents, _alreadyFetchedComponents;
		private VarioSL.Entities.CollectionClasses.ComponentClearingCollection	_componentClearings;
		private bool	_alwaysFetchComponentClearings, _alreadyFetchedComponentClearings;
		private VarioSL.Entities.CollectionClasses.ComponentFillingCollection	_componentFillings;
		private bool	_alwaysFetchComponentFillings, _alreadyFetchedComponentFillings;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AmCashservices</summary>
			public static readonly string AmCashservices = "AmCashservices";
			/// <summary>Member name Components</summary>
			public static readonly string Components = "Components";
			/// <summary>Member name ComponentClearings</summary>
			public static readonly string ComponentClearings = "ComponentClearings";
			/// <summary>Member name ComponentFillings</summary>
			public static readonly string ComponentFillings = "ComponentFillings";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AutomatEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AutomatEntity() :base("AutomatEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		public AutomatEntity(System.Int64 automatID):base("AutomatEntity")
		{
			InitClassFetch(automatID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AutomatEntity(System.Int64 automatID, IPrefetchPath prefetchPathToUse):base("AutomatEntity")
		{
			InitClassFetch(automatID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="validator">The custom validator object for this AutomatEntity</param>
		public AutomatEntity(System.Int64 automatID, IValidator validator):base("AutomatEntity")
		{
			InitClassFetch(automatID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AutomatEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_amCashservices = (VarioSL.Entities.CollectionClasses.CashServiceCollection)info.GetValue("_amCashservices", typeof(VarioSL.Entities.CollectionClasses.CashServiceCollection));
			_alwaysFetchAmCashservices = info.GetBoolean("_alwaysFetchAmCashservices");
			_alreadyFetchedAmCashservices = info.GetBoolean("_alreadyFetchedAmCashservices");

			_components = (VarioSL.Entities.CollectionClasses.ComponentCollection)info.GetValue("_components", typeof(VarioSL.Entities.CollectionClasses.ComponentCollection));
			_alwaysFetchComponents = info.GetBoolean("_alwaysFetchComponents");
			_alreadyFetchedComponents = info.GetBoolean("_alreadyFetchedComponents");

			_componentClearings = (VarioSL.Entities.CollectionClasses.ComponentClearingCollection)info.GetValue("_componentClearings", typeof(VarioSL.Entities.CollectionClasses.ComponentClearingCollection));
			_alwaysFetchComponentClearings = info.GetBoolean("_alwaysFetchComponentClearings");
			_alreadyFetchedComponentClearings = info.GetBoolean("_alreadyFetchedComponentClearings");

			_componentFillings = (VarioSL.Entities.CollectionClasses.ComponentFillingCollection)info.GetValue("_componentFillings", typeof(VarioSL.Entities.CollectionClasses.ComponentFillingCollection));
			_alwaysFetchComponentFillings = info.GetBoolean("_alwaysFetchComponentFillings");
			_alreadyFetchedComponentFillings = info.GetBoolean("_alreadyFetchedComponentFillings");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAmCashservices = (_amCashservices.Count > 0);
			_alreadyFetchedComponents = (_components.Count > 0);
			_alreadyFetchedComponentClearings = (_componentClearings.Count > 0);
			_alreadyFetchedComponentFillings = (_componentFillings.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AmCashservices":
					toReturn.Add(Relations.CashServiceEntityUsingAutomatID);
					break;
				case "Components":
					toReturn.Add(Relations.ComponentEntityUsingAutomatID);
					break;
				case "ComponentClearings":
					toReturn.Add(Relations.ComponentClearingEntityUsingAutomatID);
					break;
				case "ComponentFillings":
					toReturn.Add(Relations.ComponentFillingEntityUsingAutomatID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_amCashservices", (!this.MarkedForDeletion?_amCashservices:null));
			info.AddValue("_alwaysFetchAmCashservices", _alwaysFetchAmCashservices);
			info.AddValue("_alreadyFetchedAmCashservices", _alreadyFetchedAmCashservices);
			info.AddValue("_components", (!this.MarkedForDeletion?_components:null));
			info.AddValue("_alwaysFetchComponents", _alwaysFetchComponents);
			info.AddValue("_alreadyFetchedComponents", _alreadyFetchedComponents);
			info.AddValue("_componentClearings", (!this.MarkedForDeletion?_componentClearings:null));
			info.AddValue("_alwaysFetchComponentClearings", _alwaysFetchComponentClearings);
			info.AddValue("_alreadyFetchedComponentClearings", _alreadyFetchedComponentClearings);
			info.AddValue("_componentFillings", (!this.MarkedForDeletion?_componentFillings:null));
			info.AddValue("_alwaysFetchComponentFillings", _alwaysFetchComponentFillings);
			info.AddValue("_alreadyFetchedComponentFillings", _alreadyFetchedComponentFillings);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AmCashservices":
					_alreadyFetchedAmCashservices = true;
					if(entity!=null)
					{
						this.AmCashservices.Add((CashServiceEntity)entity);
					}
					break;
				case "Components":
					_alreadyFetchedComponents = true;
					if(entity!=null)
					{
						this.Components.Add((ComponentEntity)entity);
					}
					break;
				case "ComponentClearings":
					_alreadyFetchedComponentClearings = true;
					if(entity!=null)
					{
						this.ComponentClearings.Add((ComponentClearingEntity)entity);
					}
					break;
				case "ComponentFillings":
					_alreadyFetchedComponentFillings = true;
					if(entity!=null)
					{
						this.ComponentFillings.Add((ComponentFillingEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AmCashservices":
					_amCashservices.Add((CashServiceEntity)relatedEntity);
					break;
				case "Components":
					_components.Add((ComponentEntity)relatedEntity);
					break;
				case "ComponentClearings":
					_componentClearings.Add((ComponentClearingEntity)relatedEntity);
					break;
				case "ComponentFillings":
					_componentFillings.Add((ComponentFillingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AmCashservices":
					this.PerformRelatedEntityRemoval(_amCashservices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Components":
					this.PerformRelatedEntityRemoval(_components, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentClearings":
					this.PerformRelatedEntityRemoval(_componentClearings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentFillings":
					this.PerformRelatedEntityRemoval(_componentFillings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_amCashservices);
			toReturn.Add(_components);
			toReturn.Add(_componentClearings);
			toReturn.Add(_componentFillings);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 automatID)
		{
			return FetchUsingPK(automatID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 automatID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(automatID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 automatID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(automatID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 automatID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(automatID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AutomatID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AutomatRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CashServiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceCollection GetMultiAmCashservices(bool forceFetch)
		{
			return GetMultiAmCashservices(forceFetch, _amCashservices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceCollection GetMultiAmCashservices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAmCashservices(forceFetch, _amCashservices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceCollection GetMultiAmCashservices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAmCashservices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceCollection GetMultiAmCashservices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAmCashservices || forceFetch || _alwaysFetchAmCashservices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_amCashservices);
				_amCashservices.SuppressClearInGetMulti=!forceFetch;
				_amCashservices.EntityFactoryToUse = entityFactoryToUse;
				_amCashservices.GetMultiManyToOne(this, filter);
				_amCashservices.SuppressClearInGetMulti=false;
				_alreadyFetchedAmCashservices = true;
			}
			return _amCashservices;
		}

		/// <summary> Sets the collection parameters for the collection for 'AmCashservices'. These settings will be taken into account
		/// when the property AmCashservices is requested or GetMultiAmCashservices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAmCashservices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_amCashservices.SortClauses=sortClauses;
			_amCashservices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch)
		{
			return GetMultiComponents(forceFetch, _components.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponents(forceFetch, _components.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponents || forceFetch || _alwaysFetchComponents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_components);
				_components.SuppressClearInGetMulti=!forceFetch;
				_components.EntityFactoryToUse = entityFactoryToUse;
				_components.GetMultiManyToOne(this, null, null, filter);
				_components.SuppressClearInGetMulti=false;
				_alreadyFetchedComponents = true;
			}
			return _components;
		}

		/// <summary> Sets the collection parameters for the collection for 'Components'. These settings will be taken into account
		/// when the property Components is requested or GetMultiComponents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_components.SortClauses=sortClauses;
			_components.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch)
		{
			return GetMultiComponentClearings(forceFetch, _componentClearings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentClearings(forceFetch, _componentClearings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentClearings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentClearings || forceFetch || _alwaysFetchComponentClearings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentClearings);
				_componentClearings.SuppressClearInGetMulti=!forceFetch;
				_componentClearings.EntityFactoryToUse = entityFactoryToUse;
				_componentClearings.GetMultiManyToOne(this, null, null, null, filter);
				_componentClearings.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentClearings = true;
			}
			return _componentClearings;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentClearings'. These settings will be taken into account
		/// when the property ComponentClearings is requested or GetMultiComponentClearings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentClearings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentClearings.SortClauses=sortClauses;
			_componentClearings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentFillings(forceFetch, _componentFillings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentFillings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentFillings || forceFetch || _alwaysFetchComponentFillings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentFillings);
				_componentFillings.SuppressClearInGetMulti=!forceFetch;
				_componentFillings.EntityFactoryToUse = entityFactoryToUse;
				_componentFillings.GetMultiManyToOne(this, null, null, null, null, filter);
				_componentFillings.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentFillings = true;
			}
			return _componentFillings;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentFillings'. These settings will be taken into account
		/// when the property ComponentFillings is requested or GetMultiComponentFillings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentFillings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentFillings.SortClauses=sortClauses;
			_componentFillings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AmCashservices", _amCashservices);
			toReturn.Add("Components", _components);
			toReturn.Add("ComponentClearings", _componentClearings);
			toReturn.Add("ComponentFillings", _componentFillings);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="validator">The validator object for this AutomatEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 automatID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(automatID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_amCashservices = new VarioSL.Entities.CollectionClasses.CashServiceCollection();
			_amCashservices.SetContainingEntityInfo(this, "Automat");

			_components = new VarioSL.Entities.CollectionClasses.ComponentCollection();
			_components.SetContainingEntityInfo(this, "Automat");

			_componentClearings = new VarioSL.Entities.CollectionClasses.ComponentClearingCollection();
			_componentClearings.SetContainingEntityInfo(this, "Automat");

			_componentFillings = new VarioSL.Entities.CollectionClasses.ComponentFillingCollection();
			_componentFillings.SetContainingEntityInfo(this, "Automat");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Error", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastAlarm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Warning", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="automatID">PK value for Automat which data should be fetched into this Automat object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 automatID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AutomatFieldIndex.AutomatID].ForcedCurrentValueWrite(automatID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAutomatDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AutomatEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AutomatRelations Relations
		{
			get	{ return new AutomatRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashService' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmCashservices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceCollection(), (IEntityRelation)GetRelationsForField("AmCashservices")[0], (int)VarioSL.Entities.EntityType.AutomatEntity, (int)VarioSL.Entities.EntityType.CashServiceEntity, 0, null, null, null, "AmCashservices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Component' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentCollection(), (IEntityRelation)GetRelationsForField("Components")[0], (int)VarioSL.Entities.EntityType.AutomatEntity, (int)VarioSL.Entities.EntityType.ComponentEntity, 0, null, null, null, "Components", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentClearing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentClearings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentClearingCollection(), (IEntityRelation)GetRelationsForField("ComponentClearings")[0], (int)VarioSL.Entities.EntityType.AutomatEntity, (int)VarioSL.Entities.EntityType.ComponentClearingEntity, 0, null, null, null, "ComponentClearings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentFilling' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentFillings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentFillingCollection(), (IEntityRelation)GetRelationsForField("ComponentFillings")[0], (int)VarioSL.Entities.EntityType.AutomatEntity, (int)VarioSL.Entities.EntityType.ComponentFillingEntity, 0, null, null, null, "ComponentFillings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutomatID property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."AUTOMATID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AutomatID
		{
			get { return (System.Int64)GetValue((int)AutomatFieldIndex.AutomatID, true); }
			set	{ SetValue((int)AutomatFieldIndex.AutomatID, value, true); }
		}

		/// <summary> The AutomatNumber property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."AUTOMATNR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AutomatNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutomatFieldIndex.AutomatNumber, false); }
			set	{ SetValue((int)AutomatFieldIndex.AutomatNumber, value, true); }
		}

		/// <summary> The CashAmount property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."CASHAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashAmount
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutomatFieldIndex.CashAmount, false); }
			set	{ SetValue((int)AutomatFieldIndex.CashAmount, value, true); }
		}

		/// <summary> The ClientID property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutomatFieldIndex.ClientID, false); }
			set	{ SetValue((int)AutomatFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AutomatFieldIndex.CreationDate, false); }
			set	{ SetValue((int)AutomatFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The DeviceClass property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."DEVICECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClass
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutomatFieldIndex.DeviceClass, false); }
			set	{ SetValue((int)AutomatFieldIndex.DeviceClass, value, true); }
		}

		/// <summary> The Error property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."ERROR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Error
		{
			get { return (Nullable<System.Int16>)GetValue((int)AutomatFieldIndex.Error, false); }
			set	{ SetValue((int)AutomatFieldIndex.Error, value, true); }
		}

		/// <summary> The LastAlarm property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."LASTALARM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastAlarm
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AutomatFieldIndex.LastAlarm, false); }
			set	{ SetValue((int)AutomatFieldIndex.LastAlarm, value, true); }
		}

		/// <summary> The LastData property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."LASTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastData
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AutomatFieldIndex.LastData, false); }
			set	{ SetValue((int)AutomatFieldIndex.LastData, value, true); }
		}

		/// <summary> The MountingPlateNumber property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."MOUNTINGPLATENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> MountingPlateNumber
		{
			get { return (Nullable<System.Int16>)GetValue((int)AutomatFieldIndex.MountingPlateNumber, false); }
			set	{ SetValue((int)AutomatFieldIndex.MountingPlateNumber, value, true); }
		}

		/// <summary> The Notice property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."NOTICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notice
		{
			get { return (System.String)GetValue((int)AutomatFieldIndex.Notice, true); }
			set	{ SetValue((int)AutomatFieldIndex.Notice, value, true); }
		}

		/// <summary> The VehicleNo property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."VEHICLENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VehicleNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)AutomatFieldIndex.VehicleNo, false); }
			set	{ SetValue((int)AutomatFieldIndex.VehicleNo, value, true); }
		}

		/// <summary> The Warning property of the Entity Automat<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_AUTOMAT"."WARNING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Warning
		{
			get { return (Nullable<System.Int16>)GetValue((int)AutomatFieldIndex.Warning, false); }
			set	{ SetValue((int)AutomatFieldIndex.Warning, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CashServiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAmCashservices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceCollection AmCashservices
		{
			get	{ return GetMultiAmCashservices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AmCashservices. When set to true, AmCashservices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmCashservices is accessed. You can always execute/ a forced fetch by calling GetMultiAmCashservices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmCashservices
		{
			get	{ return _alwaysFetchAmCashservices; }
			set	{ _alwaysFetchAmCashservices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmCashservices already has been fetched. Setting this property to false when AmCashservices has been fetched
		/// will clear the AmCashservices collection well. Setting this property to true while AmCashservices hasn't been fetched disables lazy loading for AmCashservices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmCashservices
		{
			get { return _alreadyFetchedAmCashservices;}
			set 
			{
				if(_alreadyFetchedAmCashservices && !value && (_amCashservices != null))
				{
					_amCashservices.Clear();
				}
				_alreadyFetchedAmCashservices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentCollection Components
		{
			get	{ return GetMultiComponents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Components. When set to true, Components is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Components is accessed. You can always execute/ a forced fetch by calling GetMultiComponents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponents
		{
			get	{ return _alwaysFetchComponents; }
			set	{ _alwaysFetchComponents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Components already has been fetched. Setting this property to false when Components has been fetched
		/// will clear the Components collection well. Setting this property to true while Components hasn't been fetched disables lazy loading for Components</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponents
		{
			get { return _alreadyFetchedComponents;}
			set 
			{
				if(_alreadyFetchedComponents && !value && (_components != null))
				{
					_components.Clear();
				}
				_alreadyFetchedComponents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentClearings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection ComponentClearings
		{
			get	{ return GetMultiComponentClearings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentClearings. When set to true, ComponentClearings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentClearings is accessed. You can always execute/ a forced fetch by calling GetMultiComponentClearings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentClearings
		{
			get	{ return _alwaysFetchComponentClearings; }
			set	{ _alwaysFetchComponentClearings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentClearings already has been fetched. Setting this property to false when ComponentClearings has been fetched
		/// will clear the ComponentClearings collection well. Setting this property to true while ComponentClearings hasn't been fetched disables lazy loading for ComponentClearings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentClearings
		{
			get { return _alreadyFetchedComponentClearings;}
			set 
			{
				if(_alreadyFetchedComponentClearings && !value && (_componentClearings != null))
				{
					_componentClearings.Clear();
				}
				_alreadyFetchedComponentClearings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentFillings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection ComponentFillings
		{
			get	{ return GetMultiComponentFillings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentFillings. When set to true, ComponentFillings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentFillings is accessed. You can always execute/ a forced fetch by calling GetMultiComponentFillings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentFillings
		{
			get	{ return _alwaysFetchComponentFillings; }
			set	{ _alwaysFetchComponentFillings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentFillings already has been fetched. Setting this property to false when ComponentFillings has been fetched
		/// will clear the ComponentFillings collection well. Setting this property to true while ComponentFillings hasn't been fetched disables lazy loading for ComponentFillings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentFillings
		{
			get { return _alreadyFetchedComponentFillings;}
			set 
			{
				if(_alreadyFetchedComponentFillings && !value && (_componentFillings != null))
				{
					_componentFillings.Clear();
				}
				_alreadyFetchedComponentFillings = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AutomatEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
