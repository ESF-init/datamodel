﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AreaList'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AreaListEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AreaListElementGroupEntity _areaListElementGroup;
		private bool	_alwaysFetchAreaListElementGroup, _alreadyFetchedAreaListElementGroup, _areaListElementGroupReturnsNewIfNotFound;
		private AreaTypeEntity _areaListType;
		private bool	_alwaysFetchAreaListType, _alreadyFetchedAreaListType, _areaListTypeReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValue;
		private bool	_alwaysFetchAttributeValue, _alreadyFetchedAttributeValue, _attributeValueReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AreaListElementGroup</summary>
			public static readonly string AreaListElementGroup = "AreaListElementGroup";
			/// <summary>Member name AreaListType</summary>
			public static readonly string AreaListType = "AreaListType";
			/// <summary>Member name AttributeValue</summary>
			public static readonly string AttributeValue = "AttributeValue";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AreaListEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AreaListEntity() :base("AreaListEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		public AreaListEntity(System.Int64 areaListID):base("AreaListEntity")
		{
			InitClassFetch(areaListID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AreaListEntity(System.Int64 areaListID, IPrefetchPath prefetchPathToUse):base("AreaListEntity")
		{
			InitClassFetch(areaListID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="validator">The custom validator object for this AreaListEntity</param>
		public AreaListEntity(System.Int64 areaListID, IValidator validator):base("AreaListEntity")
		{
			InitClassFetch(areaListID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AreaListEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaListElementGroup = (AreaListElementGroupEntity)info.GetValue("_areaListElementGroup", typeof(AreaListElementGroupEntity));
			if(_areaListElementGroup!=null)
			{
				_areaListElementGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_areaListElementGroupReturnsNewIfNotFound = info.GetBoolean("_areaListElementGroupReturnsNewIfNotFound");
			_alwaysFetchAreaListElementGroup = info.GetBoolean("_alwaysFetchAreaListElementGroup");
			_alreadyFetchedAreaListElementGroup = info.GetBoolean("_alreadyFetchedAreaListElementGroup");

			_areaListType = (AreaTypeEntity)info.GetValue("_areaListType", typeof(AreaTypeEntity));
			if(_areaListType!=null)
			{
				_areaListType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_areaListTypeReturnsNewIfNotFound = info.GetBoolean("_areaListTypeReturnsNewIfNotFound");
			_alwaysFetchAreaListType = info.GetBoolean("_alwaysFetchAreaListType");
			_alreadyFetchedAreaListType = info.GetBoolean("_alreadyFetchedAreaListType");

			_attributeValue = (AttributeValueEntity)info.GetValue("_attributeValue", typeof(AttributeValueEntity));
			if(_attributeValue!=null)
			{
				_attributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueReturnsNewIfNotFound = info.GetBoolean("_attributeValueReturnsNewIfNotFound");
			_alwaysFetchAttributeValue = info.GetBoolean("_alwaysFetchAttributeValue");
			_alreadyFetchedAttributeValue = info.GetBoolean("_alreadyFetchedAttributeValue");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AreaListFieldIndex)fieldIndex)
			{
				case AreaListFieldIndex.AreaListElementGroupId:
					DesetupSyncAreaListElementGroup(true, false);
					_alreadyFetchedAreaListElementGroup = false;
					break;
				case AreaListFieldIndex.AreaListTypeID:
					DesetupSyncAreaListType(true, false);
					_alreadyFetchedAreaListType = false;
					break;
				case AreaListFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case AreaListFieldIndex.TicketGroupAttributeID:
					DesetupSyncAttributeValue(true, false);
					_alreadyFetchedAttributeValue = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaListElementGroup = (_areaListElementGroup != null);
			_alreadyFetchedAreaListType = (_areaListType != null);
			_alreadyFetchedAttributeValue = (_attributeValue != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AreaListElementGroup":
					toReturn.Add(Relations.AreaListElementGroupEntityUsingAreaListElementGroupId);
					break;
				case "AreaListType":
					toReturn.Add(Relations.AreaTypeEntityUsingAreaListTypeID);
					break;
				case "AttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingTicketGroupAttributeID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaListElementGroup", (!this.MarkedForDeletion?_areaListElementGroup:null));
			info.AddValue("_areaListElementGroupReturnsNewIfNotFound", _areaListElementGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAreaListElementGroup", _alwaysFetchAreaListElementGroup);
			info.AddValue("_alreadyFetchedAreaListElementGroup", _alreadyFetchedAreaListElementGroup);
			info.AddValue("_areaListType", (!this.MarkedForDeletion?_areaListType:null));
			info.AddValue("_areaListTypeReturnsNewIfNotFound", _areaListTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAreaListType", _alwaysFetchAreaListType);
			info.AddValue("_alreadyFetchedAreaListType", _alreadyFetchedAreaListType);
			info.AddValue("_attributeValue", (!this.MarkedForDeletion?_attributeValue:null));
			info.AddValue("_attributeValueReturnsNewIfNotFound", _attributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValue", _alwaysFetchAttributeValue);
			info.AddValue("_alreadyFetchedAttributeValue", _alreadyFetchedAttributeValue);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AreaListElementGroup":
					_alreadyFetchedAreaListElementGroup = true;
					this.AreaListElementGroup = (AreaListElementGroupEntity)entity;
					break;
				case "AreaListType":
					_alreadyFetchedAreaListType = true;
					this.AreaListType = (AreaTypeEntity)entity;
					break;
				case "AttributeValue":
					_alreadyFetchedAttributeValue = true;
					this.AttributeValue = (AttributeValueEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AreaListElementGroup":
					SetupSyncAreaListElementGroup(relatedEntity);
					break;
				case "AreaListType":
					SetupSyncAreaListType(relatedEntity);
					break;
				case "AttributeValue":
					SetupSyncAttributeValue(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AreaListElementGroup":
					DesetupSyncAreaListElementGroup(false, true);
					break;
				case "AreaListType":
					DesetupSyncAreaListType(false, true);
					break;
				case "AttributeValue":
					DesetupSyncAttributeValue(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_areaListElementGroup!=null)
			{
				toReturn.Add(_areaListElementGroup);
			}
			if(_areaListType!=null)
			{
				toReturn.Add(_areaListType);
			}
			if(_attributeValue!=null)
			{
				toReturn.Add(_attributeValue);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListID)
		{
			return FetchUsingPK(areaListID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(areaListID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(areaListID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 areaListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(areaListID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AreaListID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AreaListRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AreaListElementGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AreaListElementGroupEntity' which is related to this entity.</returns>
		public AreaListElementGroupEntity GetSingleAreaListElementGroup()
		{
			return GetSingleAreaListElementGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'AreaListElementGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AreaListElementGroupEntity' which is related to this entity.</returns>
		public virtual AreaListElementGroupEntity GetSingleAreaListElementGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedAreaListElementGroup || forceFetch || _alwaysFetchAreaListElementGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AreaListElementGroupEntityUsingAreaListElementGroupId);
				AreaListElementGroupEntity newEntity = new AreaListElementGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AreaListElementGroupId);
				}
				if(fetchResult)
				{
					newEntity = (AreaListElementGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_areaListElementGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AreaListElementGroup = newEntity;
				_alreadyFetchedAreaListElementGroup = fetchResult;
			}
			return _areaListElementGroup;
		}


		/// <summary> Retrieves the related entity of type 'AreaTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AreaTypeEntity' which is related to this entity.</returns>
		public AreaTypeEntity GetSingleAreaListType()
		{
			return GetSingleAreaListType(false);
		}

		/// <summary> Retrieves the related entity of type 'AreaTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AreaTypeEntity' which is related to this entity.</returns>
		public virtual AreaTypeEntity GetSingleAreaListType(bool forceFetch)
		{
			if( ( !_alreadyFetchedAreaListType || forceFetch || _alwaysFetchAreaListType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AreaTypeEntityUsingAreaListTypeID);
				AreaTypeEntity newEntity = new AreaTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AreaListTypeID);
				}
				if(fetchResult)
				{
					newEntity = (AreaTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_areaListTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AreaListType = newEntity;
				_alreadyFetchedAreaListType = fetchResult;
			}
			return _areaListType;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValue()
		{
			return GetSingleAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValue || forceFetch || _alwaysFetchAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingTicketGroupAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketGroupAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValue = newEntity;
				_alreadyFetchedAttributeValue = fetchResult;
			}
			return _attributeValue;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AreaListElementGroup", _areaListElementGroup);
			toReturn.Add("AreaListType", _areaListType);
			toReturn.Add("AttributeValue", _attributeValue);
			toReturn.Add("Tariff", _tariff);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="validator">The validator object for this AreaListEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 areaListID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(areaListID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_areaListElementGroupReturnsNewIfNotFound = false;
			_areaListTypeReturnsNewIfNotFound = false;
			_attributeValueReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListElementGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListKeyGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaListTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrgID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Sequential", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpatialReferenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketGroupAttributeID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _areaListElementGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAreaListElementGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _areaListElementGroup, new PropertyChangedEventHandler( OnAreaListElementGroupPropertyChanged ), "AreaListElementGroup", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AreaListElementGroupEntityUsingAreaListElementGroupIdStatic, true, signalRelatedEntity, "AreaList", resetFKFields, new int[] { (int)AreaListFieldIndex.AreaListElementGroupId } );		
			_areaListElementGroup = null;
		}
		
		/// <summary> setups the sync logic for member _areaListElementGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAreaListElementGroup(IEntityCore relatedEntity)
		{
			if(_areaListElementGroup!=relatedEntity)
			{		
				DesetupSyncAreaListElementGroup(true, true);
				_areaListElementGroup = (AreaListElementGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _areaListElementGroup, new PropertyChangedEventHandler( OnAreaListElementGroupPropertyChanged ), "AreaListElementGroup", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AreaListElementGroupEntityUsingAreaListElementGroupIdStatic, true, ref _alreadyFetchedAreaListElementGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAreaListElementGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _areaListType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAreaListType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _areaListType, new PropertyChangedEventHandler( OnAreaListTypePropertyChanged ), "AreaListType", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AreaTypeEntityUsingAreaListTypeIDStatic, true, signalRelatedEntity, "AreaListType", resetFKFields, new int[] { (int)AreaListFieldIndex.AreaListTypeID } );		
			_areaListType = null;
		}
		
		/// <summary> setups the sync logic for member _areaListType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAreaListType(IEntityCore relatedEntity)
		{
			if(_areaListType!=relatedEntity)
			{		
				DesetupSyncAreaListType(true, true);
				_areaListType = (AreaTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _areaListType, new PropertyChangedEventHandler( OnAreaListTypePropertyChanged ), "AreaListType", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AreaTypeEntityUsingAreaListTypeIDStatic, true, ref _alreadyFetchedAreaListType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAreaListTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValue, new PropertyChangedEventHandler( OnAttributeValuePropertyChanged ), "AttributeValue", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AttributeValueEntityUsingTicketGroupAttributeIDStatic, true, signalRelatedEntity, "AreaList", resetFKFields, new int[] { (int)AreaListFieldIndex.TicketGroupAttributeID } );		
			_attributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValue(IEntityCore relatedEntity)
		{
			if(_attributeValue!=relatedEntity)
			{		
				DesetupSyncAttributeValue(true, true);
				_attributeValue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValue, new PropertyChangedEventHandler( OnAttributeValuePropertyChanged ), "AttributeValue", VarioSL.Entities.RelationClasses.StaticAreaListRelations.AttributeValueEntityUsingTicketGroupAttributeIDStatic, true, ref _alreadyFetchedAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAreaListRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "AreaList", resetFKFields, new int[] { (int)AreaListFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticAreaListRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="areaListID">PK value for AreaList which data should be fetched into this AreaList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 areaListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AreaListFieldIndex.AreaListID].ForcedCurrentValueWrite(areaListID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAreaListDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AreaListEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AreaListRelations Relations
		{
			get	{ return new AreaListRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaListElementGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListElementGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListElementGroupCollection(), (IEntityRelation)GetRelationsForField("AreaListElementGroup")[0], (int)VarioSL.Entities.EntityType.AreaListEntity, (int)VarioSL.Entities.EntityType.AreaListElementGroupEntity, 0, null, null, null, "AreaListElementGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaTypeCollection(), (IEntityRelation)GetRelationsForField("AreaListType")[0], (int)VarioSL.Entities.EntityType.AreaListEntity, (int)VarioSL.Entities.EntityType.AreaTypeEntity, 0, null, null, null, "AreaListType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValue")[0], (int)VarioSL.Entities.EntityType.AreaListEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.AreaListEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AreaListElementGroupId property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."AREALISTELEMENTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Int64 AreaListElementGroupId
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.AreaListElementGroupId, true); }
			set	{ SetValue((int)AreaListFieldIndex.AreaListElementGroupId, value, true); }
		}

		/// <summary> The AreaListID property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."AREALISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 AreaListID
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.AreaListID, true); }
			set	{ SetValue((int)AreaListFieldIndex.AreaListID, value, true); }
		}

		/// <summary> The AreaListKey property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."AREALISTKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AreaListKey
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.AreaListKey, true); }
			set	{ SetValue((int)AreaListFieldIndex.AreaListKey, value, true); }
		}

		/// <summary> The AreaListKeyGroup property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."AREALISTKEYGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AreaListKeyGroup
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.AreaListKeyGroup, true); }
			set	{ SetValue((int)AreaListFieldIndex.AreaListKeyGroup, value, true); }
		}

		/// <summary> The AreaListTypeID property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."AREALISTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AreaListTypeID
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.AreaListTypeID, true); }
			set	{ SetValue((int)AreaListFieldIndex.AreaListTypeID, value, true); }
		}

		/// <summary> The OrgID property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."ORGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrgID
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.OrgID, true); }
			set	{ SetValue((int)AreaListFieldIndex.OrgID, value, true); }
		}

		/// <summary> The Sequential property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."SEQUENTIAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Sequential
		{
			get { return (Nullable<System.Int16>)GetValue((int)AreaListFieldIndex.Sequential, false); }
			set	{ SetValue((int)AreaListFieldIndex.Sequential, value, true); }
		}

		/// <summary> The SpatialReferenceNumber property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."SPATIALREFERENCENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SpatialReferenceNumber
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.SpatialReferenceNumber, true); }
			set	{ SetValue((int)AreaListFieldIndex.SpatialReferenceNumber, value, true); }
		}

		/// <summary> The TariffID property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)AreaListFieldIndex.TariffID, true); }
			set	{ SetValue((int)AreaListFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TicketGroupAttributeID property of the Entity AreaList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREALIST"."TICKETGROUPATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketGroupAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AreaListFieldIndex.TicketGroupAttributeID, false); }
			set	{ SetValue((int)AreaListFieldIndex.TicketGroupAttributeID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AreaListElementGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAreaListElementGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AreaListElementGroupEntity AreaListElementGroup
		{
			get	{ return GetSingleAreaListElementGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAreaListElementGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaList", "AreaListElementGroup", _areaListElementGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListElementGroup. When set to true, AreaListElementGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListElementGroup is accessed. You can always execute a forced fetch by calling GetSingleAreaListElementGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListElementGroup
		{
			get	{ return _alwaysFetchAreaListElementGroup; }
			set	{ _alwaysFetchAreaListElementGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListElementGroup already has been fetched. Setting this property to false when AreaListElementGroup has been fetched
		/// will set AreaListElementGroup to null as well. Setting this property to true while AreaListElementGroup hasn't been fetched disables lazy loading for AreaListElementGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListElementGroup
		{
			get { return _alreadyFetchedAreaListElementGroup;}
			set 
			{
				if(_alreadyFetchedAreaListElementGroup && !value)
				{
					this.AreaListElementGroup = null;
				}
				_alreadyFetchedAreaListElementGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AreaListElementGroup is not found
		/// in the database. When set to true, AreaListElementGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AreaListElementGroupReturnsNewIfNotFound
		{
			get	{ return _areaListElementGroupReturnsNewIfNotFound; }
			set { _areaListElementGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AreaTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAreaListType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AreaTypeEntity AreaListType
		{
			get	{ return GetSingleAreaListType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAreaListType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaListType", "AreaListType", _areaListType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListType. When set to true, AreaListType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListType is accessed. You can always execute a forced fetch by calling GetSingleAreaListType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListType
		{
			get	{ return _alwaysFetchAreaListType; }
			set	{ _alwaysFetchAreaListType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListType already has been fetched. Setting this property to false when AreaListType has been fetched
		/// will set AreaListType to null as well. Setting this property to true while AreaListType hasn't been fetched disables lazy loading for AreaListType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListType
		{
			get { return _alreadyFetchedAreaListType;}
			set 
			{
				if(_alreadyFetchedAreaListType && !value)
				{
					this.AreaListType = null;
				}
				_alreadyFetchedAreaListType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AreaListType is not found
		/// in the database. When set to true, AreaListType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AreaListTypeReturnsNewIfNotFound
		{
			get	{ return _areaListTypeReturnsNewIfNotFound; }
			set { _areaListTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValue
		{
			get	{ return GetSingleAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaList", "AttributeValue", _attributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValue. When set to true, AttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValue is accessed. You can always execute a forced fetch by calling GetSingleAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValue
		{
			get	{ return _alwaysFetchAttributeValue; }
			set	{ _alwaysFetchAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValue already has been fetched. Setting this property to false when AttributeValue has been fetched
		/// will set AttributeValue to null as well. Setting this property to true while AttributeValue hasn't been fetched disables lazy loading for AttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValue
		{
			get { return _alreadyFetchedAttributeValue;}
			set 
			{
				if(_alreadyFetchedAttributeValue && !value)
				{
					this.AttributeValue = null;
				}
				_alreadyFetchedAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValue is not found
		/// in the database. When set to true, AttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueReturnsNewIfNotFound
		{
			get	{ return _attributeValueReturnsNewIfNotFound; }
			set { _attributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AreaList", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AreaListEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
