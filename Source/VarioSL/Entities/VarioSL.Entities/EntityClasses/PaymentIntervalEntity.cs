﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentInterval'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PaymentIntervalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection	_ticketPaymentIntervals;
		private bool	_alwaysFetchTicketPaymentIntervals, _alreadyFetchedTicketPaymentIntervals;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketPaymentInterval;
		private bool	_alwaysFetchTicketCollectionViaTicketPaymentInterval, _alreadyFetchedTicketCollectionViaTicketPaymentInterval;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TicketPaymentIntervals</summary>
			public static readonly string TicketPaymentIntervals = "TicketPaymentIntervals";
			/// <summary>Member name TicketCollectionViaTicketPaymentInterval</summary>
			public static readonly string TicketCollectionViaTicketPaymentInterval = "TicketCollectionViaTicketPaymentInterval";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentIntervalEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentIntervalEntity() :base("PaymentIntervalEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		public PaymentIntervalEntity(System.Int32 paymentIntervalID):base("PaymentIntervalEntity")
		{
			InitClassFetch(paymentIntervalID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentIntervalEntity(System.Int32 paymentIntervalID, IPrefetchPath prefetchPathToUse):base("PaymentIntervalEntity")
		{
			InitClassFetch(paymentIntervalID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="validator">The custom validator object for this PaymentIntervalEntity</param>
		public PaymentIntervalEntity(System.Int32 paymentIntervalID, IValidator validator):base("PaymentIntervalEntity")
		{
			InitClassFetch(paymentIntervalID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentIntervalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticketPaymentIntervals = (VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection)info.GetValue("_ticketPaymentIntervals", typeof(VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection));
			_alwaysFetchTicketPaymentIntervals = info.GetBoolean("_alwaysFetchTicketPaymentIntervals");
			_alreadyFetchedTicketPaymentIntervals = info.GetBoolean("_alreadyFetchedTicketPaymentIntervals");
			_ticketCollectionViaTicketPaymentInterval = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketPaymentInterval", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketPaymentInterval = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketPaymentInterval");
			_alreadyFetchedTicketCollectionViaTicketPaymentInterval = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketPaymentInterval");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicketPaymentIntervals = (_ticketPaymentIntervals.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketPaymentInterval = (_ticketCollectionViaTicketPaymentInterval.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TicketPaymentIntervals":
					toReturn.Add(Relations.TicketPaymentIntervalEntityUsingPaymentIntervalID);
					break;
				case "TicketCollectionViaTicketPaymentInterval":
					toReturn.Add(Relations.TicketPaymentIntervalEntityUsingPaymentIntervalID, "PaymentIntervalEntity__", "TicketPaymentInterval_", JoinHint.None);
					toReturn.Add(TicketPaymentIntervalEntity.Relations.TicketEntityUsingTicketID, "TicketPaymentInterval_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticketPaymentIntervals", (!this.MarkedForDeletion?_ticketPaymentIntervals:null));
			info.AddValue("_alwaysFetchTicketPaymentIntervals", _alwaysFetchTicketPaymentIntervals);
			info.AddValue("_alreadyFetchedTicketPaymentIntervals", _alreadyFetchedTicketPaymentIntervals);
			info.AddValue("_ticketCollectionViaTicketPaymentInterval", (!this.MarkedForDeletion?_ticketCollectionViaTicketPaymentInterval:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketPaymentInterval", _alwaysFetchTicketCollectionViaTicketPaymentInterval);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketPaymentInterval", _alreadyFetchedTicketCollectionViaTicketPaymentInterval);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TicketPaymentIntervals":
					_alreadyFetchedTicketPaymentIntervals = true;
					if(entity!=null)
					{
						this.TicketPaymentIntervals.Add((TicketPaymentIntervalEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketPaymentInterval":
					_alreadyFetchedTicketCollectionViaTicketPaymentInterval = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketPaymentInterval.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TicketPaymentIntervals":
					_ticketPaymentIntervals.Add((TicketPaymentIntervalEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TicketPaymentIntervals":
					this.PerformRelatedEntityRemoval(_ticketPaymentIntervals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ticketPaymentIntervals);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntervalID)
		{
			return FetchUsingPK(paymentIntervalID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntervalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentIntervalID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntervalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentIntervalID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 paymentIntervalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentIntervalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentIntervalID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentIntervalRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketPaymentIntervalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, _ticketPaymentIntervals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketPaymentIntervalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, _ticketPaymentIntervals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketPaymentIntervals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection GetMultiTicketPaymentIntervals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketPaymentIntervals || forceFetch || _alwaysFetchTicketPaymentIntervals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketPaymentIntervals);
				_ticketPaymentIntervals.SuppressClearInGetMulti=!forceFetch;
				_ticketPaymentIntervals.EntityFactoryToUse = entityFactoryToUse;
				_ticketPaymentIntervals.GetMultiManyToOne(this, null, filter);
				_ticketPaymentIntervals.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketPaymentIntervals = true;
			}
			return _ticketPaymentIntervals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketPaymentIntervals'. These settings will be taken into account
		/// when the property TicketPaymentIntervals is requested or GetMultiTicketPaymentIntervals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketPaymentIntervals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketPaymentIntervals.SortClauses=sortClauses;
			_ticketPaymentIntervals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketPaymentInterval(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketPaymentInterval(forceFetch, _ticketCollectionViaTicketPaymentInterval.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketPaymentInterval(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketPaymentInterval || forceFetch || _alwaysFetchTicketCollectionViaTicketPaymentInterval) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketPaymentInterval);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PaymentIntervalFields.PaymentIntervalID, ComparisonOperator.Equal, this.PaymentIntervalID, "PaymentIntervalEntity__"));
				_ticketCollectionViaTicketPaymentInterval.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketPaymentInterval.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketPaymentInterval.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketPaymentInterval"));
				_ticketCollectionViaTicketPaymentInterval.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketPaymentInterval = true;
			}
			return _ticketCollectionViaTicketPaymentInterval;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketPaymentInterval'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketPaymentInterval is requested or GetMultiTicketCollectionViaTicketPaymentInterval is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketPaymentInterval(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketPaymentInterval.SortClauses=sortClauses;
			_ticketCollectionViaTicketPaymentInterval.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TicketPaymentIntervals", _ticketPaymentIntervals);
			toReturn.Add("TicketCollectionViaTicketPaymentInterval", _ticketCollectionViaTicketPaymentInterval);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="validator">The validator object for this PaymentIntervalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 paymentIntervalID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentIntervalID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ticketPaymentIntervals = new VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection();
			_ticketPaymentIntervals.SetContainingEntityInfo(this, "PaymentInterval");
			_ticketCollectionViaTicketPaymentInterval = new VarioSL.Entities.CollectionClasses.TicketCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntervalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntervalName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentIntervalID">PK value for PaymentInterval which data should be fetched into this PaymentInterval object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 paymentIntervalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentIntervalFieldIndex.PaymentIntervalID].ForcedCurrentValueWrite(paymentIntervalID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentIntervalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentIntervalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentIntervalRelations Relations
		{
			get	{ return new PaymentIntervalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketPaymentInterval' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketPaymentIntervals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection(), (IEntityRelation)GetRelationsForField("TicketPaymentIntervals")[0], (int)VarioSL.Entities.EntityType.PaymentIntervalEntity, (int)VarioSL.Entities.EntityType.TicketPaymentIntervalEntity, 0, null, null, null, "TicketPaymentIntervals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketPaymentInterval
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketPaymentIntervalEntityUsingPaymentIntervalID;
				intermediateRelation.SetAliases(string.Empty, "TicketPaymentInterval_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.PaymentIntervalEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketPaymentInterval"), "TicketCollectionViaTicketPaymentInterval", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity PaymentInterval<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PAYMENTINTERVAL"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PaymentIntervalFieldIndex.Description, true); }
			set	{ SetValue((int)PaymentIntervalFieldIndex.Description, value, true); }
		}

		/// <summary> The PaymentIntervalID property of the Entity PaymentInterval<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PAYMENTINTERVAL"."PAYMENTINTERVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 PaymentIntervalID
		{
			get { return (System.Int32)GetValue((int)PaymentIntervalFieldIndex.PaymentIntervalID, true); }
			set	{ SetValue((int)PaymentIntervalFieldIndex.PaymentIntervalID, value, true); }
		}

		/// <summary> The PaymentIntervalName property of the Entity PaymentInterval<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PAYMENTINTERVAL"."PAYMENTINTERVALNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PaymentIntervalName
		{
			get { return (System.String)GetValue((int)PaymentIntervalFieldIndex.PaymentIntervalName, true); }
			set	{ SetValue((int)PaymentIntervalFieldIndex.PaymentIntervalName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TicketPaymentIntervalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketPaymentIntervals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketPaymentIntervalCollection TicketPaymentIntervals
		{
			get	{ return GetMultiTicketPaymentIntervals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketPaymentIntervals. When set to true, TicketPaymentIntervals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketPaymentIntervals is accessed. You can always execute/ a forced fetch by calling GetMultiTicketPaymentIntervals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketPaymentIntervals
		{
			get	{ return _alwaysFetchTicketPaymentIntervals; }
			set	{ _alwaysFetchTicketPaymentIntervals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketPaymentIntervals already has been fetched. Setting this property to false when TicketPaymentIntervals has been fetched
		/// will clear the TicketPaymentIntervals collection well. Setting this property to true while TicketPaymentIntervals hasn't been fetched disables lazy loading for TicketPaymentIntervals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketPaymentIntervals
		{
			get { return _alreadyFetchedTicketPaymentIntervals;}
			set 
			{
				if(_alreadyFetchedTicketPaymentIntervals && !value && (_ticketPaymentIntervals != null))
				{
					_ticketPaymentIntervals.Clear();
				}
				_alreadyFetchedTicketPaymentIntervals = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketPaymentInterval()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketPaymentInterval
		{
			get { return GetMultiTicketCollectionViaTicketPaymentInterval(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketPaymentInterval. When set to true, TicketCollectionViaTicketPaymentInterval is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketPaymentInterval is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketPaymentInterval(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketPaymentInterval
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketPaymentInterval; }
			set	{ _alwaysFetchTicketCollectionViaTicketPaymentInterval = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketPaymentInterval already has been fetched. Setting this property to false when TicketCollectionViaTicketPaymentInterval has been fetched
		/// will clear the TicketCollectionViaTicketPaymentInterval collection well. Setting this property to true while TicketCollectionViaTicketPaymentInterval hasn't been fetched disables lazy loading for TicketCollectionViaTicketPaymentInterval</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketPaymentInterval
		{
			get { return _alreadyFetchedTicketCollectionViaTicketPaymentInterval;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketPaymentInterval && !value && (_ticketCollectionViaTicketPaymentInterval != null))
				{
					_ticketCollectionViaTicketPaymentInterval.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketPaymentInterval = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PaymentIntervalEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
