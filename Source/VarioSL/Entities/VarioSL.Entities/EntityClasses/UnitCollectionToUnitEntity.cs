﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UnitCollectionToUnit'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UnitCollectionToUnitEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private UnitEntity _unit;
		private bool	_alwaysFetchUnit, _alreadyFetchedUnit, _unitReturnsNewIfNotFound;
		private UnitCollectionEntity _unitCollection;
		private bool	_alwaysFetchUnitCollection, _alreadyFetchedUnitCollection, _unitCollectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Unit</summary>
			public static readonly string Unit = "Unit";
			/// <summary>Member name UnitCollection</summary>
			public static readonly string UnitCollection = "UnitCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UnitCollectionToUnitEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UnitCollectionToUnitEntity() :base("UnitCollectionToUnitEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		public UnitCollectionToUnitEntity(System.Int64 unitCollectionID, System.Int64 unitID):base("UnitCollectionToUnitEntity")
		{
			InitClassFetch(unitCollectionID, unitID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UnitCollectionToUnitEntity(System.Int64 unitCollectionID, System.Int64 unitID, IPrefetchPath prefetchPathToUse):base("UnitCollectionToUnitEntity")
		{
			InitClassFetch(unitCollectionID, unitID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="validator">The custom validator object for this UnitCollectionToUnitEntity</param>
		public UnitCollectionToUnitEntity(System.Int64 unitCollectionID, System.Int64 unitID, IValidator validator):base("UnitCollectionToUnitEntity")
		{
			InitClassFetch(unitCollectionID, unitID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UnitCollectionToUnitEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_unit = (UnitEntity)info.GetValue("_unit", typeof(UnitEntity));
			if(_unit!=null)
			{
				_unit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitReturnsNewIfNotFound = info.GetBoolean("_unitReturnsNewIfNotFound");
			_alwaysFetchUnit = info.GetBoolean("_alwaysFetchUnit");
			_alreadyFetchedUnit = info.GetBoolean("_alreadyFetchedUnit");

			_unitCollection = (UnitCollectionEntity)info.GetValue("_unitCollection", typeof(UnitCollectionEntity));
			if(_unitCollection!=null)
			{
				_unitCollection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitCollectionReturnsNewIfNotFound = info.GetBoolean("_unitCollectionReturnsNewIfNotFound");
			_alwaysFetchUnitCollection = info.GetBoolean("_alwaysFetchUnitCollection");
			_alreadyFetchedUnitCollection = info.GetBoolean("_alreadyFetchedUnitCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UnitCollectionToUnitFieldIndex)fieldIndex)
			{
				case UnitCollectionToUnitFieldIndex.UnitCollectionID:
					DesetupSyncUnitCollection(true, false);
					_alreadyFetchedUnitCollection = false;
					break;
				case UnitCollectionToUnitFieldIndex.UnitID:
					DesetupSyncUnit(true, false);
					_alreadyFetchedUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUnit = (_unit != null);
			_alreadyFetchedUnitCollection = (_unitCollection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Unit":
					toReturn.Add(Relations.UnitEntityUsingUnitID);
					break;
				case "UnitCollection":
					toReturn.Add(Relations.UnitCollectionEntityUsingUnitCollectionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_unit", (!this.MarkedForDeletion?_unit:null));
			info.AddValue("_unitReturnsNewIfNotFound", _unitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnit", _alwaysFetchUnit);
			info.AddValue("_alreadyFetchedUnit", _alreadyFetchedUnit);
			info.AddValue("_unitCollection", (!this.MarkedForDeletion?_unitCollection:null));
			info.AddValue("_unitCollectionReturnsNewIfNotFound", _unitCollectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnitCollection", _alwaysFetchUnitCollection);
			info.AddValue("_alreadyFetchedUnitCollection", _alreadyFetchedUnitCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Unit":
					_alreadyFetchedUnit = true;
					this.Unit = (UnitEntity)entity;
					break;
				case "UnitCollection":
					_alreadyFetchedUnitCollection = true;
					this.UnitCollection = (UnitCollectionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Unit":
					SetupSyncUnit(relatedEntity);
					break;
				case "UnitCollection":
					SetupSyncUnitCollection(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Unit":
					DesetupSyncUnit(false, true);
					break;
				case "UnitCollection":
					DesetupSyncUnitCollection(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_unit!=null)
			{
				toReturn.Add(_unit);
			}
			if(_unitCollection!=null)
			{
				toReturn.Add(_unitCollection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, System.Int64 unitID)
		{
			return FetchUsingPK(unitCollectionID, unitID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, System.Int64 unitID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(unitCollectionID, unitID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(unitCollectionID, unitID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 unitCollectionID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(unitCollectionID, unitID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UnitCollectionID, this.UnitID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UnitCollectionToUnitRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public UnitEntity GetSingleUnit()
		{
			return GetSingleUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public virtual UnitEntity GetSingleUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnit || forceFetch || _alwaysFetchUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitEntityUsingUnitID);
				UnitEntity newEntity = new UnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitID);
				}
				if(fetchResult)
				{
					newEntity = (UnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Unit = newEntity;
				_alreadyFetchedUnit = fetchResult;
			}
			return _unit;
		}


		/// <summary> Retrieves the related entity of type 'UnitCollectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitCollectionEntity' which is related to this entity.</returns>
		public UnitCollectionEntity GetSingleUnitCollection()
		{
			return GetSingleUnitCollection(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitCollectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitCollectionEntity' which is related to this entity.</returns>
		public virtual UnitCollectionEntity GetSingleUnitCollection(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnitCollection || forceFetch || _alwaysFetchUnitCollection) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitCollectionEntityUsingUnitCollectionID);
				UnitCollectionEntity newEntity = new UnitCollectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitCollectionID);
				}
				if(fetchResult)
				{
					newEntity = (UnitCollectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitCollectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UnitCollection = newEntity;
				_alreadyFetchedUnitCollection = fetchResult;
			}
			return _unitCollection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Unit", _unit);
			toReturn.Add("UnitCollection", _unitCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="validator">The validator object for this UnitCollectionToUnitEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 unitCollectionID, System.Int64 unitID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(unitCollectionID, unitID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_unitReturnsNewIfNotFound = false;
			_unitCollectionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitCollectionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _unit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticUnitCollectionToUnitRelations.UnitEntityUsingUnitIDStatic, true, signalRelatedEntity, "UnitCollectionToUnits", resetFKFields, new int[] { (int)UnitCollectionToUnitFieldIndex.UnitID } );		
			_unit = null;
		}
		
		/// <summary> setups the sync logic for member _unit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnit(IEntityCore relatedEntity)
		{
			if(_unit!=relatedEntity)
			{		
				DesetupSyncUnit(true, true);
				_unit = (UnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticUnitCollectionToUnitRelations.UnitEntityUsingUnitIDStatic, true, ref _alreadyFetchedUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _unitCollection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnitCollection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unitCollection, new PropertyChangedEventHandler( OnUnitCollectionPropertyChanged ), "UnitCollection", VarioSL.Entities.RelationClasses.StaticUnitCollectionToUnitRelations.UnitCollectionEntityUsingUnitCollectionIDStatic, true, signalRelatedEntity, "UnitCollectionToUnits", resetFKFields, new int[] { (int)UnitCollectionToUnitFieldIndex.UnitCollectionID } );		
			_unitCollection = null;
		}
		
		/// <summary> setups the sync logic for member _unitCollection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnitCollection(IEntityCore relatedEntity)
		{
			if(_unitCollection!=relatedEntity)
			{		
				DesetupSyncUnitCollection(true, true);
				_unitCollection = (UnitCollectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unitCollection, new PropertyChangedEventHandler( OnUnitCollectionPropertyChanged ), "UnitCollection", VarioSL.Entities.RelationClasses.StaticUnitCollectionToUnitRelations.UnitCollectionEntityUsingUnitCollectionIDStatic, true, ref _alreadyFetchedUnitCollection, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitCollectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="unitCollectionID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="unitID">PK value for UnitCollectionToUnit which data should be fetched into this UnitCollectionToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 unitCollectionID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UnitCollectionToUnitFieldIndex.UnitCollectionID].ForcedCurrentValueWrite(unitCollectionID);
				this.Fields[(int)UnitCollectionToUnitFieldIndex.UnitID].ForcedCurrentValueWrite(unitID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUnitCollectionToUnitDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UnitCollectionToUnitEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UnitCollectionToUnitRelations Relations
		{
			get	{ return new UnitCollectionToUnitRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("Unit")[0], (int)VarioSL.Entities.EntityType.UnitCollectionToUnitEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "Unit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollection
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionCollection(), (IEntityRelation)GetRelationsForField("UnitCollection")[0], (int)VarioSL.Entities.EntityType.UnitCollectionToUnitEntity, (int)VarioSL.Entities.EntityType.UnitCollectionEntity, 0, null, null, null, "UnitCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UnitCollectionID property of the Entity UnitCollectionToUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTIONTOUNIT"."UNITCOLLECTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 UnitCollectionID
		{
			get { return (System.Int64)GetValue((int)UnitCollectionToUnitFieldIndex.UnitCollectionID, true); }
			set	{ SetValue((int)UnitCollectionToUnitFieldIndex.UnitCollectionID, value, true); }
		}

		/// <summary> The UnitID property of the Entity UnitCollectionToUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_UNITCOLLECTIONTOUNIT"."UNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 UnitID
		{
			get { return (System.Int64)GetValue((int)UnitCollectionToUnitFieldIndex.UnitID, true); }
			set	{ SetValue((int)UnitCollectionToUnitFieldIndex.UnitID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'UnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitEntity Unit
		{
			get	{ return GetSingleUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UnitCollectionToUnits", "Unit", _unit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Unit. When set to true, Unit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Unit is accessed. You can always execute a forced fetch by calling GetSingleUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnit
		{
			get	{ return _alwaysFetchUnit; }
			set	{ _alwaysFetchUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Unit already has been fetched. Setting this property to false when Unit has been fetched
		/// will set Unit to null as well. Setting this property to true while Unit hasn't been fetched disables lazy loading for Unit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnit
		{
			get { return _alreadyFetchedUnit;}
			set 
			{
				if(_alreadyFetchedUnit && !value)
				{
					this.Unit = null;
				}
				_alreadyFetchedUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Unit is not found
		/// in the database. When set to true, Unit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitReturnsNewIfNotFound
		{
			get	{ return _unitReturnsNewIfNotFound; }
			set { _unitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UnitCollectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnitCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitCollectionEntity UnitCollection
		{
			get	{ return GetSingleUnitCollection(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnitCollection(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UnitCollectionToUnits", "UnitCollection", _unitCollection, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollection. When set to true, UnitCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollection is accessed. You can always execute a forced fetch by calling GetSingleUnitCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollection
		{
			get	{ return _alwaysFetchUnitCollection; }
			set	{ _alwaysFetchUnitCollection = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollection already has been fetched. Setting this property to false when UnitCollection has been fetched
		/// will set UnitCollection to null as well. Setting this property to true while UnitCollection hasn't been fetched disables lazy loading for UnitCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollection
		{
			get { return _alreadyFetchedUnitCollection;}
			set 
			{
				if(_alreadyFetchedUnitCollection && !value)
				{
					this.UnitCollection = null;
				}
				_alreadyFetchedUnitCollection = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UnitCollection is not found
		/// in the database. When set to true, UnitCollection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitCollectionReturnsNewIfNotFound
		{
			get	{ return _unitCollectionReturnsNewIfNotFound; }
			set { _unitCollectionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UnitCollectionToUnitEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
