﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PageContentGroupToContent'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PageContentGroupToContentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private PageContentGroupEntity _pageContentGroup;
		private bool	_alwaysFetchPageContentGroup, _alreadyFetchedPageContentGroup, _pageContentGroupReturnsNewIfNotFound;
		private PageContentEntity _pageContent;
		private bool	_alwaysFetchPageContent, _alreadyFetchedPageContent, _pageContentReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PageContentGroup</summary>
			public static readonly string PageContentGroup = "PageContentGroup";
			/// <summary>Member name PageContent</summary>
			public static readonly string PageContent = "PageContent";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PageContentGroupToContentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PageContentGroupToContentEntity() :base("PageContentGroupToContentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		public PageContentGroupToContentEntity(System.Int64 pageContentGroupid, System.Int64 pageContentID):base("PageContentGroupToContentEntity")
		{
			InitClassFetch(pageContentGroupid, pageContentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageContentGroupToContentEntity(System.Int64 pageContentGroupid, System.Int64 pageContentID, IPrefetchPath prefetchPathToUse):base("PageContentGroupToContentEntity")
		{
			InitClassFetch(pageContentGroupid, pageContentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="validator">The custom validator object for this PageContentGroupToContentEntity</param>
		public PageContentGroupToContentEntity(System.Int64 pageContentGroupid, System.Int64 pageContentID, IValidator validator):base("PageContentGroupToContentEntity")
		{
			InitClassFetch(pageContentGroupid, pageContentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageContentGroupToContentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pageContentGroup = (PageContentGroupEntity)info.GetValue("_pageContentGroup", typeof(PageContentGroupEntity));
			if(_pageContentGroup!=null)
			{
				_pageContentGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageContentGroupReturnsNewIfNotFound = info.GetBoolean("_pageContentGroupReturnsNewIfNotFound");
			_alwaysFetchPageContentGroup = info.GetBoolean("_alwaysFetchPageContentGroup");
			_alreadyFetchedPageContentGroup = info.GetBoolean("_alreadyFetchedPageContentGroup");

			_pageContent = (PageContentEntity)info.GetValue("_pageContent", typeof(PageContentEntity));
			if(_pageContent!=null)
			{
				_pageContent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageContentReturnsNewIfNotFound = info.GetBoolean("_pageContentReturnsNewIfNotFound");
			_alwaysFetchPageContent = info.GetBoolean("_alwaysFetchPageContent");
			_alreadyFetchedPageContent = info.GetBoolean("_alreadyFetchedPageContent");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PageContentGroupToContentFieldIndex)fieldIndex)
			{
				case PageContentGroupToContentFieldIndex.PageContentGroupid:
					DesetupSyncPageContentGroup(true, false);
					_alreadyFetchedPageContentGroup = false;
					break;
				case PageContentGroupToContentFieldIndex.PageContentID:
					DesetupSyncPageContent(true, false);
					_alreadyFetchedPageContent = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPageContentGroup = (_pageContentGroup != null);
			_alreadyFetchedPageContent = (_pageContent != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PageContentGroup":
					toReturn.Add(Relations.PageContentGroupEntityUsingPageContentGroupid);
					break;
				case "PageContent":
					toReturn.Add(Relations.PageContentEntityUsingPageContentID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pageContentGroup", (!this.MarkedForDeletion?_pageContentGroup:null));
			info.AddValue("_pageContentGroupReturnsNewIfNotFound", _pageContentGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageContentGroup", _alwaysFetchPageContentGroup);
			info.AddValue("_alreadyFetchedPageContentGroup", _alreadyFetchedPageContentGroup);
			info.AddValue("_pageContent", (!this.MarkedForDeletion?_pageContent:null));
			info.AddValue("_pageContentReturnsNewIfNotFound", _pageContentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageContent", _alwaysFetchPageContent);
			info.AddValue("_alreadyFetchedPageContent", _alreadyFetchedPageContent);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PageContentGroup":
					_alreadyFetchedPageContentGroup = true;
					this.PageContentGroup = (PageContentGroupEntity)entity;
					break;
				case "PageContent":
					_alreadyFetchedPageContent = true;
					this.PageContent = (PageContentEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PageContentGroup":
					SetupSyncPageContentGroup(relatedEntity);
					break;
				case "PageContent":
					SetupSyncPageContent(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PageContentGroup":
					DesetupSyncPageContentGroup(false, true);
					break;
				case "PageContent":
					DesetupSyncPageContent(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pageContentGroup!=null)
			{
				toReturn.Add(_pageContentGroup);
			}
			if(_pageContent!=null)
			{
				toReturn.Add(_pageContent);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentGroupid, System.Int64 pageContentID)
		{
			return FetchUsingPK(pageContentGroupid, pageContentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentGroupid, System.Int64 pageContentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pageContentGroupid, pageContentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentGroupid, System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pageContentGroupid, pageContentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pageContentGroupid, System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pageContentGroupid, pageContentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PageContentGroupid, this.PageContentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PageContentGroupToContentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PageContentGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageContentGroupEntity' which is related to this entity.</returns>
		public PageContentGroupEntity GetSinglePageContentGroup()
		{
			return GetSinglePageContentGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'PageContentGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageContentGroupEntity' which is related to this entity.</returns>
		public virtual PageContentGroupEntity GetSinglePageContentGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageContentGroup || forceFetch || _alwaysFetchPageContentGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageContentGroupEntityUsingPageContentGroupid);
				PageContentGroupEntity newEntity = new PageContentGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageContentGroupid);
				}
				if(fetchResult)
				{
					newEntity = (PageContentGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageContentGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageContentGroup = newEntity;
				_alreadyFetchedPageContentGroup = fetchResult;
			}
			return _pageContentGroup;
		}


		/// <summary> Retrieves the related entity of type 'PageContentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageContentEntity' which is related to this entity.</returns>
		public PageContentEntity GetSinglePageContent()
		{
			return GetSinglePageContent(false);
		}

		/// <summary> Retrieves the related entity of type 'PageContentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageContentEntity' which is related to this entity.</returns>
		public virtual PageContentEntity GetSinglePageContent(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageContent || forceFetch || _alwaysFetchPageContent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageContentEntityUsingPageContentID);
				PageContentEntity newEntity = new PageContentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageContentID);
				}
				if(fetchResult)
				{
					newEntity = (PageContentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageContentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageContent = newEntity;
				_alreadyFetchedPageContent = fetchResult;
			}
			return _pageContent;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PageContentGroup", _pageContentGroup);
			toReturn.Add("PageContent", _pageContent);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="validator">The validator object for this PageContentGroupToContentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 pageContentGroupid, System.Int64 pageContentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pageContentGroupid, pageContentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_pageContentGroupReturnsNewIfNotFound = false;
			_pageContentReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageContentGroupid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageContentID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pageContentGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageContentGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageContentGroup, new PropertyChangedEventHandler( OnPageContentGroupPropertyChanged ), "PageContentGroup", VarioSL.Entities.RelationClasses.StaticPageContentGroupToContentRelations.PageContentGroupEntityUsingPageContentGroupidStatic, true, signalRelatedEntity, "PageContentGroupToContents", resetFKFields, new int[] { (int)PageContentGroupToContentFieldIndex.PageContentGroupid } );		
			_pageContentGroup = null;
		}
		
		/// <summary> setups the sync logic for member _pageContentGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageContentGroup(IEntityCore relatedEntity)
		{
			if(_pageContentGroup!=relatedEntity)
			{		
				DesetupSyncPageContentGroup(true, true);
				_pageContentGroup = (PageContentGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageContentGroup, new PropertyChangedEventHandler( OnPageContentGroupPropertyChanged ), "PageContentGroup", VarioSL.Entities.RelationClasses.StaticPageContentGroupToContentRelations.PageContentGroupEntityUsingPageContentGroupidStatic, true, ref _alreadyFetchedPageContentGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageContentGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageContent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageContent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageContent, new PropertyChangedEventHandler( OnPageContentPropertyChanged ), "PageContent", VarioSL.Entities.RelationClasses.StaticPageContentGroupToContentRelations.PageContentEntityUsingPageContentIDStatic, true, signalRelatedEntity, "PageContentGroupToContent", resetFKFields, new int[] { (int)PageContentGroupToContentFieldIndex.PageContentID } );		
			_pageContent = null;
		}
		
		/// <summary> setups the sync logic for member _pageContent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageContent(IEntityCore relatedEntity)
		{
			if(_pageContent!=relatedEntity)
			{		
				DesetupSyncPageContent(true, true);
				_pageContent = (PageContentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageContent, new PropertyChangedEventHandler( OnPageContentPropertyChanged ), "PageContent", VarioSL.Entities.RelationClasses.StaticPageContentGroupToContentRelations.PageContentEntityUsingPageContentIDStatic, true, ref _alreadyFetchedPageContent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageContentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pageContentGroupid">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="pageContentID">PK value for PageContentGroupToContent which data should be fetched into this PageContentGroupToContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 pageContentGroupid, System.Int64 pageContentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PageContentGroupToContentFieldIndex.PageContentGroupid].ForcedCurrentValueWrite(pageContentGroupid);
				this.Fields[(int)PageContentGroupToContentFieldIndex.PageContentID].ForcedCurrentValueWrite(pageContentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePageContentGroupToContentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PageContentGroupToContentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PageContentGroupToContentRelations Relations
		{
			get	{ return new PageContentGroupToContentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContentGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContentGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentGroupCollection(), (IEntityRelation)GetRelationsForField("PageContentGroup")[0], (int)VarioSL.Entities.EntityType.PageContentGroupToContentEntity, (int)VarioSL.Entities.EntityType.PageContentGroupEntity, 0, null, null, null, "PageContentGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentCollection(), (IEntityRelation)GetRelationsForField("PageContent")[0], (int)VarioSL.Entities.EntityType.PageContentGroupToContentEntity, (int)VarioSL.Entities.EntityType.PageContentEntity, 0, null, null, null, "PageContent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PageContentGroupid property of the Entity PageContentGroupToContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PAGECONTENTGROUP_CONTENT"."PAGECONTENTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PageContentGroupid
		{
			get { return (System.Int64)GetValue((int)PageContentGroupToContentFieldIndex.PageContentGroupid, true); }
			set	{ SetValue((int)PageContentGroupToContentFieldIndex.PageContentGroupid, value, true); }
		}

		/// <summary> The PageContentID property of the Entity PageContentGroupToContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PAGECONTENTGROUP_CONTENT"."PAGECONTENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PageContentID
		{
			get { return (System.Int64)GetValue((int)PageContentGroupToContentFieldIndex.PageContentID, true); }
			set	{ SetValue((int)PageContentGroupToContentFieldIndex.PageContentID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PageContentGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageContentGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PageContentGroupEntity PageContentGroup
		{
			get	{ return GetSinglePageContentGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageContentGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageContentGroupToContents", "PageContentGroup", _pageContentGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageContentGroup. When set to true, PageContentGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContentGroup is accessed. You can always execute a forced fetch by calling GetSinglePageContentGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContentGroup
		{
			get	{ return _alwaysFetchPageContentGroup; }
			set	{ _alwaysFetchPageContentGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContentGroup already has been fetched. Setting this property to false when PageContentGroup has been fetched
		/// will set PageContentGroup to null as well. Setting this property to true while PageContentGroup hasn't been fetched disables lazy loading for PageContentGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContentGroup
		{
			get { return _alreadyFetchedPageContentGroup;}
			set 
			{
				if(_alreadyFetchedPageContentGroup && !value)
				{
					this.PageContentGroup = null;
				}
				_alreadyFetchedPageContentGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageContentGroup is not found
		/// in the database. When set to true, PageContentGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PageContentGroupReturnsNewIfNotFound
		{
			get	{ return _pageContentGroupReturnsNewIfNotFound; }
			set { _pageContentGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageContentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageContent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PageContentEntity PageContent
		{
			get	{ return GetSinglePageContent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageContent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PageContentGroupToContent", "PageContent", _pageContent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageContent. When set to true, PageContent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContent is accessed. You can always execute a forced fetch by calling GetSinglePageContent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContent
		{
			get	{ return _alwaysFetchPageContent; }
			set	{ _alwaysFetchPageContent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContent already has been fetched. Setting this property to false when PageContent has been fetched
		/// will set PageContent to null as well. Setting this property to true while PageContent hasn't been fetched disables lazy loading for PageContent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContent
		{
			get { return _alreadyFetchedPageContent;}
			set 
			{
				if(_alreadyFetchedPageContent && !value)
				{
					this.PageContent = null;
				}
				_alreadyFetchedPageContent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageContent is not found
		/// in the database. When set to true, PageContent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PageContentReturnsNewIfNotFound
		{
			get	{ return _pageContentReturnsNewIfNotFound; }
			set { _pageContentReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PageContentGroupToContentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
