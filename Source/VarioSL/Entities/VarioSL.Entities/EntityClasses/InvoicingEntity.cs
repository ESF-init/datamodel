﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Invoicing'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class InvoicingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static InvoicingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public InvoicingEntity() :base("InvoicingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		public InvoicingEntity(System.Int64 invoicingID):base("InvoicingEntity")
		{
			InitClassFetch(invoicingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public InvoicingEntity(System.Int64 invoicingID, IPrefetchPath prefetchPathToUse):base("InvoicingEntity")
		{
			InitClassFetch(invoicingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="validator">The custom validator object for this InvoicingEntity</param>
		public InvoicingEntity(System.Int64 invoicingID, IValidator validator):base("InvoicingEntity")
		{
			InitClassFetch(invoicingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InvoicingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((InvoicingFieldIndex)fieldIndex)
			{
				case InvoicingFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case InvoicingFieldIndex.UserID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedUserList = (_userList != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingInvoicingID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingInvoicingID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_invoices);
			toReturn.Add(_jobs);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoicingID)
		{
			return FetchUsingPK(invoicingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoicingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(invoicingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoicingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(invoicingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoicingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(invoicingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.InvoicingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new InvoicingRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("UserList", _userList);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Jobs", _jobs);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="validator">The validator object for this InvoicingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 invoicingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(invoicingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "Invoicing");

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "Invoicing");
			_clientReturnsNewIfNotFound = false;
			_userListReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountingDueDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormIds", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicesCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceTotalAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicingFilterType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicingType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPseudoInvoicing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawInvoicingResultData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SepaFilePath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueDateTime", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticInvoicingRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Invoicings", resetFKFields, new int[] { (int)InvoicingFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticInvoicingRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticInvoicingRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "Invoicings", resetFKFields, new int[] { (int)InvoicingFieldIndex.UserID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticInvoicingRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="invoicingID">PK value for Invoicing which data should be fetched into this Invoicing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 invoicingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)InvoicingFieldIndex.InvoicingID].ForcedCurrentValueWrite(invoicingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateInvoicingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new InvoicingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static InvoicingRelations Relations
		{
			get	{ return new InvoicingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.InvoicingEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.InvoicingEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.InvoicingEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.InvoicingEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountingDueDate property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."ACCOUNTINGDUEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> AccountingDueDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InvoicingFieldIndex.AccountingDueDate, false); }
			set	{ SetValue((int)InvoicingFieldIndex.AccountingDueDate, value, true); }
		}

		/// <summary> The ClientID property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)InvoicingFieldIndex.ClientID, true); }
			set	{ SetValue((int)InvoicingFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CompletionTime property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."COMPLETIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CompletionTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InvoicingFieldIndex.CompletionTime, false); }
			set	{ SetValue((int)InvoicingFieldIndex.CompletionTime, value, true); }
		}

		/// <summary> The FormIds property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."FORMIDS"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FormIds
		{
			get { return (System.String)GetValue((int)InvoicingFieldIndex.FormIds, true); }
			set	{ SetValue((int)InvoicingFieldIndex.FormIds, value, true); }
		}

		/// <summary> The InvoicesCount property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."INVOICESCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InvoicesCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)InvoicingFieldIndex.InvoicesCount, false); }
			set	{ SetValue((int)InvoicingFieldIndex.InvoicesCount, value, true); }
		}

		/// <summary> The InvoiceTotalAmount property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."INVOICETOTALAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoiceTotalAmount
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoicingFieldIndex.InvoiceTotalAmount, false); }
			set	{ SetValue((int)InvoicingFieldIndex.InvoiceTotalAmount, value, true); }
		}

		/// <summary> The InvoicingFilterType property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."INVOICINGFILTERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.InvoicingFilterType InvoicingFilterType
		{
			get { return (VarioSL.Entities.Enumerations.InvoicingFilterType)GetValue((int)InvoicingFieldIndex.InvoicingFilterType, true); }
			set	{ SetValue((int)InvoicingFieldIndex.InvoicingFilterType, value, true); }
		}

		/// <summary> The InvoicingID property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."INVOICINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 InvoicingID
		{
			get { return (System.Int64)GetValue((int)InvoicingFieldIndex.InvoicingID, true); }
			set	{ SetValue((int)InvoicingFieldIndex.InvoicingID, value, true); }
		}

		/// <summary> The InvoicingType property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."INVOICINGTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.InvoicingType InvoicingType
		{
			get { return (VarioSL.Entities.Enumerations.InvoicingType)GetValue((int)InvoicingFieldIndex.InvoicingType, true); }
			set	{ SetValue((int)InvoicingFieldIndex.InvoicingType, value, true); }
		}

		/// <summary> The IsPseudoInvoicing property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."ISPSEUDOINVOICING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPseudoInvoicing
		{
			get { return (System.Boolean)GetValue((int)InvoicingFieldIndex.IsPseudoInvoicing, true); }
			set	{ SetValue((int)InvoicingFieldIndex.IsPseudoInvoicing, value, true); }
		}

		/// <summary> The LastModified property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)InvoicingFieldIndex.LastModified, true); }
			set	{ SetValue((int)InvoicingFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)InvoicingFieldIndex.LastUser, true); }
			set	{ SetValue((int)InvoicingFieldIndex.LastUser, value, true); }
		}

		/// <summary> The RawInvoicingResultData property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."RAWINVOICINGRESULTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RawInvoicingResultData
		{
			get { return (System.String)GetValue((int)InvoicingFieldIndex.RawInvoicingResultData, true); }
			set	{ SetValue((int)InvoicingFieldIndex.RawInvoicingResultData, value, true); }
		}

		/// <summary> The RequestTime property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."REQUESTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime RequestTime
		{
			get { return (System.DateTime)GetValue((int)InvoicingFieldIndex.RequestTime, true); }
			set	{ SetValue((int)InvoicingFieldIndex.RequestTime, value, true); }
		}

		/// <summary> The SepaFilePath property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."SEPAFILEPATH"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 260<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SepaFilePath
		{
			get { return (System.String)GetValue((int)InvoicingFieldIndex.SepaFilePath, true); }
			set	{ SetValue((int)InvoicingFieldIndex.SepaFilePath, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)InvoicingFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)InvoicingFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserID property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 UserID
		{
			get { return (System.Int64)GetValue((int)InvoicingFieldIndex.UserID, true); }
			set	{ SetValue((int)InvoicingFieldIndex.UserID, value, true); }
		}

		/// <summary> The ValueDateTime property of the Entity Invoicing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICING"."VALUEDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValueDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InvoicingFieldIndex.ValueDateTime, false); }
			set	{ SetValue((int)InvoicingFieldIndex.ValueDateTime, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoicings", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoicings", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.InvoicingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
