﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PrinterToUnit'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PrinterToUnitEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private UnitEntity _unit;
		private bool	_alwaysFetchUnit, _alreadyFetchedUnit, _unitReturnsNewIfNotFound;
		private PrinterEntity _printer;
		private bool	_alwaysFetchPrinter, _alreadyFetchedPrinter, _printerReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Unit</summary>
			public static readonly string Unit = "Unit";
			/// <summary>Member name Printer</summary>
			public static readonly string Printer = "Printer";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PrinterToUnitEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PrinterToUnitEntity() :base("PrinterToUnitEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		public PrinterToUnitEntity(System.Int64 printerID, System.Int64 unitID):base("PrinterToUnitEntity")
		{
			InitClassFetch(printerID, unitID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PrinterToUnitEntity(System.Int64 printerID, System.Int64 unitID, IPrefetchPath prefetchPathToUse):base("PrinterToUnitEntity")
		{
			InitClassFetch(printerID, unitID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="validator">The custom validator object for this PrinterToUnitEntity</param>
		public PrinterToUnitEntity(System.Int64 printerID, System.Int64 unitID, IValidator validator):base("PrinterToUnitEntity")
		{
			InitClassFetch(printerID, unitID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PrinterToUnitEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_unit = (UnitEntity)info.GetValue("_unit", typeof(UnitEntity));
			if(_unit!=null)
			{
				_unit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitReturnsNewIfNotFound = info.GetBoolean("_unitReturnsNewIfNotFound");
			_alwaysFetchUnit = info.GetBoolean("_alwaysFetchUnit");
			_alreadyFetchedUnit = info.GetBoolean("_alreadyFetchedUnit");

			_printer = (PrinterEntity)info.GetValue("_printer", typeof(PrinterEntity));
			if(_printer!=null)
			{
				_printer.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printerReturnsNewIfNotFound = info.GetBoolean("_printerReturnsNewIfNotFound");
			_alwaysFetchPrinter = info.GetBoolean("_alwaysFetchPrinter");
			_alreadyFetchedPrinter = info.GetBoolean("_alreadyFetchedPrinter");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PrinterToUnitFieldIndex)fieldIndex)
			{
				case PrinterToUnitFieldIndex.PrinterID:
					DesetupSyncPrinter(true, false);
					_alreadyFetchedPrinter = false;
					break;
				case PrinterToUnitFieldIndex.UnitID:
					DesetupSyncUnit(true, false);
					_alreadyFetchedUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUnit = (_unit != null);
			_alreadyFetchedPrinter = (_printer != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Unit":
					toReturn.Add(Relations.UnitEntityUsingUnitID);
					break;
				case "Printer":
					toReturn.Add(Relations.PrinterEntityUsingPrinterID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_unit", (!this.MarkedForDeletion?_unit:null));
			info.AddValue("_unitReturnsNewIfNotFound", _unitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnit", _alwaysFetchUnit);
			info.AddValue("_alreadyFetchedUnit", _alreadyFetchedUnit);
			info.AddValue("_printer", (!this.MarkedForDeletion?_printer:null));
			info.AddValue("_printerReturnsNewIfNotFound", _printerReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrinter", _alwaysFetchPrinter);
			info.AddValue("_alreadyFetchedPrinter", _alreadyFetchedPrinter);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Unit":
					_alreadyFetchedUnit = true;
					this.Unit = (UnitEntity)entity;
					break;
				case "Printer":
					_alreadyFetchedPrinter = true;
					this.Printer = (PrinterEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Unit":
					SetupSyncUnit(relatedEntity);
					break;
				case "Printer":
					SetupSyncPrinter(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Unit":
					DesetupSyncUnit(false, true);
					break;
				case "Printer":
					DesetupSyncPrinter(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_unit!=null)
			{
				toReturn.Add(_unit);
			}
			if(_printer!=null)
			{
				toReturn.Add(_printer);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printerID, System.Int64 unitID)
		{
			return FetchUsingPK(printerID, unitID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printerID, System.Int64 unitID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(printerID, unitID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printerID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(printerID, unitID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printerID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(printerID, unitID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PrinterID, this.UnitID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PrinterToUnitRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public UnitEntity GetSingleUnit()
		{
			return GetSingleUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public virtual UnitEntity GetSingleUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnit || forceFetch || _alwaysFetchUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitEntityUsingUnitID);
				UnitEntity newEntity = new UnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitID);
				}
				if(fetchResult)
				{
					newEntity = (UnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Unit = newEntity;
				_alreadyFetchedUnit = fetchResult;
			}
			return _unit;
		}


		/// <summary> Retrieves the related entity of type 'PrinterEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PrinterEntity' which is related to this entity.</returns>
		public PrinterEntity GetSinglePrinter()
		{
			return GetSinglePrinter(false);
		}

		/// <summary> Retrieves the related entity of type 'PrinterEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PrinterEntity' which is related to this entity.</returns>
		public virtual PrinterEntity GetSinglePrinter(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrinter || forceFetch || _alwaysFetchPrinter) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PrinterEntityUsingPrinterID);
				PrinterEntity newEntity = new PrinterEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrinterID);
				}
				if(fetchResult)
				{
					newEntity = (PrinterEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printerReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Printer = newEntity;
				_alreadyFetchedPrinter = fetchResult;
			}
			return _printer;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Unit", _unit);
			toReturn.Add("Printer", _printer);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="validator">The validator object for this PrinterToUnitEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 printerID, System.Int64 unitID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(printerID, unitID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_unitReturnsNewIfNotFound = false;
			_printerReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefault", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrinterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _unit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticPrinterToUnitRelations.UnitEntityUsingUnitIDStatic, true, signalRelatedEntity, "PrinterToUnits", resetFKFields, new int[] { (int)PrinterToUnitFieldIndex.UnitID } );		
			_unit = null;
		}
		
		/// <summary> setups the sync logic for member _unit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnit(IEntityCore relatedEntity)
		{
			if(_unit!=relatedEntity)
			{		
				DesetupSyncUnit(true, true);
				_unit = (UnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticPrinterToUnitRelations.UnitEntityUsingUnitIDStatic, true, ref _alreadyFetchedUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printer</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrinter(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printer, new PropertyChangedEventHandler( OnPrinterPropertyChanged ), "Printer", VarioSL.Entities.RelationClasses.StaticPrinterToUnitRelations.PrinterEntityUsingPrinterIDStatic, true, signalRelatedEntity, "PrinterToUnits", resetFKFields, new int[] { (int)PrinterToUnitFieldIndex.PrinterID } );		
			_printer = null;
		}
		
		/// <summary> setups the sync logic for member _printer</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrinter(IEntityCore relatedEntity)
		{
			if(_printer!=relatedEntity)
			{		
				DesetupSyncPrinter(true, true);
				_printer = (PrinterEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printer, new PropertyChangedEventHandler( OnPrinterPropertyChanged ), "Printer", VarioSL.Entities.RelationClasses.StaticPrinterToUnitRelations.PrinterEntityUsingPrinterIDStatic, true, ref _alreadyFetchedPrinter, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrinterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="printerID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="unitID">PK value for PrinterToUnit which data should be fetched into this PrinterToUnit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 printerID, System.Int64 unitID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PrinterToUnitFieldIndex.PrinterID].ForcedCurrentValueWrite(printerID);
				this.Fields[(int)PrinterToUnitFieldIndex.UnitID].ForcedCurrentValueWrite(unitID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePrinterToUnitDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PrinterToUnitEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PrinterToUnitRelations Relations
		{
			get	{ return new PrinterToUnitRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("Unit")[0], (int)VarioSL.Entities.EntityType.PrinterToUnitEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "Unit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Printer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrinter
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrinterCollection(), (IEntityRelation)GetRelationsForField("Printer")[0], (int)VarioSL.Entities.EntityType.PrinterToUnitEntity, (int)VarioSL.Entities.EntityType.PrinterEntity, 0, null, null, null, "Printer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The IsDefault property of the Entity PrinterToUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRINTERTOUNIT"."ISDEFAULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsDefault
		{
			get { return (Nullable<System.Boolean>)GetValue((int)PrinterToUnitFieldIndex.IsDefault, false); }
			set	{ SetValue((int)PrinterToUnitFieldIndex.IsDefault, value, true); }
		}

		/// <summary> The PrinterID property of the Entity PrinterToUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRINTERTOUNIT"."PRINTERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PrinterID
		{
			get { return (System.Int64)GetValue((int)PrinterToUnitFieldIndex.PrinterID, true); }
			set	{ SetValue((int)PrinterToUnitFieldIndex.PrinterID, value, true); }
		}

		/// <summary> The UnitID property of the Entity PrinterToUnit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRINTERTOUNIT"."UNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 UnitID
		{
			get { return (System.Int64)GetValue((int)PrinterToUnitFieldIndex.UnitID, true); }
			set	{ SetValue((int)PrinterToUnitFieldIndex.UnitID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'UnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitEntity Unit
		{
			get	{ return GetSingleUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PrinterToUnits", "Unit", _unit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Unit. When set to true, Unit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Unit is accessed. You can always execute a forced fetch by calling GetSingleUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnit
		{
			get	{ return _alwaysFetchUnit; }
			set	{ _alwaysFetchUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Unit already has been fetched. Setting this property to false when Unit has been fetched
		/// will set Unit to null as well. Setting this property to true while Unit hasn't been fetched disables lazy loading for Unit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnit
		{
			get { return _alreadyFetchedUnit;}
			set 
			{
				if(_alreadyFetchedUnit && !value)
				{
					this.Unit = null;
				}
				_alreadyFetchedUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Unit is not found
		/// in the database. When set to true, Unit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitReturnsNewIfNotFound
		{
			get	{ return _unitReturnsNewIfNotFound; }
			set { _unitReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PrinterEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrinter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PrinterEntity Printer
		{
			get	{ return GetSinglePrinter(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrinter(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PrinterToUnits", "Printer", _printer, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Printer. When set to true, Printer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Printer is accessed. You can always execute a forced fetch by calling GetSinglePrinter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrinter
		{
			get	{ return _alwaysFetchPrinter; }
			set	{ _alwaysFetchPrinter = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Printer already has been fetched. Setting this property to false when Printer has been fetched
		/// will set Printer to null as well. Setting this property to true while Printer hasn't been fetched disables lazy loading for Printer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrinter
		{
			get { return _alreadyFetchedPrinter;}
			set 
			{
				if(_alreadyFetchedPrinter && !value)
				{
					this.Printer = null;
				}
				_alreadyFetchedPrinter = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Printer is not found
		/// in the database. When set to true, Printer will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrinterReturnsNewIfNotFound
		{
			get	{ return _printerReturnsNewIfNotFound; }
			set { _printerReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PrinterToUnitEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
