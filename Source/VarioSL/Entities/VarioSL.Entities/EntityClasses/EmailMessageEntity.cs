﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EmailMessage'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class EmailMessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EmailEventEntity _emailEvent;
		private bool	_alwaysFetchEmailEvent, _alreadyFetchedEmailEvent, _emailEventReturnsNewIfNotFound;
		private FormEntity _form;
		private bool	_alwaysFetchForm, _alreadyFetchedForm, _formReturnsNewIfNotFound;
		private MessageTypeEntity _emailMessageType;
		private bool	_alwaysFetchEmailMessageType, _alreadyFetchedEmailMessageType, _emailMessageTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EmailEvent</summary>
			public static readonly string EmailEvent = "EmailEvent";
			/// <summary>Member name Form</summary>
			public static readonly string Form = "Form";
			/// <summary>Member name EmailMessageType</summary>
			public static readonly string EmailMessageType = "EmailMessageType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EmailMessageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public EmailMessageEntity() :base("EmailMessageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		public EmailMessageEntity(System.Int64 emailEventID, System.Int64 formID):base("EmailMessageEntity")
		{
			InitClassFetch(emailEventID, formID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EmailMessageEntity(System.Int64 emailEventID, System.Int64 formID, IPrefetchPath prefetchPathToUse):base("EmailMessageEntity")
		{
			InitClassFetch(emailEventID, formID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="validator">The custom validator object for this EmailMessageEntity</param>
		public EmailMessageEntity(System.Int64 emailEventID, System.Int64 formID, IValidator validator):base("EmailMessageEntity")
		{
			InitClassFetch(emailEventID, formID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EmailMessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_emailEvent = (EmailEventEntity)info.GetValue("_emailEvent", typeof(EmailEventEntity));
			if(_emailEvent!=null)
			{
				_emailEvent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailEventReturnsNewIfNotFound = info.GetBoolean("_emailEventReturnsNewIfNotFound");
			_alwaysFetchEmailEvent = info.GetBoolean("_alwaysFetchEmailEvent");
			_alreadyFetchedEmailEvent = info.GetBoolean("_alreadyFetchedEmailEvent");

			_form = (FormEntity)info.GetValue("_form", typeof(FormEntity));
			if(_form!=null)
			{
				_form.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_formReturnsNewIfNotFound = info.GetBoolean("_formReturnsNewIfNotFound");
			_alwaysFetchForm = info.GetBoolean("_alwaysFetchForm");
			_alreadyFetchedForm = info.GetBoolean("_alreadyFetchedForm");

			_emailMessageType = (MessageTypeEntity)info.GetValue("_emailMessageType", typeof(MessageTypeEntity));
			if(_emailMessageType!=null)
			{
				_emailMessageType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailMessageTypeReturnsNewIfNotFound = info.GetBoolean("_emailMessageTypeReturnsNewIfNotFound");
			_alwaysFetchEmailMessageType = info.GetBoolean("_alwaysFetchEmailMessageType");
			_alreadyFetchedEmailMessageType = info.GetBoolean("_alreadyFetchedEmailMessageType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EmailMessageFieldIndex)fieldIndex)
			{
				case EmailMessageFieldIndex.EmailEventID:
					DesetupSyncEmailEvent(true, false);
					_alreadyFetchedEmailEvent = false;
					break;
				case EmailMessageFieldIndex.FormID:
					DesetupSyncForm(true, false);
					_alreadyFetchedForm = false;
					break;
				case EmailMessageFieldIndex.MessageType:
					DesetupSyncEmailMessageType(true, false);
					_alreadyFetchedEmailMessageType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEmailEvent = (_emailEvent != null);
			_alreadyFetchedForm = (_form != null);
			_alreadyFetchedEmailMessageType = (_emailMessageType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EmailEvent":
					toReturn.Add(Relations.EmailEventEntityUsingEmailEventID);
					break;
				case "Form":
					toReturn.Add(Relations.FormEntityUsingFormID);
					break;
				case "EmailMessageType":
					toReturn.Add(Relations.MessageTypeEntityUsingMessageType);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_emailEvent", (!this.MarkedForDeletion?_emailEvent:null));
			info.AddValue("_emailEventReturnsNewIfNotFound", _emailEventReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailEvent", _alwaysFetchEmailEvent);
			info.AddValue("_alreadyFetchedEmailEvent", _alreadyFetchedEmailEvent);
			info.AddValue("_form", (!this.MarkedForDeletion?_form:null));
			info.AddValue("_formReturnsNewIfNotFound", _formReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForm", _alwaysFetchForm);
			info.AddValue("_alreadyFetchedForm", _alreadyFetchedForm);
			info.AddValue("_emailMessageType", (!this.MarkedForDeletion?_emailMessageType:null));
			info.AddValue("_emailMessageTypeReturnsNewIfNotFound", _emailMessageTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailMessageType", _alwaysFetchEmailMessageType);
			info.AddValue("_alreadyFetchedEmailMessageType", _alreadyFetchedEmailMessageType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EmailEvent":
					_alreadyFetchedEmailEvent = true;
					this.EmailEvent = (EmailEventEntity)entity;
					break;
				case "Form":
					_alreadyFetchedForm = true;
					this.Form = (FormEntity)entity;
					break;
				case "EmailMessageType":
					_alreadyFetchedEmailMessageType = true;
					this.EmailMessageType = (MessageTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					SetupSyncEmailEvent(relatedEntity);
					break;
				case "Form":
					SetupSyncForm(relatedEntity);
					break;
				case "EmailMessageType":
					SetupSyncEmailMessageType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					DesetupSyncEmailEvent(false, true);
					break;
				case "Form":
					DesetupSyncForm(false, true);
					break;
				case "EmailMessageType":
					DesetupSyncEmailMessageType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_emailEvent!=null)
			{
				toReturn.Add(_emailEvent);
			}
			if(_form!=null)
			{
				toReturn.Add(_form);
			}
			if(_emailMessageType!=null)
			{
				toReturn.Add(_emailMessageType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int64 formID)
		{
			return FetchUsingPK(emailEventID, formID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int64 formID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(emailEventID, formID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(emailEventID, formID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(emailEventID, formID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EmailEventID, this.FormID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EmailMessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public EmailEventEntity GetSingleEmailEvent()
		{
			return GetSingleEmailEvent(false);
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public virtual EmailEventEntity GetSingleEmailEvent(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailEvent || forceFetch || _alwaysFetchEmailEvent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EmailEventEntityUsingEmailEventID);
				EmailEventEntity newEntity = new EmailEventEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmailEventID);
				}
				if(fetchResult)
				{
					newEntity = (EmailEventEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailEventReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailEvent = newEntity;
				_alreadyFetchedEmailEvent = fetchResult;
			}
			return _emailEvent;
		}


		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public FormEntity GetSingleForm()
		{
			return GetSingleForm(false);
		}

		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public virtual FormEntity GetSingleForm(bool forceFetch)
		{
			if( ( !_alreadyFetchedForm || forceFetch || _alwaysFetchForm) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FormEntityUsingFormID);
				FormEntity newEntity = new FormEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FormID);
				}
				if(fetchResult)
				{
					newEntity = (FormEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_formReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Form = newEntity;
				_alreadyFetchedForm = fetchResult;
			}
			return _form;
		}


		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public MessageTypeEntity GetSingleEmailMessageType()
		{
			return GetSingleEmailMessageType(false);
		}

		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public virtual MessageTypeEntity GetSingleEmailMessageType(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailMessageType || forceFetch || _alwaysFetchEmailMessageType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessageTypeEntityUsingMessageType);
				MessageTypeEntity newEntity = new MessageTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessageType.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MessageTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailMessageTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailMessageType = newEntity;
				_alreadyFetchedEmailMessageType = fetchResult;
			}
			return _emailMessageType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EmailEvent", _emailEvent);
			toReturn.Add("Form", _form);
			toReturn.Add("EmailMessageType", _emailMessageType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="validator">The validator object for this EmailMessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 emailEventID, System.Int64 formID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(emailEventID, formID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_emailEventReturnsNewIfNotFound = false;
			_formReturnsNewIfNotFound = false;
			_emailMessageTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Subject", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _emailEvent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailEvent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, signalRelatedEntity, "EmailMessages", resetFKFields, new int[] { (int)EmailMessageFieldIndex.EmailEventID } );		
			_emailEvent = null;
		}
		
		/// <summary> setups the sync logic for member _emailEvent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailEvent(IEntityCore relatedEntity)
		{
			if(_emailEvent!=relatedEntity)
			{		
				DesetupSyncEmailEvent(true, true);
				_emailEvent = (EmailEventEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, ref _alreadyFetchedEmailEvent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailEventPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _form</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForm(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.FormEntityUsingFormIDStatic, true, signalRelatedEntity, "EmailMessages", resetFKFields, new int[] { (int)EmailMessageFieldIndex.FormID } );		
			_form = null;
		}
		
		/// <summary> setups the sync logic for member _form</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForm(IEntityCore relatedEntity)
		{
			if(_form!=relatedEntity)
			{		
				DesetupSyncForm(true, true);
				_form = (FormEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.FormEntityUsingFormIDStatic, true, ref _alreadyFetchedForm, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _emailMessageType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailMessageType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailMessageType, new PropertyChangedEventHandler( OnEmailMessageTypePropertyChanged ), "EmailMessageType", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.MessageTypeEntityUsingMessageTypeStatic, true, signalRelatedEntity, "EmailMessages", resetFKFields, new int[] { (int)EmailMessageFieldIndex.MessageType } );		
			_emailMessageType = null;
		}
		
		/// <summary> setups the sync logic for member _emailMessageType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailMessageType(IEntityCore relatedEntity)
		{
			if(_emailMessageType!=relatedEntity)
			{		
				DesetupSyncEmailMessageType(true, true);
				_emailMessageType = (MessageTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailMessageType, new PropertyChangedEventHandler( OnEmailMessageTypePropertyChanged ), "EmailMessageType", VarioSL.Entities.RelationClasses.StaticEmailMessageRelations.MessageTypeEntityUsingMessageTypeStatic, true, ref _alreadyFetchedEmailMessageType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailMessageTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="emailEventID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="formID">PK value for EmailMessage which data should be fetched into this EmailMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 emailEventID, System.Int64 formID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EmailMessageFieldIndex.EmailEventID].ForcedCurrentValueWrite(emailEventID);
				this.Fields[(int)EmailMessageFieldIndex.FormID].ForcedCurrentValueWrite(formID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEmailMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EmailMessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EmailMessageRelations Relations
		{
			get	{ return new EmailMessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEvent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEvent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventCollection(), (IEntityRelation)GetRelationsForField("EmailEvent")[0], (int)VarioSL.Entities.EntityType.EmailMessageEntity, (int)VarioSL.Entities.EntityType.EmailEventEntity, 0, null, null, null, "EmailEvent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForm
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Form")[0], (int)VarioSL.Entities.EntityType.EmailMessageEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Form", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailMessageType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageTypeCollection(), (IEntityRelation)GetRelationsForField("EmailMessageType")[0], (int)VarioSL.Entities.EntityType.EmailMessageEntity, (int)VarioSL.Entities.EntityType.MessageTypeEntity, 0, null, null, null, "EmailMessageType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EmailEventID property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."EMAILEVENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 EmailEventID
		{
			get { return (System.Int64)GetValue((int)EmailMessageFieldIndex.EmailEventID, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.EmailEventID, value, true); }
		}

		/// <summary> The FormID property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FormID
		{
			get { return (System.Int64)GetValue((int)EmailMessageFieldIndex.FormID, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.FormID, value, true); }
		}

		/// <summary> The LastModified property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)EmailMessageFieldIndex.LastModified, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)EmailMessageFieldIndex.LastUser, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageType property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."MESSAGETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MessageType
		{
			get { return (Nullable<System.Int32>)GetValue((int)EmailMessageFieldIndex.MessageType, false); }
			set	{ SetValue((int)EmailMessageFieldIndex.MessageType, value, true); }
		}

		/// <summary> The Subject property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."SUBJECT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Subject
		{
			get { return (System.String)GetValue((int)EmailMessageFieldIndex.Subject, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.Subject, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity EmailMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EMAILMESSAGE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)EmailMessageFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)EmailMessageFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'EmailEventEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailEvent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EmailEventEntity EmailEvent
		{
			get	{ return GetSingleEmailEvent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailEvent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailMessages", "EmailEvent", _emailEvent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEvent. When set to true, EmailEvent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEvent is accessed. You can always execute a forced fetch by calling GetSingleEmailEvent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEvent
		{
			get	{ return _alwaysFetchEmailEvent; }
			set	{ _alwaysFetchEmailEvent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEvent already has been fetched. Setting this property to false when EmailEvent has been fetched
		/// will set EmailEvent to null as well. Setting this property to true while EmailEvent hasn't been fetched disables lazy loading for EmailEvent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEvent
		{
			get { return _alreadyFetchedEmailEvent;}
			set 
			{
				if(_alreadyFetchedEmailEvent && !value)
				{
					this.EmailEvent = null;
				}
				_alreadyFetchedEmailEvent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailEvent is not found
		/// in the database. When set to true, EmailEvent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EmailEventReturnsNewIfNotFound
		{
			get	{ return _emailEventReturnsNewIfNotFound; }
			set { _emailEventReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FormEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForm()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FormEntity Form
		{
			get	{ return GetSingleForm(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForm(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailMessages", "Form", _form, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Form. When set to true, Form is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Form is accessed. You can always execute a forced fetch by calling GetSingleForm(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForm
		{
			get	{ return _alwaysFetchForm; }
			set	{ _alwaysFetchForm = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Form already has been fetched. Setting this property to false when Form has been fetched
		/// will set Form to null as well. Setting this property to true while Form hasn't been fetched disables lazy loading for Form</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForm
		{
			get { return _alreadyFetchedForm;}
			set 
			{
				if(_alreadyFetchedForm && !value)
				{
					this.Form = null;
				}
				_alreadyFetchedForm = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Form is not found
		/// in the database. When set to true, Form will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FormReturnsNewIfNotFound
		{
			get	{ return _formReturnsNewIfNotFound; }
			set { _formReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessageTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailMessageType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MessageTypeEntity EmailMessageType
		{
			get	{ return GetSingleEmailMessageType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailMessageType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EmailMessages", "EmailMessageType", _emailMessageType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailMessageType. When set to true, EmailMessageType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailMessageType is accessed. You can always execute a forced fetch by calling GetSingleEmailMessageType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailMessageType
		{
			get	{ return _alwaysFetchEmailMessageType; }
			set	{ _alwaysFetchEmailMessageType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailMessageType already has been fetched. Setting this property to false when EmailMessageType has been fetched
		/// will set EmailMessageType to null as well. Setting this property to true while EmailMessageType hasn't been fetched disables lazy loading for EmailMessageType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailMessageType
		{
			get { return _alreadyFetchedEmailMessageType;}
			set 
			{
				if(_alreadyFetchedEmailMessageType && !value)
				{
					this.EmailMessageType = null;
				}
				_alreadyFetchedEmailMessageType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailMessageType is not found
		/// in the database. When set to true, EmailMessageType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EmailMessageTypeReturnsNewIfNotFound
		{
			get	{ return _emailMessageTypeReturnsNewIfNotFound; }
			set { _emailMessageTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.EmailMessageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
