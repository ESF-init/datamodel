﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AttributeToMobilityProvider'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AttributeToMobilityProviderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CustomAttributeEntity _customAttribute;
		private bool	_alwaysFetchCustomAttribute, _alreadyFetchedCustomAttribute, _customAttributeReturnsNewIfNotFound;
		private MobilityProviderEntity _mobilityProvider;
		private bool	_alwaysFetchMobilityProvider, _alreadyFetchedMobilityProvider, _mobilityProviderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomAttribute</summary>
			public static readonly string CustomAttribute = "CustomAttribute";
			/// <summary>Member name MobilityProvider</summary>
			public static readonly string MobilityProvider = "MobilityProvider";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AttributeToMobilityProviderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AttributeToMobilityProviderEntity() :base("AttributeToMobilityProviderEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		public AttributeToMobilityProviderEntity(System.Int64 attributeID, System.Int64 mobilityProviderID):base("AttributeToMobilityProviderEntity")
		{
			InitClassFetch(attributeID, mobilityProviderID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttributeToMobilityProviderEntity(System.Int64 attributeID, System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse):base("AttributeToMobilityProviderEntity")
		{
			InitClassFetch(attributeID, mobilityProviderID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="validator">The custom validator object for this AttributeToMobilityProviderEntity</param>
		public AttributeToMobilityProviderEntity(System.Int64 attributeID, System.Int64 mobilityProviderID, IValidator validator):base("AttributeToMobilityProviderEntity")
		{
			InitClassFetch(attributeID, mobilityProviderID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttributeToMobilityProviderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customAttribute = (CustomAttributeEntity)info.GetValue("_customAttribute", typeof(CustomAttributeEntity));
			if(_customAttribute!=null)
			{
				_customAttribute.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customAttributeReturnsNewIfNotFound = info.GetBoolean("_customAttributeReturnsNewIfNotFound");
			_alwaysFetchCustomAttribute = info.GetBoolean("_alwaysFetchCustomAttribute");
			_alreadyFetchedCustomAttribute = info.GetBoolean("_alreadyFetchedCustomAttribute");

			_mobilityProvider = (MobilityProviderEntity)info.GetValue("_mobilityProvider", typeof(MobilityProviderEntity));
			if(_mobilityProvider!=null)
			{
				_mobilityProvider.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mobilityProviderReturnsNewIfNotFound = info.GetBoolean("_mobilityProviderReturnsNewIfNotFound");
			_alwaysFetchMobilityProvider = info.GetBoolean("_alwaysFetchMobilityProvider");
			_alreadyFetchedMobilityProvider = info.GetBoolean("_alreadyFetchedMobilityProvider");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AttributeToMobilityProviderFieldIndex)fieldIndex)
			{
				case AttributeToMobilityProviderFieldIndex.AttributeID:
					DesetupSyncCustomAttribute(true, false);
					_alreadyFetchedCustomAttribute = false;
					break;
				case AttributeToMobilityProviderFieldIndex.MobilityProviderID:
					DesetupSyncMobilityProvider(true, false);
					_alreadyFetchedMobilityProvider = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomAttribute = (_customAttribute != null);
			_alreadyFetchedMobilityProvider = (_mobilityProvider != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomAttribute":
					toReturn.Add(Relations.CustomAttributeEntityUsingAttributeID);
					break;
				case "MobilityProvider":
					toReturn.Add(Relations.MobilityProviderEntityUsingMobilityProviderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customAttribute", (!this.MarkedForDeletion?_customAttribute:null));
			info.AddValue("_customAttributeReturnsNewIfNotFound", _customAttributeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomAttribute", _alwaysFetchCustomAttribute);
			info.AddValue("_alreadyFetchedCustomAttribute", _alreadyFetchedCustomAttribute);
			info.AddValue("_mobilityProvider", (!this.MarkedForDeletion?_mobilityProvider:null));
			info.AddValue("_mobilityProviderReturnsNewIfNotFound", _mobilityProviderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMobilityProvider", _alwaysFetchMobilityProvider);
			info.AddValue("_alreadyFetchedMobilityProvider", _alreadyFetchedMobilityProvider);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomAttribute":
					_alreadyFetchedCustomAttribute = true;
					this.CustomAttribute = (CustomAttributeEntity)entity;
					break;
				case "MobilityProvider":
					_alreadyFetchedMobilityProvider = true;
					this.MobilityProvider = (MobilityProviderEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomAttribute":
					SetupSyncCustomAttribute(relatedEntity);
					break;
				case "MobilityProvider":
					SetupSyncMobilityProvider(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomAttribute":
					DesetupSyncCustomAttribute(false, true);
					break;
				case "MobilityProvider":
					DesetupSyncMobilityProvider(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customAttribute!=null)
			{
				toReturn.Add(_customAttribute);
			}
			if(_mobilityProvider!=null)
			{
				toReturn.Add(_mobilityProvider);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, System.Int64 mobilityProviderID)
		{
			return FetchUsingPK(attributeID, mobilityProviderID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(attributeID, mobilityProviderID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(attributeID, mobilityProviderID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeID, System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(attributeID, mobilityProviderID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AttributeID, this.MobilityProviderID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AttributeToMobilityProviderRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomAttributeEntity' which is related to this entity.</returns>
		public CustomAttributeEntity GetSingleCustomAttribute()
		{
			return GetSingleCustomAttribute(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomAttributeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomAttributeEntity' which is related to this entity.</returns>
		public virtual CustomAttributeEntity GetSingleCustomAttribute(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomAttribute || forceFetch || _alwaysFetchCustomAttribute) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomAttributeEntityUsingAttributeID);
				CustomAttributeEntity newEntity = new CustomAttributeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeID);
				}
				if(fetchResult)
				{
					newEntity = (CustomAttributeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customAttributeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomAttribute = newEntity;
				_alreadyFetchedCustomAttribute = fetchResult;
			}
			return _customAttribute;
		}


		/// <summary> Retrieves the related entity of type 'MobilityProviderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MobilityProviderEntity' which is related to this entity.</returns>
		public MobilityProviderEntity GetSingleMobilityProvider()
		{
			return GetSingleMobilityProvider(false);
		}

		/// <summary> Retrieves the related entity of type 'MobilityProviderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MobilityProviderEntity' which is related to this entity.</returns>
		public virtual MobilityProviderEntity GetSingleMobilityProvider(bool forceFetch)
		{
			if( ( !_alreadyFetchedMobilityProvider || forceFetch || _alwaysFetchMobilityProvider) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MobilityProviderEntityUsingMobilityProviderID);
				MobilityProviderEntity newEntity = new MobilityProviderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MobilityProviderID);
				}
				if(fetchResult)
				{
					newEntity = (MobilityProviderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mobilityProviderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MobilityProvider = newEntity;
				_alreadyFetchedMobilityProvider = fetchResult;
			}
			return _mobilityProvider;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomAttribute", _customAttribute);
			toReturn.Add("MobilityProvider", _mobilityProvider);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="validator">The validator object for this AttributeToMobilityProviderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 attributeID, System.Int64 mobilityProviderID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(attributeID, mobilityProviderID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_customAttributeReturnsNewIfNotFound = false;
			_mobilityProviderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobilityProviderID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customAttribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customAttribute, new PropertyChangedEventHandler( OnCustomAttributePropertyChanged ), "CustomAttribute", VarioSL.Entities.RelationClasses.StaticAttributeToMobilityProviderRelations.CustomAttributeEntityUsingAttributeIDStatic, true, signalRelatedEntity, "AttributeToMobilityProviders", resetFKFields, new int[] { (int)AttributeToMobilityProviderFieldIndex.AttributeID } );		
			_customAttribute = null;
		}
		
		/// <summary> setups the sync logic for member _customAttribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomAttribute(IEntityCore relatedEntity)
		{
			if(_customAttribute!=relatedEntity)
			{		
				DesetupSyncCustomAttribute(true, true);
				_customAttribute = (CustomAttributeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customAttribute, new PropertyChangedEventHandler( OnCustomAttributePropertyChanged ), "CustomAttribute", VarioSL.Entities.RelationClasses.StaticAttributeToMobilityProviderRelations.CustomAttributeEntityUsingAttributeIDStatic, true, ref _alreadyFetchedCustomAttribute, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mobilityProvider</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMobilityProvider(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mobilityProvider, new PropertyChangedEventHandler( OnMobilityProviderPropertyChanged ), "MobilityProvider", VarioSL.Entities.RelationClasses.StaticAttributeToMobilityProviderRelations.MobilityProviderEntityUsingMobilityProviderIDStatic, true, signalRelatedEntity, "AttributeToMobilityProviders", resetFKFields, new int[] { (int)AttributeToMobilityProviderFieldIndex.MobilityProviderID } );		
			_mobilityProvider = null;
		}
		
		/// <summary> setups the sync logic for member _mobilityProvider</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMobilityProvider(IEntityCore relatedEntity)
		{
			if(_mobilityProvider!=relatedEntity)
			{		
				DesetupSyncMobilityProvider(true, true);
				_mobilityProvider = (MobilityProviderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mobilityProvider, new PropertyChangedEventHandler( OnMobilityProviderPropertyChanged ), "MobilityProvider", VarioSL.Entities.RelationClasses.StaticAttributeToMobilityProviderRelations.MobilityProviderEntityUsingMobilityProviderIDStatic, true, ref _alreadyFetchedMobilityProvider, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMobilityProviderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="attributeID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="mobilityProviderID">PK value for AttributeToMobilityProvider which data should be fetched into this AttributeToMobilityProvider object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 attributeID, System.Int64 mobilityProviderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AttributeToMobilityProviderFieldIndex.AttributeID].ForcedCurrentValueWrite(attributeID);
				this.Fields[(int)AttributeToMobilityProviderFieldIndex.MobilityProviderID].ForcedCurrentValueWrite(mobilityProviderID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAttributeToMobilityProviderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AttributeToMobilityProviderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AttributeToMobilityProviderRelations Relations
		{
			get	{ return new AttributeToMobilityProviderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttribute'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttribute
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeCollection(), (IEntityRelation)GetRelationsForField("CustomAttribute")[0], (int)VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity, (int)VarioSL.Entities.EntityType.CustomAttributeEntity, 0, null, null, null, "CustomAttribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MobilityProvider'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMobilityProvider
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MobilityProviderCollection(), (IEntityRelation)GetRelationsForField("MobilityProvider")[0], (int)VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity, (int)VarioSL.Entities.EntityType.MobilityProviderEntity, 0, null, null, null, "MobilityProvider", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeID property of the Entity AttributeToMobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTETOMOBILITYPROVIDER"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 AttributeID
		{
			get { return (System.Int64)GetValue((int)AttributeToMobilityProviderFieldIndex.AttributeID, true); }
			set	{ SetValue((int)AttributeToMobilityProviderFieldIndex.AttributeID, value, true); }
		}

		/// <summary> The MobilityProviderID property of the Entity AttributeToMobilityProvider<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTETOMOBILITYPROVIDER"."MOBILITYPROVIDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 MobilityProviderID
		{
			get { return (System.Int64)GetValue((int)AttributeToMobilityProviderFieldIndex.MobilityProviderID, true); }
			set	{ SetValue((int)AttributeToMobilityProviderFieldIndex.MobilityProviderID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CustomAttributeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomAttribute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomAttributeEntity CustomAttribute
		{
			get	{ return GetSingleCustomAttribute(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomAttribute(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeToMobilityProviders", "CustomAttribute", _customAttribute, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttribute. When set to true, CustomAttribute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttribute is accessed. You can always execute a forced fetch by calling GetSingleCustomAttribute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttribute
		{
			get	{ return _alwaysFetchCustomAttribute; }
			set	{ _alwaysFetchCustomAttribute = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttribute already has been fetched. Setting this property to false when CustomAttribute has been fetched
		/// will set CustomAttribute to null as well. Setting this property to true while CustomAttribute hasn't been fetched disables lazy loading for CustomAttribute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttribute
		{
			get { return _alreadyFetchedCustomAttribute;}
			set 
			{
				if(_alreadyFetchedCustomAttribute && !value)
				{
					this.CustomAttribute = null;
				}
				_alreadyFetchedCustomAttribute = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomAttribute is not found
		/// in the database. When set to true, CustomAttribute will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomAttributeReturnsNewIfNotFound
		{
			get	{ return _customAttributeReturnsNewIfNotFound; }
			set { _customAttributeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MobilityProviderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMobilityProvider()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MobilityProviderEntity MobilityProvider
		{
			get	{ return GetSingleMobilityProvider(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMobilityProvider(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeToMobilityProviders", "MobilityProvider", _mobilityProvider, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MobilityProvider. When set to true, MobilityProvider is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MobilityProvider is accessed. You can always execute a forced fetch by calling GetSingleMobilityProvider(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMobilityProvider
		{
			get	{ return _alwaysFetchMobilityProvider; }
			set	{ _alwaysFetchMobilityProvider = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MobilityProvider already has been fetched. Setting this property to false when MobilityProvider has been fetched
		/// will set MobilityProvider to null as well. Setting this property to true while MobilityProvider hasn't been fetched disables lazy loading for MobilityProvider</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMobilityProvider
		{
			get { return _alreadyFetchedMobilityProvider;}
			set 
			{
				if(_alreadyFetchedMobilityProvider && !value)
				{
					this.MobilityProvider = null;
				}
				_alreadyFetchedMobilityProvider = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MobilityProvider is not found
		/// in the database. When set to true, MobilityProvider will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MobilityProviderReturnsNewIfNotFound
		{
			get	{ return _mobilityProviderReturnsNewIfNotFound; }
			set { _mobilityProviderReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
