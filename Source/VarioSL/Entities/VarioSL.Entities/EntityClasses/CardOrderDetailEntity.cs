﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardOrderDetail'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardOrderDetailEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private PendingOrderEntity _pendingOrder;
		private bool	_alwaysFetchPendingOrder, _alreadyFetchedPendingOrder, _pendingOrderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PendingOrder</summary>
			public static readonly string PendingOrder = "PendingOrder";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardOrderDetailEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardOrderDetailEntity() :base("CardOrderDetailEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		public CardOrderDetailEntity(System.Int64 cardOrderDetailID):base("CardOrderDetailEntity")
		{
			InitClassFetch(cardOrderDetailID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardOrderDetailEntity(System.Int64 cardOrderDetailID, IPrefetchPath prefetchPathToUse):base("CardOrderDetailEntity")
		{
			InitClassFetch(cardOrderDetailID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="validator">The custom validator object for this CardOrderDetailEntity</param>
		public CardOrderDetailEntity(System.Int64 cardOrderDetailID, IValidator validator):base("CardOrderDetailEntity")
		{
			InitClassFetch(cardOrderDetailID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardOrderDetailEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pendingOrder = (PendingOrderEntity)info.GetValue("_pendingOrder", typeof(PendingOrderEntity));
			if(_pendingOrder!=null)
			{
				_pendingOrder.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pendingOrderReturnsNewIfNotFound = info.GetBoolean("_pendingOrderReturnsNewIfNotFound");
			_alwaysFetchPendingOrder = info.GetBoolean("_alwaysFetchPendingOrder");
			_alreadyFetchedPendingOrder = info.GetBoolean("_alreadyFetchedPendingOrder");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardOrderDetailFieldIndex)fieldIndex)
			{
				case CardOrderDetailFieldIndex.PendingOrderID:
					DesetupSyncPendingOrder(true, false);
					_alreadyFetchedPendingOrder = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPendingOrder = (_pendingOrder != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PendingOrder":
					toReturn.Add(Relations.PendingOrderEntityUsingPendingOrderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pendingOrder", (!this.MarkedForDeletion?_pendingOrder:null));
			info.AddValue("_pendingOrderReturnsNewIfNotFound", _pendingOrderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPendingOrder", _alwaysFetchPendingOrder);
			info.AddValue("_alreadyFetchedPendingOrder", _alreadyFetchedPendingOrder);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PendingOrder":
					_alreadyFetchedPendingOrder = true;
					this.PendingOrder = (PendingOrderEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PendingOrder":
					SetupSyncPendingOrder(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PendingOrder":
					DesetupSyncPendingOrder(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_pendingOrder!=null)
			{
				toReturn.Add(_pendingOrder);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardOrderDetailID)
		{
			return FetchUsingPK(cardOrderDetailID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardOrderDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardOrderDetailID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardOrderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardOrderDetailID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardOrderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardOrderDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardOrderDetailID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardOrderDetailRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PendingOrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PendingOrderEntity' which is related to this entity.</returns>
		public PendingOrderEntity GetSinglePendingOrder()
		{
			return GetSinglePendingOrder(false);
		}

		/// <summary> Retrieves the related entity of type 'PendingOrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PendingOrderEntity' which is related to this entity.</returns>
		public virtual PendingOrderEntity GetSinglePendingOrder(bool forceFetch)
		{
			if( ( !_alreadyFetchedPendingOrder || forceFetch || _alwaysFetchPendingOrder) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PendingOrderEntityUsingPendingOrderID);
				PendingOrderEntity newEntity = new PendingOrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PendingOrderID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PendingOrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pendingOrderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PendingOrder = newEntity;
				_alreadyFetchedPendingOrder = fetchResult;
			}
			return _pendingOrder;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PendingOrder", _pendingOrder);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="validator">The validator object for this CardOrderDetailEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardOrderDetailID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardOrderDetailID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_pendingOrderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardHolderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardOrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PendingOrderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplacedCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _pendingOrder</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPendingOrder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pendingOrder, new PropertyChangedEventHandler( OnPendingOrderPropertyChanged ), "PendingOrder", VarioSL.Entities.RelationClasses.StaticCardOrderDetailRelations.PendingOrderEntityUsingPendingOrderIDStatic, true, signalRelatedEntity, "CardOrderDetails", resetFKFields, new int[] { (int)CardOrderDetailFieldIndex.PendingOrderID } );		
			_pendingOrder = null;
		}
		
		/// <summary> setups the sync logic for member _pendingOrder</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPendingOrder(IEntityCore relatedEntity)
		{
			if(_pendingOrder!=relatedEntity)
			{		
				DesetupSyncPendingOrder(true, true);
				_pendingOrder = (PendingOrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pendingOrder, new PropertyChangedEventHandler( OnPendingOrderPropertyChanged ), "PendingOrder", VarioSL.Entities.RelationClasses.StaticCardOrderDetailRelations.PendingOrderEntityUsingPendingOrderIDStatic, true, ref _alreadyFetchedPendingOrder, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPendingOrderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardOrderDetailID">PK value for CardOrderDetail which data should be fetched into this CardOrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardOrderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardOrderDetailFieldIndex.CardOrderDetailID].ForcedCurrentValueWrite(cardOrderDetailID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardOrderDetailDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardOrderDetailEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardOrderDetailRelations Relations
		{
			get	{ return new CardOrderDetailRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PendingOrder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPendingOrder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PendingOrderCollection(), (IEntityRelation)GetRelationsForField("PendingOrder")[0], (int)VarioSL.Entities.EntityType.CardOrderDetailEntity, (int)VarioSL.Entities.EntityType.PendingOrderEntity, 0, null, null, null, "PendingOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardHolderID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."CARDHOLDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardHolderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardOrderDetailFieldIndex.CardHolderID, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.CardHolderID, value, true); }
		}

		/// <summary> The CardOrderDetailID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."CARDORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardOrderDetailID
		{
			get { return (System.Int64)GetValue((int)CardOrderDetailFieldIndex.CardOrderDetailID, true); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.CardOrderDetailID, value, true); }
		}

		/// <summary> The ClientID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardOrderDetailFieldIndex.ClientID, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardOrderDetailFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The LastModified property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModified
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardOrderDetailFieldIndex.LastModified, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CardOrderDetailFieldIndex.LastUser, true); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrderDetailID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."ORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrderDetailID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardOrderDetailFieldIndex.OrderDetailID, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.OrderDetailID, value, true); }
		}

		/// <summary> The PendingOrderID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."PENDINGORDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PendingOrderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardOrderDetailFieldIndex.PendingOrderID, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.PendingOrderID, value, true); }
		}

		/// <summary> The ReplacedCardID property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."REPLACEDCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReplacedCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardOrderDetailFieldIndex.ReplacedCardID, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.ReplacedCardID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CardOrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDORDERDETAIL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> TransactionCounter
		{
			get { return (Nullable<System.Decimal>)GetValue((int)CardOrderDetailFieldIndex.TransactionCounter, false); }
			set	{ SetValue((int)CardOrderDetailFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PendingOrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePendingOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PendingOrderEntity PendingOrder
		{
			get	{ return GetSinglePendingOrder(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPendingOrder(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardOrderDetails", "PendingOrder", _pendingOrder, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PendingOrder. When set to true, PendingOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PendingOrder is accessed. You can always execute a forced fetch by calling GetSinglePendingOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPendingOrder
		{
			get	{ return _alwaysFetchPendingOrder; }
			set	{ _alwaysFetchPendingOrder = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PendingOrder already has been fetched. Setting this property to false when PendingOrder has been fetched
		/// will set PendingOrder to null as well. Setting this property to true while PendingOrder hasn't been fetched disables lazy loading for PendingOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPendingOrder
		{
			get { return _alreadyFetchedPendingOrder;}
			set 
			{
				if(_alreadyFetchedPendingOrder && !value)
				{
					this.PendingOrder = null;
				}
				_alreadyFetchedPendingOrder = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PendingOrder is not found
		/// in the database. When set to true, PendingOrder will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PendingOrderReturnsNewIfNotFound
		{
			get	{ return _pendingOrderReturnsNewIfNotFound; }
			set { _pendingOrderReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardOrderDetailEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
