﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceReaderJob'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceReaderJobEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DeviceReaderEntity _targetDeviceReader;
		private bool	_alwaysFetchTargetDeviceReader, _alreadyFetchedTargetDeviceReader, _targetDeviceReaderReturnsNewIfNotFound;
		private DeviceReaderEntity _workerDeviceReader;
		private bool	_alwaysFetchWorkerDeviceReader, _alreadyFetchedWorkerDeviceReader, _workerDeviceReaderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TargetDeviceReader</summary>
			public static readonly string TargetDeviceReader = "TargetDeviceReader";
			/// <summary>Member name WorkerDeviceReader</summary>
			public static readonly string WorkerDeviceReader = "WorkerDeviceReader";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceReaderJobEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceReaderJobEntity() :base("DeviceReaderJobEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		public DeviceReaderJobEntity(System.Int64 deviceReaderJobID):base("DeviceReaderJobEntity")
		{
			InitClassFetch(deviceReaderJobID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceReaderJobEntity(System.Int64 deviceReaderJobID, IPrefetchPath prefetchPathToUse):base("DeviceReaderJobEntity")
		{
			InitClassFetch(deviceReaderJobID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="validator">The custom validator object for this DeviceReaderJobEntity</param>
		public DeviceReaderJobEntity(System.Int64 deviceReaderJobID, IValidator validator):base("DeviceReaderJobEntity")
		{
			InitClassFetch(deviceReaderJobID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceReaderJobEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_targetDeviceReader = (DeviceReaderEntity)info.GetValue("_targetDeviceReader", typeof(DeviceReaderEntity));
			if(_targetDeviceReader!=null)
			{
				_targetDeviceReader.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_targetDeviceReaderReturnsNewIfNotFound = info.GetBoolean("_targetDeviceReaderReturnsNewIfNotFound");
			_alwaysFetchTargetDeviceReader = info.GetBoolean("_alwaysFetchTargetDeviceReader");
			_alreadyFetchedTargetDeviceReader = info.GetBoolean("_alreadyFetchedTargetDeviceReader");

			_workerDeviceReader = (DeviceReaderEntity)info.GetValue("_workerDeviceReader", typeof(DeviceReaderEntity));
			if(_workerDeviceReader!=null)
			{
				_workerDeviceReader.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_workerDeviceReaderReturnsNewIfNotFound = info.GetBoolean("_workerDeviceReaderReturnsNewIfNotFound");
			_alwaysFetchWorkerDeviceReader = info.GetBoolean("_alwaysFetchWorkerDeviceReader");
			_alreadyFetchedWorkerDeviceReader = info.GetBoolean("_alreadyFetchedWorkerDeviceReader");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeviceReaderJobFieldIndex)fieldIndex)
			{
				case DeviceReaderJobFieldIndex.TargetReaderID:
					DesetupSyncTargetDeviceReader(true, false);
					_alreadyFetchedTargetDeviceReader = false;
					break;
				case DeviceReaderJobFieldIndex.WorkerReaderID:
					DesetupSyncWorkerDeviceReader(true, false);
					_alreadyFetchedWorkerDeviceReader = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTargetDeviceReader = (_targetDeviceReader != null);
			_alreadyFetchedWorkerDeviceReader = (_workerDeviceReader != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TargetDeviceReader":
					toReturn.Add(Relations.DeviceReaderEntityUsingTargetReaderID);
					break;
				case "WorkerDeviceReader":
					toReturn.Add(Relations.DeviceReaderEntityUsingWorkerReaderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_targetDeviceReader", (!this.MarkedForDeletion?_targetDeviceReader:null));
			info.AddValue("_targetDeviceReaderReturnsNewIfNotFound", _targetDeviceReaderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTargetDeviceReader", _alwaysFetchTargetDeviceReader);
			info.AddValue("_alreadyFetchedTargetDeviceReader", _alreadyFetchedTargetDeviceReader);
			info.AddValue("_workerDeviceReader", (!this.MarkedForDeletion?_workerDeviceReader:null));
			info.AddValue("_workerDeviceReaderReturnsNewIfNotFound", _workerDeviceReaderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWorkerDeviceReader", _alwaysFetchWorkerDeviceReader);
			info.AddValue("_alreadyFetchedWorkerDeviceReader", _alreadyFetchedWorkerDeviceReader);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TargetDeviceReader":
					_alreadyFetchedTargetDeviceReader = true;
					this.TargetDeviceReader = (DeviceReaderEntity)entity;
					break;
				case "WorkerDeviceReader":
					_alreadyFetchedWorkerDeviceReader = true;
					this.WorkerDeviceReader = (DeviceReaderEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TargetDeviceReader":
					SetupSyncTargetDeviceReader(relatedEntity);
					break;
				case "WorkerDeviceReader":
					SetupSyncWorkerDeviceReader(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TargetDeviceReader":
					DesetupSyncTargetDeviceReader(false, true);
					break;
				case "WorkerDeviceReader":
					DesetupSyncWorkerDeviceReader(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_targetDeviceReader!=null)
			{
				toReturn.Add(_targetDeviceReader);
			}
			if(_workerDeviceReader!=null)
			{
				toReturn.Add(_workerDeviceReader);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderJobID)
		{
			return FetchUsingPK(deviceReaderJobID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderJobID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceReaderJobID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderJobID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceReaderJobID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderJobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceReaderJobID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceReaderJobID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceReaderJobRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public DeviceReaderEntity GetSingleTargetDeviceReader()
		{
			return GetSingleTargetDeviceReader(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public virtual DeviceReaderEntity GetSingleTargetDeviceReader(bool forceFetch)
		{
			if( ( !_alreadyFetchedTargetDeviceReader || forceFetch || _alwaysFetchTargetDeviceReader) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceReaderEntityUsingTargetReaderID);
				DeviceReaderEntity newEntity = new DeviceReaderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TargetReaderID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceReaderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_targetDeviceReaderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TargetDeviceReader = newEntity;
				_alreadyFetchedTargetDeviceReader = fetchResult;
			}
			return _targetDeviceReader;
		}


		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public DeviceReaderEntity GetSingleWorkerDeviceReader()
		{
			return GetSingleWorkerDeviceReader(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceReaderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceReaderEntity' which is related to this entity.</returns>
		public virtual DeviceReaderEntity GetSingleWorkerDeviceReader(bool forceFetch)
		{
			if( ( !_alreadyFetchedWorkerDeviceReader || forceFetch || _alwaysFetchWorkerDeviceReader) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceReaderEntityUsingWorkerReaderID);
				DeviceReaderEntity newEntity = new DeviceReaderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.WorkerReaderID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceReaderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_workerDeviceReaderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WorkerDeviceReader = newEntity;
				_alreadyFetchedWorkerDeviceReader = fetchResult;
			}
			return _workerDeviceReader;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TargetDeviceReader", _targetDeviceReader);
			toReturn.Add("WorkerDeviceReader", _workerDeviceReader);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="validator">The validator object for this DeviceReaderJobEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceReaderJobID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceReaderJobID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_targetDeviceReaderReturnsNewIfNotFound = false;
			_workerDeviceReaderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderJobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceRole", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobParameter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobPriority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TargetReaderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Timeout", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkerReaderID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _targetDeviceReader</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTargetDeviceReader(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _targetDeviceReader, new PropertyChangedEventHandler( OnTargetDeviceReaderPropertyChanged ), "TargetDeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderJobRelations.DeviceReaderEntityUsingTargetReaderIDStatic, true, signalRelatedEntity, "TargetDeviceReaderJobs", resetFKFields, new int[] { (int)DeviceReaderJobFieldIndex.TargetReaderID } );		
			_targetDeviceReader = null;
		}
		
		/// <summary> setups the sync logic for member _targetDeviceReader</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTargetDeviceReader(IEntityCore relatedEntity)
		{
			if(_targetDeviceReader!=relatedEntity)
			{		
				DesetupSyncTargetDeviceReader(true, true);
				_targetDeviceReader = (DeviceReaderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _targetDeviceReader, new PropertyChangedEventHandler( OnTargetDeviceReaderPropertyChanged ), "TargetDeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderJobRelations.DeviceReaderEntityUsingTargetReaderIDStatic, true, ref _alreadyFetchedTargetDeviceReader, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTargetDeviceReaderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _workerDeviceReader</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWorkerDeviceReader(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _workerDeviceReader, new PropertyChangedEventHandler( OnWorkerDeviceReaderPropertyChanged ), "WorkerDeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderJobRelations.DeviceReaderEntityUsingWorkerReaderIDStatic, true, signalRelatedEntity, "WorkerDeviceReaderJobs", resetFKFields, new int[] { (int)DeviceReaderJobFieldIndex.WorkerReaderID } );		
			_workerDeviceReader = null;
		}
		
		/// <summary> setups the sync logic for member _workerDeviceReader</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWorkerDeviceReader(IEntityCore relatedEntity)
		{
			if(_workerDeviceReader!=relatedEntity)
			{		
				DesetupSyncWorkerDeviceReader(true, true);
				_workerDeviceReader = (DeviceReaderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _workerDeviceReader, new PropertyChangedEventHandler( OnWorkerDeviceReaderPropertyChanged ), "WorkerDeviceReader", VarioSL.Entities.RelationClasses.StaticDeviceReaderJobRelations.DeviceReaderEntityUsingWorkerReaderIDStatic, true, ref _alreadyFetchedWorkerDeviceReader, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWorkerDeviceReaderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceReaderJobID">PK value for DeviceReaderJob which data should be fetched into this DeviceReaderJob object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceReaderJobID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceReaderJobFieldIndex.DeviceReaderJobID].ForcedCurrentValueWrite(deviceReaderJobID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceReaderJobDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceReaderJobEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceReaderJobRelations Relations
		{
			get	{ return new DeviceReaderJobRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReader'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetDeviceReader
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderCollection(), (IEntityRelation)GetRelationsForField("TargetDeviceReader")[0], (int)VarioSL.Entities.EntityType.DeviceReaderJobEntity, (int)VarioSL.Entities.EntityType.DeviceReaderEntity, 0, null, null, null, "TargetDeviceReader", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReader'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkerDeviceReader
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderCollection(), (IEntityRelation)GetRelationsForField("WorkerDeviceReader")[0], (int)VarioSL.Entities.EntityType.DeviceReaderJobEntity, (int)VarioSL.Entities.EntityType.DeviceReaderEntity, 0, null, null, null, "WorkerDeviceReader", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceReaderJobID property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."DEVICEREADERJOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceReaderJobID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderJobFieldIndex.DeviceReaderJobID, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.DeviceReaderJobID, value, true); }
		}

		/// <summary> The DeviceRole property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."DEVICEROLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceRole
		{
			get { return (System.Int32)GetValue((int)DeviceReaderJobFieldIndex.DeviceRole, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.DeviceRole, value, true); }
		}

		/// <summary> The JobParameter property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."JOBPARAMETER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JobParameter
		{
			get { return (System.String)GetValue((int)DeviceReaderJobFieldIndex.JobParameter, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.JobParameter, value, true); }
		}

		/// <summary> The JobPriority property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."JOBPRIORITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 JobPriority
		{
			get { return (System.Int32)GetValue((int)DeviceReaderJobFieldIndex.JobPriority, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.JobPriority, value, true); }
		}

		/// <summary> The JobStatus property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."JOBSTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 JobStatus
		{
			get { return (System.Int32)GetValue((int)DeviceReaderJobFieldIndex.JobStatus, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.JobStatus, value, true); }
		}

		/// <summary> The JobType property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."JOBTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 JobType
		{
			get { return (System.Int32)GetValue((int)DeviceReaderJobFieldIndex.JobType, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.JobType, value, true); }
		}

		/// <summary> The LastModified property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderJobFieldIndex.LastModified, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DeviceReaderJobFieldIndex.LastUser, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TargetReaderID property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."TARGETREADERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TargetReaderID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderJobFieldIndex.TargetReaderID, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.TargetReaderID, value, true); }
		}

		/// <summary> The Timeout property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."TIMEOUT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Timeout
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderJobFieldIndex.Timeout, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.Timeout, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DeviceReaderJobFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderJobFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The WorkerReaderID property of the Entity DeviceReaderJob<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADERJOB"."WORKERREADERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> WorkerReaderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceReaderJobFieldIndex.WorkerReaderID, false); }
			set	{ SetValue((int)DeviceReaderJobFieldIndex.WorkerReaderID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DeviceReaderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTargetDeviceReader()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceReaderEntity TargetDeviceReader
		{
			get	{ return GetSingleTargetDeviceReader(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTargetDeviceReader(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TargetDeviceReaderJobs", "TargetDeviceReader", _targetDeviceReader, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TargetDeviceReader. When set to true, TargetDeviceReader is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetDeviceReader is accessed. You can always execute a forced fetch by calling GetSingleTargetDeviceReader(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetDeviceReader
		{
			get	{ return _alwaysFetchTargetDeviceReader; }
			set	{ _alwaysFetchTargetDeviceReader = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TargetDeviceReader already has been fetched. Setting this property to false when TargetDeviceReader has been fetched
		/// will set TargetDeviceReader to null as well. Setting this property to true while TargetDeviceReader hasn't been fetched disables lazy loading for TargetDeviceReader</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetDeviceReader
		{
			get { return _alreadyFetchedTargetDeviceReader;}
			set 
			{
				if(_alreadyFetchedTargetDeviceReader && !value)
				{
					this.TargetDeviceReader = null;
				}
				_alreadyFetchedTargetDeviceReader = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TargetDeviceReader is not found
		/// in the database. When set to true, TargetDeviceReader will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TargetDeviceReaderReturnsNewIfNotFound
		{
			get	{ return _targetDeviceReaderReturnsNewIfNotFound; }
			set { _targetDeviceReaderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceReaderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWorkerDeviceReader()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceReaderEntity WorkerDeviceReader
		{
			get	{ return GetSingleWorkerDeviceReader(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWorkerDeviceReader(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WorkerDeviceReaderJobs", "WorkerDeviceReader", _workerDeviceReader, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WorkerDeviceReader. When set to true, WorkerDeviceReader is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkerDeviceReader is accessed. You can always execute a forced fetch by calling GetSingleWorkerDeviceReader(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkerDeviceReader
		{
			get	{ return _alwaysFetchWorkerDeviceReader; }
			set	{ _alwaysFetchWorkerDeviceReader = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkerDeviceReader already has been fetched. Setting this property to false when WorkerDeviceReader has been fetched
		/// will set WorkerDeviceReader to null as well. Setting this property to true while WorkerDeviceReader hasn't been fetched disables lazy loading for WorkerDeviceReader</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkerDeviceReader
		{
			get { return _alreadyFetchedWorkerDeviceReader;}
			set 
			{
				if(_alreadyFetchedWorkerDeviceReader && !value)
				{
					this.WorkerDeviceReader = null;
				}
				_alreadyFetchedWorkerDeviceReader = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WorkerDeviceReader is not found
		/// in the database. When set to true, WorkerDeviceReader will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool WorkerDeviceReaderReturnsNewIfNotFound
		{
			get	{ return _workerDeviceReaderReturnsNewIfNotFound; }
			set { _workerDeviceReaderReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceReaderJobEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
