﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ServerResponseTimeDataAggregation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ServerResponseTimeDataAggregationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ServerResponseTimeDataAggregationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ServerResponseTimeDataAggregationEntity() :base("ServerResponseTimeDataAggregationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		public ServerResponseTimeDataAggregationEntity(System.Int64 aggregationID):base("ServerResponseTimeDataAggregationEntity")
		{
			InitClassFetch(aggregationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ServerResponseTimeDataAggregationEntity(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse):base("ServerResponseTimeDataAggregationEntity")
		{
			InitClassFetch(aggregationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="validator">The custom validator object for this ServerResponseTimeDataAggregationEntity</param>
		public ServerResponseTimeDataAggregationEntity(System.Int64 aggregationID, IValidator validator):base("ServerResponseTimeDataAggregationEntity")
		{
			InitClassFetch(aggregationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServerResponseTimeDataAggregationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID)
		{
			return FetchUsingPK(aggregationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(aggregationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(aggregationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(aggregationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AggregationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ServerResponseTimeDataAggregationRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="validator">The validator object for this ServerResponseTimeDataAggregationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 aggregationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(aggregationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AggregationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Hostname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IndicatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberAboveThreshold", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfAuthorizedTx", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PerformanceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Quantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TotalNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="aggregationID">PK value for ServerResponseTimeDataAggregation which data should be fetched into this ServerResponseTimeDataAggregation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 aggregationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ServerResponseTimeDataAggregationFieldIndex.AggregationID].ForcedCurrentValueWrite(aggregationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateServerResponseTimeDataAggregationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ServerResponseTimeDataAggregationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ServerResponseTimeDataAggregationRelations Relations
		{
			get	{ return new ServerResponseTimeDataAggregationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AggregationFrom property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."AGGREGATIONFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationFrom
		{
			get { return (System.DateTime)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationFrom, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationFrom, value, true); }
		}

		/// <summary> The AggregationID property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."AGGREGATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AggregationID
		{
			get { return (System.Int64)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationID, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationID, value, true); }
		}

		/// <summary> The AggregationTo property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."AGGREGATIONTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AggregationTo
		{
			get { return (System.DateTime)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationTo, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationTo, value, true); }
		}

		/// <summary> The AggregationType property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."AGGREGATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.AggregationType AggregationType
		{
			get { return (VarioSL.Entities.Enumerations.AggregationType)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationType, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.AggregationType, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."DEVICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The Duration property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."DURATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 12, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> Duration
		{
			get { return (Nullable<System.Double>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.Duration, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.Duration, value, true); }
		}

		/// <summary> The EventTime property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."EVENTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EventTime
		{
			get { return (System.DateTime)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.EventTime, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.EventTime, value, true); }
		}

		/// <summary> The Hostname property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."HOSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Hostname
		{
			get { return (System.String)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.Hostname, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.Hostname, value, true); }
		}

		/// <summary> The IndicatorID property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."INDICATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 IndicatorID
		{
			get { return (System.Int64)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.IndicatorID, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.IndicatorID, value, true); }
		}

		/// <summary> The NumberAboveThreshold property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."NUMBERABOVETHRESHOLD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NumberAboveThreshold
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.NumberAboveThreshold, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.NumberAboveThreshold, value, true); }
		}

		/// <summary> The NumberOfAuthorizedTx property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."NUMBEROFAUTHORIZEDTX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> NumberOfAuthorizedTx
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.NumberOfAuthorizedTx, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.NumberOfAuthorizedTx, value, true); }
		}

		/// <summary> The PerformanceID property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."PERFORMANCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PerformanceID
		{
			get { return (System.Int64)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.PerformanceID, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.PerformanceID, value, true); }
		}

		/// <summary> The Quantity property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."QUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Quantity
		{
			get { return (Nullable<System.Int32>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.Quantity, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.Quantity, value, true); }
		}

		/// <summary> The SalesChannelID property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."SALESCHANNELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesChannelID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.SalesChannelID, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.SalesChannelID, value, true); }
		}

		/// <summary> The ShiftBegin property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."SHIFTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftBegin
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.ShiftBegin, false); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.ShiftBegin, value, true); }
		}

		/// <summary> The TotalNumber property of the Entity ServerResponseTimeDataAggregation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_SRTDATAAGGREGATION"."TOTALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TotalNumber
		{
			get { return (System.Int32)GetValue((int)ServerResponseTimeDataAggregationFieldIndex.TotalNumber, true); }
			set	{ SetValue((int)ServerResponseTimeDataAggregationFieldIndex.TotalNumber, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ServerResponseTimeDataAggregationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
