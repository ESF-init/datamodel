﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DunningLevelConfiguration'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DunningLevelConfigurationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection	_previousDunningLevel;
		private bool	_alwaysFetchPreviousDunningLevel, _alreadyFetchedPreviousDunningLevel;
		private VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection	_dunningLevelPostingKeys;
		private bool	_alwaysFetchDunningLevelPostingKeys, _alreadyFetchedDunningLevelPostingKeys;
		private VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection	_dunningToInvoices;
		private bool	_alwaysFetchDunningToInvoices, _alreadyFetchedDunningToInvoices;
		private DunningLevelConfigurationEntity _nextDunningLevel;
		private bool	_alwaysFetchNextDunningLevel, _alreadyFetchedNextDunningLevel, _nextDunningLevelReturnsNewIfNotFound;
		private FormEntity _form;
		private bool	_alwaysFetchForm, _alreadyFetchedForm, _formReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name NextDunningLevel</summary>
			public static readonly string NextDunningLevel = "NextDunningLevel";
			/// <summary>Member name Form</summary>
			public static readonly string Form = "Form";
			/// <summary>Member name PreviousDunningLevel</summary>
			public static readonly string PreviousDunningLevel = "PreviousDunningLevel";
			/// <summary>Member name DunningLevelPostingKeys</summary>
			public static readonly string DunningLevelPostingKeys = "DunningLevelPostingKeys";
			/// <summary>Member name DunningToInvoices</summary>
			public static readonly string DunningToInvoices = "DunningToInvoices";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DunningLevelConfigurationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DunningLevelConfigurationEntity() :base("DunningLevelConfigurationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		public DunningLevelConfigurationEntity(System.Int64 dunningLevelID):base("DunningLevelConfigurationEntity")
		{
			InitClassFetch(dunningLevelID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DunningLevelConfigurationEntity(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse):base("DunningLevelConfigurationEntity")
		{
			InitClassFetch(dunningLevelID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="validator">The custom validator object for this DunningLevelConfigurationEntity</param>
		public DunningLevelConfigurationEntity(System.Int64 dunningLevelID, IValidator validator):base("DunningLevelConfigurationEntity")
		{
			InitClassFetch(dunningLevelID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DunningLevelConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_previousDunningLevel = (VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection)info.GetValue("_previousDunningLevel", typeof(VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection));
			_alwaysFetchPreviousDunningLevel = info.GetBoolean("_alwaysFetchPreviousDunningLevel");
			_alreadyFetchedPreviousDunningLevel = info.GetBoolean("_alreadyFetchedPreviousDunningLevel");

			_dunningLevelPostingKeys = (VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection)info.GetValue("_dunningLevelPostingKeys", typeof(VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection));
			_alwaysFetchDunningLevelPostingKeys = info.GetBoolean("_alwaysFetchDunningLevelPostingKeys");
			_alreadyFetchedDunningLevelPostingKeys = info.GetBoolean("_alreadyFetchedDunningLevelPostingKeys");

			_dunningToInvoices = (VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection)info.GetValue("_dunningToInvoices", typeof(VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection));
			_alwaysFetchDunningToInvoices = info.GetBoolean("_alwaysFetchDunningToInvoices");
			_alreadyFetchedDunningToInvoices = info.GetBoolean("_alreadyFetchedDunningToInvoices");
			_nextDunningLevel = (DunningLevelConfigurationEntity)info.GetValue("_nextDunningLevel", typeof(DunningLevelConfigurationEntity));
			if(_nextDunningLevel!=null)
			{
				_nextDunningLevel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_nextDunningLevelReturnsNewIfNotFound = info.GetBoolean("_nextDunningLevelReturnsNewIfNotFound");
			_alwaysFetchNextDunningLevel = info.GetBoolean("_alwaysFetchNextDunningLevel");
			_alreadyFetchedNextDunningLevel = info.GetBoolean("_alreadyFetchedNextDunningLevel");

			_form = (FormEntity)info.GetValue("_form", typeof(FormEntity));
			if(_form!=null)
			{
				_form.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_formReturnsNewIfNotFound = info.GetBoolean("_formReturnsNewIfNotFound");
			_alwaysFetchForm = info.GetBoolean("_alwaysFetchForm");
			_alreadyFetchedForm = info.GetBoolean("_alreadyFetchedForm");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DunningLevelConfigurationFieldIndex)fieldIndex)
			{
				case DunningLevelConfigurationFieldIndex.FormID:
					DesetupSyncForm(true, false);
					_alreadyFetchedForm = false;
					break;
				case DunningLevelConfigurationFieldIndex.NextLevelID:
					DesetupSyncNextDunningLevel(true, false);
					_alreadyFetchedNextDunningLevel = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPreviousDunningLevel = (_previousDunningLevel.Count > 0);
			_alreadyFetchedDunningLevelPostingKeys = (_dunningLevelPostingKeys.Count > 0);
			_alreadyFetchedDunningToInvoices = (_dunningToInvoices.Count > 0);
			_alreadyFetchedNextDunningLevel = (_nextDunningLevel != null);
			_alreadyFetchedForm = (_form != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "NextDunningLevel":
					toReturn.Add(Relations.DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelID);
					break;
				case "Form":
					toReturn.Add(Relations.FormEntityUsingFormID);
					break;
				case "PreviousDunningLevel":
					toReturn.Add(Relations.DunningLevelConfigurationEntityUsingNextLevelID);
					break;
				case "DunningLevelPostingKeys":
					toReturn.Add(Relations.DunningLevelPostingKeyEntityUsingDunningLevelID);
					break;
				case "DunningToInvoices":
					toReturn.Add(Relations.DunningToInvoiceEntityUsingDunningLevelID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_previousDunningLevel", (!this.MarkedForDeletion?_previousDunningLevel:null));
			info.AddValue("_alwaysFetchPreviousDunningLevel", _alwaysFetchPreviousDunningLevel);
			info.AddValue("_alreadyFetchedPreviousDunningLevel", _alreadyFetchedPreviousDunningLevel);
			info.AddValue("_dunningLevelPostingKeys", (!this.MarkedForDeletion?_dunningLevelPostingKeys:null));
			info.AddValue("_alwaysFetchDunningLevelPostingKeys", _alwaysFetchDunningLevelPostingKeys);
			info.AddValue("_alreadyFetchedDunningLevelPostingKeys", _alreadyFetchedDunningLevelPostingKeys);
			info.AddValue("_dunningToInvoices", (!this.MarkedForDeletion?_dunningToInvoices:null));
			info.AddValue("_alwaysFetchDunningToInvoices", _alwaysFetchDunningToInvoices);
			info.AddValue("_alreadyFetchedDunningToInvoices", _alreadyFetchedDunningToInvoices);
			info.AddValue("_nextDunningLevel", (!this.MarkedForDeletion?_nextDunningLevel:null));
			info.AddValue("_nextDunningLevelReturnsNewIfNotFound", _nextDunningLevelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNextDunningLevel", _alwaysFetchNextDunningLevel);
			info.AddValue("_alreadyFetchedNextDunningLevel", _alreadyFetchedNextDunningLevel);
			info.AddValue("_form", (!this.MarkedForDeletion?_form:null));
			info.AddValue("_formReturnsNewIfNotFound", _formReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForm", _alwaysFetchForm);
			info.AddValue("_alreadyFetchedForm", _alreadyFetchedForm);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "NextDunningLevel":
					_alreadyFetchedNextDunningLevel = true;
					this.NextDunningLevel = (DunningLevelConfigurationEntity)entity;
					break;
				case "Form":
					_alreadyFetchedForm = true;
					this.Form = (FormEntity)entity;
					break;
				case "PreviousDunningLevel":
					_alreadyFetchedPreviousDunningLevel = true;
					if(entity!=null)
					{
						this.PreviousDunningLevel.Add((DunningLevelConfigurationEntity)entity);
					}
					break;
				case "DunningLevelPostingKeys":
					_alreadyFetchedDunningLevelPostingKeys = true;
					if(entity!=null)
					{
						this.DunningLevelPostingKeys.Add((DunningLevelPostingKeyEntity)entity);
					}
					break;
				case "DunningToInvoices":
					_alreadyFetchedDunningToInvoices = true;
					if(entity!=null)
					{
						this.DunningToInvoices.Add((DunningToInvoiceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "NextDunningLevel":
					SetupSyncNextDunningLevel(relatedEntity);
					break;
				case "Form":
					SetupSyncForm(relatedEntity);
					break;
				case "PreviousDunningLevel":
					_previousDunningLevel.Add((DunningLevelConfigurationEntity)relatedEntity);
					break;
				case "DunningLevelPostingKeys":
					_dunningLevelPostingKeys.Add((DunningLevelPostingKeyEntity)relatedEntity);
					break;
				case "DunningToInvoices":
					_dunningToInvoices.Add((DunningToInvoiceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "NextDunningLevel":
					DesetupSyncNextDunningLevel(false, true);
					break;
				case "Form":
					DesetupSyncForm(false, true);
					break;
				case "PreviousDunningLevel":
					this.PerformRelatedEntityRemoval(_previousDunningLevel, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningLevelPostingKeys":
					this.PerformRelatedEntityRemoval(_dunningLevelPostingKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningToInvoices":
					this.PerformRelatedEntityRemoval(_dunningToInvoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_nextDunningLevel!=null)
			{
				toReturn.Add(_nextDunningLevel);
			}
			if(_form!=null)
			{
				toReturn.Add(_form);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_previousDunningLevel);
			toReturn.Add(_dunningLevelPostingKeys);
			toReturn.Add(_dunningToInvoices);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID)
		{
			return FetchUsingPK(dunningLevelID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(dunningLevelID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(dunningLevelID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(dunningLevelID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DunningLevelID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DunningLevelConfigurationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelConfigurationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiPreviousDunningLevel(bool forceFetch)
		{
			return GetMultiPreviousDunningLevel(forceFetch, _previousDunningLevel.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelConfigurationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiPreviousDunningLevel(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPreviousDunningLevel(forceFetch, _previousDunningLevel.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiPreviousDunningLevel(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPreviousDunningLevel(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection GetMultiPreviousDunningLevel(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPreviousDunningLevel || forceFetch || _alwaysFetchPreviousDunningLevel) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_previousDunningLevel);
				_previousDunningLevel.SuppressClearInGetMulti=!forceFetch;
				_previousDunningLevel.EntityFactoryToUse = entityFactoryToUse;
				_previousDunningLevel.GetMultiManyToOne(this, null, filter);
				_previousDunningLevel.SuppressClearInGetMulti=false;
				_alreadyFetchedPreviousDunningLevel = true;
			}
			return _previousDunningLevel;
		}

		/// <summary> Sets the collection parameters for the collection for 'PreviousDunningLevel'. These settings will be taken into account
		/// when the property PreviousDunningLevel is requested or GetMultiPreviousDunningLevel is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPreviousDunningLevel(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_previousDunningLevel.SortClauses=sortClauses;
			_previousDunningLevel.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelPostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, _dunningLevelPostingKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelPostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, _dunningLevelPostingKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningLevelPostingKeys || forceFetch || _alwaysFetchDunningLevelPostingKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningLevelPostingKeys);
				_dunningLevelPostingKeys.SuppressClearInGetMulti=!forceFetch;
				_dunningLevelPostingKeys.EntityFactoryToUse = entityFactoryToUse;
				_dunningLevelPostingKeys.GetMultiManyToOne(this, null, filter);
				_dunningLevelPostingKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningLevelPostingKeys = true;
			}
			return _dunningLevelPostingKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningLevelPostingKeys'. These settings will be taken into account
		/// when the property DunningLevelPostingKeys is requested or GetMultiDunningLevelPostingKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningLevelPostingKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningLevelPostingKeys.SortClauses=sortClauses;
			_dunningLevelPostingKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToInvoices || forceFetch || _alwaysFetchDunningToInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToInvoices);
				_dunningToInvoices.SuppressClearInGetMulti=!forceFetch;
				_dunningToInvoices.EntityFactoryToUse = entityFactoryToUse;
				_dunningToInvoices.GetMultiManyToOne(this, null, null, filter);
				_dunningToInvoices.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToInvoices = true;
			}
			return _dunningToInvoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToInvoices'. These settings will be taken into account
		/// when the property DunningToInvoices is requested or GetMultiDunningToInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToInvoices.SortClauses=sortClauses;
			_dunningToInvoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public DunningLevelConfigurationEntity GetSingleNextDunningLevel()
		{
			return GetSingleNextDunningLevel(false);
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public virtual DunningLevelConfigurationEntity GetSingleNextDunningLevel(bool forceFetch)
		{
			if( ( !_alreadyFetchedNextDunningLevel || forceFetch || _alwaysFetchNextDunningLevel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelID);
				DunningLevelConfigurationEntity newEntity = new DunningLevelConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NextLevelID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DunningLevelConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_nextDunningLevelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NextDunningLevel = newEntity;
				_alreadyFetchedNextDunningLevel = fetchResult;
			}
			return _nextDunningLevel;
		}


		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public FormEntity GetSingleForm()
		{
			return GetSingleForm(false);
		}

		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public virtual FormEntity GetSingleForm(bool forceFetch)
		{
			if( ( !_alreadyFetchedForm || forceFetch || _alwaysFetchForm) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FormEntityUsingFormID);
				FormEntity newEntity = new FormEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FormID);
				}
				if(fetchResult)
				{
					newEntity = (FormEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_formReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Form = newEntity;
				_alreadyFetchedForm = fetchResult;
			}
			return _form;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("NextDunningLevel", _nextDunningLevel);
			toReturn.Add("Form", _form);
			toReturn.Add("PreviousDunningLevel", _previousDunningLevel);
			toReturn.Add("DunningLevelPostingKeys", _dunningLevelPostingKeys);
			toReturn.Add("DunningToInvoices", _dunningToInvoices);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="validator">The validator object for this DunningLevelConfigurationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 dunningLevelID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(dunningLevelID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_previousDunningLevel = new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection();
			_previousDunningLevel.SetContainingEntityInfo(this, "NextDunningLevel");

			_dunningLevelPostingKeys = new VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection();
			_dunningLevelPostingKeys.SetContainingEntityInfo(this, "DunningLevel");

			_dunningToInvoices = new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection();
			_dunningToInvoices.SetContainingEntityInfo(this, "DunningLevelConfiguration");
			_nextDunningLevelReturnsNewIfNotFound = false;
			_formReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Action", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdditionalToleranceDays", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GraceDays", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsFirstLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NextLevelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _nextDunningLevel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNextDunningLevel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _nextDunningLevel, new PropertyChangedEventHandler( OnNextDunningLevelPropertyChanged ), "NextDunningLevel", VarioSL.Entities.RelationClasses.StaticDunningLevelConfigurationRelations.DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelIDStatic, true, signalRelatedEntity, "PreviousDunningLevel", resetFKFields, new int[] { (int)DunningLevelConfigurationFieldIndex.NextLevelID } );		
			_nextDunningLevel = null;
		}
		
		/// <summary> setups the sync logic for member _nextDunningLevel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNextDunningLevel(IEntityCore relatedEntity)
		{
			if(_nextDunningLevel!=relatedEntity)
			{		
				DesetupSyncNextDunningLevel(true, true);
				_nextDunningLevel = (DunningLevelConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _nextDunningLevel, new PropertyChangedEventHandler( OnNextDunningLevelPropertyChanged ), "NextDunningLevel", VarioSL.Entities.RelationClasses.StaticDunningLevelConfigurationRelations.DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelIDStatic, true, ref _alreadyFetchedNextDunningLevel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNextDunningLevelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _form</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForm(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticDunningLevelConfigurationRelations.FormEntityUsingFormIDStatic, true, signalRelatedEntity, "DunningLevels", resetFKFields, new int[] { (int)DunningLevelConfigurationFieldIndex.FormID } );		
			_form = null;
		}
		
		/// <summary> setups the sync logic for member _form</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForm(IEntityCore relatedEntity)
		{
			if(_form!=relatedEntity)
			{		
				DesetupSyncForm(true, true);
				_form = (FormEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticDunningLevelConfigurationRelations.FormEntityUsingFormIDStatic, true, ref _alreadyFetchedForm, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="dunningLevelID">PK value for DunningLevelConfiguration which data should be fetched into this DunningLevelConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 dunningLevelID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DunningLevelConfigurationFieldIndex.DunningLevelID].ForcedCurrentValueWrite(dunningLevelID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDunningLevelConfigurationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DunningLevelConfigurationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DunningLevelConfigurationRelations Relations
		{
			get	{ return new DunningLevelConfigurationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPreviousDunningLevel
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection(), (IEntityRelation)GetRelationsForField("PreviousDunningLevel")[0], (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, 0, null, null, null, "PreviousDunningLevel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelPostingKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningLevelPostingKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection(), (IEntityRelation)GetRelationsForField("DunningLevelPostingKeys")[0], (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, (int)VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity, 0, null, null, null, "DunningLevelPostingKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToInvoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection(), (IEntityRelation)GetRelationsForField("DunningToInvoices")[0], (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, 0, null, null, null, "DunningToInvoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNextDunningLevel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection(), (IEntityRelation)GetRelationsForField("NextDunningLevel")[0], (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, 0, null, null, null, "NextDunningLevel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForm
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Form")[0], (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Form", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Action property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."ACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.DunningAction Action
		{
			get { return (VarioSL.Entities.Enumerations.DunningAction)GetValue((int)DunningLevelConfigurationFieldIndex.Action, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.Action, value, true); }
		}

		/// <summary> The AdditionalToleranceDays property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."ADDITIONALTOLERANCEDAYS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AdditionalToleranceDays
		{
			get { return (System.Int32)GetValue((int)DunningLevelConfigurationFieldIndex.AdditionalToleranceDays, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.AdditionalToleranceDays, value, true); }
		}

		/// <summary> The ClientID property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)DunningLevelConfigurationFieldIndex.ClientID, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DunningLevelID property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."DUNNINGLEVELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DunningLevelID
		{
			get { return (System.Int64)GetValue((int)DunningLevelConfigurationFieldIndex.DunningLevelID, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.DunningLevelID, value, true); }
		}

		/// <summary> The FormID property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FormID
		{
			get { return (System.Int64)GetValue((int)DunningLevelConfigurationFieldIndex.FormID, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.FormID, value, true); }
		}

		/// <summary> The GraceDays property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."GRACEDAYS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GraceDays
		{
			get { return (System.Int32)GetValue((int)DunningLevelConfigurationFieldIndex.GraceDays, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.GraceDays, value, true); }
		}

		/// <summary> The IsFirstLevel property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."ISFIRSTLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsFirstLevel
		{
			get { return (System.Boolean)GetValue((int)DunningLevelConfigurationFieldIndex.IsFirstLevel, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.IsFirstLevel, value, true); }
		}

		/// <summary> The LastModified property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DunningLevelConfigurationFieldIndex.LastModified, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DunningLevelConfigurationFieldIndex.LastUser, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DunningLevelConfigurationFieldIndex.Name, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.Name, value, true); }
		}

		/// <summary> The NextLevelID property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."NEXTLEVELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NextLevelID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DunningLevelConfigurationFieldIndex.NextLevelID, false); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.NextLevelID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DunningLevelConfiguration<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVEL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DunningLevelConfigurationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DunningLevelConfigurationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPreviousDunningLevel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection PreviousDunningLevel
		{
			get	{ return GetMultiPreviousDunningLevel(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PreviousDunningLevel. When set to true, PreviousDunningLevel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PreviousDunningLevel is accessed. You can always execute/ a forced fetch by calling GetMultiPreviousDunningLevel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPreviousDunningLevel
		{
			get	{ return _alwaysFetchPreviousDunningLevel; }
			set	{ _alwaysFetchPreviousDunningLevel = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PreviousDunningLevel already has been fetched. Setting this property to false when PreviousDunningLevel has been fetched
		/// will clear the PreviousDunningLevel collection well. Setting this property to true while PreviousDunningLevel hasn't been fetched disables lazy loading for PreviousDunningLevel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPreviousDunningLevel
		{
			get { return _alreadyFetchedPreviousDunningLevel;}
			set 
			{
				if(_alreadyFetchedPreviousDunningLevel && !value && (_previousDunningLevel != null))
				{
					_previousDunningLevel.Clear();
				}
				_alreadyFetchedPreviousDunningLevel = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningLevelPostingKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection DunningLevelPostingKeys
		{
			get	{ return GetMultiDunningLevelPostingKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningLevelPostingKeys. When set to true, DunningLevelPostingKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningLevelPostingKeys is accessed. You can always execute/ a forced fetch by calling GetMultiDunningLevelPostingKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningLevelPostingKeys
		{
			get	{ return _alwaysFetchDunningLevelPostingKeys; }
			set	{ _alwaysFetchDunningLevelPostingKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningLevelPostingKeys already has been fetched. Setting this property to false when DunningLevelPostingKeys has been fetched
		/// will clear the DunningLevelPostingKeys collection well. Setting this property to true while DunningLevelPostingKeys hasn't been fetched disables lazy loading for DunningLevelPostingKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningLevelPostingKeys
		{
			get { return _alreadyFetchedDunningLevelPostingKeys;}
			set 
			{
				if(_alreadyFetchedDunningLevelPostingKeys && !value && (_dunningLevelPostingKeys != null))
				{
					_dunningLevelPostingKeys.Clear();
				}
				_alreadyFetchedDunningLevelPostingKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection DunningToInvoices
		{
			get	{ return GetMultiDunningToInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToInvoices. When set to true, DunningToInvoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToInvoices is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToInvoices
		{
			get	{ return _alwaysFetchDunningToInvoices; }
			set	{ _alwaysFetchDunningToInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToInvoices already has been fetched. Setting this property to false when DunningToInvoices has been fetched
		/// will clear the DunningToInvoices collection well. Setting this property to true while DunningToInvoices hasn't been fetched disables lazy loading for DunningToInvoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToInvoices
		{
			get { return _alreadyFetchedDunningToInvoices;}
			set 
			{
				if(_alreadyFetchedDunningToInvoices && !value && (_dunningToInvoices != null))
				{
					_dunningToInvoices.Clear();
				}
				_alreadyFetchedDunningToInvoices = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DunningLevelConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNextDunningLevel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DunningLevelConfigurationEntity NextDunningLevel
		{
			get	{ return GetSingleNextDunningLevel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNextDunningLevel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PreviousDunningLevel", "NextDunningLevel", _nextDunningLevel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NextDunningLevel. When set to true, NextDunningLevel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NextDunningLevel is accessed. You can always execute a forced fetch by calling GetSingleNextDunningLevel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNextDunningLevel
		{
			get	{ return _alwaysFetchNextDunningLevel; }
			set	{ _alwaysFetchNextDunningLevel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NextDunningLevel already has been fetched. Setting this property to false when NextDunningLevel has been fetched
		/// will set NextDunningLevel to null as well. Setting this property to true while NextDunningLevel hasn't been fetched disables lazy loading for NextDunningLevel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNextDunningLevel
		{
			get { return _alreadyFetchedNextDunningLevel;}
			set 
			{
				if(_alreadyFetchedNextDunningLevel && !value)
				{
					this.NextDunningLevel = null;
				}
				_alreadyFetchedNextDunningLevel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NextDunningLevel is not found
		/// in the database. When set to true, NextDunningLevel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NextDunningLevelReturnsNewIfNotFound
		{
			get	{ return _nextDunningLevelReturnsNewIfNotFound; }
			set { _nextDunningLevelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FormEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForm()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FormEntity Form
		{
			get	{ return GetSingleForm(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForm(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningLevels", "Form", _form, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Form. When set to true, Form is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Form is accessed. You can always execute a forced fetch by calling GetSingleForm(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForm
		{
			get	{ return _alwaysFetchForm; }
			set	{ _alwaysFetchForm = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Form already has been fetched. Setting this property to false when Form has been fetched
		/// will set Form to null as well. Setting this property to true while Form hasn't been fetched disables lazy loading for Form</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForm
		{
			get { return _alreadyFetchedForm;}
			set 
			{
				if(_alreadyFetchedForm && !value)
				{
					this.Form = null;
				}
				_alreadyFetchedForm = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Form is not found
		/// in the database. When set to true, Form will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FormReturnsNewIfNotFound
		{
			get	{ return _formReturnsNewIfNotFound; }
			set { _formReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
