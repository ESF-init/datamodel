﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrganizationType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrganizationTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.OrganizationCollection	_organizations;
		private bool	_alwaysFetchOrganizations, _alreadyFetchedOrganizations;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Organizations</summary>
			public static readonly string Organizations = "Organizations";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrganizationTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrganizationTypeEntity() :base("OrganizationTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		public OrganizationTypeEntity(System.Int64 organizationTypeID):base("OrganizationTypeEntity")
		{
			InitClassFetch(organizationTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrganizationTypeEntity(System.Int64 organizationTypeID, IPrefetchPath prefetchPathToUse):base("OrganizationTypeEntity")
		{
			InitClassFetch(organizationTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="validator">The custom validator object for this OrganizationTypeEntity</param>
		public OrganizationTypeEntity(System.Int64 organizationTypeID, IValidator validator):base("OrganizationTypeEntity")
		{
			InitClassFetch(organizationTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrganizationTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_organizations = (VarioSL.Entities.CollectionClasses.OrganizationCollection)info.GetValue("_organizations", typeof(VarioSL.Entities.CollectionClasses.OrganizationCollection));
			_alwaysFetchOrganizations = info.GetBoolean("_alwaysFetchOrganizations");
			_alreadyFetchedOrganizations = info.GetBoolean("_alreadyFetchedOrganizations");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrganizations = (_organizations.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Organizations":
					toReturn.Add(Relations.OrganizationEntityUsingOrganizationTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_organizations", (!this.MarkedForDeletion?_organizations:null));
			info.AddValue("_alwaysFetchOrganizations", _alwaysFetchOrganizations);
			info.AddValue("_alreadyFetchedOrganizations", _alreadyFetchedOrganizations);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Organizations":
					_alreadyFetchedOrganizations = true;
					if(entity!=null)
					{
						this.Organizations.Add((OrganizationEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Organizations":
					_organizations.Add((OrganizationEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Organizations":
					this.PerformRelatedEntityRemoval(_organizations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_organizations);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationTypeID)
		{
			return FetchUsingPK(organizationTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(organizationTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(organizationTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(organizationTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrganizationTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrganizationTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch)
		{
			return GetMultiOrganizations(forceFetch, _organizations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizations(forceFetch, _organizations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationCollection GetMultiOrganizations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizations || forceFetch || _alwaysFetchOrganizations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizations);
				_organizations.SuppressClearInGetMulti=!forceFetch;
				_organizations.EntityFactoryToUse = entityFactoryToUse;
				_organizations.GetMultiManyToOne(null, this, filter);
				_organizations.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizations = true;
			}
			return _organizations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Organizations'. These settings will be taken into account
		/// when the property Organizations is requested or GetMultiOrganizations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizations.SortClauses=sortClauses;
			_organizations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Organizations", _organizations);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="validator">The validator object for this OrganizationTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 organizationTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(organizationTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_organizations = new VarioSL.Entities.CollectionClasses.OrganizationCollection();
			_organizations.SetContainingEntityInfo(this, "OrganizationType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Literal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="organizationTypeID">PK value for OrganizationType which data should be fetched into this OrganizationType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 organizationTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrganizationTypeFieldIndex.OrganizationTypeID].ForcedCurrentValueWrite(organizationTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrganizationTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrganizationTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrganizationTypeRelations Relations
		{
			get	{ return new OrganizationTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organizations")[0], (int)VarioSL.Entities.EntityType.OrganizationTypeEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organizations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)OrganizationTypeFieldIndex.Description, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)OrganizationTypeFieldIndex.LastModified, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)OrganizationTypeFieldIndex.LastUser, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Literal property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."LITERAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Literal
		{
			get { return (System.String)GetValue((int)OrganizationTypeFieldIndex.Literal, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.Literal, value, true); }
		}

		/// <summary> The OrganizationTypeID property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."ORGANIZATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrganizationTypeID
		{
			get { return (System.Int64)GetValue((int)OrganizationTypeFieldIndex.OrganizationTypeID, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.OrganizationTypeID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity OrganizationType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONTYPE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)OrganizationTypeFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)OrganizationTypeFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrganizationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationCollection Organizations
		{
			get	{ return GetMultiOrganizations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Organizations. When set to true, Organizations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organizations is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizations
		{
			get	{ return _alwaysFetchOrganizations; }
			set	{ _alwaysFetchOrganizations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organizations already has been fetched. Setting this property to false when Organizations has been fetched
		/// will clear the Organizations collection well. Setting this property to true while Organizations hasn't been fetched disables lazy loading for Organizations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizations
		{
			get { return _alreadyFetchedOrganizations;}
			set 
			{
				if(_alreadyFetchedOrganizations && !value && (_organizations != null))
				{
					_organizations.Clear();
				}
				_alreadyFetchedOrganizations = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrganizationTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
