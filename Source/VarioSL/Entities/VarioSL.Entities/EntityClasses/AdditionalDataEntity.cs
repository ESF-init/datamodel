﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AdditionalData'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AdditionalDataEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private PerformanceEntity _performance;
		private bool	_alwaysFetchPerformance, _alreadyFetchedPerformance, _performanceReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Performance</summary>
			public static readonly string Performance = "Performance";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AdditionalDataEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AdditionalDataEntity() :base("AdditionalDataEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		public AdditionalDataEntity(System.Int64 additionalDataID):base("AdditionalDataEntity")
		{
			InitClassFetch(additionalDataID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AdditionalDataEntity(System.Int64 additionalDataID, IPrefetchPath prefetchPathToUse):base("AdditionalDataEntity")
		{
			InitClassFetch(additionalDataID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="validator">The custom validator object for this AdditionalDataEntity</param>
		public AdditionalDataEntity(System.Int64 additionalDataID, IValidator validator):base("AdditionalDataEntity")
		{
			InitClassFetch(additionalDataID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdditionalDataEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_performance = (PerformanceEntity)info.GetValue("_performance", typeof(PerformanceEntity));
			if(_performance!=null)
			{
				_performance.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_performanceReturnsNewIfNotFound = info.GetBoolean("_performanceReturnsNewIfNotFound");
			_alwaysFetchPerformance = info.GetBoolean("_alwaysFetchPerformance");
			_alreadyFetchedPerformance = info.GetBoolean("_alreadyFetchedPerformance");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AdditionalDataFieldIndex)fieldIndex)
			{
				case AdditionalDataFieldIndex.PerformanceID:
					DesetupSyncPerformance(true, false);
					_alreadyFetchedPerformance = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPerformance = (_performance != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Performance":
					toReturn.Add(Relations.PerformanceEntityUsingPerformanceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			info.AddValue("_performance", (!this.MarkedForDeletion?_performance:null));
			info.AddValue("_performanceReturnsNewIfNotFound", _performanceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerformance", _alwaysFetchPerformance);
			info.AddValue("_alreadyFetchedPerformance", _alreadyFetchedPerformance);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Performance":
					_alreadyFetchedPerformance = true;
					this.Performance = (PerformanceEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Performance":
					SetupSyncPerformance(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Performance":
					DesetupSyncPerformance(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_performance!=null)
			{
				toReturn.Add(_performance);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="performanceID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPerformanceID(System.Int64 performanceID)
		{
			return FetchUsingUCPerformanceID( performanceID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="performanceID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPerformanceID(System.Int64 performanceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPerformanceID( performanceID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="performanceID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPerformanceID(System.Int64 performanceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPerformanceID( performanceID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="performanceID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPerformanceID(System.Int64 performanceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((AdditionalDataDAO)CreateDAOInstance()).FetchAdditionalDataUsingUCPerformanceID(this, this.Transaction, performanceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 additionalDataID)
		{
			return FetchUsingPK(additionalDataID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 additionalDataID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(additionalDataID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 additionalDataID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(additionalDataID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 additionalDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(additionalDataID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AdditionalDataID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AdditionalDataRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PerformanceEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PerformanceEntity' which is related to this entity.</returns>
		public PerformanceEntity GetSinglePerformance()
		{
			return GetSinglePerformance(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PerformanceEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PerformanceEntity' which is related to this entity.</returns>
		public virtual PerformanceEntity GetSinglePerformance(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerformance || forceFetch || _alwaysFetchPerformance) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PerformanceEntityUsingPerformanceID);
				PerformanceEntity newEntity = new PerformanceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PerformanceID);
				}
				if(fetchResult)
				{
					newEntity = (PerformanceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_performanceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Performance = newEntity;
				_alreadyFetchedPerformance = fetchResult;
			}
			return _performance;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Performance", _performance);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="validator">The validator object for this AdditionalDataEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 additionalDataID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(additionalDataID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_performanceReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdditionalDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HostName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PerformanceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _performance</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerformance(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _performance, new PropertyChangedEventHandler( OnPerformancePropertyChanged ), "Performance", VarioSL.Entities.RelationClasses.StaticAdditionalDataRelations.PerformanceEntityUsingPerformanceIDStatic, true, signalRelatedEntity, "AdditionalData", resetFKFields, new int[] { (int)AdditionalDataFieldIndex.PerformanceID } );
			_performance = null;
		}
	
		/// <summary> setups the sync logic for member _performance</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerformance(IEntityCore relatedEntity)
		{
			if(_performance!=relatedEntity)
			{
				DesetupSyncPerformance(true, true);
				_performance = (PerformanceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _performance, new PropertyChangedEventHandler( OnPerformancePropertyChanged ), "Performance", VarioSL.Entities.RelationClasses.StaticAdditionalDataRelations.PerformanceEntityUsingPerformanceIDStatic, true, ref _alreadyFetchedPerformance, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPerformancePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="additionalDataID">PK value for AdditionalData which data should be fetched into this AdditionalData object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 additionalDataID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AdditionalDataFieldIndex.AdditionalDataID].ForcedCurrentValueWrite(additionalDataID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAdditionalDataDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AdditionalDataEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AdditionalDataRelations Relations
		{
			get	{ return new AdditionalDataRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Performance'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerformance
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PerformanceCollection(), (IEntityRelation)GetRelationsForField("Performance")[0], (int)VarioSL.Entities.EntityType.AdditionalDataEntity, (int)VarioSL.Entities.EntityType.PerformanceEntity, 0, null, null, null, "Performance", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdditionalDataID property of the Entity AdditionalData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_ADDITIONALDATA"."ADDITIONALDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AdditionalDataID
		{
			get { return (System.Int64)GetValue((int)AdditionalDataFieldIndex.AdditionalDataID, true); }
			set	{ SetValue((int)AdditionalDataFieldIndex.AdditionalDataID, value, true); }
		}

		/// <summary> The HostName property of the Entity AdditionalData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_ADDITIONALDATA"."HOSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HostName
		{
			get { return (System.String)GetValue((int)AdditionalDataFieldIndex.HostName, true); }
			set	{ SetValue((int)AdditionalDataFieldIndex.HostName, value, true); }
		}

		/// <summary> The PerformanceID property of the Entity AdditionalData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_ADDITIONALDATA"."PERFORMANCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PerformanceID
		{
			get { return (System.Int64)GetValue((int)AdditionalDataFieldIndex.PerformanceID, true); }
			set	{ SetValue((int)AdditionalDataFieldIndex.PerformanceID, value, true); }
		}

		/// <summary> The TransactionID property of the Entity AdditionalData<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PERF_ADDITIONALDATA"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TransactionID
		{
			get { return (System.String)GetValue((int)AdditionalDataFieldIndex.TransactionID, true); }
			set	{ SetValue((int)AdditionalDataFieldIndex.TransactionID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PerformanceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerformance()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PerformanceEntity Performance
		{
			get	{ return GetSinglePerformance(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPerformance(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_performance !=null);
						DesetupSyncPerformance(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Performance");
						}
					}
					else
					{
						if(_performance!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "AdditionalData");
							SetupSyncPerformance(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Performance. When set to true, Performance is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Performance is accessed. You can always execute a forced fetch by calling GetSinglePerformance(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerformance
		{
			get	{ return _alwaysFetchPerformance; }
			set	{ _alwaysFetchPerformance = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Performance already has been fetched. Setting this property to false when Performance has been fetched
		/// will set Performance to null as well. Setting this property to true while Performance hasn't been fetched disables lazy loading for Performance</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerformance
		{
			get { return _alreadyFetchedPerformance;}
			set 
			{
				if(_alreadyFetchedPerformance && !value)
				{
					this.Performance = null;
				}
				_alreadyFetchedPerformance = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Performance is not found
		/// in the database. When set to true, Performance will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PerformanceReturnsNewIfNotFound
		{
			get	{ return _performanceReturnsNewIfNotFound; }
			set	{ _performanceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AdditionalDataEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
