﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareStageAlias'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareStageAliasEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private FareStageEntity _fareStage;
		private bool	_alwaysFetchFareStage, _alreadyFetchedFareStage, _fareStageReturnsNewIfNotFound;
		private FareStageTypeEntity _fareStageType;
		private bool	_alwaysFetchFareStageType, _alreadyFetchedFareStageType, _fareStageTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FareStage</summary>
			public static readonly string FareStage = "FareStage";
			/// <summary>Member name FareStageType</summary>
			public static readonly string FareStageType = "FareStageType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareStageAliasEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareStageAliasEntity() :base("FareStageAliasEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		public FareStageAliasEntity(System.Int64 fareStageAliasID):base("FareStageAliasEntity")
		{
			InitClassFetch(fareStageAliasID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareStageAliasEntity(System.Int64 fareStageAliasID, IPrefetchPath prefetchPathToUse):base("FareStageAliasEntity")
		{
			InitClassFetch(fareStageAliasID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="validator">The custom validator object for this FareStageAliasEntity</param>
		public FareStageAliasEntity(System.Int64 fareStageAliasID, IValidator validator):base("FareStageAliasEntity")
		{
			InitClassFetch(fareStageAliasID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareStageAliasEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareStage = (FareStageEntity)info.GetValue("_fareStage", typeof(FareStageEntity));
			if(_fareStage!=null)
			{
				_fareStage.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageReturnsNewIfNotFound = info.GetBoolean("_fareStageReturnsNewIfNotFound");
			_alwaysFetchFareStage = info.GetBoolean("_alwaysFetchFareStage");
			_alreadyFetchedFareStage = info.GetBoolean("_alreadyFetchedFareStage");

			_fareStageType = (FareStageTypeEntity)info.GetValue("_fareStageType", typeof(FareStageTypeEntity));
			if(_fareStageType!=null)
			{
				_fareStageType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareStageTypeReturnsNewIfNotFound = info.GetBoolean("_fareStageTypeReturnsNewIfNotFound");
			_alwaysFetchFareStageType = info.GetBoolean("_alwaysFetchFareStageType");
			_alreadyFetchedFareStageType = info.GetBoolean("_alreadyFetchedFareStageType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareStageAliasFieldIndex)fieldIndex)
			{
				case FareStageAliasFieldIndex.FareStageID:
					DesetupSyncFareStage(true, false);
					_alreadyFetchedFareStage = false;
					break;
				case FareStageAliasFieldIndex.FareStageTypeID:
					DesetupSyncFareStageType(true, false);
					_alreadyFetchedFareStageType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareStage = (_fareStage != null);
			_alreadyFetchedFareStageType = (_fareStageType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FareStage":
					toReturn.Add(Relations.FareStageEntityUsingFareStageID);
					break;
				case "FareStageType":
					toReturn.Add(Relations.FareStageTypeEntityUsingFareStageTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareStage", (!this.MarkedForDeletion?_fareStage:null));
			info.AddValue("_fareStageReturnsNewIfNotFound", _fareStageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStage", _alwaysFetchFareStage);
			info.AddValue("_alreadyFetchedFareStage", _alreadyFetchedFareStage);
			info.AddValue("_fareStageType", (!this.MarkedForDeletion?_fareStageType:null));
			info.AddValue("_fareStageTypeReturnsNewIfNotFound", _fareStageTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareStageType", _alwaysFetchFareStageType);
			info.AddValue("_alreadyFetchedFareStageType", _alreadyFetchedFareStageType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FareStage":
					_alreadyFetchedFareStage = true;
					this.FareStage = (FareStageEntity)entity;
					break;
				case "FareStageType":
					_alreadyFetchedFareStageType = true;
					this.FareStageType = (FareStageTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FareStage":
					SetupSyncFareStage(relatedEntity);
					break;
				case "FareStageType":
					SetupSyncFareStageType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FareStage":
					DesetupSyncFareStage(false, true);
					break;
				case "FareStageType":
					DesetupSyncFareStageType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fareStage!=null)
			{
				toReturn.Add(_fareStage);
			}
			if(_fareStageType!=null)
			{
				toReturn.Add(_fareStageType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageAliasID)
		{
			return FetchUsingPK(fareStageAliasID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageAliasID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareStageAliasID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageAliasID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareStageAliasID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareStageAliasID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareStageAliasID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareStageAliasID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareStageAliasRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public FareStageEntity GetSingleFareStage()
		{
			return GetSingleFareStage(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageEntity' which is related to this entity.</returns>
		public virtual FareStageEntity GetSingleFareStage(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStage || forceFetch || _alwaysFetchFareStage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageEntityUsingFareStageID);
				FareStageEntity newEntity = new FareStageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageID);
				}
				if(fetchResult)
				{
					newEntity = (FareStageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStage = newEntity;
				_alreadyFetchedFareStage = fetchResult;
			}
			return _fareStage;
		}


		/// <summary> Retrieves the related entity of type 'FareStageTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareStageTypeEntity' which is related to this entity.</returns>
		public FareStageTypeEntity GetSingleFareStageType()
		{
			return GetSingleFareStageType(false);
		}

		/// <summary> Retrieves the related entity of type 'FareStageTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareStageTypeEntity' which is related to this entity.</returns>
		public virtual FareStageTypeEntity GetSingleFareStageType(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareStageType || forceFetch || _alwaysFetchFareStageType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareStageTypeEntityUsingFareStageTypeID);
				FareStageTypeEntity newEntity = new FareStageTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareStageTypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareStageTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareStageTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareStageType = newEntity;
				_alreadyFetchedFareStageType = fetchResult;
			}
			return _fareStageType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FareStage", _fareStage);
			toReturn.Add("FareStageType", _fareStageType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="validator">The validator object for this FareStageAliasEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareStageAliasID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareStageAliasID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_fareStageReturnsNewIfNotFound = false;
			_fareStageTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageAliasID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareStageTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fareStage</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticFareStageAliasRelations.FareStageEntityUsingFareStageIDStatic, true, signalRelatedEntity, "FareStageAlias", resetFKFields, new int[] { (int)FareStageAliasFieldIndex.FareStageID } );		
			_fareStage = null;
		}
		
		/// <summary> setups the sync logic for member _fareStage</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStage(IEntityCore relatedEntity)
		{
			if(_fareStage!=relatedEntity)
			{		
				DesetupSyncFareStage(true, true);
				_fareStage = (FareStageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStage, new PropertyChangedEventHandler( OnFareStagePropertyChanged ), "FareStage", VarioSL.Entities.RelationClasses.StaticFareStageAliasRelations.FareStageEntityUsingFareStageIDStatic, true, ref _alreadyFetchedFareStage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareStageType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareStageType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareStageType, new PropertyChangedEventHandler( OnFareStageTypePropertyChanged ), "FareStageType", VarioSL.Entities.RelationClasses.StaticFareStageAliasRelations.FareStageTypeEntityUsingFareStageTypeIDStatic, true, signalRelatedEntity, "FareStageAlia", resetFKFields, new int[] { (int)FareStageAliasFieldIndex.FareStageTypeID } );		
			_fareStageType = null;
		}
		
		/// <summary> setups the sync logic for member _fareStageType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareStageType(IEntityCore relatedEntity)
		{
			if(_fareStageType!=relatedEntity)
			{		
				DesetupSyncFareStageType(true, true);
				_fareStageType = (FareStageTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareStageType, new PropertyChangedEventHandler( OnFareStageTypePropertyChanged ), "FareStageType", VarioSL.Entities.RelationClasses.StaticFareStageAliasRelations.FareStageTypeEntityUsingFareStageTypeIDStatic, true, ref _alreadyFetchedFareStageType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareStageTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareStageAliasID">PK value for FareStageAlias which data should be fetched into this FareStageAlias object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareStageAliasID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareStageAliasFieldIndex.FareStageAliasID].ForcedCurrentValueWrite(fareStageAliasID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareStageAliasDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareStageAliasEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareStageAliasRelations Relations
		{
			get	{ return new FareStageAliasRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageCollection(), (IEntityRelation)GetRelationsForField("FareStage")[0], (int)VarioSL.Entities.EntityType.FareStageAliasEntity, (int)VarioSL.Entities.EntityType.FareStageEntity, 0, null, null, null, "FareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageTypeCollection(), (IEntityRelation)GetRelationsForField("FareStageType")[0], (int)VarioSL.Entities.EntityType.FareStageAliasEntity, (int)VarioSL.Entities.EntityType.FareStageTypeEntity, 0, null, null, null, "FareStageType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeValueID property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageAliasFieldIndex.AttributeValueID, false); }
			set	{ SetValue((int)FareStageAliasFieldIndex.AttributeValueID, value, true); }
		}

		/// <summary> The Description property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FareStageAliasFieldIndex.Description, true); }
			set	{ SetValue((int)FareStageAliasFieldIndex.Description, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageAliasFieldIndex.ExternalNumber, false); }
			set	{ SetValue((int)FareStageAliasFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The FareStageAliasID property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."FARESTAGEALIASID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareStageAliasID
		{
			get { return (System.Int64)GetValue((int)FareStageAliasFieldIndex.FareStageAliasID, true); }
			set	{ SetValue((int)FareStageAliasFieldIndex.FareStageAliasID, value, true); }
		}

		/// <summary> The FareStageClass property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."FARESTAGECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareStageClass
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareStageAliasFieldIndex.FareStageClass, false); }
			set	{ SetValue((int)FareStageAliasFieldIndex.FareStageClass, value, true); }
		}

		/// <summary> The FareStageID property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."FARESTAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FareStageID
		{
			get { return (System.Int64)GetValue((int)FareStageAliasFieldIndex.FareStageID, true); }
			set	{ SetValue((int)FareStageAliasFieldIndex.FareStageID, value, true); }
		}

		/// <summary> The FareStageTypeID property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."FARESTAGETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> FareStageTypeID
		{
			get { return (Nullable<System.Decimal>)GetValue((int)FareStageAliasFieldIndex.FareStageTypeID, false); }
			set	{ SetValue((int)FareStageAliasFieldIndex.FareStageTypeID, value, true); }
		}

		/// <summary> The Name property of the Entity FareStageAlias<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARESTAGEALIAS"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareStageAliasFieldIndex.Name, true); }
			set	{ SetValue((int)FareStageAliasFieldIndex.Name, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'FareStageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageEntity FareStage
		{
			get	{ return GetSingleFareStage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStageAlias", "FareStage", _fareStage, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStage. When set to true, FareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStage is accessed. You can always execute a forced fetch by calling GetSingleFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStage
		{
			get	{ return _alwaysFetchFareStage; }
			set	{ _alwaysFetchFareStage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStage already has been fetched. Setting this property to false when FareStage has been fetched
		/// will set FareStage to null as well. Setting this property to true while FareStage hasn't been fetched disables lazy loading for FareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStage
		{
			get { return _alreadyFetchedFareStage;}
			set 
			{
				if(_alreadyFetchedFareStage && !value)
				{
					this.FareStage = null;
				}
				_alreadyFetchedFareStage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStage is not found
		/// in the database. When set to true, FareStage will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageReturnsNewIfNotFound
		{
			get	{ return _fareStageReturnsNewIfNotFound; }
			set { _fareStageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareStageTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareStageType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareStageTypeEntity FareStageType
		{
			get	{ return GetSingleFareStageType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareStageType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareStageAlia", "FareStageType", _fareStageType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageType. When set to true, FareStageType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageType is accessed. You can always execute a forced fetch by calling GetSingleFareStageType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageType
		{
			get	{ return _alwaysFetchFareStageType; }
			set	{ _alwaysFetchFareStageType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageType already has been fetched. Setting this property to false when FareStageType has been fetched
		/// will set FareStageType to null as well. Setting this property to true while FareStageType hasn't been fetched disables lazy loading for FareStageType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageType
		{
			get { return _alreadyFetchedFareStageType;}
			set 
			{
				if(_alreadyFetchedFareStageType && !value)
				{
					this.FareStageType = null;
				}
				_alreadyFetchedFareStageType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareStageType is not found
		/// in the database. When set to true, FareStageType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareStageTypeReturnsNewIfNotFound
		{
			get	{ return _fareStageTypeReturnsNewIfNotFound; }
			set { _fareStageTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareStageAliasEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
