﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyQuestion'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SurveyQuestionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SurveyAnswerCollection	_surveyAnswers;
		private bool	_alwaysFetchSurveyAnswers, _alreadyFetchedSurveyAnswers;
		private VarioSL.Entities.CollectionClasses.SurveyChoiceCollection	_followedUpChoices;
		private bool	_alwaysFetchFollowedUpChoices, _alreadyFetchedFollowedUpChoices;
		private VarioSL.Entities.CollectionClasses.SurveyChoiceCollection	_surveyChoices;
		private bool	_alwaysFetchSurveyChoices, _alreadyFetchedSurveyChoices;
		private SurveyEntity _survey;
		private bool	_alwaysFetchSurvey, _alreadyFetchedSurvey, _surveyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Survey</summary>
			public static readonly string Survey = "Survey";
			/// <summary>Member name SurveyAnswers</summary>
			public static readonly string SurveyAnswers = "SurveyAnswers";
			/// <summary>Member name FollowedUpChoices</summary>
			public static readonly string FollowedUpChoices = "FollowedUpChoices";
			/// <summary>Member name SurveyChoices</summary>
			public static readonly string SurveyChoices = "SurveyChoices";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyQuestionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyQuestionEntity() :base("SurveyQuestionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		public SurveyQuestionEntity(System.Int64 surveyQuestionID):base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyQuestionEntity(System.Int64 surveyQuestionID, IPrefetchPath prefetchPathToUse):base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="validator">The custom validator object for this SurveyQuestionEntity</param>
		public SurveyQuestionEntity(System.Int64 surveyQuestionID, IValidator validator):base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyQuestionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyAnswers = (VarioSL.Entities.CollectionClasses.SurveyAnswerCollection)info.GetValue("_surveyAnswers", typeof(VarioSL.Entities.CollectionClasses.SurveyAnswerCollection));
			_alwaysFetchSurveyAnswers = info.GetBoolean("_alwaysFetchSurveyAnswers");
			_alreadyFetchedSurveyAnswers = info.GetBoolean("_alreadyFetchedSurveyAnswers");

			_followedUpChoices = (VarioSL.Entities.CollectionClasses.SurveyChoiceCollection)info.GetValue("_followedUpChoices", typeof(VarioSL.Entities.CollectionClasses.SurveyChoiceCollection));
			_alwaysFetchFollowedUpChoices = info.GetBoolean("_alwaysFetchFollowedUpChoices");
			_alreadyFetchedFollowedUpChoices = info.GetBoolean("_alreadyFetchedFollowedUpChoices");

			_surveyChoices = (VarioSL.Entities.CollectionClasses.SurveyChoiceCollection)info.GetValue("_surveyChoices", typeof(VarioSL.Entities.CollectionClasses.SurveyChoiceCollection));
			_alwaysFetchSurveyChoices = info.GetBoolean("_alwaysFetchSurveyChoices");
			_alreadyFetchedSurveyChoices = info.GetBoolean("_alreadyFetchedSurveyChoices");
			_survey = (SurveyEntity)info.GetValue("_survey", typeof(SurveyEntity));
			if(_survey!=null)
			{
				_survey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyReturnsNewIfNotFound = info.GetBoolean("_surveyReturnsNewIfNotFound");
			_alwaysFetchSurvey = info.GetBoolean("_alwaysFetchSurvey");
			_alreadyFetchedSurvey = info.GetBoolean("_alreadyFetchedSurvey");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyQuestionFieldIndex)fieldIndex)
			{
				case SurveyQuestionFieldIndex.SurveyID:
					DesetupSyncSurvey(true, false);
					_alreadyFetchedSurvey = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyAnswers = (_surveyAnswers.Count > 0);
			_alreadyFetchedFollowedUpChoices = (_followedUpChoices.Count > 0);
			_alreadyFetchedSurveyChoices = (_surveyChoices.Count > 0);
			_alreadyFetchedSurvey = (_survey != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Survey":
					toReturn.Add(Relations.SurveyEntityUsingSurveyID);
					break;
				case "SurveyAnswers":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyQuestionID);
					break;
				case "FollowedUpChoices":
					toReturn.Add(Relations.SurveyChoiceEntityUsingFollowUpQuestionID);
					break;
				case "SurveyChoices":
					toReturn.Add(Relations.SurveyChoiceEntityUsingSurveyQuestionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyAnswers", (!this.MarkedForDeletion?_surveyAnswers:null));
			info.AddValue("_alwaysFetchSurveyAnswers", _alwaysFetchSurveyAnswers);
			info.AddValue("_alreadyFetchedSurveyAnswers", _alreadyFetchedSurveyAnswers);
			info.AddValue("_followedUpChoices", (!this.MarkedForDeletion?_followedUpChoices:null));
			info.AddValue("_alwaysFetchFollowedUpChoices", _alwaysFetchFollowedUpChoices);
			info.AddValue("_alreadyFetchedFollowedUpChoices", _alreadyFetchedFollowedUpChoices);
			info.AddValue("_surveyChoices", (!this.MarkedForDeletion?_surveyChoices:null));
			info.AddValue("_alwaysFetchSurveyChoices", _alwaysFetchSurveyChoices);
			info.AddValue("_alreadyFetchedSurveyChoices", _alreadyFetchedSurveyChoices);
			info.AddValue("_survey", (!this.MarkedForDeletion?_survey:null));
			info.AddValue("_surveyReturnsNewIfNotFound", _surveyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurvey", _alwaysFetchSurvey);
			info.AddValue("_alreadyFetchedSurvey", _alreadyFetchedSurvey);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Survey":
					_alreadyFetchedSurvey = true;
					this.Survey = (SurveyEntity)entity;
					break;
				case "SurveyAnswers":
					_alreadyFetchedSurveyAnswers = true;
					if(entity!=null)
					{
						this.SurveyAnswers.Add((SurveyAnswerEntity)entity);
					}
					break;
				case "FollowedUpChoices":
					_alreadyFetchedFollowedUpChoices = true;
					if(entity!=null)
					{
						this.FollowedUpChoices.Add((SurveyChoiceEntity)entity);
					}
					break;
				case "SurveyChoices":
					_alreadyFetchedSurveyChoices = true;
					if(entity!=null)
					{
						this.SurveyChoices.Add((SurveyChoiceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Survey":
					SetupSyncSurvey(relatedEntity);
					break;
				case "SurveyAnswers":
					_surveyAnswers.Add((SurveyAnswerEntity)relatedEntity);
					break;
				case "FollowedUpChoices":
					_followedUpChoices.Add((SurveyChoiceEntity)relatedEntity);
					break;
				case "SurveyChoices":
					_surveyChoices.Add((SurveyChoiceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Survey":
					DesetupSyncSurvey(false, true);
					break;
				case "SurveyAnswers":
					this.PerformRelatedEntityRemoval(_surveyAnswers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FollowedUpChoices":
					this.PerformRelatedEntityRemoval(_followedUpChoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyChoices":
					this.PerformRelatedEntityRemoval(_surveyChoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_survey!=null)
			{
				toReturn.Add(_survey);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyAnswers);
			toReturn.Add(_followedUpChoices);
			toReturn.Add(_surveyChoices);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyQuestionID)
		{
			return FetchUsingPK(surveyQuestionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyQuestionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyQuestionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyQuestionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyQuestionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 surveyQuestionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyQuestionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyQuestionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyQuestionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswers(forceFetch, _surveyAnswers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswers || forceFetch || _alwaysFetchSurveyAnswers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswers);
				_surveyAnswers.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswers.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswers.GetMultiManyToOne(null, this, null, filter);
				_surveyAnswers.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswers = true;
			}
			return _surveyAnswers;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswers'. These settings will be taken into account
		/// when the property SurveyAnswers is requested or GetMultiSurveyAnswers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswers.SortClauses=sortClauses;
			_surveyAnswers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiFollowedUpChoices(bool forceFetch)
		{
			return GetMultiFollowedUpChoices(forceFetch, _followedUpChoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiFollowedUpChoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFollowedUpChoices(forceFetch, _followedUpChoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiFollowedUpChoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFollowedUpChoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiFollowedUpChoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFollowedUpChoices || forceFetch || _alwaysFetchFollowedUpChoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_followedUpChoices);
				_followedUpChoices.SuppressClearInGetMulti=!forceFetch;
				_followedUpChoices.EntityFactoryToUse = entityFactoryToUse;
				_followedUpChoices.GetMultiManyToOne(this, null, filter);
				_followedUpChoices.SuppressClearInGetMulti=false;
				_alreadyFetchedFollowedUpChoices = true;
			}
			return _followedUpChoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'FollowedUpChoices'. These settings will be taken into account
		/// when the property FollowedUpChoices is requested or GetMultiFollowedUpChoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFollowedUpChoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_followedUpChoices.SortClauses=sortClauses;
			_followedUpChoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiSurveyChoices(bool forceFetch)
		{
			return GetMultiSurveyChoices(forceFetch, _surveyChoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiSurveyChoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyChoices(forceFetch, _surveyChoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiSurveyChoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyChoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyChoiceCollection GetMultiSurveyChoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyChoices || forceFetch || _alwaysFetchSurveyChoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyChoices);
				_surveyChoices.SuppressClearInGetMulti=!forceFetch;
				_surveyChoices.EntityFactoryToUse = entityFactoryToUse;
				_surveyChoices.GetMultiManyToOne(null, this, filter);
				_surveyChoices.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyChoices = true;
			}
			return _surveyChoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyChoices'. These settings will be taken into account
		/// when the property SurveyChoices is requested or GetMultiSurveyChoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyChoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyChoices.SortClauses=sortClauses;
			_surveyChoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public SurveyEntity GetSingleSurvey()
		{
			return GetSingleSurvey(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyEntity' which is related to this entity.</returns>
		public virtual SurveyEntity GetSingleSurvey(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurvey || forceFetch || _alwaysFetchSurvey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyEntityUsingSurveyID);
				SurveyEntity newEntity = new SurveyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyID);
				}
				if(fetchResult)
				{
					newEntity = (SurveyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Survey = newEntity;
				_alreadyFetchedSurvey = fetchResult;
			}
			return _survey;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Survey", _survey);
			toReturn.Add("SurveyAnswers", _surveyAnswers);
			toReturn.Add("FollowedUpChoices", _followedUpChoices);
			toReturn.Add("SurveyChoices", _surveyChoices);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="validator">The validator object for this SurveyQuestionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 surveyQuestionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyQuestionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyAnswers = new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection();
			_surveyAnswers.SetContainingEntityInfo(this, "SurveyQuestion");

			_followedUpChoices = new VarioSL.Entities.CollectionClasses.SurveyChoiceCollection();
			_followedUpChoices.SetContainingEntityInfo(this, "FollowUpQuestion");

			_surveyChoices = new VarioSL.Entities.CollectionClasses.SurveyChoiceCollection();
			_surveyChoices.SetContainingEntityInfo(this, "SurveyQuestion");
			_surveyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasMultipleAnswers", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsRetired", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Language", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rank", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyQuestionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _survey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurvey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyQuestionRelations.SurveyEntityUsingSurveyIDStatic, true, signalRelatedEntity, "SurveyQuestions", resetFKFields, new int[] { (int)SurveyQuestionFieldIndex.SurveyID } );		
			_survey = null;
		}
		
		/// <summary> setups the sync logic for member _survey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurvey(IEntityCore relatedEntity)
		{
			if(_survey!=relatedEntity)
			{		
				DesetupSyncSurvey(true, true);
				_survey = (SurveyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _survey, new PropertyChangedEventHandler( OnSurveyPropertyChanged ), "Survey", VarioSL.Entities.RelationClasses.StaticSurveyQuestionRelations.SurveyEntityUsingSurveyIDStatic, true, ref _alreadyFetchedSurvey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyQuestionID">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 surveyQuestionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyQuestionFieldIndex.SurveyQuestionID].ForcedCurrentValueWrite(surveyQuestionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyQuestionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyQuestionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyQuestionRelations Relations
		{
			get	{ return new SurveyQuestionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswers")[0], (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, (int)VarioSL.Entities.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyChoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFollowedUpChoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyChoiceCollection(), (IEntityRelation)GetRelationsForField("FollowedUpChoices")[0], (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, (int)VarioSL.Entities.EntityType.SurveyChoiceEntity, 0, null, null, null, "FollowedUpChoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyChoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyChoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyChoiceCollection(), (IEntityRelation)GetRelationsForField("SurveyChoices")[0], (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, (int)VarioSL.Entities.EntityType.SurveyChoiceEntity, 0, null, null, null, "SurveyChoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Survey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurvey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyCollection(), (IEntityRelation)GetRelationsForField("Survey")[0], (int)VarioSL.Entities.EntityType.SurveyQuestionEntity, (int)VarioSL.Entities.EntityType.SurveyEntity, 0, null, null, null, "Survey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The HasMultipleAnswers property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."HASMULTIPLEANSWERS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasMultipleAnswers
		{
			get { return (System.Boolean)GetValue((int)SurveyQuestionFieldIndex.HasMultipleAnswers, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.HasMultipleAnswers, value, true); }
		}

		/// <summary> The IsRetired property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."ISRETIRED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsRetired
		{
			get { return (System.Boolean)GetValue((int)SurveyQuestionFieldIndex.IsRetired, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.IsRetired, value, true); }
		}

		/// <summary> The Language property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."LANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Language
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.Language, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Language, value, true); }
		}

		/// <summary> The LastModified property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SurveyQuestionFieldIndex.LastModified, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.LastUser, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Rank property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."RANK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Rank
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.Rank, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Rank, value, true); }
		}

		/// <summary> The SurveyID property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."SURVEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SurveyID
		{
			get { return (System.Int64)GetValue((int)SurveyQuestionFieldIndex.SurveyID, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.SurveyID, value, true); }
		}

		/// <summary> The SurveyQuestionID property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."SURVEYQUESTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SurveyQuestionID
		{
			get { return (System.Int64)GetValue((int)SurveyQuestionFieldIndex.SurveyQuestionID, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.SurveyQuestionID, value, true); }
		}

		/// <summary> The Text property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."TEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.Text, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Text, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SURVEYQUESTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SurveyQuestionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyAnswerCollection SurveyAnswers
		{
			get	{ return GetMultiSurveyAnswers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswers. When set to true, SurveyAnswers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswers is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswers
		{
			get	{ return _alwaysFetchSurveyAnswers; }
			set	{ _alwaysFetchSurveyAnswers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswers already has been fetched. Setting this property to false when SurveyAnswers has been fetched
		/// will clear the SurveyAnswers collection well. Setting this property to true while SurveyAnswers hasn't been fetched disables lazy loading for SurveyAnswers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswers
		{
			get { return _alreadyFetchedSurveyAnswers;}
			set 
			{
				if(_alreadyFetchedSurveyAnswers && !value && (_surveyAnswers != null))
				{
					_surveyAnswers.Clear();
				}
				_alreadyFetchedSurveyAnswers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFollowedUpChoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyChoiceCollection FollowedUpChoices
		{
			get	{ return GetMultiFollowedUpChoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FollowedUpChoices. When set to true, FollowedUpChoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FollowedUpChoices is accessed. You can always execute/ a forced fetch by calling GetMultiFollowedUpChoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFollowedUpChoices
		{
			get	{ return _alwaysFetchFollowedUpChoices; }
			set	{ _alwaysFetchFollowedUpChoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FollowedUpChoices already has been fetched. Setting this property to false when FollowedUpChoices has been fetched
		/// will clear the FollowedUpChoices collection well. Setting this property to true while FollowedUpChoices hasn't been fetched disables lazy loading for FollowedUpChoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFollowedUpChoices
		{
			get { return _alreadyFetchedFollowedUpChoices;}
			set 
			{
				if(_alreadyFetchedFollowedUpChoices && !value && (_followedUpChoices != null))
				{
					_followedUpChoices.Clear();
				}
				_alreadyFetchedFollowedUpChoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyChoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyChoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyChoiceCollection SurveyChoices
		{
			get	{ return GetMultiSurveyChoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyChoices. When set to true, SurveyChoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyChoices is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyChoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyChoices
		{
			get	{ return _alwaysFetchSurveyChoices; }
			set	{ _alwaysFetchSurveyChoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyChoices already has been fetched. Setting this property to false when SurveyChoices has been fetched
		/// will clear the SurveyChoices collection well. Setting this property to true while SurveyChoices hasn't been fetched disables lazy loading for SurveyChoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyChoices
		{
			get { return _alreadyFetchedSurveyChoices;}
			set 
			{
				if(_alreadyFetchedSurveyChoices && !value && (_surveyChoices != null))
				{
					_surveyChoices.Clear();
				}
				_alreadyFetchedSurveyChoices = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SurveyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurvey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SurveyEntity Survey
		{
			get	{ return GetSingleSurvey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurvey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyQuestions", "Survey", _survey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Survey. When set to true, Survey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Survey is accessed. You can always execute a forced fetch by calling GetSingleSurvey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurvey
		{
			get	{ return _alwaysFetchSurvey; }
			set	{ _alwaysFetchSurvey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Survey already has been fetched. Setting this property to false when Survey has been fetched
		/// will set Survey to null as well. Setting this property to true while Survey hasn't been fetched disables lazy loading for Survey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurvey
		{
			get { return _alreadyFetchedSurvey;}
			set 
			{
				if(_alreadyFetchedSurvey && !value)
				{
					this.Survey = null;
				}
				_alreadyFetchedSurvey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Survey is not found
		/// in the database. When set to true, Survey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SurveyReturnsNewIfNotFound
		{
			get	{ return _surveyReturnsNewIfNotFound; }
			set { _surveyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SurveyQuestionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
