﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceReader'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceReaderEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection	_deviceCommunicationHistories;
		private bool	_alwaysFetchDeviceCommunicationHistories, _alreadyFetchedDeviceCommunicationHistories;
		private VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection	_deviceReaderCryptoResources;
		private bool	_alwaysFetchDeviceReaderCryptoResources, _alreadyFetchedDeviceReaderCryptoResources;
		private VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection	_targetDeviceReaderJobs;
		private bool	_alwaysFetchTargetDeviceReaderJobs, _alreadyFetchedTargetDeviceReaderJobs;
		private VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection	_workerDeviceReaderJobs;
		private bool	_alwaysFetchWorkerDeviceReaderJobs, _alreadyFetchedWorkerDeviceReaderJobs;
		private VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection	_deviceReaderKeys;
		private bool	_alwaysFetchDeviceReaderKeys, _alreadyFetchedDeviceReaderKeys;
		private VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection	_deviceReaderToCerts;
		private bool	_alwaysFetchDeviceReaderToCerts, _alreadyFetchedDeviceReaderToCerts;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceCommunicationHistories</summary>
			public static readonly string DeviceCommunicationHistories = "DeviceCommunicationHistories";
			/// <summary>Member name DeviceReaderCryptoResources</summary>
			public static readonly string DeviceReaderCryptoResources = "DeviceReaderCryptoResources";
			/// <summary>Member name TargetDeviceReaderJobs</summary>
			public static readonly string TargetDeviceReaderJobs = "TargetDeviceReaderJobs";
			/// <summary>Member name WorkerDeviceReaderJobs</summary>
			public static readonly string WorkerDeviceReaderJobs = "WorkerDeviceReaderJobs";
			/// <summary>Member name DeviceReaderKeys</summary>
			public static readonly string DeviceReaderKeys = "DeviceReaderKeys";
			/// <summary>Member name DeviceReaderToCerts</summary>
			public static readonly string DeviceReaderToCerts = "DeviceReaderToCerts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceReaderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceReaderEntity() :base("DeviceReaderEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		public DeviceReaderEntity(System.Int64 deviceReaderID):base("DeviceReaderEntity")
		{
			InitClassFetch(deviceReaderID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceReaderEntity(System.Int64 deviceReaderID, IPrefetchPath prefetchPathToUse):base("DeviceReaderEntity")
		{
			InitClassFetch(deviceReaderID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="validator">The custom validator object for this DeviceReaderEntity</param>
		public DeviceReaderEntity(System.Int64 deviceReaderID, IValidator validator):base("DeviceReaderEntity")
		{
			InitClassFetch(deviceReaderID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceReaderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_deviceCommunicationHistories = (VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection)info.GetValue("_deviceCommunicationHistories", typeof(VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection));
			_alwaysFetchDeviceCommunicationHistories = info.GetBoolean("_alwaysFetchDeviceCommunicationHistories");
			_alreadyFetchedDeviceCommunicationHistories = info.GetBoolean("_alreadyFetchedDeviceCommunicationHistories");

			_deviceReaderCryptoResources = (VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection)info.GetValue("_deviceReaderCryptoResources", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection));
			_alwaysFetchDeviceReaderCryptoResources = info.GetBoolean("_alwaysFetchDeviceReaderCryptoResources");
			_alreadyFetchedDeviceReaderCryptoResources = info.GetBoolean("_alreadyFetchedDeviceReaderCryptoResources");

			_targetDeviceReaderJobs = (VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection)info.GetValue("_targetDeviceReaderJobs", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection));
			_alwaysFetchTargetDeviceReaderJobs = info.GetBoolean("_alwaysFetchTargetDeviceReaderJobs");
			_alreadyFetchedTargetDeviceReaderJobs = info.GetBoolean("_alreadyFetchedTargetDeviceReaderJobs");

			_workerDeviceReaderJobs = (VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection)info.GetValue("_workerDeviceReaderJobs", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection));
			_alwaysFetchWorkerDeviceReaderJobs = info.GetBoolean("_alwaysFetchWorkerDeviceReaderJobs");
			_alreadyFetchedWorkerDeviceReaderJobs = info.GetBoolean("_alreadyFetchedWorkerDeviceReaderJobs");

			_deviceReaderKeys = (VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection)info.GetValue("_deviceReaderKeys", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection));
			_alwaysFetchDeviceReaderKeys = info.GetBoolean("_alwaysFetchDeviceReaderKeys");
			_alreadyFetchedDeviceReaderKeys = info.GetBoolean("_alreadyFetchedDeviceReaderKeys");

			_deviceReaderToCerts = (VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection)info.GetValue("_deviceReaderToCerts", typeof(VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection));
			_alwaysFetchDeviceReaderToCerts = info.GetBoolean("_alwaysFetchDeviceReaderToCerts");
			_alreadyFetchedDeviceReaderToCerts = info.GetBoolean("_alreadyFetchedDeviceReaderToCerts");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDeviceCommunicationHistories = (_deviceCommunicationHistories.Count > 0);
			_alreadyFetchedDeviceReaderCryptoResources = (_deviceReaderCryptoResources.Count > 0);
			_alreadyFetchedTargetDeviceReaderJobs = (_targetDeviceReaderJobs.Count > 0);
			_alreadyFetchedWorkerDeviceReaderJobs = (_workerDeviceReaderJobs.Count > 0);
			_alreadyFetchedDeviceReaderKeys = (_deviceReaderKeys.Count > 0);
			_alreadyFetchedDeviceReaderToCerts = (_deviceReaderToCerts.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceCommunicationHistories":
					toReturn.Add(Relations.DeviceCommunicationHistoryEntityUsingDeviceReaderID);
					break;
				case "DeviceReaderCryptoResources":
					toReturn.Add(Relations.DeviceReaderCryptoResourceEntityUsingDeviceReaderID);
					break;
				case "TargetDeviceReaderJobs":
					toReturn.Add(Relations.DeviceReaderJobEntityUsingTargetReaderID);
					break;
				case "WorkerDeviceReaderJobs":
					toReturn.Add(Relations.DeviceReaderJobEntityUsingWorkerReaderID);
					break;
				case "DeviceReaderKeys":
					toReturn.Add(Relations.DeviceReaderKeyEntityUsingDeviceReaderID);
					break;
				case "DeviceReaderToCerts":
					toReturn.Add(Relations.DeviceReaderToCertEntityUsingDeviceReaderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_deviceCommunicationHistories", (!this.MarkedForDeletion?_deviceCommunicationHistories:null));
			info.AddValue("_alwaysFetchDeviceCommunicationHistories", _alwaysFetchDeviceCommunicationHistories);
			info.AddValue("_alreadyFetchedDeviceCommunicationHistories", _alreadyFetchedDeviceCommunicationHistories);
			info.AddValue("_deviceReaderCryptoResources", (!this.MarkedForDeletion?_deviceReaderCryptoResources:null));
			info.AddValue("_alwaysFetchDeviceReaderCryptoResources", _alwaysFetchDeviceReaderCryptoResources);
			info.AddValue("_alreadyFetchedDeviceReaderCryptoResources", _alreadyFetchedDeviceReaderCryptoResources);
			info.AddValue("_targetDeviceReaderJobs", (!this.MarkedForDeletion?_targetDeviceReaderJobs:null));
			info.AddValue("_alwaysFetchTargetDeviceReaderJobs", _alwaysFetchTargetDeviceReaderJobs);
			info.AddValue("_alreadyFetchedTargetDeviceReaderJobs", _alreadyFetchedTargetDeviceReaderJobs);
			info.AddValue("_workerDeviceReaderJobs", (!this.MarkedForDeletion?_workerDeviceReaderJobs:null));
			info.AddValue("_alwaysFetchWorkerDeviceReaderJobs", _alwaysFetchWorkerDeviceReaderJobs);
			info.AddValue("_alreadyFetchedWorkerDeviceReaderJobs", _alreadyFetchedWorkerDeviceReaderJobs);
			info.AddValue("_deviceReaderKeys", (!this.MarkedForDeletion?_deviceReaderKeys:null));
			info.AddValue("_alwaysFetchDeviceReaderKeys", _alwaysFetchDeviceReaderKeys);
			info.AddValue("_alreadyFetchedDeviceReaderKeys", _alreadyFetchedDeviceReaderKeys);
			info.AddValue("_deviceReaderToCerts", (!this.MarkedForDeletion?_deviceReaderToCerts:null));
			info.AddValue("_alwaysFetchDeviceReaderToCerts", _alwaysFetchDeviceReaderToCerts);
			info.AddValue("_alreadyFetchedDeviceReaderToCerts", _alreadyFetchedDeviceReaderToCerts);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceCommunicationHistories":
					_alreadyFetchedDeviceCommunicationHistories = true;
					if(entity!=null)
					{
						this.DeviceCommunicationHistories.Add((DeviceCommunicationHistoryEntity)entity);
					}
					break;
				case "DeviceReaderCryptoResources":
					_alreadyFetchedDeviceReaderCryptoResources = true;
					if(entity!=null)
					{
						this.DeviceReaderCryptoResources.Add((DeviceReaderCryptoResourceEntity)entity);
					}
					break;
				case "TargetDeviceReaderJobs":
					_alreadyFetchedTargetDeviceReaderJobs = true;
					if(entity!=null)
					{
						this.TargetDeviceReaderJobs.Add((DeviceReaderJobEntity)entity);
					}
					break;
				case "WorkerDeviceReaderJobs":
					_alreadyFetchedWorkerDeviceReaderJobs = true;
					if(entity!=null)
					{
						this.WorkerDeviceReaderJobs.Add((DeviceReaderJobEntity)entity);
					}
					break;
				case "DeviceReaderKeys":
					_alreadyFetchedDeviceReaderKeys = true;
					if(entity!=null)
					{
						this.DeviceReaderKeys.Add((DeviceReaderKeyEntity)entity);
					}
					break;
				case "DeviceReaderToCerts":
					_alreadyFetchedDeviceReaderToCerts = true;
					if(entity!=null)
					{
						this.DeviceReaderToCerts.Add((DeviceReaderToCertEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceCommunicationHistories":
					_deviceCommunicationHistories.Add((DeviceCommunicationHistoryEntity)relatedEntity);
					break;
				case "DeviceReaderCryptoResources":
					_deviceReaderCryptoResources.Add((DeviceReaderCryptoResourceEntity)relatedEntity);
					break;
				case "TargetDeviceReaderJobs":
					_targetDeviceReaderJobs.Add((DeviceReaderJobEntity)relatedEntity);
					break;
				case "WorkerDeviceReaderJobs":
					_workerDeviceReaderJobs.Add((DeviceReaderJobEntity)relatedEntity);
					break;
				case "DeviceReaderKeys":
					_deviceReaderKeys.Add((DeviceReaderKeyEntity)relatedEntity);
					break;
				case "DeviceReaderToCerts":
					_deviceReaderToCerts.Add((DeviceReaderToCertEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceCommunicationHistories":
					this.PerformRelatedEntityRemoval(_deviceCommunicationHistories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceReaderCryptoResources":
					this.PerformRelatedEntityRemoval(_deviceReaderCryptoResources, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TargetDeviceReaderJobs":
					this.PerformRelatedEntityRemoval(_targetDeviceReaderJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WorkerDeviceReaderJobs":
					this.PerformRelatedEntityRemoval(_workerDeviceReaderJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceReaderKeys":
					this.PerformRelatedEntityRemoval(_deviceReaderKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceReaderToCerts":
					this.PerformRelatedEntityRemoval(_deviceReaderToCerts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_deviceCommunicationHistories);
			toReturn.Add(_deviceReaderCryptoResources);
			toReturn.Add(_targetDeviceReaderJobs);
			toReturn.Add(_workerDeviceReaderJobs);
			toReturn.Add(_deviceReaderKeys);
			toReturn.Add(_deviceReaderToCerts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderID)
		{
			return FetchUsingPK(deviceReaderID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceReaderID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceReaderID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceReaderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceReaderID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceReaderID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceReaderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DeviceCommunicationHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceCommunicationHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection GetMultiDeviceCommunicationHistories(bool forceFetch)
		{
			return GetMultiDeviceCommunicationHistories(forceFetch, _deviceCommunicationHistories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceCommunicationHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceCommunicationHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection GetMultiDeviceCommunicationHistories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceCommunicationHistories(forceFetch, _deviceCommunicationHistories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceCommunicationHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection GetMultiDeviceCommunicationHistories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceCommunicationHistories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceCommunicationHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection GetMultiDeviceCommunicationHistories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceCommunicationHistories || forceFetch || _alwaysFetchDeviceCommunicationHistories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceCommunicationHistories);
				_deviceCommunicationHistories.SuppressClearInGetMulti=!forceFetch;
				_deviceCommunicationHistories.EntityFactoryToUse = entityFactoryToUse;
				_deviceCommunicationHistories.GetMultiManyToOne(this, filter);
				_deviceCommunicationHistories.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceCommunicationHistories = true;
			}
			return _deviceCommunicationHistories;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceCommunicationHistories'. These settings will be taken into account
		/// when the property DeviceCommunicationHistories is requested or GetMultiDeviceCommunicationHistories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceCommunicationHistories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceCommunicationHistories.SortClauses=sortClauses;
			_deviceCommunicationHistories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderCryptoResourceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, _deviceReaderCryptoResources.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderCryptoResourceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, _deviceReaderCryptoResources.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderCryptoResources(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection GetMultiDeviceReaderCryptoResources(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderCryptoResources || forceFetch || _alwaysFetchDeviceReaderCryptoResources) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderCryptoResources);
				_deviceReaderCryptoResources.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderCryptoResources.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderCryptoResources.GetMultiManyToOne(null, this, filter);
				_deviceReaderCryptoResources.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderCryptoResources = true;
			}
			return _deviceReaderCryptoResources;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderCryptoResources'. These settings will be taken into account
		/// when the property DeviceReaderCryptoResources is requested or GetMultiDeviceReaderCryptoResources is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderCryptoResources(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderCryptoResources.SortClauses=sortClauses;
			_deviceReaderCryptoResources.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiTargetDeviceReaderJobs(bool forceFetch)
		{
			return GetMultiTargetDeviceReaderJobs(forceFetch, _targetDeviceReaderJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiTargetDeviceReaderJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTargetDeviceReaderJobs(forceFetch, _targetDeviceReaderJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiTargetDeviceReaderJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTargetDeviceReaderJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiTargetDeviceReaderJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTargetDeviceReaderJobs || forceFetch || _alwaysFetchTargetDeviceReaderJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_targetDeviceReaderJobs);
				_targetDeviceReaderJobs.SuppressClearInGetMulti=!forceFetch;
				_targetDeviceReaderJobs.EntityFactoryToUse = entityFactoryToUse;
				_targetDeviceReaderJobs.GetMultiManyToOne(this, null, filter);
				_targetDeviceReaderJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedTargetDeviceReaderJobs = true;
			}
			return _targetDeviceReaderJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'TargetDeviceReaderJobs'. These settings will be taken into account
		/// when the property TargetDeviceReaderJobs is requested or GetMultiTargetDeviceReaderJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTargetDeviceReaderJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_targetDeviceReaderJobs.SortClauses=sortClauses;
			_targetDeviceReaderJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiWorkerDeviceReaderJobs(bool forceFetch)
		{
			return GetMultiWorkerDeviceReaderJobs(forceFetch, _workerDeviceReaderJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiWorkerDeviceReaderJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkerDeviceReaderJobs(forceFetch, _workerDeviceReaderJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiWorkerDeviceReaderJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkerDeviceReaderJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection GetMultiWorkerDeviceReaderJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkerDeviceReaderJobs || forceFetch || _alwaysFetchWorkerDeviceReaderJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workerDeviceReaderJobs);
				_workerDeviceReaderJobs.SuppressClearInGetMulti=!forceFetch;
				_workerDeviceReaderJobs.EntityFactoryToUse = entityFactoryToUse;
				_workerDeviceReaderJobs.GetMultiManyToOne(null, this, filter);
				_workerDeviceReaderJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkerDeviceReaderJobs = true;
			}
			return _workerDeviceReaderJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkerDeviceReaderJobs'. These settings will be taken into account
		/// when the property WorkerDeviceReaderJobs is requested or GetMultiWorkerDeviceReaderJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkerDeviceReaderJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workerDeviceReaderJobs.SortClauses=sortClauses;
			_workerDeviceReaderJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection GetMultiDeviceReaderKeys(bool forceFetch)
		{
			return GetMultiDeviceReaderKeys(forceFetch, _deviceReaderKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection GetMultiDeviceReaderKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderKeys(forceFetch, _deviceReaderKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection GetMultiDeviceReaderKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection GetMultiDeviceReaderKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderKeys || forceFetch || _alwaysFetchDeviceReaderKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderKeys);
				_deviceReaderKeys.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderKeys.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderKeys.GetMultiManyToOne(this, filter);
				_deviceReaderKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderKeys = true;
			}
			return _deviceReaderKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderKeys'. These settings will be taken into account
		/// when the property DeviceReaderKeys is requested or GetMultiDeviceReaderKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderKeys.SortClauses=sortClauses;
			_deviceReaderKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, _deviceReaderToCerts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceReaderToCertEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, _deviceReaderToCerts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceReaderToCerts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection GetMultiDeviceReaderToCerts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceReaderToCerts || forceFetch || _alwaysFetchDeviceReaderToCerts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceReaderToCerts);
				_deviceReaderToCerts.SuppressClearInGetMulti=!forceFetch;
				_deviceReaderToCerts.EntityFactoryToUse = entityFactoryToUse;
				_deviceReaderToCerts.GetMultiManyToOne(null, this, filter);
				_deviceReaderToCerts.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceReaderToCerts = true;
			}
			return _deviceReaderToCerts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceReaderToCerts'. These settings will be taken into account
		/// when the property DeviceReaderToCerts is requested or GetMultiDeviceReaderToCerts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceReaderToCerts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceReaderToCerts.SortClauses=sortClauses;
			_deviceReaderToCerts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceCommunicationHistories", _deviceCommunicationHistories);
			toReturn.Add("DeviceReaderCryptoResources", _deviceReaderCryptoResources);
			toReturn.Add("TargetDeviceReaderJobs", _targetDeviceReaderJobs);
			toReturn.Add("WorkerDeviceReaderJobs", _workerDeviceReaderJobs);
			toReturn.Add("DeviceReaderKeys", _deviceReaderKeys);
			toReturn.Add("DeviceReaderToCerts", _deviceReaderToCerts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="validator">The validator object for this DeviceReaderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceReaderID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceReaderID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_deviceCommunicationHistories = new VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection();
			_deviceCommunicationHistories.SetContainingEntityInfo(this, "DeviceReader");

			_deviceReaderCryptoResources = new VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection();
			_deviceReaderCryptoResources.SetContainingEntityInfo(this, "DeviceReader");

			_targetDeviceReaderJobs = new VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection();
			_targetDeviceReaderJobs.SetContainingEntityInfo(this, "TargetDeviceReader");

			_workerDeviceReaderJobs = new VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection();
			_workerDeviceReaderJobs.SetContainingEntityInfo(this, "WorkerDeviceReader");

			_deviceReaderKeys = new VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection();
			_deviceReaderKeys.SetContainingEntityInfo(this, "DeviceReader");

			_deviceReaderToCerts = new VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection();
			_deviceReaderToCerts.SetContainingEntityInfo(this, "DeviceReader");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceReaderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceRole", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KEK", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastConnectionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceReaderID">PK value for DeviceReader which data should be fetched into this DeviceReader object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceReaderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceReaderFieldIndex.DeviceReaderID].ForcedCurrentValueWrite(deviceReaderID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceReaderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceReaderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceReaderRelations Relations
		{
			get	{ return new DeviceReaderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceCommunicationHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceCommunicationHistories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection(), (IEntityRelation)GetRelationsForField("DeviceCommunicationHistories")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceCommunicationHistoryEntity, 0, null, null, null, "DeviceCommunicationHistories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderCryptoResource' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderCryptoResources
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderCryptoResources")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity, 0, null, null, null, "DeviceReaderCryptoResources", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetDeviceReaderJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection(), (IEntityRelation)GetRelationsForField("TargetDeviceReaderJobs")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceReaderJobEntity, 0, null, null, null, "TargetDeviceReaderJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkerDeviceReaderJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection(), (IEntityRelation)GetRelationsForField("WorkerDeviceReaderJobs")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceReaderJobEntity, 0, null, null, null, "WorkerDeviceReaderJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderKeys")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceReaderKeyEntity, 0, null, null, null, "DeviceReaderKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceReaderToCert' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceReaderToCerts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection(), (IEntityRelation)GetRelationsForField("DeviceReaderToCerts")[0], (int)VarioSL.Entities.EntityType.DeviceReaderEntity, (int)VarioSL.Entities.EntityType.DeviceReaderToCertEntity, 0, null, null, null, "DeviceReaderToCerts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DeviceReaderFieldIndex.Description, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceReaderFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DeviceNo property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceReaderFieldIndex.DeviceNo, false); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceNo, value, true); }
		}

		/// <summary> The DeviceReaderID property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICEREADERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceReaderID
		{
			get { return (System.Int64)GetValue((int)DeviceReaderFieldIndex.DeviceReaderID, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceReaderID, value, true); }
		}

		/// <summary> The DeviceReaderNumber property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICEREADERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceReaderNumber
		{
			get { return (System.Int32)GetValue((int)DeviceReaderFieldIndex.DeviceReaderNumber, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceReaderNumber, value, true); }
		}

		/// <summary> The DeviceRole property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICEROLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceRole
		{
			get { return (System.Int32)GetValue((int)DeviceReaderFieldIndex.DeviceRole, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceRole, value, true); }
		}

		/// <summary> The DeviceStatus property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."DEVICESTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceStatus
		{
			get { return (System.Int32)GetValue((int)DeviceReaderFieldIndex.DeviceStatus, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.DeviceStatus, value, true); }
		}

		/// <summary> The KEK property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."KEK"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String KEK
		{
			get { return (System.String)GetValue((int)DeviceReaderFieldIndex.KEK, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.KEK, value, true); }
		}

		/// <summary> The LastConnectionDate property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."LASTCONNECTIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastConnectionDate
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderFieldIndex.LastConnectionDate, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.LastConnectionDate, value, true); }
		}

		/// <summary> The LastModified property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DeviceReaderFieldIndex.LastModified, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DeviceReaderFieldIndex.LastUser, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeviceReaderFieldIndex.Name, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.Name, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DeviceReader<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DEVICEREADER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DeviceReaderFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DeviceReaderFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DeviceCommunicationHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceCommunicationHistories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCommunicationHistoryCollection DeviceCommunicationHistories
		{
			get	{ return GetMultiDeviceCommunicationHistories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceCommunicationHistories. When set to true, DeviceCommunicationHistories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceCommunicationHistories is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceCommunicationHistories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceCommunicationHistories
		{
			get	{ return _alwaysFetchDeviceCommunicationHistories; }
			set	{ _alwaysFetchDeviceCommunicationHistories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceCommunicationHistories already has been fetched. Setting this property to false when DeviceCommunicationHistories has been fetched
		/// will clear the DeviceCommunicationHistories collection well. Setting this property to true while DeviceCommunicationHistories hasn't been fetched disables lazy loading for DeviceCommunicationHistories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceCommunicationHistories
		{
			get { return _alreadyFetchedDeviceCommunicationHistories;}
			set 
			{
				if(_alreadyFetchedDeviceCommunicationHistories && !value && (_deviceCommunicationHistories != null))
				{
					_deviceCommunicationHistories.Clear();
				}
				_alreadyFetchedDeviceCommunicationHistories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderCryptoResourceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderCryptoResources()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderCryptoResourceCollection DeviceReaderCryptoResources
		{
			get	{ return GetMultiDeviceReaderCryptoResources(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderCryptoResources. When set to true, DeviceReaderCryptoResources is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderCryptoResources is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderCryptoResources(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderCryptoResources
		{
			get	{ return _alwaysFetchDeviceReaderCryptoResources; }
			set	{ _alwaysFetchDeviceReaderCryptoResources = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderCryptoResources already has been fetched. Setting this property to false when DeviceReaderCryptoResources has been fetched
		/// will clear the DeviceReaderCryptoResources collection well. Setting this property to true while DeviceReaderCryptoResources hasn't been fetched disables lazy loading for DeviceReaderCryptoResources</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderCryptoResources
		{
			get { return _alreadyFetchedDeviceReaderCryptoResources;}
			set 
			{
				if(_alreadyFetchedDeviceReaderCryptoResources && !value && (_deviceReaderCryptoResources != null))
				{
					_deviceReaderCryptoResources.Clear();
				}
				_alreadyFetchedDeviceReaderCryptoResources = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTargetDeviceReaderJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection TargetDeviceReaderJobs
		{
			get	{ return GetMultiTargetDeviceReaderJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TargetDeviceReaderJobs. When set to true, TargetDeviceReaderJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetDeviceReaderJobs is accessed. You can always execute/ a forced fetch by calling GetMultiTargetDeviceReaderJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetDeviceReaderJobs
		{
			get	{ return _alwaysFetchTargetDeviceReaderJobs; }
			set	{ _alwaysFetchTargetDeviceReaderJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TargetDeviceReaderJobs already has been fetched. Setting this property to false when TargetDeviceReaderJobs has been fetched
		/// will clear the TargetDeviceReaderJobs collection well. Setting this property to true while TargetDeviceReaderJobs hasn't been fetched disables lazy loading for TargetDeviceReaderJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetDeviceReaderJobs
		{
			get { return _alreadyFetchedTargetDeviceReaderJobs;}
			set 
			{
				if(_alreadyFetchedTargetDeviceReaderJobs && !value && (_targetDeviceReaderJobs != null))
				{
					_targetDeviceReaderJobs.Clear();
				}
				_alreadyFetchedTargetDeviceReaderJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkerDeviceReaderJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderJobCollection WorkerDeviceReaderJobs
		{
			get	{ return GetMultiWorkerDeviceReaderJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkerDeviceReaderJobs. When set to true, WorkerDeviceReaderJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkerDeviceReaderJobs is accessed. You can always execute/ a forced fetch by calling GetMultiWorkerDeviceReaderJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkerDeviceReaderJobs
		{
			get	{ return _alwaysFetchWorkerDeviceReaderJobs; }
			set	{ _alwaysFetchWorkerDeviceReaderJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkerDeviceReaderJobs already has been fetched. Setting this property to false when WorkerDeviceReaderJobs has been fetched
		/// will clear the WorkerDeviceReaderJobs collection well. Setting this property to true while WorkerDeviceReaderJobs hasn't been fetched disables lazy loading for WorkerDeviceReaderJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkerDeviceReaderJobs
		{
			get { return _alreadyFetchedWorkerDeviceReaderJobs;}
			set 
			{
				if(_alreadyFetchedWorkerDeviceReaderJobs && !value && (_workerDeviceReaderJobs != null))
				{
					_workerDeviceReaderJobs.Clear();
				}
				_alreadyFetchedWorkerDeviceReaderJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderKeyCollection DeviceReaderKeys
		{
			get	{ return GetMultiDeviceReaderKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderKeys. When set to true, DeviceReaderKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderKeys is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderKeys
		{
			get	{ return _alwaysFetchDeviceReaderKeys; }
			set	{ _alwaysFetchDeviceReaderKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderKeys already has been fetched. Setting this property to false when DeviceReaderKeys has been fetched
		/// will clear the DeviceReaderKeys collection well. Setting this property to true while DeviceReaderKeys hasn't been fetched disables lazy loading for DeviceReaderKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderKeys
		{
			get { return _alreadyFetchedDeviceReaderKeys;}
			set 
			{
				if(_alreadyFetchedDeviceReaderKeys && !value && (_deviceReaderKeys != null))
				{
					_deviceReaderKeys.Clear();
				}
				_alreadyFetchedDeviceReaderKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceReaderToCertEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceReaderToCerts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceReaderToCertCollection DeviceReaderToCerts
		{
			get	{ return GetMultiDeviceReaderToCerts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceReaderToCerts. When set to true, DeviceReaderToCerts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceReaderToCerts is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceReaderToCerts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceReaderToCerts
		{
			get	{ return _alwaysFetchDeviceReaderToCerts; }
			set	{ _alwaysFetchDeviceReaderToCerts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceReaderToCerts already has been fetched. Setting this property to false when DeviceReaderToCerts has been fetched
		/// will clear the DeviceReaderToCerts collection well. Setting this property to true while DeviceReaderToCerts hasn't been fetched disables lazy loading for DeviceReaderToCerts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceReaderToCerts
		{
			get { return _alreadyFetchedDeviceReaderToCerts;}
			set 
			{
				if(_alreadyFetchedDeviceReaderToCerts && !value && (_deviceReaderToCerts != null))
				{
					_deviceReaderToCerts.Clear();
				}
				_alreadyFetchedDeviceReaderToCerts = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceReaderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
