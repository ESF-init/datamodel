﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ListLayoutObject'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ListLayoutObjectEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private LayoutEntity _layout;
		private bool	_alwaysFetchLayout, _alreadyFetchedLayout, _layoutReturnsNewIfNotFound;
		private LayoutEntity _page;
		private bool	_alwaysFetchPage, _alreadyFetchedPage, _pageReturnsNewIfNotFound;
		private ListLayoutTypeEntity _listLayoutType;
		private bool	_alwaysFetchListLayoutType, _alreadyFetchedListLayoutType, _listLayoutTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Layout</summary>
			public static readonly string Layout = "Layout";
			/// <summary>Member name Page</summary>
			public static readonly string Page = "Page";
			/// <summary>Member name ListLayoutType</summary>
			public static readonly string ListLayoutType = "ListLayoutType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ListLayoutObjectEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ListLayoutObjectEntity() :base("ListLayoutObjectEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		public ListLayoutObjectEntity(System.Int64 layoutObjectID):base("ListLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ListLayoutObjectEntity(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse):base("ListLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="validator">The custom validator object for this ListLayoutObjectEntity</param>
		public ListLayoutObjectEntity(System.Int64 layoutObjectID, IValidator validator):base("ListLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ListLayoutObjectEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_layout = (LayoutEntity)info.GetValue("_layout", typeof(LayoutEntity));
			if(_layout!=null)
			{
				_layout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_layoutReturnsNewIfNotFound = info.GetBoolean("_layoutReturnsNewIfNotFound");
			_alwaysFetchLayout = info.GetBoolean("_alwaysFetchLayout");
			_alreadyFetchedLayout = info.GetBoolean("_alreadyFetchedLayout");

			_page = (LayoutEntity)info.GetValue("_page", typeof(LayoutEntity));
			if(_page!=null)
			{
				_page.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageReturnsNewIfNotFound = info.GetBoolean("_pageReturnsNewIfNotFound");
			_alwaysFetchPage = info.GetBoolean("_alwaysFetchPage");
			_alreadyFetchedPage = info.GetBoolean("_alreadyFetchedPage");

			_listLayoutType = (ListLayoutTypeEntity)info.GetValue("_listLayoutType", typeof(ListLayoutTypeEntity));
			if(_listLayoutType!=null)
			{
				_listLayoutType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_listLayoutTypeReturnsNewIfNotFound = info.GetBoolean("_listLayoutTypeReturnsNewIfNotFound");
			_alwaysFetchListLayoutType = info.GetBoolean("_alwaysFetchListLayoutType");
			_alreadyFetchedListLayoutType = info.GetBoolean("_alreadyFetchedListLayoutType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ListLayoutObjectFieldIndex)fieldIndex)
			{
				case ListLayoutObjectFieldIndex.LayoutID:
					DesetupSyncLayout(true, false);
					_alreadyFetchedLayout = false;
					break;
				case ListLayoutObjectFieldIndex.LayoutTypeId:
					DesetupSyncListLayoutType(true, false);
					_alreadyFetchedListLayoutType = false;
					break;
				case ListLayoutObjectFieldIndex.PageID:
					DesetupSyncPage(true, false);
					_alreadyFetchedPage = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLayout = (_layout != null);
			_alreadyFetchedPage = (_page != null);
			_alreadyFetchedListLayoutType = (_listLayoutType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Layout":
					toReturn.Add(Relations.LayoutEntityUsingLayoutID);
					break;
				case "Page":
					toReturn.Add(Relations.LayoutEntityUsingPageID);
					break;
				case "ListLayoutType":
					toReturn.Add(Relations.ListLayoutTypeEntityUsingLayoutTypeId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_layout", (!this.MarkedForDeletion?_layout:null));
			info.AddValue("_layoutReturnsNewIfNotFound", _layoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLayout", _alwaysFetchLayout);
			info.AddValue("_alreadyFetchedLayout", _alreadyFetchedLayout);
			info.AddValue("_page", (!this.MarkedForDeletion?_page:null));
			info.AddValue("_pageReturnsNewIfNotFound", _pageReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPage", _alwaysFetchPage);
			info.AddValue("_alreadyFetchedPage", _alreadyFetchedPage);
			info.AddValue("_listLayoutType", (!this.MarkedForDeletion?_listLayoutType:null));
			info.AddValue("_listLayoutTypeReturnsNewIfNotFound", _listLayoutTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchListLayoutType", _alwaysFetchListLayoutType);
			info.AddValue("_alreadyFetchedListLayoutType", _alreadyFetchedListLayoutType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Layout":
					_alreadyFetchedLayout = true;
					this.Layout = (LayoutEntity)entity;
					break;
				case "Page":
					_alreadyFetchedPage = true;
					this.Page = (LayoutEntity)entity;
					break;
				case "ListLayoutType":
					_alreadyFetchedListLayoutType = true;
					this.ListLayoutType = (ListLayoutTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Layout":
					SetupSyncLayout(relatedEntity);
					break;
				case "Page":
					SetupSyncPage(relatedEntity);
					break;
				case "ListLayoutType":
					SetupSyncListLayoutType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Layout":
					DesetupSyncLayout(false, true);
					break;
				case "Page":
					DesetupSyncPage(false, true);
					break;
				case "ListLayoutType":
					DesetupSyncListLayoutType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_layout!=null)
			{
				toReturn.Add(_layout);
			}
			if(_page!=null)
			{
				toReturn.Add(_page);
			}
			if(_listLayoutType!=null)
			{
				toReturn.Add(_listLayoutType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID)
		{
			return FetchUsingPK(layoutObjectID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutObjectID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutObjectID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ListLayoutObjectRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleLayout()
		{
			return GetSingleLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedLayout || forceFetch || _alwaysFetchLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutID);
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_layoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Layout = newEntity;
				_alreadyFetchedLayout = fetchResult;
			}
			return _layout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSinglePage()
		{
			return GetSinglePage(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSinglePage(bool forceFetch)
		{
			if( ( !_alreadyFetchedPage || forceFetch || _alwaysFetchPage) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingPageID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Page = newEntity;
				_alreadyFetchedPage = fetchResult;
			}
			return _page;
		}


		/// <summary> Retrieves the related entity of type 'ListLayoutTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ListLayoutTypeEntity' which is related to this entity.</returns>
		public ListLayoutTypeEntity GetSingleListLayoutType()
		{
			return GetSingleListLayoutType(false);
		}

		/// <summary> Retrieves the related entity of type 'ListLayoutTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ListLayoutTypeEntity' which is related to this entity.</returns>
		public virtual ListLayoutTypeEntity GetSingleListLayoutType(bool forceFetch)
		{
			if( ( !_alreadyFetchedListLayoutType || forceFetch || _alwaysFetchListLayoutType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ListLayoutTypeEntityUsingLayoutTypeId);
				ListLayoutTypeEntity newEntity = new ListLayoutTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutTypeId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ListLayoutTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_listLayoutTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ListLayoutType = newEntity;
				_alreadyFetchedListLayoutType = fetchResult;
			}
			return _listLayoutType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Layout", _layout);
			toReturn.Add("Page", _page);
			toReturn.Add("ListLayoutType", _listLayoutType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="validator">The validator object for this ListLayoutObjectEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutObjectID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutObjectID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_layoutReturnsNewIfNotFound = false;
			_pageReturnsNewIfNotFound = false;
			_listLayoutTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Column", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutTypeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Row", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _layout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, signalRelatedEntity, "ListLayoutObjects", resetFKFields, new int[] { (int)ListLayoutObjectFieldIndex.LayoutID } );		
			_layout = null;
		}
		
		/// <summary> setups the sync logic for member _layout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLayout(IEntityCore relatedEntity)
		{
			if(_layout!=relatedEntity)
			{		
				DesetupSyncLayout(true, true);
				_layout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, ref _alreadyFetchedLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _page</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPage(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _page, new PropertyChangedEventHandler( OnPagePropertyChanged ), "Page", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.LayoutEntityUsingPageIDStatic, true, signalRelatedEntity, "ListLayoutObjectsPage", resetFKFields, new int[] { (int)ListLayoutObjectFieldIndex.PageID } );		
			_page = null;
		}
		
		/// <summary> setups the sync logic for member _page</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPage(IEntityCore relatedEntity)
		{
			if(_page!=relatedEntity)
			{		
				DesetupSyncPage(true, true);
				_page = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _page, new PropertyChangedEventHandler( OnPagePropertyChanged ), "Page", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.LayoutEntityUsingPageIDStatic, true, ref _alreadyFetchedPage, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPagePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _listLayoutType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncListLayoutType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _listLayoutType, new PropertyChangedEventHandler( OnListLayoutTypePropertyChanged ), "ListLayoutType", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.ListLayoutTypeEntityUsingLayoutTypeIdStatic, true, signalRelatedEntity, "ListLayoutObjects", resetFKFields, new int[] { (int)ListLayoutObjectFieldIndex.LayoutTypeId } );		
			_listLayoutType = null;
		}
		
		/// <summary> setups the sync logic for member _listLayoutType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncListLayoutType(IEntityCore relatedEntity)
		{
			if(_listLayoutType!=relatedEntity)
			{		
				DesetupSyncListLayoutType(true, true);
				_listLayoutType = (ListLayoutTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _listLayoutType, new PropertyChangedEventHandler( OnListLayoutTypePropertyChanged ), "ListLayoutType", VarioSL.Entities.RelationClasses.StaticListLayoutObjectRelations.ListLayoutTypeEntityUsingLayoutTypeIdStatic, true, ref _alreadyFetchedListLayoutType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnListLayoutTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutObjectID">PK value for ListLayoutObject which data should be fetched into this ListLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ListLayoutObjectFieldIndex.LayoutObjectID].ForcedCurrentValueWrite(layoutObjectID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateListLayoutObjectDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ListLayoutObjectEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ListLayoutObjectRelations Relations
		{
			get	{ return new ListLayoutObjectRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layout")[0], (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPage
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Page")[0], (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Page", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ListLayoutType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathListLayoutType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ListLayoutTypeCollection(), (IEntityRelation)GetRelationsForField("ListLayoutType")[0], (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, (int)VarioSL.Entities.EntityType.ListLayoutTypeEntity, 0, null, null, null, "ListLayoutType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Column property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."COLUMN_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Column
		{
			get { return (System.Double)GetValue((int)ListLayoutObjectFieldIndex.Column, true); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.Column, value, true); }
		}

		/// <summary> The LayoutID property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)ListLayoutObjectFieldIndex.LayoutID, true); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The LayoutObjectID property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."LAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutObjectID
		{
			get { return (System.Int64)GetValue((int)ListLayoutObjectFieldIndex.LayoutObjectID, true); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.LayoutObjectID, value, true); }
		}

		/// <summary> The LayoutTypeId property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."LISTLAYOUTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TariffListLayoutType> LayoutTypeId
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TariffListLayoutType>)GetValue((int)ListLayoutObjectFieldIndex.LayoutTypeId, false); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.LayoutTypeId, value, true); }
		}

		/// <summary> The Name property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ListLayoutObjectFieldIndex.Name, true); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.Name, value, true); }
		}

		/// <summary> The PageID property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."PAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PageID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ListLayoutObjectFieldIndex.PageID, false); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.PageID, value, true); }
		}

		/// <summary> The Row property of the Entity ListLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LISTLAYOUTOBJECT"."ROW_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Row
		{
			get { return (System.Double)GetValue((int)ListLayoutObjectFieldIndex.Row, true); }
			set	{ SetValue((int)ListLayoutObjectFieldIndex.Row, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity Layout
		{
			get	{ return GetSingleLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ListLayoutObjects", "Layout", _layout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Layout. When set to true, Layout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layout is accessed. You can always execute a forced fetch by calling GetSingleLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayout
		{
			get	{ return _alwaysFetchLayout; }
			set	{ _alwaysFetchLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layout already has been fetched. Setting this property to false when Layout has been fetched
		/// will set Layout to null as well. Setting this property to true while Layout hasn't been fetched disables lazy loading for Layout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayout
		{
			get { return _alreadyFetchedLayout;}
			set 
			{
				if(_alreadyFetchedLayout && !value)
				{
					this.Layout = null;
				}
				_alreadyFetchedLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Layout is not found
		/// in the database. When set to true, Layout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LayoutReturnsNewIfNotFound
		{
			get	{ return _layoutReturnsNewIfNotFound; }
			set { _layoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity Page
		{
			get	{ return GetSinglePage(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPage(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ListLayoutObjectsPage", "Page", _page, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Page. When set to true, Page is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Page is accessed. You can always execute a forced fetch by calling GetSinglePage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPage
		{
			get	{ return _alwaysFetchPage; }
			set	{ _alwaysFetchPage = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Page already has been fetched. Setting this property to false when Page has been fetched
		/// will set Page to null as well. Setting this property to true while Page hasn't been fetched disables lazy loading for Page</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPage
		{
			get { return _alreadyFetchedPage;}
			set 
			{
				if(_alreadyFetchedPage && !value)
				{
					this.Page = null;
				}
				_alreadyFetchedPage = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Page is not found
		/// in the database. When set to true, Page will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PageReturnsNewIfNotFound
		{
			get	{ return _pageReturnsNewIfNotFound; }
			set { _pageReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ListLayoutTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleListLayoutType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ListLayoutTypeEntity ListLayoutType
		{
			get	{ return GetSingleListLayoutType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncListLayoutType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ListLayoutObjects", "ListLayoutType", _listLayoutType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ListLayoutType. When set to true, ListLayoutType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ListLayoutType is accessed. You can always execute a forced fetch by calling GetSingleListLayoutType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchListLayoutType
		{
			get	{ return _alwaysFetchListLayoutType; }
			set	{ _alwaysFetchListLayoutType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ListLayoutType already has been fetched. Setting this property to false when ListLayoutType has been fetched
		/// will set ListLayoutType to null as well. Setting this property to true while ListLayoutType hasn't been fetched disables lazy loading for ListLayoutType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedListLayoutType
		{
			get { return _alreadyFetchedListLayoutType;}
			set 
			{
				if(_alreadyFetchedListLayoutType && !value)
				{
					this.ListLayoutType = null;
				}
				_alreadyFetchedListLayoutType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ListLayoutType is not found
		/// in the database. When set to true, ListLayoutType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ListLayoutTypeReturnsNewIfNotFound
		{
			get	{ return _listLayoutTypeReturnsNewIfNotFound; }
			set { _listLayoutTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
