﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CryptoContent'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CryptoContentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CryptoCryptogramArchiveEntity _cryptoCryptogramArchive;
		private bool	_alwaysFetchCryptoCryptogramArchive, _alreadyFetchedCryptoCryptogramArchive, _cryptoCryptogramArchiveReturnsNewIfNotFound;
		private CryptoResourceEntity _cryptoResourceCryptogram;
		private bool	_alwaysFetchCryptoResourceCryptogram, _alreadyFetchedCryptoResourceCryptogram, _cryptoResourceCryptogramReturnsNewIfNotFound;
		private CryptoResourceEntity _cryptoResourceSignCertificate;
		private bool	_alwaysFetchCryptoResourceSignCertificate, _alreadyFetchedCryptoResourceSignCertificate, _cryptoResourceSignCertificateReturnsNewIfNotFound;
		private CryptoResourceEntity _cryptoResourceSubCertificate;
		private bool	_alwaysFetchCryptoResourceSubCertificate, _alreadyFetchedCryptoResourceSubCertificate, _cryptoResourceSubCertificateReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CryptoCryptogramArchive</summary>
			public static readonly string CryptoCryptogramArchive = "CryptoCryptogramArchive";
			/// <summary>Member name CryptoResourceCryptogram</summary>
			public static readonly string CryptoResourceCryptogram = "CryptoResourceCryptogram";
			/// <summary>Member name CryptoResourceSignCertificate</summary>
			public static readonly string CryptoResourceSignCertificate = "CryptoResourceSignCertificate";
			/// <summary>Member name CryptoResourceSubCertificate</summary>
			public static readonly string CryptoResourceSubCertificate = "CryptoResourceSubCertificate";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CryptoContentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CryptoContentEntity() :base("CryptoContentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		public CryptoContentEntity(System.Int64 contentID):base("CryptoContentEntity")
		{
			InitClassFetch(contentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CryptoContentEntity(System.Int64 contentID, IPrefetchPath prefetchPathToUse):base("CryptoContentEntity")
		{
			InitClassFetch(contentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="validator">The custom validator object for this CryptoContentEntity</param>
		public CryptoContentEntity(System.Int64 contentID, IValidator validator):base("CryptoContentEntity")
		{
			InitClassFetch(contentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CryptoContentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cryptoCryptogramArchive = (CryptoCryptogramArchiveEntity)info.GetValue("_cryptoCryptogramArchive", typeof(CryptoCryptogramArchiveEntity));
			if(_cryptoCryptogramArchive!=null)
			{
				_cryptoCryptogramArchive.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoCryptogramArchiveReturnsNewIfNotFound = info.GetBoolean("_cryptoCryptogramArchiveReturnsNewIfNotFound");
			_alwaysFetchCryptoCryptogramArchive = info.GetBoolean("_alwaysFetchCryptoCryptogramArchive");
			_alreadyFetchedCryptoCryptogramArchive = info.GetBoolean("_alreadyFetchedCryptoCryptogramArchive");

			_cryptoResourceCryptogram = (CryptoResourceEntity)info.GetValue("_cryptoResourceCryptogram", typeof(CryptoResourceEntity));
			if(_cryptoResourceCryptogram!=null)
			{
				_cryptoResourceCryptogram.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoResourceCryptogramReturnsNewIfNotFound = info.GetBoolean("_cryptoResourceCryptogramReturnsNewIfNotFound");
			_alwaysFetchCryptoResourceCryptogram = info.GetBoolean("_alwaysFetchCryptoResourceCryptogram");
			_alreadyFetchedCryptoResourceCryptogram = info.GetBoolean("_alreadyFetchedCryptoResourceCryptogram");

			_cryptoResourceSignCertificate = (CryptoResourceEntity)info.GetValue("_cryptoResourceSignCertificate", typeof(CryptoResourceEntity));
			if(_cryptoResourceSignCertificate!=null)
			{
				_cryptoResourceSignCertificate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoResourceSignCertificateReturnsNewIfNotFound = info.GetBoolean("_cryptoResourceSignCertificateReturnsNewIfNotFound");
			_alwaysFetchCryptoResourceSignCertificate = info.GetBoolean("_alwaysFetchCryptoResourceSignCertificate");
			_alreadyFetchedCryptoResourceSignCertificate = info.GetBoolean("_alreadyFetchedCryptoResourceSignCertificate");

			_cryptoResourceSubCertificate = (CryptoResourceEntity)info.GetValue("_cryptoResourceSubCertificate", typeof(CryptoResourceEntity));
			if(_cryptoResourceSubCertificate!=null)
			{
				_cryptoResourceSubCertificate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoResourceSubCertificateReturnsNewIfNotFound = info.GetBoolean("_cryptoResourceSubCertificateReturnsNewIfNotFound");
			_alwaysFetchCryptoResourceSubCertificate = info.GetBoolean("_alwaysFetchCryptoResourceSubCertificate");
			_alreadyFetchedCryptoResourceSubCertificate = info.GetBoolean("_alreadyFetchedCryptoResourceSubCertificate");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CryptoContentFieldIndex)fieldIndex)
			{
				case CryptoContentFieldIndex.CryptogramArchiveID:
					DesetupSyncCryptoCryptogramArchive(true, false);
					_alreadyFetchedCryptoCryptogramArchive = false;
					break;
				case CryptoContentFieldIndex.CryptogramResourceID:
					DesetupSyncCryptoResourceCryptogram(true, false);
					_alreadyFetchedCryptoResourceCryptogram = false;
					break;
				case CryptoContentFieldIndex.SignCertificateResourceID:
					DesetupSyncCryptoResourceSignCertificate(true, false);
					_alreadyFetchedCryptoResourceSignCertificate = false;
					break;
				case CryptoContentFieldIndex.SubCertificateResourceID:
					DesetupSyncCryptoResourceSubCertificate(true, false);
					_alreadyFetchedCryptoResourceSubCertificate = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCryptoCryptogramArchive = (_cryptoCryptogramArchive != null);
			_alreadyFetchedCryptoResourceCryptogram = (_cryptoResourceCryptogram != null);
			_alreadyFetchedCryptoResourceSignCertificate = (_cryptoResourceSignCertificate != null);
			_alreadyFetchedCryptoResourceSubCertificate = (_cryptoResourceSubCertificate != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CryptoCryptogramArchive":
					toReturn.Add(Relations.CryptoCryptogramArchiveEntityUsingCryptogramArchiveID);
					break;
				case "CryptoResourceCryptogram":
					toReturn.Add(Relations.CryptoResourceEntityUsingCryptogramResourceID);
					break;
				case "CryptoResourceSignCertificate":
					toReturn.Add(Relations.CryptoResourceEntityUsingSignCertificateResourceID);
					break;
				case "CryptoResourceSubCertificate":
					toReturn.Add(Relations.CryptoResourceEntityUsingSubCertificateResourceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cryptoCryptogramArchive", (!this.MarkedForDeletion?_cryptoCryptogramArchive:null));
			info.AddValue("_cryptoCryptogramArchiveReturnsNewIfNotFound", _cryptoCryptogramArchiveReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoCryptogramArchive", _alwaysFetchCryptoCryptogramArchive);
			info.AddValue("_alreadyFetchedCryptoCryptogramArchive", _alreadyFetchedCryptoCryptogramArchive);
			info.AddValue("_cryptoResourceCryptogram", (!this.MarkedForDeletion?_cryptoResourceCryptogram:null));
			info.AddValue("_cryptoResourceCryptogramReturnsNewIfNotFound", _cryptoResourceCryptogramReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoResourceCryptogram", _alwaysFetchCryptoResourceCryptogram);
			info.AddValue("_alreadyFetchedCryptoResourceCryptogram", _alreadyFetchedCryptoResourceCryptogram);
			info.AddValue("_cryptoResourceSignCertificate", (!this.MarkedForDeletion?_cryptoResourceSignCertificate:null));
			info.AddValue("_cryptoResourceSignCertificateReturnsNewIfNotFound", _cryptoResourceSignCertificateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoResourceSignCertificate", _alwaysFetchCryptoResourceSignCertificate);
			info.AddValue("_alreadyFetchedCryptoResourceSignCertificate", _alreadyFetchedCryptoResourceSignCertificate);
			info.AddValue("_cryptoResourceSubCertificate", (!this.MarkedForDeletion?_cryptoResourceSubCertificate:null));
			info.AddValue("_cryptoResourceSubCertificateReturnsNewIfNotFound", _cryptoResourceSubCertificateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoResourceSubCertificate", _alwaysFetchCryptoResourceSubCertificate);
			info.AddValue("_alreadyFetchedCryptoResourceSubCertificate", _alreadyFetchedCryptoResourceSubCertificate);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CryptoCryptogramArchive":
					_alreadyFetchedCryptoCryptogramArchive = true;
					this.CryptoCryptogramArchive = (CryptoCryptogramArchiveEntity)entity;
					break;
				case "CryptoResourceCryptogram":
					_alreadyFetchedCryptoResourceCryptogram = true;
					this.CryptoResourceCryptogram = (CryptoResourceEntity)entity;
					break;
				case "CryptoResourceSignCertificate":
					_alreadyFetchedCryptoResourceSignCertificate = true;
					this.CryptoResourceSignCertificate = (CryptoResourceEntity)entity;
					break;
				case "CryptoResourceSubCertificate":
					_alreadyFetchedCryptoResourceSubCertificate = true;
					this.CryptoResourceSubCertificate = (CryptoResourceEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CryptoCryptogramArchive":
					SetupSyncCryptoCryptogramArchive(relatedEntity);
					break;
				case "CryptoResourceCryptogram":
					SetupSyncCryptoResourceCryptogram(relatedEntity);
					break;
				case "CryptoResourceSignCertificate":
					SetupSyncCryptoResourceSignCertificate(relatedEntity);
					break;
				case "CryptoResourceSubCertificate":
					SetupSyncCryptoResourceSubCertificate(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CryptoCryptogramArchive":
					DesetupSyncCryptoCryptogramArchive(false, true);
					break;
				case "CryptoResourceCryptogram":
					DesetupSyncCryptoResourceCryptogram(false, true);
					break;
				case "CryptoResourceSignCertificate":
					DesetupSyncCryptoResourceSignCertificate(false, true);
					break;
				case "CryptoResourceSubCertificate":
					DesetupSyncCryptoResourceSubCertificate(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cryptoCryptogramArchive!=null)
			{
				toReturn.Add(_cryptoCryptogramArchive);
			}
			if(_cryptoResourceCryptogram!=null)
			{
				toReturn.Add(_cryptoResourceCryptogram);
			}
			if(_cryptoResourceSignCertificate!=null)
			{
				toReturn.Add(_cryptoResourceSignCertificate);
			}
			if(_cryptoResourceSubCertificate!=null)
			{
				toReturn.Add(_cryptoResourceSubCertificate);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="keyLoadCounter">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="shortSamNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCKeyLoadCounterShortSamNumber(System.Int32 keyLoadCounter, System.String shortSamNumber)
		{
			return FetchUsingUCKeyLoadCounterShortSamNumber( keyLoadCounter,  shortSamNumber, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="keyLoadCounter">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="shortSamNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCKeyLoadCounterShortSamNumber(System.Int32 keyLoadCounter, System.String shortSamNumber, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCKeyLoadCounterShortSamNumber( keyLoadCounter,  shortSamNumber, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="keyLoadCounter">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="shortSamNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCKeyLoadCounterShortSamNumber(System.Int32 keyLoadCounter, System.String shortSamNumber, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCKeyLoadCounterShortSamNumber( keyLoadCounter,  shortSamNumber, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="keyLoadCounter">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="shortSamNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCKeyLoadCounterShortSamNumber(System.Int32 keyLoadCounter, System.String shortSamNumber, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CryptoContentDAO)CreateDAOInstance()).FetchCryptoContentUsingUCKeyLoadCounterShortSamNumber(this, this.Transaction, keyLoadCounter, shortSamNumber, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contentID)
		{
			return FetchUsingPK(contentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(contentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(contentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(contentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ContentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CryptoContentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CryptoCryptogramArchiveEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoCryptogramArchiveEntity' which is related to this entity.</returns>
		public CryptoCryptogramArchiveEntity GetSingleCryptoCryptogramArchive()
		{
			return GetSingleCryptoCryptogramArchive(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoCryptogramArchiveEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoCryptogramArchiveEntity' which is related to this entity.</returns>
		public virtual CryptoCryptogramArchiveEntity GetSingleCryptoCryptogramArchive(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoCryptogramArchive || forceFetch || _alwaysFetchCryptoCryptogramArchive) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoCryptogramArchiveEntityUsingCryptogramArchiveID);
				CryptoCryptogramArchiveEntity newEntity = new CryptoCryptogramArchiveEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CryptogramArchiveID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoCryptogramArchiveEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoCryptogramArchiveReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoCryptogramArchive = newEntity;
				_alreadyFetchedCryptoCryptogramArchive = fetchResult;
			}
			return _cryptoCryptogramArchive;
		}


		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public CryptoResourceEntity GetSingleCryptoResourceCryptogram()
		{
			return GetSingleCryptoResourceCryptogram(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public virtual CryptoResourceEntity GetSingleCryptoResourceCryptogram(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoResourceCryptogram || forceFetch || _alwaysFetchCryptoResourceCryptogram) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoResourceEntityUsingCryptogramResourceID);
				CryptoResourceEntity newEntity = new CryptoResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CryptogramResourceID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoResourceCryptogramReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoResourceCryptogram = newEntity;
				_alreadyFetchedCryptoResourceCryptogram = fetchResult;
			}
			return _cryptoResourceCryptogram;
		}


		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public CryptoResourceEntity GetSingleCryptoResourceSignCertificate()
		{
			return GetSingleCryptoResourceSignCertificate(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public virtual CryptoResourceEntity GetSingleCryptoResourceSignCertificate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoResourceSignCertificate || forceFetch || _alwaysFetchCryptoResourceSignCertificate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoResourceEntityUsingSignCertificateResourceID);
				CryptoResourceEntity newEntity = new CryptoResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SignCertificateResourceID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoResourceSignCertificateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoResourceSignCertificate = newEntity;
				_alreadyFetchedCryptoResourceSignCertificate = fetchResult;
			}
			return _cryptoResourceSignCertificate;
		}


		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public CryptoResourceEntity GetSingleCryptoResourceSubCertificate()
		{
			return GetSingleCryptoResourceSubCertificate(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoResourceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoResourceEntity' which is related to this entity.</returns>
		public virtual CryptoResourceEntity GetSingleCryptoResourceSubCertificate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoResourceSubCertificate || forceFetch || _alwaysFetchCryptoResourceSubCertificate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoResourceEntityUsingSubCertificateResourceID);
				CryptoResourceEntity newEntity = new CryptoResourceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SubCertificateResourceID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoResourceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoResourceSubCertificateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoResourceSubCertificate = newEntity;
				_alreadyFetchedCryptoResourceSubCertificate = fetchResult;
			}
			return _cryptoResourceSubCertificate;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CryptoCryptogramArchive", _cryptoCryptogramArchive);
			toReturn.Add("CryptoResourceCryptogram", _cryptoResourceCryptogram);
			toReturn.Add("CryptoResourceSignCertificate", _cryptoResourceSignCertificate);
			toReturn.Add("CryptoResourceSubCertificate", _cryptoResourceSubCertificate);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="validator">The validator object for this CryptoContentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 contentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(contentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cryptoCryptogramArchiveReturnsNewIfNotFound = false;
			_cryptoResourceCryptogramReturnsNewIfNotFound = false;
			_cryptoResourceSignCertificateReturnsNewIfNotFound = false;
			_cryptoResourceSubCertificateReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptogramArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptogramFilename", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptogramResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ErrorType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyLoadCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortSamNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SignCertificateFilename", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SignCertificateResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubCertificateFilename", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubCertificateResourceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cryptoCryptogramArchive</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoCryptogramArchive(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoCryptogramArchive, new PropertyChangedEventHandler( OnCryptoCryptogramArchivePropertyChanged ), "CryptoCryptogramArchive", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoCryptogramArchiveEntityUsingCryptogramArchiveIDStatic, true, signalRelatedEntity, "CryptoContents", resetFKFields, new int[] { (int)CryptoContentFieldIndex.CryptogramArchiveID } );		
			_cryptoCryptogramArchive = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoCryptogramArchive</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoCryptogramArchive(IEntityCore relatedEntity)
		{
			if(_cryptoCryptogramArchive!=relatedEntity)
			{		
				DesetupSyncCryptoCryptogramArchive(true, true);
				_cryptoCryptogramArchive = (CryptoCryptogramArchiveEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoCryptogramArchive, new PropertyChangedEventHandler( OnCryptoCryptogramArchivePropertyChanged ), "CryptoCryptogramArchive", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoCryptogramArchiveEntityUsingCryptogramArchiveIDStatic, true, ref _alreadyFetchedCryptoCryptogramArchive, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoCryptogramArchivePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cryptoResourceCryptogram</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoResourceCryptogram(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoResourceCryptogram, new PropertyChangedEventHandler( OnCryptoResourceCryptogramPropertyChanged ), "CryptoResourceCryptogram", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingCryptogramResourceIDStatic, true, signalRelatedEntity, "CryptoContentCryptograms", resetFKFields, new int[] { (int)CryptoContentFieldIndex.CryptogramResourceID } );		
			_cryptoResourceCryptogram = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoResourceCryptogram</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoResourceCryptogram(IEntityCore relatedEntity)
		{
			if(_cryptoResourceCryptogram!=relatedEntity)
			{		
				DesetupSyncCryptoResourceCryptogram(true, true);
				_cryptoResourceCryptogram = (CryptoResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoResourceCryptogram, new PropertyChangedEventHandler( OnCryptoResourceCryptogramPropertyChanged ), "CryptoResourceCryptogram", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingCryptogramResourceIDStatic, true, ref _alreadyFetchedCryptoResourceCryptogram, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoResourceCryptogramPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cryptoResourceSignCertificate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoResourceSignCertificate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoResourceSignCertificate, new PropertyChangedEventHandler( OnCryptoResourceSignCertificatePropertyChanged ), "CryptoResourceSignCertificate", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingSignCertificateResourceIDStatic, true, signalRelatedEntity, "CryptoContentSignCertificates", resetFKFields, new int[] { (int)CryptoContentFieldIndex.SignCertificateResourceID } );		
			_cryptoResourceSignCertificate = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoResourceSignCertificate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoResourceSignCertificate(IEntityCore relatedEntity)
		{
			if(_cryptoResourceSignCertificate!=relatedEntity)
			{		
				DesetupSyncCryptoResourceSignCertificate(true, true);
				_cryptoResourceSignCertificate = (CryptoResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoResourceSignCertificate, new PropertyChangedEventHandler( OnCryptoResourceSignCertificatePropertyChanged ), "CryptoResourceSignCertificate", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingSignCertificateResourceIDStatic, true, ref _alreadyFetchedCryptoResourceSignCertificate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoResourceSignCertificatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cryptoResourceSubCertificate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoResourceSubCertificate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoResourceSubCertificate, new PropertyChangedEventHandler( OnCryptoResourceSubCertificatePropertyChanged ), "CryptoResourceSubCertificate", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingSubCertificateResourceIDStatic, true, signalRelatedEntity, "CryptoContentSubCertificates", resetFKFields, new int[] { (int)CryptoContentFieldIndex.SubCertificateResourceID } );		
			_cryptoResourceSubCertificate = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoResourceSubCertificate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoResourceSubCertificate(IEntityCore relatedEntity)
		{
			if(_cryptoResourceSubCertificate!=relatedEntity)
			{		
				DesetupSyncCryptoResourceSubCertificate(true, true);
				_cryptoResourceSubCertificate = (CryptoResourceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoResourceSubCertificate, new PropertyChangedEventHandler( OnCryptoResourceSubCertificatePropertyChanged ), "CryptoResourceSubCertificate", VarioSL.Entities.RelationClasses.StaticCryptoContentRelations.CryptoResourceEntityUsingSubCertificateResourceIDStatic, true, ref _alreadyFetchedCryptoResourceSubCertificate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoResourceSubCertificatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="contentID">PK value for CryptoContent which data should be fetched into this CryptoContent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 contentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CryptoContentFieldIndex.ContentID].ForcedCurrentValueWrite(contentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCryptoContentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CryptoContentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CryptoContentRelations Relations
		{
			get	{ return new CryptoContentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoCryptogramArchive'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoCryptogramArchive
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoCryptogramArchiveCollection(), (IEntityRelation)GetRelationsForField("CryptoCryptogramArchive")[0], (int)VarioSL.Entities.EntityType.CryptoContentEntity, (int)VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity, 0, null, null, null, "CryptoCryptogramArchive", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoResourceCryptogram
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoResourceCollection(), (IEntityRelation)GetRelationsForField("CryptoResourceCryptogram")[0], (int)VarioSL.Entities.EntityType.CryptoContentEntity, (int)VarioSL.Entities.EntityType.CryptoResourceEntity, 0, null, null, null, "CryptoResourceCryptogram", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoResourceSignCertificate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoResourceCollection(), (IEntityRelation)GetRelationsForField("CryptoResourceSignCertificate")[0], (int)VarioSL.Entities.EntityType.CryptoContentEntity, (int)VarioSL.Entities.EntityType.CryptoResourceEntity, 0, null, null, null, "CryptoResourceSignCertificate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoResource'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoResourceSubCertificate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoResourceCollection(), (IEntityRelation)GetRelationsForField("CryptoResourceSubCertificate")[0], (int)VarioSL.Entities.EntityType.CryptoContentEntity, (int)VarioSL.Entities.EntityType.CryptoResourceEntity, 0, null, null, null, "CryptoResourceSubCertificate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContentID property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."CONTENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ContentID
		{
			get { return (System.Int64)GetValue((int)CryptoContentFieldIndex.ContentID, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.ContentID, value, true); }
		}

		/// <summary> The CryptogramArchiveID property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."CRYPTOGRAMARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CryptogramArchiveID
		{
			get { return (System.Int64)GetValue((int)CryptoContentFieldIndex.CryptogramArchiveID, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.CryptogramArchiveID, value, true); }
		}

		/// <summary> The CryptogramFilename property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."CRYPTOGRAMFILENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CryptogramFilename
		{
			get { return (System.String)GetValue((int)CryptoContentFieldIndex.CryptogramFilename, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.CryptogramFilename, value, true); }
		}

		/// <summary> The CryptogramResourceID property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."CRYPTOGRAMRESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CryptogramResourceID
		{
			get { return (System.Int64)GetValue((int)CryptoContentFieldIndex.CryptogramResourceID, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.CryptogramResourceID, value, true); }
		}

		/// <summary> The ErrorType property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."ERRORTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CryptogramErrorType ErrorType
		{
			get { return (VarioSL.Entities.Enumerations.CryptogramErrorType)GetValue((int)CryptoContentFieldIndex.ErrorType, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.ErrorType, value, true); }
		}

		/// <summary> The KeyLoadCounter property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."KEYLOADCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 KeyLoadCounter
		{
			get { return (System.Int32)GetValue((int)CryptoContentFieldIndex.KeyLoadCounter, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.KeyLoadCounter, value, true); }
		}

		/// <summary> The LastModified property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CryptoContentFieldIndex.LastModified, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CryptoContentFieldIndex.LastUser, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LoadStatus property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."LOADSTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CryptogramLoadStatus LoadStatus
		{
			get { return (VarioSL.Entities.Enumerations.CryptogramLoadStatus)GetValue((int)CryptoContentFieldIndex.LoadStatus, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.LoadStatus, value, true); }
		}

		/// <summary> The ShortSamNumber property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."SHORTSAMNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ShortSamNumber
		{
			get { return (System.String)GetValue((int)CryptoContentFieldIndex.ShortSamNumber, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.ShortSamNumber, value, true); }
		}

		/// <summary> The SignCertificateFilename property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."SIGNCERTIFICATEFILENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SignCertificateFilename
		{
			get { return (System.String)GetValue((int)CryptoContentFieldIndex.SignCertificateFilename, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.SignCertificateFilename, value, true); }
		}

		/// <summary> The SignCertificateResourceID property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."SIGNCERTIFICATERESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SignCertificateResourceID
		{
			get { return (System.Int64)GetValue((int)CryptoContentFieldIndex.SignCertificateResourceID, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.SignCertificateResourceID, value, true); }
		}

		/// <summary> The SubCertificateFilename property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."SUBCERTIFICATEFILENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SubCertificateFilename
		{
			get { return (System.String)GetValue((int)CryptoContentFieldIndex.SubCertificateFilename, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.SubCertificateFilename, value, true); }
		}

		/// <summary> The SubCertificateResourceID property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."SUBCERTIFICATERESOURCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SubCertificateResourceID
		{
			get { return (System.Int64)GetValue((int)CryptoContentFieldIndex.SubCertificateResourceID, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.SubCertificateResourceID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CryptoContent<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_CONTENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CryptoContentFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CryptoContentFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CryptoCryptogramArchiveEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoCryptogramArchive()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoCryptogramArchiveEntity CryptoCryptogramArchive
		{
			get	{ return GetSingleCryptoCryptogramArchive(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoCryptogramArchive(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoContents", "CryptoCryptogramArchive", _cryptoCryptogramArchive, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoCryptogramArchive. When set to true, CryptoCryptogramArchive is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoCryptogramArchive is accessed. You can always execute a forced fetch by calling GetSingleCryptoCryptogramArchive(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoCryptogramArchive
		{
			get	{ return _alwaysFetchCryptoCryptogramArchive; }
			set	{ _alwaysFetchCryptoCryptogramArchive = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoCryptogramArchive already has been fetched. Setting this property to false when CryptoCryptogramArchive has been fetched
		/// will set CryptoCryptogramArchive to null as well. Setting this property to true while CryptoCryptogramArchive hasn't been fetched disables lazy loading for CryptoCryptogramArchive</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoCryptogramArchive
		{
			get { return _alreadyFetchedCryptoCryptogramArchive;}
			set 
			{
				if(_alreadyFetchedCryptoCryptogramArchive && !value)
				{
					this.CryptoCryptogramArchive = null;
				}
				_alreadyFetchedCryptoCryptogramArchive = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoCryptogramArchive is not found
		/// in the database. When set to true, CryptoCryptogramArchive will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoCryptogramArchiveReturnsNewIfNotFound
		{
			get	{ return _cryptoCryptogramArchiveReturnsNewIfNotFound; }
			set { _cryptoCryptogramArchiveReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CryptoResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoResourceCryptogram()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoResourceEntity CryptoResourceCryptogram
		{
			get	{ return GetSingleCryptoResourceCryptogram(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoResourceCryptogram(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoContentCryptograms", "CryptoResourceCryptogram", _cryptoResourceCryptogram, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoResourceCryptogram. When set to true, CryptoResourceCryptogram is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoResourceCryptogram is accessed. You can always execute a forced fetch by calling GetSingleCryptoResourceCryptogram(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoResourceCryptogram
		{
			get	{ return _alwaysFetchCryptoResourceCryptogram; }
			set	{ _alwaysFetchCryptoResourceCryptogram = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoResourceCryptogram already has been fetched. Setting this property to false when CryptoResourceCryptogram has been fetched
		/// will set CryptoResourceCryptogram to null as well. Setting this property to true while CryptoResourceCryptogram hasn't been fetched disables lazy loading for CryptoResourceCryptogram</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoResourceCryptogram
		{
			get { return _alreadyFetchedCryptoResourceCryptogram;}
			set 
			{
				if(_alreadyFetchedCryptoResourceCryptogram && !value)
				{
					this.CryptoResourceCryptogram = null;
				}
				_alreadyFetchedCryptoResourceCryptogram = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoResourceCryptogram is not found
		/// in the database. When set to true, CryptoResourceCryptogram will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoResourceCryptogramReturnsNewIfNotFound
		{
			get	{ return _cryptoResourceCryptogramReturnsNewIfNotFound; }
			set { _cryptoResourceCryptogramReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CryptoResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoResourceSignCertificate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoResourceEntity CryptoResourceSignCertificate
		{
			get	{ return GetSingleCryptoResourceSignCertificate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoResourceSignCertificate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoContentSignCertificates", "CryptoResourceSignCertificate", _cryptoResourceSignCertificate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoResourceSignCertificate. When set to true, CryptoResourceSignCertificate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoResourceSignCertificate is accessed. You can always execute a forced fetch by calling GetSingleCryptoResourceSignCertificate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoResourceSignCertificate
		{
			get	{ return _alwaysFetchCryptoResourceSignCertificate; }
			set	{ _alwaysFetchCryptoResourceSignCertificate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoResourceSignCertificate already has been fetched. Setting this property to false when CryptoResourceSignCertificate has been fetched
		/// will set CryptoResourceSignCertificate to null as well. Setting this property to true while CryptoResourceSignCertificate hasn't been fetched disables lazy loading for CryptoResourceSignCertificate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoResourceSignCertificate
		{
			get { return _alreadyFetchedCryptoResourceSignCertificate;}
			set 
			{
				if(_alreadyFetchedCryptoResourceSignCertificate && !value)
				{
					this.CryptoResourceSignCertificate = null;
				}
				_alreadyFetchedCryptoResourceSignCertificate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoResourceSignCertificate is not found
		/// in the database. When set to true, CryptoResourceSignCertificate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoResourceSignCertificateReturnsNewIfNotFound
		{
			get	{ return _cryptoResourceSignCertificateReturnsNewIfNotFound; }
			set { _cryptoResourceSignCertificateReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CryptoResourceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoResourceSubCertificate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoResourceEntity CryptoResourceSubCertificate
		{
			get	{ return GetSingleCryptoResourceSubCertificate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoResourceSubCertificate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoContentSubCertificates", "CryptoResourceSubCertificate", _cryptoResourceSubCertificate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoResourceSubCertificate. When set to true, CryptoResourceSubCertificate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoResourceSubCertificate is accessed. You can always execute a forced fetch by calling GetSingleCryptoResourceSubCertificate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoResourceSubCertificate
		{
			get	{ return _alwaysFetchCryptoResourceSubCertificate; }
			set	{ _alwaysFetchCryptoResourceSubCertificate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoResourceSubCertificate already has been fetched. Setting this property to false when CryptoResourceSubCertificate has been fetched
		/// will set CryptoResourceSubCertificate to null as well. Setting this property to true while CryptoResourceSubCertificate hasn't been fetched disables lazy loading for CryptoResourceSubCertificate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoResourceSubCertificate
		{
			get { return _alreadyFetchedCryptoResourceSubCertificate;}
			set 
			{
				if(_alreadyFetchedCryptoResourceSubCertificate && !value)
				{
					this.CryptoResourceSubCertificate = null;
				}
				_alreadyFetchedCryptoResourceSubCertificate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoResourceSubCertificate is not found
		/// in the database. When set to true, CryptoResourceSubCertificate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoResourceSubCertificateReturnsNewIfNotFound
		{
			get	{ return _cryptoResourceSubCertificateReturnsNewIfNotFound; }
			set { _cryptoResourceSubCertificateReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CryptoContentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
