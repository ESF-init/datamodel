﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ConfigurationOverwrite'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ConfigurationOverwriteEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private ConfigurationDefinitionEntity _configurationDefinition;
		private bool	_alwaysFetchConfigurationDefinition, _alreadyFetchedConfigurationDefinition, _configurationDefinitionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ConfigurationDefinition</summary>
			public static readonly string ConfigurationDefinition = "ConfigurationDefinition";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ConfigurationOverwriteEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ConfigurationOverwriteEntity() :base("ConfigurationOverwriteEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		public ConfigurationOverwriteEntity(System.Int64 configurationOverwriteID):base("ConfigurationOverwriteEntity")
		{
			InitClassFetch(configurationOverwriteID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ConfigurationOverwriteEntity(System.Int64 configurationOverwriteID, IPrefetchPath prefetchPathToUse):base("ConfigurationOverwriteEntity")
		{
			InitClassFetch(configurationOverwriteID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="validator">The custom validator object for this ConfigurationOverwriteEntity</param>
		public ConfigurationOverwriteEntity(System.Int64 configurationOverwriteID, IValidator validator):base("ConfigurationOverwriteEntity")
		{
			InitClassFetch(configurationOverwriteID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ConfigurationOverwriteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_configurationDefinition = (ConfigurationDefinitionEntity)info.GetValue("_configurationDefinition", typeof(ConfigurationDefinitionEntity));
			if(_configurationDefinition!=null)
			{
				_configurationDefinition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_configurationDefinitionReturnsNewIfNotFound = info.GetBoolean("_configurationDefinitionReturnsNewIfNotFound");
			_alwaysFetchConfigurationDefinition = info.GetBoolean("_alwaysFetchConfigurationDefinition");
			_alreadyFetchedConfigurationDefinition = info.GetBoolean("_alreadyFetchedConfigurationDefinition");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ConfigurationOverwriteFieldIndex)fieldIndex)
			{
				case ConfigurationOverwriteFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ConfigurationOverwriteFieldIndex.ConfigurationDefinitionID:
					DesetupSyncConfigurationDefinition(true, false);
					_alreadyFetchedConfigurationDefinition = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedConfigurationDefinition = (_configurationDefinition != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "ConfigurationDefinition":
					toReturn.Add(Relations.ConfigurationDefinitionEntityUsingConfigurationDefinitionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_configurationDefinition", (!this.MarkedForDeletion?_configurationDefinition:null));
			info.AddValue("_configurationDefinitionReturnsNewIfNotFound", _configurationDefinitionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchConfigurationDefinition", _alwaysFetchConfigurationDefinition);
			info.AddValue("_alreadyFetchedConfigurationDefinition", _alreadyFetchedConfigurationDefinition);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "ConfigurationDefinition":
					_alreadyFetchedConfigurationDefinition = true;
					this.ConfigurationDefinition = (ConfigurationDefinitionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "ConfigurationDefinition":
					SetupSyncConfigurationDefinition(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "ConfigurationDefinition":
					DesetupSyncConfigurationDefinition(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_configurationDefinition!=null)
			{
				toReturn.Add(_configurationDefinition);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="configurationDefinitionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue(System.Int64 clientID, System.Int64 configurationDefinitionID, System.String specifierType, System.String specifierValue)
		{
			return FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue( clientID,  configurationDefinitionID,  specifierType,  specifierValue, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="configurationDefinitionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue(System.Int64 clientID, System.Int64 configurationDefinitionID, System.String specifierType, System.String specifierValue, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue( clientID,  configurationDefinitionID,  specifierType,  specifierValue, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="configurationDefinitionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue(System.Int64 clientID, System.Int64 configurationDefinitionID, System.String specifierType, System.String specifierValue, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue( clientID,  configurationDefinitionID,  specifierType,  specifierValue, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="configurationDefinitionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierType">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="specifierValue">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue(System.Int64 clientID, System.Int64 configurationDefinitionID, System.String specifierType, System.String specifierValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ConfigurationOverwriteDAO)CreateDAOInstance()).FetchConfigurationOverwriteUsingUCClientIDConfigurationDefinitionIDSpecifierTypeSpecifierValue(this, this.Transaction, clientID, configurationDefinitionID, specifierType, specifierValue, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationOverwriteID)
		{
			return FetchUsingPK(configurationOverwriteID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationOverwriteID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(configurationOverwriteID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationOverwriteID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(configurationOverwriteID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationOverwriteID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(configurationOverwriteID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ConfigurationOverwriteID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ConfigurationOverwriteRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'ConfigurationDefinitionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ConfigurationDefinitionEntity' which is related to this entity.</returns>
		public ConfigurationDefinitionEntity GetSingleConfigurationDefinition()
		{
			return GetSingleConfigurationDefinition(false);
		}

		/// <summary> Retrieves the related entity of type 'ConfigurationDefinitionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ConfigurationDefinitionEntity' which is related to this entity.</returns>
		public virtual ConfigurationDefinitionEntity GetSingleConfigurationDefinition(bool forceFetch)
		{
			if( ( !_alreadyFetchedConfigurationDefinition || forceFetch || _alwaysFetchConfigurationDefinition) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ConfigurationDefinitionEntityUsingConfigurationDefinitionID);
				ConfigurationDefinitionEntity newEntity = new ConfigurationDefinitionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ConfigurationDefinitionID);
				}
				if(fetchResult)
				{
					newEntity = (ConfigurationDefinitionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_configurationDefinitionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ConfigurationDefinition = newEntity;
				_alreadyFetchedConfigurationDefinition = fetchResult;
			}
			return _configurationDefinition;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("ConfigurationDefinition", _configurationDefinition);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="validator">The validator object for this ConfigurationOverwriteEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 configurationOverwriteID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(configurationOverwriteID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientReturnsNewIfNotFound = false;
			_configurationDefinitionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfigurationDefinitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfigurationOverwriteID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpecifierType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpecifierValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Value", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticConfigurationOverwriteRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ConfigurationOverwrites", resetFKFields, new int[] { (int)ConfigurationOverwriteFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticConfigurationOverwriteRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _configurationDefinition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncConfigurationDefinition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _configurationDefinition, new PropertyChangedEventHandler( OnConfigurationDefinitionPropertyChanged ), "ConfigurationDefinition", VarioSL.Entities.RelationClasses.StaticConfigurationOverwriteRelations.ConfigurationDefinitionEntityUsingConfigurationDefinitionIDStatic, true, signalRelatedEntity, "ConfigurationOverwrites", resetFKFields, new int[] { (int)ConfigurationOverwriteFieldIndex.ConfigurationDefinitionID } );		
			_configurationDefinition = null;
		}
		
		/// <summary> setups the sync logic for member _configurationDefinition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncConfigurationDefinition(IEntityCore relatedEntity)
		{
			if(_configurationDefinition!=relatedEntity)
			{		
				DesetupSyncConfigurationDefinition(true, true);
				_configurationDefinition = (ConfigurationDefinitionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _configurationDefinition, new PropertyChangedEventHandler( OnConfigurationDefinitionPropertyChanged ), "ConfigurationDefinition", VarioSL.Entities.RelationClasses.StaticConfigurationOverwriteRelations.ConfigurationDefinitionEntityUsingConfigurationDefinitionIDStatic, true, ref _alreadyFetchedConfigurationDefinition, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnConfigurationDefinitionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="configurationOverwriteID">PK value for ConfigurationOverwrite which data should be fetched into this ConfigurationOverwrite object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 configurationOverwriteID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ConfigurationOverwriteFieldIndex.ConfigurationOverwriteID].ForcedCurrentValueWrite(configurationOverwriteID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateConfigurationOverwriteDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ConfigurationOverwriteEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ConfigurationOverwriteRelations Relations
		{
			get	{ return new ConfigurationOverwriteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ConfigurationOverwriteEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConfigurationDefinition'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfigurationDefinition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationDefinitionCollection(), (IEntityRelation)GetRelationsForField("ConfigurationDefinition")[0], (int)VarioSL.Entities.EntityType.ConfigurationOverwriteEntity, (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, 0, null, null, null, "ConfigurationDefinition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)ConfigurationOverwriteFieldIndex.ClientID, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ConfigurationDefinitionID property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."CONFIGURATIONDEFINITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ConfigurationDefinitionID
		{
			get { return (System.Int64)GetValue((int)ConfigurationOverwriteFieldIndex.ConfigurationDefinitionID, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.ConfigurationDefinitionID, value, true); }
		}

		/// <summary> The ConfigurationOverwriteID property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."CONFIGURATIONOVERWRITEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ConfigurationOverwriteID
		{
			get { return (System.Int64)GetValue((int)ConfigurationOverwriteFieldIndex.ConfigurationOverwriteID, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.ConfigurationOverwriteID, value, true); }
		}

		/// <summary> The LastModified property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ConfigurationOverwriteFieldIndex.LastModified, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ConfigurationOverwriteFieldIndex.LastUser, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SpecifierType property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."SPECIFIERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SpecifierType
		{
			get { return (System.String)GetValue((int)ConfigurationOverwriteFieldIndex.SpecifierType, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.SpecifierType, value, true); }
		}

		/// <summary> The SpecifierValue property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."SPECIFIERVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SpecifierValue
		{
			get { return (System.String)GetValue((int)ConfigurationOverwriteFieldIndex.SpecifierValue, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.SpecifierValue, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ConfigurationOverwriteFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The Value property of the Entity ConfigurationOverwrite<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONOVERWRITE"."VALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Value
		{
			get { return (System.String)GetValue((int)ConfigurationOverwriteFieldIndex.Value, true); }
			set	{ SetValue((int)ConfigurationOverwriteFieldIndex.Value, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ConfigurationOverwrites", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ConfigurationDefinitionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleConfigurationDefinition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ConfigurationDefinitionEntity ConfigurationDefinition
		{
			get	{ return GetSingleConfigurationDefinition(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncConfigurationDefinition(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ConfigurationOverwrites", "ConfigurationDefinition", _configurationDefinition, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ConfigurationDefinition. When set to true, ConfigurationDefinition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ConfigurationDefinition is accessed. You can always execute a forced fetch by calling GetSingleConfigurationDefinition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfigurationDefinition
		{
			get	{ return _alwaysFetchConfigurationDefinition; }
			set	{ _alwaysFetchConfigurationDefinition = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ConfigurationDefinition already has been fetched. Setting this property to false when ConfigurationDefinition has been fetched
		/// will set ConfigurationDefinition to null as well. Setting this property to true while ConfigurationDefinition hasn't been fetched disables lazy loading for ConfigurationDefinition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfigurationDefinition
		{
			get { return _alreadyFetchedConfigurationDefinition;}
			set 
			{
				if(_alreadyFetchedConfigurationDefinition && !value)
				{
					this.ConfigurationDefinition = null;
				}
				_alreadyFetchedConfigurationDefinition = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ConfigurationDefinition is not found
		/// in the database. When set to true, ConfigurationDefinition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ConfigurationDefinitionReturnsNewIfNotFound
		{
			get	{ return _configurationDefinitionReturnsNewIfNotFound; }
			set { _configurationDefinitionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ConfigurationOverwriteEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
