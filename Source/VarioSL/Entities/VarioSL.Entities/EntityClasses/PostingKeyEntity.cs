﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PostingKey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PostingKeyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection	_dunningLevelPostingKeys;
		private bool	_alwaysFetchDunningLevelPostingKeys, _alreadyFetchedDunningLevelPostingKeys;
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name DunningLevelPostingKeys</summary>
			public static readonly string DunningLevelPostingKeys = "DunningLevelPostingKeys";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PostingKeyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PostingKeyEntity() :base("PostingKeyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		public PostingKeyEntity(System.Int64 postingKeyID):base("PostingKeyEntity")
		{
			InitClassFetch(postingKeyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PostingKeyEntity(System.Int64 postingKeyID, IPrefetchPath prefetchPathToUse):base("PostingKeyEntity")
		{
			InitClassFetch(postingKeyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="validator">The custom validator object for this PostingKeyEntity</param>
		public PostingKeyEntity(System.Int64 postingKeyID, IValidator validator):base("PostingKeyEntity")
		{
			InitClassFetch(postingKeyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PostingKeyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningLevelPostingKeys = (VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection)info.GetValue("_dunningLevelPostingKeys", typeof(VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection));
			_alwaysFetchDunningLevelPostingKeys = info.GetBoolean("_alwaysFetchDunningLevelPostingKeys");
			_alreadyFetchedDunningLevelPostingKeys = info.GetBoolean("_alreadyFetchedDunningLevelPostingKeys");

			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PostingKeyFieldIndex)fieldIndex)
			{
				case PostingKeyFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningLevelPostingKeys = (_dunningLevelPostingKeys.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedClient = (_client != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "DunningLevelPostingKeys":
					toReturn.Add(Relations.DunningLevelPostingKeyEntityUsingPostingKeyID);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingPostingKeyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningLevelPostingKeys", (!this.MarkedForDeletion?_dunningLevelPostingKeys:null));
			info.AddValue("_alwaysFetchDunningLevelPostingKeys", _alwaysFetchDunningLevelPostingKeys);
			info.AddValue("_alreadyFetchedDunningLevelPostingKeys", _alreadyFetchedDunningLevelPostingKeys);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "DunningLevelPostingKeys":
					_alreadyFetchedDunningLevelPostingKeys = true;
					if(entity!=null)
					{
						this.DunningLevelPostingKeys.Add((DunningLevelPostingKeyEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "DunningLevelPostingKeys":
					_dunningLevelPostingKeys.Add((DunningLevelPostingKeyEntity)relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "DunningLevelPostingKeys":
					this.PerformRelatedEntityRemoval(_dunningLevelPostingKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_dunningLevelPostingKeys);
			toReturn.Add(_postings);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="postingKey">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDPostingKey(System.Int64 clientID, System.Int64 postingKey)
		{
			return FetchUsingUCClientIDPostingKey( clientID,  postingKey, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="postingKey">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDPostingKey(System.Int64 clientID, System.Int64 postingKey, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCClientIDPostingKey( clientID,  postingKey, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="postingKey">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDPostingKey(System.Int64 clientID, System.Int64 postingKey, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCClientIDPostingKey( clientID,  postingKey, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="postingKey">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDPostingKey(System.Int64 clientID, System.Int64 postingKey, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PostingKeyDAO)CreateDAOInstance()).FetchPostingKeyUsingUCClientIDPostingKey(this, this.Transaction, clientID, postingKey, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingKeyID)
		{
			return FetchUsingPK(postingKeyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingKeyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(postingKeyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(postingKeyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(postingKeyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PostingKeyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PostingKeyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelPostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, _dunningLevelPostingKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningLevelPostingKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, _dunningLevelPostingKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningLevelPostingKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection GetMultiDunningLevelPostingKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningLevelPostingKeys || forceFetch || _alwaysFetchDunningLevelPostingKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningLevelPostingKeys);
				_dunningLevelPostingKeys.SuppressClearInGetMulti=!forceFetch;
				_dunningLevelPostingKeys.EntityFactoryToUse = entityFactoryToUse;
				_dunningLevelPostingKeys.GetMultiManyToOne(null, this, filter);
				_dunningLevelPostingKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningLevelPostingKeys = true;
			}
			return _dunningLevelPostingKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningLevelPostingKeys'. These settings will be taken into account
		/// when the property DunningLevelPostingKeys is requested or GetMultiDunningLevelPostingKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningLevelPostingKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningLevelPostingKeys.SortClauses=sortClauses;
			_dunningLevelPostingKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(null, null, null, null, null, this, null, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("DunningLevelPostingKeys", _dunningLevelPostingKeys);
			toReturn.Add("Postings", _postings);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="validator">The validator object for this PostingKeyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 postingKeyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(postingKeyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_dunningLevelPostingKeys = new VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection();
			_dunningLevelPostingKeys.SetContainingEntityInfo(this, "PostingKey");

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "PostingKey");
			_clientReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancelKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Category", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBillingRelevant", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsManuallyBookable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsVisibleInAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Liability", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingScope", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticPostingKeyRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "PostingKeys", resetFKFields, new int[] { (int)PostingKeyFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticPostingKeyRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="postingKeyID">PK value for PostingKey which data should be fetched into this PostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 postingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PostingKeyFieldIndex.PostingKeyID].ForcedCurrentValueWrite(postingKeyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePostingKeyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PostingKeyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PostingKeyRelations Relations
		{
			get	{ return new PostingKeyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelPostingKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningLevelPostingKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection(), (IEntityRelation)GetRelationsForField("DunningLevelPostingKeys")[0], (int)VarioSL.Entities.EntityType.PostingKeyEntity, (int)VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity, 0, null, null, null, "DunningLevelPostingKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.PostingKeyEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.PostingKeyEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CancelKey property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."CANCELKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancelKey
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingKeyFieldIndex.CancelKey, false); }
			set	{ SetValue((int)PostingKeyFieldIndex.CancelKey, value, true); }
		}

		/// <summary> The Category property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."CATEGORY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.PostingKeyCategory Category
		{
			get { return (VarioSL.Entities.Enumerations.PostingKeyCategory)GetValue((int)PostingKeyFieldIndex.Category, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.Category, value, true); }
		}

		/// <summary> The ClientID property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)PostingKeyFieldIndex.ClientID, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PostingKeyFieldIndex.Description, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.Description, value, true); }
		}

		/// <summary> The IsBillingRelevant property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."ISBILLINGRELEVANT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsBillingRelevant
		{
			get { return (Nullable<System.Boolean>)GetValue((int)PostingKeyFieldIndex.IsBillingRelevant, false); }
			set	{ SetValue((int)PostingKeyFieldIndex.IsBillingRelevant, value, true); }
		}

		/// <summary> The IsCredit property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."ISCREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCredit
		{
			get { return (System.Boolean)GetValue((int)PostingKeyFieldIndex.IsCredit, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.IsCredit, value, true); }
		}

		/// <summary> The IsManuallyBookable property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."ISMANUALLYBOOKABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsManuallyBookable
		{
			get { return (System.Boolean)GetValue((int)PostingKeyFieldIndex.IsManuallyBookable, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.IsManuallyBookable, value, true); }
		}

		/// <summary> The IsVisibleInAccount property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."ISVISIBLEINACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsVisibleInAccount
		{
			get { return (Nullable<System.Boolean>)GetValue((int)PostingKeyFieldIndex.IsVisibleInAccount, false); }
			set	{ SetValue((int)PostingKeyFieldIndex.IsVisibleInAccount, value, true); }
		}

		/// <summary> The LastModified property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PostingKeyFieldIndex.LastModified, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PostingKeyFieldIndex.LastUser, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Liability property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."LIABILITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Liability
		{
			get { return (System.Int32)GetValue((int)PostingKeyFieldIndex.Liability, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.Liability, value, true); }
		}

		/// <summary> The PostingKey property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."POSTINGKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PostingKey
		{
			get { return (System.Int64)GetValue((int)PostingKeyFieldIndex.PostingKey, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.PostingKey, value, true); }
		}

		/// <summary> The PostingKeyID property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."POSTINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PostingKeyID
		{
			get { return (System.Int64)GetValue((int)PostingKeyFieldIndex.PostingKeyID, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.PostingKeyID, value, true); }
		}

		/// <summary> The PostingKeyTypeID property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."POSTINGKEYTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PostingKeyTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingKeyFieldIndex.PostingKeyTypeID, false); }
			set	{ SetValue((int)PostingKeyFieldIndex.PostingKeyTypeID, value, true); }
		}

		/// <summary> The PostingScope property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."POSTINGSCOPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PostingScope
		{
			get { return (System.Int32)GetValue((int)PostingKeyFieldIndex.PostingScope, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.PostingScope, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTINGKEY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PostingKeyFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PostingKeyFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DunningLevelPostingKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningLevelPostingKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningLevelPostingKeyCollection DunningLevelPostingKeys
		{
			get	{ return GetMultiDunningLevelPostingKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningLevelPostingKeys. When set to true, DunningLevelPostingKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningLevelPostingKeys is accessed. You can always execute/ a forced fetch by calling GetMultiDunningLevelPostingKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningLevelPostingKeys
		{
			get	{ return _alwaysFetchDunningLevelPostingKeys; }
			set	{ _alwaysFetchDunningLevelPostingKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningLevelPostingKeys already has been fetched. Setting this property to false when DunningLevelPostingKeys has been fetched
		/// will clear the DunningLevelPostingKeys collection well. Setting this property to true while DunningLevelPostingKeys hasn't been fetched disables lazy loading for DunningLevelPostingKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningLevelPostingKeys
		{
			get { return _alreadyFetchedDunningLevelPostingKeys;}
			set 
			{
				if(_alreadyFetchedDunningLevelPostingKeys && !value && (_dunningLevelPostingKeys != null))
				{
					_dunningLevelPostingKeys.Clear();
				}
				_alreadyFetchedDunningLevelPostingKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PostingKeys", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PostingKeyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
