﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketDeviceClass'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketDeviceClassEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection	_ticketDeviceClassOutputDevice;
		private bool	_alwaysFetchTicketDeviceClassOutputDevice, _alreadyFetchedTicketDeviceClassOutputDevice;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection	_ticketDeviceClassPaymentMethods;
		private bool	_alwaysFetchTicketDeviceClassPaymentMethods, _alreadyFetchedTicketDeviceClassPaymentMethods;
		private VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection _devicePaymentMethodCollection;
		private bool	_alwaysFetchDevicePaymentMethodCollection, _alreadyFetchedDevicePaymentMethodCollection;
		private VarioSL.Entities.CollectionClasses.OutputDeviceCollection _outputDevices;
		private bool	_alwaysFetchOutputDevices, _alreadyFetchedOutputDevices;
		private CalendarEntity _calendar;
		private bool	_alwaysFetchCalendar, _alreadyFetchedCalendar, _calendarReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private EticketEntity _eticket;
		private bool	_alwaysFetchEticket, _alreadyFetchedEticket, _eticketReturnsNewIfNotFound;
		private FareMatrixEntity _fareMatrix;
		private bool	_alwaysFetchFareMatrix, _alreadyFetchedFareMatrix, _fareMatrixReturnsNewIfNotFound;
		private LayoutEntity _cancellationLayout;
		private bool	_alwaysFetchCancellationLayout, _alreadyFetchedCancellationLayout, _cancellationLayoutReturnsNewIfNotFound;
		private LayoutEntity _groupLayout;
		private bool	_alwaysFetchGroupLayout, _alreadyFetchedGroupLayout, _groupLayoutReturnsNewIfNotFound;
		private LayoutEntity _printLayout;
		private bool	_alwaysFetchPrintLayout, _alreadyFetchedPrintLayout, _printLayoutReturnsNewIfNotFound;
		private LayoutEntity _receiptLayout;
		private bool	_alwaysFetchReceiptLayout, _alreadyFetchedReceiptLayout, _receiptLayoutReturnsNewIfNotFound;
		private RulePeriodEntity _rulePeriod;
		private bool	_alwaysFetchRulePeriod, _alreadyFetchedRulePeriod, _rulePeriodReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private TicketSelectionModeEntity _ticketSelectionMode;
		private bool	_alwaysFetchTicketSelectionMode, _alreadyFetchedTicketSelectionMode, _ticketSelectionModeReturnsNewIfNotFound;
		private UserKeyEntity _userKey1;
		private bool	_alwaysFetchUserKey1, _alreadyFetchedUserKey1, _userKey1ReturnsNewIfNotFound;
		private UserKeyEntity _userKey2;
		private bool	_alwaysFetchUserKey2, _alreadyFetchedUserKey2, _userKey2ReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Calendar</summary>
			public static readonly string Calendar = "Calendar";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Eticket</summary>
			public static readonly string Eticket = "Eticket";
			/// <summary>Member name FareMatrix</summary>
			public static readonly string FareMatrix = "FareMatrix";
			/// <summary>Member name CancellationLayout</summary>
			public static readonly string CancellationLayout = "CancellationLayout";
			/// <summary>Member name GroupLayout</summary>
			public static readonly string GroupLayout = "GroupLayout";
			/// <summary>Member name PrintLayout</summary>
			public static readonly string PrintLayout = "PrintLayout";
			/// <summary>Member name ReceiptLayout</summary>
			public static readonly string ReceiptLayout = "ReceiptLayout";
			/// <summary>Member name RulePeriod</summary>
			public static readonly string RulePeriod = "RulePeriod";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name TicketSelectionMode</summary>
			public static readonly string TicketSelectionMode = "TicketSelectionMode";
			/// <summary>Member name UserKey1</summary>
			public static readonly string UserKey1 = "UserKey1";
			/// <summary>Member name UserKey2</summary>
			public static readonly string UserKey2 = "UserKey2";
			/// <summary>Member name TicketDeviceClassOutputDevice</summary>
			public static readonly string TicketDeviceClassOutputDevice = "TicketDeviceClassOutputDevice";
			/// <summary>Member name TicketDeviceClassPaymentMethods</summary>
			public static readonly string TicketDeviceClassPaymentMethods = "TicketDeviceClassPaymentMethods";
			/// <summary>Member name DevicePaymentMethodCollection</summary>
			public static readonly string DevicePaymentMethodCollection = "DevicePaymentMethodCollection";
			/// <summary>Member name OutputDevices</summary>
			public static readonly string OutputDevices = "OutputDevices";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketDeviceClassEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketDeviceClassEntity() :base("TicketDeviceClassEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		public TicketDeviceClassEntity(System.Int64 ticketDeviceClassID):base("TicketDeviceClassEntity")
		{
			InitClassFetch(ticketDeviceClassID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketDeviceClassEntity(System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse):base("TicketDeviceClassEntity")
		{
			InitClassFetch(ticketDeviceClassID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="validator">The custom validator object for this TicketDeviceClassEntity</param>
		public TicketDeviceClassEntity(System.Int64 ticketDeviceClassID, IValidator validator):base("TicketDeviceClassEntity")
		{
			InitClassFetch(ticketDeviceClassID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketDeviceClassEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticketDeviceClassOutputDevice = (VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection)info.GetValue("_ticketDeviceClassOutputDevice", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection));
			_alwaysFetchTicketDeviceClassOutputDevice = info.GetBoolean("_alwaysFetchTicketDeviceClassOutputDevice");
			_alreadyFetchedTicketDeviceClassOutputDevice = info.GetBoolean("_alreadyFetchedTicketDeviceClassOutputDevice");

			_ticketDeviceClassPaymentMethods = (VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection)info.GetValue("_ticketDeviceClassPaymentMethods", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection));
			_alwaysFetchTicketDeviceClassPaymentMethods = info.GetBoolean("_alwaysFetchTicketDeviceClassPaymentMethods");
			_alreadyFetchedTicketDeviceClassPaymentMethods = info.GetBoolean("_alreadyFetchedTicketDeviceClassPaymentMethods");
			_devicePaymentMethodCollection = (VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection)info.GetValue("_devicePaymentMethodCollection", typeof(VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection));
			_alwaysFetchDevicePaymentMethodCollection = info.GetBoolean("_alwaysFetchDevicePaymentMethodCollection");
			_alreadyFetchedDevicePaymentMethodCollection = info.GetBoolean("_alreadyFetchedDevicePaymentMethodCollection");

			_outputDevices = (VarioSL.Entities.CollectionClasses.OutputDeviceCollection)info.GetValue("_outputDevices", typeof(VarioSL.Entities.CollectionClasses.OutputDeviceCollection));
			_alwaysFetchOutputDevices = info.GetBoolean("_alwaysFetchOutputDevices");
			_alreadyFetchedOutputDevices = info.GetBoolean("_alreadyFetchedOutputDevices");
			_calendar = (CalendarEntity)info.GetValue("_calendar", typeof(CalendarEntity));
			if(_calendar!=null)
			{
				_calendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_calendarReturnsNewIfNotFound = info.GetBoolean("_calendarReturnsNewIfNotFound");
			_alwaysFetchCalendar = info.GetBoolean("_alwaysFetchCalendar");
			_alreadyFetchedCalendar = info.GetBoolean("_alreadyFetchedCalendar");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_eticket = (EticketEntity)info.GetValue("_eticket", typeof(EticketEntity));
			if(_eticket!=null)
			{
				_eticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_eticketReturnsNewIfNotFound = info.GetBoolean("_eticketReturnsNewIfNotFound");
			_alwaysFetchEticket = info.GetBoolean("_alwaysFetchEticket");
			_alreadyFetchedEticket = info.GetBoolean("_alreadyFetchedEticket");

			_fareMatrix = (FareMatrixEntity)info.GetValue("_fareMatrix", typeof(FareMatrixEntity));
			if(_fareMatrix!=null)
			{
				_fareMatrix.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareMatrixReturnsNewIfNotFound = info.GetBoolean("_fareMatrixReturnsNewIfNotFound");
			_alwaysFetchFareMatrix = info.GetBoolean("_alwaysFetchFareMatrix");
			_alreadyFetchedFareMatrix = info.GetBoolean("_alreadyFetchedFareMatrix");

			_cancellationLayout = (LayoutEntity)info.GetValue("_cancellationLayout", typeof(LayoutEntity));
			if(_cancellationLayout!=null)
			{
				_cancellationLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cancellationLayoutReturnsNewIfNotFound = info.GetBoolean("_cancellationLayoutReturnsNewIfNotFound");
			_alwaysFetchCancellationLayout = info.GetBoolean("_alwaysFetchCancellationLayout");
			_alreadyFetchedCancellationLayout = info.GetBoolean("_alreadyFetchedCancellationLayout");

			_groupLayout = (LayoutEntity)info.GetValue("_groupLayout", typeof(LayoutEntity));
			if(_groupLayout!=null)
			{
				_groupLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_groupLayoutReturnsNewIfNotFound = info.GetBoolean("_groupLayoutReturnsNewIfNotFound");
			_alwaysFetchGroupLayout = info.GetBoolean("_alwaysFetchGroupLayout");
			_alreadyFetchedGroupLayout = info.GetBoolean("_alreadyFetchedGroupLayout");

			_printLayout = (LayoutEntity)info.GetValue("_printLayout", typeof(LayoutEntity));
			if(_printLayout!=null)
			{
				_printLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printLayoutReturnsNewIfNotFound = info.GetBoolean("_printLayoutReturnsNewIfNotFound");
			_alwaysFetchPrintLayout = info.GetBoolean("_alwaysFetchPrintLayout");
			_alreadyFetchedPrintLayout = info.GetBoolean("_alreadyFetchedPrintLayout");

			_receiptLayout = (LayoutEntity)info.GetValue("_receiptLayout", typeof(LayoutEntity));
			if(_receiptLayout!=null)
			{
				_receiptLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_receiptLayoutReturnsNewIfNotFound = info.GetBoolean("_receiptLayoutReturnsNewIfNotFound");
			_alwaysFetchReceiptLayout = info.GetBoolean("_alwaysFetchReceiptLayout");
			_alreadyFetchedReceiptLayout = info.GetBoolean("_alreadyFetchedReceiptLayout");

			_rulePeriod = (RulePeriodEntity)info.GetValue("_rulePeriod", typeof(RulePeriodEntity));
			if(_rulePeriod!=null)
			{
				_rulePeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_rulePeriodReturnsNewIfNotFound = info.GetBoolean("_rulePeriodReturnsNewIfNotFound");
			_alwaysFetchRulePeriod = info.GetBoolean("_alwaysFetchRulePeriod");
			_alreadyFetchedRulePeriod = info.GetBoolean("_alreadyFetchedRulePeriod");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_ticketSelectionMode = (TicketSelectionModeEntity)info.GetValue("_ticketSelectionMode", typeof(TicketSelectionModeEntity));
			if(_ticketSelectionMode!=null)
			{
				_ticketSelectionMode.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketSelectionModeReturnsNewIfNotFound = info.GetBoolean("_ticketSelectionModeReturnsNewIfNotFound");
			_alwaysFetchTicketSelectionMode = info.GetBoolean("_alwaysFetchTicketSelectionMode");
			_alreadyFetchedTicketSelectionMode = info.GetBoolean("_alreadyFetchedTicketSelectionMode");

			_userKey1 = (UserKeyEntity)info.GetValue("_userKey1", typeof(UserKeyEntity));
			if(_userKey1!=null)
			{
				_userKey1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userKey1ReturnsNewIfNotFound = info.GetBoolean("_userKey1ReturnsNewIfNotFound");
			_alwaysFetchUserKey1 = info.GetBoolean("_alwaysFetchUserKey1");
			_alreadyFetchedUserKey1 = info.GetBoolean("_alreadyFetchedUserKey1");

			_userKey2 = (UserKeyEntity)info.GetValue("_userKey2", typeof(UserKeyEntity));
			if(_userKey2!=null)
			{
				_userKey2.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userKey2ReturnsNewIfNotFound = info.GetBoolean("_userKey2ReturnsNewIfNotFound");
			_alwaysFetchUserKey2 = info.GetBoolean("_alwaysFetchUserKey2");
			_alreadyFetchedUserKey2 = info.GetBoolean("_alreadyFetchedUserKey2");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketDeviceClassFieldIndex)fieldIndex)
			{
				case TicketDeviceClassFieldIndex.CalendarID:
					DesetupSyncCalendar(true, false);
					_alreadyFetchedCalendar = false;
					break;
				case TicketDeviceClassFieldIndex.CancellationLayoutID:
					DesetupSyncCancellationLayout(true, false);
					_alreadyFetchedCancellationLayout = false;
					break;
				case TicketDeviceClassFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case TicketDeviceClassFieldIndex.EtickID:
					DesetupSyncEticket(true, false);
					_alreadyFetchedEticket = false;
					break;
				case TicketDeviceClassFieldIndex.GroupLayoutID:
					DesetupSyncGroupLayout(true, false);
					_alreadyFetchedGroupLayout = false;
					break;
				case TicketDeviceClassFieldIndex.Key1:
					DesetupSyncUserKey1(true, false);
					_alreadyFetchedUserKey1 = false;
					break;
				case TicketDeviceClassFieldIndex.Key2:
					DesetupSyncUserKey2(true, false);
					_alreadyFetchedUserKey2 = false;
					break;
				case TicketDeviceClassFieldIndex.MatrixID:
					DesetupSyncFareMatrix(true, false);
					_alreadyFetchedFareMatrix = false;
					break;
				case TicketDeviceClassFieldIndex.PrintLayoutID:
					DesetupSyncPrintLayout(true, false);
					_alreadyFetchedPrintLayout = false;
					break;
				case TicketDeviceClassFieldIndex.ReceiptLayoutID:
					DesetupSyncReceiptLayout(true, false);
					_alreadyFetchedReceiptLayout = false;
					break;
				case TicketDeviceClassFieldIndex.RulePeriodID:
					DesetupSyncRulePeriod(true, false);
					_alreadyFetchedRulePeriod = false;
					break;
				case TicketDeviceClassFieldIndex.SelectionMode:
					DesetupSyncTicketSelectionMode(true, false);
					_alreadyFetchedTicketSelectionMode = false;
					break;
				case TicketDeviceClassFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicketDeviceClassOutputDevice = (_ticketDeviceClassOutputDevice.Count > 0);
			_alreadyFetchedTicketDeviceClassPaymentMethods = (_ticketDeviceClassPaymentMethods.Count > 0);
			_alreadyFetchedDevicePaymentMethodCollection = (_devicePaymentMethodCollection.Count > 0);
			_alreadyFetchedOutputDevices = (_outputDevices.Count > 0);
			_alreadyFetchedCalendar = (_calendar != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedEticket = (_eticket != null);
			_alreadyFetchedFareMatrix = (_fareMatrix != null);
			_alreadyFetchedCancellationLayout = (_cancellationLayout != null);
			_alreadyFetchedGroupLayout = (_groupLayout != null);
			_alreadyFetchedPrintLayout = (_printLayout != null);
			_alreadyFetchedReceiptLayout = (_receiptLayout != null);
			_alreadyFetchedRulePeriod = (_rulePeriod != null);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedTicketSelectionMode = (_ticketSelectionMode != null);
			_alreadyFetchedUserKey1 = (_userKey1 != null);
			_alreadyFetchedUserKey2 = (_userKey2 != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Calendar":
					toReturn.Add(Relations.CalendarEntityUsingCalendarID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "Eticket":
					toReturn.Add(Relations.EticketEntityUsingEtickID);
					break;
				case "FareMatrix":
					toReturn.Add(Relations.FareMatrixEntityUsingMatrixID);
					break;
				case "CancellationLayout":
					toReturn.Add(Relations.LayoutEntityUsingCancellationLayoutID);
					break;
				case "GroupLayout":
					toReturn.Add(Relations.LayoutEntityUsingGroupLayoutID);
					break;
				case "PrintLayout":
					toReturn.Add(Relations.LayoutEntityUsingPrintLayoutID);
					break;
				case "ReceiptLayout":
					toReturn.Add(Relations.LayoutEntityUsingReceiptLayoutID);
					break;
				case "RulePeriod":
					toReturn.Add(Relations.RulePeriodEntityUsingRulePeriodID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "TicketSelectionMode":
					toReturn.Add(Relations.TicketSelectionModeEntityUsingSelectionMode);
					break;
				case "UserKey1":
					toReturn.Add(Relations.UserKeyEntityUsingKey1);
					break;
				case "UserKey2":
					toReturn.Add(Relations.UserKeyEntityUsingKey2);
					break;
				case "TicketDeviceClassOutputDevice":
					toReturn.Add(Relations.TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID);
					break;
				case "TicketDeviceClassPaymentMethods":
					toReturn.Add(Relations.TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID);
					break;
				case "DevicePaymentMethodCollection":
					toReturn.Add(Relations.TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID, "TicketDeviceClassEntity__", "TicketDeviceClassPaymentMethod_", JoinHint.None);
					toReturn.Add(TicketDeviceClassPaymentMethodEntity.Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID, "TicketDeviceClassPaymentMethod_", string.Empty, JoinHint.None);
					break;
				case "OutputDevices":
					toReturn.Add(Relations.TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID, "TicketDeviceClassEntity__", "TicketDeviceClassOutputDevice_", JoinHint.None);
					toReturn.Add(TicketDeviceClassOutputDeviceEntity.Relations.OutputDeviceEntityUsingOutputDeviceID, "TicketDeviceClassOutputDevice_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticketDeviceClassOutputDevice", (!this.MarkedForDeletion?_ticketDeviceClassOutputDevice:null));
			info.AddValue("_alwaysFetchTicketDeviceClassOutputDevice", _alwaysFetchTicketDeviceClassOutputDevice);
			info.AddValue("_alreadyFetchedTicketDeviceClassOutputDevice", _alreadyFetchedTicketDeviceClassOutputDevice);
			info.AddValue("_ticketDeviceClassPaymentMethods", (!this.MarkedForDeletion?_ticketDeviceClassPaymentMethods:null));
			info.AddValue("_alwaysFetchTicketDeviceClassPaymentMethods", _alwaysFetchTicketDeviceClassPaymentMethods);
			info.AddValue("_alreadyFetchedTicketDeviceClassPaymentMethods", _alreadyFetchedTicketDeviceClassPaymentMethods);
			info.AddValue("_devicePaymentMethodCollection", (!this.MarkedForDeletion?_devicePaymentMethodCollection:null));
			info.AddValue("_alwaysFetchDevicePaymentMethodCollection", _alwaysFetchDevicePaymentMethodCollection);
			info.AddValue("_alreadyFetchedDevicePaymentMethodCollection", _alreadyFetchedDevicePaymentMethodCollection);
			info.AddValue("_outputDevices", (!this.MarkedForDeletion?_outputDevices:null));
			info.AddValue("_alwaysFetchOutputDevices", _alwaysFetchOutputDevices);
			info.AddValue("_alreadyFetchedOutputDevices", _alreadyFetchedOutputDevices);
			info.AddValue("_calendar", (!this.MarkedForDeletion?_calendar:null));
			info.AddValue("_calendarReturnsNewIfNotFound", _calendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCalendar", _alwaysFetchCalendar);
			info.AddValue("_alreadyFetchedCalendar", _alreadyFetchedCalendar);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_eticket", (!this.MarkedForDeletion?_eticket:null));
			info.AddValue("_eticketReturnsNewIfNotFound", _eticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEticket", _alwaysFetchEticket);
			info.AddValue("_alreadyFetchedEticket", _alreadyFetchedEticket);
			info.AddValue("_fareMatrix", (!this.MarkedForDeletion?_fareMatrix:null));
			info.AddValue("_fareMatrixReturnsNewIfNotFound", _fareMatrixReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareMatrix", _alwaysFetchFareMatrix);
			info.AddValue("_alreadyFetchedFareMatrix", _alreadyFetchedFareMatrix);
			info.AddValue("_cancellationLayout", (!this.MarkedForDeletion?_cancellationLayout:null));
			info.AddValue("_cancellationLayoutReturnsNewIfNotFound", _cancellationLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCancellationLayout", _alwaysFetchCancellationLayout);
			info.AddValue("_alreadyFetchedCancellationLayout", _alreadyFetchedCancellationLayout);
			info.AddValue("_groupLayout", (!this.MarkedForDeletion?_groupLayout:null));
			info.AddValue("_groupLayoutReturnsNewIfNotFound", _groupLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGroupLayout", _alwaysFetchGroupLayout);
			info.AddValue("_alreadyFetchedGroupLayout", _alreadyFetchedGroupLayout);
			info.AddValue("_printLayout", (!this.MarkedForDeletion?_printLayout:null));
			info.AddValue("_printLayoutReturnsNewIfNotFound", _printLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrintLayout", _alwaysFetchPrintLayout);
			info.AddValue("_alreadyFetchedPrintLayout", _alreadyFetchedPrintLayout);
			info.AddValue("_receiptLayout", (!this.MarkedForDeletion?_receiptLayout:null));
			info.AddValue("_receiptLayoutReturnsNewIfNotFound", _receiptLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReceiptLayout", _alwaysFetchReceiptLayout);
			info.AddValue("_alreadyFetchedReceiptLayout", _alreadyFetchedReceiptLayout);
			info.AddValue("_rulePeriod", (!this.MarkedForDeletion?_rulePeriod:null));
			info.AddValue("_rulePeriodReturnsNewIfNotFound", _rulePeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRulePeriod", _alwaysFetchRulePeriod);
			info.AddValue("_alreadyFetchedRulePeriod", _alreadyFetchedRulePeriod);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_ticketSelectionMode", (!this.MarkedForDeletion?_ticketSelectionMode:null));
			info.AddValue("_ticketSelectionModeReturnsNewIfNotFound", _ticketSelectionModeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketSelectionMode", _alwaysFetchTicketSelectionMode);
			info.AddValue("_alreadyFetchedTicketSelectionMode", _alreadyFetchedTicketSelectionMode);
			info.AddValue("_userKey1", (!this.MarkedForDeletion?_userKey1:null));
			info.AddValue("_userKey1ReturnsNewIfNotFound", _userKey1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserKey1", _alwaysFetchUserKey1);
			info.AddValue("_alreadyFetchedUserKey1", _alreadyFetchedUserKey1);
			info.AddValue("_userKey2", (!this.MarkedForDeletion?_userKey2:null));
			info.AddValue("_userKey2ReturnsNewIfNotFound", _userKey2ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserKey2", _alwaysFetchUserKey2);
			info.AddValue("_alreadyFetchedUserKey2", _alreadyFetchedUserKey2);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Calendar":
					_alreadyFetchedCalendar = true;
					this.Calendar = (CalendarEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Eticket":
					_alreadyFetchedEticket = true;
					this.Eticket = (EticketEntity)entity;
					break;
				case "FareMatrix":
					_alreadyFetchedFareMatrix = true;
					this.FareMatrix = (FareMatrixEntity)entity;
					break;
				case "CancellationLayout":
					_alreadyFetchedCancellationLayout = true;
					this.CancellationLayout = (LayoutEntity)entity;
					break;
				case "GroupLayout":
					_alreadyFetchedGroupLayout = true;
					this.GroupLayout = (LayoutEntity)entity;
					break;
				case "PrintLayout":
					_alreadyFetchedPrintLayout = true;
					this.PrintLayout = (LayoutEntity)entity;
					break;
				case "ReceiptLayout":
					_alreadyFetchedReceiptLayout = true;
					this.ReceiptLayout = (LayoutEntity)entity;
					break;
				case "RulePeriod":
					_alreadyFetchedRulePeriod = true;
					this.RulePeriod = (RulePeriodEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "TicketSelectionMode":
					_alreadyFetchedTicketSelectionMode = true;
					this.TicketSelectionMode = (TicketSelectionModeEntity)entity;
					break;
				case "UserKey1":
					_alreadyFetchedUserKey1 = true;
					this.UserKey1 = (UserKeyEntity)entity;
					break;
				case "UserKey2":
					_alreadyFetchedUserKey2 = true;
					this.UserKey2 = (UserKeyEntity)entity;
					break;
				case "TicketDeviceClassOutputDevice":
					_alreadyFetchedTicketDeviceClassOutputDevice = true;
					if(entity!=null)
					{
						this.TicketDeviceClassOutputDevice.Add((TicketDeviceClassOutputDeviceEntity)entity);
					}
					break;
				case "TicketDeviceClassPaymentMethods":
					_alreadyFetchedTicketDeviceClassPaymentMethods = true;
					if(entity!=null)
					{
						this.TicketDeviceClassPaymentMethods.Add((TicketDeviceClassPaymentMethodEntity)entity);
					}
					break;
				case "DevicePaymentMethodCollection":
					_alreadyFetchedDevicePaymentMethodCollection = true;
					if(entity!=null)
					{
						this.DevicePaymentMethodCollection.Add((DevicePaymentMethodEntity)entity);
					}
					break;
				case "OutputDevices":
					_alreadyFetchedOutputDevices = true;
					if(entity!=null)
					{
						this.OutputDevices.Add((OutputDeviceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Calendar":
					SetupSyncCalendar(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Eticket":
					SetupSyncEticket(relatedEntity);
					break;
				case "FareMatrix":
					SetupSyncFareMatrix(relatedEntity);
					break;
				case "CancellationLayout":
					SetupSyncCancellationLayout(relatedEntity);
					break;
				case "GroupLayout":
					SetupSyncGroupLayout(relatedEntity);
					break;
				case "PrintLayout":
					SetupSyncPrintLayout(relatedEntity);
					break;
				case "ReceiptLayout":
					SetupSyncReceiptLayout(relatedEntity);
					break;
				case "RulePeriod":
					SetupSyncRulePeriod(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "TicketSelectionMode":
					SetupSyncTicketSelectionMode(relatedEntity);
					break;
				case "UserKey1":
					SetupSyncUserKey1(relatedEntity);
					break;
				case "UserKey2":
					SetupSyncUserKey2(relatedEntity);
					break;
				case "TicketDeviceClassOutputDevice":
					_ticketDeviceClassOutputDevice.Add((TicketDeviceClassOutputDeviceEntity)relatedEntity);
					break;
				case "TicketDeviceClassPaymentMethods":
					_ticketDeviceClassPaymentMethods.Add((TicketDeviceClassPaymentMethodEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Calendar":
					DesetupSyncCalendar(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Eticket":
					DesetupSyncEticket(false, true);
					break;
				case "FareMatrix":
					DesetupSyncFareMatrix(false, true);
					break;
				case "CancellationLayout":
					DesetupSyncCancellationLayout(false, true);
					break;
				case "GroupLayout":
					DesetupSyncGroupLayout(false, true);
					break;
				case "PrintLayout":
					DesetupSyncPrintLayout(false, true);
					break;
				case "ReceiptLayout":
					DesetupSyncReceiptLayout(false, true);
					break;
				case "RulePeriod":
					DesetupSyncRulePeriod(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "TicketSelectionMode":
					DesetupSyncTicketSelectionMode(false, true);
					break;
				case "UserKey1":
					DesetupSyncUserKey1(false, true);
					break;
				case "UserKey2":
					DesetupSyncUserKey2(false, true);
					break;
				case "TicketDeviceClassOutputDevice":
					this.PerformRelatedEntityRemoval(_ticketDeviceClassOutputDevice, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClassPaymentMethods":
					this.PerformRelatedEntityRemoval(_ticketDeviceClassPaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_calendar!=null)
			{
				toReturn.Add(_calendar);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_eticket!=null)
			{
				toReturn.Add(_eticket);
			}
			if(_fareMatrix!=null)
			{
				toReturn.Add(_fareMatrix);
			}
			if(_cancellationLayout!=null)
			{
				toReturn.Add(_cancellationLayout);
			}
			if(_groupLayout!=null)
			{
				toReturn.Add(_groupLayout);
			}
			if(_printLayout!=null)
			{
				toReturn.Add(_printLayout);
			}
			if(_receiptLayout!=null)
			{
				toReturn.Add(_receiptLayout);
			}
			if(_rulePeriod!=null)
			{
				toReturn.Add(_rulePeriod);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_ticketSelectionMode!=null)
			{
				toReturn.Add(_ticketSelectionMode);
			}
			if(_userKey1!=null)
			{
				toReturn.Add(_userKey1);
			}
			if(_userKey2!=null)
			{
				toReturn.Add(_userKey2);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ticketDeviceClassOutputDevice);
			toReturn.Add(_ticketDeviceClassPaymentMethods);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketDeviceClassID)
		{
			return FetchUsingPK(ticketDeviceClassID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketDeviceClassID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketDeviceClassID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketDeviceClassID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketDeviceClassID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketDeviceClassRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassOutputDeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, _ticketDeviceClassOutputDevice.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassOutputDeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, _ticketDeviceClassOutputDevice.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClassOutputDevice || forceFetch || _alwaysFetchTicketDeviceClassOutputDevice) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClassOutputDevice);
				_ticketDeviceClassOutputDevice.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClassOutputDevice.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClassOutputDevice.GetMultiManyToOne(null, this, filter);
				_ticketDeviceClassOutputDevice.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClassOutputDevice = true;
			}
			return _ticketDeviceClassOutputDevice;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClassOutputDevice'. These settings will be taken into account
		/// when the property TicketDeviceClassOutputDevice is requested or GetMultiTicketDeviceClassOutputDevice is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClassOutputDevice(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClassOutputDevice.SortClauses=sortClauses;
			_ticketDeviceClassOutputDevice.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethods(bool forceFetch)
		{
			return GetMultiTicketDeviceClassPaymentMethods(forceFetch, _ticketDeviceClassPaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClassPaymentMethods(forceFetch, _ticketDeviceClassPaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClassPaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection GetMultiTicketDeviceClassPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClassPaymentMethods || forceFetch || _alwaysFetchTicketDeviceClassPaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClassPaymentMethods);
				_ticketDeviceClassPaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClassPaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClassPaymentMethods.GetMultiManyToOne(null, this, filter);
				_ticketDeviceClassPaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClassPaymentMethods = true;
			}
			return _ticketDeviceClassPaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClassPaymentMethods'. These settings will be taken into account
		/// when the property TicketDeviceClassPaymentMethods is requested or GetMultiTicketDeviceClassPaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClassPaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClassPaymentMethods.SortClauses=sortClauses;
			_ticketDeviceClassPaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DevicePaymentMethodEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DevicePaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection GetMultiDevicePaymentMethodCollection(bool forceFetch)
		{
			return GetMultiDevicePaymentMethodCollection(forceFetch, _devicePaymentMethodCollection.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DevicePaymentMethodEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection GetMultiDevicePaymentMethodCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDevicePaymentMethodCollection || forceFetch || _alwaysFetchDevicePaymentMethodCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_devicePaymentMethodCollection);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketDeviceClassFields.TicketDeviceClassID, ComparisonOperator.Equal, this.TicketDeviceClassID, "TicketDeviceClassEntity__"));
				_devicePaymentMethodCollection.SuppressClearInGetMulti=!forceFetch;
				_devicePaymentMethodCollection.EntityFactoryToUse = entityFactoryToUse;
				_devicePaymentMethodCollection.GetMulti(filter, GetRelationsForField("DevicePaymentMethodCollection"));
				_devicePaymentMethodCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDevicePaymentMethodCollection = true;
			}
			return _devicePaymentMethodCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DevicePaymentMethodCollection'. These settings will be taken into account
		/// when the property DevicePaymentMethodCollection is requested or GetMultiDevicePaymentMethodCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDevicePaymentMethodCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_devicePaymentMethodCollection.SortClauses=sortClauses;
			_devicePaymentMethodCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OutputDeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OutputDeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OutputDeviceCollection GetMultiOutputDevices(bool forceFetch)
		{
			return GetMultiOutputDevices(forceFetch, _outputDevices.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OutputDeviceEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OutputDeviceCollection GetMultiOutputDevices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOutputDevices || forceFetch || _alwaysFetchOutputDevices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_outputDevices);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketDeviceClassFields.TicketDeviceClassID, ComparisonOperator.Equal, this.TicketDeviceClassID, "TicketDeviceClassEntity__"));
				_outputDevices.SuppressClearInGetMulti=!forceFetch;
				_outputDevices.EntityFactoryToUse = entityFactoryToUse;
				_outputDevices.GetMulti(filter, GetRelationsForField("OutputDevices"));
				_outputDevices.SuppressClearInGetMulti=false;
				_alreadyFetchedOutputDevices = true;
			}
			return _outputDevices;
		}

		/// <summary> Sets the collection parameters for the collection for 'OutputDevices'. These settings will be taken into account
		/// when the property OutputDevices is requested or GetMultiOutputDevices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOutputDevices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_outputDevices.SortClauses=sortClauses;
			_outputDevices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public CalendarEntity GetSingleCalendar()
		{
			return GetSingleCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'CalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CalendarEntity' which is related to this entity.</returns>
		public virtual CalendarEntity GetSingleCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedCalendar || forceFetch || _alwaysFetchCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CalendarEntityUsingCalendarID);
				CalendarEntity newEntity = new CalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CalendarID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_calendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Calendar = newEntity;
				_alreadyFetchedCalendar = fetchResult;
			}
			return _calendar;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'EticketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EticketEntity' which is related to this entity.</returns>
		public EticketEntity GetSingleEticket()
		{
			return GetSingleEticket(false);
		}

		/// <summary> Retrieves the related entity of type 'EticketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EticketEntity' which is related to this entity.</returns>
		public virtual EticketEntity GetSingleEticket(bool forceFetch)
		{
			if( ( !_alreadyFetchedEticket || forceFetch || _alwaysFetchEticket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EticketEntityUsingEtickID);
				EticketEntity newEntity = new EticketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EtickID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EticketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_eticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Eticket = newEntity;
				_alreadyFetchedEticket = fetchResult;
			}
			return _eticket;
		}


		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public FareMatrixEntity GetSingleFareMatrix()
		{
			return GetSingleFareMatrix(false);
		}

		/// <summary> Retrieves the related entity of type 'FareMatrixEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareMatrixEntity' which is related to this entity.</returns>
		public virtual FareMatrixEntity GetSingleFareMatrix(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareMatrix || forceFetch || _alwaysFetchFareMatrix) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareMatrixEntityUsingMatrixID);
				FareMatrixEntity newEntity = new FareMatrixEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MatrixID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareMatrixEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareMatrixReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareMatrix = newEntity;
				_alreadyFetchedFareMatrix = fetchResult;
			}
			return _fareMatrix;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleCancellationLayout()
		{
			return GetSingleCancellationLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleCancellationLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedCancellationLayout || forceFetch || _alwaysFetchCancellationLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingCancellationLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CancellationLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cancellationLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CancellationLayout = newEntity;
				_alreadyFetchedCancellationLayout = fetchResult;
			}
			return _cancellationLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleGroupLayout()
		{
			return GetSingleGroupLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleGroupLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedGroupLayout || forceFetch || _alwaysFetchGroupLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingGroupLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.GroupLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_groupLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.GroupLayout = newEntity;
				_alreadyFetchedGroupLayout = fetchResult;
			}
			return _groupLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSinglePrintLayout()
		{
			return GetSinglePrintLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSinglePrintLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrintLayout || forceFetch || _alwaysFetchPrintLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingPrintLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrintLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PrintLayout = newEntity;
				_alreadyFetchedPrintLayout = fetchResult;
			}
			return _printLayout;
		}


		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleReceiptLayout()
		{
			return GetSingleReceiptLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleReceiptLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedReceiptLayout || forceFetch || _alwaysFetchReceiptLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingReceiptLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReceiptLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_receiptLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReceiptLayout = newEntity;
				_alreadyFetchedReceiptLayout = fetchResult;
			}
			return _receiptLayout;
		}


		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public RulePeriodEntity GetSingleRulePeriod()
		{
			return GetSingleRulePeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public virtual RulePeriodEntity GetSingleRulePeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedRulePeriod || forceFetch || _alwaysFetchRulePeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RulePeriodEntityUsingRulePeriodID);
				RulePeriodEntity newEntity = new RulePeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RulePeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RulePeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_rulePeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RulePeriod = newEntity;
				_alreadyFetchedRulePeriod = fetchResult;
			}
			return _rulePeriod;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'TicketSelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketSelectionModeEntity' which is related to this entity.</returns>
		public TicketSelectionModeEntity GetSingleTicketSelectionMode()
		{
			return GetSingleTicketSelectionMode(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketSelectionModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketSelectionModeEntity' which is related to this entity.</returns>
		public virtual TicketSelectionModeEntity GetSingleTicketSelectionMode(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketSelectionMode || forceFetch || _alwaysFetchTicketSelectionMode) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketSelectionModeEntityUsingSelectionMode);
				TicketSelectionModeEntity newEntity = new TicketSelectionModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SelectionMode.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketSelectionModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketSelectionModeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketSelectionMode = newEntity;
				_alreadyFetchedTicketSelectionMode = fetchResult;
			}
			return _ticketSelectionMode;
		}


		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public UserKeyEntity GetSingleUserKey1()
		{
			return GetSingleUserKey1(false);
		}

		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public virtual UserKeyEntity GetSingleUserKey1(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserKey1 || forceFetch || _alwaysFetchUserKey1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserKeyEntityUsingKey1);
				UserKeyEntity newEntity = new UserKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Key1.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userKey1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserKey1 = newEntity;
				_alreadyFetchedUserKey1 = fetchResult;
			}
			return _userKey1;
		}


		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public UserKeyEntity GetSingleUserKey2()
		{
			return GetSingleUserKey2(false);
		}

		/// <summary> Retrieves the related entity of type 'UserKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserKeyEntity' which is related to this entity.</returns>
		public virtual UserKeyEntity GetSingleUserKey2(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserKey2 || forceFetch || _alwaysFetchUserKey2) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserKeyEntityUsingKey2);
				UserKeyEntity newEntity = new UserKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Key2.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userKey2ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserKey2 = newEntity;
				_alreadyFetchedUserKey2 = fetchResult;
			}
			return _userKey2;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Calendar", _calendar);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Eticket", _eticket);
			toReturn.Add("FareMatrix", _fareMatrix);
			toReturn.Add("CancellationLayout", _cancellationLayout);
			toReturn.Add("GroupLayout", _groupLayout);
			toReturn.Add("PrintLayout", _printLayout);
			toReturn.Add("ReceiptLayout", _receiptLayout);
			toReturn.Add("RulePeriod", _rulePeriod);
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("TicketSelectionMode", _ticketSelectionMode);
			toReturn.Add("UserKey1", _userKey1);
			toReturn.Add("UserKey2", _userKey2);
			toReturn.Add("TicketDeviceClassOutputDevice", _ticketDeviceClassOutputDevice);
			toReturn.Add("TicketDeviceClassPaymentMethods", _ticketDeviceClassPaymentMethods);
			toReturn.Add("DevicePaymentMethodCollection", _devicePaymentMethodCollection);
			toReturn.Add("OutputDevices", _outputDevices);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="validator">The validator object for this TicketDeviceClassEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketDeviceClassID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketDeviceClassID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ticketDeviceClassOutputDevice = new VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection();
			_ticketDeviceClassOutputDevice.SetContainingEntityInfo(this, "TicketDeviceClass");

			_ticketDeviceClassPaymentMethods = new VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection();
			_ticketDeviceClassPaymentMethods.SetContainingEntityInfo(this, "TicketDeviceClasses");
			_devicePaymentMethodCollection = new VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection();
			_outputDevices = new VarioSL.Entities.CollectionClasses.OutputDeviceCollection();
			_calendarReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_eticketReturnsNewIfNotFound = false;
			_fareMatrixReturnsNewIfNotFound = false;
			_cancellationLayoutReturnsNewIfNotFound = false;
			_groupLayoutReturnsNewIfNotFound = false;
			_printLayoutReturnsNewIfNotFound = false;
			_receiptLayoutReturnsNewIfNotFound = false;
			_rulePeriodReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			_ticketSelectionModeReturnsNewIfNotFound = false;
			_userKey1ReturnsNewIfNotFound = false;
			_userKey2ReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Active", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CalendarID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickCancellationQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickEmission", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Key1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Key2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MatrixID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RulePeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SelectionMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketDeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _calendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.CalendarEntityUsingCalendarIDStatic, true, signalRelatedEntity, "TicketDeviceClass", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.CalendarID } );		
			_calendar = null;
		}
		
		/// <summary> setups the sync logic for member _calendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCalendar(IEntityCore relatedEntity)
		{
			if(_calendar!=relatedEntity)
			{		
				DesetupSyncCalendar(true, true);
				_calendar = (CalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _calendar, new PropertyChangedEventHandler( OnCalendarPropertyChanged ), "Calendar", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.CalendarEntityUsingCalendarIDStatic, true, ref _alreadyFetchedCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "TicketDeviceClasses", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _eticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEticket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _eticket, new PropertyChangedEventHandler( OnEticketPropertyChanged ), "Eticket", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.EticketEntityUsingEtickIDStatic, true, signalRelatedEntity, "TicketDeviceClass", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.EtickID } );		
			_eticket = null;
		}
		
		/// <summary> setups the sync logic for member _eticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEticket(IEntityCore relatedEntity)
		{
			if(_eticket!=relatedEntity)
			{		
				DesetupSyncEticket(true, true);
				_eticket = (EticketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _eticket, new PropertyChangedEventHandler( OnEticketPropertyChanged ), "Eticket", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.EticketEntityUsingEtickIDStatic, true, ref _alreadyFetchedEticket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEticketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareMatrix</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareMatrix(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.FareMatrixEntityUsingMatrixIDStatic, true, signalRelatedEntity, "TicketDeviceClass", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.MatrixID } );		
			_fareMatrix = null;
		}
		
		/// <summary> setups the sync logic for member _fareMatrix</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareMatrix(IEntityCore relatedEntity)
		{
			if(_fareMatrix!=relatedEntity)
			{		
				DesetupSyncFareMatrix(true, true);
				_fareMatrix = (FareMatrixEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareMatrix, new PropertyChangedEventHandler( OnFareMatrixPropertyChanged ), "FareMatrix", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.FareMatrixEntityUsingMatrixIDStatic, true, ref _alreadyFetchedFareMatrix, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareMatrixPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cancellationLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCancellationLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cancellationLayout, new PropertyChangedEventHandler( OnCancellationLayoutPropertyChanged ), "CancellationLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingCancellationLayoutIDStatic, true, signalRelatedEntity, "TicketDeviceclassesCancellationLayout", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.CancellationLayoutID } );		
			_cancellationLayout = null;
		}
		
		/// <summary> setups the sync logic for member _cancellationLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCancellationLayout(IEntityCore relatedEntity)
		{
			if(_cancellationLayout!=relatedEntity)
			{		
				DesetupSyncCancellationLayout(true, true);
				_cancellationLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cancellationLayout, new PropertyChangedEventHandler( OnCancellationLayoutPropertyChanged ), "CancellationLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingCancellationLayoutIDStatic, true, ref _alreadyFetchedCancellationLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCancellationLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _groupLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGroupLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _groupLayout, new PropertyChangedEventHandler( OnGroupLayoutPropertyChanged ), "GroupLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingGroupLayoutIDStatic, true, signalRelatedEntity, "TicketDeviceclassesGroupLayout", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.GroupLayoutID } );		
			_groupLayout = null;
		}
		
		/// <summary> setups the sync logic for member _groupLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGroupLayout(IEntityCore relatedEntity)
		{
			if(_groupLayout!=relatedEntity)
			{		
				DesetupSyncGroupLayout(true, true);
				_groupLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _groupLayout, new PropertyChangedEventHandler( OnGroupLayoutPropertyChanged ), "GroupLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingGroupLayoutIDStatic, true, ref _alreadyFetchedGroupLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGroupLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrintLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printLayout, new PropertyChangedEventHandler( OnPrintLayoutPropertyChanged ), "PrintLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingPrintLayoutIDStatic, true, signalRelatedEntity, "TicketDeviceclassesPrintLayout", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.PrintLayoutID } );		
			_printLayout = null;
		}
		
		/// <summary> setups the sync logic for member _printLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrintLayout(IEntityCore relatedEntity)
		{
			if(_printLayout!=relatedEntity)
			{		
				DesetupSyncPrintLayout(true, true);
				_printLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printLayout, new PropertyChangedEventHandler( OnPrintLayoutPropertyChanged ), "PrintLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingPrintLayoutIDStatic, true, ref _alreadyFetchedPrintLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrintLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _receiptLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReceiptLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _receiptLayout, new PropertyChangedEventHandler( OnReceiptLayoutPropertyChanged ), "ReceiptLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingReceiptLayoutIDStatic, true, signalRelatedEntity, "TicketDeviceclassesReceiptLayout", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.ReceiptLayoutID } );		
			_receiptLayout = null;
		}
		
		/// <summary> setups the sync logic for member _receiptLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReceiptLayout(IEntityCore relatedEntity)
		{
			if(_receiptLayout!=relatedEntity)
			{		
				DesetupSyncReceiptLayout(true, true);
				_receiptLayout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _receiptLayout, new PropertyChangedEventHandler( OnReceiptLayoutPropertyChanged ), "ReceiptLayout", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.LayoutEntityUsingReceiptLayoutIDStatic, true, ref _alreadyFetchedReceiptLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReceiptLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _rulePeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRulePeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, signalRelatedEntity, "TicketDeviceClass", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.RulePeriodID } );		
			_rulePeriod = null;
		}
		
		/// <summary> setups the sync logic for member _rulePeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRulePeriod(IEntityCore relatedEntity)
		{
			if(_rulePeriod!=relatedEntity)
			{		
				DesetupSyncRulePeriod(true, true);
				_rulePeriod = (RulePeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.RulePeriodEntityUsingRulePeriodIDStatic, true, ref _alreadyFetchedRulePeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRulePeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "TicketDeviceClasses", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketSelectionMode</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketSelectionMode(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketSelectionMode, new PropertyChangedEventHandler( OnTicketSelectionModePropertyChanged ), "TicketSelectionMode", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.TicketSelectionModeEntityUsingSelectionModeStatic, true, signalRelatedEntity, "TicketDeviceClasses", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.SelectionMode } );		
			_ticketSelectionMode = null;
		}
		
		/// <summary> setups the sync logic for member _ticketSelectionMode</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketSelectionMode(IEntityCore relatedEntity)
		{
			if(_ticketSelectionMode!=relatedEntity)
			{		
				DesetupSyncTicketSelectionMode(true, true);
				_ticketSelectionMode = (TicketSelectionModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketSelectionMode, new PropertyChangedEventHandler( OnTicketSelectionModePropertyChanged ), "TicketSelectionMode", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.TicketSelectionModeEntityUsingSelectionModeStatic, true, ref _alreadyFetchedTicketSelectionMode, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketSelectionModePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userKey1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserKey1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userKey1, new PropertyChangedEventHandler( OnUserKey1PropertyChanged ), "UserKey1", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.UserKeyEntityUsingKey1Static, true, signalRelatedEntity, "TicketDeviceclassesKey1", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.Key1 } );		
			_userKey1 = null;
		}
		
		/// <summary> setups the sync logic for member _userKey1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserKey1(IEntityCore relatedEntity)
		{
			if(_userKey1!=relatedEntity)
			{		
				DesetupSyncUserKey1(true, true);
				_userKey1 = (UserKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userKey1, new PropertyChangedEventHandler( OnUserKey1PropertyChanged ), "UserKey1", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.UserKeyEntityUsingKey1Static, true, ref _alreadyFetchedUserKey1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserKey1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userKey2</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserKey2(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userKey2, new PropertyChangedEventHandler( OnUserKey2PropertyChanged ), "UserKey2", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.UserKeyEntityUsingKey2Static, true, signalRelatedEntity, "TicketDeviceclassesKey2", resetFKFields, new int[] { (int)TicketDeviceClassFieldIndex.Key2 } );		
			_userKey2 = null;
		}
		
		/// <summary> setups the sync logic for member _userKey2</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserKey2(IEntityCore relatedEntity)
		{
			if(_userKey2!=relatedEntity)
			{		
				DesetupSyncUserKey2(true, true);
				_userKey2 = (UserKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userKey2, new PropertyChangedEventHandler( OnUserKey2PropertyChanged ), "UserKey2", VarioSL.Entities.RelationClasses.StaticTicketDeviceClassRelations.UserKeyEntityUsingKey2Static, true, ref _alreadyFetchedUserKey2, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserKey2PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketDeviceClassID">PK value for TicketDeviceClass which data should be fetched into this TicketDeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketDeviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketDeviceClassFieldIndex.TicketDeviceClassID].ForcedCurrentValueWrite(ticketDeviceClassID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketDeviceClassDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketDeviceClassEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketDeviceClassRelations Relations
		{
			get	{ return new TicketDeviceClassRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClassOutputDevice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClassOutputDevice
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClassOutputDevice")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity, 0, null, null, null, "TicketDeviceClassOutputDevice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClassPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClassPaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClassPaymentMethods")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity, 0, null, null, null, "TicketDeviceClassPaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DevicePaymentMethod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevicePaymentMethodCollection
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID;
				intermediateRelation.SetAliases(string.Empty, "TicketDeviceClassPaymentMethod_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.DevicePaymentMethodEntity, 0, null, null, GetRelationsForField("DevicePaymentMethodCollection"), "DevicePaymentMethodCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutputDevice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutputDevices
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID;
				intermediateRelation.SetAliases(string.Empty, "TicketDeviceClassOutputDevice_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OutputDeviceCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.OutputDeviceEntity, 0, null, null, GetRelationsForField("OutputDevices"), "OutputDevices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendar")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Eticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEticket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EticketCollection(), (IEntityRelation)GetRelationsForField("Eticket")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.EticketEntity, 0, null, null, null, "Eticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrix
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrix")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrix", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCancellationLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("CancellationLayout")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "CancellationLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGroupLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("GroupLayout")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "GroupLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("PrintLayout")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "PrintLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReceiptLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("ReceiptLayout")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "ReceiptLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriod")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketSelectionMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketSelectionMode
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketSelectionModeCollection(), (IEntityRelation)GetRelationsForField("TicketSelectionMode")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketSelectionModeEntity, 0, null, null, null, "TicketSelectionMode", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKey1
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKey1")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKey1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKey2
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKey2")[0], (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKey2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Active property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."ACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Active
		{
			get { return (Nullable<System.Boolean>)GetValue((int)TicketDeviceClassFieldIndex.Active, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.Active, value, true); }
		}

		/// <summary> The CalendarID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."CALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CalendarID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.CalendarID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.CalendarID, value, true); }
		}

		/// <summary> The CancellationLayoutID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."CANCELLATIONLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.CancellationLayoutID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.CancellationLayoutID, value, true); }
		}

		/// <summary> The CancellationQuantity property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."CANCELLATIONQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationQuantity
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.CancellationQuantity, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.CancellationQuantity, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DeviceClassID
		{
			get { return (System.Int64)GetValue((int)TicketDeviceClassFieldIndex.DeviceClassID, true); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The EtickCancellationQuantity property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."ETICKCANCELLATIONQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EtickCancellationQuantity
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.EtickCancellationQuantity, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.EtickCancellationQuantity, value, true); }
		}

		/// <summary> The EtickEmission property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."ETICKEMISSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> EtickEmission
		{
			get { return (Nullable<System.Int16>)GetValue((int)TicketDeviceClassFieldIndex.EtickEmission, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.EtickEmission, value, true); }
		}

		/// <summary> The EtickID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."ETICKID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EtickID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.EtickID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.EtickID, value, true); }
		}

		/// <summary> The GroupLayoutID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."GROUPLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GroupLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.GroupLayoutID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.GroupLayoutID, value, true); }
		}

		/// <summary> The Key1 property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."KEY1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Key1
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.Key1, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.Key1, value, true); }
		}

		/// <summary> The Key2 property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."KEY2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Key2
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.Key2, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.Key2, value, true); }
		}

		/// <summary> The MatrixID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."MATRIXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MatrixID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.MatrixID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.MatrixID, value, true); }
		}

		/// <summary> The Name property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TicketDeviceClassFieldIndex.Name, true); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.Name, value, true); }
		}

		/// <summary> The Price property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."PRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Price
		{
			get { return (Nullable<System.Int32>)GetValue((int)TicketDeviceClassFieldIndex.Price, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.Price, value, true); }
		}

		/// <summary> The PrintLayoutID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."PRINTLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.PrintLayoutID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.PrintLayoutID, value, true); }
		}

		/// <summary> The ReceiptLayoutID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."RECEIPTLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReceiptLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.ReceiptLayoutID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.ReceiptLayoutID, value, true); }
		}

		/// <summary> The RulePeriodID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."RULEPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RulePeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.RulePeriodID, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.RulePeriodID, value, true); }
		}

		/// <summary> The SelectionMode property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."SELECTIONMODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SelectionMode
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketDeviceClassFieldIndex.SelectionMode, false); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.SelectionMode, value, true); }
		}

		/// <summary> The TicketDeviceClassID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."TICKETDEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TicketDeviceClassID
		{
			get { return (System.Int64)GetValue((int)TicketDeviceClassFieldIndex.TicketDeviceClassID, true); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.TicketDeviceClassID, value, true); }
		}

		/// <summary> The TicketID property of the Entity TicketDeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_DEVICECLASS"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)TicketDeviceClassFieldIndex.TicketID, true); }
			set	{ SetValue((int)TicketDeviceClassFieldIndex.TicketID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClassOutputDevice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection TicketDeviceClassOutputDevice
		{
			get	{ return GetMultiTicketDeviceClassOutputDevice(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClassOutputDevice. When set to true, TicketDeviceClassOutputDevice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClassOutputDevice is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClassOutputDevice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClassOutputDevice
		{
			get	{ return _alwaysFetchTicketDeviceClassOutputDevice; }
			set	{ _alwaysFetchTicketDeviceClassOutputDevice = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClassOutputDevice already has been fetched. Setting this property to false when TicketDeviceClassOutputDevice has been fetched
		/// will clear the TicketDeviceClassOutputDevice collection well. Setting this property to true while TicketDeviceClassOutputDevice hasn't been fetched disables lazy loading for TicketDeviceClassOutputDevice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClassOutputDevice
		{
			get { return _alreadyFetchedTicketDeviceClassOutputDevice;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClassOutputDevice && !value && (_ticketDeviceClassOutputDevice != null))
				{
					_ticketDeviceClassOutputDevice.Clear();
				}
				_alreadyFetchedTicketDeviceClassOutputDevice = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClassPaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassPaymentMethodCollection TicketDeviceClassPaymentMethods
		{
			get	{ return GetMultiTicketDeviceClassPaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClassPaymentMethods. When set to true, TicketDeviceClassPaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClassPaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClassPaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClassPaymentMethods
		{
			get	{ return _alwaysFetchTicketDeviceClassPaymentMethods; }
			set	{ _alwaysFetchTicketDeviceClassPaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClassPaymentMethods already has been fetched. Setting this property to false when TicketDeviceClassPaymentMethods has been fetched
		/// will clear the TicketDeviceClassPaymentMethods collection well. Setting this property to true while TicketDeviceClassPaymentMethods hasn't been fetched disables lazy loading for TicketDeviceClassPaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClassPaymentMethods
		{
			get { return _alreadyFetchedTicketDeviceClassPaymentMethods;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClassPaymentMethods && !value && (_ticketDeviceClassPaymentMethods != null))
				{
					_ticketDeviceClassPaymentMethods.Clear();
				}
				_alreadyFetchedTicketDeviceClassPaymentMethods = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DevicePaymentMethodEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDevicePaymentMethodCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DevicePaymentMethodCollection DevicePaymentMethodCollection
		{
			get { return GetMultiDevicePaymentMethodCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DevicePaymentMethodCollection. When set to true, DevicePaymentMethodCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DevicePaymentMethodCollection is accessed. You can always execute a forced fetch by calling GetMultiDevicePaymentMethodCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevicePaymentMethodCollection
		{
			get	{ return _alwaysFetchDevicePaymentMethodCollection; }
			set	{ _alwaysFetchDevicePaymentMethodCollection = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DevicePaymentMethodCollection already has been fetched. Setting this property to false when DevicePaymentMethodCollection has been fetched
		/// will clear the DevicePaymentMethodCollection collection well. Setting this property to true while DevicePaymentMethodCollection hasn't been fetched disables lazy loading for DevicePaymentMethodCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevicePaymentMethodCollection
		{
			get { return _alreadyFetchedDevicePaymentMethodCollection;}
			set 
			{
				if(_alreadyFetchedDevicePaymentMethodCollection && !value && (_devicePaymentMethodCollection != null))
				{
					_devicePaymentMethodCollection.Clear();
				}
				_alreadyFetchedDevicePaymentMethodCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OutputDeviceEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOutputDevices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OutputDeviceCollection OutputDevices
		{
			get { return GetMultiOutputDevices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OutputDevices. When set to true, OutputDevices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutputDevices is accessed. You can always execute a forced fetch by calling GetMultiOutputDevices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutputDevices
		{
			get	{ return _alwaysFetchOutputDevices; }
			set	{ _alwaysFetchOutputDevices = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutputDevices already has been fetched. Setting this property to false when OutputDevices has been fetched
		/// will clear the OutputDevices collection well. Setting this property to true while OutputDevices hasn't been fetched disables lazy loading for OutputDevices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutputDevices
		{
			get { return _alreadyFetchedOutputDevices;}
			set 
			{
				if(_alreadyFetchedOutputDevices && !value && (_outputDevices != null))
				{
					_outputDevices.Clear();
				}
				_alreadyFetchedOutputDevices = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CalendarEntity Calendar
		{
			get	{ return GetSingleCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClass", "Calendar", _calendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Calendar. When set to true, Calendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendar is accessed. You can always execute a forced fetch by calling GetSingleCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendar
		{
			get	{ return _alwaysFetchCalendar; }
			set	{ _alwaysFetchCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendar already has been fetched. Setting this property to false when Calendar has been fetched
		/// will set Calendar to null as well. Setting this property to true while Calendar hasn't been fetched disables lazy loading for Calendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendar
		{
			get { return _alreadyFetchedCalendar;}
			set 
			{
				if(_alreadyFetchedCalendar && !value)
				{
					this.Calendar = null;
				}
				_alreadyFetchedCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Calendar is not found
		/// in the database. When set to true, Calendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CalendarReturnsNewIfNotFound
		{
			get	{ return _calendarReturnsNewIfNotFound; }
			set { _calendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClasses", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EticketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEticket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EticketEntity Eticket
		{
			get	{ return GetSingleEticket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEticket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClass", "Eticket", _eticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Eticket. When set to true, Eticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Eticket is accessed. You can always execute a forced fetch by calling GetSingleEticket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEticket
		{
			get	{ return _alwaysFetchEticket; }
			set	{ _alwaysFetchEticket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Eticket already has been fetched. Setting this property to false when Eticket has been fetched
		/// will set Eticket to null as well. Setting this property to true while Eticket hasn't been fetched disables lazy loading for Eticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEticket
		{
			get { return _alreadyFetchedEticket;}
			set 
			{
				if(_alreadyFetchedEticket && !value)
				{
					this.Eticket = null;
				}
				_alreadyFetchedEticket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Eticket is not found
		/// in the database. When set to true, Eticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EticketReturnsNewIfNotFound
		{
			get	{ return _eticketReturnsNewIfNotFound; }
			set { _eticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareMatrixEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareMatrix()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareMatrixEntity FareMatrix
		{
			get	{ return GetSingleFareMatrix(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareMatrix(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClass", "FareMatrix", _fareMatrix, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrix. When set to true, FareMatrix is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrix is accessed. You can always execute a forced fetch by calling GetSingleFareMatrix(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrix
		{
			get	{ return _alwaysFetchFareMatrix; }
			set	{ _alwaysFetchFareMatrix = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrix already has been fetched. Setting this property to false when FareMatrix has been fetched
		/// will set FareMatrix to null as well. Setting this property to true while FareMatrix hasn't been fetched disables lazy loading for FareMatrix</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrix
		{
			get { return _alreadyFetchedFareMatrix;}
			set 
			{
				if(_alreadyFetchedFareMatrix && !value)
				{
					this.FareMatrix = null;
				}
				_alreadyFetchedFareMatrix = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareMatrix is not found
		/// in the database. When set to true, FareMatrix will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareMatrixReturnsNewIfNotFound
		{
			get	{ return _fareMatrixReturnsNewIfNotFound; }
			set { _fareMatrixReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCancellationLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity CancellationLayout
		{
			get	{ return GetSingleCancellationLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCancellationLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesCancellationLayout", "CancellationLayout", _cancellationLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CancellationLayout. When set to true, CancellationLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CancellationLayout is accessed. You can always execute a forced fetch by calling GetSingleCancellationLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCancellationLayout
		{
			get	{ return _alwaysFetchCancellationLayout; }
			set	{ _alwaysFetchCancellationLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CancellationLayout already has been fetched. Setting this property to false when CancellationLayout has been fetched
		/// will set CancellationLayout to null as well. Setting this property to true while CancellationLayout hasn't been fetched disables lazy loading for CancellationLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCancellationLayout
		{
			get { return _alreadyFetchedCancellationLayout;}
			set 
			{
				if(_alreadyFetchedCancellationLayout && !value)
				{
					this.CancellationLayout = null;
				}
				_alreadyFetchedCancellationLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CancellationLayout is not found
		/// in the database. When set to true, CancellationLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CancellationLayoutReturnsNewIfNotFound
		{
			get	{ return _cancellationLayoutReturnsNewIfNotFound; }
			set { _cancellationLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGroupLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity GroupLayout
		{
			get	{ return GetSingleGroupLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGroupLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesGroupLayout", "GroupLayout", _groupLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for GroupLayout. When set to true, GroupLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GroupLayout is accessed. You can always execute a forced fetch by calling GetSingleGroupLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGroupLayout
		{
			get	{ return _alwaysFetchGroupLayout; }
			set	{ _alwaysFetchGroupLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property GroupLayout already has been fetched. Setting this property to false when GroupLayout has been fetched
		/// will set GroupLayout to null as well. Setting this property to true while GroupLayout hasn't been fetched disables lazy loading for GroupLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGroupLayout
		{
			get { return _alreadyFetchedGroupLayout;}
			set 
			{
				if(_alreadyFetchedGroupLayout && !value)
				{
					this.GroupLayout = null;
				}
				_alreadyFetchedGroupLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property GroupLayout is not found
		/// in the database. When set to true, GroupLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool GroupLayoutReturnsNewIfNotFound
		{
			get	{ return _groupLayoutReturnsNewIfNotFound; }
			set { _groupLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrintLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity PrintLayout
		{
			get	{ return GetSinglePrintLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrintLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesPrintLayout", "PrintLayout", _printLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PrintLayout. When set to true, PrintLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintLayout is accessed. You can always execute a forced fetch by calling GetSinglePrintLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintLayout
		{
			get	{ return _alwaysFetchPrintLayout; }
			set	{ _alwaysFetchPrintLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintLayout already has been fetched. Setting this property to false when PrintLayout has been fetched
		/// will set PrintLayout to null as well. Setting this property to true while PrintLayout hasn't been fetched disables lazy loading for PrintLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintLayout
		{
			get { return _alreadyFetchedPrintLayout;}
			set 
			{
				if(_alreadyFetchedPrintLayout && !value)
				{
					this.PrintLayout = null;
				}
				_alreadyFetchedPrintLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PrintLayout is not found
		/// in the database. When set to true, PrintLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrintLayoutReturnsNewIfNotFound
		{
			get	{ return _printLayoutReturnsNewIfNotFound; }
			set { _printLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReceiptLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity ReceiptLayout
		{
			get	{ return GetSingleReceiptLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReceiptLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesReceiptLayout", "ReceiptLayout", _receiptLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReceiptLayout. When set to true, ReceiptLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReceiptLayout is accessed. You can always execute a forced fetch by calling GetSingleReceiptLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReceiptLayout
		{
			get	{ return _alwaysFetchReceiptLayout; }
			set	{ _alwaysFetchReceiptLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReceiptLayout already has been fetched. Setting this property to false when ReceiptLayout has been fetched
		/// will set ReceiptLayout to null as well. Setting this property to true while ReceiptLayout hasn't been fetched disables lazy loading for ReceiptLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReceiptLayout
		{
			get { return _alreadyFetchedReceiptLayout;}
			set 
			{
				if(_alreadyFetchedReceiptLayout && !value)
				{
					this.ReceiptLayout = null;
				}
				_alreadyFetchedReceiptLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReceiptLayout is not found
		/// in the database. When set to true, ReceiptLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReceiptLayoutReturnsNewIfNotFound
		{
			get	{ return _receiptLayoutReturnsNewIfNotFound; }
			set { _receiptLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RulePeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRulePeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RulePeriodEntity RulePeriod
		{
			get	{ return GetSingleRulePeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRulePeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClass", "RulePeriod", _rulePeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriod. When set to true, RulePeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriod is accessed. You can always execute a forced fetch by calling GetSingleRulePeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriod
		{
			get	{ return _alwaysFetchRulePeriod; }
			set	{ _alwaysFetchRulePeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriod already has been fetched. Setting this property to false when RulePeriod has been fetched
		/// will set RulePeriod to null as well. Setting this property to true while RulePeriod hasn't been fetched disables lazy loading for RulePeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriod
		{
			get { return _alreadyFetchedRulePeriod;}
			set 
			{
				if(_alreadyFetchedRulePeriod && !value)
				{
					this.RulePeriod = null;
				}
				_alreadyFetchedRulePeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RulePeriod is not found
		/// in the database. When set to true, RulePeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RulePeriodReturnsNewIfNotFound
		{
			get	{ return _rulePeriodReturnsNewIfNotFound; }
			set { _rulePeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClasses", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketSelectionModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketSelectionMode()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketSelectionModeEntity TicketSelectionMode
		{
			get	{ return GetSingleTicketSelectionMode(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketSelectionMode(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceClasses", "TicketSelectionMode", _ticketSelectionMode, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketSelectionMode. When set to true, TicketSelectionMode is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketSelectionMode is accessed. You can always execute a forced fetch by calling GetSingleTicketSelectionMode(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketSelectionMode
		{
			get	{ return _alwaysFetchTicketSelectionMode; }
			set	{ _alwaysFetchTicketSelectionMode = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketSelectionMode already has been fetched. Setting this property to false when TicketSelectionMode has been fetched
		/// will set TicketSelectionMode to null as well. Setting this property to true while TicketSelectionMode hasn't been fetched disables lazy loading for TicketSelectionMode</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketSelectionMode
		{
			get { return _alreadyFetchedTicketSelectionMode;}
			set 
			{
				if(_alreadyFetchedTicketSelectionMode && !value)
				{
					this.TicketSelectionMode = null;
				}
				_alreadyFetchedTicketSelectionMode = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketSelectionMode is not found
		/// in the database. When set to true, TicketSelectionMode will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketSelectionModeReturnsNewIfNotFound
		{
			get	{ return _ticketSelectionModeReturnsNewIfNotFound; }
			set { _ticketSelectionModeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserKey1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserKeyEntity UserKey1
		{
			get	{ return GetSingleUserKey1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserKey1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesKey1", "UserKey1", _userKey1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserKey1. When set to true, UserKey1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKey1 is accessed. You can always execute a forced fetch by calling GetSingleUserKey1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKey1
		{
			get	{ return _alwaysFetchUserKey1; }
			set	{ _alwaysFetchUserKey1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKey1 already has been fetched. Setting this property to false when UserKey1 has been fetched
		/// will set UserKey1 to null as well. Setting this property to true while UserKey1 hasn't been fetched disables lazy loading for UserKey1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKey1
		{
			get { return _alreadyFetchedUserKey1;}
			set 
			{
				if(_alreadyFetchedUserKey1 && !value)
				{
					this.UserKey1 = null;
				}
				_alreadyFetchedUserKey1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserKey1 is not found
		/// in the database. When set to true, UserKey1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserKey1ReturnsNewIfNotFound
		{
			get	{ return _userKey1ReturnsNewIfNotFound; }
			set { _userKey1ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserKey2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserKeyEntity UserKey2
		{
			get	{ return GetSingleUserKey2(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserKey2(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketDeviceclassesKey2", "UserKey2", _userKey2, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserKey2. When set to true, UserKey2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKey2 is accessed. You can always execute a forced fetch by calling GetSingleUserKey2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKey2
		{
			get	{ return _alwaysFetchUserKey2; }
			set	{ _alwaysFetchUserKey2 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKey2 already has been fetched. Setting this property to false when UserKey2 has been fetched
		/// will set UserKey2 to null as well. Setting this property to true while UserKey2 hasn't been fetched disables lazy loading for UserKey2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKey2
		{
			get { return _alreadyFetchedUserKey2;}
			set 
			{
				if(_alreadyFetchedUserKey2 && !value)
				{
					this.UserKey2 = null;
				}
				_alreadyFetchedUserKey2 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserKey2 is not found
		/// in the database. When set to true, UserKey2 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserKey2ReturnsNewIfNotFound
		{
			get	{ return _userKey2ReturnsNewIfNotFound; }
			set { _userKey2ReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
