﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardChipType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardChipTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection	_ticketCardChipTypes;
		private bool	_alwaysFetchTicketCardChipTypes, _alreadyFetchedTicketCardChipTypes;
		private VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection	_cardPhysicalDetails;
		private bool	_alwaysFetchCardPhysicalDetails, _alreadyFetchedCardPhysicalDetails;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketPhysicalCardType;
		private bool	_alwaysFetchTicketCollectionViaTicketPhysicalCardType, _alreadyFetchedTicketCollectionViaTicketPhysicalCardType;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TicketCardChipTypes</summary>
			public static readonly string TicketCardChipTypes = "TicketCardChipTypes";
			/// <summary>Member name CardPhysicalDetails</summary>
			public static readonly string CardPhysicalDetails = "CardPhysicalDetails";
			/// <summary>Member name TicketCollectionViaTicketPhysicalCardType</summary>
			public static readonly string TicketCollectionViaTicketPhysicalCardType = "TicketCollectionViaTicketPhysicalCardType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardChipTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardChipTypeEntity() :base("CardChipTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		public CardChipTypeEntity(System.Int64 cardChipTypeID):base("CardChipTypeEntity")
		{
			InitClassFetch(cardChipTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardChipTypeEntity(System.Int64 cardChipTypeID, IPrefetchPath prefetchPathToUse):base("CardChipTypeEntity")
		{
			InitClassFetch(cardChipTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="validator">The custom validator object for this CardChipTypeEntity</param>
		public CardChipTypeEntity(System.Int64 cardChipTypeID, IValidator validator):base("CardChipTypeEntity")
		{
			InitClassFetch(cardChipTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardChipTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_ticketCardChipTypes = (VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection)info.GetValue("_ticketCardChipTypes", typeof(VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection));
			_alwaysFetchTicketCardChipTypes = info.GetBoolean("_alwaysFetchTicketCardChipTypes");
			_alreadyFetchedTicketCardChipTypes = info.GetBoolean("_alreadyFetchedTicketCardChipTypes");

			_cardPhysicalDetails = (VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection)info.GetValue("_cardPhysicalDetails", typeof(VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection));
			_alwaysFetchCardPhysicalDetails = info.GetBoolean("_alwaysFetchCardPhysicalDetails");
			_alreadyFetchedCardPhysicalDetails = info.GetBoolean("_alreadyFetchedCardPhysicalDetails");
			_ticketCollectionViaTicketPhysicalCardType = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketPhysicalCardType", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketPhysicalCardType = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketPhysicalCardType");
			_alreadyFetchedTicketCollectionViaTicketPhysicalCardType = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketPhysicalCardType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTicketCardChipTypes = (_ticketCardChipTypes.Count > 0);
			_alreadyFetchedCardPhysicalDetails = (_cardPhysicalDetails.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketPhysicalCardType = (_ticketCollectionViaTicketPhysicalCardType.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TicketCardChipTypes":
					toReturn.Add(Relations.TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID);
					break;
				case "CardPhysicalDetails":
					toReturn.Add(Relations.CardPhysicalDetailEntityUsingChipTypeID);
					break;
				case "TicketCollectionViaTicketPhysicalCardType":
					toReturn.Add(Relations.TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID, "CardChipTypeEntity__", "TicketPhysicalCardType_", JoinHint.None);
					toReturn.Add(TicketPhysicalCardTypeEntity.Relations.TicketEntityUsingTicketID, "TicketPhysicalCardType_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ticketCardChipTypes", (!this.MarkedForDeletion?_ticketCardChipTypes:null));
			info.AddValue("_alwaysFetchTicketCardChipTypes", _alwaysFetchTicketCardChipTypes);
			info.AddValue("_alreadyFetchedTicketCardChipTypes", _alreadyFetchedTicketCardChipTypes);
			info.AddValue("_cardPhysicalDetails", (!this.MarkedForDeletion?_cardPhysicalDetails:null));
			info.AddValue("_alwaysFetchCardPhysicalDetails", _alwaysFetchCardPhysicalDetails);
			info.AddValue("_alreadyFetchedCardPhysicalDetails", _alreadyFetchedCardPhysicalDetails);
			info.AddValue("_ticketCollectionViaTicketPhysicalCardType", (!this.MarkedForDeletion?_ticketCollectionViaTicketPhysicalCardType:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketPhysicalCardType", _alwaysFetchTicketCollectionViaTicketPhysicalCardType);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketPhysicalCardType", _alreadyFetchedTicketCollectionViaTicketPhysicalCardType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TicketCardChipTypes":
					_alreadyFetchedTicketCardChipTypes = true;
					if(entity!=null)
					{
						this.TicketCardChipTypes.Add((TicketPhysicalCardTypeEntity)entity);
					}
					break;
				case "CardPhysicalDetails":
					_alreadyFetchedCardPhysicalDetails = true;
					if(entity!=null)
					{
						this.CardPhysicalDetails.Add((CardPhysicalDetailEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketPhysicalCardType":
					_alreadyFetchedTicketCollectionViaTicketPhysicalCardType = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketPhysicalCardType.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TicketCardChipTypes":
					_ticketCardChipTypes.Add((TicketPhysicalCardTypeEntity)relatedEntity);
					break;
				case "CardPhysicalDetails":
					_cardPhysicalDetails.Add((CardPhysicalDetailEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TicketCardChipTypes":
					this.PerformRelatedEntityRemoval(_ticketCardChipTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardPhysicalDetails":
					this.PerformRelatedEntityRemoval(_cardPhysicalDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_ticketCardChipTypes);
			toReturn.Add(_cardPhysicalDetails);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardChipTypeID)
		{
			return FetchUsingPK(cardChipTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardChipTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardChipTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardChipTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardChipTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardChipTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardChipTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardChipTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardChipTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketPhysicalCardTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketCardChipTypes(bool forceFetch)
		{
			return GetMultiTicketCardChipTypes(forceFetch, _ticketCardChipTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketPhysicalCardTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketCardChipTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketCardChipTypes(forceFetch, _ticketCardChipTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketCardChipTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketCardChipTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection GetMultiTicketCardChipTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketCardChipTypes || forceFetch || _alwaysFetchTicketCardChipTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCardChipTypes);
				_ticketCardChipTypes.SuppressClearInGetMulti=!forceFetch;
				_ticketCardChipTypes.EntityFactoryToUse = entityFactoryToUse;
				_ticketCardChipTypes.GetMultiManyToOne(this, null, filter);
				_ticketCardChipTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCardChipTypes = true;
			}
			return _ticketCardChipTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCardChipTypes'. These settings will be taken into account
		/// when the property TicketCardChipTypes is requested or GetMultiTicketCardChipTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCardChipTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCardChipTypes.SortClauses=sortClauses;
			_ticketCardChipTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardPhysicalDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardPhysicalDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection GetMultiCardPhysicalDetails(bool forceFetch)
		{
			return GetMultiCardPhysicalDetails(forceFetch, _cardPhysicalDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardPhysicalDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardPhysicalDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection GetMultiCardPhysicalDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardPhysicalDetails(forceFetch, _cardPhysicalDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardPhysicalDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection GetMultiCardPhysicalDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardPhysicalDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardPhysicalDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection GetMultiCardPhysicalDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardPhysicalDetails || forceFetch || _alwaysFetchCardPhysicalDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardPhysicalDetails);
				_cardPhysicalDetails.SuppressClearInGetMulti=!forceFetch;
				_cardPhysicalDetails.EntityFactoryToUse = entityFactoryToUse;
				_cardPhysicalDetails.GetMultiManyToOne(this, filter);
				_cardPhysicalDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedCardPhysicalDetails = true;
			}
			return _cardPhysicalDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardPhysicalDetails'. These settings will be taken into account
		/// when the property CardPhysicalDetails is requested or GetMultiCardPhysicalDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardPhysicalDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardPhysicalDetails.SortClauses=sortClauses;
			_cardPhysicalDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketPhysicalCardType(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketPhysicalCardType(forceFetch, _ticketCollectionViaTicketPhysicalCardType.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketPhysicalCardType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketPhysicalCardType || forceFetch || _alwaysFetchTicketCollectionViaTicketPhysicalCardType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketPhysicalCardType);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CardChipTypeFields.CardChipTypeID, ComparisonOperator.Equal, this.CardChipTypeID, "CardChipTypeEntity__"));
				_ticketCollectionViaTicketPhysicalCardType.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketPhysicalCardType.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketPhysicalCardType.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketPhysicalCardType"));
				_ticketCollectionViaTicketPhysicalCardType.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketPhysicalCardType = true;
			}
			return _ticketCollectionViaTicketPhysicalCardType;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketPhysicalCardType'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketPhysicalCardType is requested or GetMultiTicketCollectionViaTicketPhysicalCardType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketPhysicalCardType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketPhysicalCardType.SortClauses=sortClauses;
			_ticketCollectionViaTicketPhysicalCardType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TicketCardChipTypes", _ticketCardChipTypes);
			toReturn.Add("CardPhysicalDetails", _cardPhysicalDetails);
			toReturn.Add("TicketCollectionViaTicketPhysicalCardType", _ticketCollectionViaTicketPhysicalCardType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="validator">The validator object for this CardChipTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardChipTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardChipTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_ticketCardChipTypes = new VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection();
			_ticketCardChipTypes.SetContainingEntityInfo(this, "CardChipType");

			_cardPhysicalDetails = new VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection();
			_cardPhysicalDetails.SetContainingEntityInfo(this, "CardChipType");
			_ticketCollectionViaTicketPhysicalCardType = new VarioSL.Entities.CollectionClasses.TicketCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Abbreviation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardChipTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Category", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceMappingNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaximumCappingPotCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardChipTypeID">PK value for CardChipType which data should be fetched into this CardChipType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardChipTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardChipTypeFieldIndex.CardChipTypeID].ForcedCurrentValueWrite(cardChipTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardChipTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardChipTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardChipTypeRelations Relations
		{
			get	{ return new CardChipTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketPhysicalCardType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCardChipTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection(), (IEntityRelation)GetRelationsForField("TicketCardChipTypes")[0], (int)VarioSL.Entities.EntityType.CardChipTypeEntity, (int)VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity, 0, null, null, null, "TicketCardChipTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardPhysicalDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardPhysicalDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection(), (IEntityRelation)GetRelationsForField("CardPhysicalDetails")[0], (int)VarioSL.Entities.EntityType.CardChipTypeEntity, (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, 0, null, null, null, "CardPhysicalDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketPhysicalCardType
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID;
				intermediateRelation.SetAliases(string.Empty, "TicketPhysicalCardType_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.CardChipTypeEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketPhysicalCardType"), "TicketCollectionViaTicketPhysicalCardType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Abbreviation property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."ABBREVIATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Abbreviation
		{
			get { return (System.String)GetValue((int)CardChipTypeFieldIndex.Abbreviation, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.Abbreviation, value, true); }
		}

		/// <summary> The CardChipTypeID property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."PHYSICALCARDTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardChipTypeID
		{
			get { return (System.Int64)GetValue((int)CardChipTypeFieldIndex.CardChipTypeID, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.CardChipTypeID, value, true); }
		}

		/// <summary> The Category property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."CATEGORY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Category
		{
			get { return (System.Int32)GetValue((int)CardChipTypeFieldIndex.Category, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.Category, value, true); }
		}

		/// <summary> The Description property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CardChipTypeFieldIndex.Description, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceMappingNumber property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."DEVICEMAPPINGNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceMappingNumber
		{
			get { return (System.Int32)GetValue((int)CardChipTypeFieldIndex.DeviceMappingNumber, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.DeviceMappingNumber, value, true); }
		}

		/// <summary> The MaximumCappingPotCount property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."MAXIMUMCAPPINGPOTCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaximumCappingPotCount
		{
			get { return (System.Int32)GetValue((int)CardChipTypeFieldIndex.MaximumCappingPotCount, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.MaximumCappingPotCount, value, true); }
		}

		/// <summary> The Name property of the Entity CardChipType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_PHYSICALCARDTYPE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CardChipTypeFieldIndex.Name, true); }
			set	{ SetValue((int)CardChipTypeFieldIndex.Name, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'TicketPhysicalCardTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCardChipTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketPhysicalCardTypeCollection TicketCardChipTypes
		{
			get	{ return GetMultiTicketCardChipTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCardChipTypes. When set to true, TicketCardChipTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCardChipTypes is accessed. You can always execute/ a forced fetch by calling GetMultiTicketCardChipTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCardChipTypes
		{
			get	{ return _alwaysFetchTicketCardChipTypes; }
			set	{ _alwaysFetchTicketCardChipTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCardChipTypes already has been fetched. Setting this property to false when TicketCardChipTypes has been fetched
		/// will clear the TicketCardChipTypes collection well. Setting this property to true while TicketCardChipTypes hasn't been fetched disables lazy loading for TicketCardChipTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCardChipTypes
		{
			get { return _alreadyFetchedTicketCardChipTypes;}
			set 
			{
				if(_alreadyFetchedTicketCardChipTypes && !value && (_ticketCardChipTypes != null))
				{
					_ticketCardChipTypes.Clear();
				}
				_alreadyFetchedTicketCardChipTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardPhysicalDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardPhysicalDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection CardPhysicalDetails
		{
			get	{ return GetMultiCardPhysicalDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardPhysicalDetails. When set to true, CardPhysicalDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardPhysicalDetails is accessed. You can always execute/ a forced fetch by calling GetMultiCardPhysicalDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardPhysicalDetails
		{
			get	{ return _alwaysFetchCardPhysicalDetails; }
			set	{ _alwaysFetchCardPhysicalDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardPhysicalDetails already has been fetched. Setting this property to false when CardPhysicalDetails has been fetched
		/// will clear the CardPhysicalDetails collection well. Setting this property to true while CardPhysicalDetails hasn't been fetched disables lazy loading for CardPhysicalDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardPhysicalDetails
		{
			get { return _alreadyFetchedCardPhysicalDetails;}
			set 
			{
				if(_alreadyFetchedCardPhysicalDetails && !value && (_cardPhysicalDetails != null))
				{
					_cardPhysicalDetails.Clear();
				}
				_alreadyFetchedCardPhysicalDetails = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketPhysicalCardType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketPhysicalCardType
		{
			get { return GetMultiTicketCollectionViaTicketPhysicalCardType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketPhysicalCardType. When set to true, TicketCollectionViaTicketPhysicalCardType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketPhysicalCardType is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketPhysicalCardType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketPhysicalCardType
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketPhysicalCardType; }
			set	{ _alwaysFetchTicketCollectionViaTicketPhysicalCardType = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketPhysicalCardType already has been fetched. Setting this property to false when TicketCollectionViaTicketPhysicalCardType has been fetched
		/// will clear the TicketCollectionViaTicketPhysicalCardType collection well. Setting this property to true while TicketCollectionViaTicketPhysicalCardType hasn't been fetched disables lazy loading for TicketCollectionViaTicketPhysicalCardType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketPhysicalCardType
		{
			get { return _alreadyFetchedTicketCollectionViaTicketPhysicalCardType;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketPhysicalCardType && !value && (_ticketCollectionViaTicketPhysicalCardType != null))
				{
					_ticketCollectionViaTicketPhysicalCardType.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketPhysicalCardType = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardChipTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
