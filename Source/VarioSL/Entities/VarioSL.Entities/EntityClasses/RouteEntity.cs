﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Route'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RouteEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AttributeValueEntity _tariffAttributeValue;
		private bool	_alwaysFetchTariffAttributeValue, _alreadyFetchedTariffAttributeValue, _tariffAttributeValueReturnsNewIfNotFound;
		private AttributeValueEntity _ticketGroupAttributeValue;
		private bool	_alwaysFetchTicketGroupAttributeValue, _alreadyFetchedTicketGroupAttributeValue, _ticketGroupAttributeValueReturnsNewIfNotFound;
		private PrintTextEntity _printText1;
		private bool	_alwaysFetchPrintText1, _alreadyFetchedPrintText1, _printText1ReturnsNewIfNotFound;
		private PrintTextEntity _printText2;
		private bool	_alwaysFetchPrintText2, _alreadyFetchedPrintText2, _printText2ReturnsNewIfNotFound;
		private PrintTextEntity _printText3;
		private bool	_alwaysFetchPrintText3, _alreadyFetchedPrintText3, _printText3ReturnsNewIfNotFound;
		private RouteNameEntity _routeName;
		private bool	_alwaysFetchRouteName, _alreadyFetchedRouteName, _routeNameReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name TariffAttributeValue</summary>
			public static readonly string TariffAttributeValue = "TariffAttributeValue";
			/// <summary>Member name TicketGroupAttributeValue</summary>
			public static readonly string TicketGroupAttributeValue = "TicketGroupAttributeValue";
			/// <summary>Member name PrintText1</summary>
			public static readonly string PrintText1 = "PrintText1";
			/// <summary>Member name PrintText2</summary>
			public static readonly string PrintText2 = "PrintText2";
			/// <summary>Member name PrintText3</summary>
			public static readonly string PrintText3 = "PrintText3";
			/// <summary>Member name RouteName</summary>
			public static readonly string RouteName = "RouteName";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RouteEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RouteEntity() :base("RouteEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		public RouteEntity(System.Int64 routeID):base("RouteEntity")
		{
			InitClassFetch(routeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RouteEntity(System.Int64 routeID, IPrefetchPath prefetchPathToUse):base("RouteEntity")
		{
			InitClassFetch(routeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="validator">The custom validator object for this RouteEntity</param>
		public RouteEntity(System.Int64 routeID, IValidator validator):base("RouteEntity")
		{
			InitClassFetch(routeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RouteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_tariffAttributeValue = (AttributeValueEntity)info.GetValue("_tariffAttributeValue", typeof(AttributeValueEntity));
			if(_tariffAttributeValue!=null)
			{
				_tariffAttributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffAttributeValueReturnsNewIfNotFound = info.GetBoolean("_tariffAttributeValueReturnsNewIfNotFound");
			_alwaysFetchTariffAttributeValue = info.GetBoolean("_alwaysFetchTariffAttributeValue");
			_alreadyFetchedTariffAttributeValue = info.GetBoolean("_alreadyFetchedTariffAttributeValue");

			_ticketGroupAttributeValue = (AttributeValueEntity)info.GetValue("_ticketGroupAttributeValue", typeof(AttributeValueEntity));
			if(_ticketGroupAttributeValue!=null)
			{
				_ticketGroupAttributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketGroupAttributeValueReturnsNewIfNotFound = info.GetBoolean("_ticketGroupAttributeValueReturnsNewIfNotFound");
			_alwaysFetchTicketGroupAttributeValue = info.GetBoolean("_alwaysFetchTicketGroupAttributeValue");
			_alreadyFetchedTicketGroupAttributeValue = info.GetBoolean("_alreadyFetchedTicketGroupAttributeValue");

			_printText1 = (PrintTextEntity)info.GetValue("_printText1", typeof(PrintTextEntity));
			if(_printText1!=null)
			{
				_printText1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printText1ReturnsNewIfNotFound = info.GetBoolean("_printText1ReturnsNewIfNotFound");
			_alwaysFetchPrintText1 = info.GetBoolean("_alwaysFetchPrintText1");
			_alreadyFetchedPrintText1 = info.GetBoolean("_alreadyFetchedPrintText1");

			_printText2 = (PrintTextEntity)info.GetValue("_printText2", typeof(PrintTextEntity));
			if(_printText2!=null)
			{
				_printText2.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printText2ReturnsNewIfNotFound = info.GetBoolean("_printText2ReturnsNewIfNotFound");
			_alwaysFetchPrintText2 = info.GetBoolean("_alwaysFetchPrintText2");
			_alreadyFetchedPrintText2 = info.GetBoolean("_alreadyFetchedPrintText2");

			_printText3 = (PrintTextEntity)info.GetValue("_printText3", typeof(PrintTextEntity));
			if(_printText3!=null)
			{
				_printText3.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_printText3ReturnsNewIfNotFound = info.GetBoolean("_printText3ReturnsNewIfNotFound");
			_alwaysFetchPrintText3 = info.GetBoolean("_alwaysFetchPrintText3");
			_alreadyFetchedPrintText3 = info.GetBoolean("_alreadyFetchedPrintText3");

			_routeName = (RouteNameEntity)info.GetValue("_routeName", typeof(RouteNameEntity));
			if(_routeName!=null)
			{
				_routeName.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_routeNameReturnsNewIfNotFound = info.GetBoolean("_routeNameReturnsNewIfNotFound");
			_alwaysFetchRouteName = info.GetBoolean("_alwaysFetchRouteName");
			_alreadyFetchedRouteName = info.GetBoolean("_alreadyFetchedRouteName");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RouteFieldIndex)fieldIndex)
			{
				case RouteFieldIndex.PrintText1ID:
					DesetupSyncPrintText1(true, false);
					_alreadyFetchedPrintText1 = false;
					break;
				case RouteFieldIndex.PrintText2ID:
					DesetupSyncPrintText2(true, false);
					_alreadyFetchedPrintText2 = false;
					break;
				case RouteFieldIndex.PrintText3ID:
					DesetupSyncPrintText3(true, false);
					_alreadyFetchedPrintText3 = false;
					break;
				case RouteFieldIndex.RouteNameID:
					DesetupSyncRouteName(true, false);
					_alreadyFetchedRouteName = false;
					break;
				case RouteFieldIndex.TariffAttributeID:
					DesetupSyncTariffAttributeValue(true, false);
					_alreadyFetchedTariffAttributeValue = false;
					break;
				case RouteFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case RouteFieldIndex.TicketGroupAttributeID:
					DesetupSyncTicketGroupAttributeValue(true, false);
					_alreadyFetchedTicketGroupAttributeValue = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedTariffAttributeValue = (_tariffAttributeValue != null);
			_alreadyFetchedTicketGroupAttributeValue = (_ticketGroupAttributeValue != null);
			_alreadyFetchedPrintText1 = (_printText1 != null);
			_alreadyFetchedPrintText2 = (_printText2 != null);
			_alreadyFetchedPrintText3 = (_printText3 != null);
			_alreadyFetchedRouteName = (_routeName != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "TariffAttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingTariffAttributeID);
					break;
				case "TicketGroupAttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingTicketGroupAttributeID);
					break;
				case "PrintText1":
					toReturn.Add(Relations.PrintTextEntityUsingPrintText1ID);
					break;
				case "PrintText2":
					toReturn.Add(Relations.PrintTextEntityUsingPrintText2ID);
					break;
				case "PrintText3":
					toReturn.Add(Relations.PrintTextEntityUsingPrintText3ID);
					break;
				case "RouteName":
					toReturn.Add(Relations.RouteNameEntityUsingRouteNameID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_tariffAttributeValue", (!this.MarkedForDeletion?_tariffAttributeValue:null));
			info.AddValue("_tariffAttributeValueReturnsNewIfNotFound", _tariffAttributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariffAttributeValue", _alwaysFetchTariffAttributeValue);
			info.AddValue("_alreadyFetchedTariffAttributeValue", _alreadyFetchedTariffAttributeValue);
			info.AddValue("_ticketGroupAttributeValue", (!this.MarkedForDeletion?_ticketGroupAttributeValue:null));
			info.AddValue("_ticketGroupAttributeValueReturnsNewIfNotFound", _ticketGroupAttributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicketGroupAttributeValue", _alwaysFetchTicketGroupAttributeValue);
			info.AddValue("_alreadyFetchedTicketGroupAttributeValue", _alreadyFetchedTicketGroupAttributeValue);
			info.AddValue("_printText1", (!this.MarkedForDeletion?_printText1:null));
			info.AddValue("_printText1ReturnsNewIfNotFound", _printText1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrintText1", _alwaysFetchPrintText1);
			info.AddValue("_alreadyFetchedPrintText1", _alreadyFetchedPrintText1);
			info.AddValue("_printText2", (!this.MarkedForDeletion?_printText2:null));
			info.AddValue("_printText2ReturnsNewIfNotFound", _printText2ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrintText2", _alwaysFetchPrintText2);
			info.AddValue("_alreadyFetchedPrintText2", _alreadyFetchedPrintText2);
			info.AddValue("_printText3", (!this.MarkedForDeletion?_printText3:null));
			info.AddValue("_printText3ReturnsNewIfNotFound", _printText3ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPrintText3", _alwaysFetchPrintText3);
			info.AddValue("_alreadyFetchedPrintText3", _alreadyFetchedPrintText3);
			info.AddValue("_routeName", (!this.MarkedForDeletion?_routeName:null));
			info.AddValue("_routeNameReturnsNewIfNotFound", _routeNameReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRouteName", _alwaysFetchRouteName);
			info.AddValue("_alreadyFetchedRouteName", _alreadyFetchedRouteName);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "TariffAttributeValue":
					_alreadyFetchedTariffAttributeValue = true;
					this.TariffAttributeValue = (AttributeValueEntity)entity;
					break;
				case "TicketGroupAttributeValue":
					_alreadyFetchedTicketGroupAttributeValue = true;
					this.TicketGroupAttributeValue = (AttributeValueEntity)entity;
					break;
				case "PrintText1":
					_alreadyFetchedPrintText1 = true;
					this.PrintText1 = (PrintTextEntity)entity;
					break;
				case "PrintText2":
					_alreadyFetchedPrintText2 = true;
					this.PrintText2 = (PrintTextEntity)entity;
					break;
				case "PrintText3":
					_alreadyFetchedPrintText3 = true;
					this.PrintText3 = (PrintTextEntity)entity;
					break;
				case "RouteName":
					_alreadyFetchedRouteName = true;
					this.RouteName = (RouteNameEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "TariffAttributeValue":
					SetupSyncTariffAttributeValue(relatedEntity);
					break;
				case "TicketGroupAttributeValue":
					SetupSyncTicketGroupAttributeValue(relatedEntity);
					break;
				case "PrintText1":
					SetupSyncPrintText1(relatedEntity);
					break;
				case "PrintText2":
					SetupSyncPrintText2(relatedEntity);
					break;
				case "PrintText3":
					SetupSyncPrintText3(relatedEntity);
					break;
				case "RouteName":
					SetupSyncRouteName(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "TariffAttributeValue":
					DesetupSyncTariffAttributeValue(false, true);
					break;
				case "TicketGroupAttributeValue":
					DesetupSyncTicketGroupAttributeValue(false, true);
					break;
				case "PrintText1":
					DesetupSyncPrintText1(false, true);
					break;
				case "PrintText2":
					DesetupSyncPrintText2(false, true);
					break;
				case "PrintText3":
					DesetupSyncPrintText3(false, true);
					break;
				case "RouteName":
					DesetupSyncRouteName(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariffAttributeValue!=null)
			{
				toReturn.Add(_tariffAttributeValue);
			}
			if(_ticketGroupAttributeValue!=null)
			{
				toReturn.Add(_ticketGroupAttributeValue);
			}
			if(_printText1!=null)
			{
				toReturn.Add(_printText1);
			}
			if(_printText2!=null)
			{
				toReturn.Add(_printText2);
			}
			if(_printText3!=null)
			{
				toReturn.Add(_printText3);
			}
			if(_routeName!=null)
			{
				toReturn.Add(_routeName);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 routeID)
		{
			return FetchUsingPK(routeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 routeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(routeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 routeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(routeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 routeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(routeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RouteID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RouteRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleTariffAttributeValue()
		{
			return GetSingleTariffAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleTariffAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariffAttributeValue || forceFetch || _alwaysFetchTariffAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingTariffAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffAttributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TariffAttributeValue = newEntity;
				_alreadyFetchedTariffAttributeValue = fetchResult;
			}
			return _tariffAttributeValue;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleTicketGroupAttributeValue()
		{
			return GetSingleTicketGroupAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleTicketGroupAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicketGroupAttributeValue || forceFetch || _alwaysFetchTicketGroupAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingTicketGroupAttributeID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketGroupAttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketGroupAttributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TicketGroupAttributeValue = newEntity;
				_alreadyFetchedTicketGroupAttributeValue = fetchResult;
			}
			return _ticketGroupAttributeValue;
		}


		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public PrintTextEntity GetSinglePrintText1()
		{
			return GetSinglePrintText1(false);
		}

		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public virtual PrintTextEntity GetSinglePrintText1(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrintText1 || forceFetch || _alwaysFetchPrintText1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PrintTextEntityUsingPrintText1ID);
				PrintTextEntity newEntity = new PrintTextEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrintText1ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PrintTextEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printText1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PrintText1 = newEntity;
				_alreadyFetchedPrintText1 = fetchResult;
			}
			return _printText1;
		}


		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public PrintTextEntity GetSinglePrintText2()
		{
			return GetSinglePrintText2(false);
		}

		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public virtual PrintTextEntity GetSinglePrintText2(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrintText2 || forceFetch || _alwaysFetchPrintText2) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PrintTextEntityUsingPrintText2ID);
				PrintTextEntity newEntity = new PrintTextEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrintText2ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PrintTextEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printText2ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PrintText2 = newEntity;
				_alreadyFetchedPrintText2 = fetchResult;
			}
			return _printText2;
		}


		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public PrintTextEntity GetSinglePrintText3()
		{
			return GetSinglePrintText3(false);
		}

		/// <summary> Retrieves the related entity of type 'PrintTextEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PrintTextEntity' which is related to this entity.</returns>
		public virtual PrintTextEntity GetSinglePrintText3(bool forceFetch)
		{
			if( ( !_alreadyFetchedPrintText3 || forceFetch || _alwaysFetchPrintText3) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PrintTextEntityUsingPrintText3ID);
				PrintTextEntity newEntity = new PrintTextEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PrintText3ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PrintTextEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_printText3ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PrintText3 = newEntity;
				_alreadyFetchedPrintText3 = fetchResult;
			}
			return _printText3;
		}


		/// <summary> Retrieves the related entity of type 'RouteNameEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RouteNameEntity' which is related to this entity.</returns>
		public RouteNameEntity GetSingleRouteName()
		{
			return GetSingleRouteName(false);
		}

		/// <summary> Retrieves the related entity of type 'RouteNameEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RouteNameEntity' which is related to this entity.</returns>
		public virtual RouteNameEntity GetSingleRouteName(bool forceFetch)
		{
			if( ( !_alreadyFetchedRouteName || forceFetch || _alwaysFetchRouteName) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RouteNameEntityUsingRouteNameID);
				RouteNameEntity newEntity = new RouteNameEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RouteNameID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RouteNameEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_routeNameReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RouteName = newEntity;
				_alreadyFetchedRouteName = fetchResult;
			}
			return _routeName;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("TariffAttributeValue", _tariffAttributeValue);
			toReturn.Add("TicketGroupAttributeValue", _ticketGroupAttributeValue);
			toReturn.Add("PrintText1", _printText1);
			toReturn.Add("PrintText2", _printText2);
			toReturn.Add("PrintText3", _printText3);
			toReturn.Add("RouteName", _routeName);
			toReturn.Add("Tariff", _tariff);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="validator">The validator object for this RouteEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 routeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(routeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_tariffAttributeValueReturnsNewIfNotFound = false;
			_ticketGroupAttributeValueReturnsNewIfNotFound = false;
			_printText1ReturnsNewIfNotFound = false;
			_printText2ReturnsNewIfNotFound = false;
			_printText3ReturnsNewIfNotFound = false;
			_routeNameReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationInfo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Exchangeable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintText1ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintText2ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintText3ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNameID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNameObsolete", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextFixed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketGroupAttributeID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariffAttributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariffAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariffAttributeValue, new PropertyChangedEventHandler( OnTariffAttributeValuePropertyChanged ), "TariffAttributeValue", VarioSL.Entities.RelationClasses.StaticRouteRelations.AttributeValueEntityUsingTariffAttributeIDStatic, true, signalRelatedEntity, "TariffAttributeRoutes", resetFKFields, new int[] { (int)RouteFieldIndex.TariffAttributeID } );		
			_tariffAttributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _tariffAttributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariffAttributeValue(IEntityCore relatedEntity)
		{
			if(_tariffAttributeValue!=relatedEntity)
			{		
				DesetupSyncTariffAttributeValue(true, true);
				_tariffAttributeValue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariffAttributeValue, new PropertyChangedEventHandler( OnTariffAttributeValuePropertyChanged ), "TariffAttributeValue", VarioSL.Entities.RelationClasses.StaticRouteRelations.AttributeValueEntityUsingTariffAttributeIDStatic, true, ref _alreadyFetchedTariffAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticketGroupAttributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicketGroupAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticketGroupAttributeValue, new PropertyChangedEventHandler( OnTicketGroupAttributeValuePropertyChanged ), "TicketGroupAttributeValue", VarioSL.Entities.RelationClasses.StaticRouteRelations.AttributeValueEntityUsingTicketGroupAttributeIDStatic, true, signalRelatedEntity, "TicketGroupAttributeRoutes", resetFKFields, new int[] { (int)RouteFieldIndex.TicketGroupAttributeID } );		
			_ticketGroupAttributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _ticketGroupAttributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicketGroupAttributeValue(IEntityCore relatedEntity)
		{
			if(_ticketGroupAttributeValue!=relatedEntity)
			{		
				DesetupSyncTicketGroupAttributeValue(true, true);
				_ticketGroupAttributeValue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticketGroupAttributeValue, new PropertyChangedEventHandler( OnTicketGroupAttributeValuePropertyChanged ), "TicketGroupAttributeValue", VarioSL.Entities.RelationClasses.StaticRouteRelations.AttributeValueEntityUsingTicketGroupAttributeIDStatic, true, ref _alreadyFetchedTicketGroupAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketGroupAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printText1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrintText1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printText1, new PropertyChangedEventHandler( OnPrintText1PropertyChanged ), "PrintText1", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText1IDStatic, true, signalRelatedEntity, "RoutesPrintText1", resetFKFields, new int[] { (int)RouteFieldIndex.PrintText1ID } );		
			_printText1 = null;
		}
		
		/// <summary> setups the sync logic for member _printText1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrintText1(IEntityCore relatedEntity)
		{
			if(_printText1!=relatedEntity)
			{		
				DesetupSyncPrintText1(true, true);
				_printText1 = (PrintTextEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printText1, new PropertyChangedEventHandler( OnPrintText1PropertyChanged ), "PrintText1", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText1IDStatic, true, ref _alreadyFetchedPrintText1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrintText1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printText2</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrintText2(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printText2, new PropertyChangedEventHandler( OnPrintText2PropertyChanged ), "PrintText2", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText2IDStatic, true, signalRelatedEntity, "RoutesPrintText2", resetFKFields, new int[] { (int)RouteFieldIndex.PrintText2ID } );		
			_printText2 = null;
		}
		
		/// <summary> setups the sync logic for member _printText2</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrintText2(IEntityCore relatedEntity)
		{
			if(_printText2!=relatedEntity)
			{		
				DesetupSyncPrintText2(true, true);
				_printText2 = (PrintTextEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printText2, new PropertyChangedEventHandler( OnPrintText2PropertyChanged ), "PrintText2", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText2IDStatic, true, ref _alreadyFetchedPrintText2, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrintText2PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _printText3</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPrintText3(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _printText3, new PropertyChangedEventHandler( OnPrintText3PropertyChanged ), "PrintText3", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText3IDStatic, true, signalRelatedEntity, "RoutesPrintText3", resetFKFields, new int[] { (int)RouteFieldIndex.PrintText3ID } );		
			_printText3 = null;
		}
		
		/// <summary> setups the sync logic for member _printText3</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPrintText3(IEntityCore relatedEntity)
		{
			if(_printText3!=relatedEntity)
			{		
				DesetupSyncPrintText3(true, true);
				_printText3 = (PrintTextEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _printText3, new PropertyChangedEventHandler( OnPrintText3PropertyChanged ), "PrintText3", VarioSL.Entities.RelationClasses.StaticRouteRelations.PrintTextEntityUsingPrintText3IDStatic, true, ref _alreadyFetchedPrintText3, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPrintText3PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _routeName</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRouteName(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _routeName, new PropertyChangedEventHandler( OnRouteNamePropertyChanged ), "RouteName", VarioSL.Entities.RelationClasses.StaticRouteRelations.RouteNameEntityUsingRouteNameIDStatic, true, signalRelatedEntity, "Routes", resetFKFields, new int[] { (int)RouteFieldIndex.RouteNameID } );		
			_routeName = null;
		}
		
		/// <summary> setups the sync logic for member _routeName</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRouteName(IEntityCore relatedEntity)
		{
			if(_routeName!=relatedEntity)
			{		
				DesetupSyncRouteName(true, true);
				_routeName = (RouteNameEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _routeName, new PropertyChangedEventHandler( OnRouteNamePropertyChanged ), "RouteName", VarioSL.Entities.RelationClasses.StaticRouteRelations.RouteNameEntityUsingRouteNameIDStatic, true, ref _alreadyFetchedRouteName, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRouteNamePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticRouteRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "Routes", resetFKFields, new int[] { (int)RouteFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticRouteRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="routeID">PK value for Route which data should be fetched into this Route object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 routeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RouteFieldIndex.RouteID].ForcedCurrentValueWrite(routeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRouteDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RouteEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RouteRelations Relations
		{
			get	{ return new RouteRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("TariffAttributeValue")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "TariffAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketGroupAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("TicketGroupAttributeValue")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "TicketGroupAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrintText'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintText1
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrintTextCollection(), (IEntityRelation)GetRelationsForField("PrintText1")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.PrintTextEntity, 0, null, null, null, "PrintText1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrintText'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintText2
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrintTextCollection(), (IEntityRelation)GetRelationsForField("PrintText2")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.PrintTextEntity, 0, null, null, null, "PrintText2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrintText'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintText3
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrintTextCollection(), (IEntityRelation)GetRelationsForField("PrintText3")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.PrintTextEntity, 0, null, null, null, "PrintText3", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RouteName'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteName
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteNameCollection(), (IEntityRelation)GetRelationsForField("RouteName")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.RouteNameEntity, 0, null, null, null, "RouteName", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.RouteEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BoardingInfo property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."BOARDINGINFO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BoardingInfo
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.BoardingInfo, true); }
			set	{ SetValue((int)RouteFieldIndex.BoardingInfo, value, true); }
		}

		/// <summary> The DestinationInfo property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."DESTINATIONINFO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DestinationInfo
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.DestinationInfo, true); }
			set	{ SetValue((int)RouteFieldIndex.DestinationInfo, value, true); }
		}

		/// <summary> The Exchangeable property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."EXCHANGEABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Exchangeable
		{
			get { return (Nullable<System.Int16>)GetValue((int)RouteFieldIndex.Exchangeable, false); }
			set	{ SetValue((int)RouteFieldIndex.Exchangeable, value, true); }
		}

		/// <summary> The PrintText1ID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."PRINTTEXT1ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintText1ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.PrintText1ID, false); }
			set	{ SetValue((int)RouteFieldIndex.PrintText1ID, value, true); }
		}

		/// <summary> The PrintText2ID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."PRINTTEXT2ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintText2ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.PrintText2ID, false); }
			set	{ SetValue((int)RouteFieldIndex.PrintText2ID, value, true); }
		}

		/// <summary> The PrintText3ID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."PRINTTEXT3ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PrintText3ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.PrintText3ID, false); }
			set	{ SetValue((int)RouteFieldIndex.PrintText3ID, value, true); }
		}

		/// <summary> The RouteID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."ROUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RouteID
		{
			get { return (System.Int64)GetValue((int)RouteFieldIndex.RouteID, true); }
			set	{ SetValue((int)RouteFieldIndex.RouteID, value, true); }
		}

		/// <summary> The RouteNameID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."ROUTENAMEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RouteNameID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.RouteNameID, false); }
			set	{ SetValue((int)RouteFieldIndex.RouteNameID, value, true); }
		}

		/// <summary> The RouteNameObsolete property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."ROUTENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RouteNameObsolete
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.RouteNameObsolete, true); }
			set	{ SetValue((int)RouteFieldIndex.RouteNameObsolete, value, true); }
		}

		/// <summary> The RouteText property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."ROUTETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RouteText
		{
			get { return (System.String)GetValue((int)RouteFieldIndex.RouteText, true); }
			set	{ SetValue((int)RouteFieldIndex.RouteText, value, true); }
		}

		/// <summary> The TariffAttributeID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."TARIFFATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.TariffAttributeID, false); }
			set	{ SetValue((int)RouteFieldIndex.TariffAttributeID, value, true); }
		}

		/// <summary> The TariffID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.TariffID, false); }
			set	{ SetValue((int)RouteFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TextFixed property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."TEXTFIXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TextFixed
		{
			get { return (Nullable<System.Int16>)GetValue((int)RouteFieldIndex.TextFixed, false); }
			set	{ SetValue((int)RouteFieldIndex.TextFixed, value, true); }
		}

		/// <summary> The TicketCategoryID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."TICKETCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketCategoryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.TicketCategoryID, false); }
			set	{ SetValue((int)RouteFieldIndex.TicketCategoryID, value, true); }
		}

		/// <summary> The TicketGroupAttributeID property of the Entity Route<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ROUTE"."TICKETGROUPATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketGroupAttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RouteFieldIndex.TicketGroupAttributeID, false); }
			set	{ SetValue((int)RouteFieldIndex.TicketGroupAttributeID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariffAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity TariffAttributeValue
		{
			get	{ return GetSingleTariffAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariffAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TariffAttributeRoutes", "TariffAttributeValue", _tariffAttributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TariffAttributeValue. When set to true, TariffAttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TariffAttributeValue is accessed. You can always execute a forced fetch by calling GetSingleTariffAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffAttributeValue
		{
			get	{ return _alwaysFetchTariffAttributeValue; }
			set	{ _alwaysFetchTariffAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TariffAttributeValue already has been fetched. Setting this property to false when TariffAttributeValue has been fetched
		/// will set TariffAttributeValue to null as well. Setting this property to true while TariffAttributeValue hasn't been fetched disables lazy loading for TariffAttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffAttributeValue
		{
			get { return _alreadyFetchedTariffAttributeValue;}
			set 
			{
				if(_alreadyFetchedTariffAttributeValue && !value)
				{
					this.TariffAttributeValue = null;
				}
				_alreadyFetchedTariffAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TariffAttributeValue is not found
		/// in the database. When set to true, TariffAttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffAttributeValueReturnsNewIfNotFound
		{
			get	{ return _tariffAttributeValueReturnsNewIfNotFound; }
			set { _tariffAttributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicketGroupAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity TicketGroupAttributeValue
		{
			get	{ return GetSingleTicketGroupAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicketGroupAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketGroupAttributeRoutes", "TicketGroupAttributeValue", _ticketGroupAttributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TicketGroupAttributeValue. When set to true, TicketGroupAttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketGroupAttributeValue is accessed. You can always execute a forced fetch by calling GetSingleTicketGroupAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketGroupAttributeValue
		{
			get	{ return _alwaysFetchTicketGroupAttributeValue; }
			set	{ _alwaysFetchTicketGroupAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketGroupAttributeValue already has been fetched. Setting this property to false when TicketGroupAttributeValue has been fetched
		/// will set TicketGroupAttributeValue to null as well. Setting this property to true while TicketGroupAttributeValue hasn't been fetched disables lazy loading for TicketGroupAttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketGroupAttributeValue
		{
			get { return _alreadyFetchedTicketGroupAttributeValue;}
			set 
			{
				if(_alreadyFetchedTicketGroupAttributeValue && !value)
				{
					this.TicketGroupAttributeValue = null;
				}
				_alreadyFetchedTicketGroupAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TicketGroupAttributeValue is not found
		/// in the database. When set to true, TicketGroupAttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketGroupAttributeValueReturnsNewIfNotFound
		{
			get	{ return _ticketGroupAttributeValueReturnsNewIfNotFound; }
			set { _ticketGroupAttributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PrintTextEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrintText1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PrintTextEntity PrintText1
		{
			get	{ return GetSinglePrintText1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrintText1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoutesPrintText1", "PrintText1", _printText1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PrintText1. When set to true, PrintText1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintText1 is accessed. You can always execute a forced fetch by calling GetSinglePrintText1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintText1
		{
			get	{ return _alwaysFetchPrintText1; }
			set	{ _alwaysFetchPrintText1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintText1 already has been fetched. Setting this property to false when PrintText1 has been fetched
		/// will set PrintText1 to null as well. Setting this property to true while PrintText1 hasn't been fetched disables lazy loading for PrintText1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintText1
		{
			get { return _alreadyFetchedPrintText1;}
			set 
			{
				if(_alreadyFetchedPrintText1 && !value)
				{
					this.PrintText1 = null;
				}
				_alreadyFetchedPrintText1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PrintText1 is not found
		/// in the database. When set to true, PrintText1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrintText1ReturnsNewIfNotFound
		{
			get	{ return _printText1ReturnsNewIfNotFound; }
			set { _printText1ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PrintTextEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrintText2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PrintTextEntity PrintText2
		{
			get	{ return GetSinglePrintText2(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrintText2(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoutesPrintText2", "PrintText2", _printText2, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PrintText2. When set to true, PrintText2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintText2 is accessed. You can always execute a forced fetch by calling GetSinglePrintText2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintText2
		{
			get	{ return _alwaysFetchPrintText2; }
			set	{ _alwaysFetchPrintText2 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintText2 already has been fetched. Setting this property to false when PrintText2 has been fetched
		/// will set PrintText2 to null as well. Setting this property to true while PrintText2 hasn't been fetched disables lazy loading for PrintText2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintText2
		{
			get { return _alreadyFetchedPrintText2;}
			set 
			{
				if(_alreadyFetchedPrintText2 && !value)
				{
					this.PrintText2 = null;
				}
				_alreadyFetchedPrintText2 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PrintText2 is not found
		/// in the database. When set to true, PrintText2 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrintText2ReturnsNewIfNotFound
		{
			get	{ return _printText2ReturnsNewIfNotFound; }
			set { _printText2ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PrintTextEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePrintText3()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PrintTextEntity PrintText3
		{
			get	{ return GetSinglePrintText3(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPrintText3(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RoutesPrintText3", "PrintText3", _printText3, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PrintText3. When set to true, PrintText3 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintText3 is accessed. You can always execute a forced fetch by calling GetSinglePrintText3(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintText3
		{
			get	{ return _alwaysFetchPrintText3; }
			set	{ _alwaysFetchPrintText3 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintText3 already has been fetched. Setting this property to false when PrintText3 has been fetched
		/// will set PrintText3 to null as well. Setting this property to true while PrintText3 hasn't been fetched disables lazy loading for PrintText3</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintText3
		{
			get { return _alreadyFetchedPrintText3;}
			set 
			{
				if(_alreadyFetchedPrintText3 && !value)
				{
					this.PrintText3 = null;
				}
				_alreadyFetchedPrintText3 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PrintText3 is not found
		/// in the database. When set to true, PrintText3 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PrintText3ReturnsNewIfNotFound
		{
			get	{ return _printText3ReturnsNewIfNotFound; }
			set { _printText3ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RouteNameEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRouteName()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RouteNameEntity RouteName
		{
			get	{ return GetSingleRouteName(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRouteName(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Routes", "RouteName", _routeName, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RouteName. When set to true, RouteName is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteName is accessed. You can always execute a forced fetch by calling GetSingleRouteName(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteName
		{
			get	{ return _alwaysFetchRouteName; }
			set	{ _alwaysFetchRouteName = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteName already has been fetched. Setting this property to false when RouteName has been fetched
		/// will set RouteName to null as well. Setting this property to true while RouteName hasn't been fetched disables lazy loading for RouteName</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteName
		{
			get { return _alreadyFetchedRouteName;}
			set 
			{
				if(_alreadyFetchedRouteName && !value)
				{
					this.RouteName = null;
				}
				_alreadyFetchedRouteName = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RouteName is not found
		/// in the database. When set to true, RouteName will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RouteNameReturnsNewIfNotFound
		{
			get	{ return _routeNameReturnsNewIfNotFound; }
			set { _routeNameReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Routes", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RouteEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
