﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareEvasionCategory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareEvasionCategoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection	_fareEvasionReasonToCategories;
		private bool	_alwaysFetchFareEvasionReasonToCategories, _alreadyFetchedFareEvasionReasonToCategories;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidents;
		private bool	_alwaysFetchFareEvasionIncidents, _alreadyFetchedFareEvasionIncidents;
		private VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection _assignedReasons;
		private bool	_alwaysFetchAssignedReasons, _alreadyFetchedAssignedReasons;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name FareEvasionReasonToCategories</summary>
			public static readonly string FareEvasionReasonToCategories = "FareEvasionReasonToCategories";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name FareEvasionIncidents</summary>
			public static readonly string FareEvasionIncidents = "FareEvasionIncidents";
			/// <summary>Member name AssignedReasons</summary>
			public static readonly string AssignedReasons = "AssignedReasons";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareEvasionCategoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareEvasionCategoryEntity() :base("FareEvasionCategoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		public FareEvasionCategoryEntity(System.Int64 fareEvasionCategoryID):base("FareEvasionCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareEvasionCategoryEntity(System.Int64 fareEvasionCategoryID, IPrefetchPath prefetchPathToUse):base("FareEvasionCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="validator">The custom validator object for this FareEvasionCategoryEntity</param>
		public FareEvasionCategoryEntity(System.Int64 fareEvasionCategoryID, IValidator validator):base("FareEvasionCategoryEntity")
		{
			InitClassFetch(fareEvasionCategoryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareEvasionCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareEvasionReasonToCategories = (VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection)info.GetValue("_fareEvasionReasonToCategories", typeof(VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection));
			_alwaysFetchFareEvasionReasonToCategories = info.GetBoolean("_alwaysFetchFareEvasionReasonToCategories");
			_alreadyFetchedFareEvasionReasonToCategories = info.GetBoolean("_alreadyFetchedFareEvasionReasonToCategories");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_fareEvasionIncidents = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidents", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidents = info.GetBoolean("_alwaysFetchFareEvasionIncidents");
			_alreadyFetchedFareEvasionIncidents = info.GetBoolean("_alreadyFetchedFareEvasionIncidents");
			_assignedReasons = (VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection)info.GetValue("_assignedReasons", typeof(VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection));
			_alwaysFetchAssignedReasons = info.GetBoolean("_alwaysFetchAssignedReasons");
			_alreadyFetchedAssignedReasons = info.GetBoolean("_alreadyFetchedAssignedReasons");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareEvasionCategoryFieldIndex)fieldIndex)
			{
				case FareEvasionCategoryFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareEvasionReasonToCategories = (_fareEvasionReasonToCategories.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedFareEvasionIncidents = (_fareEvasionIncidents.Count > 0);
			_alreadyFetchedAssignedReasons = (_assignedReasons.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "FareEvasionReasonToCategories":
					toReturn.Add(Relations.FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingFareEvasionCategoryID);
					break;
				case "FareEvasionIncidents":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingFareEvasionCategoryID);
					break;
				case "AssignedReasons":
					toReturn.Add(Relations.FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID, "FareEvasionCategoryEntity__", "FareEvasionReasonToCategory_", JoinHint.None);
					toReturn.Add(FareEvasionReasonToCategoryEntity.Relations.FareEvasionReasonEntityUsingFareEvasionReasonID, "FareEvasionReasonToCategory_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareEvasionReasonToCategories", (!this.MarkedForDeletion?_fareEvasionReasonToCategories:null));
			info.AddValue("_alwaysFetchFareEvasionReasonToCategories", _alwaysFetchFareEvasionReasonToCategories);
			info.AddValue("_alreadyFetchedFareEvasionReasonToCategories", _alreadyFetchedFareEvasionReasonToCategories);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_fareEvasionIncidents", (!this.MarkedForDeletion?_fareEvasionIncidents:null));
			info.AddValue("_alwaysFetchFareEvasionIncidents", _alwaysFetchFareEvasionIncidents);
			info.AddValue("_alreadyFetchedFareEvasionIncidents", _alreadyFetchedFareEvasionIncidents);
			info.AddValue("_assignedReasons", (!this.MarkedForDeletion?_assignedReasons:null));
			info.AddValue("_alwaysFetchAssignedReasons", _alwaysFetchAssignedReasons);
			info.AddValue("_alreadyFetchedAssignedReasons", _alreadyFetchedAssignedReasons);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "FareEvasionReasonToCategories":
					_alreadyFetchedFareEvasionReasonToCategories = true;
					if(entity!=null)
					{
						this.FareEvasionReasonToCategories.Add((FareEvasionReasonToCategoryEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "FareEvasionIncidents":
					_alreadyFetchedFareEvasionIncidents = true;
					if(entity!=null)
					{
						this.FareEvasionIncidents.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "AssignedReasons":
					_alreadyFetchedAssignedReasons = true;
					if(entity!=null)
					{
						this.AssignedReasons.Add((FareEvasionReasonEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "FareEvasionReasonToCategories":
					_fareEvasionReasonToCategories.Add((FareEvasionReasonToCategoryEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "FareEvasionIncidents":
					_fareEvasionIncidents.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "FareEvasionReasonToCategories":
					this.PerformRelatedEntityRemoval(_fareEvasionReasonToCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidents":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareEvasionReasonToCategories);
			toReturn.Add(_tickets);
			toReturn.Add(_fareEvasionIncidents);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="tariffID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCNumberTariffID(System.Int64 number, System.Int64 tariffID)
		{
			return FetchUsingUCNumberTariffID( number,  tariffID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="tariffID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCNumberTariffID(System.Int64 number, System.Int64 tariffID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCNumberTariffID( number,  tariffID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="tariffID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCNumberTariffID(System.Int64 number, System.Int64 tariffID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCNumberTariffID( number,  tariffID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="tariffID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCNumberTariffID(System.Int64 number, System.Int64 tariffID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((FareEvasionCategoryDAO)CreateDAOInstance()).FetchFareEvasionCategoryUsingUCNumberTariffID(this, this.Transaction, number, tariffID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID)
		{
			return FetchUsingPK(fareEvasionCategoryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareEvasionCategoryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareEvasionCategoryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareEvasionCategoryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareEvasionCategoryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareEvasionCategoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonToCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionReasonToCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection GetMultiFareEvasionReasonToCategories(bool forceFetch)
		{
			return GetMultiFareEvasionReasonToCategories(forceFetch, _fareEvasionReasonToCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonToCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionReasonToCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection GetMultiFareEvasionReasonToCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionReasonToCategories(forceFetch, _fareEvasionReasonToCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonToCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection GetMultiFareEvasionReasonToCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionReasonToCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonToCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection GetMultiFareEvasionReasonToCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionReasonToCategories || forceFetch || _alwaysFetchFareEvasionReasonToCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionReasonToCategories);
				_fareEvasionReasonToCategories.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionReasonToCategories.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionReasonToCategories.GetMultiManyToOne(this, null, filter);
				_fareEvasionReasonToCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionReasonToCategories = true;
			}
			return _fareEvasionReasonToCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionReasonToCategories'. These settings will be taken into account
		/// when the property FareEvasionReasonToCategories is requested or GetMultiFareEvasionReasonToCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionReasonToCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionReasonToCategories.SortClauses=sortClauses;
			_fareEvasionReasonToCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidents || forceFetch || _alwaysFetchFareEvasionIncidents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidents);
				_fareEvasionIncidents.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidents.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidents.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_fareEvasionIncidents.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidents = true;
			}
			return _fareEvasionIncidents;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidents'. These settings will be taken into account
		/// when the property FareEvasionIncidents is requested or GetMultiFareEvasionIncidents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidents.SortClauses=sortClauses;
			_fareEvasionIncidents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionReasonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiAssignedReasons(bool forceFetch)
		{
			return GetMultiAssignedReasons(forceFetch, _assignedReasons.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiAssignedReasons(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAssignedReasons || forceFetch || _alwaysFetchAssignedReasons) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_assignedReasons);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(FareEvasionCategoryFields.FareEvasionCategoryID, ComparisonOperator.Equal, this.FareEvasionCategoryID, "FareEvasionCategoryEntity__"));
				_assignedReasons.SuppressClearInGetMulti=!forceFetch;
				_assignedReasons.EntityFactoryToUse = entityFactoryToUse;
				_assignedReasons.GetMulti(filter, GetRelationsForField("AssignedReasons"));
				_assignedReasons.SuppressClearInGetMulti=false;
				_alreadyFetchedAssignedReasons = true;
			}
			return _assignedReasons;
		}

		/// <summary> Sets the collection parameters for the collection for 'AssignedReasons'. These settings will be taken into account
		/// when the property AssignedReasons is requested or GetMultiAssignedReasons is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAssignedReasons(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_assignedReasons.SortClauses=sortClauses;
			_assignedReasons.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("FareEvasionReasonToCategories", _fareEvasionReasonToCategories);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("FareEvasionIncidents", _fareEvasionIncidents);
			toReturn.Add("AssignedReasons", _assignedReasons);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="validator">The validator object for this FareEvasionCategoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareEvasionCategoryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareEvasionCategoryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareEvasionReasonToCategories = new VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection();
			_fareEvasionReasonToCategories.SetContainingEntityInfo(this, "FareEvasionCategory");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "FareEvasionCategory");

			_fareEvasionIncidents = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidents.SetContainingEntityInfo(this, "FareEvasionCategory");
			_assignedReasons = new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection();
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareEvasionCategoryRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "FareEvasionCategories", resetFKFields, new int[] { (int)FareEvasionCategoryFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareEvasionCategoryRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareEvasionCategoryID">PK value for FareEvasionCategory which data should be fetched into this FareEvasionCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareEvasionCategoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareEvasionCategoryFieldIndex.FareEvasionCategoryID].ForcedCurrentValueWrite(fareEvasionCategoryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareEvasionCategoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareEvasionCategoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareEvasionCategoryRelations Relations
		{
			get	{ return new FareEvasionCategoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionReasonToCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionReasonToCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection(), (IEntityRelation)GetRelationsForField("FareEvasionReasonToCategories")[0], (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, (int)VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity, 0, null, null, null, "FareEvasionReasonToCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidents")[0], (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionReason'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAssignedReasons
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID;
				intermediateRelation.SetAliases(string.Empty, "FareEvasionReasonToCategory_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, (int)VarioSL.Entities.EntityType.FareEvasionReasonEntity, 0, null, null, GetRelationsForField("AssignedReasons"), "AssignedReasons", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity FareEvasionCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONCATEGORY"."CATEGORYDESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FareEvasionCategoryFieldIndex.Description, true); }
			set	{ SetValue((int)FareEvasionCategoryFieldIndex.Description, value, true); }
		}

		/// <summary> The FareEvasionCategoryID property of the Entity FareEvasionCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONCATEGORY"."FAREEVASIONCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareEvasionCategoryID
		{
			get { return (System.Int64)GetValue((int)FareEvasionCategoryFieldIndex.FareEvasionCategoryID, true); }
			set	{ SetValue((int)FareEvasionCategoryFieldIndex.FareEvasionCategoryID, value, true); }
		}

		/// <summary> The Name property of the Entity FareEvasionCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONCATEGORY"."CATEGORYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareEvasionCategoryFieldIndex.Name, true); }
			set	{ SetValue((int)FareEvasionCategoryFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity FareEvasionCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONCATEGORY"."CATEGORYNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Number
		{
			get { return (System.Int64)GetValue((int)FareEvasionCategoryFieldIndex.Number, true); }
			set	{ SetValue((int)FareEvasionCategoryFieldIndex.Number, value, true); }
		}

		/// <summary> The TariffID property of the Entity FareEvasionCategory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FAREEVASIONCATEGORY"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)FareEvasionCategoryFieldIndex.TariffID, true); }
			set	{ SetValue((int)FareEvasionCategoryFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonToCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionReasonToCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionReasonToCategoryCollection FareEvasionReasonToCategories
		{
			get	{ return GetMultiFareEvasionReasonToCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionReasonToCategories. When set to true, FareEvasionReasonToCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionReasonToCategories is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionReasonToCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionReasonToCategories
		{
			get	{ return _alwaysFetchFareEvasionReasonToCategories; }
			set	{ _alwaysFetchFareEvasionReasonToCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionReasonToCategories already has been fetched. Setting this property to false when FareEvasionReasonToCategories has been fetched
		/// will clear the FareEvasionReasonToCategories collection well. Setting this property to true while FareEvasionReasonToCategories hasn't been fetched disables lazy loading for FareEvasionReasonToCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionReasonToCategories
		{
			get { return _alreadyFetchedFareEvasionReasonToCategories;}
			set 
			{
				if(_alreadyFetchedFareEvasionReasonToCategories && !value && (_fareEvasionReasonToCategories != null))
				{
					_fareEvasionReasonToCategories.Clear();
				}
				_alreadyFetchedFareEvasionReasonToCategories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidents
		{
			get	{ return GetMultiFareEvasionIncidents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidents. When set to true, FareEvasionIncidents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidents is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidents
		{
			get	{ return _alwaysFetchFareEvasionIncidents; }
			set	{ _alwaysFetchFareEvasionIncidents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidents already has been fetched. Setting this property to false when FareEvasionIncidents has been fetched
		/// will clear the FareEvasionIncidents collection well. Setting this property to true while FareEvasionIncidents hasn't been fetched disables lazy loading for FareEvasionIncidents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidents
		{
			get { return _alreadyFetchedFareEvasionIncidents;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidents && !value && (_fareEvasionIncidents != null))
				{
					_fareEvasionIncidents.Clear();
				}
				_alreadyFetchedFareEvasionIncidents = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAssignedReasons()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection AssignedReasons
		{
			get { return GetMultiAssignedReasons(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AssignedReasons. When set to true, AssignedReasons is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AssignedReasons is accessed. You can always execute a forced fetch by calling GetMultiAssignedReasons(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAssignedReasons
		{
			get	{ return _alwaysFetchAssignedReasons; }
			set	{ _alwaysFetchAssignedReasons = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AssignedReasons already has been fetched. Setting this property to false when AssignedReasons has been fetched
		/// will clear the AssignedReasons collection well. Setting this property to true while AssignedReasons hasn't been fetched disables lazy loading for AssignedReasons</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAssignedReasons
		{
			get { return _alreadyFetchedAssignedReasons;}
			set 
			{
				if(_alreadyFetchedAssignedReasons && !value && (_assignedReasons != null))
				{
					_assignedReasons.Clear();
				}
				_alreadyFetchedAssignedReasons = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionCategories", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
