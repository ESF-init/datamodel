﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ComponentType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ComponentTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection	_cashServiceBalance;
		private bool	_alwaysFetchCashServiceBalance, _alreadyFetchedCashServiceBalance;
		private VarioSL.Entities.CollectionClasses.ComponentCollection	_components;
		private bool	_alwaysFetchComponents, _alreadyFetchedComponents;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CashServiceBalance</summary>
			public static readonly string CashServiceBalance = "CashServiceBalance";
			/// <summary>Member name Components</summary>
			public static readonly string Components = "Components";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ComponentTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ComponentTypeEntity() :base("ComponentTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		public ComponentTypeEntity(System.Int64 componentTypeID):base("ComponentTypeEntity")
		{
			InitClassFetch(componentTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ComponentTypeEntity(System.Int64 componentTypeID, IPrefetchPath prefetchPathToUse):base("ComponentTypeEntity")
		{
			InitClassFetch(componentTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="validator">The custom validator object for this ComponentTypeEntity</param>
		public ComponentTypeEntity(System.Int64 componentTypeID, IValidator validator):base("ComponentTypeEntity")
		{
			InitClassFetch(componentTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ComponentTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cashServiceBalance = (VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection)info.GetValue("_cashServiceBalance", typeof(VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection));
			_alwaysFetchCashServiceBalance = info.GetBoolean("_alwaysFetchCashServiceBalance");
			_alreadyFetchedCashServiceBalance = info.GetBoolean("_alreadyFetchedCashServiceBalance");

			_components = (VarioSL.Entities.CollectionClasses.ComponentCollection)info.GetValue("_components", typeof(VarioSL.Entities.CollectionClasses.ComponentCollection));
			_alwaysFetchComponents = info.GetBoolean("_alwaysFetchComponents");
			_alreadyFetchedComponents = info.GetBoolean("_alreadyFetchedComponents");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCashServiceBalance = (_cashServiceBalance.Count > 0);
			_alreadyFetchedComponents = (_components.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CashServiceBalance":
					toReturn.Add(Relations.CashServiceBalanceEntityUsingComponentTypeID);
					break;
				case "Components":
					toReturn.Add(Relations.ComponentEntityUsingComponentTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cashServiceBalance", (!this.MarkedForDeletion?_cashServiceBalance:null));
			info.AddValue("_alwaysFetchCashServiceBalance", _alwaysFetchCashServiceBalance);
			info.AddValue("_alreadyFetchedCashServiceBalance", _alreadyFetchedCashServiceBalance);
			info.AddValue("_components", (!this.MarkedForDeletion?_components:null));
			info.AddValue("_alwaysFetchComponents", _alwaysFetchComponents);
			info.AddValue("_alreadyFetchedComponents", _alreadyFetchedComponents);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CashServiceBalance":
					_alreadyFetchedCashServiceBalance = true;
					if(entity!=null)
					{
						this.CashServiceBalance.Add((CashServiceBalanceEntity)entity);
					}
					break;
				case "Components":
					_alreadyFetchedComponents = true;
					if(entity!=null)
					{
						this.Components.Add((ComponentEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CashServiceBalance":
					_cashServiceBalance.Add((CashServiceBalanceEntity)relatedEntity);
					break;
				case "Components":
					_components.Add((ComponentEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CashServiceBalance":
					this.PerformRelatedEntityRemoval(_cashServiceBalance, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Components":
					this.PerformRelatedEntityRemoval(_components, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cashServiceBalance);
			toReturn.Add(_components);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentTypeID)
		{
			return FetchUsingPK(componentTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(componentTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(componentTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(componentTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ComponentTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ComponentTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceBalanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch)
		{
			return GetMultiCashServiceBalance(forceFetch, _cashServiceBalance.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceBalanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCashServiceBalance(forceFetch, _cashServiceBalance.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCashServiceBalance(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCashServiceBalance || forceFetch || _alwaysFetchCashServiceBalance) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cashServiceBalance);
				_cashServiceBalance.SuppressClearInGetMulti=!forceFetch;
				_cashServiceBalance.EntityFactoryToUse = entityFactoryToUse;
				_cashServiceBalance.GetMultiManyToOne(null, this, filter);
				_cashServiceBalance.SuppressClearInGetMulti=false;
				_alreadyFetchedCashServiceBalance = true;
			}
			return _cashServiceBalance;
		}

		/// <summary> Sets the collection parameters for the collection for 'CashServiceBalance'. These settings will be taken into account
		/// when the property CashServiceBalance is requested or GetMultiCashServiceBalance is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCashServiceBalance(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cashServiceBalance.SortClauses=sortClauses;
			_cashServiceBalance.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch)
		{
			return GetMultiComponents(forceFetch, _components.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponents(forceFetch, _components.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentCollection GetMultiComponents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponents || forceFetch || _alwaysFetchComponents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_components);
				_components.SuppressClearInGetMulti=!forceFetch;
				_components.EntityFactoryToUse = entityFactoryToUse;
				_components.GetMultiManyToOne(null, null, this, filter);
				_components.SuppressClearInGetMulti=false;
				_alreadyFetchedComponents = true;
			}
			return _components;
		}

		/// <summary> Sets the collection parameters for the collection for 'Components'. These settings will be taken into account
		/// when the property Components is requested or GetMultiComponents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_components.SortClauses=sortClauses;
			_components.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CashServiceBalance", _cashServiceBalance);
			toReturn.Add("Components", _components);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="validator">The validator object for this ComponentTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 componentTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(componentTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cashServiceBalance = new VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection();
			_cashServiceBalance.SetContainingEntityInfo(this, "ComponentType");

			_components = new VarioSL.Entities.CollectionClasses.ComponentCollection();
			_components.SetContainingEntityInfo(this, "ComponentType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxWarnLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinWarnLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Unit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="componentTypeID">PK value for ComponentType which data should be fetched into this ComponentType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 componentTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ComponentTypeFieldIndex.ComponentTypeID].ForcedCurrentValueWrite(componentTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateComponentTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ComponentTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ComponentTypeRelations Relations
		{
			get	{ return new ComponentTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashServiceBalance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCashServiceBalance
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection(), (IEntityRelation)GetRelationsForField("CashServiceBalance")[0], (int)VarioSL.Entities.EntityType.ComponentTypeEntity, (int)VarioSL.Entities.EntityType.CashServiceBalanceEntity, 0, null, null, null, "CashServiceBalance", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Component' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentCollection(), (IEntityRelation)GetRelationsForField("Components")[0], (int)VarioSL.Entities.EntityType.ComponentTypeEntity, (int)VarioSL.Entities.EntityType.ComponentEntity, 0, null, null, null, "Components", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ComponentName property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."COMPONENTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ComponentName
		{
			get { return (System.String)GetValue((int)ComponentTypeFieldIndex.ComponentName, true); }
			set	{ SetValue((int)ComponentTypeFieldIndex.ComponentName, value, true); }
		}

		/// <summary> The ComponentTypeID property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."COMPONENTTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ComponentTypeID
		{
			get { return (System.Int64)GetValue((int)ComponentTypeFieldIndex.ComponentTypeID, true); }
			set	{ SetValue((int)ComponentTypeFieldIndex.ComponentTypeID, value, true); }
		}

		/// <summary> The MaxWarnLevel property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."MAXWARNLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxWarnLevel
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentTypeFieldIndex.MaxWarnLevel, false); }
			set	{ SetValue((int)ComponentTypeFieldIndex.MaxWarnLevel, value, true); }
		}

		/// <summary> The MinWarnLevel property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."MINWARNLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MinWarnLevel
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentTypeFieldIndex.MinWarnLevel, false); }
			set	{ SetValue((int)ComponentTypeFieldIndex.MinWarnLevel, value, true); }
		}

		/// <summary> The Unit property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."UNIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Unit
		{
			get { return (System.String)GetValue((int)ComponentTypeFieldIndex.Unit, true); }
			set	{ SetValue((int)ComponentTypeFieldIndex.Unit, value, true); }
		}

		/// <summary> The Visible property of the Entity ComponentType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTTYPE"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Visible
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ComponentTypeFieldIndex.Visible, false); }
			set	{ SetValue((int)ComponentTypeFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCashServiceBalance()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection CashServiceBalance
		{
			get	{ return GetMultiCashServiceBalance(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CashServiceBalance. When set to true, CashServiceBalance is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CashServiceBalance is accessed. You can always execute/ a forced fetch by calling GetMultiCashServiceBalance(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCashServiceBalance
		{
			get	{ return _alwaysFetchCashServiceBalance; }
			set	{ _alwaysFetchCashServiceBalance = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CashServiceBalance already has been fetched. Setting this property to false when CashServiceBalance has been fetched
		/// will clear the CashServiceBalance collection well. Setting this property to true while CashServiceBalance hasn't been fetched disables lazy loading for CashServiceBalance</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCashServiceBalance
		{
			get { return _alreadyFetchedCashServiceBalance;}
			set 
			{
				if(_alreadyFetchedCashServiceBalance && !value && (_cashServiceBalance != null))
				{
					_cashServiceBalance.Clear();
				}
				_alreadyFetchedCashServiceBalance = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentCollection Components
		{
			get	{ return GetMultiComponents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Components. When set to true, Components is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Components is accessed. You can always execute/ a forced fetch by calling GetMultiComponents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponents
		{
			get	{ return _alwaysFetchComponents; }
			set	{ _alwaysFetchComponents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Components already has been fetched. Setting this property to false when Components has been fetched
		/// will clear the Components collection well. Setting this property to true while Components hasn't been fetched disables lazy loading for Components</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponents
		{
			get { return _alreadyFetchedComponents;}
			set 
			{
				if(_alreadyFetchedComponents && !value && (_components != null))
				{
					_components.Clear();
				}
				_alreadyFetchedComponents = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ComponentTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
