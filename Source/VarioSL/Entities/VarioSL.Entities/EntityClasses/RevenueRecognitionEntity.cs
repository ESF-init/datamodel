﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RevenueRecognition'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RevenueRecognitionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ApportionCollection	_apportions;
		private bool	_alwaysFetchApportions, _alreadyFetchedApportions;
		private VarioSL.Entities.CollectionClasses.SettledRevenueCollection	_settledRevenues;
		private bool	_alwaysFetchSettledRevenues, _alreadyFetchedSettledRevenues;
		private CloseoutPeriodEntity _closeoutPeriod;
		private bool	_alwaysFetchCloseoutPeriod, _alreadyFetchedCloseoutPeriod, _closeoutPeriodReturnsNewIfNotFound;
		private RevenueSettlementEntity _revenueSettlement;
		private bool	_alwaysFetchRevenueSettlement, _alreadyFetchedRevenueSettlement, _revenueSettlementReturnsNewIfNotFound;
		private TransactionJournalEntity _transactionJournal;
		private bool	_alwaysFetchTransactionJournal, _alreadyFetchedTransactionJournal, _transactionJournalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CloseoutPeriod</summary>
			public static readonly string CloseoutPeriod = "CloseoutPeriod";
			/// <summary>Member name RevenueSettlement</summary>
			public static readonly string RevenueSettlement = "RevenueSettlement";
			/// <summary>Member name TransactionJournal</summary>
			public static readonly string TransactionJournal = "TransactionJournal";
			/// <summary>Member name Apportions</summary>
			public static readonly string Apportions = "Apportions";
			/// <summary>Member name SettledRevenues</summary>
			public static readonly string SettledRevenues = "SettledRevenues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RevenueRecognitionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RevenueRecognitionEntity() :base("RevenueRecognitionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		public RevenueRecognitionEntity(System.Int64 revenueRecognitionID):base("RevenueRecognitionEntity")
		{
			InitClassFetch(revenueRecognitionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RevenueRecognitionEntity(System.Int64 revenueRecognitionID, IPrefetchPath prefetchPathToUse):base("RevenueRecognitionEntity")
		{
			InitClassFetch(revenueRecognitionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="validator">The custom validator object for this RevenueRecognitionEntity</param>
		public RevenueRecognitionEntity(System.Int64 revenueRecognitionID, IValidator validator):base("RevenueRecognitionEntity")
		{
			InitClassFetch(revenueRecognitionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RevenueRecognitionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportions = (VarioSL.Entities.CollectionClasses.ApportionCollection)info.GetValue("_apportions", typeof(VarioSL.Entities.CollectionClasses.ApportionCollection));
			_alwaysFetchApportions = info.GetBoolean("_alwaysFetchApportions");
			_alreadyFetchedApportions = info.GetBoolean("_alreadyFetchedApportions");

			_settledRevenues = (VarioSL.Entities.CollectionClasses.SettledRevenueCollection)info.GetValue("_settledRevenues", typeof(VarioSL.Entities.CollectionClasses.SettledRevenueCollection));
			_alwaysFetchSettledRevenues = info.GetBoolean("_alwaysFetchSettledRevenues");
			_alreadyFetchedSettledRevenues = info.GetBoolean("_alreadyFetchedSettledRevenues");
			_closeoutPeriod = (CloseoutPeriodEntity)info.GetValue("_closeoutPeriod", typeof(CloseoutPeriodEntity));
			if(_closeoutPeriod!=null)
			{
				_closeoutPeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_closeoutPeriodReturnsNewIfNotFound = info.GetBoolean("_closeoutPeriodReturnsNewIfNotFound");
			_alwaysFetchCloseoutPeriod = info.GetBoolean("_alwaysFetchCloseoutPeriod");
			_alreadyFetchedCloseoutPeriod = info.GetBoolean("_alreadyFetchedCloseoutPeriod");

			_revenueSettlement = (RevenueSettlementEntity)info.GetValue("_revenueSettlement", typeof(RevenueSettlementEntity));
			if(_revenueSettlement!=null)
			{
				_revenueSettlement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueSettlementReturnsNewIfNotFound = info.GetBoolean("_revenueSettlementReturnsNewIfNotFound");
			_alwaysFetchRevenueSettlement = info.GetBoolean("_alwaysFetchRevenueSettlement");
			_alreadyFetchedRevenueSettlement = info.GetBoolean("_alreadyFetchedRevenueSettlement");

			_transactionJournal = (TransactionJournalEntity)info.GetValue("_transactionJournal", typeof(TransactionJournalEntity));
			if(_transactionJournal!=null)
			{
				_transactionJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionJournalReturnsNewIfNotFound = info.GetBoolean("_transactionJournalReturnsNewIfNotFound");
			_alwaysFetchTransactionJournal = info.GetBoolean("_alwaysFetchTransactionJournal");
			_alreadyFetchedTransactionJournal = info.GetBoolean("_alreadyFetchedTransactionJournal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RevenueRecognitionFieldIndex)fieldIndex)
			{
				case RevenueRecognitionFieldIndex.CloseoutPeriodID:
					DesetupSyncCloseoutPeriod(true, false);
					_alreadyFetchedCloseoutPeriod = false;
					break;
				case RevenueRecognitionFieldIndex.RevenueSettlementID:
					DesetupSyncRevenueSettlement(true, false);
					_alreadyFetchedRevenueSettlement = false;
					break;
				case RevenueRecognitionFieldIndex.TransactionJournalID:
					DesetupSyncTransactionJournal(true, false);
					_alreadyFetchedTransactionJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportions = (_apportions.Count > 0);
			_alreadyFetchedSettledRevenues = (_settledRevenues.Count > 0);
			_alreadyFetchedCloseoutPeriod = (_closeoutPeriod != null);
			_alreadyFetchedRevenueSettlement = (_revenueSettlement != null);
			_alreadyFetchedTransactionJournal = (_transactionJournal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CloseoutPeriod":
					toReturn.Add(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
					break;
				case "RevenueSettlement":
					toReturn.Add(Relations.RevenueSettlementEntityUsingRevenueSettlementID);
					break;
				case "TransactionJournal":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalID);
					break;
				case "Apportions":
					toReturn.Add(Relations.ApportionEntityUsingRevenueRecognitionID);
					break;
				case "SettledRevenues":
					toReturn.Add(Relations.SettledRevenueEntityUsingRevenueRecognitionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportions", (!this.MarkedForDeletion?_apportions:null));
			info.AddValue("_alwaysFetchApportions", _alwaysFetchApportions);
			info.AddValue("_alreadyFetchedApportions", _alreadyFetchedApportions);
			info.AddValue("_settledRevenues", (!this.MarkedForDeletion?_settledRevenues:null));
			info.AddValue("_alwaysFetchSettledRevenues", _alwaysFetchSettledRevenues);
			info.AddValue("_alreadyFetchedSettledRevenues", _alreadyFetchedSettledRevenues);
			info.AddValue("_closeoutPeriod", (!this.MarkedForDeletion?_closeoutPeriod:null));
			info.AddValue("_closeoutPeriodReturnsNewIfNotFound", _closeoutPeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCloseoutPeriod", _alwaysFetchCloseoutPeriod);
			info.AddValue("_alreadyFetchedCloseoutPeriod", _alreadyFetchedCloseoutPeriod);
			info.AddValue("_revenueSettlement", (!this.MarkedForDeletion?_revenueSettlement:null));
			info.AddValue("_revenueSettlementReturnsNewIfNotFound", _revenueSettlementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueSettlement", _alwaysFetchRevenueSettlement);
			info.AddValue("_alreadyFetchedRevenueSettlement", _alreadyFetchedRevenueSettlement);
			info.AddValue("_transactionJournal", (!this.MarkedForDeletion?_transactionJournal:null));
			info.AddValue("_transactionJournalReturnsNewIfNotFound", _transactionJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionJournal", _alwaysFetchTransactionJournal);
			info.AddValue("_alreadyFetchedTransactionJournal", _alreadyFetchedTransactionJournal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CloseoutPeriod":
					_alreadyFetchedCloseoutPeriod = true;
					this.CloseoutPeriod = (CloseoutPeriodEntity)entity;
					break;
				case "RevenueSettlement":
					_alreadyFetchedRevenueSettlement = true;
					this.RevenueSettlement = (RevenueSettlementEntity)entity;
					break;
				case "TransactionJournal":
					_alreadyFetchedTransactionJournal = true;
					this.TransactionJournal = (TransactionJournalEntity)entity;
					break;
				case "Apportions":
					_alreadyFetchedApportions = true;
					if(entity!=null)
					{
						this.Apportions.Add((ApportionEntity)entity);
					}
					break;
				case "SettledRevenues":
					_alreadyFetchedSettledRevenues = true;
					if(entity!=null)
					{
						this.SettledRevenues.Add((SettledRevenueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					SetupSyncCloseoutPeriod(relatedEntity);
					break;
				case "RevenueSettlement":
					SetupSyncRevenueSettlement(relatedEntity);
					break;
				case "TransactionJournal":
					SetupSyncTransactionJournal(relatedEntity);
					break;
				case "Apportions":
					_apportions.Add((ApportionEntity)relatedEntity);
					break;
				case "SettledRevenues":
					_settledRevenues.Add((SettledRevenueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					DesetupSyncCloseoutPeriod(false, true);
					break;
				case "RevenueSettlement":
					DesetupSyncRevenueSettlement(false, true);
					break;
				case "TransactionJournal":
					DesetupSyncTransactionJournal(false, true);
					break;
				case "Apportions":
					this.PerformRelatedEntityRemoval(_apportions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettledRevenues":
					this.PerformRelatedEntityRemoval(_settledRevenues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_closeoutPeriod!=null)
			{
				toReturn.Add(_closeoutPeriod);
			}
			if(_revenueSettlement!=null)
			{
				toReturn.Add(_revenueSettlement);
			}
			if(_transactionJournal!=null)
			{
				toReturn.Add(_transactionJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_apportions);
			toReturn.Add(_settledRevenues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID)
		{
			return FetchUsingPK(revenueRecognitionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(revenueRecognitionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(revenueRecognitionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(revenueRecognitionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RevenueRecognitionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RevenueRecognitionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportions || forceFetch || _alwaysFetchApportions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportions);
				_apportions.SuppressClearInGetMulti=!forceFetch;
				_apportions.EntityFactoryToUse = entityFactoryToUse;
				_apportions.GetMultiManyToOne(null, this, null, filter);
				_apportions.SuppressClearInGetMulti=false;
				_alreadyFetchedApportions = true;
			}
			return _apportions;
		}

		/// <summary> Sets the collection parameters for the collection for 'Apportions'. These settings will be taken into account
		/// when the property Apportions is requested or GetMultiApportions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportions.SortClauses=sortClauses;
			_apportions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettledRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch)
		{
			return GetMultiSettledRevenues(forceFetch, _settledRevenues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettledRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettledRevenues(forceFetch, _settledRevenues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettledRevenues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettledRevenues || forceFetch || _alwaysFetchSettledRevenues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settledRevenues);
				_settledRevenues.SuppressClearInGetMulti=!forceFetch;
				_settledRevenues.EntityFactoryToUse = entityFactoryToUse;
				_settledRevenues.GetMultiManyToOne(this, null, filter);
				_settledRevenues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettledRevenues = true;
			}
			return _settledRevenues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettledRevenues'. These settings will be taken into account
		/// when the property SettledRevenues is requested or GetMultiSettledRevenues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettledRevenues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settledRevenues.SortClauses=sortClauses;
			_settledRevenues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public CloseoutPeriodEntity GetSingleCloseoutPeriod()
		{
			return GetSingleCloseoutPeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public virtual CloseoutPeriodEntity GetSingleCloseoutPeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedCloseoutPeriod || forceFetch || _alwaysFetchCloseoutPeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
				CloseoutPeriodEntity newEntity = new CloseoutPeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CloseoutPeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CloseoutPeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_closeoutPeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CloseoutPeriod = newEntity;
				_alreadyFetchedCloseoutPeriod = fetchResult;
			}
			return _closeoutPeriod;
		}


		/// <summary> Retrieves the related entity of type 'RevenueSettlementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueSettlementEntity' which is related to this entity.</returns>
		public RevenueSettlementEntity GetSingleRevenueSettlement()
		{
			return GetSingleRevenueSettlement(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueSettlementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueSettlementEntity' which is related to this entity.</returns>
		public virtual RevenueSettlementEntity GetSingleRevenueSettlement(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueSettlement || forceFetch || _alwaysFetchRevenueSettlement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueSettlementEntityUsingRevenueSettlementID);
				RevenueSettlementEntity newEntity = new RevenueSettlementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RevenueSettlementID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RevenueSettlementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueSettlementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueSettlement = newEntity;
				_alreadyFetchedRevenueSettlement = fetchResult;
			}
			return _revenueSettlement;
		}


		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleTransactionJournal()
		{
			return GetSingleTransactionJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleTransactionJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionJournal || forceFetch || _alwaysFetchTransactionJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalID);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionJournalID);
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionJournal = newEntity;
				_alreadyFetchedTransactionJournal = fetchResult;
			}
			return _transactionJournal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CloseoutPeriod", _closeoutPeriod);
			toReturn.Add("RevenueSettlement", _revenueSettlement);
			toReturn.Add("TransactionJournal", _transactionJournal);
			toReturn.Add("Apportions", _apportions);
			toReturn.Add("SettledRevenues", _settledRevenues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="validator">The validator object for this RevenueRecognitionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 revenueRecognitionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(revenueRecognitionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_apportions = new VarioSL.Entities.CollectionClasses.ApportionCollection();
			_apportions.SetContainingEntityInfo(this, "RevenueRecognition");

			_settledRevenues = new VarioSL.Entities.CollectionClasses.SettledRevenueCollection();
			_settledRevenues.SetContainingEntityInfo(this, "RevenueRecognition");
			_closeoutPeriodReturnsNewIfNotFound = false;
			_revenueSettlementReturnsNewIfNotFound = false;
			_transactionJournalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AppliedPassTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncludeInSettlement", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Premium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _closeoutPeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCloseoutPeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, signalRelatedEntity, "RevenueRecognitions", resetFKFields, new int[] { (int)RevenueRecognitionFieldIndex.CloseoutPeriodID } );		
			_closeoutPeriod = null;
		}
		
		/// <summary> setups the sync logic for member _closeoutPeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCloseoutPeriod(IEntityCore relatedEntity)
		{
			if(_closeoutPeriod!=relatedEntity)
			{		
				DesetupSyncCloseoutPeriod(true, true);
				_closeoutPeriod = (CloseoutPeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, ref _alreadyFetchedCloseoutPeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCloseoutPeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _revenueSettlement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueSettlement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueSettlement, new PropertyChangedEventHandler( OnRevenueSettlementPropertyChanged ), "RevenueSettlement", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.RevenueSettlementEntityUsingRevenueSettlementIDStatic, true, signalRelatedEntity, "RevenueRecognition", resetFKFields, new int[] { (int)RevenueRecognitionFieldIndex.RevenueSettlementID } );		
			_revenueSettlement = null;
		}
		
		/// <summary> setups the sync logic for member _revenueSettlement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueSettlement(IEntityCore relatedEntity)
		{
			if(_revenueSettlement!=relatedEntity)
			{		
				DesetupSyncRevenueSettlement(true, true);
				_revenueSettlement = (RevenueSettlementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueSettlement, new PropertyChangedEventHandler( OnRevenueSettlementPropertyChanged ), "RevenueSettlement", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.RevenueSettlementEntityUsingRevenueSettlementIDStatic, true, ref _alreadyFetchedRevenueSettlement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueSettlementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, signalRelatedEntity, "RevenueRecognitions", resetFKFields, new int[] { (int)RevenueRecognitionFieldIndex.TransactionJournalID } );		
			_transactionJournal = null;
		}
		
		/// <summary> setups the sync logic for member _transactionJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionJournal(IEntityCore relatedEntity)
		{
			if(_transactionJournal!=relatedEntity)
			{		
				DesetupSyncTransactionJournal(true, true);
				_transactionJournal = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRevenueRecognitionRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, ref _alreadyFetchedTransactionJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="revenueRecognitionID">PK value for RevenueRecognition which data should be fetched into this RevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 revenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RevenueRecognitionFieldIndex.RevenueRecognitionID].ForcedCurrentValueWrite(revenueRecognitionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRevenueRecognitionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RevenueRecognitionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RevenueRecognitionRelations Relations
		{
			get	{ return new RevenueRecognitionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Apportion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionCollection(), (IEntityRelation)GetRelationsForField("Apportions")[0], (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, (int)VarioSL.Entities.EntityType.ApportionEntity, 0, null, null, null, "Apportions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettledRevenue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettledRevenues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettledRevenueCollection(), (IEntityRelation)GetRelationsForField("SettledRevenues")[0], (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, (int)VarioSL.Entities.EntityType.SettledRevenueEntity, 0, null, null, null, "SettledRevenues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloseoutPeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloseoutPeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CloseoutPeriodCollection(), (IEntityRelation)GetRelationsForField("CloseoutPeriod")[0], (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, 0, null, null, null, "CloseoutPeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueSettlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueSettlement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection(), (IEntityRelation)GetRelationsForField("RevenueSettlement")[0], (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, 0, null, null, null, "RevenueSettlement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournal")[0], (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)RevenueRecognitionFieldIndex.Amount, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.Amount, value, true); }
		}

		/// <summary> The AppliedPassTicketID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."APPLIEDPASSTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AppliedPassTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.AppliedPassTicketID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.AppliedPassTicketID, value, true); }
		}

		/// <summary> The BaseFare property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."BASEFARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.BaseFare, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The BoardingGuid property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."BOARDINGGUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BoardingGuid
		{
			get { return (System.String)GetValue((int)RevenueRecognitionFieldIndex.BoardingGuid, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.BoardingGuid, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."CLOSEOUTPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.CloseoutPeriodID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."CREDITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)RevenueRecognitionFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The CustomerGroup property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."CUSTOMERGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueRecognitionFieldIndex.CustomerGroup, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.CustomerGroup, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."DEBITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)RevenueRecognitionFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The IncludeInSettlement property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."INCLUDEINSETTLEMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IncludeInSettlement
		{
			get { return (Nullable<System.Boolean>)GetValue((int)RevenueRecognitionFieldIndex.IncludeInSettlement, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.IncludeInSettlement, value, true); }
		}

		/// <summary> The LastModified property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)RevenueRecognitionFieldIndex.LastModified, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)RevenueRecognitionFieldIndex.LastUser, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."LINEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OperatorID
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueRecognitionFieldIndex.OperatorID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The PostingDate property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."POSTINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RevenueRecognitionFieldIndex.PostingDate, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."POSTINGREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)RevenueRecognitionFieldIndex.PostingReference, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The Premium property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."PREMIUM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Premium
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.Premium, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.Premium, value, true); }
		}

		/// <summary> The RevenueRecognitionID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."REVENUERECOGNITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)RevenueRecognitionFieldIndex.RevenueRecognitionID, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.RevenueRecognitionID, value, true); }
		}

		/// <summary> The RevenueSettlementID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."REVENUESETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RevenueSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.RevenueSettlementID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.RevenueSettlementID, value, true); }
		}

		/// <summary> The TicketID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.TicketID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketNumber property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."TICKETNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueRecognitionFieldIndex.TicketNumber, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.TicketNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)RevenueRecognitionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionJournalID
		{
			get { return (System.Int64)GetValue((int)RevenueRecognitionFieldIndex.TransactionJournalID, true); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The TransitAccountID property of the Entity RevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUERECOGNITION"."TRANSITACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransitAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueRecognitionFieldIndex.TransitAccountID, false); }
			set	{ SetValue((int)RevenueRecognitionFieldIndex.TransitAccountID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection Apportions
		{
			get	{ return GetMultiApportions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Apportions. When set to true, Apportions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Apportions is accessed. You can always execute/ a forced fetch by calling GetMultiApportions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportions
		{
			get	{ return _alwaysFetchApportions; }
			set	{ _alwaysFetchApportions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Apportions already has been fetched. Setting this property to false when Apportions has been fetched
		/// will clear the Apportions collection well. Setting this property to true while Apportions hasn't been fetched disables lazy loading for Apportions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportions
		{
			get { return _alreadyFetchedApportions;}
			set 
			{
				if(_alreadyFetchedApportions && !value && (_apportions != null))
				{
					_apportions.Clear();
				}
				_alreadyFetchedApportions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettledRevenues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettledRevenueCollection SettledRevenues
		{
			get	{ return GetMultiSettledRevenues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettledRevenues. When set to true, SettledRevenues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettledRevenues is accessed. You can always execute/ a forced fetch by calling GetMultiSettledRevenues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettledRevenues
		{
			get	{ return _alwaysFetchSettledRevenues; }
			set	{ _alwaysFetchSettledRevenues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettledRevenues already has been fetched. Setting this property to false when SettledRevenues has been fetched
		/// will clear the SettledRevenues collection well. Setting this property to true while SettledRevenues hasn't been fetched disables lazy loading for SettledRevenues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettledRevenues
		{
			get { return _alreadyFetchedSettledRevenues;}
			set 
			{
				if(_alreadyFetchedSettledRevenues && !value && (_settledRevenues != null))
				{
					_settledRevenues.Clear();
				}
				_alreadyFetchedSettledRevenues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CloseoutPeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCloseoutPeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CloseoutPeriodEntity CloseoutPeriod
		{
			get	{ return GetSingleCloseoutPeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCloseoutPeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueRecognitions", "CloseoutPeriod", _closeoutPeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CloseoutPeriod. When set to true, CloseoutPeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloseoutPeriod is accessed. You can always execute a forced fetch by calling GetSingleCloseoutPeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloseoutPeriod
		{
			get	{ return _alwaysFetchCloseoutPeriod; }
			set	{ _alwaysFetchCloseoutPeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloseoutPeriod already has been fetched. Setting this property to false when CloseoutPeriod has been fetched
		/// will set CloseoutPeriod to null as well. Setting this property to true while CloseoutPeriod hasn't been fetched disables lazy loading for CloseoutPeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloseoutPeriod
		{
			get { return _alreadyFetchedCloseoutPeriod;}
			set 
			{
				if(_alreadyFetchedCloseoutPeriod && !value)
				{
					this.CloseoutPeriod = null;
				}
				_alreadyFetchedCloseoutPeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CloseoutPeriod is not found
		/// in the database. When set to true, CloseoutPeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CloseoutPeriodReturnsNewIfNotFound
		{
			get	{ return _closeoutPeriodReturnsNewIfNotFound; }
			set { _closeoutPeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RevenueSettlementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueSettlement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueSettlementEntity RevenueSettlement
		{
			get	{ return GetSingleRevenueSettlement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueSettlement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueRecognition", "RevenueSettlement", _revenueSettlement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueSettlement. When set to true, RevenueSettlement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueSettlement is accessed. You can always execute a forced fetch by calling GetSingleRevenueSettlement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueSettlement
		{
			get	{ return _alwaysFetchRevenueSettlement; }
			set	{ _alwaysFetchRevenueSettlement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueSettlement already has been fetched. Setting this property to false when RevenueSettlement has been fetched
		/// will set RevenueSettlement to null as well. Setting this property to true while RevenueSettlement hasn't been fetched disables lazy loading for RevenueSettlement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueSettlement
		{
			get { return _alreadyFetchedRevenueSettlement;}
			set 
			{
				if(_alreadyFetchedRevenueSettlement && !value)
				{
					this.RevenueSettlement = null;
				}
				_alreadyFetchedRevenueSettlement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueSettlement is not found
		/// in the database. When set to true, RevenueSettlement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueSettlementReturnsNewIfNotFound
		{
			get	{ return _revenueSettlementReturnsNewIfNotFound; }
			set { _revenueSettlementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity TransactionJournal
		{
			get	{ return GetSingleTransactionJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransactionJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueRecognitions", "TransactionJournal", _transactionJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournal. When set to true, TransactionJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournal is accessed. You can always execute a forced fetch by calling GetSingleTransactionJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournal
		{
			get	{ return _alwaysFetchTransactionJournal; }
			set	{ _alwaysFetchTransactionJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournal already has been fetched. Setting this property to false when TransactionJournal has been fetched
		/// will set TransactionJournal to null as well. Setting this property to true while TransactionJournal hasn't been fetched disables lazy loading for TransactionJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournal
		{
			get { return _alreadyFetchedTransactionJournal;}
			set 
			{
				if(_alreadyFetchedTransactionJournal && !value)
				{
					this.TransactionJournal = null;
				}
				_alreadyFetchedTransactionJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionJournal is not found
		/// in the database. When set to true, TransactionJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionJournalReturnsNewIfNotFound
		{
			get	{ return _transactionJournalReturnsNewIfNotFound; }
			set { _transactionJournalReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
