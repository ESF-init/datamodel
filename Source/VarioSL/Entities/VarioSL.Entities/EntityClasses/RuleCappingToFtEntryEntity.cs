﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RuleCappingToFtEntry'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RuleCappingToFtEntryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private FareTableEntryEntity _fareTableEntry;
		private bool	_alwaysFetchFareTableEntry, _alreadyFetchedFareTableEntry, _fareTableEntryReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FareTableEntry</summary>
			public static readonly string FareTableEntry = "FareTableEntry";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RuleCappingToFtEntryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleCappingToFtEntryEntity() :base("RuleCappingToFtEntryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		public RuleCappingToFtEntryEntity(System.Int64 fareTableEntryID, System.Int64 ruleCappingID):base("RuleCappingToFtEntryEntity")
		{
			InitClassFetch(fareTableEntryID, ruleCappingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RuleCappingToFtEntryEntity(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse):base("RuleCappingToFtEntryEntity")
		{
			InitClassFetch(fareTableEntryID, ruleCappingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="validator">The custom validator object for this RuleCappingToFtEntryEntity</param>
		public RuleCappingToFtEntryEntity(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IValidator validator):base("RuleCappingToFtEntryEntity")
		{
			InitClassFetch(fareTableEntryID, ruleCappingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleCappingToFtEntryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareTableEntry = (FareTableEntryEntity)info.GetValue("_fareTableEntry", typeof(FareTableEntryEntity));
			if(_fareTableEntry!=null)
			{
				_fareTableEntry.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareTableEntryReturnsNewIfNotFound = info.GetBoolean("_fareTableEntryReturnsNewIfNotFound");
			_alwaysFetchFareTableEntry = info.GetBoolean("_alwaysFetchFareTableEntry");
			_alreadyFetchedFareTableEntry = info.GetBoolean("_alreadyFetchedFareTableEntry");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RuleCappingToFtEntryFieldIndex)fieldIndex)
			{
				case RuleCappingToFtEntryFieldIndex.FareTableEntryID:
					DesetupSyncFareTableEntry(true, false);
					_alreadyFetchedFareTableEntry = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareTableEntry = (_fareTableEntry != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FareTableEntry":
					toReturn.Add(Relations.FareTableEntryEntityUsingFareTableEntryID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareTableEntry", (!this.MarkedForDeletion?_fareTableEntry:null));
			info.AddValue("_fareTableEntryReturnsNewIfNotFound", _fareTableEntryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareTableEntry", _alwaysFetchFareTableEntry);
			info.AddValue("_alreadyFetchedFareTableEntry", _alreadyFetchedFareTableEntry);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FareTableEntry":
					_alreadyFetchedFareTableEntry = true;
					this.FareTableEntry = (FareTableEntryEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FareTableEntry":
					SetupSyncFareTableEntry(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FareTableEntry":
					DesetupSyncFareTableEntry(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fareTableEntry!=null)
			{
				toReturn.Add(_fareTableEntry);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, System.Int64 ruleCappingID)
		{
			return FetchUsingPK(fareTableEntryID, ruleCappingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareTableEntryID, ruleCappingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareTableEntryID, ruleCappingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareTableEntryID, ruleCappingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareTableEntryID, this.RuleCappingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RuleCappingToFtEntryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntryEntity' which is related to this entity.</returns>
		public FareTableEntryEntity GetSingleFareTableEntry()
		{
			return GetSingleFareTableEntry(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntryEntity' which is related to this entity.</returns>
		public virtual FareTableEntryEntity GetSingleFareTableEntry(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareTableEntry || forceFetch || _alwaysFetchFareTableEntry) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntryEntityUsingFareTableEntryID);
				FareTableEntryEntity newEntity = new FareTableEntryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareTableEntryID);
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareTableEntryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareTableEntry = newEntity;
				_alreadyFetchedFareTableEntry = fetchResult;
			}
			return _fareTableEntry;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FareTableEntry", _fareTableEntry);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="validator">The validator object for this RuleCappingToFtEntryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareTableEntryID, ruleCappingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_fareTableEntryReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTableEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleCappingID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fareTableEntry</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareTableEntry(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareTableEntry, new PropertyChangedEventHandler( OnFareTableEntryPropertyChanged ), "FareTableEntry", VarioSL.Entities.RelationClasses.StaticRuleCappingToFtEntryRelations.FareTableEntryEntityUsingFareTableEntryIDStatic, true, signalRelatedEntity, "RuleCappingToFtEntry", resetFKFields, new int[] { (int)RuleCappingToFtEntryFieldIndex.FareTableEntryID } );		
			_fareTableEntry = null;
		}
		
		/// <summary> setups the sync logic for member _fareTableEntry</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareTableEntry(IEntityCore relatedEntity)
		{
			if(_fareTableEntry!=relatedEntity)
			{		
				DesetupSyncFareTableEntry(true, true);
				_fareTableEntry = (FareTableEntryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareTableEntry, new PropertyChangedEventHandler( OnFareTableEntryPropertyChanged ), "FareTableEntry", VarioSL.Entities.RelationClasses.StaticRuleCappingToFtEntryRelations.FareTableEntryEntityUsingFareTableEntryIDStatic, true, ref _alreadyFetchedFareTableEntry, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareTableEntryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareTableEntryID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="ruleCappingID">PK value for RuleCappingToFtEntry which data should be fetched into this RuleCappingToFtEntry object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareTableEntryID, System.Int64 ruleCappingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RuleCappingToFtEntryFieldIndex.FareTableEntryID].ForcedCurrentValueWrite(fareTableEntryID);
				this.Fields[(int)RuleCappingToFtEntryFieldIndex.RuleCappingID].ForcedCurrentValueWrite(ruleCappingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRuleCappingToFtEntryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RuleCappingToFtEntryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RuleCappingToFtEntryRelations Relations
		{
			get	{ return new RuleCappingToFtEntryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTableEntry'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTableEntry
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableEntryCollection(), (IEntityRelation)GetRelationsForField("FareTableEntry")[0], (int)VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity, (int)VarioSL.Entities.EntityType.FareTableEntryEntity, 0, null, null, null, "FareTableEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FareTableEntryID property of the Entity RuleCappingToFtEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPINGTOFTENTRY"."FARETABLEENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareTableEntryID
		{
			get { return (System.Int64)GetValue((int)RuleCappingToFtEntryFieldIndex.FareTableEntryID, true); }
			set	{ SetValue((int)RuleCappingToFtEntryFieldIndex.FareTableEntryID, value, true); }
		}

		/// <summary> The RuleCappingID property of the Entity RuleCappingToFtEntry<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_RULE_CAPPINGTOFTENTRY"."RULECAPPINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RuleCappingID
		{
			get { return (System.Int64)GetValue((int)RuleCappingToFtEntryFieldIndex.RuleCappingID, true); }
			set	{ SetValue((int)RuleCappingToFtEntryFieldIndex.RuleCappingID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'FareTableEntryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareTableEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntryEntity FareTableEntry
		{
			get	{ return GetSingleFareTableEntry(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareTableEntry(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RuleCappingToFtEntry", "FareTableEntry", _fareTableEntry, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareTableEntry. When set to true, FareTableEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTableEntry is accessed. You can always execute a forced fetch by calling GetSingleFareTableEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTableEntry
		{
			get	{ return _alwaysFetchFareTableEntry; }
			set	{ _alwaysFetchFareTableEntry = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTableEntry already has been fetched. Setting this property to false when FareTableEntry has been fetched
		/// will set FareTableEntry to null as well. Setting this property to true while FareTableEntry hasn't been fetched disables lazy loading for FareTableEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTableEntry
		{
			get { return _alreadyFetchedFareTableEntry;}
			set 
			{
				if(_alreadyFetchedFareTableEntry && !value)
				{
					this.FareTableEntry = null;
				}
				_alreadyFetchedFareTableEntry = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareTableEntry is not found
		/// in the database. When set to true, FareTableEntry will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareTableEntryReturnsNewIfNotFound
		{
			get	{ return _fareTableEntryReturnsNewIfNotFound; }
			set { _fareTableEntryReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
