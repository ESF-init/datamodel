﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrganizationParticipant'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrganizationParticipantEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private ParticipantGroupEntity _participantGroup;
		private bool	_alwaysFetchParticipantGroup, _alreadyFetchedParticipantGroup, _participantGroupReturnsNewIfNotFound;
		private PhotographEntity _photograph;
		private bool	_alwaysFetchPhotograph, _alreadyFetchedPhotograph, _photographReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name ParticipantGroup</summary>
			public static readonly string ParticipantGroup = "ParticipantGroup";
			/// <summary>Member name Photograph</summary>
			public static readonly string Photograph = "Photograph";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrganizationParticipantEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrganizationParticipantEntity() :base("OrganizationParticipantEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		public OrganizationParticipantEntity(System.Int64 organizationParticipantID):base("OrganizationParticipantEntity")
		{
			InitClassFetch(organizationParticipantID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrganizationParticipantEntity(System.Int64 organizationParticipantID, IPrefetchPath prefetchPathToUse):base("OrganizationParticipantEntity")
		{
			InitClassFetch(organizationParticipantID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="validator">The custom validator object for this OrganizationParticipantEntity</param>
		public OrganizationParticipantEntity(System.Int64 organizationParticipantID, IValidator validator):base("OrganizationParticipantEntity")
		{
			InitClassFetch(organizationParticipantID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrganizationParticipantEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_participantGroup = (ParticipantGroupEntity)info.GetValue("_participantGroup", typeof(ParticipantGroupEntity));
			if(_participantGroup!=null)
			{
				_participantGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_participantGroupReturnsNewIfNotFound = info.GetBoolean("_participantGroupReturnsNewIfNotFound");
			_alwaysFetchParticipantGroup = info.GetBoolean("_alwaysFetchParticipantGroup");
			_alreadyFetchedParticipantGroup = info.GetBoolean("_alreadyFetchedParticipantGroup");

			_photograph = (PhotographEntity)info.GetValue("_photograph", typeof(PhotographEntity));
			if(_photograph!=null)
			{
				_photograph.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_photographReturnsNewIfNotFound = info.GetBoolean("_photographReturnsNewIfNotFound");
			_alwaysFetchPhotograph = info.GetBoolean("_alwaysFetchPhotograph");
			_alreadyFetchedPhotograph = info.GetBoolean("_alreadyFetchedPhotograph");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrganizationParticipantFieldIndex)fieldIndex)
			{
				case OrganizationParticipantFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case OrganizationParticipantFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case OrganizationParticipantFieldIndex.ParticipantGroupID:
					DesetupSyncParticipantGroup(true, false);
					_alreadyFetchedParticipantGroup = false;
					break;
				case OrganizationParticipantFieldIndex.PhotographID:
					DesetupSyncPhotograph(true, false);
					_alreadyFetchedPhotograph = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedParticipantGroup = (_participantGroup != null);
			_alreadyFetchedPhotograph = (_photograph != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "ParticipantGroup":
					toReturn.Add(Relations.ParticipantGroupEntityUsingParticipantGroupID);
					break;
				case "Photograph":
					toReturn.Add(Relations.PhotographEntityUsingPhotographID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_participantGroup", (!this.MarkedForDeletion?_participantGroup:null));
			info.AddValue("_participantGroupReturnsNewIfNotFound", _participantGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParticipantGroup", _alwaysFetchParticipantGroup);
			info.AddValue("_alreadyFetchedParticipantGroup", _alreadyFetchedParticipantGroup);
			info.AddValue("_photograph", (!this.MarkedForDeletion?_photograph:null));
			info.AddValue("_photographReturnsNewIfNotFound", _photographReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPhotograph", _alwaysFetchPhotograph);
			info.AddValue("_alreadyFetchedPhotograph", _alreadyFetchedPhotograph);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "ParticipantGroup":
					_alreadyFetchedParticipantGroup = true;
					this.ParticipantGroup = (ParticipantGroupEntity)entity;
					break;
				case "Photograph":
					_alreadyFetchedPhotograph = true;
					this.Photograph = (PhotographEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "ParticipantGroup":
					SetupSyncParticipantGroup(relatedEntity);
					break;
				case "Photograph":
					SetupSyncPhotograph(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "ParticipantGroup":
					DesetupSyncParticipantGroup(false, true);
					break;
				case "Photograph":
					DesetupSyncPhotograph(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_participantGroup!=null)
			{
				toReturn.Add(_participantGroup);
			}
			if(_photograph!=null)
			{
				toReturn.Add(_photograph);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationParticipantID)
		{
			return FetchUsingPK(organizationParticipantID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationParticipantID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(organizationParticipantID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationParticipantID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(organizationParticipantID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 organizationParticipantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(organizationParticipantID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrganizationParticipantID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrganizationParticipantRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'ParticipantGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParticipantGroupEntity' which is related to this entity.</returns>
		public ParticipantGroupEntity GetSingleParticipantGroup()
		{
			return GetSingleParticipantGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'ParticipantGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParticipantGroupEntity' which is related to this entity.</returns>
		public virtual ParticipantGroupEntity GetSingleParticipantGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedParticipantGroup || forceFetch || _alwaysFetchParticipantGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParticipantGroupEntityUsingParticipantGroupID);
				ParticipantGroupEntity newEntity = new ParticipantGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParticipantGroupID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ParticipantGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_participantGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParticipantGroup = newEntity;
				_alreadyFetchedParticipantGroup = fetchResult;
			}
			return _participantGroup;
		}


		/// <summary> Retrieves the related entity of type 'PhotographEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PhotographEntity' which is related to this entity.</returns>
		public PhotographEntity GetSinglePhotograph()
		{
			return GetSinglePhotograph(false);
		}

		/// <summary> Retrieves the related entity of type 'PhotographEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PhotographEntity' which is related to this entity.</returns>
		public virtual PhotographEntity GetSinglePhotograph(bool forceFetch)
		{
			if( ( !_alreadyFetchedPhotograph || forceFetch || _alwaysFetchPhotograph) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PhotographEntityUsingPhotographID);
				PhotographEntity newEntity = new PhotographEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PhotographID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PhotographEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_photographReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Photograph = newEntity;
				_alreadyFetchedPhotograph = fetchResult;
			}
			return _photograph;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("ParticipantGroup", _participantGroup);
			toReturn.Add("Photograph", _photograph);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="validator">The validator object for this OrganizationParticipantEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 organizationParticipantID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(organizationParticipantID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_participantGroupReturnsNewIfNotFound = false;
			_photographReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CellPhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DateOfBirth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FaxNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Gender", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Identifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationParticipantID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParticipantGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhotographID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "OrganizationParticipants", resetFKFields, new int[] { (int)OrganizationParticipantFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "OrganizationParticipants", resetFKFields, new int[] { (int)OrganizationParticipantFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _participantGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParticipantGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _participantGroup, new PropertyChangedEventHandler( OnParticipantGroupPropertyChanged ), "ParticipantGroup", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.ParticipantGroupEntityUsingParticipantGroupIDStatic, true, signalRelatedEntity, "OrganizationParticipants", resetFKFields, new int[] { (int)OrganizationParticipantFieldIndex.ParticipantGroupID } );		
			_participantGroup = null;
		}
		
		/// <summary> setups the sync logic for member _participantGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParticipantGroup(IEntityCore relatedEntity)
		{
			if(_participantGroup!=relatedEntity)
			{		
				DesetupSyncParticipantGroup(true, true);
				_participantGroup = (ParticipantGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _participantGroup, new PropertyChangedEventHandler( OnParticipantGroupPropertyChanged ), "ParticipantGroup", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.ParticipantGroupEntityUsingParticipantGroupIDStatic, true, ref _alreadyFetchedParticipantGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParticipantGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _photograph</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPhotograph(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _photograph, new PropertyChangedEventHandler( OnPhotographPropertyChanged ), "Photograph", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.PhotographEntityUsingPhotographIDStatic, true, signalRelatedEntity, "OrganizationParticipants", resetFKFields, new int[] { (int)OrganizationParticipantFieldIndex.PhotographID } );		
			_photograph = null;
		}
		
		/// <summary> setups the sync logic for member _photograph</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPhotograph(IEntityCore relatedEntity)
		{
			if(_photograph!=relatedEntity)
			{		
				DesetupSyncPhotograph(true, true);
				_photograph = (PhotographEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _photograph, new PropertyChangedEventHandler( OnPhotographPropertyChanged ), "Photograph", VarioSL.Entities.RelationClasses.StaticOrganizationParticipantRelations.PhotographEntityUsingPhotographIDStatic, true, ref _alreadyFetchedPhotograph, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPhotographPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="organizationParticipantID">PK value for OrganizationParticipant which data should be fetched into this OrganizationParticipant object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 organizationParticipantID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrganizationParticipantFieldIndex.OrganizationParticipantID].ForcedCurrentValueWrite(organizationParticipantID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrganizationParticipantDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrganizationParticipantEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrganizationParticipantRelations Relations
		{
			get	{ return new OrganizationParticipantRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParticipantGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParticipantGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParticipantGroupCollection(), (IEntityRelation)GetRelationsForField("ParticipantGroup")[0], (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, (int)VarioSL.Entities.EntityType.ParticipantGroupEntity, 0, null, null, null, "ParticipantGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Photograph'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPhotograph
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PhotographCollection(), (IEntityRelation)GetRelationsForField("Photograph")[0], (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, (int)VarioSL.Entities.EntityType.PhotographEntity, 0, null, null, null, "Photograph", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardID property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrganizationParticipantFieldIndex.CardID, false); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.CardID, value, true); }
		}

		/// <summary> The CellPhoneNumber property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."CELLPHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CellPhoneNumber
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.CellPhoneNumber, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.CellPhoneNumber, value, true); }
		}

		/// <summary> The ContractID property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)OrganizationParticipantFieldIndex.ContractID, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.ContractID, value, true); }
		}

		/// <summary> The DateOfBirth property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."DATEOFBIRTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DateOfBirth
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrganizationParticipantFieldIndex.DateOfBirth, false); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.DateOfBirth, value, true); }
		}

		/// <summary> The Email property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.Email, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.Email, value, true); }
		}

		/// <summary> The FaxNumber property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."FAXNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FaxNumber
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.FaxNumber, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.FaxNumber, value, true); }
		}

		/// <summary> The FirstName property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."FIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.FirstName, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.FirstName, value, true); }
		}

		/// <summary> The Gender property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."GENDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.Gender> Gender
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.Gender>)GetValue((int)OrganizationParticipantFieldIndex.Gender, false); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.Gender, value, true); }
		}

		/// <summary> The Identifier property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."IDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.Identifier, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.Identifier, value, true); }
		}

		/// <summary> The LastModified property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)OrganizationParticipantFieldIndex.LastModified, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastName property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."LASTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastName
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.LastName, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.LastName, value, true); }
		}

		/// <summary> The LastUser property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.LastUser, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrganizationParticipantID property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."ORGANIZATIONPARTICIPANTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrganizationParticipantID
		{
			get { return (System.Int64)GetValue((int)OrganizationParticipantFieldIndex.OrganizationParticipantID, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.OrganizationParticipantID, value, true); }
		}

		/// <summary> The ParticipantGroupID property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."PARTICIPANTGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ParticipantGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrganizationParticipantFieldIndex.ParticipantGroupID, false); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.ParticipantGroupID, value, true); }
		}

		/// <summary> The PhoneNumber property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."PHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhoneNumber
		{
			get { return (System.String)GetValue((int)OrganizationParticipantFieldIndex.PhoneNumber, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.PhoneNumber, value, true); }
		}

		/// <summary> The PhotographID property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."PHOTOGRAPHID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PhotographID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrganizationParticipantFieldIndex.PhotographID, false); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.PhotographID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity OrganizationParticipant<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORGANIZATIONPARTICIPANT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)OrganizationParticipantFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)OrganizationParticipantFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrganizationParticipants", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrganizationParticipants", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ParticipantGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParticipantGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParticipantGroupEntity ParticipantGroup
		{
			get	{ return GetSingleParticipantGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParticipantGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrganizationParticipants", "ParticipantGroup", _participantGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParticipantGroup. When set to true, ParticipantGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParticipantGroup is accessed. You can always execute a forced fetch by calling GetSingleParticipantGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParticipantGroup
		{
			get	{ return _alwaysFetchParticipantGroup; }
			set	{ _alwaysFetchParticipantGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParticipantGroup already has been fetched. Setting this property to false when ParticipantGroup has been fetched
		/// will set ParticipantGroup to null as well. Setting this property to true while ParticipantGroup hasn't been fetched disables lazy loading for ParticipantGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParticipantGroup
		{
			get { return _alreadyFetchedParticipantGroup;}
			set 
			{
				if(_alreadyFetchedParticipantGroup && !value)
				{
					this.ParticipantGroup = null;
				}
				_alreadyFetchedParticipantGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParticipantGroup is not found
		/// in the database. When set to true, ParticipantGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParticipantGroupReturnsNewIfNotFound
		{
			get	{ return _participantGroupReturnsNewIfNotFound; }
			set { _participantGroupReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PhotographEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePhotograph()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PhotographEntity Photograph
		{
			get	{ return GetSinglePhotograph(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPhotograph(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrganizationParticipants", "Photograph", _photograph, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Photograph. When set to true, Photograph is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Photograph is accessed. You can always execute a forced fetch by calling GetSinglePhotograph(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPhotograph
		{
			get	{ return _alwaysFetchPhotograph; }
			set	{ _alwaysFetchPhotograph = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Photograph already has been fetched. Setting this property to false when Photograph has been fetched
		/// will set Photograph to null as well. Setting this property to true while Photograph hasn't been fetched disables lazy loading for Photograph</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPhotograph
		{
			get { return _alreadyFetchedPhotograph;}
			set 
			{
				if(_alreadyFetchedPhotograph && !value)
				{
					this.Photograph = null;
				}
				_alreadyFetchedPhotograph = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Photograph is not found
		/// in the database. When set to true, Photograph will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PhotographReturnsNewIfNotFound
		{
			get	{ return _photographReturnsNewIfNotFound; }
			set { _photographReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
