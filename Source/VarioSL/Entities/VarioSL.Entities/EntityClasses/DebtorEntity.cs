﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Debtor'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DebtorEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AccountEntryCollection	_accountEntries;
		private bool	_alwaysFetchAccountEntries, _alreadyFetchedAccountEntries;
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResults;
		private bool	_alwaysFetchClearingResults, _alreadyFetchedClearingResults;
		private VarioSL.Entities.CollectionClasses.ComponentClearingCollection	_componentClearingsByMaintenanceStaff;
		private bool	_alwaysFetchComponentClearingsByMaintenanceStaff, _alreadyFetchedComponentClearingsByMaintenanceStaff;
		private VarioSL.Entities.CollectionClasses.ComponentClearingCollection	_componentClearingsByPersonnelClearing;
		private bool	_alwaysFetchComponentClearingsByPersonnelClearing, _alreadyFetchedComponentClearingsByPersonnelClearing;
		private VarioSL.Entities.CollectionClasses.ComponentFillingCollection	_componentFillingsByMaintenanceStaff;
		private bool	_alwaysFetchComponentFillingsByMaintenanceStaff, _alreadyFetchedComponentFillingsByMaintenanceStaff;
		private VarioSL.Entities.CollectionClasses.ComponentFillingCollection	_componentFillingsByPersonnelFilling;
		private bool	_alwaysFetchComponentFillingsByPersonnelFilling, _alreadyFetchedComponentFillingsByPersonnelFilling;
		private VarioSL.Entities.CollectionClasses.QualificationCollection	_qualifications;
		private bool	_alwaysFetchQualifications, _alreadyFetchedQualifications;
		private VarioSL.Entities.CollectionClasses.UserListCollection	_users;
		private bool	_alwaysFetchUsers, _alreadyFetchedUsers;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidents;
		private bool	_alwaysFetchFareEvasionIncidents, _alreadyFetchedFareEvasionIncidents;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DepotEntity _depot;
		private bool	_alwaysFetchDepot, _alreadyFetchedDepot, _depotReturnsNewIfNotFound;
		private VarioAddressEntity _address;
		private bool	_alwaysFetchAddress, _alreadyFetchedAddress, _addressReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Depot</summary>
			public static readonly string Depot = "Depot";
			/// <summary>Member name AccountEntries</summary>
			public static readonly string AccountEntries = "AccountEntries";
			/// <summary>Member name ClearingResults</summary>
			public static readonly string ClearingResults = "ClearingResults";
			/// <summary>Member name ComponentClearingsByMaintenanceStaff</summary>
			public static readonly string ComponentClearingsByMaintenanceStaff = "ComponentClearingsByMaintenanceStaff";
			/// <summary>Member name ComponentClearingsByPersonnelClearing</summary>
			public static readonly string ComponentClearingsByPersonnelClearing = "ComponentClearingsByPersonnelClearing";
			/// <summary>Member name ComponentFillingsByMaintenanceStaff</summary>
			public static readonly string ComponentFillingsByMaintenanceStaff = "ComponentFillingsByMaintenanceStaff";
			/// <summary>Member name ComponentFillingsByPersonnelFilling</summary>
			public static readonly string ComponentFillingsByPersonnelFilling = "ComponentFillingsByPersonnelFilling";
			/// <summary>Member name Qualifications</summary>
			public static readonly string Qualifications = "Qualifications";
			/// <summary>Member name Users</summary>
			public static readonly string Users = "Users";
			/// <summary>Member name FareEvasionIncidents</summary>
			public static readonly string FareEvasionIncidents = "FareEvasionIncidents";
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DebtorEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DebtorEntity() :base("DebtorEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		public DebtorEntity(System.Int64 debtorID):base("DebtorEntity")
		{
			InitClassFetch(debtorID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DebtorEntity(System.Int64 debtorID, IPrefetchPath prefetchPathToUse):base("DebtorEntity")
		{
			InitClassFetch(debtorID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="validator">The custom validator object for this DebtorEntity</param>
		public DebtorEntity(System.Int64 debtorID, IValidator validator):base("DebtorEntity")
		{
			InitClassFetch(debtorID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DebtorEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_accountEntries = (VarioSL.Entities.CollectionClasses.AccountEntryCollection)info.GetValue("_accountEntries", typeof(VarioSL.Entities.CollectionClasses.AccountEntryCollection));
			_alwaysFetchAccountEntries = info.GetBoolean("_alwaysFetchAccountEntries");
			_alreadyFetchedAccountEntries = info.GetBoolean("_alreadyFetchedAccountEntries");

			_clearingResults = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResults", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResults = info.GetBoolean("_alwaysFetchClearingResults");
			_alreadyFetchedClearingResults = info.GetBoolean("_alreadyFetchedClearingResults");

			_componentClearingsByMaintenanceStaff = (VarioSL.Entities.CollectionClasses.ComponentClearingCollection)info.GetValue("_componentClearingsByMaintenanceStaff", typeof(VarioSL.Entities.CollectionClasses.ComponentClearingCollection));
			_alwaysFetchComponentClearingsByMaintenanceStaff = info.GetBoolean("_alwaysFetchComponentClearingsByMaintenanceStaff");
			_alreadyFetchedComponentClearingsByMaintenanceStaff = info.GetBoolean("_alreadyFetchedComponentClearingsByMaintenanceStaff");

			_componentClearingsByPersonnelClearing = (VarioSL.Entities.CollectionClasses.ComponentClearingCollection)info.GetValue("_componentClearingsByPersonnelClearing", typeof(VarioSL.Entities.CollectionClasses.ComponentClearingCollection));
			_alwaysFetchComponentClearingsByPersonnelClearing = info.GetBoolean("_alwaysFetchComponentClearingsByPersonnelClearing");
			_alreadyFetchedComponentClearingsByPersonnelClearing = info.GetBoolean("_alreadyFetchedComponentClearingsByPersonnelClearing");

			_componentFillingsByMaintenanceStaff = (VarioSL.Entities.CollectionClasses.ComponentFillingCollection)info.GetValue("_componentFillingsByMaintenanceStaff", typeof(VarioSL.Entities.CollectionClasses.ComponentFillingCollection));
			_alwaysFetchComponentFillingsByMaintenanceStaff = info.GetBoolean("_alwaysFetchComponentFillingsByMaintenanceStaff");
			_alreadyFetchedComponentFillingsByMaintenanceStaff = info.GetBoolean("_alreadyFetchedComponentFillingsByMaintenanceStaff");

			_componentFillingsByPersonnelFilling = (VarioSL.Entities.CollectionClasses.ComponentFillingCollection)info.GetValue("_componentFillingsByPersonnelFilling", typeof(VarioSL.Entities.CollectionClasses.ComponentFillingCollection));
			_alwaysFetchComponentFillingsByPersonnelFilling = info.GetBoolean("_alwaysFetchComponentFillingsByPersonnelFilling");
			_alreadyFetchedComponentFillingsByPersonnelFilling = info.GetBoolean("_alreadyFetchedComponentFillingsByPersonnelFilling");

			_qualifications = (VarioSL.Entities.CollectionClasses.QualificationCollection)info.GetValue("_qualifications", typeof(VarioSL.Entities.CollectionClasses.QualificationCollection));
			_alwaysFetchQualifications = info.GetBoolean("_alwaysFetchQualifications");
			_alreadyFetchedQualifications = info.GetBoolean("_alreadyFetchedQualifications");

			_users = (VarioSL.Entities.CollectionClasses.UserListCollection)info.GetValue("_users", typeof(VarioSL.Entities.CollectionClasses.UserListCollection));
			_alwaysFetchUsers = info.GetBoolean("_alwaysFetchUsers");
			_alreadyFetchedUsers = info.GetBoolean("_alreadyFetchedUsers");

			_fareEvasionIncidents = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidents", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidents = info.GetBoolean("_alwaysFetchFareEvasionIncidents");
			_alreadyFetchedFareEvasionIncidents = info.GetBoolean("_alreadyFetchedFareEvasionIncidents");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_depot = (DepotEntity)info.GetValue("_depot", typeof(DepotEntity));
			if(_depot!=null)
			{
				_depot.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_depotReturnsNewIfNotFound = info.GetBoolean("_depotReturnsNewIfNotFound");
			_alwaysFetchDepot = info.GetBoolean("_alwaysFetchDepot");
			_alreadyFetchedDepot = info.GetBoolean("_alreadyFetchedDepot");
			_address = (VarioAddressEntity)info.GetValue("_address", typeof(VarioAddressEntity));
			if(_address!=null)
			{
				_address.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressReturnsNewIfNotFound = info.GetBoolean("_addressReturnsNewIfNotFound");
			_alwaysFetchAddress = info.GetBoolean("_alwaysFetchAddress");
			_alreadyFetchedAddress = info.GetBoolean("_alreadyFetchedAddress");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DebtorFieldIndex)fieldIndex)
			{
				case DebtorFieldIndex.AddressID:
					DesetupSyncAddress(true, false);
					_alreadyFetchedAddress = false;
					break;
				case DebtorFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case DebtorFieldIndex.DepotID:
					DesetupSyncDepot(true, false);
					_alreadyFetchedDepot = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAccountEntries = (_accountEntries.Count > 0);
			_alreadyFetchedClearingResults = (_clearingResults.Count > 0);
			_alreadyFetchedComponentClearingsByMaintenanceStaff = (_componentClearingsByMaintenanceStaff.Count > 0);
			_alreadyFetchedComponentClearingsByPersonnelClearing = (_componentClearingsByPersonnelClearing.Count > 0);
			_alreadyFetchedComponentFillingsByMaintenanceStaff = (_componentFillingsByMaintenanceStaff.Count > 0);
			_alreadyFetchedComponentFillingsByPersonnelFilling = (_componentFillingsByPersonnelFilling.Count > 0);
			_alreadyFetchedQualifications = (_qualifications.Count > 0);
			_alreadyFetchedUsers = (_users.Count > 0);
			_alreadyFetchedFareEvasionIncidents = (_fareEvasionIncidents.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDepot = (_depot != null);
			_alreadyFetchedAddress = (_address != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Depot":
					toReturn.Add(Relations.DepotEntityUsingDepotID);
					break;
				case "AccountEntries":
					toReturn.Add(Relations.AccountEntryEntityUsingContractID);
					break;
				case "ClearingResults":
					toReturn.Add(Relations.ClearingResultEntityUsingDebtorID);
					break;
				case "ComponentClearingsByMaintenanceStaff":
					toReturn.Add(Relations.ComponentClearingEntityUsingMaintenanceStaffID);
					break;
				case "ComponentClearingsByPersonnelClearing":
					toReturn.Add(Relations.ComponentClearingEntityUsingPersonnelClearingID);
					break;
				case "ComponentFillingsByMaintenanceStaff":
					toReturn.Add(Relations.ComponentFillingEntityUsingMaintenanceStaffID);
					break;
				case "ComponentFillingsByPersonnelFilling":
					toReturn.Add(Relations.ComponentFillingEntityUsingPersonnelFillingID);
					break;
				case "Qualifications":
					toReturn.Add(Relations.QualificationEntityUsingDebtorID);
					break;
				case "Users":
					toReturn.Add(Relations.UserListEntityUsingDebtorID);
					break;
				case "FareEvasionIncidents":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingDebtorID);
					break;
				case "Address":
					toReturn.Add(Relations.VarioAddressEntityUsingAddressID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_accountEntries", (!this.MarkedForDeletion?_accountEntries:null));
			info.AddValue("_alwaysFetchAccountEntries", _alwaysFetchAccountEntries);
			info.AddValue("_alreadyFetchedAccountEntries", _alreadyFetchedAccountEntries);
			info.AddValue("_clearingResults", (!this.MarkedForDeletion?_clearingResults:null));
			info.AddValue("_alwaysFetchClearingResults", _alwaysFetchClearingResults);
			info.AddValue("_alreadyFetchedClearingResults", _alreadyFetchedClearingResults);
			info.AddValue("_componentClearingsByMaintenanceStaff", (!this.MarkedForDeletion?_componentClearingsByMaintenanceStaff:null));
			info.AddValue("_alwaysFetchComponentClearingsByMaintenanceStaff", _alwaysFetchComponentClearingsByMaintenanceStaff);
			info.AddValue("_alreadyFetchedComponentClearingsByMaintenanceStaff", _alreadyFetchedComponentClearingsByMaintenanceStaff);
			info.AddValue("_componentClearingsByPersonnelClearing", (!this.MarkedForDeletion?_componentClearingsByPersonnelClearing:null));
			info.AddValue("_alwaysFetchComponentClearingsByPersonnelClearing", _alwaysFetchComponentClearingsByPersonnelClearing);
			info.AddValue("_alreadyFetchedComponentClearingsByPersonnelClearing", _alreadyFetchedComponentClearingsByPersonnelClearing);
			info.AddValue("_componentFillingsByMaintenanceStaff", (!this.MarkedForDeletion?_componentFillingsByMaintenanceStaff:null));
			info.AddValue("_alwaysFetchComponentFillingsByMaintenanceStaff", _alwaysFetchComponentFillingsByMaintenanceStaff);
			info.AddValue("_alreadyFetchedComponentFillingsByMaintenanceStaff", _alreadyFetchedComponentFillingsByMaintenanceStaff);
			info.AddValue("_componentFillingsByPersonnelFilling", (!this.MarkedForDeletion?_componentFillingsByPersonnelFilling:null));
			info.AddValue("_alwaysFetchComponentFillingsByPersonnelFilling", _alwaysFetchComponentFillingsByPersonnelFilling);
			info.AddValue("_alreadyFetchedComponentFillingsByPersonnelFilling", _alreadyFetchedComponentFillingsByPersonnelFilling);
			info.AddValue("_qualifications", (!this.MarkedForDeletion?_qualifications:null));
			info.AddValue("_alwaysFetchQualifications", _alwaysFetchQualifications);
			info.AddValue("_alreadyFetchedQualifications", _alreadyFetchedQualifications);
			info.AddValue("_users", (!this.MarkedForDeletion?_users:null));
			info.AddValue("_alwaysFetchUsers", _alwaysFetchUsers);
			info.AddValue("_alreadyFetchedUsers", _alreadyFetchedUsers);
			info.AddValue("_fareEvasionIncidents", (!this.MarkedForDeletion?_fareEvasionIncidents:null));
			info.AddValue("_alwaysFetchFareEvasionIncidents", _alwaysFetchFareEvasionIncidents);
			info.AddValue("_alreadyFetchedFareEvasionIncidents", _alreadyFetchedFareEvasionIncidents);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_depot", (!this.MarkedForDeletion?_depot:null));
			info.AddValue("_depotReturnsNewIfNotFound", _depotReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDepot", _alwaysFetchDepot);
			info.AddValue("_alreadyFetchedDepot", _alreadyFetchedDepot);

			info.AddValue("_address", (!this.MarkedForDeletion?_address:null));
			info.AddValue("_addressReturnsNewIfNotFound", _addressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddress", _alwaysFetchAddress);
			info.AddValue("_alreadyFetchedAddress", _alreadyFetchedAddress);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Depot":
					_alreadyFetchedDepot = true;
					this.Depot = (DepotEntity)entity;
					break;
				case "AccountEntries":
					_alreadyFetchedAccountEntries = true;
					if(entity!=null)
					{
						this.AccountEntries.Add((AccountEntryEntity)entity);
					}
					break;
				case "ClearingResults":
					_alreadyFetchedClearingResults = true;
					if(entity!=null)
					{
						this.ClearingResults.Add((ClearingResultEntity)entity);
					}
					break;
				case "ComponentClearingsByMaintenanceStaff":
					_alreadyFetchedComponentClearingsByMaintenanceStaff = true;
					if(entity!=null)
					{
						this.ComponentClearingsByMaintenanceStaff.Add((ComponentClearingEntity)entity);
					}
					break;
				case "ComponentClearingsByPersonnelClearing":
					_alreadyFetchedComponentClearingsByPersonnelClearing = true;
					if(entity!=null)
					{
						this.ComponentClearingsByPersonnelClearing.Add((ComponentClearingEntity)entity);
					}
					break;
				case "ComponentFillingsByMaintenanceStaff":
					_alreadyFetchedComponentFillingsByMaintenanceStaff = true;
					if(entity!=null)
					{
						this.ComponentFillingsByMaintenanceStaff.Add((ComponentFillingEntity)entity);
					}
					break;
				case "ComponentFillingsByPersonnelFilling":
					_alreadyFetchedComponentFillingsByPersonnelFilling = true;
					if(entity!=null)
					{
						this.ComponentFillingsByPersonnelFilling.Add((ComponentFillingEntity)entity);
					}
					break;
				case "Qualifications":
					_alreadyFetchedQualifications = true;
					if(entity!=null)
					{
						this.Qualifications.Add((QualificationEntity)entity);
					}
					break;
				case "Users":
					_alreadyFetchedUsers = true;
					if(entity!=null)
					{
						this.Users.Add((UserListEntity)entity);
					}
					break;
				case "FareEvasionIncidents":
					_alreadyFetchedFareEvasionIncidents = true;
					if(entity!=null)
					{
						this.FareEvasionIncidents.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "Address":
					_alreadyFetchedAddress = true;
					this.Address = (VarioAddressEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Depot":
					SetupSyncDepot(relatedEntity);
					break;
				case "AccountEntries":
					_accountEntries.Add((AccountEntryEntity)relatedEntity);
					break;
				case "ClearingResults":
					_clearingResults.Add((ClearingResultEntity)relatedEntity);
					break;
				case "ComponentClearingsByMaintenanceStaff":
					_componentClearingsByMaintenanceStaff.Add((ComponentClearingEntity)relatedEntity);
					break;
				case "ComponentClearingsByPersonnelClearing":
					_componentClearingsByPersonnelClearing.Add((ComponentClearingEntity)relatedEntity);
					break;
				case "ComponentFillingsByMaintenanceStaff":
					_componentFillingsByMaintenanceStaff.Add((ComponentFillingEntity)relatedEntity);
					break;
				case "ComponentFillingsByPersonnelFilling":
					_componentFillingsByPersonnelFilling.Add((ComponentFillingEntity)relatedEntity);
					break;
				case "Qualifications":
					_qualifications.Add((QualificationEntity)relatedEntity);
					break;
				case "Users":
					_users.Add((UserListEntity)relatedEntity);
					break;
				case "FareEvasionIncidents":
					_fareEvasionIncidents.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "Address":
					SetupSyncAddress(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Depot":
					DesetupSyncDepot(false, true);
					break;
				case "AccountEntries":
					this.PerformRelatedEntityRemoval(_accountEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClearingResults":
					this.PerformRelatedEntityRemoval(_clearingResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentClearingsByMaintenanceStaff":
					this.PerformRelatedEntityRemoval(_componentClearingsByMaintenanceStaff, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentClearingsByPersonnelClearing":
					this.PerformRelatedEntityRemoval(_componentClearingsByPersonnelClearing, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentFillingsByMaintenanceStaff":
					this.PerformRelatedEntityRemoval(_componentFillingsByMaintenanceStaff, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ComponentFillingsByPersonnelFilling":
					this.PerformRelatedEntityRemoval(_componentFillingsByPersonnelFilling, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Qualifications":
					this.PerformRelatedEntityRemoval(_qualifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Users":
					this.PerformRelatedEntityRemoval(_users, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidents":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Address":
					DesetupSyncAddress(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_depot!=null)
			{
				toReturn.Add(_depot);
			}
			if(_address!=null)
			{
				toReturn.Add(_address);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_accountEntries);
			toReturn.Add(_clearingResults);
			toReturn.Add(_componentClearingsByMaintenanceStaff);
			toReturn.Add(_componentClearingsByPersonnelClearing);
			toReturn.Add(_componentFillingsByMaintenanceStaff);
			toReturn.Add(_componentFillingsByPersonnelFilling);
			toReturn.Add(_qualifications);
			toReturn.Add(_users);
			toReturn.Add(_fareEvasionIncidents);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="debtorNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDDebtorNumber(System.Int64 clientID, System.Int64 debtorNumber)
		{
			return FetchUsingUCClientIDDebtorNumber( clientID,  debtorNumber, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="debtorNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDDebtorNumber(System.Int64 clientID, System.Int64 debtorNumber, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCClientIDDebtorNumber( clientID,  debtorNumber, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="debtorNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDDebtorNumber(System.Int64 clientID, System.Int64 debtorNumber, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCClientIDDebtorNumber( clientID,  debtorNumber, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="debtorNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDDebtorNumber(System.Int64 clientID, System.Int64 debtorNumber, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((DebtorDAO)CreateDAOInstance()).FetchDebtorUsingUCClientIDDebtorNumber(this, this.Transaction, clientID, debtorNumber, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID)
		{
			return FetchUsingUCAddressID( addressID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((DebtorDAO)CreateDAOInstance()).FetchDebtorUsingUCAddressID(this, this.Transaction, addressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorID)
		{
			return FetchUsingPK(debtorID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(debtorID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(debtorID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 debtorID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(debtorID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DebtorID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DebtorRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AccountEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAccountEntries(forceFetch, _accountEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAccountEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection GetMultiAccountEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAccountEntries || forceFetch || _alwaysFetchAccountEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_accountEntries);
				_accountEntries.SuppressClearInGetMulti=!forceFetch;
				_accountEntries.EntityFactoryToUse = entityFactoryToUse;
				_accountEntries.GetMultiManyToOne(null, this, null, filter);
				_accountEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedAccountEntries = true;
			}
			return _accountEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'AccountEntries'. These settings will be taken into account
		/// when the property AccountEntries is requested or GetMultiAccountEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAccountEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_accountEntries.SortClauses=sortClauses;
			_accountEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResults || forceFetch || _alwaysFetchClearingResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResults);
				_clearingResults.SuppressClearInGetMulti=!forceFetch;
				_clearingResults.EntityFactoryToUse = entityFactoryToUse;
				_clearingResults.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_clearingResults.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResults = true;
			}
			return _clearingResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResults'. These settings will be taken into account
		/// when the property ClearingResults is requested or GetMultiClearingResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResults.SortClauses=sortClauses;
			_clearingResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByMaintenanceStaff(bool forceFetch)
		{
			return GetMultiComponentClearingsByMaintenanceStaff(forceFetch, _componentClearingsByMaintenanceStaff.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByMaintenanceStaff(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentClearingsByMaintenanceStaff(forceFetch, _componentClearingsByMaintenanceStaff.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByMaintenanceStaff(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentClearingsByMaintenanceStaff(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByMaintenanceStaff(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentClearingsByMaintenanceStaff || forceFetch || _alwaysFetchComponentClearingsByMaintenanceStaff) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentClearingsByMaintenanceStaff);
				_componentClearingsByMaintenanceStaff.SuppressClearInGetMulti=!forceFetch;
				_componentClearingsByMaintenanceStaff.EntityFactoryToUse = entityFactoryToUse;
				_componentClearingsByMaintenanceStaff.GetMultiManyToOne(null, null, this, null, filter);
				_componentClearingsByMaintenanceStaff.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentClearingsByMaintenanceStaff = true;
			}
			return _componentClearingsByMaintenanceStaff;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentClearingsByMaintenanceStaff'. These settings will be taken into account
		/// when the property ComponentClearingsByMaintenanceStaff is requested or GetMultiComponentClearingsByMaintenanceStaff is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentClearingsByMaintenanceStaff(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentClearingsByMaintenanceStaff.SortClauses=sortClauses;
			_componentClearingsByMaintenanceStaff.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByPersonnelClearing(bool forceFetch)
		{
			return GetMultiComponentClearingsByPersonnelClearing(forceFetch, _componentClearingsByPersonnelClearing.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentClearingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByPersonnelClearing(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentClearingsByPersonnelClearing(forceFetch, _componentClearingsByPersonnelClearing.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByPersonnelClearing(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentClearingsByPersonnelClearing(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection GetMultiComponentClearingsByPersonnelClearing(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentClearingsByPersonnelClearing || forceFetch || _alwaysFetchComponentClearingsByPersonnelClearing) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentClearingsByPersonnelClearing);
				_componentClearingsByPersonnelClearing.SuppressClearInGetMulti=!forceFetch;
				_componentClearingsByPersonnelClearing.EntityFactoryToUse = entityFactoryToUse;
				_componentClearingsByPersonnelClearing.GetMultiManyToOne(null, null, null, this, filter);
				_componentClearingsByPersonnelClearing.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentClearingsByPersonnelClearing = true;
			}
			return _componentClearingsByPersonnelClearing;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentClearingsByPersonnelClearing'. These settings will be taken into account
		/// when the property ComponentClearingsByPersonnelClearing is requested or GetMultiComponentClearingsByPersonnelClearing is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentClearingsByPersonnelClearing(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentClearingsByPersonnelClearing.SortClauses=sortClauses;
			_componentClearingsByPersonnelClearing.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByMaintenanceStaff(bool forceFetch)
		{
			return GetMultiComponentFillingsByMaintenanceStaff(forceFetch, _componentFillingsByMaintenanceStaff.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByMaintenanceStaff(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentFillingsByMaintenanceStaff(forceFetch, _componentFillingsByMaintenanceStaff.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByMaintenanceStaff(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentFillingsByMaintenanceStaff(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByMaintenanceStaff(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentFillingsByMaintenanceStaff || forceFetch || _alwaysFetchComponentFillingsByMaintenanceStaff) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentFillingsByMaintenanceStaff);
				_componentFillingsByMaintenanceStaff.SuppressClearInGetMulti=!forceFetch;
				_componentFillingsByMaintenanceStaff.EntityFactoryToUse = entityFactoryToUse;
				_componentFillingsByMaintenanceStaff.GetMultiManyToOne(null, null, null, this, null, filter);
				_componentFillingsByMaintenanceStaff.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentFillingsByMaintenanceStaff = true;
			}
			return _componentFillingsByMaintenanceStaff;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentFillingsByMaintenanceStaff'. These settings will be taken into account
		/// when the property ComponentFillingsByMaintenanceStaff is requested or GetMultiComponentFillingsByMaintenanceStaff is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentFillingsByMaintenanceStaff(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentFillingsByMaintenanceStaff.SortClauses=sortClauses;
			_componentFillingsByMaintenanceStaff.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByPersonnelFilling(bool forceFetch)
		{
			return GetMultiComponentFillingsByPersonnelFilling(forceFetch, _componentFillingsByPersonnelFilling.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ComponentFillingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByPersonnelFilling(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiComponentFillingsByPersonnelFilling(forceFetch, _componentFillingsByPersonnelFilling.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByPersonnelFilling(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiComponentFillingsByPersonnelFilling(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection GetMultiComponentFillingsByPersonnelFilling(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedComponentFillingsByPersonnelFilling || forceFetch || _alwaysFetchComponentFillingsByPersonnelFilling) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_componentFillingsByPersonnelFilling);
				_componentFillingsByPersonnelFilling.SuppressClearInGetMulti=!forceFetch;
				_componentFillingsByPersonnelFilling.EntityFactoryToUse = entityFactoryToUse;
				_componentFillingsByPersonnelFilling.GetMultiManyToOne(null, null, null, null, this, filter);
				_componentFillingsByPersonnelFilling.SuppressClearInGetMulti=false;
				_alreadyFetchedComponentFillingsByPersonnelFilling = true;
			}
			return _componentFillingsByPersonnelFilling;
		}

		/// <summary> Sets the collection parameters for the collection for 'ComponentFillingsByPersonnelFilling'. These settings will be taken into account
		/// when the property ComponentFillingsByPersonnelFilling is requested or GetMultiComponentFillingsByPersonnelFilling is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersComponentFillingsByPersonnelFilling(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_componentFillingsByPersonnelFilling.SortClauses=sortClauses;
			_componentFillingsByPersonnelFilling.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'QualificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'QualificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.QualificationCollection GetMultiQualifications(bool forceFetch)
		{
			return GetMultiQualifications(forceFetch, _qualifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'QualificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'QualificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.QualificationCollection GetMultiQualifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiQualifications(forceFetch, _qualifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'QualificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.QualificationCollection GetMultiQualifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiQualifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'QualificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.QualificationCollection GetMultiQualifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedQualifications || forceFetch || _alwaysFetchQualifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_qualifications);
				_qualifications.SuppressClearInGetMulti=!forceFetch;
				_qualifications.EntityFactoryToUse = entityFactoryToUse;
				_qualifications.GetMultiManyToOne(this, filter);
				_qualifications.SuppressClearInGetMulti=false;
				_alreadyFetchedQualifications = true;
			}
			return _qualifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'Qualifications'. These settings will be taken into account
		/// when the property Qualifications is requested or GetMultiQualifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersQualifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_qualifications.SortClauses=sortClauses;
			_qualifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch)
		{
			return GetMultiUsers(forceFetch, _users.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUsers(forceFetch, _users.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUsers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUsers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUsers || forceFetch || _alwaysFetchUsers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_users);
				_users.SuppressClearInGetMulti=!forceFetch;
				_users.EntityFactoryToUse = entityFactoryToUse;
				_users.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_users.SuppressClearInGetMulti=false;
				_alreadyFetchedUsers = true;
			}
			return _users;
		}

		/// <summary> Sets the collection parameters for the collection for 'Users'. These settings will be taken into account
		/// when the property Users is requested or GetMultiUsers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUsers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_users.SortClauses=sortClauses;
			_users.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidents || forceFetch || _alwaysFetchFareEvasionIncidents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidents);
				_fareEvasionIncidents.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidents.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidents.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_fareEvasionIncidents.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidents = true;
			}
			return _fareEvasionIncidents;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidents'. These settings will be taken into account
		/// when the property FareEvasionIncidents is requested or GetMultiFareEvasionIncidents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidents.SortClauses=sortClauses;
			_fareEvasionIncidents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public DepotEntity GetSingleDepot()
		{
			return GetSingleDepot(false);
		}

		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public virtual DepotEntity GetSingleDepot(bool forceFetch)
		{
			if( ( !_alreadyFetchedDepot || forceFetch || _alwaysFetchDepot) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DepotEntityUsingDepotID);
				DepotEntity newEntity = new DepotEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DepotID);
				}
				if(fetchResult)
				{
					newEntity = (DepotEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_depotReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Depot = newEntity;
				_alreadyFetchedDepot = fetchResult;
			}
			return _depot;
		}

		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public VarioAddressEntity GetSingleAddress()
		{
			return GetSingleAddress(false);
		}
		
		/// <summary> Retrieves the related entity of type 'VarioAddressEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VarioAddressEntity' which is related to this entity.</returns>
		public virtual VarioAddressEntity GetSingleAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddress || forceFetch || _alwaysFetchAddress) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VarioAddressEntityUsingAddressID);
				VarioAddressEntity newEntity = new VarioAddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VarioAddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Address = newEntity;
				_alreadyFetchedAddress = fetchResult;
			}
			return _address;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Depot", _depot);
			toReturn.Add("AccountEntries", _accountEntries);
			toReturn.Add("ClearingResults", _clearingResults);
			toReturn.Add("ComponentClearingsByMaintenanceStaff", _componentClearingsByMaintenanceStaff);
			toReturn.Add("ComponentClearingsByPersonnelClearing", _componentClearingsByPersonnelClearing);
			toReturn.Add("ComponentFillingsByMaintenanceStaff", _componentFillingsByMaintenanceStaff);
			toReturn.Add("ComponentFillingsByPersonnelFilling", _componentFillingsByPersonnelFilling);
			toReturn.Add("Qualifications", _qualifications);
			toReturn.Add("Users", _users);
			toReturn.Add("FareEvasionIncidents", _fareEvasionIncidents);
			toReturn.Add("Address", _address);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="validator">The validator object for this DebtorEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 debtorID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(debtorID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_accountEntries = new VarioSL.Entities.CollectionClasses.AccountEntryCollection();
			_accountEntries.SetContainingEntityInfo(this, "Debtor");

			_clearingResults = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResults.SetContainingEntityInfo(this, "Debtor");

			_componentClearingsByMaintenanceStaff = new VarioSL.Entities.CollectionClasses.ComponentClearingCollection();
			_componentClearingsByMaintenanceStaff.SetContainingEntityInfo(this, "MaintenanceStaff");

			_componentClearingsByPersonnelClearing = new VarioSL.Entities.CollectionClasses.ComponentClearingCollection();
			_componentClearingsByPersonnelClearing.SetContainingEntityInfo(this, "PersonnelClearing");

			_componentFillingsByMaintenanceStaff = new VarioSL.Entities.CollectionClasses.ComponentFillingCollection();
			_componentFillingsByMaintenanceStaff.SetContainingEntityInfo(this, "MaintenanceStaff");

			_componentFillingsByPersonnelFilling = new VarioSL.Entities.CollectionClasses.ComponentFillingCollection();
			_componentFillingsByPersonnelFilling.SetContainingEntityInfo(this, "PersonnelFilling");

			_qualifications = new VarioSL.Entities.CollectionClasses.QualificationCollection();
			_qualifications.SetContainingEntityInfo(this, "Debtor");

			_users = new VarioSL.Entities.CollectionClasses.UserListCollection();
			_users.SetContainingEntityInfo(this, "Debtor");

			_fareEvasionIncidents = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidents.SetContainingEntityInfo(this, "Debtor");
			_clientReturnsNewIfNotFound = false;
			_depotReturnsNewIfNotFound = false;
			_addressReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BalancingDebitor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Changestock", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditorAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotLastLogon", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExitDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalDebtorNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumberType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTraining", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastBalanceDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lockernumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaintenanceLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxDaysSinceLastClearance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinAllowedAccountBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganisationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Pin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PreferredLanguage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Salutation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StaffIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleLastLogon", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticDebtorRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Debtors", resetFKFields, new int[] { (int)DebtorFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticDebtorRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _depot</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDepot(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticDebtorRelations.DepotEntityUsingDepotIDStatic, true, signalRelatedEntity, "Debtors", resetFKFields, new int[] { (int)DebtorFieldIndex.DepotID } );		
			_depot = null;
		}
		
		/// <summary> setups the sync logic for member _depot</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDepot(IEntityCore relatedEntity)
		{
			if(_depot!=relatedEntity)
			{		
				DesetupSyncDepot(true, true);
				_depot = (DepotEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticDebtorRelations.DepotEntityUsingDepotIDStatic, true, ref _alreadyFetchedDepot, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDepotPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _address</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticDebtorRelations.VarioAddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "Debtor", resetFKFields, new int[] { (int)DebtorFieldIndex.AddressID } );
			_address = null;
		}
	
		/// <summary> setups the sync logic for member _address</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddress(IEntityCore relatedEntity)
		{
			if(_address!=relatedEntity)
			{
				DesetupSyncAddress(true, true);
				_address = (VarioAddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticDebtorRelations.VarioAddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedAddress, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="debtorID">PK value for Debtor which data should be fetched into this Debtor object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 debtorID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DebtorFieldIndex.DebtorID].ForcedCurrentValueWrite(debtorID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDebtorDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DebtorEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DebtorRelations Relations
		{
			get	{ return new DebtorRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountEntryCollection(), (IEntityRelation)GetRelationsForField("AccountEntries")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.AccountEntryEntity, 0, null, null, null, "AccountEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResults")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentClearing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentClearingsByMaintenanceStaff
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentClearingCollection(), (IEntityRelation)GetRelationsForField("ComponentClearingsByMaintenanceStaff")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ComponentClearingEntity, 0, null, null, null, "ComponentClearingsByMaintenanceStaff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentClearing' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentClearingsByPersonnelClearing
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentClearingCollection(), (IEntityRelation)GetRelationsForField("ComponentClearingsByPersonnelClearing")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ComponentClearingEntity, 0, null, null, null, "ComponentClearingsByPersonnelClearing", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentFilling' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentFillingsByMaintenanceStaff
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentFillingCollection(), (IEntityRelation)GetRelationsForField("ComponentFillingsByMaintenanceStaff")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ComponentFillingEntity, 0, null, null, null, "ComponentFillingsByMaintenanceStaff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ComponentFilling' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponentFillingsByPersonnelFilling
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentFillingCollection(), (IEntityRelation)GetRelationsForField("ComponentFillingsByPersonnelFilling")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ComponentFillingEntity, 0, null, null, null, "ComponentFillingsByPersonnelFilling", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Qualification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathQualifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.QualificationCollection(), (IEntityRelation)GetRelationsForField("Qualifications")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.QualificationEntity, 0, null, null, null, "Qualifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUsers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("Users")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "Users", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidents")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDepot
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("Depot")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "Depot", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioAddress'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioAddressCollection(), (IEntityRelation)GetRelationsForField("Address")[0], (int)VarioSL.Entities.EntityType.DebtorEntity, (int)VarioSL.Entities.EntityType.VarioAddressEntity, 0, null, null, null, "Address", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountBalance property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."ACCOUNTBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AccountBalance
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.AccountBalance, true); }
			set	{ SetValue((int)DebtorFieldIndex.AccountBalance, value, true); }
		}

		/// <summary> The AddressID property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.AddressID, false); }
			set	{ SetValue((int)DebtorFieldIndex.AddressID, value, true); }
		}

		/// <summary> The BalancingDebitor property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."BALANCINGDEBITOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BalancingDebitor
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.BalancingDebitor, false); }
			set	{ SetValue((int)DebtorFieldIndex.BalancingDebitor, value, true); }
		}

		/// <summary> The Changestock property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."CHANGESTOCK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Changestock
		{
			get { return (System.Int64)GetValue((int)DebtorFieldIndex.Changestock, true); }
			set	{ SetValue((int)DebtorFieldIndex.Changestock, value, true); }
		}

		/// <summary> The ClientID property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)DebtorFieldIndex.ClientID, true); }
			set	{ SetValue((int)DebtorFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractorID property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."CONTRACTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.ContractorID, false); }
			set	{ SetValue((int)DebtorFieldIndex.ContractorID, value, true); }
		}

		/// <summary> The CreditorAccountNumber property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."CREDITORACCOUNTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreditorAccountNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.CreditorAccountNumber, false); }
			set	{ SetValue((int)DebtorFieldIndex.CreditorAccountNumber, value, true); }
		}

		/// <summary> The DebitBalance property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEBITBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DebitBalance
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.DebitBalance, true); }
			set	{ SetValue((int)DebtorFieldIndex.DebitBalance, value, true); }
		}

		/// <summary> The DebtorAccountNumber property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEBTORACCOUNTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorAccountNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.DebtorAccountNumber, false); }
			set	{ SetValue((int)DebtorFieldIndex.DebtorAccountNumber, value, true); }
		}

		/// <summary> The DebtorID property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DebtorID
		{
			get { return (System.Int64)GetValue((int)DebtorFieldIndex.DebtorID, true); }
			set	{ SetValue((int)DebtorFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The DebtorNumber property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEBTORNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DebtorNumber
		{
			get { return (System.Int64)GetValue((int)DebtorFieldIndex.DebtorNumber, true); }
			set	{ SetValue((int)DebtorFieldIndex.DebtorNumber, value, true); }
		}

		/// <summary> The DepotID property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DepotID
		{
			get { return (System.Int64)GetValue((int)DebtorFieldIndex.DepotID, true); }
			set	{ SetValue((int)DebtorFieldIndex.DepotID, value, true); }
		}

		/// <summary> The DepotLastLogon property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DEPOTLASTLOGON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DepotLastLogon
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorFieldIndex.DepotLastLogon, false); }
			set	{ SetValue((int)DebtorFieldIndex.DepotLastLogon, value, true); }
		}

		/// <summary> The Description property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.Description, true); }
			set	{ SetValue((int)DebtorFieldIndex.Description, value, true); }
		}

		/// <summary> The ExitDate property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."EXITDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ExitDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorFieldIndex.ExitDate, false); }
			set	{ SetValue((int)DebtorFieldIndex.ExitDate, value, true); }
		}

		/// <summary> The ExternalDebtorNumber property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."EXTERNALDEBTORNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalDebtorNumber
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.ExternalDebtorNumber, true); }
			set	{ SetValue((int)DebtorFieldIndex.ExternalDebtorNumber, value, true); }
		}

		/// <summary> The ExternalNumberType property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."EXTERNALNUMBERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExternalNumberType
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.ExternalNumberType, false); }
			set	{ SetValue((int)DebtorFieldIndex.ExternalNumberType, value, true); }
		}

		/// <summary> The FirstName property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."FIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.FirstName, true); }
			set	{ SetValue((int)DebtorFieldIndex.FirstName, value, true); }
		}

		/// <summary> The IsActive property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."ACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)DebtorFieldIndex.IsActive, true); }
			set	{ SetValue((int)DebtorFieldIndex.IsActive, value, true); }
		}

		/// <summary> The IsTraining property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."TRAININGLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTraining
		{
			get { return (System.Boolean)GetValue((int)DebtorFieldIndex.IsTraining, true); }
			set	{ SetValue((int)DebtorFieldIndex.IsTraining, value, true); }
		}

		/// <summary> The LastBalanceDate property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."LASTBALANCEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastBalanceDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorFieldIndex.LastBalanceDate, false); }
			set	{ SetValue((int)DebtorFieldIndex.LastBalanceDate, value, true); }
		}

		/// <summary> The LastName property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastName
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.LastName, true); }
			set	{ SetValue((int)DebtorFieldIndex.LastName, value, true); }
		}

		/// <summary> The Lockernumber property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."LOCKERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Lockernumber
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.Lockernumber, true); }
			set	{ SetValue((int)DebtorFieldIndex.Lockernumber, value, true); }
		}

		/// <summary> The MaintenanceLevel property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."MAINTENANCELEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MaintenanceLevel
		{
			get { return (Nullable<System.Int32>)GetValue((int)DebtorFieldIndex.MaintenanceLevel, false); }
			set	{ SetValue((int)DebtorFieldIndex.MaintenanceLevel, value, true); }
		}

		/// <summary> The MaxDaysSinceLastClearance property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."MAXDAYSSINCELASTCLEARANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MaxDaysSinceLastClearance
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.MaxDaysSinceLastClearance, true); }
			set	{ SetValue((int)DebtorFieldIndex.MaxDaysSinceLastClearance, value, true); }
		}

		/// <summary> The MinAllowedAccountBalance property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."MINALLOWEDACCOUNTBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MinAllowedAccountBalance
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.MinAllowedAccountBalance, true); }
			set	{ SetValue((int)DebtorFieldIndex.MinAllowedAccountBalance, value, true); }
		}

		/// <summary> The OrganisationCode property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."ORGANISATIONCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrganisationCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)DebtorFieldIndex.OrganisationCode, false); }
			set	{ SetValue((int)DebtorFieldIndex.OrganisationCode, value, true); }
		}

		/// <summary> The Pin property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."PERSONALCODENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Pin
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.Pin, true); }
			set	{ SetValue((int)DebtorFieldIndex.Pin, value, true); }
		}

		/// <summary> The PreferredLanguage property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."PREFERREDLANGUAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PreferredLanguage
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.PreferredLanguage, true); }
			set	{ SetValue((int)DebtorFieldIndex.PreferredLanguage, value, true); }
		}

		/// <summary> The Salutation property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."TITLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Salutation
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.Salutation, true); }
			set	{ SetValue((int)DebtorFieldIndex.Salutation, value, true); }
		}

		/// <summary> The ServiceLevel property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."SERVICELEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ServiceLevel
		{
			get { return (System.Int32)GetValue((int)DebtorFieldIndex.ServiceLevel, true); }
			set	{ SetValue((int)DebtorFieldIndex.ServiceLevel, value, true); }
		}

		/// <summary> The StaffIdentifier property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."STAFFIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StaffIdentifier
		{
			get { return (System.String)GetValue((int)DebtorFieldIndex.StaffIdentifier, true); }
			set	{ SetValue((int)DebtorFieldIndex.StaffIdentifier, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorFieldIndex.ValidTo, false); }
			set	{ SetValue((int)DebtorFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The VehicleLastLogon property of the Entity Debtor<br/><br/></summary>
		/// <remarks>Mapped on  table field: "DM_DEBTOR"."VEHICLELASTLOGON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> VehicleLastLogon
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DebtorFieldIndex.VehicleLastLogon, false); }
			set	{ SetValue((int)DebtorFieldIndex.VehicleLastLogon, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AccountEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAccountEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AccountEntryCollection AccountEntries
		{
			get	{ return GetMultiAccountEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AccountEntries. When set to true, AccountEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountEntries is accessed. You can always execute/ a forced fetch by calling GetMultiAccountEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountEntries
		{
			get	{ return _alwaysFetchAccountEntries; }
			set	{ _alwaysFetchAccountEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountEntries already has been fetched. Setting this property to false when AccountEntries has been fetched
		/// will clear the AccountEntries collection well. Setting this property to true while AccountEntries hasn't been fetched disables lazy loading for AccountEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountEntries
		{
			get { return _alreadyFetchedAccountEntries;}
			set 
			{
				if(_alreadyFetchedAccountEntries && !value && (_accountEntries != null))
				{
					_accountEntries.Clear();
				}
				_alreadyFetchedAccountEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResults
		{
			get	{ return GetMultiClearingResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResults. When set to true, ClearingResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResults is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResults
		{
			get	{ return _alwaysFetchClearingResults; }
			set	{ _alwaysFetchClearingResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResults already has been fetched. Setting this property to false when ClearingResults has been fetched
		/// will clear the ClearingResults collection well. Setting this property to true while ClearingResults hasn't been fetched disables lazy loading for ClearingResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResults
		{
			get { return _alreadyFetchedClearingResults;}
			set 
			{
				if(_alreadyFetchedClearingResults && !value && (_clearingResults != null))
				{
					_clearingResults.Clear();
				}
				_alreadyFetchedClearingResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentClearingsByMaintenanceStaff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection ComponentClearingsByMaintenanceStaff
		{
			get	{ return GetMultiComponentClearingsByMaintenanceStaff(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentClearingsByMaintenanceStaff. When set to true, ComponentClearingsByMaintenanceStaff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentClearingsByMaintenanceStaff is accessed. You can always execute/ a forced fetch by calling GetMultiComponentClearingsByMaintenanceStaff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentClearingsByMaintenanceStaff
		{
			get	{ return _alwaysFetchComponentClearingsByMaintenanceStaff; }
			set	{ _alwaysFetchComponentClearingsByMaintenanceStaff = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentClearingsByMaintenanceStaff already has been fetched. Setting this property to false when ComponentClearingsByMaintenanceStaff has been fetched
		/// will clear the ComponentClearingsByMaintenanceStaff collection well. Setting this property to true while ComponentClearingsByMaintenanceStaff hasn't been fetched disables lazy loading for ComponentClearingsByMaintenanceStaff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentClearingsByMaintenanceStaff
		{
			get { return _alreadyFetchedComponentClearingsByMaintenanceStaff;}
			set 
			{
				if(_alreadyFetchedComponentClearingsByMaintenanceStaff && !value && (_componentClearingsByMaintenanceStaff != null))
				{
					_componentClearingsByMaintenanceStaff.Clear();
				}
				_alreadyFetchedComponentClearingsByMaintenanceStaff = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentClearingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentClearingsByPersonnelClearing()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentClearingCollection ComponentClearingsByPersonnelClearing
		{
			get	{ return GetMultiComponentClearingsByPersonnelClearing(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentClearingsByPersonnelClearing. When set to true, ComponentClearingsByPersonnelClearing is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentClearingsByPersonnelClearing is accessed. You can always execute/ a forced fetch by calling GetMultiComponentClearingsByPersonnelClearing(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentClearingsByPersonnelClearing
		{
			get	{ return _alwaysFetchComponentClearingsByPersonnelClearing; }
			set	{ _alwaysFetchComponentClearingsByPersonnelClearing = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentClearingsByPersonnelClearing already has been fetched. Setting this property to false when ComponentClearingsByPersonnelClearing has been fetched
		/// will clear the ComponentClearingsByPersonnelClearing collection well. Setting this property to true while ComponentClearingsByPersonnelClearing hasn't been fetched disables lazy loading for ComponentClearingsByPersonnelClearing</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentClearingsByPersonnelClearing
		{
			get { return _alreadyFetchedComponentClearingsByPersonnelClearing;}
			set 
			{
				if(_alreadyFetchedComponentClearingsByPersonnelClearing && !value && (_componentClearingsByPersonnelClearing != null))
				{
					_componentClearingsByPersonnelClearing.Clear();
				}
				_alreadyFetchedComponentClearingsByPersonnelClearing = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentFillingsByMaintenanceStaff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection ComponentFillingsByMaintenanceStaff
		{
			get	{ return GetMultiComponentFillingsByMaintenanceStaff(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentFillingsByMaintenanceStaff. When set to true, ComponentFillingsByMaintenanceStaff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentFillingsByMaintenanceStaff is accessed. You can always execute/ a forced fetch by calling GetMultiComponentFillingsByMaintenanceStaff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentFillingsByMaintenanceStaff
		{
			get	{ return _alwaysFetchComponentFillingsByMaintenanceStaff; }
			set	{ _alwaysFetchComponentFillingsByMaintenanceStaff = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentFillingsByMaintenanceStaff already has been fetched. Setting this property to false when ComponentFillingsByMaintenanceStaff has been fetched
		/// will clear the ComponentFillingsByMaintenanceStaff collection well. Setting this property to true while ComponentFillingsByMaintenanceStaff hasn't been fetched disables lazy loading for ComponentFillingsByMaintenanceStaff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentFillingsByMaintenanceStaff
		{
			get { return _alreadyFetchedComponentFillingsByMaintenanceStaff;}
			set 
			{
				if(_alreadyFetchedComponentFillingsByMaintenanceStaff && !value && (_componentFillingsByMaintenanceStaff != null))
				{
					_componentFillingsByMaintenanceStaff.Clear();
				}
				_alreadyFetchedComponentFillingsByMaintenanceStaff = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ComponentFillingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiComponentFillingsByPersonnelFilling()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ComponentFillingCollection ComponentFillingsByPersonnelFilling
		{
			get	{ return GetMultiComponentFillingsByPersonnelFilling(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ComponentFillingsByPersonnelFilling. When set to true, ComponentFillingsByPersonnelFilling is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ComponentFillingsByPersonnelFilling is accessed. You can always execute/ a forced fetch by calling GetMultiComponentFillingsByPersonnelFilling(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponentFillingsByPersonnelFilling
		{
			get	{ return _alwaysFetchComponentFillingsByPersonnelFilling; }
			set	{ _alwaysFetchComponentFillingsByPersonnelFilling = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ComponentFillingsByPersonnelFilling already has been fetched. Setting this property to false when ComponentFillingsByPersonnelFilling has been fetched
		/// will clear the ComponentFillingsByPersonnelFilling collection well. Setting this property to true while ComponentFillingsByPersonnelFilling hasn't been fetched disables lazy loading for ComponentFillingsByPersonnelFilling</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponentFillingsByPersonnelFilling
		{
			get { return _alreadyFetchedComponentFillingsByPersonnelFilling;}
			set 
			{
				if(_alreadyFetchedComponentFillingsByPersonnelFilling && !value && (_componentFillingsByPersonnelFilling != null))
				{
					_componentFillingsByPersonnelFilling.Clear();
				}
				_alreadyFetchedComponentFillingsByPersonnelFilling = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'QualificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiQualifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.QualificationCollection Qualifications
		{
			get	{ return GetMultiQualifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Qualifications. When set to true, Qualifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Qualifications is accessed. You can always execute/ a forced fetch by calling GetMultiQualifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchQualifications
		{
			get	{ return _alwaysFetchQualifications; }
			set	{ _alwaysFetchQualifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Qualifications already has been fetched. Setting this property to false when Qualifications has been fetched
		/// will clear the Qualifications collection well. Setting this property to true while Qualifications hasn't been fetched disables lazy loading for Qualifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedQualifications
		{
			get { return _alreadyFetchedQualifications;}
			set 
			{
				if(_alreadyFetchedQualifications && !value && (_qualifications != null))
				{
					_qualifications.Clear();
				}
				_alreadyFetchedQualifications = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUsers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection Users
		{
			get	{ return GetMultiUsers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Users. When set to true, Users is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Users is accessed. You can always execute/ a forced fetch by calling GetMultiUsers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUsers
		{
			get	{ return _alwaysFetchUsers; }
			set	{ _alwaysFetchUsers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Users already has been fetched. Setting this property to false when Users has been fetched
		/// will clear the Users collection well. Setting this property to true while Users hasn't been fetched disables lazy loading for Users</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUsers
		{
			get { return _alreadyFetchedUsers;}
			set 
			{
				if(_alreadyFetchedUsers && !value && (_users != null))
				{
					_users.Clear();
				}
				_alreadyFetchedUsers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidents
		{
			get	{ return GetMultiFareEvasionIncidents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidents. When set to true, FareEvasionIncidents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidents is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidents
		{
			get	{ return _alwaysFetchFareEvasionIncidents; }
			set	{ _alwaysFetchFareEvasionIncidents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidents already has been fetched. Setting this property to false when FareEvasionIncidents has been fetched
		/// will clear the FareEvasionIncidents collection well. Setting this property to true while FareEvasionIncidents hasn't been fetched disables lazy loading for FareEvasionIncidents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidents
		{
			get { return _alreadyFetchedFareEvasionIncidents;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidents && !value && (_fareEvasionIncidents != null))
				{
					_fareEvasionIncidents.Clear();
				}
				_alreadyFetchedFareEvasionIncidents = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Debtors", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DepotEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDepot()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DepotEntity Depot
		{
			get	{ return GetSingleDepot(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDepot(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Debtors", "Depot", _depot, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Depot. When set to true, Depot is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Depot is accessed. You can always execute a forced fetch by calling GetSingleDepot(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDepot
		{
			get	{ return _alwaysFetchDepot; }
			set	{ _alwaysFetchDepot = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Depot already has been fetched. Setting this property to false when Depot has been fetched
		/// will set Depot to null as well. Setting this property to true while Depot hasn't been fetched disables lazy loading for Depot</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDepot
		{
			get { return _alreadyFetchedDepot;}
			set 
			{
				if(_alreadyFetchedDepot && !value)
				{
					this.Depot = null;
				}
				_alreadyFetchedDepot = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Depot is not found
		/// in the database. When set to true, Depot will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DepotReturnsNewIfNotFound
		{
			get	{ return _depotReturnsNewIfNotFound; }
			set { _depotReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VarioAddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VarioAddressEntity Address
		{
			get	{ return GetSingleAddress(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAddress(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_address !=null);
						DesetupSyncAddress(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Address");
						}
					}
					else
					{
						if(_address!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Debtor");
							SetupSyncAddress(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Address. When set to true, Address is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Address is accessed. You can always execute a forced fetch by calling GetSingleAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddress
		{
			get	{ return _alwaysFetchAddress; }
			set	{ _alwaysFetchAddress = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Address already has been fetched. Setting this property to false when Address has been fetched
		/// will set Address to null as well. Setting this property to true while Address hasn't been fetched disables lazy loading for Address</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddress
		{
			get { return _alreadyFetchedAddress;}
			set 
			{
				if(_alreadyFetchedAddress && !value)
				{
					this.Address = null;
				}
				_alreadyFetchedAddress = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Address is not found
		/// in the database. When set to true, Address will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AddressReturnsNewIfNotFound
		{
			get	{ return _addressReturnsNewIfNotFound; }
			set	{ _addressReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DebtorEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
