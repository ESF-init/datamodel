﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'LineGroup'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class LineGroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection	_masterLineGroupId;
		private bool	_alwaysFetchMasterLineGroupId, _alreadyFetchedMasterLineGroupId;
		private VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection	_subLineGroupIds;
		private bool	_alwaysFetchSubLineGroupIds, _alreadyFetchedSubLineGroupIds;
		private VarioSL.Entities.CollectionClasses.LineToLineGroupCollection	_lineToLineGroup;
		private bool	_alwaysFetchLineToLineGroup, _alreadyFetchedLineToLineGroup;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name MasterLineGroupId</summary>
			public static readonly string MasterLineGroupId = "MasterLineGroupId";
			/// <summary>Member name SubLineGroupIds</summary>
			public static readonly string SubLineGroupIds = "SubLineGroupIds";
			/// <summary>Member name LineToLineGroup</summary>
			public static readonly string LineToLineGroup = "LineToLineGroup";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LineGroupEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public LineGroupEntity() :base("LineGroupEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		public LineGroupEntity(System.Int64 lineGroupID):base("LineGroupEntity")
		{
			InitClassFetch(lineGroupID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LineGroupEntity(System.Int64 lineGroupID, IPrefetchPath prefetchPathToUse):base("LineGroupEntity")
		{
			InitClassFetch(lineGroupID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="validator">The custom validator object for this LineGroupEntity</param>
		public LineGroupEntity(System.Int64 lineGroupID, IValidator validator):base("LineGroupEntity")
		{
			InitClassFetch(lineGroupID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LineGroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_masterLineGroupId = (VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection)info.GetValue("_masterLineGroupId", typeof(VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection));
			_alwaysFetchMasterLineGroupId = info.GetBoolean("_alwaysFetchMasterLineGroupId");
			_alreadyFetchedMasterLineGroupId = info.GetBoolean("_alreadyFetchedMasterLineGroupId");

			_subLineGroupIds = (VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection)info.GetValue("_subLineGroupIds", typeof(VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection));
			_alwaysFetchSubLineGroupIds = info.GetBoolean("_alwaysFetchSubLineGroupIds");
			_alreadyFetchedSubLineGroupIds = info.GetBoolean("_alreadyFetchedSubLineGroupIds");

			_lineToLineGroup = (VarioSL.Entities.CollectionClasses.LineToLineGroupCollection)info.GetValue("_lineToLineGroup", typeof(VarioSL.Entities.CollectionClasses.LineToLineGroupCollection));
			_alwaysFetchLineToLineGroup = info.GetBoolean("_alwaysFetchLineToLineGroup");
			_alreadyFetchedLineToLineGroup = info.GetBoolean("_alreadyFetchedLineToLineGroup");

			_ticket = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticket", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LineGroupFieldIndex)fieldIndex)
			{
				case LineGroupFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMasterLineGroupId = (_masterLineGroupId.Count > 0);
			_alreadyFetchedSubLineGroupIds = (_subLineGroupIds.Count > 0);
			_alreadyFetchedLineToLineGroup = (_lineToLineGroup.Count > 0);
			_alreadyFetchedTicket = (_ticket.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "MasterLineGroupId":
					toReturn.Add(Relations.LineGroupToLineGroupEntityUsingLineGroupID);
					break;
				case "SubLineGroupIds":
					toReturn.Add(Relations.LineGroupToLineGroupEntityUsingMasterLineGroupID);
					break;
				case "LineToLineGroup":
					toReturn.Add(Relations.LineToLineGroupEntityUsingLineGroupID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingLineGroupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_masterLineGroupId", (!this.MarkedForDeletion?_masterLineGroupId:null));
			info.AddValue("_alwaysFetchMasterLineGroupId", _alwaysFetchMasterLineGroupId);
			info.AddValue("_alreadyFetchedMasterLineGroupId", _alreadyFetchedMasterLineGroupId);
			info.AddValue("_subLineGroupIds", (!this.MarkedForDeletion?_subLineGroupIds:null));
			info.AddValue("_alwaysFetchSubLineGroupIds", _alwaysFetchSubLineGroupIds);
			info.AddValue("_alreadyFetchedSubLineGroupIds", _alreadyFetchedSubLineGroupIds);
			info.AddValue("_lineToLineGroup", (!this.MarkedForDeletion?_lineToLineGroup:null));
			info.AddValue("_alwaysFetchLineToLineGroup", _alwaysFetchLineToLineGroup);
			info.AddValue("_alreadyFetchedLineToLineGroup", _alreadyFetchedLineToLineGroup);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "MasterLineGroupId":
					_alreadyFetchedMasterLineGroupId = true;
					if(entity!=null)
					{
						this.MasterLineGroupId.Add((LineGroupToLineGroupEntity)entity);
					}
					break;
				case "SubLineGroupIds":
					_alreadyFetchedSubLineGroupIds = true;
					if(entity!=null)
					{
						this.SubLineGroupIds.Add((LineGroupToLineGroupEntity)entity);
					}
					break;
				case "LineToLineGroup":
					_alreadyFetchedLineToLineGroup = true;
					if(entity!=null)
					{
						this.LineToLineGroup.Add((LineToLineGroupEntity)entity);
					}
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					if(entity!=null)
					{
						this.Ticket.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "MasterLineGroupId":
					_masterLineGroupId.Add((LineGroupToLineGroupEntity)relatedEntity);
					break;
				case "SubLineGroupIds":
					_subLineGroupIds.Add((LineGroupToLineGroupEntity)relatedEntity);
					break;
				case "LineToLineGroup":
					_lineToLineGroup.Add((LineToLineGroupEntity)relatedEntity);
					break;
				case "Ticket":
					_ticket.Add((TicketEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "MasterLineGroupId":
					this.PerformRelatedEntityRemoval(_masterLineGroupId, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SubLineGroupIds":
					this.PerformRelatedEntityRemoval(_subLineGroupIds, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "LineToLineGroup":
					this.PerformRelatedEntityRemoval(_lineToLineGroup, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Ticket":
					this.PerformRelatedEntityRemoval(_ticket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_masterLineGroupId);
			toReturn.Add(_subLineGroupIds);
			toReturn.Add(_lineToLineGroup);
			toReturn.Add(_ticket);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineGroupID)
		{
			return FetchUsingPK(lineGroupID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineGroupID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(lineGroupID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(lineGroupID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 lineGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(lineGroupID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LineGroupID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LineGroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiMasterLineGroupId(bool forceFetch)
		{
			return GetMultiMasterLineGroupId(forceFetch, _masterLineGroupId.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiMasterLineGroupId(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMasterLineGroupId(forceFetch, _masterLineGroupId.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiMasterLineGroupId(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMasterLineGroupId(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiMasterLineGroupId(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMasterLineGroupId || forceFetch || _alwaysFetchMasterLineGroupId) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_masterLineGroupId);
				_masterLineGroupId.SuppressClearInGetMulti=!forceFetch;
				_masterLineGroupId.EntityFactoryToUse = entityFactoryToUse;
				_masterLineGroupId.GetMultiManyToOne(this, null, filter);
				_masterLineGroupId.SuppressClearInGetMulti=false;
				_alreadyFetchedMasterLineGroupId = true;
			}
			return _masterLineGroupId;
		}

		/// <summary> Sets the collection parameters for the collection for 'MasterLineGroupId'. These settings will be taken into account
		/// when the property MasterLineGroupId is requested or GetMultiMasterLineGroupId is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMasterLineGroupId(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_masterLineGroupId.SortClauses=sortClauses;
			_masterLineGroupId.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiSubLineGroupIds(bool forceFetch)
		{
			return GetMultiSubLineGroupIds(forceFetch, _subLineGroupIds.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiSubLineGroupIds(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSubLineGroupIds(forceFetch, _subLineGroupIds.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiSubLineGroupIds(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSubLineGroupIds(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection GetMultiSubLineGroupIds(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSubLineGroupIds || forceFetch || _alwaysFetchSubLineGroupIds) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_subLineGroupIds);
				_subLineGroupIds.SuppressClearInGetMulti=!forceFetch;
				_subLineGroupIds.EntityFactoryToUse = entityFactoryToUse;
				_subLineGroupIds.GetMultiManyToOne(null, this, filter);
				_subLineGroupIds.SuppressClearInGetMulti=false;
				_alreadyFetchedSubLineGroupIds = true;
			}
			return _subLineGroupIds;
		}

		/// <summary> Sets the collection parameters for the collection for 'SubLineGroupIds'. These settings will be taken into account
		/// when the property SubLineGroupIds is requested or GetMultiSubLineGroupIds is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSubLineGroupIds(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_subLineGroupIds.SortClauses=sortClauses;
			_subLineGroupIds.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LineToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineToLineGroupCollection GetMultiLineToLineGroup(bool forceFetch)
		{
			return GetMultiLineToLineGroup(forceFetch, _lineToLineGroup.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LineToLineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineToLineGroupCollection GetMultiLineToLineGroup(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLineToLineGroup(forceFetch, _lineToLineGroup.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LineToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineToLineGroupCollection GetMultiLineToLineGroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLineToLineGroup(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineToLineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LineToLineGroupCollection GetMultiLineToLineGroup(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLineToLineGroup || forceFetch || _alwaysFetchLineToLineGroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_lineToLineGroup);
				_lineToLineGroup.SuppressClearInGetMulti=!forceFetch;
				_lineToLineGroup.EntityFactoryToUse = entityFactoryToUse;
				_lineToLineGroup.GetMultiManyToOne(this, filter);
				_lineToLineGroup.SuppressClearInGetMulti=false;
				_alreadyFetchedLineToLineGroup = true;
			}
			return _lineToLineGroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'LineToLineGroup'. These settings will be taken into account
		/// when the property LineToLineGroup is requested or GetMultiLineToLineGroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLineToLineGroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_lineToLineGroup.SortClauses=sortClauses;
			_lineToLineGroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicket(bool forceFetch)
		{
			return GetMultiTicket(forceFetch, _ticket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicket(forceFetch, _ticket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticket);
				_ticket.SuppressClearInGetMulti=!forceFetch;
				_ticket.EntityFactoryToUse = entityFactoryToUse;
				_ticket.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticket.SuppressClearInGetMulti=false;
				_alreadyFetchedTicket = true;
			}
			return _ticket;
		}

		/// <summary> Sets the collection parameters for the collection for 'Ticket'. These settings will be taken into account
		/// when the property Ticket is requested or GetMultiTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticket.SortClauses=sortClauses;
			_ticket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("MasterLineGroupId", _masterLineGroupId);
			toReturn.Add("SubLineGroupIds", _subLineGroupIds);
			toReturn.Add("LineToLineGroup", _lineToLineGroup);
			toReturn.Add("Ticket", _ticket);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="validator">The validator object for this LineGroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 lineGroupID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(lineGroupID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_masterLineGroupId = new VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection();
			_masterLineGroupId.SetContainingEntityInfo(this, "SubLineGroup");

			_subLineGroupIds = new VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection();
			_subLineGroupIds.SetContainingEntityInfo(this, "MasterLineGroup");

			_lineToLineGroup = new VarioSL.Entities.CollectionClasses.LineToLineGroupCollection();
			_lineToLineGroup.SetContainingEntityInfo(this, "LineGroup");

			_ticket = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticket.SetContainingEntityInfo(this, "LineGroup");
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLineGroupRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "LineGroups", resetFKFields, new int[] { (int)LineGroupFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLineGroupRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="lineGroupID">PK value for LineGroup which data should be fetched into this LineGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 lineGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LineGroupFieldIndex.LineGroupID].ForcedCurrentValueWrite(lineGroupID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLineGroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LineGroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LineGroupRelations Relations
		{
			get	{ return new LineGroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LineGroupToLineGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMasterLineGroupId
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection(), (IEntityRelation)GetRelationsForField("MasterLineGroupId")[0], (int)VarioSL.Entities.EntityType.LineGroupEntity, (int)VarioSL.Entities.EntityType.LineGroupToLineGroupEntity, 0, null, null, null, "MasterLineGroupId", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LineGroupToLineGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSubLineGroupIds
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection(), (IEntityRelation)GetRelationsForField("SubLineGroupIds")[0], (int)VarioSL.Entities.EntityType.LineGroupEntity, (int)VarioSL.Entities.EntityType.LineGroupToLineGroupEntity, 0, null, null, null, "SubLineGroupIds", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LineToLineGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLineToLineGroup
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineToLineGroupCollection(), (IEntityRelation)GetRelationsForField("LineToLineGroup")[0], (int)VarioSL.Entities.EntityType.LineGroupEntity, (int)VarioSL.Entities.EntityType.LineToLineGroupEntity, 0, null, null, null, "LineToLineGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.LineGroupEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.LineGroupEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LineGroupID property of the Entity LineGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LINEGROUP"."LINEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LineGroupID
		{
			get { return (System.Int64)GetValue((int)LineGroupFieldIndex.LineGroupID, true); }
			set	{ SetValue((int)LineGroupFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The LineGroupName property of the Entity LineGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LINEGROUP"."LINEGROUPNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LineGroupName
		{
			get { return (System.String)GetValue((int)LineGroupFieldIndex.LineGroupName, true); }
			set	{ SetValue((int)LineGroupFieldIndex.LineGroupName, value, true); }
		}

		/// <summary> The LineGroupType property of the Entity LineGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LINEGROUP"."LINEGROUPTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffLineGroupType LineGroupType
		{
			get { return (VarioSL.Entities.Enumerations.TariffLineGroupType)GetValue((int)LineGroupFieldIndex.LineGroupType, true); }
			set	{ SetValue((int)LineGroupFieldIndex.LineGroupType, value, true); }
		}

		/// <summary> The TariffID property of the Entity LineGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LINEGROUP"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)LineGroupFieldIndex.TariffID, true); }
			set	{ SetValue((int)LineGroupFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMasterLineGroupId()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection MasterLineGroupId
		{
			get	{ return GetMultiMasterLineGroupId(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MasterLineGroupId. When set to true, MasterLineGroupId is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MasterLineGroupId is accessed. You can always execute/ a forced fetch by calling GetMultiMasterLineGroupId(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMasterLineGroupId
		{
			get	{ return _alwaysFetchMasterLineGroupId; }
			set	{ _alwaysFetchMasterLineGroupId = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MasterLineGroupId already has been fetched. Setting this property to false when MasterLineGroupId has been fetched
		/// will clear the MasterLineGroupId collection well. Setting this property to true while MasterLineGroupId hasn't been fetched disables lazy loading for MasterLineGroupId</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMasterLineGroupId
		{
			get { return _alreadyFetchedMasterLineGroupId;}
			set 
			{
				if(_alreadyFetchedMasterLineGroupId && !value && (_masterLineGroupId != null))
				{
					_masterLineGroupId.Clear();
				}
				_alreadyFetchedMasterLineGroupId = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LineGroupToLineGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSubLineGroupIds()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupToLineGroupCollection SubLineGroupIds
		{
			get	{ return GetMultiSubLineGroupIds(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SubLineGroupIds. When set to true, SubLineGroupIds is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SubLineGroupIds is accessed. You can always execute/ a forced fetch by calling GetMultiSubLineGroupIds(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSubLineGroupIds
		{
			get	{ return _alwaysFetchSubLineGroupIds; }
			set	{ _alwaysFetchSubLineGroupIds = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SubLineGroupIds already has been fetched. Setting this property to false when SubLineGroupIds has been fetched
		/// will clear the SubLineGroupIds collection well. Setting this property to true while SubLineGroupIds hasn't been fetched disables lazy loading for SubLineGroupIds</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSubLineGroupIds
		{
			get { return _alreadyFetchedSubLineGroupIds;}
			set 
			{
				if(_alreadyFetchedSubLineGroupIds && !value && (_subLineGroupIds != null))
				{
					_subLineGroupIds.Clear();
				}
				_alreadyFetchedSubLineGroupIds = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LineToLineGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLineToLineGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineToLineGroupCollection LineToLineGroup
		{
			get	{ return GetMultiLineToLineGroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LineToLineGroup. When set to true, LineToLineGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LineToLineGroup is accessed. You can always execute/ a forced fetch by calling GetMultiLineToLineGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLineToLineGroup
		{
			get	{ return _alwaysFetchLineToLineGroup; }
			set	{ _alwaysFetchLineToLineGroup = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LineToLineGroup already has been fetched. Setting this property to false when LineToLineGroup has been fetched
		/// will clear the LineToLineGroup collection well. Setting this property to true while LineToLineGroup hasn't been fetched disables lazy loading for LineToLineGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLineToLineGroup
		{
			get { return _alreadyFetchedLineToLineGroup;}
			set 
			{
				if(_alreadyFetchedLineToLineGroup && !value && (_lineToLineGroup != null))
				{
					_lineToLineGroup.Clear();
				}
				_alreadyFetchedLineToLineGroup = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Ticket
		{
			get	{ return GetMultiTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute/ a forced fetch by calling GetMultiTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will clear the Ticket collection well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value && (_ticket != null))
				{
					_ticket.Clear();
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LineGroups", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.LineGroupEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
