﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TransactionJourneyHistory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TransactionJourneyHistoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TransactionJourneyHistoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionJourneyHistoryEntity() :base("TransactionJourneyHistoryEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionJourneyHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TransactionJourneyHistoryRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardReferenceTransactionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTransactionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStopName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromStopNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JourneyLegCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToStopName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToStopNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripSerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeName", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTransactionJourneyHistoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TransactionJourneyHistoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TransactionJourneyHistoryRelations Relations
		{
			get	{ return new TransactionJourneyHistoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardBalance property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."CARDBALANCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardBalance
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.CardBalance, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.CardBalance, value, true); }
		}

		/// <summary> The CardID property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."CARDID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)TransactionJourneyHistoryFieldIndex.CardID, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardReferenceTransactionNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."CARDREFTRANSACTIONNO"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardReferenceTransactionNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.CardReferenceTransactionNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.CardReferenceTransactionNumber, value, true); }
		}

		/// <summary> The CardTransactionNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."CARDTRANSACTIONNO"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardTransactionNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.CardTransactionNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.CardTransactionNumber, value, true); }
		}

		/// <summary> The FromStopName property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."FROMSTOPNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FromStopName
		{
			get { return (System.String)GetValue((int)TransactionJourneyHistoryFieldIndex.FromStopName, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.FromStopName, value, true); }
		}

		/// <summary> The FromStopNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."FROMSTOPNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromStopNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.FromStopNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.FromStopNumber, value, true); }
		}

		/// <summary> The JourneyLegCounter property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."JOURNEYLEGCOUNTER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> JourneyLegCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.JourneyLegCounter, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.JourneyLegCounter, value, true); }
		}

		/// <summary> The Price property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."PRICE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Price
		{
			get { return (Nullable<System.Decimal>)GetValue((int)TransactionJourneyHistoryFieldIndex.Price, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.Price, value, true); }
		}

		/// <summary> The TicketName property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TICKETNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TicketName
		{
			get { return (System.String)GetValue((int)TransactionJourneyHistoryFieldIndex.TicketName, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TicketName, value, true); }
		}

		/// <summary> The TicketNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TICKETNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.TicketNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TicketNumber, value, true); }
		}

		/// <summary> The ToStopName property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TOSTOPNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ToStopName
		{
			get { return (System.String)GetValue((int)TransactionJourneyHistoryFieldIndex.ToStopName, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.ToStopName, value, true); }
		}

		/// <summary> The ToStopNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TOSTOPNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ToStopNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.ToStopNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.ToStopNumber, value, true); }
		}

		/// <summary> The TransactionID property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TRANSACTIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionID
		{
			get { return (System.Int64)GetValue((int)TransactionJourneyHistoryFieldIndex.TransactionID, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The TripDateTime property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TRIPDATETIME"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TripDateTime
		{
			get { return (System.DateTime)GetValue((int)TransactionJourneyHistoryFieldIndex.TripDateTime, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TripDateTime, value, true); }
		}

		/// <summary> The TripSerialNumber property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TRIPSERIALNO"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripSerialNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJourneyHistoryFieldIndex.TripSerialNumber, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TripSerialNumber, value, true); }
		}

		/// <summary> The TypeID property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TYPEID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJourneyHistoryFieldIndex.TypeID, false); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TypeID, value, true); }
		}

		/// <summary> The TypeName property of the Entity TransactionJourneyHistory<br/><br/></summary>
		/// <remarks>Mapped on  view field: "SL_TRANSACTIONJOURNEYHISTORY"."TYPENAME"<br/>
		/// View field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TypeName
		{
			get { return (System.String)GetValue((int)TransactionJourneyHistoryFieldIndex.TypeName, true); }
			set	{ SetValue((int)TransactionJourneyHistoryFieldIndex.TypeName, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TransactionJourneyHistoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
