﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DunningProcess'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DunningProcessEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection	_dunningToInvoices;
		private bool	_alwaysFetchDunningToInvoices, _alreadyFetchedDunningToInvoices;
		private VarioSL.Entities.CollectionClasses.DunningToPostingCollection	_dunningToPostings;
		private bool	_alwaysFetchDunningToPostings, _alreadyFetchedDunningToPostings;
		private VarioSL.Entities.CollectionClasses.DunningToProductCollection	_dunningToProducts;
		private bool	_alwaysFetchDunningToProducts, _alreadyFetchedDunningToProducts;
		private VarioSL.Entities.CollectionClasses.PostingCollection _postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.ProductCollection _products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name DunningToInvoices</summary>
			public static readonly string DunningToInvoices = "DunningToInvoices";
			/// <summary>Member name DunningToPostings</summary>
			public static readonly string DunningToPostings = "DunningToPostings";
			/// <summary>Member name DunningToProducts</summary>
			public static readonly string DunningToProducts = "DunningToProducts";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DunningProcessEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DunningProcessEntity() :base("DunningProcessEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		public DunningProcessEntity(System.Int64 dunningProcessID):base("DunningProcessEntity")
		{
			InitClassFetch(dunningProcessID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DunningProcessEntity(System.Int64 dunningProcessID, IPrefetchPath prefetchPathToUse):base("DunningProcessEntity")
		{
			InitClassFetch(dunningProcessID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="validator">The custom validator object for this DunningProcessEntity</param>
		public DunningProcessEntity(System.Int64 dunningProcessID, IValidator validator):base("DunningProcessEntity")
		{
			InitClassFetch(dunningProcessID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DunningProcessEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningToInvoices = (VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection)info.GetValue("_dunningToInvoices", typeof(VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection));
			_alwaysFetchDunningToInvoices = info.GetBoolean("_alwaysFetchDunningToInvoices");
			_alreadyFetchedDunningToInvoices = info.GetBoolean("_alreadyFetchedDunningToInvoices");

			_dunningToPostings = (VarioSL.Entities.CollectionClasses.DunningToPostingCollection)info.GetValue("_dunningToPostings", typeof(VarioSL.Entities.CollectionClasses.DunningToPostingCollection));
			_alwaysFetchDunningToPostings = info.GetBoolean("_alwaysFetchDunningToPostings");
			_alreadyFetchedDunningToPostings = info.GetBoolean("_alreadyFetchedDunningToPostings");

			_dunningToProducts = (VarioSL.Entities.CollectionClasses.DunningToProductCollection)info.GetValue("_dunningToProducts", typeof(VarioSL.Entities.CollectionClasses.DunningToProductCollection));
			_alwaysFetchDunningToProducts = info.GetBoolean("_alwaysFetchDunningToProducts");
			_alreadyFetchedDunningToProducts = info.GetBoolean("_alreadyFetchedDunningToProducts");
			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");
			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DunningProcessFieldIndex)fieldIndex)
			{
				case DunningProcessFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningToInvoices = (_dunningToInvoices.Count > 0);
			_alreadyFetchedDunningToPostings = (_dunningToPostings.Count > 0);
			_alreadyFetchedDunningToProducts = (_dunningToProducts.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedContract = (_contract != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "DunningToInvoices":
					toReturn.Add(Relations.DunningToInvoiceEntityUsingDunningProcessID);
					break;
				case "DunningToPostings":
					toReturn.Add(Relations.DunningToPostingEntityUsingDunningProcessID);
					break;
				case "DunningToProducts":
					toReturn.Add(Relations.DunningToProductEntityUsingDunningProcessID);
					break;
				case "Postings":
					toReturn.Add(Relations.DunningToPostingEntityUsingDunningProcessID, "DunningProcessEntity__", "DunningToPosting_", JoinHint.None);
					toReturn.Add(DunningToPostingEntity.Relations.PostingEntityUsingPostingID, "DunningToPosting_", string.Empty, JoinHint.None);
					break;
				case "Products":
					toReturn.Add(Relations.DunningToProductEntityUsingDunningProcessID, "DunningProcessEntity__", "DunningToProduct_", JoinHint.None);
					toReturn.Add(DunningToProductEntity.Relations.ProductEntityUsingProductID, "DunningToProduct_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningToInvoices", (!this.MarkedForDeletion?_dunningToInvoices:null));
			info.AddValue("_alwaysFetchDunningToInvoices", _alwaysFetchDunningToInvoices);
			info.AddValue("_alreadyFetchedDunningToInvoices", _alreadyFetchedDunningToInvoices);
			info.AddValue("_dunningToPostings", (!this.MarkedForDeletion?_dunningToPostings:null));
			info.AddValue("_alwaysFetchDunningToPostings", _alwaysFetchDunningToPostings);
			info.AddValue("_alreadyFetchedDunningToPostings", _alreadyFetchedDunningToPostings);
			info.AddValue("_dunningToProducts", (!this.MarkedForDeletion?_dunningToProducts:null));
			info.AddValue("_alwaysFetchDunningToProducts", _alwaysFetchDunningToProducts);
			info.AddValue("_alreadyFetchedDunningToProducts", _alreadyFetchedDunningToProducts);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "DunningToInvoices":
					_alreadyFetchedDunningToInvoices = true;
					if(entity!=null)
					{
						this.DunningToInvoices.Add((DunningToInvoiceEntity)entity);
					}
					break;
				case "DunningToPostings":
					_alreadyFetchedDunningToPostings = true;
					if(entity!=null)
					{
						this.DunningToPostings.Add((DunningToPostingEntity)entity);
					}
					break;
				case "DunningToProducts":
					_alreadyFetchedDunningToProducts = true;
					if(entity!=null)
					{
						this.DunningToProducts.Add((DunningToProductEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "DunningToInvoices":
					_dunningToInvoices.Add((DunningToInvoiceEntity)relatedEntity);
					break;
				case "DunningToPostings":
					_dunningToPostings.Add((DunningToPostingEntity)relatedEntity);
					break;
				case "DunningToProducts":
					_dunningToProducts.Add((DunningToProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "DunningToInvoices":
					this.PerformRelatedEntityRemoval(_dunningToInvoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningToPostings":
					this.PerformRelatedEntityRemoval(_dunningToPostings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningToProducts":
					this.PerformRelatedEntityRemoval(_dunningToProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_dunningToInvoices);
			toReturn.Add(_dunningToPostings);
			toReturn.Add(_dunningToProducts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningProcessID)
		{
			return FetchUsingPK(dunningProcessID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningProcessID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(dunningProcessID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningProcessID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(dunningProcessID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningProcessID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(dunningProcessID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DunningProcessID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DunningProcessRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToInvoices || forceFetch || _alwaysFetchDunningToInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToInvoices);
				_dunningToInvoices.SuppressClearInGetMulti=!forceFetch;
				_dunningToInvoices.EntityFactoryToUse = entityFactoryToUse;
				_dunningToInvoices.GetMultiManyToOne(null, this, null, filter);
				_dunningToInvoices.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToInvoices = true;
			}
			return _dunningToInvoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToInvoices'. These settings will be taken into account
		/// when the property DunningToInvoices is requested or GetMultiDunningToInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToInvoices.SortClauses=sortClauses;
			_dunningToInvoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToPostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch)
		{
			return GetMultiDunningToPostings(forceFetch, _dunningToPostings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToPostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToPostings(forceFetch, _dunningToPostings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToPostings || forceFetch || _alwaysFetchDunningToPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToPostings);
				_dunningToPostings.SuppressClearInGetMulti=!forceFetch;
				_dunningToPostings.EntityFactoryToUse = entityFactoryToUse;
				_dunningToPostings.GetMultiManyToOne(this, null, filter);
				_dunningToPostings.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToPostings = true;
			}
			return _dunningToPostings;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToPostings'. These settings will be taken into account
		/// when the property DunningToPostings is requested or GetMultiDunningToPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToPostings.SortClauses=sortClauses;
			_dunningToPostings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch)
		{
			return GetMultiDunningToProducts(forceFetch, _dunningToProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToProducts(forceFetch, _dunningToProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToProducts || forceFetch || _alwaysFetchDunningToProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToProducts);
				_dunningToProducts.SuppressClearInGetMulti=!forceFetch;
				_dunningToProducts.EntityFactoryToUse = entityFactoryToUse;
				_dunningToProducts.GetMultiManyToOne(this, null, filter);
				_dunningToProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToProducts = true;
			}
			return _dunningToProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToProducts'. These settings will be taken into account
		/// when the property DunningToProducts is requested or GetMultiDunningToProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToProducts.SortClauses=sortClauses;
			_dunningToProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DunningProcessFields.DunningProcessID, ComparisonOperator.Equal, this.DunningProcessID, "DunningProcessEntity__"));
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMulti(filter, GetRelationsForField("Postings"));
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DunningProcessFields.DunningProcessID, ComparisonOperator.Equal, this.DunningProcessID, "DunningProcessEntity__"));
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMulti(filter, GetRelationsForField("Products"));
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Contract", _contract);
			toReturn.Add("DunningToInvoices", _dunningToInvoices);
			toReturn.Add("DunningToPostings", _dunningToPostings);
			toReturn.Add("DunningToProducts", _dunningToProducts);
			toReturn.Add("Postings", _postings);
			toReturn.Add("Products", _products);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="validator">The validator object for this DunningProcessEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 dunningProcessID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(dunningProcessID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_dunningToInvoices = new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection();
			_dunningToInvoices.SetContainingEntityInfo(this, "DunningProcess");

			_dunningToPostings = new VarioSL.Entities.CollectionClasses.DunningToPostingCollection();
			_dunningToPostings.SetContainingEntityInfo(this, "DunningProcess");

			_dunningToProducts = new VarioSL.Entities.CollectionClasses.DunningToProductCollection();
			_dunningToProducts.SetContainingEntityInfo(this, "DunningProcess");
			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_contractReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningProcessID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsClosed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticDunningProcessRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "DunningProcesses", resetFKFields, new int[] { (int)DunningProcessFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticDunningProcessRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="dunningProcessID">PK value for DunningProcess which data should be fetched into this DunningProcess object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 dunningProcessID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DunningProcessFieldIndex.DunningProcessID].ForcedCurrentValueWrite(dunningProcessID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDunningProcessDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DunningProcessEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DunningProcessRelations Relations
		{
			get	{ return new DunningProcessRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToInvoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection(), (IEntityRelation)GetRelationsForField("DunningToInvoices")[0], (int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, 0, null, null, null, "DunningToInvoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToPosting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToPostingCollection(), (IEntityRelation)GetRelationsForField("DunningToPostings")[0], (int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.DunningToPostingEntity, 0, null, null, null, "DunningToPostings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToProductCollection(), (IEntityRelation)GetRelationsForField("DunningToProducts")[0], (int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.DunningToProductEntity, 0, null, null, null, "DunningToProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DunningToPostingEntityUsingDunningProcessID;
				intermediateRelation.SetAliases(string.Empty, "DunningToPosting_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, GetRelationsForField("Postings"), "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DunningToProductEntityUsingDunningProcessID;
				intermediateRelation.SetAliases(string.Empty, "DunningToProduct_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, GetRelationsForField("Products"), "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.DunningProcessEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ContractID property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)DunningProcessFieldIndex.ContractID, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreationDate
		{
			get { return (System.DateTime)GetValue((int)DunningProcessFieldIndex.CreationDate, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The Description property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DunningProcessFieldIndex.Description, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.Description, value, true); }
		}

		/// <summary> The DunningProcessID property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."DUNNINGPROCESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DunningProcessID
		{
			get { return (System.Int64)GetValue((int)DunningProcessFieldIndex.DunningProcessID, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.DunningProcessID, value, true); }
		}

		/// <summary> The ExternalID property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."EXTERNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalID
		{
			get { return (System.String)GetValue((int)DunningProcessFieldIndex.ExternalID, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.ExternalID, value, true); }
		}

		/// <summary> The IsClosed property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."ISCLOSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsClosed
		{
			get { return (System.Boolean)GetValue((int)DunningProcessFieldIndex.IsClosed, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.IsClosed, value, true); }
		}

		/// <summary> The LastModified property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DunningProcessFieldIndex.LastModified, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUpdated property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."LASTUPDATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastUpdated
		{
			get { return (System.DateTime)GetValue((int)DunningProcessFieldIndex.LastUpdated, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.LastUpdated, value, true); }
		}

		/// <summary> The LastUser property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DunningProcessFieldIndex.LastUser, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.LastUser, value, true); }
		}

		/// <summary> The ProcessNumber property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."PROCESSNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ProcessNumber
		{
			get { return (System.String)GetValue((int)DunningProcessFieldIndex.ProcessNumber, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.ProcessNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DunningProcess<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGPROCESS"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DunningProcessFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DunningProcessFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection DunningToInvoices
		{
			get	{ return GetMultiDunningToInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToInvoices. When set to true, DunningToInvoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToInvoices is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToInvoices
		{
			get	{ return _alwaysFetchDunningToInvoices; }
			set	{ _alwaysFetchDunningToInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToInvoices already has been fetched. Setting this property to false when DunningToInvoices has been fetched
		/// will clear the DunningToInvoices collection well. Setting this property to true while DunningToInvoices hasn't been fetched disables lazy loading for DunningToInvoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToInvoices
		{
			get { return _alreadyFetchedDunningToInvoices;}
			set 
			{
				if(_alreadyFetchedDunningToInvoices && !value && (_dunningToInvoices != null))
				{
					_dunningToInvoices.Clear();
				}
				_alreadyFetchedDunningToInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToPostingCollection DunningToPostings
		{
			get	{ return GetMultiDunningToPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToPostings. When set to true, DunningToPostings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToPostings is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToPostings
		{
			get	{ return _alwaysFetchDunningToPostings; }
			set	{ _alwaysFetchDunningToPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToPostings already has been fetched. Setting this property to false when DunningToPostings has been fetched
		/// will clear the DunningToPostings collection well. Setting this property to true while DunningToPostings hasn't been fetched disables lazy loading for DunningToPostings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToPostings
		{
			get { return _alreadyFetchedDunningToPostings;}
			set 
			{
				if(_alreadyFetchedDunningToPostings && !value && (_dunningToPostings != null))
				{
					_dunningToPostings.Clear();
				}
				_alreadyFetchedDunningToPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToProductCollection DunningToProducts
		{
			get	{ return GetMultiDunningToProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToProducts. When set to true, DunningToProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToProducts is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToProducts
		{
			get	{ return _alwaysFetchDunningToProducts; }
			set	{ _alwaysFetchDunningToProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToProducts already has been fetched. Setting this property to false when DunningToProducts has been fetched
		/// will clear the DunningToProducts collection well. Setting this property to true while DunningToProducts hasn't been fetched disables lazy loading for DunningToProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToProducts
		{
			get { return _alreadyFetchedDunningToProducts;}
			set 
			{
				if(_alreadyFetchedDunningToProducts && !value && (_dunningToProducts != null))
				{
					_dunningToProducts.Clear();
				}
				_alreadyFetchedDunningToProducts = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get { return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get { return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningProcesses", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DunningProcessEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
