﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FilterValueSet'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FilterValueSetEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FilterValueCollection	_filterValues;
		private bool	_alwaysFetchFilterValues, _alreadyFetchedFilterValues;
		private VarioSL.Entities.CollectionClasses.ReportJobCollection	_reportJobs;
		private bool	_alwaysFetchReportJobs, _alreadyFetchedReportJobs;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private UserListEntity _userList_;
		private bool	_alwaysFetchUserList_, _alreadyFetchedUserList_, _userList_ReturnsNewIfNotFound;
		private FilterListEntity _filterList;
		private bool	_alwaysFetchFilterList, _alreadyFetchedFilterList, _filterListReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name UserList_</summary>
			public static readonly string UserList_ = "UserList_";
			/// <summary>Member name FilterList</summary>
			public static readonly string FilterList = "FilterList";
			/// <summary>Member name FilterValues</summary>
			public static readonly string FilterValues = "FilterValues";
			/// <summary>Member name ReportJobs</summary>
			public static readonly string ReportJobs = "ReportJobs";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FilterValueSetEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilterValueSetEntity() :base("FilterValueSetEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		public FilterValueSetEntity(System.Int64 filterValueSetID):base("FilterValueSetEntity")
		{
			InitClassFetch(filterValueSetID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FilterValueSetEntity(System.Int64 filterValueSetID, IPrefetchPath prefetchPathToUse):base("FilterValueSetEntity")
		{
			InitClassFetch(filterValueSetID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="validator">The custom validator object for this FilterValueSetEntity</param>
		public FilterValueSetEntity(System.Int64 filterValueSetID, IValidator validator):base("FilterValueSetEntity")
		{
			InitClassFetch(filterValueSetID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilterValueSetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_filterValues = (VarioSL.Entities.CollectionClasses.FilterValueCollection)info.GetValue("_filterValues", typeof(VarioSL.Entities.CollectionClasses.FilterValueCollection));
			_alwaysFetchFilterValues = info.GetBoolean("_alwaysFetchFilterValues");
			_alreadyFetchedFilterValues = info.GetBoolean("_alreadyFetchedFilterValues");

			_reportJobs = (VarioSL.Entities.CollectionClasses.ReportJobCollection)info.GetValue("_reportJobs", typeof(VarioSL.Entities.CollectionClasses.ReportJobCollection));
			_alwaysFetchReportJobs = info.GetBoolean("_alwaysFetchReportJobs");
			_alreadyFetchedReportJobs = info.GetBoolean("_alreadyFetchedReportJobs");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_userList_ = (UserListEntity)info.GetValue("_userList_", typeof(UserListEntity));
			if(_userList_!=null)
			{
				_userList_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userList_ReturnsNewIfNotFound = info.GetBoolean("_userList_ReturnsNewIfNotFound");
			_alwaysFetchUserList_ = info.GetBoolean("_alwaysFetchUserList_");
			_alreadyFetchedUserList_ = info.GetBoolean("_alreadyFetchedUserList_");

			_filterList = (FilterListEntity)info.GetValue("_filterList", typeof(FilterListEntity));
			if(_filterList!=null)
			{
				_filterList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_filterListReturnsNewIfNotFound = info.GetBoolean("_filterListReturnsNewIfNotFound");
			_alwaysFetchFilterList = info.GetBoolean("_alwaysFetchFilterList");
			_alreadyFetchedFilterList = info.GetBoolean("_alreadyFetchedFilterList");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FilterValueSetFieldIndex)fieldIndex)
			{
				case FilterValueSetFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case FilterValueSetFieldIndex.FilterListID:
					DesetupSyncFilterList(true, false);
					_alreadyFetchedFilterList = false;
					break;
				case FilterValueSetFieldIndex.OwnerID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				case FilterValueSetFieldIndex.UserID:
					DesetupSyncUserList_(true, false);
					_alreadyFetchedUserList_ = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFilterValues = (_filterValues.Count > 0);
			_alreadyFetchedReportJobs = (_reportJobs.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedUserList_ = (_userList_ != null);
			_alreadyFetchedFilterList = (_filterList != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingOwnerID);
					break;
				case "UserList_":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "FilterList":
					toReturn.Add(Relations.FilterListEntityUsingFilterListID);
					break;
				case "FilterValues":
					toReturn.Add(Relations.FilterValueEntityUsingFilterValueSetID);
					break;
				case "ReportJobs":
					toReturn.Add(Relations.ReportJobEntityUsingFilterValueSetID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_filterValues", (!this.MarkedForDeletion?_filterValues:null));
			info.AddValue("_alwaysFetchFilterValues", _alwaysFetchFilterValues);
			info.AddValue("_alreadyFetchedFilterValues", _alreadyFetchedFilterValues);
			info.AddValue("_reportJobs", (!this.MarkedForDeletion?_reportJobs:null));
			info.AddValue("_alwaysFetchReportJobs", _alwaysFetchReportJobs);
			info.AddValue("_alreadyFetchedReportJobs", _alreadyFetchedReportJobs);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_userList_", (!this.MarkedForDeletion?_userList_:null));
			info.AddValue("_userList_ReturnsNewIfNotFound", _userList_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList_", _alwaysFetchUserList_);
			info.AddValue("_alreadyFetchedUserList_", _alreadyFetchedUserList_);
			info.AddValue("_filterList", (!this.MarkedForDeletion?_filterList:null));
			info.AddValue("_filterListReturnsNewIfNotFound", _filterListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFilterList", _alwaysFetchFilterList);
			info.AddValue("_alreadyFetchedFilterList", _alreadyFetchedFilterList);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "UserList_":
					_alreadyFetchedUserList_ = true;
					this.UserList_ = (UserListEntity)entity;
					break;
				case "FilterList":
					_alreadyFetchedFilterList = true;
					this.FilterList = (FilterListEntity)entity;
					break;
				case "FilterValues":
					_alreadyFetchedFilterValues = true;
					if(entity!=null)
					{
						this.FilterValues.Add((FilterValueEntity)entity);
					}
					break;
				case "ReportJobs":
					_alreadyFetchedReportJobs = true;
					if(entity!=null)
					{
						this.ReportJobs.Add((ReportJobEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "UserList_":
					SetupSyncUserList_(relatedEntity);
					break;
				case "FilterList":
					SetupSyncFilterList(relatedEntity);
					break;
				case "FilterValues":
					_filterValues.Add((FilterValueEntity)relatedEntity);
					break;
				case "ReportJobs":
					_reportJobs.Add((ReportJobEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "UserList_":
					DesetupSyncUserList_(false, true);
					break;
				case "FilterList":
					DesetupSyncFilterList(false, true);
					break;
				case "FilterValues":
					this.PerformRelatedEntityRemoval(_filterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportJobs":
					this.PerformRelatedEntityRemoval(_reportJobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_userList_!=null)
			{
				toReturn.Add(_userList_);
			}
			if(_filterList!=null)
			{
				toReturn.Add(_filterList);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_filterValues);
			toReturn.Add(_reportJobs);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueSetID)
		{
			return FetchUsingPK(filterValueSetID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueSetID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(filterValueSetID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueSetID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(filterValueSetID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterValueSetID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(filterValueSetID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FilterValueSetID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FilterValueSetRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch)
		{
			return GetMultiFilterValues(forceFetch, _filterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValues(forceFetch, _filterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueCollection GetMultiFilterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValues || forceFetch || _alwaysFetchFilterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValues);
				_filterValues.SuppressClearInGetMulti=!forceFetch;
				_filterValues.EntityFactoryToUse = entityFactoryToUse;
				_filterValues.GetMultiManyToOne(null, this, filter);
				_filterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValues = true;
			}
			return _filterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValues'. These settings will be taken into account
		/// when the property FilterValues is requested or GetMultiFilterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValues.SortClauses=sortClauses;
			_filterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportJobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportJobs(forceFetch, _reportJobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection GetMultiReportJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportJobs || forceFetch || _alwaysFetchReportJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportJobs);
				_reportJobs.SuppressClearInGetMulti=!forceFetch;
				_reportJobs.EntityFactoryToUse = entityFactoryToUse;
				_reportJobs.GetMultiManyToOne(null, null, this, null, filter);
				_reportJobs.SuppressClearInGetMulti=false;
				_alreadyFetchedReportJobs = true;
			}
			return _reportJobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportJobs'. These settings will be taken into account
		/// when the property ReportJobs is requested or GetMultiReportJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportJobs.SortClauses=sortClauses;
			_reportJobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingOwnerID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList_()
		{
			return GetSingleUserList_(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList_(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList_ || forceFetch || _alwaysFetchUserList_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userList_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList_ = newEntity;
				_alreadyFetchedUserList_ = fetchResult;
			}
			return _userList_;
		}


		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public FilterListEntity GetSingleFilterList()
		{
			return GetSingleFilterList(false);
		}

		/// <summary> Retrieves the related entity of type 'FilterListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FilterListEntity' which is related to this entity.</returns>
		public virtual FilterListEntity GetSingleFilterList(bool forceFetch)
		{
			if( ( !_alreadyFetchedFilterList || forceFetch || _alwaysFetchFilterList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FilterListEntityUsingFilterListID);
				FilterListEntity newEntity = new FilterListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FilterListID);
				}
				if(fetchResult)
				{
					newEntity = (FilterListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_filterListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FilterList = newEntity;
				_alreadyFetchedFilterList = fetchResult;
			}
			return _filterList;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("UserList", _userList);
			toReturn.Add("UserList_", _userList_);
			toReturn.Add("FilterList", _filterList);
			toReturn.Add("FilterValues", _filterValues);
			toReturn.Add("ReportJobs", _reportJobs);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="validator">The validator object for this FilterValueSetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 filterValueSetID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(filterValueSetID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_filterValues = new VarioSL.Entities.CollectionClasses.FilterValueCollection();
			_filterValues.SetContainingEntityInfo(this, "FilterValueSet");

			_reportJobs = new VarioSL.Entities.CollectionClasses.ReportJobCollection();
			_reportJobs.SetContainingEntityInfo(this, "FilterValueSet");
			_clientReturnsNewIfNotFound = false;
			_userListReturnsNewIfNotFound = false;
			_userList_ReturnsNewIfNotFound = false;
			_filterListReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Changed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterValueSetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProtected", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsStandard", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "FilterValueSets", resetFKFields, new int[] { (int)FilterValueSetFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.UserListEntityUsingOwnerIDStatic, true, signalRelatedEntity, "FilterValueSets", resetFKFields, new int[] { (int)FilterValueSetFieldIndex.OwnerID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.UserListEntityUsingOwnerIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userList_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList_, new PropertyChangedEventHandler( OnUserList_PropertyChanged ), "UserList_", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "FilterValueSets_", resetFKFields, new int[] { (int)FilterValueSetFieldIndex.UserID } );		
			_userList_ = null;
		}
		
		/// <summary> setups the sync logic for member _userList_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList_(IEntityCore relatedEntity)
		{
			if(_userList_!=relatedEntity)
			{		
				DesetupSyncUserList_(true, true);
				_userList_ = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList_, new PropertyChangedEventHandler( OnUserList_PropertyChanged ), "UserList_", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUserList_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserList_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _filterList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFilterList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.FilterListEntityUsingFilterListIDStatic, true, signalRelatedEntity, "FilterValueSets", resetFKFields, new int[] { (int)FilterValueSetFieldIndex.FilterListID } );		
			_filterList = null;
		}
		
		/// <summary> setups the sync logic for member _filterList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFilterList(IEntityCore relatedEntity)
		{
			if(_filterList!=relatedEntity)
			{		
				DesetupSyncFilterList(true, true);
				_filterList = (FilterListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _filterList, new PropertyChangedEventHandler( OnFilterListPropertyChanged ), "FilterList", VarioSL.Entities.RelationClasses.StaticFilterValueSetRelations.FilterListEntityUsingFilterListIDStatic, true, ref _alreadyFetchedFilterList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFilterListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="filterValueSetID">PK value for FilterValueSet which data should be fetched into this FilterValueSet object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 filterValueSetID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FilterValueSetFieldIndex.FilterValueSetID].ForcedCurrentValueWrite(filterValueSetID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFilterValueSetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FilterValueSetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FilterValueSetRelations Relations
		{
			get	{ return new FilterValueSetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueCollection(), (IEntityRelation)GetRelationsForField("FilterValues")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.FilterValueEntity, 0, null, null, null, "FilterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJob' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobCollection(), (IEntityRelation)GetRelationsForField("ReportJobs")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.ReportJobEntity, 0, null, null, null, "ReportJobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList_
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList_")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListCollection(), (IEntityRelation)GetRelationsForField("FilterList")[0], (int)VarioSL.Entities.EntityType.FilterValueSetEntity, (int)VarioSL.Entities.EntityType.FilterListEntity, 0, null, null, null, "FilterList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Changed property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."CHANGED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Changed
		{
			get { return (System.DateTime)GetValue((int)FilterValueSetFieldIndex.Changed, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.Changed, value, true); }
		}

		/// <summary> The ClientID property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)FilterValueSetFieldIndex.ClientID, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.ClientID, value, true); }
		}

		/// <summary> The FilterListID property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."FILTERLISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FilterListID
		{
			get { return (System.Int64)GetValue((int)FilterValueSetFieldIndex.FilterListID, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.FilterListID, value, true); }
		}

		/// <summary> The FilterValueSetID property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."FILTERVALUESETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FilterValueSetID
		{
			get { return (System.Int64)GetValue((int)FilterValueSetFieldIndex.FilterValueSetID, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.FilterValueSetID, value, true); }
		}

		/// <summary> The IsProtected property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."ISPROTECTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProtected
		{
			get { return (System.Boolean)GetValue((int)FilterValueSetFieldIndex.IsProtected, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.IsProtected, value, true); }
		}

		/// <summary> The IsStandard property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."ISSTANDARD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsStandard
		{
			get { return (System.Boolean)GetValue((int)FilterValueSetFieldIndex.IsStandard, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.IsStandard, value, true); }
		}

		/// <summary> The LastModified property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FilterValueSetFieldIndex.LastModified, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FilterValueSetFieldIndex.LastUser, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FilterValueSetFieldIndex.Name, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.Name, value, true); }
		}

		/// <summary> The OwnerID property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."OWNERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OwnerID
		{
			get { return (System.Int64)GetValue((int)FilterValueSetFieldIndex.OwnerID, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.OwnerID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FilterValueSetFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserID property of the Entity FilterValueSet<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERVALUESET"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 UserID
		{
			get { return (System.Int64)GetValue((int)FilterValueSetFieldIndex.UserID, true); }
			set	{ SetValue((int)FilterValueSetFieldIndex.UserID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FilterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueCollection FilterValues
		{
			get	{ return GetMultiFilterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValues. When set to true, FilterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValues is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValues
		{
			get	{ return _alwaysFetchFilterValues; }
			set	{ _alwaysFetchFilterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValues already has been fetched. Setting this property to false when FilterValues has been fetched
		/// will clear the FilterValues collection well. Setting this property to true while FilterValues hasn't been fetched disables lazy loading for FilterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValues
		{
			get { return _alreadyFetchedFilterValues;}
			set 
			{
				if(_alreadyFetchedFilterValues && !value && (_filterValues != null))
				{
					_filterValues.Clear();
				}
				_alreadyFetchedFilterValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportJobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportJobCollection ReportJobs
		{
			get	{ return GetMultiReportJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJobs. When set to true, ReportJobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJobs is accessed. You can always execute/ a forced fetch by calling GetMultiReportJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJobs
		{
			get	{ return _alwaysFetchReportJobs; }
			set	{ _alwaysFetchReportJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJobs already has been fetched. Setting this property to false when ReportJobs has been fetched
		/// will clear the ReportJobs collection well. Setting this property to true while ReportJobs hasn't been fetched disables lazy loading for ReportJobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJobs
		{
			get { return _alreadyFetchedReportJobs;}
			set 
			{
				if(_alreadyFetchedReportJobs && !value && (_reportJobs != null))
				{
					_reportJobs.Clear();
				}
				_alreadyFetchedReportJobs = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValueSets", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValueSets", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList_
		{
			get	{ return GetSingleUserList_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValueSets_", "UserList_", _userList_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList_. When set to true, UserList_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList_ is accessed. You can always execute a forced fetch by calling GetSingleUserList_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList_
		{
			get	{ return _alwaysFetchUserList_; }
			set	{ _alwaysFetchUserList_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList_ already has been fetched. Setting this property to false when UserList_ has been fetched
		/// will set UserList_ to null as well. Setting this property to true while UserList_ hasn't been fetched disables lazy loading for UserList_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList_
		{
			get { return _alreadyFetchedUserList_;}
			set 
			{
				if(_alreadyFetchedUserList_ && !value)
				{
					this.UserList_ = null;
				}
				_alreadyFetchedUserList_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList_ is not found
		/// in the database. When set to true, UserList_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserList_ReturnsNewIfNotFound
		{
			get	{ return _userList_ReturnsNewIfNotFound; }
			set { _userList_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FilterListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFilterList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FilterListEntity FilterList
		{
			get	{ return GetSingleFilterList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFilterList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FilterValueSets", "FilterList", _filterList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FilterList. When set to true, FilterList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterList is accessed. You can always execute a forced fetch by calling GetSingleFilterList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterList
		{
			get	{ return _alwaysFetchFilterList; }
			set	{ _alwaysFetchFilterList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterList already has been fetched. Setting this property to false when FilterList has been fetched
		/// will set FilterList to null as well. Setting this property to true while FilterList hasn't been fetched disables lazy loading for FilterList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterList
		{
			get { return _alreadyFetchedFilterList;}
			set 
			{
				if(_alreadyFetchedFilterList && !value)
				{
					this.FilterList = null;
				}
				_alreadyFetchedFilterList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FilterList is not found
		/// in the database. When set to true, FilterList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FilterListReturnsNewIfNotFound
		{
			get	{ return _filterListReturnsNewIfNotFound; }
			set { _filterListReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FilterValueSetEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
