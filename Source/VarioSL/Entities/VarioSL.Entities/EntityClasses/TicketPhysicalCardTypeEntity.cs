﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketPhysicalCardType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketPhysicalCardTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardChipTypeEntity _cardChipType;
		private bool	_alwaysFetchCardChipType, _alreadyFetchedCardChipType, _cardChipTypeReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardChipType</summary>
			public static readonly string CardChipType = "CardChipType";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketPhysicalCardTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketPhysicalCardTypeEntity() :base("TicketPhysicalCardTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		public TicketPhysicalCardTypeEntity(System.Int64 physicalCardTypeID, System.Int64 ticketID):base("TicketPhysicalCardTypeEntity")
		{
			InitClassFetch(physicalCardTypeID, ticketID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketPhysicalCardTypeEntity(System.Int64 physicalCardTypeID, System.Int64 ticketID, IPrefetchPath prefetchPathToUse):base("TicketPhysicalCardTypeEntity")
		{
			InitClassFetch(physicalCardTypeID, ticketID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="validator">The custom validator object for this TicketPhysicalCardTypeEntity</param>
		public TicketPhysicalCardTypeEntity(System.Int64 physicalCardTypeID, System.Int64 ticketID, IValidator validator):base("TicketPhysicalCardTypeEntity")
		{
			InitClassFetch(physicalCardTypeID, ticketID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketPhysicalCardTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardChipType = (CardChipTypeEntity)info.GetValue("_cardChipType", typeof(CardChipTypeEntity));
			if(_cardChipType!=null)
			{
				_cardChipType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardChipTypeReturnsNewIfNotFound = info.GetBoolean("_cardChipTypeReturnsNewIfNotFound");
			_alwaysFetchCardChipType = info.GetBoolean("_alwaysFetchCardChipType");
			_alreadyFetchedCardChipType = info.GetBoolean("_alreadyFetchedCardChipType");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketPhysicalCardTypeFieldIndex)fieldIndex)
			{
				case TicketPhysicalCardTypeFieldIndex.PhysicalCardTypeID:
					DesetupSyncCardChipType(true, false);
					_alreadyFetchedCardChipType = false;
					break;
				case TicketPhysicalCardTypeFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardChipType = (_cardChipType != null);
			_alreadyFetchedTicket = (_ticket != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardChipType":
					toReturn.Add(Relations.CardChipTypeEntityUsingPhysicalCardTypeID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardChipType", (!this.MarkedForDeletion?_cardChipType:null));
			info.AddValue("_cardChipTypeReturnsNewIfNotFound", _cardChipTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardChipType", _alwaysFetchCardChipType);
			info.AddValue("_alreadyFetchedCardChipType", _alreadyFetchedCardChipType);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardChipType":
					_alreadyFetchedCardChipType = true;
					this.CardChipType = (CardChipTypeEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardChipType":
					SetupSyncCardChipType(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardChipType":
					DesetupSyncCardChipType(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardChipType!=null)
			{
				toReturn.Add(_cardChipType);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 physicalCardTypeID, System.Int64 ticketID)
		{
			return FetchUsingPK(physicalCardTypeID, ticketID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 physicalCardTypeID, System.Int64 ticketID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(physicalCardTypeID, ticketID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 physicalCardTypeID, System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(physicalCardTypeID, ticketID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 physicalCardTypeID, System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(physicalCardTypeID, ticketID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PhysicalCardTypeID, this.TicketID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketPhysicalCardTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardChipTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardChipTypeEntity' which is related to this entity.</returns>
		public CardChipTypeEntity GetSingleCardChipType()
		{
			return GetSingleCardChipType(false);
		}

		/// <summary> Retrieves the related entity of type 'CardChipTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardChipTypeEntity' which is related to this entity.</returns>
		public virtual CardChipTypeEntity GetSingleCardChipType(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardChipType || forceFetch || _alwaysFetchCardChipType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardChipTypeEntityUsingPhysicalCardTypeID);
				CardChipTypeEntity newEntity = new CardChipTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PhysicalCardTypeID);
				}
				if(fetchResult)
				{
					newEntity = (CardChipTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardChipTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardChipType = newEntity;
				_alreadyFetchedCardChipType = fetchResult;
			}
			return _cardChipType;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardChipType", _cardChipType);
			toReturn.Add("Ticket", _ticket);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="validator">The validator object for this TicketPhysicalCardTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 physicalCardTypeID, System.Int64 ticketID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(physicalCardTypeID, ticketID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardChipTypeReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhysicalCardTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardChipType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardChipType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardChipType, new PropertyChangedEventHandler( OnCardChipTypePropertyChanged ), "CardChipType", VarioSL.Entities.RelationClasses.StaticTicketPhysicalCardTypeRelations.CardChipTypeEntityUsingPhysicalCardTypeIDStatic, true, signalRelatedEntity, "TicketCardChipTypes", resetFKFields, new int[] { (int)TicketPhysicalCardTypeFieldIndex.PhysicalCardTypeID } );		
			_cardChipType = null;
		}
		
		/// <summary> setups the sync logic for member _cardChipType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardChipType(IEntityCore relatedEntity)
		{
			if(_cardChipType!=relatedEntity)
			{		
				DesetupSyncCardChipType(true, true);
				_cardChipType = (CardChipTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardChipType, new PropertyChangedEventHandler( OnCardChipTypePropertyChanged ), "CardChipType", VarioSL.Entities.RelationClasses.StaticTicketPhysicalCardTypeRelations.CardChipTypeEntityUsingPhysicalCardTypeIDStatic, true, ref _alreadyFetchedCardChipType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardChipTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketPhysicalCardTypeRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "TicketPhysicalCardTypes", resetFKFields, new int[] { (int)TicketPhysicalCardTypeFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTicketPhysicalCardTypeRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="physicalCardTypeID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="ticketID">PK value for TicketPhysicalCardType which data should be fetched into this TicketPhysicalCardType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 physicalCardTypeID, System.Int64 ticketID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketPhysicalCardTypeFieldIndex.PhysicalCardTypeID].ForcedCurrentValueWrite(physicalCardTypeID);
				this.Fields[(int)TicketPhysicalCardTypeFieldIndex.TicketID].ForcedCurrentValueWrite(ticketID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketPhysicalCardTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketPhysicalCardTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketPhysicalCardTypeRelations Relations
		{
			get	{ return new TicketPhysicalCardTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardChipType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardChipType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardChipTypeCollection(), (IEntityRelation)GetRelationsForField("CardChipType")[0], (int)VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity, (int)VarioSL.Entities.EntityType.CardChipTypeEntity, 0, null, null, null, "CardChipType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PhysicalCardTypeID property of the Entity TicketPhysicalCardType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_PHYSICALCARDTYPE"."PHYSICALCARDTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PhysicalCardTypeID
		{
			get { return (System.Int64)GetValue((int)TicketPhysicalCardTypeFieldIndex.PhysicalCardTypeID, true); }
			set	{ SetValue((int)TicketPhysicalCardTypeFieldIndex.PhysicalCardTypeID, value, true); }
		}

		/// <summary> The TicketID property of the Entity TicketPhysicalCardType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKET_PHYSICALCARDTYPE"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)TicketPhysicalCardTypeFieldIndex.TicketID, true); }
			set	{ SetValue((int)TicketPhysicalCardTypeFieldIndex.TicketID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardChipTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardChipType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardChipTypeEntity CardChipType
		{
			get	{ return GetSingleCardChipType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardChipType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketCardChipTypes", "CardChipType", _cardChipType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardChipType. When set to true, CardChipType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardChipType is accessed. You can always execute a forced fetch by calling GetSingleCardChipType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardChipType
		{
			get	{ return _alwaysFetchCardChipType; }
			set	{ _alwaysFetchCardChipType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardChipType already has been fetched. Setting this property to false when CardChipType has been fetched
		/// will set CardChipType to null as well. Setting this property to true while CardChipType hasn't been fetched disables lazy loading for CardChipType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardChipType
		{
			get { return _alreadyFetchedCardChipType;}
			set 
			{
				if(_alreadyFetchedCardChipType && !value)
				{
					this.CardChipType = null;
				}
				_alreadyFetchedCardChipType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardChipType is not found
		/// in the database. When set to true, CardChipType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardChipTypeReturnsNewIfNotFound
		{
			get	{ return _cardChipTypeReturnsNewIfNotFound; }
			set { _cardChipTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketPhysicalCardTypes", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
