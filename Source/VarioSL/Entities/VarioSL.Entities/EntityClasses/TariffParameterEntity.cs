﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TariffParameter'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TariffParameterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection	_parameterAttributeValues;
		private bool	_alwaysFetchParameterAttributeValues, _alreadyFetchedParameterAttributeValues;
		private VarioSL.Entities.CollectionClasses.ParameterFareStageCollection	_parameterFareStage;
		private bool	_alwaysFetchParameterFareStage, _alreadyFetchedParameterFareStage;
		private VarioSL.Entities.CollectionClasses.ParameterTariffCollection	_parameterTariff;
		private bool	_alwaysFetchParameterTariff, _alreadyFetchedParameterTariff;
		private VarioSL.Entities.CollectionClasses.ParameterTicketCollection	_parameterTicket;
		private bool	_alwaysFetchParameterTicket, _alreadyFetchedParameterTicket;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParameterAttributeValues</summary>
			public static readonly string ParameterAttributeValues = "ParameterAttributeValues";
			/// <summary>Member name ParameterFareStage</summary>
			public static readonly string ParameterFareStage = "ParameterFareStage";
			/// <summary>Member name ParameterTariff</summary>
			public static readonly string ParameterTariff = "ParameterTariff";
			/// <summary>Member name ParameterTicket</summary>
			public static readonly string ParameterTicket = "ParameterTicket";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TariffParameterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TariffParameterEntity() :base("TariffParameterEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		public TariffParameterEntity(System.Int64 parameterID):base("TariffParameterEntity")
		{
			InitClassFetch(parameterID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TariffParameterEntity(System.Int64 parameterID, IPrefetchPath prefetchPathToUse):base("TariffParameterEntity")
		{
			InitClassFetch(parameterID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="validator">The custom validator object for this TariffParameterEntity</param>
		public TariffParameterEntity(System.Int64 parameterID, IValidator validator):base("TariffParameterEntity")
		{
			InitClassFetch(parameterID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TariffParameterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parameterAttributeValues = (VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection)info.GetValue("_parameterAttributeValues", typeof(VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection));
			_alwaysFetchParameterAttributeValues = info.GetBoolean("_alwaysFetchParameterAttributeValues");
			_alreadyFetchedParameterAttributeValues = info.GetBoolean("_alreadyFetchedParameterAttributeValues");

			_parameterFareStage = (VarioSL.Entities.CollectionClasses.ParameterFareStageCollection)info.GetValue("_parameterFareStage", typeof(VarioSL.Entities.CollectionClasses.ParameterFareStageCollection));
			_alwaysFetchParameterFareStage = info.GetBoolean("_alwaysFetchParameterFareStage");
			_alreadyFetchedParameterFareStage = info.GetBoolean("_alreadyFetchedParameterFareStage");

			_parameterTariff = (VarioSL.Entities.CollectionClasses.ParameterTariffCollection)info.GetValue("_parameterTariff", typeof(VarioSL.Entities.CollectionClasses.ParameterTariffCollection));
			_alwaysFetchParameterTariff = info.GetBoolean("_alwaysFetchParameterTariff");
			_alreadyFetchedParameterTariff = info.GetBoolean("_alreadyFetchedParameterTariff");

			_parameterTicket = (VarioSL.Entities.CollectionClasses.ParameterTicketCollection)info.GetValue("_parameterTicket", typeof(VarioSL.Entities.CollectionClasses.ParameterTicketCollection));
			_alwaysFetchParameterTicket = info.GetBoolean("_alwaysFetchParameterTicket");
			_alreadyFetchedParameterTicket = info.GetBoolean("_alreadyFetchedParameterTicket");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParameterAttributeValues = (_parameterAttributeValues.Count > 0);
			_alreadyFetchedParameterFareStage = (_parameterFareStage.Count > 0);
			_alreadyFetchedParameterTariff = (_parameterTariff.Count > 0);
			_alreadyFetchedParameterTicket = (_parameterTicket.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParameterAttributeValues":
					toReturn.Add(Relations.ParameterAttributeValueEntityUsingParameterID);
					break;
				case "ParameterFareStage":
					toReturn.Add(Relations.ParameterFareStageEntityUsingParameterID);
					break;
				case "ParameterTariff":
					toReturn.Add(Relations.ParameterTariffEntityUsingParameterID);
					break;
				case "ParameterTicket":
					toReturn.Add(Relations.ParameterTicketEntityUsingParameterID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parameterAttributeValues", (!this.MarkedForDeletion?_parameterAttributeValues:null));
			info.AddValue("_alwaysFetchParameterAttributeValues", _alwaysFetchParameterAttributeValues);
			info.AddValue("_alreadyFetchedParameterAttributeValues", _alreadyFetchedParameterAttributeValues);
			info.AddValue("_parameterFareStage", (!this.MarkedForDeletion?_parameterFareStage:null));
			info.AddValue("_alwaysFetchParameterFareStage", _alwaysFetchParameterFareStage);
			info.AddValue("_alreadyFetchedParameterFareStage", _alreadyFetchedParameterFareStage);
			info.AddValue("_parameterTariff", (!this.MarkedForDeletion?_parameterTariff:null));
			info.AddValue("_alwaysFetchParameterTariff", _alwaysFetchParameterTariff);
			info.AddValue("_alreadyFetchedParameterTariff", _alreadyFetchedParameterTariff);
			info.AddValue("_parameterTicket", (!this.MarkedForDeletion?_parameterTicket:null));
			info.AddValue("_alwaysFetchParameterTicket", _alwaysFetchParameterTicket);
			info.AddValue("_alreadyFetchedParameterTicket", _alreadyFetchedParameterTicket);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParameterAttributeValues":
					_alreadyFetchedParameterAttributeValues = true;
					if(entity!=null)
					{
						this.ParameterAttributeValues.Add((ParameterAttributeValueEntity)entity);
					}
					break;
				case "ParameterFareStage":
					_alreadyFetchedParameterFareStage = true;
					if(entity!=null)
					{
						this.ParameterFareStage.Add((ParameterFareStageEntity)entity);
					}
					break;
				case "ParameterTariff":
					_alreadyFetchedParameterTariff = true;
					if(entity!=null)
					{
						this.ParameterTariff.Add((ParameterTariffEntity)entity);
					}
					break;
				case "ParameterTicket":
					_alreadyFetchedParameterTicket = true;
					if(entity!=null)
					{
						this.ParameterTicket.Add((ParameterTicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParameterAttributeValues":
					_parameterAttributeValues.Add((ParameterAttributeValueEntity)relatedEntity);
					break;
				case "ParameterFareStage":
					_parameterFareStage.Add((ParameterFareStageEntity)relatedEntity);
					break;
				case "ParameterTariff":
					_parameterTariff.Add((ParameterTariffEntity)relatedEntity);
					break;
				case "ParameterTicket":
					_parameterTicket.Add((ParameterTicketEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParameterAttributeValues":
					this.PerformRelatedEntityRemoval(_parameterAttributeValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterFareStage":
					this.PerformRelatedEntityRemoval(_parameterFareStage, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterTariff":
					this.PerformRelatedEntityRemoval(_parameterTariff, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterTicket":
					this.PerformRelatedEntityRemoval(_parameterTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_parameterAttributeValues);
			toReturn.Add(_parameterFareStage);
			toReturn.Add(_parameterTariff);
			toReturn.Add(_parameterTicket);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID)
		{
			return FetchUsingPK(parameterID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(parameterID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(parameterID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(parameterID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParameterID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TariffParameterRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterAttributeValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterAttributeValues || forceFetch || _alwaysFetchParameterAttributeValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterAttributeValues);
				_parameterAttributeValues.SuppressClearInGetMulti=!forceFetch;
				_parameterAttributeValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterAttributeValues.GetMultiManyToOne(null, null, this, filter);
				_parameterAttributeValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterAttributeValues = true;
			}
			return _parameterAttributeValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterAttributeValues'. These settings will be taken into account
		/// when the property ParameterAttributeValues is requested or GetMultiParameterAttributeValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterAttributeValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterAttributeValues.SortClauses=sortClauses;
			_parameterAttributeValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterFareStage(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterFareStage || forceFetch || _alwaysFetchParameterFareStage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterFareStage);
				_parameterFareStage.SuppressClearInGetMulti=!forceFetch;
				_parameterFareStage.EntityFactoryToUse = entityFactoryToUse;
				_parameterFareStage.GetMultiManyToOne(null, null, this, filter);
				_parameterFareStage.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterFareStage = true;
			}
			return _parameterFareStage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterFareStage'. These settings will be taken into account
		/// when the property ParameterFareStage is requested or GetMultiParameterFareStage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterFareStage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterFareStage.SortClauses=sortClauses;
			_parameterFareStage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch)
		{
			return GetMultiParameterTariff(forceFetch, _parameterTariff.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterTariff(forceFetch, _parameterTariff.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterTariff(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterTariff || forceFetch || _alwaysFetchParameterTariff) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterTariff);
				_parameterTariff.SuppressClearInGetMulti=!forceFetch;
				_parameterTariff.EntityFactoryToUse = entityFactoryToUse;
				_parameterTariff.GetMultiManyToOne(null, this, filter);
				_parameterTariff.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterTariff = true;
			}
			return _parameterTariff;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterTariff'. These settings will be taken into account
		/// when the property ParameterTariff is requested or GetMultiParameterTariff is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterTariff(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterTariff.SortClauses=sortClauses;
			_parameterTariff.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch)
		{
			return GetMultiParameterTicket(forceFetch, _parameterTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterTicket(forceFetch, _parameterTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterTicket || forceFetch || _alwaysFetchParameterTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterTicket);
				_parameterTicket.SuppressClearInGetMulti=!forceFetch;
				_parameterTicket.EntityFactoryToUse = entityFactoryToUse;
				_parameterTicket.GetMultiManyToOne(null, this, null, filter);
				_parameterTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterTicket = true;
			}
			return _parameterTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterTicket'. These settings will be taken into account
		/// when the property ParameterTicket is requested or GetMultiParameterTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterTicket.SortClauses=sortClauses;
			_parameterTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParameterAttributeValues", _parameterAttributeValues);
			toReturn.Add("ParameterFareStage", _parameterFareStage);
			toReturn.Add("ParameterTariff", _parameterTariff);
			toReturn.Add("ParameterTicket", _parameterTicket);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="validator">The validator object for this TariffParameterEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 parameterID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(parameterID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_parameterAttributeValues = new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection();
			_parameterAttributeValues.SetContainingEntityInfo(this, "TmParameter");

			_parameterFareStage = new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection();
			_parameterFareStage.SetContainingEntityInfo(this, "Parameters");

			_parameterTariff = new VarioSL.Entities.CollectionClasses.ParameterTariffCollection();
			_parameterTariff.SetContainingEntityInfo(this, "Parameter");

			_parameterTicket = new VarioSL.Entities.CollectionClasses.ParameterTicketCollection();
			_parameterTicket.SetContainingEntityInfo(this, "Parameter");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DotNetType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOnDevice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterObjectType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParemeterDescription", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="parameterID">PK value for TariffParameter which data should be fetched into this TariffParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TariffParameterFieldIndex.ParameterID].ForcedCurrentValueWrite(parameterID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTariffParameterDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TariffParameterEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TariffParameterRelations Relations
		{
			get	{ return new TariffParameterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterAttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterAttributeValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection(), (IEntityRelation)GetRelationsForField("ParameterAttributeValues")[0], (int)VarioSL.Entities.EntityType.TariffParameterEntity, (int)VarioSL.Entities.EntityType.ParameterAttributeValueEntity, 0, null, null, null, "ParameterAttributeValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterFareStage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterFareStage
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection(), (IEntityRelation)GetRelationsForField("ParameterFareStage")[0], (int)VarioSL.Entities.EntityType.TariffParameterEntity, (int)VarioSL.Entities.EntityType.ParameterFareStageEntity, 0, null, null, null, "ParameterFareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterTariff
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterTariffCollection(), (IEntityRelation)GetRelationsForField("ParameterTariff")[0], (int)VarioSL.Entities.EntityType.TariffParameterEntity, (int)VarioSL.Entities.EntityType.ParameterTariffEntity, 0, null, null, null, "ParameterTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterTicketCollection(), (IEntityRelation)GetRelationsForField("ParameterTicket")[0], (int)VarioSL.Entities.EntityType.TariffParameterEntity, (int)VarioSL.Entities.EntityType.ParameterTicketEntity, 0, null, null, null, "ParameterTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DotNetType property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."DOTNETTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DotNetType
		{
			get { return (System.String)GetValue((int)TariffParameterFieldIndex.DotNetType, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.DotNetType, value, true); }
		}

		/// <summary> The NumberOnDevice property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."NUMBERONDEVICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NumberOnDevice
		{
			get { return (System.Int64)GetValue((int)TariffParameterFieldIndex.NumberOnDevice, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.NumberOnDevice, value, true); }
		}

		/// <summary> The ParameterID property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."PARAMETERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ParameterID
		{
			get { return (System.Int64)GetValue((int)TariffParameterFieldIndex.ParameterID, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.ParameterID, value, true); }
		}

		/// <summary> The ParameterName property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."PARAMETERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ParameterName
		{
			get { return (System.String)GetValue((int)TariffParameterFieldIndex.ParameterName, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.ParameterName, value, true); }
		}

		/// <summary> The ParameterObjectType property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."PARAMETEROBJECTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ParameterObjectType
		{
			get { return (System.Int64)GetValue((int)TariffParameterFieldIndex.ParameterObjectType, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.ParameterObjectType, value, true); }
		}

		/// <summary> The ParemeterDescription property of the Entity TariffParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PARAMETER"."PAREMETERDESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ParemeterDescription
		{
			get { return (System.String)GetValue((int)TariffParameterFieldIndex.ParemeterDescription, true); }
			set	{ SetValue((int)TariffParameterFieldIndex.ParemeterDescription, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterAttributeValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection ParameterAttributeValues
		{
			get	{ return GetMultiParameterAttributeValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterAttributeValues. When set to true, ParameterAttributeValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterAttributeValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterAttributeValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterAttributeValues
		{
			get	{ return _alwaysFetchParameterAttributeValues; }
			set	{ _alwaysFetchParameterAttributeValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterAttributeValues already has been fetched. Setting this property to false when ParameterAttributeValues has been fetched
		/// will clear the ParameterAttributeValues collection well. Setting this property to true while ParameterAttributeValues hasn't been fetched disables lazy loading for ParameterAttributeValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterAttributeValues
		{
			get { return _alreadyFetchedParameterAttributeValues;}
			set 
			{
				if(_alreadyFetchedParameterAttributeValues && !value && (_parameterAttributeValues != null))
				{
					_parameterAttributeValues.Clear();
				}
				_alreadyFetchedParameterAttributeValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection ParameterFareStage
		{
			get	{ return GetMultiParameterFareStage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterFareStage. When set to true, ParameterFareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterFareStage is accessed. You can always execute/ a forced fetch by calling GetMultiParameterFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterFareStage
		{
			get	{ return _alwaysFetchParameterFareStage; }
			set	{ _alwaysFetchParameterFareStage = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterFareStage already has been fetched. Setting this property to false when ParameterFareStage has been fetched
		/// will clear the ParameterFareStage collection well. Setting this property to true while ParameterFareStage hasn't been fetched disables lazy loading for ParameterFareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterFareStage
		{
			get { return _alreadyFetchedParameterFareStage;}
			set 
			{
				if(_alreadyFetchedParameterFareStage && !value && (_parameterFareStage != null))
				{
					_parameterFareStage.Clear();
				}
				_alreadyFetchedParameterFareStage = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTariffCollection ParameterTariff
		{
			get	{ return GetMultiParameterTariff(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterTariff. When set to true, ParameterTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterTariff is accessed. You can always execute/ a forced fetch by calling GetMultiParameterTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterTariff
		{
			get	{ return _alwaysFetchParameterTariff; }
			set	{ _alwaysFetchParameterTariff = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterTariff already has been fetched. Setting this property to false when ParameterTariff has been fetched
		/// will clear the ParameterTariff collection well. Setting this property to true while ParameterTariff hasn't been fetched disables lazy loading for ParameterTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterTariff
		{
			get { return _alreadyFetchedParameterTariff;}
			set 
			{
				if(_alreadyFetchedParameterTariff && !value && (_parameterTariff != null))
				{
					_parameterTariff.Clear();
				}
				_alreadyFetchedParameterTariff = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection ParameterTicket
		{
			get	{ return GetMultiParameterTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterTicket. When set to true, ParameterTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterTicket is accessed. You can always execute/ a forced fetch by calling GetMultiParameterTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterTicket
		{
			get	{ return _alwaysFetchParameterTicket; }
			set	{ _alwaysFetchParameterTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterTicket already has been fetched. Setting this property to false when ParameterTicket has been fetched
		/// will clear the ParameterTicket collection well. Setting this property to true while ParameterTicket hasn't been fetched disables lazy loading for ParameterTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterTicket
		{
			get { return _alreadyFetchedParameterTicket;}
			set 
			{
				if(_alreadyFetchedParameterTicket && !value && (_parameterTicket != null))
				{
					_parameterTicket.Clear();
				}
				_alreadyFetchedParameterTicket = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TariffParameterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
