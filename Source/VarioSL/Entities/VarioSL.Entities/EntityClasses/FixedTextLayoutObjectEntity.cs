﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FixedTextLayoutObject'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FixedTextLayoutObjectEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private LayoutEntity _layout;
		private bool	_alwaysFetchLayout, _alreadyFetchedLayout, _layoutReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Layout</summary>
			public static readonly string Layout = "Layout";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FixedTextLayoutObjectEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FixedTextLayoutObjectEntity() :base("FixedTextLayoutObjectEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		public FixedTextLayoutObjectEntity(System.Int64 layoutObjectID):base("FixedTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FixedTextLayoutObjectEntity(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse):base("FixedTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="validator">The custom validator object for this FixedTextLayoutObjectEntity</param>
		public FixedTextLayoutObjectEntity(System.Int64 layoutObjectID, IValidator validator):base("FixedTextLayoutObjectEntity")
		{
			InitClassFetch(layoutObjectID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FixedTextLayoutObjectEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_layout = (LayoutEntity)info.GetValue("_layout", typeof(LayoutEntity));
			if(_layout!=null)
			{
				_layout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_layoutReturnsNewIfNotFound = info.GetBoolean("_layoutReturnsNewIfNotFound");
			_alwaysFetchLayout = info.GetBoolean("_alwaysFetchLayout");
			_alreadyFetchedLayout = info.GetBoolean("_alreadyFetchedLayout");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FixedTextLayoutObjectFieldIndex)fieldIndex)
			{
				case FixedTextLayoutObjectFieldIndex.LayoutID:
					DesetupSyncLayout(true, false);
					_alreadyFetchedLayout = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLayout = (_layout != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Layout":
					toReturn.Add(Relations.LayoutEntityUsingLayoutID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_layout", (!this.MarkedForDeletion?_layout:null));
			info.AddValue("_layoutReturnsNewIfNotFound", _layoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLayout", _alwaysFetchLayout);
			info.AddValue("_alreadyFetchedLayout", _alreadyFetchedLayout);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Layout":
					_alreadyFetchedLayout = true;
					this.Layout = (LayoutEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Layout":
					SetupSyncLayout(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Layout":
					DesetupSyncLayout(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_layout!=null)
			{
				toReturn.Add(_layout);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID)
		{
			return FetchUsingPK(layoutObjectID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutObjectID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutObjectID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutObjectID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FixedTextLayoutObjectRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public LayoutEntity GetSingleLayout()
		{
			return GetSingleLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'LayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LayoutEntity' which is related to this entity.</returns>
		public virtual LayoutEntity GetSingleLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedLayout || forceFetch || _alwaysFetchLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LayoutEntityUsingLayoutID);
				LayoutEntity newEntity = new LayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.LayoutID);
				}
				if(fetchResult)
				{
					newEntity = (LayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_layoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Layout = newEntity;
				_alreadyFetchedLayout = fetchResult;
			}
			return _layout;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Layout", _layout);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="validator">The validator object for this FixedTextLayoutObjectEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutObjectID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutObjectID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_layoutReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientAdaptable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Column", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FontSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Height", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBold", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDouble", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsInverse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsUnderlined", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Row", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextAlignment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Width", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _layout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticFixedTextLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, signalRelatedEntity, "FixedTextLayoutObjects", resetFKFields, new int[] { (int)FixedTextLayoutObjectFieldIndex.LayoutID } );		
			_layout = null;
		}
		
		/// <summary> setups the sync logic for member _layout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLayout(IEntityCore relatedEntity)
		{
			if(_layout!=relatedEntity)
			{		
				DesetupSyncLayout(true, true);
				_layout = (LayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _layout, new PropertyChangedEventHandler( OnLayoutPropertyChanged ), "Layout", VarioSL.Entities.RelationClasses.StaticFixedTextLayoutObjectRelations.LayoutEntityUsingLayoutIDStatic, true, ref _alreadyFetchedLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutObjectID">PK value for FixedTextLayoutObject which data should be fetched into this FixedTextLayoutObject object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutObjectID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FixedTextLayoutObjectFieldIndex.LayoutObjectID].ForcedCurrentValueWrite(layoutObjectID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFixedTextLayoutObjectDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FixedTextLayoutObjectEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FixedTextLayoutObjectRelations Relations
		{
			get	{ return new FixedTextLayoutObjectRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layout")[0], (int)VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientAdaptable property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."CLIENTADAPTABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ClientAdaptable
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.ClientAdaptable, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.ClientAdaptable, value, true); }
		}

		/// <summary> The Column property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."COLUMN_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Column
		{
			get { return (System.Double)GetValue((int)FixedTextLayoutObjectFieldIndex.Column, true); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Column, value, true); }
		}

		/// <summary> The FieldWidth property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."FIELDWIDTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> FieldWidth
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.FieldWidth, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.FieldWidth, value, true); }
		}

		/// <summary> The FontSize property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."FONTSIZE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> FontSize
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.FontSize, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.FontSize, value, true); }
		}

		/// <summary> The Height property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."HEIGHT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Height
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.Height, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Height, value, true); }
		}

		/// <summary> The IsBold property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."ISBOLD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsBold
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.IsBold, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.IsBold, value, true); }
		}

		/// <summary> The IsDouble property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."ISDOUBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsDouble
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.IsDouble, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.IsDouble, value, true); }
		}

		/// <summary> The IsInverse property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."ISINVERSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsInverse
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.IsInverse, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.IsInverse, value, true); }
		}

		/// <summary> The IsUnderlined property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."ISUNDERLINED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> IsUnderlined
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.IsUnderlined, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.IsUnderlined, value, true); }
		}

		/// <summary> The LayoutID property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)FixedTextLayoutObjectFieldIndex.LayoutID, true); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The LayoutObjectID property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."LAYOUTOBJECTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutObjectID
		{
			get { return (System.Int64)GetValue((int)FixedTextLayoutObjectFieldIndex.LayoutObjectID, true); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.LayoutObjectID, value, true); }
		}

		/// <summary> The Number property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)FixedTextLayoutObjectFieldIndex.Number, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Number, value, true); }
		}

		/// <summary> The Row property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."ROW_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Row
		{
			get { return (System.Double)GetValue((int)FixedTextLayoutObjectFieldIndex.Row, true); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Row, value, true); }
		}

		/// <summary> The Text property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."TEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)FixedTextLayoutObjectFieldIndex.Text, true); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Text, value, true); }
		}

		/// <summary> The TextAlignment property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."TEXTALIGNMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TextAlignment
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.TextAlignment, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.TextAlignment, value, true); }
		}

		/// <summary> The Width property of the Entity FixedTextLayoutObject<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FIXEDTEXTLAYOUTOBJECT"."WIDTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Width
		{
			get { return (Nullable<System.Int16>)GetValue((int)FixedTextLayoutObjectFieldIndex.Width, false); }
			set	{ SetValue((int)FixedTextLayoutObjectFieldIndex.Width, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'LayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LayoutEntity Layout
		{
			get	{ return GetSingleLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FixedTextLayoutObjects", "Layout", _layout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Layout. When set to true, Layout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layout is accessed. You can always execute a forced fetch by calling GetSingleLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayout
		{
			get	{ return _alwaysFetchLayout; }
			set	{ _alwaysFetchLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layout already has been fetched. Setting this property to false when Layout has been fetched
		/// will set Layout to null as well. Setting this property to true while Layout hasn't been fetched disables lazy loading for Layout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayout
		{
			get { return _alreadyFetchedLayout;}
			set 
			{
				if(_alreadyFetchedLayout && !value)
				{
					this.Layout = null;
				}
				_alreadyFetchedLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Layout is not found
		/// in the database. When set to true, Layout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool LayoutReturnsNewIfNotFound
		{
			get	{ return _layoutReturnsNewIfNotFound; }
			set { _layoutReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
