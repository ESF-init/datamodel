﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OutputDevice'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OutputDeviceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.LayoutCollection	_layouts;
		private bool	_alwaysFetchLayouts, _alreadyFetchedLayouts;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection	_ticketDeviceClassOutputDevice;
		private bool	_alwaysFetchTicketDeviceClassOutputDevice, _alreadyFetchedTicketDeviceClassOutputDevice;
		private VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection	_ticketOutputDevices;
		private bool	_alwaysFetchTicketOutputDevices, _alreadyFetchedTicketOutputDevices;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection _ticketDeviceClasses;
		private bool	_alwaysFetchTicketDeviceClasses, _alreadyFetchedTicketDeviceClasses;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Layouts</summary>
			public static readonly string Layouts = "Layouts";
			/// <summary>Member name TicketDeviceClassOutputDevice</summary>
			public static readonly string TicketDeviceClassOutputDevice = "TicketDeviceClassOutputDevice";
			/// <summary>Member name TicketOutputDevices</summary>
			public static readonly string TicketOutputDevices = "TicketOutputDevices";
			/// <summary>Member name TicketDeviceClasses</summary>
			public static readonly string TicketDeviceClasses = "TicketDeviceClasses";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OutputDeviceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OutputDeviceEntity() :base("OutputDeviceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		public OutputDeviceEntity(System.Int64 outputDeviceID):base("OutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OutputDeviceEntity(System.Int64 outputDeviceID, IPrefetchPath prefetchPathToUse):base("OutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="validator">The custom validator object for this OutputDeviceEntity</param>
		public OutputDeviceEntity(System.Int64 outputDeviceID, IValidator validator):base("OutputDeviceEntity")
		{
			InitClassFetch(outputDeviceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OutputDeviceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_layouts = (VarioSL.Entities.CollectionClasses.LayoutCollection)info.GetValue("_layouts", typeof(VarioSL.Entities.CollectionClasses.LayoutCollection));
			_alwaysFetchLayouts = info.GetBoolean("_alwaysFetchLayouts");
			_alreadyFetchedLayouts = info.GetBoolean("_alreadyFetchedLayouts");

			_ticketDeviceClassOutputDevice = (VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection)info.GetValue("_ticketDeviceClassOutputDevice", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection));
			_alwaysFetchTicketDeviceClassOutputDevice = info.GetBoolean("_alwaysFetchTicketDeviceClassOutputDevice");
			_alreadyFetchedTicketDeviceClassOutputDevice = info.GetBoolean("_alreadyFetchedTicketDeviceClassOutputDevice");

			_ticketOutputDevices = (VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection)info.GetValue("_ticketOutputDevices", typeof(VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection));
			_alwaysFetchTicketOutputDevices = info.GetBoolean("_alwaysFetchTicketOutputDevices");
			_alreadyFetchedTicketOutputDevices = info.GetBoolean("_alreadyFetchedTicketOutputDevices");
			_ticketDeviceClasses = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClasses", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClasses = info.GetBoolean("_alwaysFetchTicketDeviceClasses");
			_alreadyFetchedTicketDeviceClasses = info.GetBoolean("_alreadyFetchedTicketDeviceClasses");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLayouts = (_layouts.Count > 0);
			_alreadyFetchedTicketDeviceClassOutputDevice = (_ticketDeviceClassOutputDevice.Count > 0);
			_alreadyFetchedTicketOutputDevices = (_ticketOutputDevices.Count > 0);
			_alreadyFetchedTicketDeviceClasses = (_ticketDeviceClasses.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Layouts":
					toReturn.Add(Relations.LayoutEntityUsingOutDeviceID);
					break;
				case "TicketDeviceClassOutputDevice":
					toReturn.Add(Relations.TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID);
					break;
				case "TicketOutputDevices":
					toReturn.Add(Relations.TicketOutputdeviceEntityUsingOutputDeviceID);
					break;
				case "TicketDeviceClasses":
					toReturn.Add(Relations.TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID, "OutputDeviceEntity__", "TicketDeviceClassOutputDevice_", JoinHint.None);
					toReturn.Add(TicketDeviceClassOutputDeviceEntity.Relations.TicketDeviceClassEntityUsingTicketDeviceClassID, "TicketDeviceClassOutputDevice_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_layouts", (!this.MarkedForDeletion?_layouts:null));
			info.AddValue("_alwaysFetchLayouts", _alwaysFetchLayouts);
			info.AddValue("_alreadyFetchedLayouts", _alreadyFetchedLayouts);
			info.AddValue("_ticketDeviceClassOutputDevice", (!this.MarkedForDeletion?_ticketDeviceClassOutputDevice:null));
			info.AddValue("_alwaysFetchTicketDeviceClassOutputDevice", _alwaysFetchTicketDeviceClassOutputDevice);
			info.AddValue("_alreadyFetchedTicketDeviceClassOutputDevice", _alreadyFetchedTicketDeviceClassOutputDevice);
			info.AddValue("_ticketOutputDevices", (!this.MarkedForDeletion?_ticketOutputDevices:null));
			info.AddValue("_alwaysFetchTicketOutputDevices", _alwaysFetchTicketOutputDevices);
			info.AddValue("_alreadyFetchedTicketOutputDevices", _alreadyFetchedTicketOutputDevices);
			info.AddValue("_ticketDeviceClasses", (!this.MarkedForDeletion?_ticketDeviceClasses:null));
			info.AddValue("_alwaysFetchTicketDeviceClasses", _alwaysFetchTicketDeviceClasses);
			info.AddValue("_alreadyFetchedTicketDeviceClasses", _alreadyFetchedTicketDeviceClasses);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Layouts":
					_alreadyFetchedLayouts = true;
					if(entity!=null)
					{
						this.Layouts.Add((LayoutEntity)entity);
					}
					break;
				case "TicketDeviceClassOutputDevice":
					_alreadyFetchedTicketDeviceClassOutputDevice = true;
					if(entity!=null)
					{
						this.TicketDeviceClassOutputDevice.Add((TicketDeviceClassOutputDeviceEntity)entity);
					}
					break;
				case "TicketOutputDevices":
					_alreadyFetchedTicketOutputDevices = true;
					if(entity!=null)
					{
						this.TicketOutputDevices.Add((TicketOutputdeviceEntity)entity);
					}
					break;
				case "TicketDeviceClasses":
					_alreadyFetchedTicketDeviceClasses = true;
					if(entity!=null)
					{
						this.TicketDeviceClasses.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Layouts":
					_layouts.Add((LayoutEntity)relatedEntity);
					break;
				case "TicketDeviceClassOutputDevice":
					_ticketDeviceClassOutputDevice.Add((TicketDeviceClassOutputDeviceEntity)relatedEntity);
					break;
				case "TicketOutputDevices":
					_ticketOutputDevices.Add((TicketOutputdeviceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Layouts":
					this.PerformRelatedEntityRemoval(_layouts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClassOutputDevice":
					this.PerformRelatedEntityRemoval(_ticketDeviceClassOutputDevice, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketOutputDevices":
					this.PerformRelatedEntityRemoval(_ticketOutputDevices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_layouts);
			toReturn.Add(_ticketDeviceClassOutputDevice);
			toReturn.Add(_ticketOutputDevices);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID)
		{
			return FetchUsingPK(outputDeviceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(outputDeviceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(outputDeviceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 outputDeviceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(outputDeviceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OutputDeviceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OutputDeviceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLayouts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLayouts || forceFetch || _alwaysFetchLayouts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_layouts);
				_layouts.SuppressClearInGetMulti=!forceFetch;
				_layouts.EntityFactoryToUse = entityFactoryToUse;
				_layouts.GetMultiManyToOne(null, this, null, null, filter);
				_layouts.SuppressClearInGetMulti=false;
				_alreadyFetchedLayouts = true;
			}
			return _layouts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Layouts'. These settings will be taken into account
		/// when the property Layouts is requested or GetMultiLayouts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLayouts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_layouts.SortClauses=sortClauses;
			_layouts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassOutputDeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, _ticketDeviceClassOutputDevice.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassOutputDeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, _ticketDeviceClassOutputDevice.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClassOutputDevice(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection GetMultiTicketDeviceClassOutputDevice(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClassOutputDevice || forceFetch || _alwaysFetchTicketDeviceClassOutputDevice) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClassOutputDevice);
				_ticketDeviceClassOutputDevice.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClassOutputDevice.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClassOutputDevice.GetMultiManyToOne(this, null, filter);
				_ticketDeviceClassOutputDevice.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClassOutputDevice = true;
			}
			return _ticketDeviceClassOutputDevice;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClassOutputDevice'. These settings will be taken into account
		/// when the property TicketDeviceClassOutputDevice is requested or GetMultiTicketDeviceClassOutputDevice is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClassOutputDevice(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClassOutputDevice.SortClauses=sortClauses;
			_ticketDeviceClassOutputDevice.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketOutputdeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch)
		{
			return GetMultiTicketOutputDevices(forceFetch, _ticketOutputDevices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketOutputdeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketOutputDevices(forceFetch, _ticketOutputDevices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketOutputDevices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection GetMultiTicketOutputDevices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketOutputDevices || forceFetch || _alwaysFetchTicketOutputDevices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketOutputDevices);
				_ticketOutputDevices.SuppressClearInGetMulti=!forceFetch;
				_ticketOutputDevices.EntityFactoryToUse = entityFactoryToUse;
				_ticketOutputDevices.GetMultiManyToOne(this, null, filter);
				_ticketOutputDevices.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketOutputDevices = true;
			}
			return _ticketOutputDevices;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketOutputDevices'. These settings will be taken into account
		/// when the property TicketOutputDevices is requested or GetMultiTicketOutputDevices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketOutputDevices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketOutputDevices.SortClauses=sortClauses;
			_ticketOutputDevices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch)
		{
			return GetMultiTicketDeviceClasses(forceFetch, _ticketDeviceClasses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketDeviceClasses || forceFetch || _alwaysFetchTicketDeviceClasses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClasses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OutputDeviceFields.OutputDeviceID, ComparisonOperator.Equal, this.OutputDeviceID, "OutputDeviceEntity__"));
				_ticketDeviceClasses.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClasses.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClasses.GetMulti(filter, GetRelationsForField("TicketDeviceClasses"));
				_ticketDeviceClasses.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClasses = true;
			}
			return _ticketDeviceClasses;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClasses'. These settings will be taken into account
		/// when the property TicketDeviceClasses is requested or GetMultiTicketDeviceClasses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClasses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClasses.SortClauses=sortClauses;
			_ticketDeviceClasses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Layouts", _layouts);
			toReturn.Add("TicketDeviceClassOutputDevice", _ticketDeviceClassOutputDevice);
			toReturn.Add("TicketOutputDevices", _ticketOutputDevices);
			toReturn.Add("TicketDeviceClasses", _ticketDeviceClasses);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="validator">The validator object for this OutputDeviceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 outputDeviceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(outputDeviceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_layouts = new VarioSL.Entities.CollectionClasses.LayoutCollection();
			_layouts.SetContainingEntityInfo(this, "OutputDevice");

			_ticketDeviceClassOutputDevice = new VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection();
			_ticketDeviceClassOutputDevice.SetContainingEntityInfo(this, "OutputDevice");

			_ticketOutputDevices = new VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection();
			_ticketOutputDevices.SetContainingEntityInfo(this, "OutputDevice");
			_ticketDeviceClasses = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendMask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutputDeviceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="outputDeviceID">PK value for OutputDevice which data should be fetched into this OutputDevice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 outputDeviceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OutputDeviceFieldIndex.OutputDeviceID].ForcedCurrentValueWrite(outputDeviceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOutputDeviceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OutputDeviceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OutputDeviceRelations Relations
		{
			get	{ return new OutputDeviceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayouts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layouts")[0], (int)VarioSL.Entities.EntityType.OutputDeviceEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layouts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClassOutputDevice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClassOutputDevice
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClassOutputDevice")[0], (int)VarioSL.Entities.EntityType.OutputDeviceEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity, 0, null, null, null, "TicketDeviceClassOutputDevice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketOutputdevice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketOutputDevices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection(), (IEntityRelation)GetRelationsForField("TicketOutputDevices")[0], (int)VarioSL.Entities.EntityType.OutputDeviceEntity, (int)VarioSL.Entities.EntityType.TicketOutputdeviceEntity, 0, null, null, null, "TicketOutputDevices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClasses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID;
				intermediateRelation.SetAliases(string.Empty, "TicketDeviceClassOutputDevice_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.OutputDeviceEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, GetRelationsForField("TicketDeviceClasses"), "TicketDeviceClasses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity OutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_OUTPUTDEVICE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)OutputDeviceFieldIndex.Description, true); }
			set	{ SetValue((int)OutputDeviceFieldIndex.Description, value, true); }
		}

		/// <summary> The EvendMask property of the Entity OutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_OUTPUTDEVICE"."EVENDMASK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendMask
		{
			get { return (Nullable<System.Int64>)GetValue((int)OutputDeviceFieldIndex.EvendMask, false); }
			set	{ SetValue((int)OutputDeviceFieldIndex.EvendMask, value, true); }
		}

		/// <summary> The Name property of the Entity OutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_OUTPUTDEVICE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)OutputDeviceFieldIndex.Name, true); }
			set	{ SetValue((int)OutputDeviceFieldIndex.Name, value, true); }
		}

		/// <summary> The OutputDeviceID property of the Entity OutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_OUTPUTDEVICE"."OUTPUTDEVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 OutputDeviceID
		{
			get { return (System.Int64)GetValue((int)OutputDeviceFieldIndex.OutputDeviceID, true); }
			set	{ SetValue((int)OutputDeviceFieldIndex.OutputDeviceID, value, true); }
		}

		/// <summary> The Visible property of the Entity OutputDevice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_OUTPUTDEVICE"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Visible
		{
			get { return (Nullable<System.Int16>)GetValue((int)OutputDeviceFieldIndex.Visible, false); }
			set	{ SetValue((int)OutputDeviceFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLayouts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection Layouts
		{
			get	{ return GetMultiLayouts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Layouts. When set to true, Layouts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layouts is accessed. You can always execute/ a forced fetch by calling GetMultiLayouts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayouts
		{
			get	{ return _alwaysFetchLayouts; }
			set	{ _alwaysFetchLayouts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layouts already has been fetched. Setting this property to false when Layouts has been fetched
		/// will clear the Layouts collection well. Setting this property to true while Layouts hasn't been fetched disables lazy loading for Layouts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayouts
		{
			get { return _alreadyFetchedLayouts;}
			set 
			{
				if(_alreadyFetchedLayouts && !value && (_layouts != null))
				{
					_layouts.Clear();
				}
				_alreadyFetchedLayouts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassOutputDeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClassOutputDevice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassOutputDeviceCollection TicketDeviceClassOutputDevice
		{
			get	{ return GetMultiTicketDeviceClassOutputDevice(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClassOutputDevice. When set to true, TicketDeviceClassOutputDevice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClassOutputDevice is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClassOutputDevice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClassOutputDevice
		{
			get	{ return _alwaysFetchTicketDeviceClassOutputDevice; }
			set	{ _alwaysFetchTicketDeviceClassOutputDevice = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClassOutputDevice already has been fetched. Setting this property to false when TicketDeviceClassOutputDevice has been fetched
		/// will clear the TicketDeviceClassOutputDevice collection well. Setting this property to true while TicketDeviceClassOutputDevice hasn't been fetched disables lazy loading for TicketDeviceClassOutputDevice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClassOutputDevice
		{
			get { return _alreadyFetchedTicketDeviceClassOutputDevice;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClassOutputDevice && !value && (_ticketDeviceClassOutputDevice != null))
				{
					_ticketDeviceClassOutputDevice.Clear();
				}
				_alreadyFetchedTicketDeviceClassOutputDevice = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketOutputdeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketOutputDevices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketOutputdeviceCollection TicketOutputDevices
		{
			get	{ return GetMultiTicketOutputDevices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketOutputDevices. When set to true, TicketOutputDevices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketOutputDevices is accessed. You can always execute/ a forced fetch by calling GetMultiTicketOutputDevices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketOutputDevices
		{
			get	{ return _alwaysFetchTicketOutputDevices; }
			set	{ _alwaysFetchTicketOutputDevices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketOutputDevices already has been fetched. Setting this property to false when TicketOutputDevices has been fetched
		/// will clear the TicketOutputDevices collection well. Setting this property to true while TicketOutputDevices hasn't been fetched disables lazy loading for TicketOutputDevices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketOutputDevices
		{
			get { return _alreadyFetchedTicketOutputDevices;}
			set 
			{
				if(_alreadyFetchedTicketOutputDevices && !value && (_ticketOutputDevices != null))
				{
					_ticketOutputDevices.Clear();
				}
				_alreadyFetchedTicketOutputDevices = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClasses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClasses
		{
			get { return GetMultiTicketDeviceClasses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClasses. When set to true, TicketDeviceClasses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClasses is accessed. You can always execute a forced fetch by calling GetMultiTicketDeviceClasses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClasses
		{
			get	{ return _alwaysFetchTicketDeviceClasses; }
			set	{ _alwaysFetchTicketDeviceClasses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClasses already has been fetched. Setting this property to false when TicketDeviceClasses has been fetched
		/// will clear the TicketDeviceClasses collection well. Setting this property to true while TicketDeviceClasses hasn't been fetched disables lazy loading for TicketDeviceClasses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClasses
		{
			get { return _alreadyFetchedTicketDeviceClasses;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClasses && !value && (_ticketDeviceClasses != null))
				{
					_ticketDeviceClasses.Clear();
				}
				_alreadyFetchedTicketDeviceClasses = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OutputDeviceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
