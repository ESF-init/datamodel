﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AbortedTransaction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AbortedTransactionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private RevenueTransactionTypeEntity _revenueTransactionType;
		private bool	_alwaysFetchRevenueTransactionType, _alreadyFetchedRevenueTransactionType, _revenueTransactionTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RevenueTransactionType</summary>
			public static readonly string RevenueTransactionType = "RevenueTransactionType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AbortedTransactionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AbortedTransactionEntity() :base("AbortedTransactionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		public AbortedTransactionEntity(System.Int64 transactionID):base("AbortedTransactionEntity")
		{
			InitClassFetch(transactionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AbortedTransactionEntity(System.Int64 transactionID, IPrefetchPath prefetchPathToUse):base("AbortedTransactionEntity")
		{
			InitClassFetch(transactionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="validator">The custom validator object for this AbortedTransactionEntity</param>
		public AbortedTransactionEntity(System.Int64 transactionID, IValidator validator):base("AbortedTransactionEntity")
		{
			InitClassFetch(transactionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AbortedTransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_revenueTransactionType = (RevenueTransactionTypeEntity)info.GetValue("_revenueTransactionType", typeof(RevenueTransactionTypeEntity));
			if(_revenueTransactionType!=null)
			{
				_revenueTransactionType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueTransactionTypeReturnsNewIfNotFound = info.GetBoolean("_revenueTransactionTypeReturnsNewIfNotFound");
			_alwaysFetchRevenueTransactionType = info.GetBoolean("_alwaysFetchRevenueTransactionType");
			_alreadyFetchedRevenueTransactionType = info.GetBoolean("_alreadyFetchedRevenueTransactionType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AbortedTransactionFieldIndex)fieldIndex)
			{
				case AbortedTransactionFieldIndex.TypeID:
					DesetupSyncRevenueTransactionType(true, false);
					_alreadyFetchedRevenueTransactionType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRevenueTransactionType = (_revenueTransactionType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RevenueTransactionType":
					toReturn.Add(Relations.RevenueTransactionTypeEntityUsingTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_revenueTransactionType", (!this.MarkedForDeletion?_revenueTransactionType:null));
			info.AddValue("_revenueTransactionTypeReturnsNewIfNotFound", _revenueTransactionTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueTransactionType", _alwaysFetchRevenueTransactionType);
			info.AddValue("_alreadyFetchedRevenueTransactionType", _alreadyFetchedRevenueTransactionType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RevenueTransactionType":
					_alreadyFetchedRevenueTransactionType = true;
					this.RevenueTransactionType = (RevenueTransactionTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RevenueTransactionType":
					SetupSyncRevenueTransactionType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RevenueTransactionType":
					DesetupSyncRevenueTransactionType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_revenueTransactionType!=null)
			{
				toReturn.Add(_revenueTransactionType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID)
		{
			return FetchUsingPK(transactionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(transactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TransactionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AbortedTransactionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public RevenueTransactionTypeEntity GetSingleRevenueTransactionType()
		{
			return GetSingleRevenueTransactionType(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueTransactionTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueTransactionTypeEntity' which is related to this entity.</returns>
		public virtual RevenueTransactionTypeEntity GetSingleRevenueTransactionType(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueTransactionType || forceFetch || _alwaysFetchRevenueTransactionType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueTransactionTypeEntityUsingTypeID);
				RevenueTransactionTypeEntity newEntity = new RevenueTransactionTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TypeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RevenueTransactionTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueTransactionTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueTransactionType = newEntity;
				_alreadyFetchedRevenueTransactionType = fetchResult;
			}
			return _revenueTransactionType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RevenueTransactionType", _revenueTransactionType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="validator">The validator object for this AbortedTransactionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 transactionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(transactionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_revenueTransactionTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardIssuerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardReferenceTransactionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardTransactionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceBookingState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePaymentMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImportDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayCardNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Saleschannel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInstanceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TikNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripSerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueOfRide", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _revenueTransactionType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueTransactionType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueTransactionType, new PropertyChangedEventHandler( OnRevenueTransactionTypePropertyChanged ), "RevenueTransactionType", VarioSL.Entities.RelationClasses.StaticAbortedTransactionRelations.RevenueTransactionTypeEntityUsingTypeIDStatic, true, signalRelatedEntity, "AbortedTransactions", resetFKFields, new int[] { (int)AbortedTransactionFieldIndex.TypeID } );		
			_revenueTransactionType = null;
		}
		
		/// <summary> setups the sync logic for member _revenueTransactionType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueTransactionType(IEntityCore relatedEntity)
		{
			if(_revenueTransactionType!=relatedEntity)
			{		
				DesetupSyncRevenueTransactionType(true, true);
				_revenueTransactionType = (RevenueTransactionTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueTransactionType, new PropertyChangedEventHandler( OnRevenueTransactionTypePropertyChanged ), "RevenueTransactionType", VarioSL.Entities.RelationClasses.StaticAbortedTransactionRelations.RevenueTransactionTypeEntityUsingTypeIDStatic, true, ref _alreadyFetchedRevenueTransactionType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueTransactionTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="transactionID">PK value for AbortedTransaction which data should be fetched into this AbortedTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AbortedTransactionFieldIndex.TransactionID].ForcedCurrentValueWrite(transactionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAbortedTransactionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AbortedTransactionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AbortedTransactionRelations Relations
		{
			get	{ return new AbortedTransactionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueTransactionType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueTransactionType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueTransactionTypeCollection(), (IEntityRelation)GetRelationsForField("RevenueTransactionType")[0], (int)VarioSL.Entities.EntityType.AbortedTransactionEntity, (int)VarioSL.Entities.EntityType.RevenueTransactionTypeEntity, 0, null, null, null, "RevenueTransactionType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CancellationID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CANCELLATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.CancellationID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CancellationID, value, true); }
		}

		/// <summary> The CardBalance property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CARDBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardBalance
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.CardBalance, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CardBalance, value, true); }
		}

		/// <summary> The CardID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)AbortedTransactionFieldIndex.CardID, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardIssuerID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CARDISSUERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardIssuerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.CardIssuerID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CardIssuerID, value, true); }
		}

		/// <summary> The CardReferenceTransactionNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CARDREFTRANSACTIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardReferenceTransactionNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.CardReferenceTransactionNumber, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CardReferenceTransactionNumber, value, true); }
		}

		/// <summary> The CardTransactionNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CARDTRANSACTIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardTransactionNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.CardTransactionNumber, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.CardTransactionNumber, value, true); }
		}

		/// <summary> The ClientID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ClientID
		{
			get { return (System.Int32)GetValue((int)AbortedTransactionFieldIndex.ClientID, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DeviceBookingState property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."DEVICEBOOKINGSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> DeviceBookingState
		{
			get { return (Nullable<System.Int16>)GetValue((int)AbortedTransactionFieldIndex.DeviceBookingState, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.DeviceBookingState, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DeviceNumber
		{
			get { return (System.Int32)GetValue((int)AbortedTransactionFieldIndex.DeviceNumber, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The DevicePaymentMethod property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."DEVICEPAYMENTMETHOD"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.DevicePaymentMethod> DevicePaymentMethod
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.DevicePaymentMethod>)GetValue((int)AbortedTransactionFieldIndex.DevicePaymentMethod, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.DevicePaymentMethod, value, true); }
		}

		/// <summary> The ImportDateTime property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."IMPORTDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ImportDateTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AbortedTransactionFieldIndex.ImportDateTime, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.ImportDateTime, value, true); }
		}

		/// <summary> The Line property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."LINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Line
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.Line, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.Line, value, true); }
		}

		/// <summary> The LineName property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."LINENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineName
		{
			get { return (System.String)GetValue((int)AbortedTransactionFieldIndex.LineName, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.LineName, value, true); }
		}

		/// <summary> The PayCardNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."PAYCARDNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PayCardNumber
		{
			get { return (System.String)GetValue((int)AbortedTransactionFieldIndex.PayCardNumber, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.PayCardNumber, value, true); }
		}

		/// <summary> The Price property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."PRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Price
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.Price, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.Price, value, true); }
		}

		/// <summary> The RouteNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."ROUTENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RouteNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.RouteNumber, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.RouteNumber, value, true); }
		}

		/// <summary> The Saleschannel property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."SALESCHANNEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Saleschannel
		{
			get { return (System.Int16)GetValue((int)AbortedTransactionFieldIndex.Saleschannel, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.Saleschannel, value, true); }
		}

		/// <summary> The ShiftID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."SHIFTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ShiftID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.ShiftID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.ShiftID, value, true); }
		}

		/// <summary> The StopFrom property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."STOPFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopFrom
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.StopFrom, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.StopFrom, value, true); }
		}

		/// <summary> The StopTo property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."STOPTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopTo
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.StopTo, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.StopTo, value, true); }
		}

		/// <summary> The TariffID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.TariffID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TicketInstanceID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TICKETINSTANCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInstanceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.TicketInstanceID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TicketInstanceID, value, true); }
		}

		/// <summary> The TikNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TIKNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TikNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.TikNumber, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TikNumber, value, true); }
		}

		/// <summary> The TransactionID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TransactionID
		{
			get { return (System.Int64)GetValue((int)AbortedTransactionFieldIndex.TransactionID, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The TripDateTime property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TRIPDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TripDateTime
		{
			get { return (System.DateTime)GetValue((int)AbortedTransactionFieldIndex.TripDateTime, true); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TripDateTime, value, true); }
		}

		/// <summary> The TripSerialNumber property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TRIPSERIALNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripSerialNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AbortedTransactionFieldIndex.TripSerialNumber, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TripSerialNumber, value, true); }
		}

		/// <summary> The TypeID property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."TYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.TypeID, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.TypeID, value, true); }
		}

		/// <summary> The ValueOfRide property of the Entity AbortedTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_ABORTEDTRANSACTION"."VALUEOFRIDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValueOfRide
		{
			get { return (Nullable<System.Int64>)GetValue((int)AbortedTransactionFieldIndex.ValueOfRide, false); }
			set	{ SetValue((int)AbortedTransactionFieldIndex.ValueOfRide, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'RevenueTransactionTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueTransactionType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueTransactionTypeEntity RevenueTransactionType
		{
			get	{ return GetSingleRevenueTransactionType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueTransactionType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AbortedTransactions", "RevenueTransactionType", _revenueTransactionType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueTransactionType. When set to true, RevenueTransactionType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueTransactionType is accessed. You can always execute a forced fetch by calling GetSingleRevenueTransactionType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueTransactionType
		{
			get	{ return _alwaysFetchRevenueTransactionType; }
			set	{ _alwaysFetchRevenueTransactionType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueTransactionType already has been fetched. Setting this property to false when RevenueTransactionType has been fetched
		/// will set RevenueTransactionType to null as well. Setting this property to true while RevenueTransactionType hasn't been fetched disables lazy loading for RevenueTransactionType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueTransactionType
		{
			get { return _alreadyFetchedRevenueTransactionType;}
			set 
			{
				if(_alreadyFetchedRevenueTransactionType && !value)
				{
					this.RevenueTransactionType = null;
				}
				_alreadyFetchedRevenueTransactionType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueTransactionType is not found
		/// in the database. When set to true, RevenueTransactionType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueTransactionTypeReturnsNewIfNotFound
		{
			get	{ return _revenueTransactionTypeReturnsNewIfNotFound; }
			set { _revenueTransactionTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AbortedTransactionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
