﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VarioSettlement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VarioSettlementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection	_settlementRunValueToVarioSettlements;
		private bool	_alwaysFetchSettlementRunValueToVarioSettlements, _alreadyFetchedSettlementRunValueToVarioSettlements;
		private VarioSL.Entities.CollectionClasses.SettlementRunValueCollection _settlementRunValues;
		private bool	_alwaysFetchSettlementRunValues, _alreadyFetchedSettlementRunValues;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DepotEntity _depot;
		private bool	_alwaysFetchDepot, _alreadyFetchedDepot, _depotReturnsNewIfNotFound;
		private UserListEntity _user;
		private bool	_alwaysFetchUser, _alreadyFetchedUser, _userReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Depot</summary>
			public static readonly string Depot = "Depot";
			/// <summary>Member name User</summary>
			public static readonly string User = "User";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name SettlementRunValueToVarioSettlements</summary>
			public static readonly string SettlementRunValueToVarioSettlements = "SettlementRunValueToVarioSettlements";
			/// <summary>Member name SettlementRunValues</summary>
			public static readonly string SettlementRunValues = "SettlementRunValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VarioSettlementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VarioSettlementEntity() :base("VarioSettlementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		public VarioSettlementEntity(System.Int64 iD):base("VarioSettlementEntity")
		{
			InitClassFetch(iD, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VarioSettlementEntity(System.Int64 iD, IPrefetchPath prefetchPathToUse):base("VarioSettlementEntity")
		{
			InitClassFetch(iD, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="validator">The custom validator object for this VarioSettlementEntity</param>
		public VarioSettlementEntity(System.Int64 iD, IValidator validator):base("VarioSettlementEntity")
		{
			InitClassFetch(iD, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VarioSettlementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_settlementRunValueToVarioSettlements = (VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection)info.GetValue("_settlementRunValueToVarioSettlements", typeof(VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection));
			_alwaysFetchSettlementRunValueToVarioSettlements = info.GetBoolean("_alwaysFetchSettlementRunValueToVarioSettlements");
			_alreadyFetchedSettlementRunValueToVarioSettlements = info.GetBoolean("_alreadyFetchedSettlementRunValueToVarioSettlements");
			_settlementRunValues = (VarioSL.Entities.CollectionClasses.SettlementRunValueCollection)info.GetValue("_settlementRunValues", typeof(VarioSL.Entities.CollectionClasses.SettlementRunValueCollection));
			_alwaysFetchSettlementRunValues = info.GetBoolean("_alwaysFetchSettlementRunValues");
			_alreadyFetchedSettlementRunValues = info.GetBoolean("_alreadyFetchedSettlementRunValues");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_depot = (DepotEntity)info.GetValue("_depot", typeof(DepotEntity));
			if(_depot!=null)
			{
				_depot.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_depotReturnsNewIfNotFound = info.GetBoolean("_depotReturnsNewIfNotFound");
			_alwaysFetchDepot = info.GetBoolean("_alwaysFetchDepot");
			_alreadyFetchedDepot = info.GetBoolean("_alreadyFetchedDepot");

			_user = (UserListEntity)info.GetValue("_user", typeof(UserListEntity));
			if(_user!=null)
			{
				_user.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userReturnsNewIfNotFound = info.GetBoolean("_userReturnsNewIfNotFound");
			_alwaysFetchUser = info.GetBoolean("_alwaysFetchUser");
			_alreadyFetchedUser = info.GetBoolean("_alreadyFetchedUser");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VarioSettlementFieldIndex)fieldIndex)
			{
				case VarioSettlementFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case VarioSettlementFieldIndex.DepotID:
					DesetupSyncDepot(true, false);
					_alreadyFetchedDepot = false;
					break;
				case VarioSettlementFieldIndex.UserID:
					DesetupSyncUser(true, false);
					_alreadyFetchedUser = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedSettlementRunValueToVarioSettlements = (_settlementRunValueToVarioSettlements.Count > 0);
			_alreadyFetchedSettlementRunValues = (_settlementRunValues.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDepot = (_depot != null);
			_alreadyFetchedUser = (_user != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Depot":
					toReturn.Add(Relations.DepotEntityUsingDepotID);
					break;
				case "User":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingAccountSettlementId);
					break;
				case "SettlementRunValueToVarioSettlements":
					toReturn.Add(Relations.SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID);
					break;
				case "SettlementRunValues":
					toReturn.Add(Relations.SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID, "VarioSettlementEntity__", "SettlementRunValueToVarioSettlement_", JoinHint.None);
					toReturn.Add(SettlementRunValueToVarioSettlementEntity.Relations.SettlementRunValueEntityUsingSettlementRunValueID, "SettlementRunValueToVarioSettlement_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_settlementRunValueToVarioSettlements", (!this.MarkedForDeletion?_settlementRunValueToVarioSettlements:null));
			info.AddValue("_alwaysFetchSettlementRunValueToVarioSettlements", _alwaysFetchSettlementRunValueToVarioSettlements);
			info.AddValue("_alreadyFetchedSettlementRunValueToVarioSettlements", _alreadyFetchedSettlementRunValueToVarioSettlements);
			info.AddValue("_settlementRunValues", (!this.MarkedForDeletion?_settlementRunValues:null));
			info.AddValue("_alwaysFetchSettlementRunValues", _alwaysFetchSettlementRunValues);
			info.AddValue("_alreadyFetchedSettlementRunValues", _alreadyFetchedSettlementRunValues);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_depot", (!this.MarkedForDeletion?_depot:null));
			info.AddValue("_depotReturnsNewIfNotFound", _depotReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDepot", _alwaysFetchDepot);
			info.AddValue("_alreadyFetchedDepot", _alreadyFetchedDepot);
			info.AddValue("_user", (!this.MarkedForDeletion?_user:null));
			info.AddValue("_userReturnsNewIfNotFound", _userReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUser", _alwaysFetchUser);
			info.AddValue("_alreadyFetchedUser", _alreadyFetchedUser);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Depot":
					_alreadyFetchedDepot = true;
					this.Depot = (DepotEntity)entity;
					break;
				case "User":
					_alreadyFetchedUser = true;
					this.User = (UserListEntity)entity;
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "SettlementRunValueToVarioSettlements":
					_alreadyFetchedSettlementRunValueToVarioSettlements = true;
					if(entity!=null)
					{
						this.SettlementRunValueToVarioSettlements.Add((SettlementRunValueToVarioSettlementEntity)entity);
					}
					break;
				case "SettlementRunValues":
					_alreadyFetchedSettlementRunValues = true;
					if(entity!=null)
					{
						this.SettlementRunValues.Add((SettlementRunValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Depot":
					SetupSyncDepot(relatedEntity);
					break;
				case "User":
					SetupSyncUser(relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				case "SettlementRunValueToVarioSettlements":
					_settlementRunValueToVarioSettlements.Add((SettlementRunValueToVarioSettlementEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Depot":
					DesetupSyncDepot(false, true);
					break;
				case "User":
					DesetupSyncUser(false, true);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementRunValueToVarioSettlements":
					this.PerformRelatedEntityRemoval(_settlementRunValueToVarioSettlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_depot!=null)
			{
				toReturn.Add(_depot);
			}
			if(_user!=null)
			{
				toReturn.Add(_user);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_postings);
			toReturn.Add(_settlementRunValueToVarioSettlements);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 iD)
		{
			return FetchUsingPK(iD, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 iD, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(iD, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 iD, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(iD, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 iD, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(iD, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VarioSettlementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(null, this, null, null, null, null, null, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueToVarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, _settlementRunValueToVarioSettlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueToVarioSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, _settlementRunValueToVarioSettlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementRunValueToVarioSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection GetMultiSettlementRunValueToVarioSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementRunValueToVarioSettlements || forceFetch || _alwaysFetchSettlementRunValueToVarioSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementRunValueToVarioSettlements);
				_settlementRunValueToVarioSettlements.SuppressClearInGetMulti=!forceFetch;
				_settlementRunValueToVarioSettlements.EntityFactoryToUse = entityFactoryToUse;
				_settlementRunValueToVarioSettlements.GetMultiManyToOne(this, null, filter);
				_settlementRunValueToVarioSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementRunValueToVarioSettlements = true;
			}
			return _settlementRunValueToVarioSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementRunValueToVarioSettlements'. These settings will be taken into account
		/// when the property SettlementRunValueToVarioSettlements is requested or GetMultiSettlementRunValueToVarioSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementRunValueToVarioSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementRunValueToVarioSettlements.SortClauses=sortClauses;
			_settlementRunValueToVarioSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch)
		{
			return GetMultiSettlementRunValues(forceFetch, _settlementRunValues.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunValueCollection GetMultiSettlementRunValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSettlementRunValues || forceFetch || _alwaysFetchSettlementRunValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementRunValues);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(VarioSettlementFields.ID, ComparisonOperator.Equal, this.ID, "VarioSettlementEntity__"));
				_settlementRunValues.SuppressClearInGetMulti=!forceFetch;
				_settlementRunValues.EntityFactoryToUse = entityFactoryToUse;
				_settlementRunValues.GetMulti(filter, GetRelationsForField("SettlementRunValues"));
				_settlementRunValues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementRunValues = true;
			}
			return _settlementRunValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementRunValues'. These settings will be taken into account
		/// when the property SettlementRunValues is requested or GetMultiSettlementRunValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementRunValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementRunValues.SortClauses=sortClauses;
			_settlementRunValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public DepotEntity GetSingleDepot()
		{
			return GetSingleDepot(false);
		}

		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public virtual DepotEntity GetSingleDepot(bool forceFetch)
		{
			if( ( !_alreadyFetchedDepot || forceFetch || _alwaysFetchDepot) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DepotEntityUsingDepotID);
				DepotEntity newEntity = new DepotEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DepotID);
				}
				if(fetchResult)
				{
					newEntity = (DepotEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_depotReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Depot = newEntity;
				_alreadyFetchedDepot = fetchResult;
			}
			return _depot;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUser()
		{
			return GetSingleUser(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUser(bool forceFetch)
		{
			if( ( !_alreadyFetchedUser || forceFetch || _alwaysFetchUser) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.User = newEntity;
				_alreadyFetchedUser = fetchResult;
			}
			return _user;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Depot", _depot);
			toReturn.Add("User", _user);
			toReturn.Add("Postings", _postings);
			toReturn.Add("SettlementRunValueToVarioSettlements", _settlementRunValueToVarioSettlements);
			toReturn.Add("SettlementRunValues", _settlementRunValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="validator">The validator object for this VarioSettlementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 iD, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(iD, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "VarioSettlement");

			_settlementRunValueToVarioSettlements = new VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection();
			_settlementRunValueToVarioSettlements.SetContainingEntityInfo(this, "VarioSettlement");
			_settlementRunValues = new VarioSL.Entities.CollectionClasses.SettlementRunValueCollection();
			_clientReturnsNewIfNotFound = false;
			_depotReturnsNewIfNotFound = false;
			_userReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BillMonth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecordCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfClosing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfSettlement", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Settlements", resetFKFields, new int[] { (int)VarioSettlementFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _depot</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDepot(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.DepotEntityUsingDepotIDStatic, true, signalRelatedEntity, "Settlements", resetFKFields, new int[] { (int)VarioSettlementFieldIndex.DepotID } );		
			_depot = null;
		}
		
		/// <summary> setups the sync logic for member _depot</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDepot(IEntityCore relatedEntity)
		{
			if(_depot!=relatedEntity)
			{		
				DesetupSyncDepot(true, true);
				_depot = (DepotEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.DepotEntityUsingDepotIDStatic, true, ref _alreadyFetchedDepot, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDepotPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _user</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUser(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "Settlements", resetFKFields, new int[] { (int)VarioSettlementFieldIndex.UserID } );		
			_user = null;
		}
		
		/// <summary> setups the sync logic for member _user</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUser(IEntityCore relatedEntity)
		{
			if(_user!=relatedEntity)
			{		
				DesetupSyncUser(true, true);
				_user = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _user, new PropertyChangedEventHandler( OnUserPropertyChanged ), "User", VarioSL.Entities.RelationClasses.StaticVarioSettlementRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUser, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="iD">PK value for VarioSettlement which data should be fetched into this VarioSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 iD, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VarioSettlementFieldIndex.ID].ForcedCurrentValueWrite(iD);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVarioSettlementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VarioSettlementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VarioSettlementRelations Relations
		{
			get	{ return new VarioSettlementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRunValueToVarioSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRunValueToVarioSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection(), (IEntityRelation)GetRelationsForField("SettlementRunValueToVarioSettlements")[0], (int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity, 0, null, null, null, "SettlementRunValueToVarioSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRunValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRunValues
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID;
				intermediateRelation.SetAliases(string.Empty, "SettlementRunValueToVarioSettlement_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunValueCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.SettlementRunValueEntity, 0, null, null, GetRelationsForField("SettlementRunValues"), "SettlementRunValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDepot
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("Depot")[0], (int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "Depot", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUser
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("User")[0], (int)VarioSL.Entities.EntityType.VarioSettlementEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "User", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BillMonth property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."BILLMONTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BillMonth
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VarioSettlementFieldIndex.BillMonth, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.BillMonth, value, true); }
		}

		/// <summary> The CashID property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."CASHID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VarioSettlementFieldIndex.CashID, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.CashID, value, true); }
		}

		/// <summary> The ClientID property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)VarioSettlementFieldIndex.ClientID, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreationDate
		{
			get { return (System.DateTime)GetValue((int)VarioSettlementFieldIndex.CreationDate, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The DepotID property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DepotID
		{
			get { return (System.Int64)GetValue((int)VarioSettlementFieldIndex.DepotID, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.DepotID, value, true); }
		}

		/// <summary> The ExportFile property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."EXPORTFILE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 999<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExportFile
		{
			get { return (System.String)GetValue((int)VarioSettlementFieldIndex.ExportFile, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.ExportFile, value, true); }
		}

		/// <summary> The ID property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ID
		{
			get { return (System.Int64)GetValue((int)VarioSettlementFieldIndex.ID, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.ID, value, true); }
		}

		/// <summary> The RecordCount property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."RECORDCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RecordCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)VarioSettlementFieldIndex.RecordCount, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.RecordCount, value, true); }
		}

		/// <summary> The SettlementNumber property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."SETTLEMENTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SettlementNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)VarioSettlementFieldIndex.SettlementNumber, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.SettlementNumber, value, true); }
		}

		/// <summary> The State property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.VarioSettlementState State
		{
			get { return (VarioSL.Entities.Enumerations.VarioSettlementState)GetValue((int)VarioSettlementFieldIndex.State, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.State, value, true); }
		}

		/// <summary> The TypeOfClosing property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."TYPEOFCLOSING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TypeOfClosing
		{
			get { return (Nullable<System.Int32>)GetValue((int)VarioSettlementFieldIndex.TypeOfClosing, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.TypeOfClosing, value, true); }
		}

		/// <summary> The TypeOfSettlement property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."TYPEOFSETTLEMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.VarioTypeOfSettlement TypeOfSettlement
		{
			get { return (VarioSL.Entities.Enumerations.VarioTypeOfSettlement)GetValue((int)VarioSettlementFieldIndex.TypeOfSettlement, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.TypeOfSettlement, value, true); }
		}

		/// <summary> The UserID property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VarioSettlementFieldIndex.UserID, false); }
			set	{ SetValue((int)VarioSettlementFieldIndex.UserID, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)VarioSettlementFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity VarioSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_SETTLEMENT"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidUntil
		{
			get { return (System.DateTime)GetValue((int)VarioSettlementFieldIndex.ValidUntil, true); }
			set	{ SetValue((int)VarioSettlementFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementRunValueToVarioSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementRunValueToVarioSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueToVarioSettlementCollection SettlementRunValueToVarioSettlements
		{
			get	{ return GetMultiSettlementRunValueToVarioSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRunValueToVarioSettlements. When set to true, SettlementRunValueToVarioSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRunValueToVarioSettlements is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementRunValueToVarioSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRunValueToVarioSettlements
		{
			get	{ return _alwaysFetchSettlementRunValueToVarioSettlements; }
			set	{ _alwaysFetchSettlementRunValueToVarioSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRunValueToVarioSettlements already has been fetched. Setting this property to false when SettlementRunValueToVarioSettlements has been fetched
		/// will clear the SettlementRunValueToVarioSettlements collection well. Setting this property to true while SettlementRunValueToVarioSettlements hasn't been fetched disables lazy loading for SettlementRunValueToVarioSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRunValueToVarioSettlements
		{
			get { return _alreadyFetchedSettlementRunValueToVarioSettlements;}
			set 
			{
				if(_alreadyFetchedSettlementRunValueToVarioSettlements && !value && (_settlementRunValueToVarioSettlements != null))
				{
					_settlementRunValueToVarioSettlements.Clear();
				}
				_alreadyFetchedSettlementRunValueToVarioSettlements = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunValueEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementRunValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunValueCollection SettlementRunValues
		{
			get { return GetMultiSettlementRunValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRunValues. When set to true, SettlementRunValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRunValues is accessed. You can always execute a forced fetch by calling GetMultiSettlementRunValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRunValues
		{
			get	{ return _alwaysFetchSettlementRunValues; }
			set	{ _alwaysFetchSettlementRunValues = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRunValues already has been fetched. Setting this property to false when SettlementRunValues has been fetched
		/// will clear the SettlementRunValues collection well. Setting this property to true while SettlementRunValues hasn't been fetched disables lazy loading for SettlementRunValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRunValues
		{
			get { return _alreadyFetchedSettlementRunValues;}
			set 
			{
				if(_alreadyFetchedSettlementRunValues && !value && (_settlementRunValues != null))
				{
					_settlementRunValues.Clear();
				}
				_alreadyFetchedSettlementRunValues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Settlements", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DepotEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDepot()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DepotEntity Depot
		{
			get	{ return GetSingleDepot(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDepot(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Settlements", "Depot", _depot, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Depot. When set to true, Depot is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Depot is accessed. You can always execute a forced fetch by calling GetSingleDepot(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDepot
		{
			get	{ return _alwaysFetchDepot; }
			set	{ _alwaysFetchDepot = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Depot already has been fetched. Setting this property to false when Depot has been fetched
		/// will set Depot to null as well. Setting this property to true while Depot hasn't been fetched disables lazy loading for Depot</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDepot
		{
			get { return _alreadyFetchedDepot;}
			set 
			{
				if(_alreadyFetchedDepot && !value)
				{
					this.Depot = null;
				}
				_alreadyFetchedDepot = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Depot is not found
		/// in the database. When set to true, Depot will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DepotReturnsNewIfNotFound
		{
			get	{ return _depotReturnsNewIfNotFound; }
			set { _depotReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUser()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity User
		{
			get	{ return GetSingleUser(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUser(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Settlements", "User", _user, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for User. When set to true, User is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time User is accessed. You can always execute a forced fetch by calling GetSingleUser(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUser
		{
			get	{ return _alwaysFetchUser; }
			set	{ _alwaysFetchUser = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property User already has been fetched. Setting this property to false when User has been fetched
		/// will set User to null as well. Setting this property to true while User hasn't been fetched disables lazy loading for User</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUser
		{
			get { return _alreadyFetchedUser;}
			set 
			{
				if(_alreadyFetchedUser && !value)
				{
					this.User = null;
				}
				_alreadyFetchedUser = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property User is not found
		/// in the database. When set to true, User will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserReturnsNewIfNotFound
		{
			get	{ return _userReturnsNewIfNotFound; }
			set { _userReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VarioSettlementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
