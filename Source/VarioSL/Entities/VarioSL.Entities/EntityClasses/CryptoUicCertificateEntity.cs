﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CryptoUicCertificate'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CryptoUicCertificateEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CryptoCertificateEntity _cryptoCertificate;
		private bool	_alwaysFetchCryptoCertificate, _alreadyFetchedCryptoCertificate, _cryptoCertificateReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CryptoCertificate</summary>
			public static readonly string CryptoCertificate = "CryptoCertificate";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CryptoUicCertificateEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CryptoUicCertificateEntity() :base("CryptoUicCertificateEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		public CryptoUicCertificateEntity(System.Int64 uicCertificateID):base("CryptoUicCertificateEntity")
		{
			InitClassFetch(uicCertificateID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CryptoUicCertificateEntity(System.Int64 uicCertificateID, IPrefetchPath prefetchPathToUse):base("CryptoUicCertificateEntity")
		{
			InitClassFetch(uicCertificateID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="validator">The custom validator object for this CryptoUicCertificateEntity</param>
		public CryptoUicCertificateEntity(System.Int64 uicCertificateID, IValidator validator):base("CryptoUicCertificateEntity")
		{
			InitClassFetch(uicCertificateID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CryptoUicCertificateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cryptoCertificate = (CryptoCertificateEntity)info.GetValue("_cryptoCertificate", typeof(CryptoCertificateEntity));
			if(_cryptoCertificate!=null)
			{
				_cryptoCertificate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cryptoCertificateReturnsNewIfNotFound = info.GetBoolean("_cryptoCertificateReturnsNewIfNotFound");
			_alwaysFetchCryptoCertificate = info.GetBoolean("_alwaysFetchCryptoCertificate");
			_alreadyFetchedCryptoCertificate = info.GetBoolean("_alreadyFetchedCryptoCertificate");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CryptoUicCertificateFieldIndex)fieldIndex)
			{
				case CryptoUicCertificateFieldIndex.CertificateID:
					DesetupSyncCryptoCertificate(true, false);
					_alreadyFetchedCryptoCertificate = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCryptoCertificate = (_cryptoCertificate != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CryptoCertificate":
					toReturn.Add(Relations.CryptoCertificateEntityUsingCertificateID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cryptoCertificate", (!this.MarkedForDeletion?_cryptoCertificate:null));
			info.AddValue("_cryptoCertificateReturnsNewIfNotFound", _cryptoCertificateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCryptoCertificate", _alwaysFetchCryptoCertificate);
			info.AddValue("_alreadyFetchedCryptoCertificate", _alreadyFetchedCryptoCertificate);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CryptoCertificate":
					_alreadyFetchedCryptoCertificate = true;
					this.CryptoCertificate = (CryptoCertificateEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CryptoCertificate":
					SetupSyncCryptoCertificate(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CryptoCertificate":
					DesetupSyncCryptoCertificate(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cryptoCertificate!=null)
			{
				toReturn.Add(_cryptoCertificate);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 uicCertificateID)
		{
			return FetchUsingPK(uicCertificateID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 uicCertificateID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uicCertificateID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 uicCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uicCertificateID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 uicCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uicCertificateID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UicCertificateID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CryptoUicCertificateRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CryptoCertificateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CryptoCertificateEntity' which is related to this entity.</returns>
		public CryptoCertificateEntity GetSingleCryptoCertificate()
		{
			return GetSingleCryptoCertificate(false);
		}

		/// <summary> Retrieves the related entity of type 'CryptoCertificateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CryptoCertificateEntity' which is related to this entity.</returns>
		public virtual CryptoCertificateEntity GetSingleCryptoCertificate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCryptoCertificate || forceFetch || _alwaysFetchCryptoCertificate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CryptoCertificateEntityUsingCertificateID);
				CryptoCertificateEntity newEntity = new CryptoCertificateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CertificateID);
				}
				if(fetchResult)
				{
					newEntity = (CryptoCertificateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cryptoCertificateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CryptoCertificate = newEntity;
				_alreadyFetchedCryptoCertificate = fetchResult;
			}
			return _cryptoCertificate;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CryptoCertificate", _cryptoCertificate);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="validator">The validator object for this CryptoUicCertificateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 uicCertificateID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uicCertificateID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cryptoCertificateReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTrusted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RicsCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SignKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UicCertificateID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cryptoCertificate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCryptoCertificate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cryptoCertificate, new PropertyChangedEventHandler( OnCryptoCertificatePropertyChanged ), "CryptoCertificate", VarioSL.Entities.RelationClasses.StaticCryptoUicCertificateRelations.CryptoCertificateEntityUsingCertificateIDStatic, true, signalRelatedEntity, "CryptoUicCertificates", resetFKFields, new int[] { (int)CryptoUicCertificateFieldIndex.CertificateID } );		
			_cryptoCertificate = null;
		}
		
		/// <summary> setups the sync logic for member _cryptoCertificate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCryptoCertificate(IEntityCore relatedEntity)
		{
			if(_cryptoCertificate!=relatedEntity)
			{		
				DesetupSyncCryptoCertificate(true, true);
				_cryptoCertificate = (CryptoCertificateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cryptoCertificate, new PropertyChangedEventHandler( OnCryptoCertificatePropertyChanged ), "CryptoCertificate", VarioSL.Entities.RelationClasses.StaticCryptoUicCertificateRelations.CryptoCertificateEntityUsingCertificateIDStatic, true, ref _alreadyFetchedCryptoCertificate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCryptoCertificatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uicCertificateID">PK value for CryptoUicCertificate which data should be fetched into this CryptoUicCertificate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 uicCertificateID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CryptoUicCertificateFieldIndex.UicCertificateID].ForcedCurrentValueWrite(uicCertificateID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCryptoUicCertificateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CryptoUicCertificateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CryptoUicCertificateRelations Relations
		{
			get	{ return new CryptoUicCertificateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CryptoCertificate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCryptoCertificate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CryptoCertificateCollection(), (IEntityRelation)GetRelationsForField("CryptoCertificate")[0], (int)VarioSL.Entities.EntityType.CryptoUicCertificateEntity, (int)VarioSL.Entities.EntityType.CryptoCertificateEntity, 0, null, null, null, "CryptoCertificate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CertificateID property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."CERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CertificateID
		{
			get { return (System.Int64)GetValue((int)CryptoUicCertificateFieldIndex.CertificateID, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.CertificateID, value, true); }
		}

		/// <summary> The IsTrusted property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."ISTRUSTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTrusted
		{
			get { return (System.Boolean)GetValue((int)CryptoUicCertificateFieldIndex.IsTrusted, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.IsTrusted, value, true); }
		}

		/// <summary> The LastModified property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CryptoUicCertificateFieldIndex.LastModified, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CryptoUicCertificateFieldIndex.LastUser, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.LastUser, value, true); }
		}

		/// <summary> The RicsCode property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."RICSCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RicsCode
		{
			get { return (System.String)GetValue((int)CryptoUicCertificateFieldIndex.RicsCode, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.RicsCode, value, true); }
		}

		/// <summary> The SignKeyID property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."SIGNKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SignKeyID
		{
			get { return (System.String)GetValue((int)CryptoUicCertificateFieldIndex.SignKeyID, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.SignKeyID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CryptoUicCertificateFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UicCertificateID property of the Entity CryptoUicCertificate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CM_UICCERTIFICATE"."UICCERTIFICATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 UicCertificateID
		{
			get { return (System.Int64)GetValue((int)CryptoUicCertificateFieldIndex.UicCertificateID, true); }
			set	{ SetValue((int)CryptoUicCertificateFieldIndex.UicCertificateID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CryptoCertificateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCryptoCertificate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CryptoCertificateEntity CryptoCertificate
		{
			get	{ return GetSingleCryptoCertificate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCryptoCertificate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CryptoUicCertificates", "CryptoCertificate", _cryptoCertificate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CryptoCertificate. When set to true, CryptoCertificate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CryptoCertificate is accessed. You can always execute a forced fetch by calling GetSingleCryptoCertificate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCryptoCertificate
		{
			get	{ return _alwaysFetchCryptoCertificate; }
			set	{ _alwaysFetchCryptoCertificate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CryptoCertificate already has been fetched. Setting this property to false when CryptoCertificate has been fetched
		/// will set CryptoCertificate to null as well. Setting this property to true while CryptoCertificate hasn't been fetched disables lazy loading for CryptoCertificate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCryptoCertificate
		{
			get { return _alreadyFetchedCryptoCertificate;}
			set 
			{
				if(_alreadyFetchedCryptoCertificate && !value)
				{
					this.CryptoCertificate = null;
				}
				_alreadyFetchedCryptoCertificate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CryptoCertificate is not found
		/// in the database. When set to true, CryptoCertificate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CryptoCertificateReturnsNewIfNotFound
		{
			get	{ return _cryptoCertificateReturnsNewIfNotFound; }
			set { _cryptoCertificateReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CryptoUicCertificateEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
