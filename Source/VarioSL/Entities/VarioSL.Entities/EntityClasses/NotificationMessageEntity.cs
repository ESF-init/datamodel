﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'NotificationMessage'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class NotificationMessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection	_notificationMessageOwners;
		private bool	_alwaysFetchNotificationMessageOwners, _alreadyFetchedNotificationMessageOwners;
		private VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection	_notificationMessageToCustomers;
		private bool	_alwaysFetchNotificationMessageToCustomers, _alreadyFetchedNotificationMessageToCustomers;
		private VarioSL.Entities.CollectionClasses.ContractCollection _contracts;
		private bool	_alwaysFetchContracts, _alreadyFetchedContracts;
		private EmailEventEntity _emailEvent;
		private bool	_alwaysFetchEmailEvent, _alreadyFetchedEmailEvent, _emailEventReturnsNewIfNotFound;
		private FormEntity _form;
		private bool	_alwaysFetchForm, _alreadyFetchedForm, _formReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EmailEvent</summary>
			public static readonly string EmailEvent = "EmailEvent";
			/// <summary>Member name Form</summary>
			public static readonly string Form = "Form";
			/// <summary>Member name NotificationMessageOwners</summary>
			public static readonly string NotificationMessageOwners = "NotificationMessageOwners";
			/// <summary>Member name NotificationMessageToCustomers</summary>
			public static readonly string NotificationMessageToCustomers = "NotificationMessageToCustomers";
			/// <summary>Member name Contracts</summary>
			public static readonly string Contracts = "Contracts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NotificationMessageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public NotificationMessageEntity() :base("NotificationMessageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		public NotificationMessageEntity(System.Int64 notificationMessageID):base("NotificationMessageEntity")
		{
			InitClassFetch(notificationMessageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NotificationMessageEntity(System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse):base("NotificationMessageEntity")
		{
			InitClassFetch(notificationMessageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="validator">The custom validator object for this NotificationMessageEntity</param>
		public NotificationMessageEntity(System.Int64 notificationMessageID, IValidator validator):base("NotificationMessageEntity")
		{
			InitClassFetch(notificationMessageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NotificationMessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_notificationMessageOwners = (VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection)info.GetValue("_notificationMessageOwners", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection));
			_alwaysFetchNotificationMessageOwners = info.GetBoolean("_alwaysFetchNotificationMessageOwners");
			_alreadyFetchedNotificationMessageOwners = info.GetBoolean("_alreadyFetchedNotificationMessageOwners");

			_notificationMessageToCustomers = (VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection)info.GetValue("_notificationMessageToCustomers", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection));
			_alwaysFetchNotificationMessageToCustomers = info.GetBoolean("_alwaysFetchNotificationMessageToCustomers");
			_alreadyFetchedNotificationMessageToCustomers = info.GetBoolean("_alreadyFetchedNotificationMessageToCustomers");
			_contracts = (VarioSL.Entities.CollectionClasses.ContractCollection)info.GetValue("_contracts", typeof(VarioSL.Entities.CollectionClasses.ContractCollection));
			_alwaysFetchContracts = info.GetBoolean("_alwaysFetchContracts");
			_alreadyFetchedContracts = info.GetBoolean("_alreadyFetchedContracts");
			_emailEvent = (EmailEventEntity)info.GetValue("_emailEvent", typeof(EmailEventEntity));
			if(_emailEvent!=null)
			{
				_emailEvent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailEventReturnsNewIfNotFound = info.GetBoolean("_emailEventReturnsNewIfNotFound");
			_alwaysFetchEmailEvent = info.GetBoolean("_alwaysFetchEmailEvent");
			_alreadyFetchedEmailEvent = info.GetBoolean("_alreadyFetchedEmailEvent");

			_form = (FormEntity)info.GetValue("_form", typeof(FormEntity));
			if(_form!=null)
			{
				_form.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_formReturnsNewIfNotFound = info.GetBoolean("_formReturnsNewIfNotFound");
			_alwaysFetchForm = info.GetBoolean("_alwaysFetchForm");
			_alreadyFetchedForm = info.GetBoolean("_alreadyFetchedForm");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((NotificationMessageFieldIndex)fieldIndex)
			{
				case NotificationMessageFieldIndex.EmailEventID:
					DesetupSyncEmailEvent(true, false);
					_alreadyFetchedEmailEvent = false;
					break;
				case NotificationMessageFieldIndex.FormID:
					DesetupSyncForm(true, false);
					_alreadyFetchedForm = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedNotificationMessageOwners = (_notificationMessageOwners.Count > 0);
			_alreadyFetchedNotificationMessageToCustomers = (_notificationMessageToCustomers.Count > 0);
			_alreadyFetchedContracts = (_contracts.Count > 0);
			_alreadyFetchedEmailEvent = (_emailEvent != null);
			_alreadyFetchedForm = (_form != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EmailEvent":
					toReturn.Add(Relations.EmailEventEntityUsingEmailEventID);
					break;
				case "Form":
					toReturn.Add(Relations.FormEntityUsingFormID);
					break;
				case "NotificationMessageOwners":
					toReturn.Add(Relations.NotificationMessageOwnerEntityUsingNotificationMessageID);
					break;
				case "NotificationMessageToCustomers":
					toReturn.Add(Relations.NotificationMessageToCustomerEntityUsingNotificationMessageID);
					break;
				case "Contracts":
					toReturn.Add(Relations.NotificationMessageOwnerEntityUsingNotificationMessageID, "NotificationMessageEntity__", "NotificationMessageOwner_", JoinHint.None);
					toReturn.Add(NotificationMessageOwnerEntity.Relations.ContractEntityUsingContractID, "NotificationMessageOwner_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_notificationMessageOwners", (!this.MarkedForDeletion?_notificationMessageOwners:null));
			info.AddValue("_alwaysFetchNotificationMessageOwners", _alwaysFetchNotificationMessageOwners);
			info.AddValue("_alreadyFetchedNotificationMessageOwners", _alreadyFetchedNotificationMessageOwners);
			info.AddValue("_notificationMessageToCustomers", (!this.MarkedForDeletion?_notificationMessageToCustomers:null));
			info.AddValue("_alwaysFetchNotificationMessageToCustomers", _alwaysFetchNotificationMessageToCustomers);
			info.AddValue("_alreadyFetchedNotificationMessageToCustomers", _alreadyFetchedNotificationMessageToCustomers);
			info.AddValue("_contracts", (!this.MarkedForDeletion?_contracts:null));
			info.AddValue("_alwaysFetchContracts", _alwaysFetchContracts);
			info.AddValue("_alreadyFetchedContracts", _alreadyFetchedContracts);
			info.AddValue("_emailEvent", (!this.MarkedForDeletion?_emailEvent:null));
			info.AddValue("_emailEventReturnsNewIfNotFound", _emailEventReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailEvent", _alwaysFetchEmailEvent);
			info.AddValue("_alreadyFetchedEmailEvent", _alreadyFetchedEmailEvent);
			info.AddValue("_form", (!this.MarkedForDeletion?_form:null));
			info.AddValue("_formReturnsNewIfNotFound", _formReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForm", _alwaysFetchForm);
			info.AddValue("_alreadyFetchedForm", _alreadyFetchedForm);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EmailEvent":
					_alreadyFetchedEmailEvent = true;
					this.EmailEvent = (EmailEventEntity)entity;
					break;
				case "Form":
					_alreadyFetchedForm = true;
					this.Form = (FormEntity)entity;
					break;
				case "NotificationMessageOwners":
					_alreadyFetchedNotificationMessageOwners = true;
					if(entity!=null)
					{
						this.NotificationMessageOwners.Add((NotificationMessageOwnerEntity)entity);
					}
					break;
				case "NotificationMessageToCustomers":
					_alreadyFetchedNotificationMessageToCustomers = true;
					if(entity!=null)
					{
						this.NotificationMessageToCustomers.Add((NotificationMessageToCustomerEntity)entity);
					}
					break;
				case "Contracts":
					_alreadyFetchedContracts = true;
					if(entity!=null)
					{
						this.Contracts.Add((ContractEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					SetupSyncEmailEvent(relatedEntity);
					break;
				case "Form":
					SetupSyncForm(relatedEntity);
					break;
				case "NotificationMessageOwners":
					_notificationMessageOwners.Add((NotificationMessageOwnerEntity)relatedEntity);
					break;
				case "NotificationMessageToCustomers":
					_notificationMessageToCustomers.Add((NotificationMessageToCustomerEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					DesetupSyncEmailEvent(false, true);
					break;
				case "Form":
					DesetupSyncForm(false, true);
					break;
				case "NotificationMessageOwners":
					this.PerformRelatedEntityRemoval(_notificationMessageOwners, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NotificationMessageToCustomers":
					this.PerformRelatedEntityRemoval(_notificationMessageToCustomers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_emailEvent!=null)
			{
				toReturn.Add(_emailEvent);
			}
			if(_form!=null)
			{
				toReturn.Add(_form);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_notificationMessageOwners);
			toReturn.Add(_notificationMessageToCustomers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 notificationMessageID)
		{
			return FetchUsingPK(notificationMessageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(notificationMessageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(notificationMessageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(notificationMessageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NotificationMessageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NotificationMessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageOwnerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch)
		{
			return GetMultiNotificationMessageOwners(forceFetch, _notificationMessageOwners.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageOwnerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessageOwners(forceFetch, _notificationMessageOwners.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessageOwners(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessageOwners || forceFetch || _alwaysFetchNotificationMessageOwners) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessageOwners);
				_notificationMessageOwners.SuppressClearInGetMulti=!forceFetch;
				_notificationMessageOwners.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessageOwners.GetMultiManyToOne(null, this, filter);
				_notificationMessageOwners.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessageOwners = true;
			}
			return _notificationMessageOwners;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessageOwners'. These settings will be taken into account
		/// when the property NotificationMessageOwners is requested or GetMultiNotificationMessageOwners is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessageOwners(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessageOwners.SortClauses=sortClauses;
			_notificationMessageOwners.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageToCustomerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, _notificationMessageToCustomers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageToCustomerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, _notificationMessageToCustomers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessageToCustomers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection GetMultiNotificationMessageToCustomers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessageToCustomers || forceFetch || _alwaysFetchNotificationMessageToCustomers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessageToCustomers);
				_notificationMessageToCustomers.SuppressClearInGetMulti=!forceFetch;
				_notificationMessageToCustomers.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessageToCustomers.GetMultiManyToOne(null, this, filter);
				_notificationMessageToCustomers.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessageToCustomers = true;
			}
			return _notificationMessageToCustomers;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessageToCustomers'. These settings will be taken into account
		/// when the property NotificationMessageToCustomers is requested or GetMultiNotificationMessageToCustomers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessageToCustomers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessageToCustomers.SortClauses=sortClauses;
			_notificationMessageToCustomers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch)
		{
			return GetMultiContracts(forceFetch, _contracts.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractCollection GetMultiContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedContracts || forceFetch || _alwaysFetchContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contracts);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(NotificationMessageFields.NotificationMessageID, ComparisonOperator.Equal, this.NotificationMessageID, "NotificationMessageEntity__"));
				_contracts.SuppressClearInGetMulti=!forceFetch;
				_contracts.EntityFactoryToUse = entityFactoryToUse;
				_contracts.GetMulti(filter, GetRelationsForField("Contracts"));
				_contracts.SuppressClearInGetMulti=false;
				_alreadyFetchedContracts = true;
			}
			return _contracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Contracts'. These settings will be taken into account
		/// when the property Contracts is requested or GetMultiContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contracts.SortClauses=sortClauses;
			_contracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public EmailEventEntity GetSingleEmailEvent()
		{
			return GetSingleEmailEvent(false);
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public virtual EmailEventEntity GetSingleEmailEvent(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailEvent || forceFetch || _alwaysFetchEmailEvent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EmailEventEntityUsingEmailEventID);
				EmailEventEntity newEntity = new EmailEventEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmailEventID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EmailEventEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailEventReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailEvent = newEntity;
				_alreadyFetchedEmailEvent = fetchResult;
			}
			return _emailEvent;
		}


		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public FormEntity GetSingleForm()
		{
			return GetSingleForm(false);
		}

		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public virtual FormEntity GetSingleForm(bool forceFetch)
		{
			if( ( !_alreadyFetchedForm || forceFetch || _alwaysFetchForm) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FormEntityUsingFormID);
				FormEntity newEntity = new FormEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FormID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FormEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_formReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Form = newEntity;
				_alreadyFetchedForm = fetchResult;
			}
			return _form;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EmailEvent", _emailEvent);
			toReturn.Add("Form", _form);
			toReturn.Add("NotificationMessageOwners", _notificationMessageOwners);
			toReturn.Add("NotificationMessageToCustomers", _notificationMessageToCustomers);
			toReturn.Add("Contracts", _contracts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="validator">The validator object for this NotificationMessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 notificationMessageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(notificationMessageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_notificationMessageOwners = new VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection();
			_notificationMessageOwners.SetContainingEntityInfo(this, "NotificationMessage");

			_notificationMessageToCustomers = new VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection();
			_notificationMessageToCustomers.SetContainingEntityInfo(this, "NotificationMessage");
			_contracts = new VarioSL.Entities.CollectionClasses.ContractCollection();
			_emailEventReturnsNewIfNotFound = false;
			_formReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HtmlFormat", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotificationMessageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Subject", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibleFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisibleTo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _emailEvent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailEvent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticNotificationMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, signalRelatedEntity, "NotificationMessages", resetFKFields, new int[] { (int)NotificationMessageFieldIndex.EmailEventID } );		
			_emailEvent = null;
		}
		
		/// <summary> setups the sync logic for member _emailEvent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailEvent(IEntityCore relatedEntity)
		{
			if(_emailEvent!=relatedEntity)
			{		
				DesetupSyncEmailEvent(true, true);
				_emailEvent = (EmailEventEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticNotificationMessageRelations.EmailEventEntityUsingEmailEventIDStatic, true, ref _alreadyFetchedEmailEvent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailEventPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _form</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForm(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticNotificationMessageRelations.FormEntityUsingFormIDStatic, true, signalRelatedEntity, "NotificationMessages", resetFKFields, new int[] { (int)NotificationMessageFieldIndex.FormID } );		
			_form = null;
		}
		
		/// <summary> setups the sync logic for member _form</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForm(IEntityCore relatedEntity)
		{
			if(_form!=relatedEntity)
			{		
				DesetupSyncForm(true, true);
				_form = (FormEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticNotificationMessageRelations.FormEntityUsingFormIDStatic, true, ref _alreadyFetchedForm, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="notificationMessageID">PK value for NotificationMessage which data should be fetched into this NotificationMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 notificationMessageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NotificationMessageFieldIndex.NotificationMessageID].ForcedCurrentValueWrite(notificationMessageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNotificationMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NotificationMessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NotificationMessageRelations Relations
		{
			get	{ return new NotificationMessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessageOwner' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessageOwners
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection(), (IEntityRelation)GetRelationsForField("NotificationMessageOwners")[0], (int)VarioSL.Entities.EntityType.NotificationMessageEntity, (int)VarioSL.Entities.EntityType.NotificationMessageOwnerEntity, 0, null, null, null, "NotificationMessageOwners", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessageToCustomer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessageToCustomers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection(), (IEntityRelation)GetRelationsForField("NotificationMessageToCustomers")[0], (int)VarioSL.Entities.EntityType.NotificationMessageEntity, (int)VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity, 0, null, null, null, "NotificationMessageToCustomers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContracts
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NotificationMessageOwnerEntityUsingNotificationMessageID;
				intermediateRelation.SetAliases(string.Empty, "NotificationMessageOwner_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.NotificationMessageEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, GetRelationsForField("Contracts"), "Contracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEvent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEvent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventCollection(), (IEntityRelation)GetRelationsForField("EmailEvent")[0], (int)VarioSL.Entities.EntityType.NotificationMessageEntity, (int)VarioSL.Entities.EntityType.EmailEventEntity, 0, null, null, null, "EmailEvent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForm
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Form")[0], (int)VarioSL.Entities.EntityType.NotificationMessageEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Form", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreationDate property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NotificationMessageFieldIndex.CreationDate, false); }
			set	{ SetValue((int)NotificationMessageFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The EmailEventID property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."EMAILEVENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EmailEventID
		{
			get { return (Nullable<System.Int64>)GetValue((int)NotificationMessageFieldIndex.EmailEventID, false); }
			set	{ SetValue((int)NotificationMessageFieldIndex.EmailEventID, value, true); }
		}

		/// <summary> The FormID property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FormID
		{
			get { return (Nullable<System.Int64>)GetValue((int)NotificationMessageFieldIndex.FormID, false); }
			set	{ SetValue((int)NotificationMessageFieldIndex.FormID, value, true); }
		}

		/// <summary> The HtmlFormat property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."HTMLFORMAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HtmlFormat
		{
			get { return (System.String)GetValue((int)NotificationMessageFieldIndex.HtmlFormat, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.HtmlFormat, value, true); }
		}

		/// <summary> The LastModified property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)NotificationMessageFieldIndex.LastModified, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)NotificationMessageFieldIndex.LastUser, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Message property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."MESSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)NotificationMessageFieldIndex.Message, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.Message, value, true); }
		}

		/// <summary> The NotificationMessageID property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."NOTIFICATIONMESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 NotificationMessageID
		{
			get { return (System.Int64)GetValue((int)NotificationMessageFieldIndex.NotificationMessageID, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.NotificationMessageID, value, true); }
		}

		/// <summary> The Subject property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."SUBJECT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Subject
		{
			get { return (System.String)GetValue((int)NotificationMessageFieldIndex.Subject, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.Subject, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)NotificationMessageFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VisibleFrom property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."VISIBLEFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime VisibleFrom
		{
			get { return (System.DateTime)GetValue((int)NotificationMessageFieldIndex.VisibleFrom, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.VisibleFrom, value, true); }
		}

		/// <summary> The VisibleTo property of the Entity NotificationMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_NOTIFICATIONMESSAGE"."VISIBLETO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime VisibleTo
		{
			get { return (System.DateTime)GetValue((int)NotificationMessageFieldIndex.VisibleTo, true); }
			set	{ SetValue((int)NotificationMessageFieldIndex.VisibleTo, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessageOwners()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection NotificationMessageOwners
		{
			get	{ return GetMultiNotificationMessageOwners(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessageOwners. When set to true, NotificationMessageOwners is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessageOwners is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessageOwners(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessageOwners
		{
			get	{ return _alwaysFetchNotificationMessageOwners; }
			set	{ _alwaysFetchNotificationMessageOwners = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessageOwners already has been fetched. Setting this property to false when NotificationMessageOwners has been fetched
		/// will clear the NotificationMessageOwners collection well. Setting this property to true while NotificationMessageOwners hasn't been fetched disables lazy loading for NotificationMessageOwners</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessageOwners
		{
			get { return _alreadyFetchedNotificationMessageOwners;}
			set 
			{
				if(_alreadyFetchedNotificationMessageOwners && !value && (_notificationMessageOwners != null))
				{
					_notificationMessageOwners.Clear();
				}
				_alreadyFetchedNotificationMessageOwners = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NotificationMessageToCustomerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessageToCustomers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageToCustomerCollection NotificationMessageToCustomers
		{
			get	{ return GetMultiNotificationMessageToCustomers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessageToCustomers. When set to true, NotificationMessageToCustomers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessageToCustomers is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessageToCustomers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessageToCustomers
		{
			get	{ return _alwaysFetchNotificationMessageToCustomers; }
			set	{ _alwaysFetchNotificationMessageToCustomers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessageToCustomers already has been fetched. Setting this property to false when NotificationMessageToCustomers has been fetched
		/// will clear the NotificationMessageToCustomers collection well. Setting this property to true while NotificationMessageToCustomers hasn't been fetched disables lazy loading for NotificationMessageToCustomers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessageToCustomers
		{
			get { return _alreadyFetchedNotificationMessageToCustomers;}
			set 
			{
				if(_alreadyFetchedNotificationMessageToCustomers && !value && (_notificationMessageToCustomers != null))
				{
					_notificationMessageToCustomers.Clear();
				}
				_alreadyFetchedNotificationMessageToCustomers = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ContractEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractCollection Contracts
		{
			get { return GetMultiContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Contracts. When set to true, Contracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contracts is accessed. You can always execute a forced fetch by calling GetMultiContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContracts
		{
			get	{ return _alwaysFetchContracts; }
			set	{ _alwaysFetchContracts = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contracts already has been fetched. Setting this property to false when Contracts has been fetched
		/// will clear the Contracts collection well. Setting this property to true while Contracts hasn't been fetched disables lazy loading for Contracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContracts
		{
			get { return _alreadyFetchedContracts;}
			set 
			{
				if(_alreadyFetchedContracts && !value && (_contracts != null))
				{
					_contracts.Clear();
				}
				_alreadyFetchedContracts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'EmailEventEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailEvent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EmailEventEntity EmailEvent
		{
			get	{ return GetSingleEmailEvent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailEvent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NotificationMessages", "EmailEvent", _emailEvent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEvent. When set to true, EmailEvent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEvent is accessed. You can always execute a forced fetch by calling GetSingleEmailEvent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEvent
		{
			get	{ return _alwaysFetchEmailEvent; }
			set	{ _alwaysFetchEmailEvent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEvent already has been fetched. Setting this property to false when EmailEvent has been fetched
		/// will set EmailEvent to null as well. Setting this property to true while EmailEvent hasn't been fetched disables lazy loading for EmailEvent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEvent
		{
			get { return _alreadyFetchedEmailEvent;}
			set 
			{
				if(_alreadyFetchedEmailEvent && !value)
				{
					this.EmailEvent = null;
				}
				_alreadyFetchedEmailEvent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailEvent is not found
		/// in the database. When set to true, EmailEvent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EmailEventReturnsNewIfNotFound
		{
			get	{ return _emailEventReturnsNewIfNotFound; }
			set { _emailEventReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FormEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForm()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FormEntity Form
		{
			get	{ return GetSingleForm(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForm(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NotificationMessages", "Form", _form, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Form. When set to true, Form is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Form is accessed. You can always execute a forced fetch by calling GetSingleForm(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForm
		{
			get	{ return _alwaysFetchForm; }
			set	{ _alwaysFetchForm = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Form already has been fetched. Setting this property to false when Form has been fetched
		/// will set Form to null as well. Setting this property to true while Form hasn't been fetched disables lazy loading for Form</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForm
		{
			get { return _alreadyFetchedForm;}
			set 
			{
				if(_alreadyFetchedForm && !value)
				{
					this.Form = null;
				}
				_alreadyFetchedForm = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Form is not found
		/// in the database. When set to true, Form will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FormReturnsNewIfNotFound
		{
			get	{ return _formReturnsNewIfNotFound; }
			set { _formReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.NotificationMessageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
