﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'StockTransfer'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class StockTransferEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardStockTransferCollection	_cardStockTransfers;
		private bool	_alwaysFetchCardStockTransfers, _alreadyFetchedCardStockTransfers;
		private VarioSL.Entities.CollectionClasses.CardCollection _cardCollectionViaCardStockTransfer;
		private bool	_alwaysFetchCardCollectionViaCardStockTransfer, _alreadyFetchedCardCollectionViaCardStockTransfer;
		private StorageLocationEntity _newStorageLocation;
		private bool	_alwaysFetchNewStorageLocation, _alreadyFetchedNewStorageLocation, _newStorageLocationReturnsNewIfNotFound;
		private StorageLocationEntity _oldStorageLocation;
		private bool	_alwaysFetchOldStorageLocation, _alreadyFetchedOldStorageLocation, _oldStorageLocationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name NewStorageLocation</summary>
			public static readonly string NewStorageLocation = "NewStorageLocation";
			/// <summary>Member name OldStorageLocation</summary>
			public static readonly string OldStorageLocation = "OldStorageLocation";
			/// <summary>Member name CardStockTransfers</summary>
			public static readonly string CardStockTransfers = "CardStockTransfers";
			/// <summary>Member name CardCollectionViaCardStockTransfer</summary>
			public static readonly string CardCollectionViaCardStockTransfer = "CardCollectionViaCardStockTransfer";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static StockTransferEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public StockTransferEntity() :base("StockTransferEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		public StockTransferEntity(System.Int64 stockTransferID):base("StockTransferEntity")
		{
			InitClassFetch(stockTransferID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public StockTransferEntity(System.Int64 stockTransferID, IPrefetchPath prefetchPathToUse):base("StockTransferEntity")
		{
			InitClassFetch(stockTransferID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="validator">The custom validator object for this StockTransferEntity</param>
		public StockTransferEntity(System.Int64 stockTransferID, IValidator validator):base("StockTransferEntity")
		{
			InitClassFetch(stockTransferID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StockTransferEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardStockTransfers = (VarioSL.Entities.CollectionClasses.CardStockTransferCollection)info.GetValue("_cardStockTransfers", typeof(VarioSL.Entities.CollectionClasses.CardStockTransferCollection));
			_alwaysFetchCardStockTransfers = info.GetBoolean("_alwaysFetchCardStockTransfers");
			_alreadyFetchedCardStockTransfers = info.GetBoolean("_alreadyFetchedCardStockTransfers");
			_cardCollectionViaCardStockTransfer = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cardCollectionViaCardStockTransfer", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCardCollectionViaCardStockTransfer = info.GetBoolean("_alwaysFetchCardCollectionViaCardStockTransfer");
			_alreadyFetchedCardCollectionViaCardStockTransfer = info.GetBoolean("_alreadyFetchedCardCollectionViaCardStockTransfer");
			_newStorageLocation = (StorageLocationEntity)info.GetValue("_newStorageLocation", typeof(StorageLocationEntity));
			if(_newStorageLocation!=null)
			{
				_newStorageLocation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_newStorageLocationReturnsNewIfNotFound = info.GetBoolean("_newStorageLocationReturnsNewIfNotFound");
			_alwaysFetchNewStorageLocation = info.GetBoolean("_alwaysFetchNewStorageLocation");
			_alreadyFetchedNewStorageLocation = info.GetBoolean("_alreadyFetchedNewStorageLocation");

			_oldStorageLocation = (StorageLocationEntity)info.GetValue("_oldStorageLocation", typeof(StorageLocationEntity));
			if(_oldStorageLocation!=null)
			{
				_oldStorageLocation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_oldStorageLocationReturnsNewIfNotFound = info.GetBoolean("_oldStorageLocationReturnsNewIfNotFound");
			_alwaysFetchOldStorageLocation = info.GetBoolean("_alwaysFetchOldStorageLocation");
			_alreadyFetchedOldStorageLocation = info.GetBoolean("_alreadyFetchedOldStorageLocation");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((StockTransferFieldIndex)fieldIndex)
			{
				case StockTransferFieldIndex.NewStorageLocationID:
					DesetupSyncNewStorageLocation(true, false);
					_alreadyFetchedNewStorageLocation = false;
					break;
				case StockTransferFieldIndex.OldStorageLocationID:
					DesetupSyncOldStorageLocation(true, false);
					_alreadyFetchedOldStorageLocation = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardStockTransfers = (_cardStockTransfers.Count > 0);
			_alreadyFetchedCardCollectionViaCardStockTransfer = (_cardCollectionViaCardStockTransfer.Count > 0);
			_alreadyFetchedNewStorageLocation = (_newStorageLocation != null);
			_alreadyFetchedOldStorageLocation = (_oldStorageLocation != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "NewStorageLocation":
					toReturn.Add(Relations.StorageLocationEntityUsingNewStorageLocationID);
					break;
				case "OldStorageLocation":
					toReturn.Add(Relations.StorageLocationEntityUsingOldStorageLocationID);
					break;
				case "CardStockTransfers":
					toReturn.Add(Relations.CardStockTransferEntityUsingStockTransferID);
					break;
				case "CardCollectionViaCardStockTransfer":
					toReturn.Add(Relations.CardStockTransferEntityUsingStockTransferID, "StockTransferEntity__", "CardStockTransfer_", JoinHint.None);
					toReturn.Add(CardStockTransferEntity.Relations.CardEntityUsingCardID, "CardStockTransfer_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardStockTransfers", (!this.MarkedForDeletion?_cardStockTransfers:null));
			info.AddValue("_alwaysFetchCardStockTransfers", _alwaysFetchCardStockTransfers);
			info.AddValue("_alreadyFetchedCardStockTransfers", _alreadyFetchedCardStockTransfers);
			info.AddValue("_cardCollectionViaCardStockTransfer", (!this.MarkedForDeletion?_cardCollectionViaCardStockTransfer:null));
			info.AddValue("_alwaysFetchCardCollectionViaCardStockTransfer", _alwaysFetchCardCollectionViaCardStockTransfer);
			info.AddValue("_alreadyFetchedCardCollectionViaCardStockTransfer", _alreadyFetchedCardCollectionViaCardStockTransfer);
			info.AddValue("_newStorageLocation", (!this.MarkedForDeletion?_newStorageLocation:null));
			info.AddValue("_newStorageLocationReturnsNewIfNotFound", _newStorageLocationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNewStorageLocation", _alwaysFetchNewStorageLocation);
			info.AddValue("_alreadyFetchedNewStorageLocation", _alreadyFetchedNewStorageLocation);
			info.AddValue("_oldStorageLocation", (!this.MarkedForDeletion?_oldStorageLocation:null));
			info.AddValue("_oldStorageLocationReturnsNewIfNotFound", _oldStorageLocationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOldStorageLocation", _alwaysFetchOldStorageLocation);
			info.AddValue("_alreadyFetchedOldStorageLocation", _alreadyFetchedOldStorageLocation);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "NewStorageLocation":
					_alreadyFetchedNewStorageLocation = true;
					this.NewStorageLocation = (StorageLocationEntity)entity;
					break;
				case "OldStorageLocation":
					_alreadyFetchedOldStorageLocation = true;
					this.OldStorageLocation = (StorageLocationEntity)entity;
					break;
				case "CardStockTransfers":
					_alreadyFetchedCardStockTransfers = true;
					if(entity!=null)
					{
						this.CardStockTransfers.Add((CardStockTransferEntity)entity);
					}
					break;
				case "CardCollectionViaCardStockTransfer":
					_alreadyFetchedCardCollectionViaCardStockTransfer = true;
					if(entity!=null)
					{
						this.CardCollectionViaCardStockTransfer.Add((CardEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "NewStorageLocation":
					SetupSyncNewStorageLocation(relatedEntity);
					break;
				case "OldStorageLocation":
					SetupSyncOldStorageLocation(relatedEntity);
					break;
				case "CardStockTransfers":
					_cardStockTransfers.Add((CardStockTransferEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "NewStorageLocation":
					DesetupSyncNewStorageLocation(false, true);
					break;
				case "OldStorageLocation":
					DesetupSyncOldStorageLocation(false, true);
					break;
				case "CardStockTransfers":
					this.PerformRelatedEntityRemoval(_cardStockTransfers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_newStorageLocation!=null)
			{
				toReturn.Add(_newStorageLocation);
			}
			if(_oldStorageLocation!=null)
			{
				toReturn.Add(_oldStorageLocation);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardStockTransfers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stockTransferID)
		{
			return FetchUsingPK(stockTransferID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stockTransferID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(stockTransferID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stockTransferID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(stockTransferID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stockTransferID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(stockTransferID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.StockTransferID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new StockTransferRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardStockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch)
		{
			return GetMultiCardStockTransfers(forceFetch, _cardStockTransfers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardStockTransferEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardStockTransfers(forceFetch, _cardStockTransfers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardStockTransfers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardStockTransferCollection GetMultiCardStockTransfers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardStockTransfers || forceFetch || _alwaysFetchCardStockTransfers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardStockTransfers);
				_cardStockTransfers.SuppressClearInGetMulti=!forceFetch;
				_cardStockTransfers.EntityFactoryToUse = entityFactoryToUse;
				_cardStockTransfers.GetMultiManyToOne(null, this, filter);
				_cardStockTransfers.SuppressClearInGetMulti=false;
				_alreadyFetchedCardStockTransfers = true;
			}
			return _cardStockTransfers;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardStockTransfers'. These settings will be taken into account
		/// when the property CardStockTransfers is requested or GetMultiCardStockTransfers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardStockTransfers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardStockTransfers.SortClauses=sortClauses;
			_cardStockTransfers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCardCollectionViaCardStockTransfer(bool forceFetch)
		{
			return GetMultiCardCollectionViaCardStockTransfer(forceFetch, _cardCollectionViaCardStockTransfer.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCardCollectionViaCardStockTransfer(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCardCollectionViaCardStockTransfer || forceFetch || _alwaysFetchCardCollectionViaCardStockTransfer) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardCollectionViaCardStockTransfer);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(StockTransferFields.StockTransferID, ComparisonOperator.Equal, this.StockTransferID, "StockTransferEntity__"));
				_cardCollectionViaCardStockTransfer.SuppressClearInGetMulti=!forceFetch;
				_cardCollectionViaCardStockTransfer.EntityFactoryToUse = entityFactoryToUse;
				_cardCollectionViaCardStockTransfer.GetMulti(filter, GetRelationsForField("CardCollectionViaCardStockTransfer"));
				_cardCollectionViaCardStockTransfer.SuppressClearInGetMulti=false;
				_alreadyFetchedCardCollectionViaCardStockTransfer = true;
			}
			return _cardCollectionViaCardStockTransfer;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardCollectionViaCardStockTransfer'. These settings will be taken into account
		/// when the property CardCollectionViaCardStockTransfer is requested or GetMultiCardCollectionViaCardStockTransfer is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardCollectionViaCardStockTransfer(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardCollectionViaCardStockTransfer.SortClauses=sortClauses;
			_cardCollectionViaCardStockTransfer.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public StorageLocationEntity GetSingleNewStorageLocation()
		{
			return GetSingleNewStorageLocation(false);
		}

		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public virtual StorageLocationEntity GetSingleNewStorageLocation(bool forceFetch)
		{
			if( ( !_alreadyFetchedNewStorageLocation || forceFetch || _alwaysFetchNewStorageLocation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StorageLocationEntityUsingNewStorageLocationID);
				StorageLocationEntity newEntity = new StorageLocationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NewStorageLocationID);
				}
				if(fetchResult)
				{
					newEntity = (StorageLocationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_newStorageLocationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.NewStorageLocation = newEntity;
				_alreadyFetchedNewStorageLocation = fetchResult;
			}
			return _newStorageLocation;
		}


		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public StorageLocationEntity GetSingleOldStorageLocation()
		{
			return GetSingleOldStorageLocation(false);
		}

		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public virtual StorageLocationEntity GetSingleOldStorageLocation(bool forceFetch)
		{
			if( ( !_alreadyFetchedOldStorageLocation || forceFetch || _alwaysFetchOldStorageLocation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StorageLocationEntityUsingOldStorageLocationID);
				StorageLocationEntity newEntity = new StorageLocationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OldStorageLocationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (StorageLocationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_oldStorageLocationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OldStorageLocation = newEntity;
				_alreadyFetchedOldStorageLocation = fetchResult;
			}
			return _oldStorageLocation;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("NewStorageLocation", _newStorageLocation);
			toReturn.Add("OldStorageLocation", _oldStorageLocation);
			toReturn.Add("CardStockTransfers", _cardStockTransfers);
			toReturn.Add("CardCollectionViaCardStockTransfer", _cardCollectionViaCardStockTransfer);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="validator">The validator object for this StockTransferEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 stockTransferID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(stockTransferID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardStockTransfers = new VarioSL.Entities.CollectionClasses.CardStockTransferCollection();
			_cardStockTransfers.SetContainingEntityInfo(this, "StockTransfer");
			_cardCollectionViaCardStockTransfer = new VarioSL.Entities.CollectionClasses.CardCollection();
			_newStorageLocationReturnsNewIfNotFound = false;
			_oldStorageLocationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NewStorageLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OldStorageLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StockTransferID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransferTime", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _newStorageLocation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNewStorageLocation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _newStorageLocation, new PropertyChangedEventHandler( OnNewStorageLocationPropertyChanged ), "NewStorageLocation", VarioSL.Entities.RelationClasses.StaticStockTransferRelations.StorageLocationEntityUsingNewStorageLocationIDStatic, true, signalRelatedEntity, "NewLocationStockTransfers", resetFKFields, new int[] { (int)StockTransferFieldIndex.NewStorageLocationID } );		
			_newStorageLocation = null;
		}
		
		/// <summary> setups the sync logic for member _newStorageLocation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNewStorageLocation(IEntityCore relatedEntity)
		{
			if(_newStorageLocation!=relatedEntity)
			{		
				DesetupSyncNewStorageLocation(true, true);
				_newStorageLocation = (StorageLocationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _newStorageLocation, new PropertyChangedEventHandler( OnNewStorageLocationPropertyChanged ), "NewStorageLocation", VarioSL.Entities.RelationClasses.StaticStockTransferRelations.StorageLocationEntityUsingNewStorageLocationIDStatic, true, ref _alreadyFetchedNewStorageLocation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNewStorageLocationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _oldStorageLocation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOldStorageLocation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _oldStorageLocation, new PropertyChangedEventHandler( OnOldStorageLocationPropertyChanged ), "OldStorageLocation", VarioSL.Entities.RelationClasses.StaticStockTransferRelations.StorageLocationEntityUsingOldStorageLocationIDStatic, true, signalRelatedEntity, "OldLocationStockTransfers", resetFKFields, new int[] { (int)StockTransferFieldIndex.OldStorageLocationID } );		
			_oldStorageLocation = null;
		}
		
		/// <summary> setups the sync logic for member _oldStorageLocation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOldStorageLocation(IEntityCore relatedEntity)
		{
			if(_oldStorageLocation!=relatedEntity)
			{		
				DesetupSyncOldStorageLocation(true, true);
				_oldStorageLocation = (StorageLocationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _oldStorageLocation, new PropertyChangedEventHandler( OnOldStorageLocationPropertyChanged ), "OldStorageLocation", VarioSL.Entities.RelationClasses.StaticStockTransferRelations.StorageLocationEntityUsingOldStorageLocationIDStatic, true, ref _alreadyFetchedOldStorageLocation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOldStorageLocationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="stockTransferID">PK value for StockTransfer which data should be fetched into this StockTransfer object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 stockTransferID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)StockTransferFieldIndex.StockTransferID].ForcedCurrentValueWrite(stockTransferID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateStockTransferDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new StockTransferEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static StockTransferRelations Relations
		{
			get	{ return new StockTransferRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardStockTransfer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardStockTransfers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardStockTransferCollection(), (IEntityRelation)GetRelationsForField("CardStockTransfers")[0], (int)VarioSL.Entities.EntityType.StockTransferEntity, (int)VarioSL.Entities.EntityType.CardStockTransferEntity, 0, null, null, null, "CardStockTransfers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardCollectionViaCardStockTransfer
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardStockTransferEntityUsingStockTransferID;
				intermediateRelation.SetAliases(string.Empty, "CardStockTransfer_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.StockTransferEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, GetRelationsForField("CardCollectionViaCardStockTransfer"), "CardCollectionViaCardStockTransfer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNewStorageLocation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("NewStorageLocation")[0], (int)VarioSL.Entities.EntityType.StockTransferEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "NewStorageLocation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOldStorageLocation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("OldStorageLocation")[0], (int)VarioSL.Entities.EntityType.StockTransferEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "OldStorageLocation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The InvoiceNumber property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."INVOICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String InvoiceNumber
		{
			get { return (System.String)GetValue((int)StockTransferFieldIndex.InvoiceNumber, true); }
			set	{ SetValue((int)StockTransferFieldIndex.InvoiceNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)StockTransferFieldIndex.LastModified, true); }
			set	{ SetValue((int)StockTransferFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)StockTransferFieldIndex.LastUser, true); }
			set	{ SetValue((int)StockTransferFieldIndex.LastUser, value, true); }
		}

		/// <summary> The NewStorageLocationID property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."NEWSTORAGELOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NewStorageLocationID
		{
			get { return (System.Int64)GetValue((int)StockTransferFieldIndex.NewStorageLocationID, true); }
			set	{ SetValue((int)StockTransferFieldIndex.NewStorageLocationID, value, true); }
		}

		/// <summary> The OldStorageLocationID property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."OLDSTORAGELOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OldStorageLocationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)StockTransferFieldIndex.OldStorageLocationID, false); }
			set	{ SetValue((int)StockTransferFieldIndex.OldStorageLocationID, value, true); }
		}

		/// <summary> The StockTransferID property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."STOCKTRANSFERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 StockTransferID
		{
			get { return (System.Int64)GetValue((int)StockTransferFieldIndex.StockTransferID, true); }
			set	{ SetValue((int)StockTransferFieldIndex.StockTransferID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)StockTransferFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)StockTransferFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransferTime property of the Entity StockTransfer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_STOCKTRANSFER"."TRANSFERTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TransferTime
		{
			get { return (System.DateTime)GetValue((int)StockTransferFieldIndex.TransferTime, true); }
			set	{ SetValue((int)StockTransferFieldIndex.TransferTime, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardStockTransferEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardStockTransfers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardStockTransferCollection CardStockTransfers
		{
			get	{ return GetMultiCardStockTransfers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardStockTransfers. When set to true, CardStockTransfers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardStockTransfers is accessed. You can always execute/ a forced fetch by calling GetMultiCardStockTransfers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardStockTransfers
		{
			get	{ return _alwaysFetchCardStockTransfers; }
			set	{ _alwaysFetchCardStockTransfers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardStockTransfers already has been fetched. Setting this property to false when CardStockTransfers has been fetched
		/// will clear the CardStockTransfers collection well. Setting this property to true while CardStockTransfers hasn't been fetched disables lazy loading for CardStockTransfers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardStockTransfers
		{
			get { return _alreadyFetchedCardStockTransfers;}
			set 
			{
				if(_alreadyFetchedCardStockTransfers && !value && (_cardStockTransfers != null))
				{
					_cardStockTransfers.Clear();
				}
				_alreadyFetchedCardStockTransfers = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardCollectionViaCardStockTransfer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection CardCollectionViaCardStockTransfer
		{
			get { return GetMultiCardCollectionViaCardStockTransfer(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardCollectionViaCardStockTransfer. When set to true, CardCollectionViaCardStockTransfer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardCollectionViaCardStockTransfer is accessed. You can always execute a forced fetch by calling GetMultiCardCollectionViaCardStockTransfer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardCollectionViaCardStockTransfer
		{
			get	{ return _alwaysFetchCardCollectionViaCardStockTransfer; }
			set	{ _alwaysFetchCardCollectionViaCardStockTransfer = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardCollectionViaCardStockTransfer already has been fetched. Setting this property to false when CardCollectionViaCardStockTransfer has been fetched
		/// will clear the CardCollectionViaCardStockTransfer collection well. Setting this property to true while CardCollectionViaCardStockTransfer hasn't been fetched disables lazy loading for CardCollectionViaCardStockTransfer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardCollectionViaCardStockTransfer
		{
			get { return _alreadyFetchedCardCollectionViaCardStockTransfer;}
			set 
			{
				if(_alreadyFetchedCardCollectionViaCardStockTransfer && !value && (_cardCollectionViaCardStockTransfer != null))
				{
					_cardCollectionViaCardStockTransfer.Clear();
				}
				_alreadyFetchedCardCollectionViaCardStockTransfer = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'StorageLocationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNewStorageLocation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual StorageLocationEntity NewStorageLocation
		{
			get	{ return GetSingleNewStorageLocation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNewStorageLocation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "NewLocationStockTransfers", "NewStorageLocation", _newStorageLocation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for NewStorageLocation. When set to true, NewStorageLocation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NewStorageLocation is accessed. You can always execute a forced fetch by calling GetSingleNewStorageLocation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNewStorageLocation
		{
			get	{ return _alwaysFetchNewStorageLocation; }
			set	{ _alwaysFetchNewStorageLocation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NewStorageLocation already has been fetched. Setting this property to false when NewStorageLocation has been fetched
		/// will set NewStorageLocation to null as well. Setting this property to true while NewStorageLocation hasn't been fetched disables lazy loading for NewStorageLocation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNewStorageLocation
		{
			get { return _alreadyFetchedNewStorageLocation;}
			set 
			{
				if(_alreadyFetchedNewStorageLocation && !value)
				{
					this.NewStorageLocation = null;
				}
				_alreadyFetchedNewStorageLocation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property NewStorageLocation is not found
		/// in the database. When set to true, NewStorageLocation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NewStorageLocationReturnsNewIfNotFound
		{
			get	{ return _newStorageLocationReturnsNewIfNotFound; }
			set { _newStorageLocationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StorageLocationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOldStorageLocation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual StorageLocationEntity OldStorageLocation
		{
			get	{ return GetSingleOldStorageLocation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOldStorageLocation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OldLocationStockTransfers", "OldStorageLocation", _oldStorageLocation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OldStorageLocation. When set to true, OldStorageLocation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OldStorageLocation is accessed. You can always execute a forced fetch by calling GetSingleOldStorageLocation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOldStorageLocation
		{
			get	{ return _alwaysFetchOldStorageLocation; }
			set	{ _alwaysFetchOldStorageLocation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OldStorageLocation already has been fetched. Setting this property to false when OldStorageLocation has been fetched
		/// will set OldStorageLocation to null as well. Setting this property to true while OldStorageLocation hasn't been fetched disables lazy loading for OldStorageLocation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOldStorageLocation
		{
			get { return _alreadyFetchedOldStorageLocation;}
			set 
			{
				if(_alreadyFetchedOldStorageLocation && !value)
				{
					this.OldStorageLocation = null;
				}
				_alreadyFetchedOldStorageLocation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OldStorageLocation is not found
		/// in the database. When set to true, OldStorageLocation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OldStorageLocationReturnsNewIfNotFound
		{
			get	{ return _oldStorageLocationReturnsNewIfNotFound; }
			set { _oldStorageLocationReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.StockTransferEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
