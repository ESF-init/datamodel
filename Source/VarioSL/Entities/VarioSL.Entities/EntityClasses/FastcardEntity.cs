﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Fastcard'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FastcardEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FastcardEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FastcardEntity() :base("FastcardEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FastcardEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FastcardRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ActiveProducts", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderContractNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderDateOfBirth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderFirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderFrameOrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderGroupExpiry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderLastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderSchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardholderUserGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceMappingNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InventoryState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBlockAllowed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsShared", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsUnblockAllowed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfPrints", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationalIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplacedCardPrintedNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SequentialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupID", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFastcardDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FastcardEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FastcardRelations Relations
		{
			get	{ return new FastcardRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ActiveProducts property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ACTIVEPRODUCTS"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ActiveProducts
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.ActiveProducts, false); }
			set	{ SetValue((int)FastcardFieldIndex.ActiveProducts, value, true); }
		}

		/// <summary> The BlockingComment property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."BLOCKINGCOMMENT"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlockingComment
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.BlockingComment, true); }
			set	{ SetValue((int)FastcardFieldIndex.BlockingComment, value, true); }
		}

		/// <summary> The BlockingReason property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."BLOCKINGREASON"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.BlockingReason BlockingReason
		{
			get { return (VarioSL.Entities.Enumerations.BlockingReason)GetValue((int)FastcardFieldIndex.BlockingReason, true); }
			set	{ SetValue((int)FastcardFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The CardholderContractID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERCONTRACTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardholderContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.CardholderContractID, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderContractID, value, true); }
		}

		/// <summary> The CardholderContractNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERCONTRACTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardholderContractNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.CardholderContractNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderContractNumber, value, true); }
		}

		/// <summary> The CardholderDateOfBirth property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERDATEOFBIRTH"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CardholderDateOfBirth
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FastcardFieldIndex.CardholderDateOfBirth, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderDateOfBirth, value, true); }
		}

		/// <summary> The CardholderEmail property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDEREMAIL"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardholderEmail
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.CardholderEmail, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderEmail, value, true); }
		}

		/// <summary> The CardholderFirstName property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERFIRSTNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardholderFirstName
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.CardholderFirstName, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderFirstName, value, true); }
		}

		/// <summary> The CardholderFrameOrganizationID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERFRAMEORGANIZATIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardholderFrameOrganizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.CardholderFrameOrganizationID, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderFrameOrganizationID, value, true); }
		}

		/// <summary> The CardholderGroupExpiry property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERGROUPEXPIRY"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CardholderGroupExpiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FastcardFieldIndex.CardholderGroupExpiry, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderGroupExpiry, value, true); }
		}

		/// <summary> The CardholderID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardholderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.CardholderID, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderID, value, true); }
		}

		/// <summary> The CardholderLastName property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERLASTNAME"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardholderLastName
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.CardholderLastName, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderLastName, value, true); }
		}

		/// <summary> The CardholderSchoolYearID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERSCHOOLYEARID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardholderSchoolYearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.CardholderSchoolYearID, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderSchoolYearID, value, true); }
		}

		/// <summary> The CardholderUserGroup property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDHOLDERUSERGROUP"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CardholderUserGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)FastcardFieldIndex.CardholderUserGroup, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardholderUserGroup, value, true); }
		}

		/// <summary> The CardID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)FastcardFieldIndex.CardID, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardPhysicalDetailDescription property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDPHYSICALDETAILDESCRIPTION"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardPhysicalDetailDescription
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.CardPhysicalDetailDescription, true); }
			set	{ SetValue((int)FastcardFieldIndex.CardPhysicalDetailDescription, value, true); }
		}

		/// <summary> The CardPhysicalDetailID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CARDPHYSICALDETAILID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardPhysicalDetailID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.CardPhysicalDetailID, false); }
			set	{ SetValue((int)FastcardFieldIndex.CardPhysicalDetailID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."CLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.ClientID, false); }
			set	{ SetValue((int)FastcardFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DeviceMappingNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."DEVICEMAPPINGNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceMappingNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)FastcardFieldIndex.DeviceMappingNumber, false); }
			set	{ SetValue((int)FastcardFieldIndex.DeviceMappingNumber, value, true); }
		}

		/// <summary> The Expiration property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."EXPIRATION"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Expiration
		{
			get { return (System.DateTime)GetValue((int)FastcardFieldIndex.Expiration, true); }
			set	{ SetValue((int)FastcardFieldIndex.Expiration, value, true); }
		}

		/// <summary> The InventoryState property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."INVENTORYSTATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 InventoryState
		{
			get { return (System.Int32)GetValue((int)FastcardFieldIndex.InventoryState, true); }
			set	{ SetValue((int)FastcardFieldIndex.InventoryState, value, true); }
		}

		/// <summary> The IsBlockAllowed property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ISBLOCKALLOWED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsBlockAllowed
		{
			get { return (Nullable<System.Boolean>)GetValue((int)FastcardFieldIndex.IsBlockAllowed, false); }
			set	{ SetValue((int)FastcardFieldIndex.IsBlockAllowed, value, true); }
		}

		/// <summary> The IsShared property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ISSHARED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsShared
		{
			get { return (System.Boolean)GetValue((int)FastcardFieldIndex.IsShared, true); }
			set	{ SetValue((int)FastcardFieldIndex.IsShared, value, true); }
		}

		/// <summary> The IsUnblockAllowed property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ISUNBLOCKALLOWED"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsUnblockAllowed
		{
			get { return (Nullable<System.Boolean>)GetValue((int)FastcardFieldIndex.IsUnblockAllowed, false); }
			set	{ SetValue((int)FastcardFieldIndex.IsUnblockAllowed, value, true); }
		}

		/// <summary> The NumberOfPrints property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."NUMBEROFPRINTS"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NumberOfPrints
		{
			get { return (Nullable<System.Int64>)GetValue((int)FastcardFieldIndex.NumberOfPrints, false); }
			set	{ SetValue((int)FastcardFieldIndex.NumberOfPrints, value, true); }
		}

		/// <summary> The OrganizationalIdentifier property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ORGANIZATIONALIDENTIFIER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationalIdentifier
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.OrganizationalIdentifier, true); }
			set	{ SetValue((int)FastcardFieldIndex.OrganizationalIdentifier, value, true); }
		}

		/// <summary> The OrganizationNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."ORGANIZATIONNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.OrganizationNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.OrganizationNumber, value, true); }
		}

		/// <summary> The PrintedDate property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."PRINTEDDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PrintedDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FastcardFieldIndex.PrintedDate, false); }
			set	{ SetValue((int)FastcardFieldIndex.PrintedDate, value, true); }
		}

		/// <summary> The PrintedNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."PRINTEDNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PrintedNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.PrintedNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.PrintedNumber, value, true); }
		}

		/// <summary> The ReplacedCardPrintedNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."REPLACEDCARDPRINTEDNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReplacedCardPrintedNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.ReplacedCardPrintedNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.ReplacedCardPrintedNumber, value, true); }
		}

		/// <summary> The SequentialNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."SEQUENTIALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SequentialNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.SequentialNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.SequentialNumber, value, true); }
		}

		/// <summary> The SerialNumber property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."SERIALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SerialNumber
		{
			get { return (System.String)GetValue((int)FastcardFieldIndex.SerialNumber, true); }
			set	{ SetValue((int)FastcardFieldIndex.SerialNumber, value, true); }
		}

		/// <summary> The State property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."STATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CardState State
		{
			get { return (VarioSL.Entities.Enumerations.CardState)GetValue((int)FastcardFieldIndex.State, true); }
			set	{ SetValue((int)FastcardFieldIndex.State, value, true); }
		}

		/// <summary> The UserGroupID property of the Entity Fastcard<br/><br/></summary>
		/// <remarks>Mapped on  view field: "VIEW_SL_FASTCARD"."USERGROUPID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UserGroupID
		{
			get { return (Nullable<System.Int32>)GetValue((int)FastcardFieldIndex.UserGroupID, false); }
			set	{ SetValue((int)FastcardFieldIndex.UserGroupID, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FastcardEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
