﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Protocol'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProtocolEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ProtocolMessageCollection	_protMessages;
		private bool	_alwaysFetchProtMessages, _alreadyFetchedProtMessages;
		private ProtocolFunctionEntity _protocolFunction;
		private bool	_alwaysFetchProtocolFunction, _alreadyFetchedProtocolFunction, _protocolFunctionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ProtocolFunction</summary>
			public static readonly string ProtocolFunction = "ProtocolFunction";
			/// <summary>Member name ProtMessages</summary>
			public static readonly string ProtMessages = "ProtMessages";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProtocolEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProtocolEntity() :base("ProtocolEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		public ProtocolEntity(System.Int64 protocolID):base("ProtocolEntity")
		{
			InitClassFetch(protocolID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProtocolEntity(System.Int64 protocolID, IPrefetchPath prefetchPathToUse):base("ProtocolEntity")
		{
			InitClassFetch(protocolID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="validator">The custom validator object for this ProtocolEntity</param>
		public ProtocolEntity(System.Int64 protocolID, IValidator validator):base("ProtocolEntity")
		{
			InitClassFetch(protocolID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProtocolEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_protMessages = (VarioSL.Entities.CollectionClasses.ProtocolMessageCollection)info.GetValue("_protMessages", typeof(VarioSL.Entities.CollectionClasses.ProtocolMessageCollection));
			_alwaysFetchProtMessages = info.GetBoolean("_alwaysFetchProtMessages");
			_alreadyFetchedProtMessages = info.GetBoolean("_alreadyFetchedProtMessages");
			_protocolFunction = (ProtocolFunctionEntity)info.GetValue("_protocolFunction", typeof(ProtocolFunctionEntity));
			if(_protocolFunction!=null)
			{
				_protocolFunction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_protocolFunctionReturnsNewIfNotFound = info.GetBoolean("_protocolFunctionReturnsNewIfNotFound");
			_alwaysFetchProtocolFunction = info.GetBoolean("_alwaysFetchProtocolFunction");
			_alreadyFetchedProtocolFunction = info.GetBoolean("_alreadyFetchedProtocolFunction");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProtocolFieldIndex)fieldIndex)
			{
				case ProtocolFieldIndex.FunctionID:
					DesetupSyncProtocolFunction(true, false);
					_alreadyFetchedProtocolFunction = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProtMessages = (_protMessages.Count > 0);
			_alreadyFetchedProtocolFunction = (_protocolFunction != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ProtocolFunction":
					toReturn.Add(Relations.ProtocolFunctionEntityUsingFunctionID);
					break;
				case "ProtMessages":
					toReturn.Add(Relations.ProtocolMessageEntityUsingProtocolID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_protMessages", (!this.MarkedForDeletion?_protMessages:null));
			info.AddValue("_alwaysFetchProtMessages", _alwaysFetchProtMessages);
			info.AddValue("_alreadyFetchedProtMessages", _alreadyFetchedProtMessages);
			info.AddValue("_protocolFunction", (!this.MarkedForDeletion?_protocolFunction:null));
			info.AddValue("_protocolFunctionReturnsNewIfNotFound", _protocolFunctionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProtocolFunction", _alwaysFetchProtocolFunction);
			info.AddValue("_alreadyFetchedProtocolFunction", _alreadyFetchedProtocolFunction);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ProtocolFunction":
					_alreadyFetchedProtocolFunction = true;
					this.ProtocolFunction = (ProtocolFunctionEntity)entity;
					break;
				case "ProtMessages":
					_alreadyFetchedProtMessages = true;
					if(entity!=null)
					{
						this.ProtMessages.Add((ProtocolMessageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ProtocolFunction":
					SetupSyncProtocolFunction(relatedEntity);
					break;
				case "ProtMessages":
					_protMessages.Add((ProtocolMessageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ProtocolFunction":
					DesetupSyncProtocolFunction(false, true);
					break;
				case "ProtMessages":
					this.PerformRelatedEntityRemoval(_protMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_protocolFunction!=null)
			{
				toReturn.Add(_protocolFunction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_protMessages);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolID)
		{
			return FetchUsingPK(protocolID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(protocolID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(protocolID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 protocolID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(protocolID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProtocolID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProtocolRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtMessages(bool forceFetch)
		{
			return GetMultiProtMessages(forceFetch, _protMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProtocolMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProtMessages(forceFetch, _protMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProtMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolMessageCollection GetMultiProtMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProtMessages || forceFetch || _alwaysFetchProtMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_protMessages);
				_protMessages.SuppressClearInGetMulti=!forceFetch;
				_protMessages.EntityFactoryToUse = entityFactoryToUse;
				_protMessages.GetMultiManyToOne(this, null, filter);
				_protMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedProtMessages = true;
			}
			return _protMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProtMessages'. These settings will be taken into account
		/// when the property ProtMessages is requested or GetMultiProtMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProtMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_protMessages.SortClauses=sortClauses;
			_protMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ProtocolFunctionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProtocolFunctionEntity' which is related to this entity.</returns>
		public ProtocolFunctionEntity GetSingleProtocolFunction()
		{
			return GetSingleProtocolFunction(false);
		}

		/// <summary> Retrieves the related entity of type 'ProtocolFunctionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProtocolFunctionEntity' which is related to this entity.</returns>
		public virtual ProtocolFunctionEntity GetSingleProtocolFunction(bool forceFetch)
		{
			if( ( !_alreadyFetchedProtocolFunction || forceFetch || _alwaysFetchProtocolFunction) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProtocolFunctionEntityUsingFunctionID);
				ProtocolFunctionEntity newEntity = new ProtocolFunctionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FunctionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProtocolFunctionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_protocolFunctionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProtocolFunction = newEntity;
				_alreadyFetchedProtocolFunction = fetchResult;
			}
			return _protocolFunction;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ProtocolFunction", _protocolFunction);
			toReturn.Add("ProtMessages", _protMessages);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="validator">The validator object for this ProtocolEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 protocolID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(protocolID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_protMessages = new VarioSL.Entities.CollectionClasses.ProtocolMessageCollection();
			_protMessages.SetContainingEntityInfo(this, "Protocol");
			_protocolFunctionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FunctionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FunctionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HostName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PayrollRelevant", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Priority", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProtocolID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReadValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _protocolFunction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProtocolFunction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _protocolFunction, new PropertyChangedEventHandler( OnProtocolFunctionPropertyChanged ), "ProtocolFunction", VarioSL.Entities.RelationClasses.StaticProtocolRelations.ProtocolFunctionEntityUsingFunctionIDStatic, true, signalRelatedEntity, "Protocols", resetFKFields, new int[] { (int)ProtocolFieldIndex.FunctionID } );		
			_protocolFunction = null;
		}
		
		/// <summary> setups the sync logic for member _protocolFunction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProtocolFunction(IEntityCore relatedEntity)
		{
			if(_protocolFunction!=relatedEntity)
			{		
				DesetupSyncProtocolFunction(true, true);
				_protocolFunction = (ProtocolFunctionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _protocolFunction, new PropertyChangedEventHandler( OnProtocolFunctionPropertyChanged ), "ProtocolFunction", VarioSL.Entities.RelationClasses.StaticProtocolRelations.ProtocolFunctionEntityUsingFunctionIDStatic, true, ref _alreadyFetchedProtocolFunction, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProtocolFunctionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="protocolID">PK value for Protocol which data should be fetched into this Protocol object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 protocolID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProtocolFieldIndex.ProtocolID].ForcedCurrentValueWrite(protocolID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProtocolDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProtocolEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProtocolRelations Relations
		{
			get	{ return new ProtocolRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProtocolMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolMessageCollection(), (IEntityRelation)GetRelationsForField("ProtMessages")[0], (int)VarioSL.Entities.EntityType.ProtocolEntity, (int)VarioSL.Entities.EntityType.ProtocolMessageEntity, 0, null, null, null, "ProtMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProtocolFunction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProtocolFunction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProtocolFunctionCollection(), (IEntityRelation)GetRelationsForField("ProtocolFunction")[0], (int)VarioSL.Entities.EntityType.ProtocolEntity, (int)VarioSL.Entities.EntityType.ProtocolFunctionEntity, 0, null, null, null, "ProtocolFunction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ClientID
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFieldIndex.ClientID, false); }
			set	{ SetValue((int)ProtocolFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DeviceNo property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProtocolFieldIndex.DeviceNo, false); }
			set	{ SetValue((int)ProtocolFieldIndex.DeviceNo, value, true); }
		}

		/// <summary> The FunctionDate property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTFCTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> FunctionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProtocolFieldIndex.FunctionDate, false); }
			set	{ SetValue((int)ProtocolFieldIndex.FunctionDate, value, true); }
		}

		/// <summary> The FunctionID property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."FCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 12, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FunctionID
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProtocolFieldIndex.FunctionID, false); }
			set	{ SetValue((int)ProtocolFieldIndex.FunctionID, value, true); }
		}

		/// <summary> The HostName property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."HOSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HostName
		{
			get { return (System.String)GetValue((int)ProtocolFieldIndex.HostName, true); }
			set	{ SetValue((int)ProtocolFieldIndex.HostName, value, true); }
		}

		/// <summary> The MessageCount property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTNUMBEROFMESSAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MessageCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProtocolFieldIndex.MessageCount, false); }
			set	{ SetValue((int)ProtocolFieldIndex.MessageCount, value, true); }
		}

		/// <summary> The PayrollRelevant property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTPAYROLLRELEVANCY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> PayrollRelevant
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProtocolFieldIndex.PayrollRelevant, false); }
			set	{ SetValue((int)ProtocolFieldIndex.PayrollRelevant, value, true); }
		}

		/// <summary> The Priority property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTPRIO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Priority
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFieldIndex.Priority, false); }
			set	{ SetValue((int)ProtocolFieldIndex.Priority, value, true); }
		}

		/// <summary> The ProtocolID property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 15, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ProtocolID
		{
			get { return (System.Int64)GetValue((int)ProtocolFieldIndex.ProtocolID, true); }
			set	{ SetValue((int)ProtocolFieldIndex.ProtocolID, value, true); }
		}

		/// <summary> The ReadValue property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTREADVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ReadValue
		{
			get { return (Nullable<System.Int16>)GetValue((int)ProtocolFieldIndex.ReadValue, false); }
			set	{ SetValue((int)ProtocolFieldIndex.ReadValue, value, true); }
		}

		/// <summary> The UserName property of the Entity Protocol<br/><br/></summary>
		/// <remarks>Mapped on  table field: "PROTOCOL"."PROTUSERNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserName
		{
			get { return (System.String)GetValue((int)ProtocolFieldIndex.UserName, true); }
			set	{ SetValue((int)ProtocolFieldIndex.UserName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ProtocolMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProtMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProtocolMessageCollection ProtMessages
		{
			get	{ return GetMultiProtMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProtMessages. When set to true, ProtMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProtMessages is accessed. You can always execute/ a forced fetch by calling GetMultiProtMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtMessages
		{
			get	{ return _alwaysFetchProtMessages; }
			set	{ _alwaysFetchProtMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProtMessages already has been fetched. Setting this property to false when ProtMessages has been fetched
		/// will clear the ProtMessages collection well. Setting this property to true while ProtMessages hasn't been fetched disables lazy loading for ProtMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtMessages
		{
			get { return _alreadyFetchedProtMessages;}
			set 
			{
				if(_alreadyFetchedProtMessages && !value && (_protMessages != null))
				{
					_protMessages.Clear();
				}
				_alreadyFetchedProtMessages = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ProtocolFunctionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProtocolFunction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProtocolFunctionEntity ProtocolFunction
		{
			get	{ return GetSingleProtocolFunction(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProtocolFunction(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Protocols", "ProtocolFunction", _protocolFunction, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProtocolFunction. When set to true, ProtocolFunction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProtocolFunction is accessed. You can always execute a forced fetch by calling GetSingleProtocolFunction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProtocolFunction
		{
			get	{ return _alwaysFetchProtocolFunction; }
			set	{ _alwaysFetchProtocolFunction = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProtocolFunction already has been fetched. Setting this property to false when ProtocolFunction has been fetched
		/// will set ProtocolFunction to null as well. Setting this property to true while ProtocolFunction hasn't been fetched disables lazy loading for ProtocolFunction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProtocolFunction
		{
			get { return _alreadyFetchedProtocolFunction;}
			set 
			{
				if(_alreadyFetchedProtocolFunction && !value)
				{
					this.ProtocolFunction = null;
				}
				_alreadyFetchedProtocolFunction = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProtocolFunction is not found
		/// in the database. When set to true, ProtocolFunction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProtocolFunctionReturnsNewIfNotFound
		{
			get	{ return _protocolFunctionReturnsNewIfNotFound; }
			set { _protocolFunctionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProtocolEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
