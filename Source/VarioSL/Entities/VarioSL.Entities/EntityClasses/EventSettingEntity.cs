﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EventSetting'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class EventSettingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EmailEventEntity _emailEvent;
		private bool	_alwaysFetchEmailEvent, _alreadyFetchedEmailEvent, _emailEventReturnsNewIfNotFound;
		private MessageTypeEntity _messageType;
		private bool	_alwaysFetchMessageType, _alreadyFetchedMessageType, _messageTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EmailEvent</summary>
			public static readonly string EmailEvent = "EmailEvent";
			/// <summary>Member name MessageType</summary>
			public static readonly string MessageType = "MessageType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EventSettingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public EventSettingEntity() :base("EventSettingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		public EventSettingEntity(System.Int64 emailEventID, System.Int32 messageTypeID):base("EventSettingEntity")
		{
			InitClassFetch(emailEventID, messageTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EventSettingEntity(System.Int64 emailEventID, System.Int32 messageTypeID, IPrefetchPath prefetchPathToUse):base("EventSettingEntity")
		{
			InitClassFetch(emailEventID, messageTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="validator">The custom validator object for this EventSettingEntity</param>
		public EventSettingEntity(System.Int64 emailEventID, System.Int32 messageTypeID, IValidator validator):base("EventSettingEntity")
		{
			InitClassFetch(emailEventID, messageTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EventSettingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_emailEvent = (EmailEventEntity)info.GetValue("_emailEvent", typeof(EmailEventEntity));
			if(_emailEvent!=null)
			{
				_emailEvent.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_emailEventReturnsNewIfNotFound = info.GetBoolean("_emailEventReturnsNewIfNotFound");
			_alwaysFetchEmailEvent = info.GetBoolean("_alwaysFetchEmailEvent");
			_alreadyFetchedEmailEvent = info.GetBoolean("_alreadyFetchedEmailEvent");

			_messageType = (MessageTypeEntity)info.GetValue("_messageType", typeof(MessageTypeEntity));
			if(_messageType!=null)
			{
				_messageType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messageTypeReturnsNewIfNotFound = info.GetBoolean("_messageTypeReturnsNewIfNotFound");
			_alwaysFetchMessageType = info.GetBoolean("_alwaysFetchMessageType");
			_alreadyFetchedMessageType = info.GetBoolean("_alreadyFetchedMessageType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EventSettingFieldIndex)fieldIndex)
			{
				case EventSettingFieldIndex.EmailEventID:
					DesetupSyncEmailEvent(true, false);
					_alreadyFetchedEmailEvent = false;
					break;
				case EventSettingFieldIndex.MessageTypeID:
					DesetupSyncMessageType(true, false);
					_alreadyFetchedMessageType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEmailEvent = (_emailEvent != null);
			_alreadyFetchedMessageType = (_messageType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EmailEvent":
					toReturn.Add(Relations.EmailEventEntityUsingEmailEventID);
					break;
				case "MessageType":
					toReturn.Add(Relations.MessageTypeEntityUsingMessageTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_emailEvent", (!this.MarkedForDeletion?_emailEvent:null));
			info.AddValue("_emailEventReturnsNewIfNotFound", _emailEventReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEmailEvent", _alwaysFetchEmailEvent);
			info.AddValue("_alreadyFetchedEmailEvent", _alreadyFetchedEmailEvent);
			info.AddValue("_messageType", (!this.MarkedForDeletion?_messageType:null));
			info.AddValue("_messageTypeReturnsNewIfNotFound", _messageTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessageType", _alwaysFetchMessageType);
			info.AddValue("_alreadyFetchedMessageType", _alreadyFetchedMessageType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EmailEvent":
					_alreadyFetchedEmailEvent = true;
					this.EmailEvent = (EmailEventEntity)entity;
					break;
				case "MessageType":
					_alreadyFetchedMessageType = true;
					this.MessageType = (MessageTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					SetupSyncEmailEvent(relatedEntity);
					break;
				case "MessageType":
					SetupSyncMessageType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EmailEvent":
					DesetupSyncEmailEvent(false, true);
					break;
				case "MessageType":
					DesetupSyncMessageType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_emailEvent!=null)
			{
				toReturn.Add(_emailEvent);
			}
			if(_messageType!=null)
			{
				toReturn.Add(_messageType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int32 messageTypeID)
		{
			return FetchUsingPK(emailEventID, messageTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int32 messageTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(emailEventID, messageTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int32 messageTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(emailEventID, messageTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 emailEventID, System.Int32 messageTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(emailEventID, messageTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EmailEventID, this.MessageTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EventSettingRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public EmailEventEntity GetSingleEmailEvent()
		{
			return GetSingleEmailEvent(false);
		}

		/// <summary> Retrieves the related entity of type 'EmailEventEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EmailEventEntity' which is related to this entity.</returns>
		public virtual EmailEventEntity GetSingleEmailEvent(bool forceFetch)
		{
			if( ( !_alreadyFetchedEmailEvent || forceFetch || _alwaysFetchEmailEvent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EmailEventEntityUsingEmailEventID);
				EmailEventEntity newEntity = new EmailEventEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EmailEventID);
				}
				if(fetchResult)
				{
					newEntity = (EmailEventEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_emailEventReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EmailEvent = newEntity;
				_alreadyFetchedEmailEvent = fetchResult;
			}
			return _emailEvent;
		}


		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public MessageTypeEntity GetSingleMessageType()
		{
			return GetSingleMessageType(false);
		}

		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public virtual MessageTypeEntity GetSingleMessageType(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessageType || forceFetch || _alwaysFetchMessageType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessageTypeEntityUsingMessageTypeID);
				MessageTypeEntity newEntity = new MessageTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessageTypeID);
				}
				if(fetchResult)
				{
					newEntity = (MessageTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messageTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MessageType = newEntity;
				_alreadyFetchedMessageType = fetchResult;
			}
			return _messageType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EmailEvent", _emailEvent);
			toReturn.Add("MessageType", _messageType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="validator">The validator object for this EventSettingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 emailEventID, System.Int32 messageTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(emailEventID, messageTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_emailEventReturnsNewIfNotFound = false;
			_messageTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailEventID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsActive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsConfigurable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _emailEvent</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEmailEvent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticEventSettingRelations.EmailEventEntityUsingEmailEventIDStatic, true, signalRelatedEntity, "EventSettings", resetFKFields, new int[] { (int)EventSettingFieldIndex.EmailEventID } );		
			_emailEvent = null;
		}
		
		/// <summary> setups the sync logic for member _emailEvent</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEmailEvent(IEntityCore relatedEntity)
		{
			if(_emailEvent!=relatedEntity)
			{		
				DesetupSyncEmailEvent(true, true);
				_emailEvent = (EmailEventEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _emailEvent, new PropertyChangedEventHandler( OnEmailEventPropertyChanged ), "EmailEvent", VarioSL.Entities.RelationClasses.StaticEventSettingRelations.EmailEventEntityUsingEmailEventIDStatic, true, ref _alreadyFetchedEmailEvent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEmailEventPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _messageType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessageType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _messageType, new PropertyChangedEventHandler( OnMessageTypePropertyChanged ), "MessageType", VarioSL.Entities.RelationClasses.StaticEventSettingRelations.MessageTypeEntityUsingMessageTypeIDStatic, true, signalRelatedEntity, "EventSettings", resetFKFields, new int[] { (int)EventSettingFieldIndex.MessageTypeID } );		
			_messageType = null;
		}
		
		/// <summary> setups the sync logic for member _messageType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessageType(IEntityCore relatedEntity)
		{
			if(_messageType!=relatedEntity)
			{		
				DesetupSyncMessageType(true, true);
				_messageType = (MessageTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _messageType, new PropertyChangedEventHandler( OnMessageTypePropertyChanged ), "MessageType", VarioSL.Entities.RelationClasses.StaticEventSettingRelations.MessageTypeEntityUsingMessageTypeIDStatic, true, ref _alreadyFetchedMessageType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessageTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="emailEventID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="messageTypeID">PK value for EventSetting which data should be fetched into this EventSetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 emailEventID, System.Int32 messageTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EventSettingFieldIndex.EmailEventID].ForcedCurrentValueWrite(emailEventID);
				this.Fields[(int)EventSettingFieldIndex.MessageTypeID].ForcedCurrentValueWrite(messageTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEventSettingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EventSettingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EventSettingRelations Relations
		{
			get	{ return new EventSettingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEvent'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEvent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventCollection(), (IEntityRelation)GetRelationsForField("EmailEvent")[0], (int)VarioSL.Entities.EntityType.EventSettingEntity, (int)VarioSL.Entities.EntityType.EmailEventEntity, 0, null, null, null, "EmailEvent", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageTypeCollection(), (IEntityRelation)GetRelationsForField("MessageType")[0], (int)VarioSL.Entities.EntityType.EventSettingEntity, (int)VarioSL.Entities.EntityType.MessageTypeEntity, 0, null, null, null, "MessageType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EmailEventID property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."EMAILEVENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 EmailEventID
		{
			get { return (System.Int64)GetValue((int)EventSettingFieldIndex.EmailEventID, true); }
			set	{ SetValue((int)EventSettingFieldIndex.EmailEventID, value, true); }
		}

		/// <summary> The IsActive property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."ISACTIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsActive
		{
			get { return (System.Boolean)GetValue((int)EventSettingFieldIndex.IsActive, true); }
			set	{ SetValue((int)EventSettingFieldIndex.IsActive, value, true); }
		}

		/// <summary> The IsConfigurable property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."ISCONFIGURABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsConfigurable
		{
			get { return (System.Boolean)GetValue((int)EventSettingFieldIndex.IsConfigurable, true); }
			set	{ SetValue((int)EventSettingFieldIndex.IsConfigurable, value, true); }
		}

		/// <summary> The LastModified property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)EventSettingFieldIndex.LastModified, true); }
			set	{ SetValue((int)EventSettingFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)EventSettingFieldIndex.LastUser, true); }
			set	{ SetValue((int)EventSettingFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageTypeID property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."MESSAGETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 MessageTypeID
		{
			get { return (System.Int32)GetValue((int)EventSettingFieldIndex.MessageTypeID, true); }
			set	{ SetValue((int)EventSettingFieldIndex.MessageTypeID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity EventSetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_EVENTSETTING"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)EventSettingFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)EventSettingFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'EmailEventEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEmailEvent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual EmailEventEntity EmailEvent
		{
			get	{ return GetSingleEmailEvent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEmailEvent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EventSettings", "EmailEvent", _emailEvent, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEvent. When set to true, EmailEvent is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEvent is accessed. You can always execute a forced fetch by calling GetSingleEmailEvent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEvent
		{
			get	{ return _alwaysFetchEmailEvent; }
			set	{ _alwaysFetchEmailEvent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEvent already has been fetched. Setting this property to false when EmailEvent has been fetched
		/// will set EmailEvent to null as well. Setting this property to true while EmailEvent hasn't been fetched disables lazy loading for EmailEvent</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEvent
		{
			get { return _alreadyFetchedEmailEvent;}
			set 
			{
				if(_alreadyFetchedEmailEvent && !value)
				{
					this.EmailEvent = null;
				}
				_alreadyFetchedEmailEvent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EmailEvent is not found
		/// in the database. When set to true, EmailEvent will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool EmailEventReturnsNewIfNotFound
		{
			get	{ return _emailEventReturnsNewIfNotFound; }
			set { _emailEventReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessageTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessageType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MessageTypeEntity MessageType
		{
			get	{ return GetSingleMessageType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessageType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EventSettings", "MessageType", _messageType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MessageType. When set to true, MessageType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageType is accessed. You can always execute a forced fetch by calling GetSingleMessageType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageType
		{
			get	{ return _alwaysFetchMessageType; }
			set	{ _alwaysFetchMessageType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageType already has been fetched. Setting this property to false when MessageType has been fetched
		/// will set MessageType to null as well. Setting this property to true while MessageType hasn't been fetched disables lazy loading for MessageType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageType
		{
			get { return _alreadyFetchedMessageType;}
			set 
			{
				if(_alreadyFetchedMessageType && !value)
				{
					this.MessageType = null;
				}
				_alreadyFetchedMessageType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MessageType is not found
		/// in the database. When set to true, MessageType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MessageTypeReturnsNewIfNotFound
		{
			get	{ return _messageTypeReturnsNewIfNotFound; }
			set { _messageTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.EventSettingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
