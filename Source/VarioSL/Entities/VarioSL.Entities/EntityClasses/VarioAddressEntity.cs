﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VarioAddress'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VarioAddressEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.UserListCollection	_userLists;
		private bool	_alwaysFetchUserLists, _alreadyFetchedUserLists;
		private DebtorEntity _debtor;
		private bool	_alwaysFetchDebtor, _alreadyFetchedDebtor, _debtorReturnsNewIfNotFound;
		private DepotEntity _depot;
		private bool	_alwaysFetchDepot, _alreadyFetchedDepot, _depotReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserLists</summary>
			public static readonly string UserLists = "UserLists";
			/// <summary>Member name Debtor</summary>
			public static readonly string Debtor = "Debtor";
			/// <summary>Member name Depot</summary>
			public static readonly string Depot = "Depot";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VarioAddressEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VarioAddressEntity() :base("VarioAddressEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		public VarioAddressEntity(System.Int64 addressID):base("VarioAddressEntity")
		{
			InitClassFetch(addressID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VarioAddressEntity(System.Int64 addressID, IPrefetchPath prefetchPathToUse):base("VarioAddressEntity")
		{
			InitClassFetch(addressID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="validator">The custom validator object for this VarioAddressEntity</param>
		public VarioAddressEntity(System.Int64 addressID, IValidator validator):base("VarioAddressEntity")
		{
			InitClassFetch(addressID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VarioAddressEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userLists = (VarioSL.Entities.CollectionClasses.UserListCollection)info.GetValue("_userLists", typeof(VarioSL.Entities.CollectionClasses.UserListCollection));
			_alwaysFetchUserLists = info.GetBoolean("_alwaysFetchUserLists");
			_alreadyFetchedUserLists = info.GetBoolean("_alreadyFetchedUserLists");
			_debtor = (DebtorEntity)info.GetValue("_debtor", typeof(DebtorEntity));
			if(_debtor!=null)
			{
				_debtor.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_debtorReturnsNewIfNotFound = info.GetBoolean("_debtorReturnsNewIfNotFound");
			_alwaysFetchDebtor = info.GetBoolean("_alwaysFetchDebtor");
			_alreadyFetchedDebtor = info.GetBoolean("_alreadyFetchedDebtor");

			_depot = (DepotEntity)info.GetValue("_depot", typeof(DepotEntity));
			if(_depot!=null)
			{
				_depot.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_depotReturnsNewIfNotFound = info.GetBoolean("_depotReturnsNewIfNotFound");
			_alwaysFetchDepot = info.GetBoolean("_alwaysFetchDepot");
			_alreadyFetchedDepot = info.GetBoolean("_alreadyFetchedDepot");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserLists = (_userLists.Count > 0);
			_alreadyFetchedDebtor = (_debtor != null);
			_alreadyFetchedDepot = (_depot != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserLists":
					toReturn.Add(Relations.UserListEntityUsingAddressID);
					break;
				case "Debtor":
					toReturn.Add(Relations.DebtorEntityUsingAddressID);
					break;
				case "Depot":
					toReturn.Add(Relations.DepotEntityUsingAddressID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userLists", (!this.MarkedForDeletion?_userLists:null));
			info.AddValue("_alwaysFetchUserLists", _alwaysFetchUserLists);
			info.AddValue("_alreadyFetchedUserLists", _alreadyFetchedUserLists);

			info.AddValue("_debtor", (!this.MarkedForDeletion?_debtor:null));
			info.AddValue("_debtorReturnsNewIfNotFound", _debtorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDebtor", _alwaysFetchDebtor);
			info.AddValue("_alreadyFetchedDebtor", _alreadyFetchedDebtor);

			info.AddValue("_depot", (!this.MarkedForDeletion?_depot:null));
			info.AddValue("_depotReturnsNewIfNotFound", _depotReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDepot", _alwaysFetchDepot);
			info.AddValue("_alreadyFetchedDepot", _alreadyFetchedDepot);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserLists":
					_alreadyFetchedUserLists = true;
					if(entity!=null)
					{
						this.UserLists.Add((UserListEntity)entity);
					}
					break;
				case "Debtor":
					_alreadyFetchedDebtor = true;
					this.Debtor = (DebtorEntity)entity;
					break;
				case "Depot":
					_alreadyFetchedDepot = true;
					this.Depot = (DepotEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserLists":
					_userLists.Add((UserListEntity)relatedEntity);
					break;
				case "Debtor":
					SetupSyncDebtor(relatedEntity);
					break;
				case "Depot":
					SetupSyncDepot(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserLists":
					this.PerformRelatedEntityRemoval(_userLists, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Debtor":
					DesetupSyncDebtor(false, true);
					break;
				case "Depot":
					DesetupSyncDepot(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_debtor!=null)
			{
				toReturn.Add(_debtor);
			}
			if(_depot!=null)
			{
				toReturn.Add(_depot);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_userLists);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID)
		{
			return FetchUsingPK(addressID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(addressID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(addressID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(addressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AddressID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VarioAddressRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch)
		{
			return GetMultiUserLists(forceFetch, _userLists.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserLists(forceFetch, _userLists.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserLists(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection GetMultiUserLists(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserLists || forceFetch || _alwaysFetchUserLists) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userLists);
				_userLists.SuppressClearInGetMulti=!forceFetch;
				_userLists.EntityFactoryToUse = entityFactoryToUse;
				_userLists.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_userLists.SuppressClearInGetMulti=false;
				_alreadyFetchedUserLists = true;
			}
			return _userLists;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserLists'. These settings will be taken into account
		/// when the property UserLists is requested or GetMultiUserLists is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserLists(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userLists.SortClauses=sortClauses;
			_userLists.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleDebtor()
		{
			return GetSingleDebtor(false);
		}
		
		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleDebtor(bool forceFetch)
		{
			if( ( !_alreadyFetchedDebtor || forceFetch || _alwaysFetchDebtor) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingAddressID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCAddressID(this.AddressID);
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_debtorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Debtor = newEntity;
				_alreadyFetchedDebtor = fetchResult;
			}
			return _debtor;
		}

		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public DepotEntity GetSingleDepot()
		{
			return GetSingleDepot(false);
		}
		
		/// <summary> Retrieves the related entity of type 'DepotEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DepotEntity' which is related to this entity.</returns>
		public virtual DepotEntity GetSingleDepot(bool forceFetch)
		{
			if( ( !_alreadyFetchedDepot || forceFetch || _alwaysFetchDepot) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DepotEntityUsingAddressID);
				DepotEntity newEntity = new DepotEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCAddressID(this.AddressID);
				}
				if(fetchResult)
				{
					newEntity = (DepotEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_depotReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Depot = newEntity;
				_alreadyFetchedDepot = fetchResult;
			}
			return _depot;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserLists", _userLists);
			toReturn.Add("Debtor", _debtor);
			toReturn.Add("Depot", _depot);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="validator">The validator object for this VarioAddressEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 addressID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(addressID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_userLists = new VarioSL.Entities.CollectionClasses.UserListCollection();
			_userLists.SetContainingEntityInfo(this, "VarioAddress");
			_debtorReturnsNewIfNotFound = false;
			_depotReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Country", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostalCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrivateMobilePhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrivatePhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Street", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StreetNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _debtor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDebtor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticVarioAddressRelations.DebtorEntityUsingAddressIDStatic, false, signalRelatedEntity, "Address", false, new int[] { (int)VarioAddressFieldIndex.AddressID } );
			_debtor = null;
		}
	
		/// <summary> setups the sync logic for member _debtor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDebtor(IEntityCore relatedEntity)
		{
			if(_debtor!=relatedEntity)
			{
				DesetupSyncDebtor(true, true);
				_debtor = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticVarioAddressRelations.DebtorEntityUsingAddressIDStatic, false, ref _alreadyFetchedDebtor, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDebtorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _depot</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDepot(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticVarioAddressRelations.DepotEntityUsingAddressIDStatic, false, signalRelatedEntity, "Address", false, new int[] { (int)VarioAddressFieldIndex.AddressID } );
			_depot = null;
		}
	
		/// <summary> setups the sync logic for member _depot</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDepot(IEntityCore relatedEntity)
		{
			if(_depot!=relatedEntity)
			{
				DesetupSyncDepot(true, true);
				_depot = (DepotEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _depot, new PropertyChangedEventHandler( OnDepotPropertyChanged ), "Depot", VarioSL.Entities.RelationClasses.StaticVarioAddressRelations.DepotEntityUsingAddressIDStatic, false, ref _alreadyFetchedDepot, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDepotPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="addressID">PK value for VarioAddress which data should be fetched into this VarioAddress object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VarioAddressFieldIndex.AddressID].ForcedCurrentValueWrite(addressID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVarioAddressDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VarioAddressEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VarioAddressRelations Relations
		{
			get	{ return new VarioAddressRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserLists
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserLists")[0], (int)VarioSL.Entities.EntityType.VarioAddressEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserLists", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtor
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtor")[0], (int)VarioSL.Entities.EntityType.VarioAddressEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Depot'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDepot
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DepotCollection(), (IEntityRelation)GetRelationsForField("Depot")[0], (int)VarioSL.Entities.EntityType.VarioAddressEntity, (int)VarioSL.Entities.EntityType.DepotEntity, 0, null, null, null, "Depot", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AddressID property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AddressID
		{
			get { return (System.Int64)GetValue((int)VarioAddressFieldIndex.AddressID, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.AddressID, value, true); }
		}

		/// <summary> The City property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."CITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.City, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.City, value, true); }
		}

		/// <summary> The Country property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."COUNTRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Country
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.Country, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.Country, value, true); }
		}

		/// <summary> The Email property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.Email, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.Email, value, true); }
		}

		/// <summary> The Fax property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."FAX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fax
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.Fax, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.Fax, value, true); }
		}

		/// <summary> The PostalCode property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."POSTALCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostalCode
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.PostalCode, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.PostalCode, value, true); }
		}

		/// <summary> The PrivateMobilePhoneNumber property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."PRIVATEMOBILEPHONENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrivateMobilePhoneNumber
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.PrivateMobilePhoneNumber, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.PrivateMobilePhoneNumber, value, true); }
		}

		/// <summary> The PrivatePhoneNumber property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."PRIVATEPHONENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrivatePhoneNumber
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.PrivatePhoneNumber, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.PrivatePhoneNumber, value, true); }
		}

		/// <summary> The Street property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."STREET"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Street
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.Street, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.Street, value, true); }
		}

		/// <summary> The StreetNumber property of the Entity VarioAddress<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_ADDRESS"."HOUSENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StreetNumber
		{
			get { return (System.String)GetValue((int)VarioAddressFieldIndex.StreetNumber, true); }
			set	{ SetValue((int)VarioAddressFieldIndex.StreetNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'UserListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserLists()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserListCollection UserLists
		{
			get	{ return GetMultiUserLists(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserLists. When set to true, UserLists is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserLists is accessed. You can always execute/ a forced fetch by calling GetMultiUserLists(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserLists
		{
			get	{ return _alwaysFetchUserLists; }
			set	{ _alwaysFetchUserLists = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserLists already has been fetched. Setting this property to false when UserLists has been fetched
		/// will clear the UserLists collection well. Setting this property to true while UserLists hasn't been fetched disables lazy loading for UserLists</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserLists
		{
			get { return _alreadyFetchedUserLists;}
			set 
			{
				if(_alreadyFetchedUserLists && !value && (_userLists != null))
				{
					_userLists.Clear();
				}
				_alreadyFetchedUserLists = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDebtor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity Debtor
		{
			get	{ return GetSingleDebtor(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncDebtor(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_debtor !=null);
						DesetupSyncDebtor(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Debtor");
						}
					}
					else
					{
						if(_debtor!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Address");
							SetupSyncDebtor(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Debtor. When set to true, Debtor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtor is accessed. You can always execute a forced fetch by calling GetSingleDebtor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtor
		{
			get	{ return _alwaysFetchDebtor; }
			set	{ _alwaysFetchDebtor = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Debtor already has been fetched. Setting this property to false when Debtor has been fetched
		/// will set Debtor to null as well. Setting this property to true while Debtor hasn't been fetched disables lazy loading for Debtor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtor
		{
			get { return _alreadyFetchedDebtor;}
			set 
			{
				if(_alreadyFetchedDebtor && !value)
				{
					this.Debtor = null;
				}
				_alreadyFetchedDebtor = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Debtor is not found
		/// in the database. When set to true, Debtor will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DebtorReturnsNewIfNotFound
		{
			get	{ return _debtorReturnsNewIfNotFound; }
			set	{ _debtorReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'DepotEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDepot()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DepotEntity Depot
		{
			get	{ return GetSingleDepot(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncDepot(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_depot !=null);
						DesetupSyncDepot(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Depot");
						}
					}
					else
					{
						if(_depot!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Address");
							SetupSyncDepot(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Depot. When set to true, Depot is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Depot is accessed. You can always execute a forced fetch by calling GetSingleDepot(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDepot
		{
			get	{ return _alwaysFetchDepot; }
			set	{ _alwaysFetchDepot = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Depot already has been fetched. Setting this property to false when Depot has been fetched
		/// will set Depot to null as well. Setting this property to true while Depot hasn't been fetched disables lazy loading for Depot</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDepot
		{
			get { return _alreadyFetchedDepot;}
			set 
			{
				if(_alreadyFetchedDepot && !value)
				{
					this.Depot = null;
				}
				_alreadyFetchedDepot = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Depot is not found
		/// in the database. When set to true, Depot will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DepotReturnsNewIfNotFound
		{
			get	{ return _depotReturnsNewIfNotFound; }
			set	{ _depotReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VarioAddressEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
