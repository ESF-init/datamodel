﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AttributeValue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AttributeValueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AreaListCollection	_areaList;
		private bool	_alwaysFetchAreaList, _alreadyFetchedAreaList;
		private VarioSL.Entities.CollectionClasses.AttributeValueListCollection	_attributeValueList;
		private bool	_alwaysFetchAttributeValueList, _alreadyFetchedAttributeValueList;
		private VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection	_businessRuleResultUserGroup;
		private bool	_alwaysFetchBusinessRuleResultUserGroup, _alreadyFetchedBusinessRuleResultUserGroup;
		private VarioSL.Entities.CollectionClasses.ChoiceCollection	_choices;
		private bool	_alwaysFetchChoices, _alreadyFetchedChoices;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_directionAttributeValueFareMatrixEntries;
		private bool	_alwaysFetchDirectionAttributeValueFareMatrixEntries, _alreadyFetchedDirectionAttributeValueFareMatrixEntries;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_distanceAttributeValueFareMatrixEntries;
		private bool	_alwaysFetchDistanceAttributeValueFareMatrixEntries, _alreadyFetchedDistanceAttributeValueFareMatrixEntries;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_lineFilterAttributeValueFareMatrixEntries;
		private bool	_alwaysFetchLineFilterAttributeValueFareMatrixEntries, _alreadyFetchedLineFilterAttributeValueFareMatrixEntries;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_tariffAttributeValueFareMatrixEntries;
		private bool	_alwaysFetchTariffAttributeValueFareMatrixEntries, _alreadyFetchedTariffAttributeValueFareMatrixEntries;
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_useDaysAttributeFareMatrixEntries;
		private bool	_alwaysFetchUseDaysAttributeFareMatrixEntries, _alreadyFetchedUseDaysAttributeFareMatrixEntries;
		private VarioSL.Entities.CollectionClasses.FareStageListCollection	_fareStageLists;
		private bool	_alwaysFetchFareStageLists, _alreadyFetchedFareStageLists;
		private VarioSL.Entities.CollectionClasses.FareTableEntryCollection	_fareTableEntries;
		private bool	_alwaysFetchFareTableEntries, _alreadyFetchedFareTableEntries;
		private VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection	_key1AttributeTransforms;
		private bool	_alwaysFetchKey1AttributeTransforms, _alreadyFetchedKey1AttributeTransforms;
		private VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection	_key2AttributeTransforms;
		private bool	_alwaysFetchKey2AttributeTransforms, _alreadyFetchedKey2AttributeTransforms;
		private VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection	_parameterAttributeValues;
		private bool	_alwaysFetchParameterAttributeValues, _alreadyFetchedParameterAttributeValues;
		private VarioSL.Entities.CollectionClasses.RouteCollection	_tariffAttributeRoutes;
		private bool	_alwaysFetchTariffAttributeRoutes, _alreadyFetchedTariffAttributeRoutes;
		private VarioSL.Entities.CollectionClasses.RouteCollection	_ticketGroupAttributeRoutes;
		private bool	_alwaysFetchTicketGroupAttributeRoutes, _alreadyFetchedTicketGroupAttributeRoutes;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProduct;
		private bool	_alwaysFetchVdvProduct, _alreadyFetchedVdvProduct;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaAttributeValueList;
		private bool	_alwaysFetchTicketCollectionViaAttributeValueList, _alreadyFetchedTicketCollectionViaAttributeValueList;
		private AttributeEntity _attribute;
		private bool	_alwaysFetchAttribute, _alreadyFetchedAttribute, _attributeReturnsNewIfNotFound;
		private LogoEntity _logo1;
		private bool	_alwaysFetchLogo1, _alreadyFetchedLogo1, _logo1ReturnsNewIfNotFound;
		private LogoEntity _logo2;
		private bool	_alwaysFetchLogo2, _alreadyFetchedLogo2, _logo2ReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Attribute</summary>
			public static readonly string Attribute = "Attribute";
			/// <summary>Member name Logo1</summary>
			public static readonly string Logo1 = "Logo1";
			/// <summary>Member name Logo2</summary>
			public static readonly string Logo2 = "Logo2";
			/// <summary>Member name AreaList</summary>
			public static readonly string AreaList = "AreaList";
			/// <summary>Member name AttributeValueList</summary>
			public static readonly string AttributeValueList = "AttributeValueList";
			/// <summary>Member name BusinessRuleResultUserGroup</summary>
			public static readonly string BusinessRuleResultUserGroup = "BusinessRuleResultUserGroup";
			/// <summary>Member name Choices</summary>
			public static readonly string Choices = "Choices";
			/// <summary>Member name DirectionAttributeValueFareMatrixEntries</summary>
			public static readonly string DirectionAttributeValueFareMatrixEntries = "DirectionAttributeValueFareMatrixEntries";
			/// <summary>Member name DistanceAttributeValueFareMatrixEntries</summary>
			public static readonly string DistanceAttributeValueFareMatrixEntries = "DistanceAttributeValueFareMatrixEntries";
			/// <summary>Member name LineFilterAttributeValueFareMatrixEntries</summary>
			public static readonly string LineFilterAttributeValueFareMatrixEntries = "LineFilterAttributeValueFareMatrixEntries";
			/// <summary>Member name TariffAttributeValueFareMatrixEntries</summary>
			public static readonly string TariffAttributeValueFareMatrixEntries = "TariffAttributeValueFareMatrixEntries";
			/// <summary>Member name UseDaysAttributeFareMatrixEntries</summary>
			public static readonly string UseDaysAttributeFareMatrixEntries = "UseDaysAttributeFareMatrixEntries";
			/// <summary>Member name FareStageLists</summary>
			public static readonly string FareStageLists = "FareStageLists";
			/// <summary>Member name FareTableEntries</summary>
			public static readonly string FareTableEntries = "FareTableEntries";
			/// <summary>Member name Key1AttributeTransforms</summary>
			public static readonly string Key1AttributeTransforms = "Key1AttributeTransforms";
			/// <summary>Member name Key2AttributeTransforms</summary>
			public static readonly string Key2AttributeTransforms = "Key2AttributeTransforms";
			/// <summary>Member name ParameterAttributeValues</summary>
			public static readonly string ParameterAttributeValues = "ParameterAttributeValues";
			/// <summary>Member name TariffAttributeRoutes</summary>
			public static readonly string TariffAttributeRoutes = "TariffAttributeRoutes";
			/// <summary>Member name TicketGroupAttributeRoutes</summary>
			public static readonly string TicketGroupAttributeRoutes = "TicketGroupAttributeRoutes";
			/// <summary>Member name VdvProduct</summary>
			public static readonly string VdvProduct = "VdvProduct";
			/// <summary>Member name TicketCollectionViaAttributeValueList</summary>
			public static readonly string TicketCollectionViaAttributeValueList = "TicketCollectionViaAttributeValueList";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AttributeValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AttributeValueEntity() :base("AttributeValueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		public AttributeValueEntity(System.Int64 attributeValueID):base("AttributeValueEntity")
		{
			InitClassFetch(attributeValueID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttributeValueEntity(System.Int64 attributeValueID, IPrefetchPath prefetchPathToUse):base("AttributeValueEntity")
		{
			InitClassFetch(attributeValueID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="validator">The custom validator object for this AttributeValueEntity</param>
		public AttributeValueEntity(System.Int64 attributeValueID, IValidator validator):base("AttributeValueEntity")
		{
			InitClassFetch(attributeValueID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttributeValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaList = (VarioSL.Entities.CollectionClasses.AreaListCollection)info.GetValue("_areaList", typeof(VarioSL.Entities.CollectionClasses.AreaListCollection));
			_alwaysFetchAreaList = info.GetBoolean("_alwaysFetchAreaList");
			_alreadyFetchedAreaList = info.GetBoolean("_alreadyFetchedAreaList");

			_attributeValueList = (VarioSL.Entities.CollectionClasses.AttributeValueListCollection)info.GetValue("_attributeValueList", typeof(VarioSL.Entities.CollectionClasses.AttributeValueListCollection));
			_alwaysFetchAttributeValueList = info.GetBoolean("_alwaysFetchAttributeValueList");
			_alreadyFetchedAttributeValueList = info.GetBoolean("_alreadyFetchedAttributeValueList");

			_businessRuleResultUserGroup = (VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection)info.GetValue("_businessRuleResultUserGroup", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection));
			_alwaysFetchBusinessRuleResultUserGroup = info.GetBoolean("_alwaysFetchBusinessRuleResultUserGroup");
			_alreadyFetchedBusinessRuleResultUserGroup = info.GetBoolean("_alreadyFetchedBusinessRuleResultUserGroup");

			_choices = (VarioSL.Entities.CollectionClasses.ChoiceCollection)info.GetValue("_choices", typeof(VarioSL.Entities.CollectionClasses.ChoiceCollection));
			_alwaysFetchChoices = info.GetBoolean("_alwaysFetchChoices");
			_alreadyFetchedChoices = info.GetBoolean("_alreadyFetchedChoices");

			_directionAttributeValueFareMatrixEntries = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_directionAttributeValueFareMatrixEntries", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchDirectionAttributeValueFareMatrixEntries = info.GetBoolean("_alwaysFetchDirectionAttributeValueFareMatrixEntries");
			_alreadyFetchedDirectionAttributeValueFareMatrixEntries = info.GetBoolean("_alreadyFetchedDirectionAttributeValueFareMatrixEntries");

			_distanceAttributeValueFareMatrixEntries = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_distanceAttributeValueFareMatrixEntries", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchDistanceAttributeValueFareMatrixEntries = info.GetBoolean("_alwaysFetchDistanceAttributeValueFareMatrixEntries");
			_alreadyFetchedDistanceAttributeValueFareMatrixEntries = info.GetBoolean("_alreadyFetchedDistanceAttributeValueFareMatrixEntries");

			_lineFilterAttributeValueFareMatrixEntries = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_lineFilterAttributeValueFareMatrixEntries", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchLineFilterAttributeValueFareMatrixEntries = info.GetBoolean("_alwaysFetchLineFilterAttributeValueFareMatrixEntries");
			_alreadyFetchedLineFilterAttributeValueFareMatrixEntries = info.GetBoolean("_alreadyFetchedLineFilterAttributeValueFareMatrixEntries");

			_tariffAttributeValueFareMatrixEntries = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_tariffAttributeValueFareMatrixEntries", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchTariffAttributeValueFareMatrixEntries = info.GetBoolean("_alwaysFetchTariffAttributeValueFareMatrixEntries");
			_alreadyFetchedTariffAttributeValueFareMatrixEntries = info.GetBoolean("_alreadyFetchedTariffAttributeValueFareMatrixEntries");

			_useDaysAttributeFareMatrixEntries = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_useDaysAttributeFareMatrixEntries", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchUseDaysAttributeFareMatrixEntries = info.GetBoolean("_alwaysFetchUseDaysAttributeFareMatrixEntries");
			_alreadyFetchedUseDaysAttributeFareMatrixEntries = info.GetBoolean("_alreadyFetchedUseDaysAttributeFareMatrixEntries");

			_fareStageLists = (VarioSL.Entities.CollectionClasses.FareStageListCollection)info.GetValue("_fareStageLists", typeof(VarioSL.Entities.CollectionClasses.FareStageListCollection));
			_alwaysFetchFareStageLists = info.GetBoolean("_alwaysFetchFareStageLists");
			_alreadyFetchedFareStageLists = info.GetBoolean("_alreadyFetchedFareStageLists");

			_fareTableEntries = (VarioSL.Entities.CollectionClasses.FareTableEntryCollection)info.GetValue("_fareTableEntries", typeof(VarioSL.Entities.CollectionClasses.FareTableEntryCollection));
			_alwaysFetchFareTableEntries = info.GetBoolean("_alwaysFetchFareTableEntries");
			_alreadyFetchedFareTableEntries = info.GetBoolean("_alreadyFetchedFareTableEntries");

			_key1AttributeTransforms = (VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection)info.GetValue("_key1AttributeTransforms", typeof(VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection));
			_alwaysFetchKey1AttributeTransforms = info.GetBoolean("_alwaysFetchKey1AttributeTransforms");
			_alreadyFetchedKey1AttributeTransforms = info.GetBoolean("_alreadyFetchedKey1AttributeTransforms");

			_key2AttributeTransforms = (VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection)info.GetValue("_key2AttributeTransforms", typeof(VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection));
			_alwaysFetchKey2AttributeTransforms = info.GetBoolean("_alwaysFetchKey2AttributeTransforms");
			_alreadyFetchedKey2AttributeTransforms = info.GetBoolean("_alreadyFetchedKey2AttributeTransforms");

			_parameterAttributeValues = (VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection)info.GetValue("_parameterAttributeValues", typeof(VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection));
			_alwaysFetchParameterAttributeValues = info.GetBoolean("_alwaysFetchParameterAttributeValues");
			_alreadyFetchedParameterAttributeValues = info.GetBoolean("_alreadyFetchedParameterAttributeValues");

			_tariffAttributeRoutes = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_tariffAttributeRoutes", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchTariffAttributeRoutes = info.GetBoolean("_alwaysFetchTariffAttributeRoutes");
			_alreadyFetchedTariffAttributeRoutes = info.GetBoolean("_alreadyFetchedTariffAttributeRoutes");

			_ticketGroupAttributeRoutes = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_ticketGroupAttributeRoutes", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchTicketGroupAttributeRoutes = info.GetBoolean("_alwaysFetchTicketGroupAttributeRoutes");
			_alreadyFetchedTicketGroupAttributeRoutes = info.GetBoolean("_alreadyFetchedTicketGroupAttributeRoutes");

			_vdvProduct = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProduct", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProduct = info.GetBoolean("_alwaysFetchVdvProduct");
			_alreadyFetchedVdvProduct = info.GetBoolean("_alreadyFetchedVdvProduct");
			_ticketCollectionViaAttributeValueList = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaAttributeValueList", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaAttributeValueList = info.GetBoolean("_alwaysFetchTicketCollectionViaAttributeValueList");
			_alreadyFetchedTicketCollectionViaAttributeValueList = info.GetBoolean("_alreadyFetchedTicketCollectionViaAttributeValueList");
			_attribute = (AttributeEntity)info.GetValue("_attribute", typeof(AttributeEntity));
			if(_attribute!=null)
			{
				_attribute.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeReturnsNewIfNotFound = info.GetBoolean("_attributeReturnsNewIfNotFound");
			_alwaysFetchAttribute = info.GetBoolean("_alwaysFetchAttribute");
			_alreadyFetchedAttribute = info.GetBoolean("_alreadyFetchedAttribute");

			_logo1 = (LogoEntity)info.GetValue("_logo1", typeof(LogoEntity));
			if(_logo1!=null)
			{
				_logo1.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_logo1ReturnsNewIfNotFound = info.GetBoolean("_logo1ReturnsNewIfNotFound");
			_alwaysFetchLogo1 = info.GetBoolean("_alwaysFetchLogo1");
			_alreadyFetchedLogo1 = info.GetBoolean("_alreadyFetchedLogo1");

			_logo2 = (LogoEntity)info.GetValue("_logo2", typeof(LogoEntity));
			if(_logo2!=null)
			{
				_logo2.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_logo2ReturnsNewIfNotFound = info.GetBoolean("_logo2ReturnsNewIfNotFound");
			_alwaysFetchLogo2 = info.GetBoolean("_alwaysFetchLogo2");
			_alreadyFetchedLogo2 = info.GetBoolean("_alreadyFetchedLogo2");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AttributeValueFieldIndex)fieldIndex)
			{
				case AttributeValueFieldIndex.AttributeID:
					DesetupSyncAttribute(true, false);
					_alreadyFetchedAttribute = false;
					break;
				case AttributeValueFieldIndex.Logo1ID:
					DesetupSyncLogo1(true, false);
					_alreadyFetchedLogo1 = false;
					break;
				case AttributeValueFieldIndex.Logo2ID:
					DesetupSyncLogo2(true, false);
					_alreadyFetchedLogo2 = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaList = (_areaList.Count > 0);
			_alreadyFetchedAttributeValueList = (_attributeValueList.Count > 0);
			_alreadyFetchedBusinessRuleResultUserGroup = (_businessRuleResultUserGroup.Count > 0);
			_alreadyFetchedChoices = (_choices.Count > 0);
			_alreadyFetchedDirectionAttributeValueFareMatrixEntries = (_directionAttributeValueFareMatrixEntries.Count > 0);
			_alreadyFetchedDistanceAttributeValueFareMatrixEntries = (_distanceAttributeValueFareMatrixEntries.Count > 0);
			_alreadyFetchedLineFilterAttributeValueFareMatrixEntries = (_lineFilterAttributeValueFareMatrixEntries.Count > 0);
			_alreadyFetchedTariffAttributeValueFareMatrixEntries = (_tariffAttributeValueFareMatrixEntries.Count > 0);
			_alreadyFetchedUseDaysAttributeFareMatrixEntries = (_useDaysAttributeFareMatrixEntries.Count > 0);
			_alreadyFetchedFareStageLists = (_fareStageLists.Count > 0);
			_alreadyFetchedFareTableEntries = (_fareTableEntries.Count > 0);
			_alreadyFetchedKey1AttributeTransforms = (_key1AttributeTransforms.Count > 0);
			_alreadyFetchedKey2AttributeTransforms = (_key2AttributeTransforms.Count > 0);
			_alreadyFetchedParameterAttributeValues = (_parameterAttributeValues.Count > 0);
			_alreadyFetchedTariffAttributeRoutes = (_tariffAttributeRoutes.Count > 0);
			_alreadyFetchedTicketGroupAttributeRoutes = (_ticketGroupAttributeRoutes.Count > 0);
			_alreadyFetchedVdvProduct = (_vdvProduct.Count > 0);
			_alreadyFetchedTicketCollectionViaAttributeValueList = (_ticketCollectionViaAttributeValueList.Count > 0);
			_alreadyFetchedAttribute = (_attribute != null);
			_alreadyFetchedLogo1 = (_logo1 != null);
			_alreadyFetchedLogo2 = (_logo2 != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Attribute":
					toReturn.Add(Relations.AttributeEntityUsingAttributeID);
					break;
				case "Logo1":
					toReturn.Add(Relations.LogoEntityUsingLogo1ID);
					break;
				case "Logo2":
					toReturn.Add(Relations.LogoEntityUsingLogo2ID);
					break;
				case "AreaList":
					toReturn.Add(Relations.AreaListEntityUsingTicketGroupAttributeID);
					break;
				case "AttributeValueList":
					toReturn.Add(Relations.AttributeValueListEntityUsingAttributeValueID);
					break;
				case "BusinessRuleResultUserGroup":
					toReturn.Add(Relations.BusinessRuleResultEntityUsingUserGroupAttributeValue);
					break;
				case "Choices":
					toReturn.Add(Relations.ChoiceEntityUsingDistanceAttributeID);
					break;
				case "DirectionAttributeValueFareMatrixEntries":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingDirectionAttributeID);
					break;
				case "DistanceAttributeValueFareMatrixEntries":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingDistanceAttributeID);
					break;
				case "LineFilterAttributeValueFareMatrixEntries":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingLineFilterAttributeID);
					break;
				case "TariffAttributeValueFareMatrixEntries":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingTariffAttributeID);
					break;
				case "UseDaysAttributeFareMatrixEntries":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingUseDaysAttributeID);
					break;
				case "FareStageLists":
					toReturn.Add(Relations.FareStageListEntityUsingAttributeValueID);
					break;
				case "FareTableEntries":
					toReturn.Add(Relations.FareTableEntryEntityUsingAttributeValueID);
					break;
				case "Key1AttributeTransforms":
					toReturn.Add(Relations.KeyAttributeTransfromEntityUsingAttributeValueFromID);
					break;
				case "Key2AttributeTransforms":
					toReturn.Add(Relations.KeyAttributeTransfromEntityUsingAttributeValueToID);
					break;
				case "ParameterAttributeValues":
					toReturn.Add(Relations.ParameterAttributeValueEntityUsingAttributeValueID);
					break;
				case "TariffAttributeRoutes":
					toReturn.Add(Relations.RouteEntityUsingTariffAttributeID);
					break;
				case "TicketGroupAttributeRoutes":
					toReturn.Add(Relations.RouteEntityUsingTicketGroupAttributeID);
					break;
				case "VdvProduct":
					toReturn.Add(Relations.VdvProductEntityUsingDistanceAttributeValueID);
					break;
				case "TicketCollectionViaAttributeValueList":
					toReturn.Add(Relations.AttributeValueListEntityUsingAttributeValueID, "AttributeValueEntity__", "AttributeValueList_", JoinHint.None);
					toReturn.Add(AttributeValueListEntity.Relations.TicketEntityUsingTicketID, "AttributeValueList_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaList", (!this.MarkedForDeletion?_areaList:null));
			info.AddValue("_alwaysFetchAreaList", _alwaysFetchAreaList);
			info.AddValue("_alreadyFetchedAreaList", _alreadyFetchedAreaList);
			info.AddValue("_attributeValueList", (!this.MarkedForDeletion?_attributeValueList:null));
			info.AddValue("_alwaysFetchAttributeValueList", _alwaysFetchAttributeValueList);
			info.AddValue("_alreadyFetchedAttributeValueList", _alreadyFetchedAttributeValueList);
			info.AddValue("_businessRuleResultUserGroup", (!this.MarkedForDeletion?_businessRuleResultUserGroup:null));
			info.AddValue("_alwaysFetchBusinessRuleResultUserGroup", _alwaysFetchBusinessRuleResultUserGroup);
			info.AddValue("_alreadyFetchedBusinessRuleResultUserGroup", _alreadyFetchedBusinessRuleResultUserGroup);
			info.AddValue("_choices", (!this.MarkedForDeletion?_choices:null));
			info.AddValue("_alwaysFetchChoices", _alwaysFetchChoices);
			info.AddValue("_alreadyFetchedChoices", _alreadyFetchedChoices);
			info.AddValue("_directionAttributeValueFareMatrixEntries", (!this.MarkedForDeletion?_directionAttributeValueFareMatrixEntries:null));
			info.AddValue("_alwaysFetchDirectionAttributeValueFareMatrixEntries", _alwaysFetchDirectionAttributeValueFareMatrixEntries);
			info.AddValue("_alreadyFetchedDirectionAttributeValueFareMatrixEntries", _alreadyFetchedDirectionAttributeValueFareMatrixEntries);
			info.AddValue("_distanceAttributeValueFareMatrixEntries", (!this.MarkedForDeletion?_distanceAttributeValueFareMatrixEntries:null));
			info.AddValue("_alwaysFetchDistanceAttributeValueFareMatrixEntries", _alwaysFetchDistanceAttributeValueFareMatrixEntries);
			info.AddValue("_alreadyFetchedDistanceAttributeValueFareMatrixEntries", _alreadyFetchedDistanceAttributeValueFareMatrixEntries);
			info.AddValue("_lineFilterAttributeValueFareMatrixEntries", (!this.MarkedForDeletion?_lineFilterAttributeValueFareMatrixEntries:null));
			info.AddValue("_alwaysFetchLineFilterAttributeValueFareMatrixEntries", _alwaysFetchLineFilterAttributeValueFareMatrixEntries);
			info.AddValue("_alreadyFetchedLineFilterAttributeValueFareMatrixEntries", _alreadyFetchedLineFilterAttributeValueFareMatrixEntries);
			info.AddValue("_tariffAttributeValueFareMatrixEntries", (!this.MarkedForDeletion?_tariffAttributeValueFareMatrixEntries:null));
			info.AddValue("_alwaysFetchTariffAttributeValueFareMatrixEntries", _alwaysFetchTariffAttributeValueFareMatrixEntries);
			info.AddValue("_alreadyFetchedTariffAttributeValueFareMatrixEntries", _alreadyFetchedTariffAttributeValueFareMatrixEntries);
			info.AddValue("_useDaysAttributeFareMatrixEntries", (!this.MarkedForDeletion?_useDaysAttributeFareMatrixEntries:null));
			info.AddValue("_alwaysFetchUseDaysAttributeFareMatrixEntries", _alwaysFetchUseDaysAttributeFareMatrixEntries);
			info.AddValue("_alreadyFetchedUseDaysAttributeFareMatrixEntries", _alreadyFetchedUseDaysAttributeFareMatrixEntries);
			info.AddValue("_fareStageLists", (!this.MarkedForDeletion?_fareStageLists:null));
			info.AddValue("_alwaysFetchFareStageLists", _alwaysFetchFareStageLists);
			info.AddValue("_alreadyFetchedFareStageLists", _alreadyFetchedFareStageLists);
			info.AddValue("_fareTableEntries", (!this.MarkedForDeletion?_fareTableEntries:null));
			info.AddValue("_alwaysFetchFareTableEntries", _alwaysFetchFareTableEntries);
			info.AddValue("_alreadyFetchedFareTableEntries", _alreadyFetchedFareTableEntries);
			info.AddValue("_key1AttributeTransforms", (!this.MarkedForDeletion?_key1AttributeTransforms:null));
			info.AddValue("_alwaysFetchKey1AttributeTransforms", _alwaysFetchKey1AttributeTransforms);
			info.AddValue("_alreadyFetchedKey1AttributeTransforms", _alreadyFetchedKey1AttributeTransforms);
			info.AddValue("_key2AttributeTransforms", (!this.MarkedForDeletion?_key2AttributeTransforms:null));
			info.AddValue("_alwaysFetchKey2AttributeTransforms", _alwaysFetchKey2AttributeTransforms);
			info.AddValue("_alreadyFetchedKey2AttributeTransforms", _alreadyFetchedKey2AttributeTransforms);
			info.AddValue("_parameterAttributeValues", (!this.MarkedForDeletion?_parameterAttributeValues:null));
			info.AddValue("_alwaysFetchParameterAttributeValues", _alwaysFetchParameterAttributeValues);
			info.AddValue("_alreadyFetchedParameterAttributeValues", _alreadyFetchedParameterAttributeValues);
			info.AddValue("_tariffAttributeRoutes", (!this.MarkedForDeletion?_tariffAttributeRoutes:null));
			info.AddValue("_alwaysFetchTariffAttributeRoutes", _alwaysFetchTariffAttributeRoutes);
			info.AddValue("_alreadyFetchedTariffAttributeRoutes", _alreadyFetchedTariffAttributeRoutes);
			info.AddValue("_ticketGroupAttributeRoutes", (!this.MarkedForDeletion?_ticketGroupAttributeRoutes:null));
			info.AddValue("_alwaysFetchTicketGroupAttributeRoutes", _alwaysFetchTicketGroupAttributeRoutes);
			info.AddValue("_alreadyFetchedTicketGroupAttributeRoutes", _alreadyFetchedTicketGroupAttributeRoutes);
			info.AddValue("_vdvProduct", (!this.MarkedForDeletion?_vdvProduct:null));
			info.AddValue("_alwaysFetchVdvProduct", _alwaysFetchVdvProduct);
			info.AddValue("_alreadyFetchedVdvProduct", _alreadyFetchedVdvProduct);
			info.AddValue("_ticketCollectionViaAttributeValueList", (!this.MarkedForDeletion?_ticketCollectionViaAttributeValueList:null));
			info.AddValue("_alwaysFetchTicketCollectionViaAttributeValueList", _alwaysFetchTicketCollectionViaAttributeValueList);
			info.AddValue("_alreadyFetchedTicketCollectionViaAttributeValueList", _alreadyFetchedTicketCollectionViaAttributeValueList);
			info.AddValue("_attribute", (!this.MarkedForDeletion?_attribute:null));
			info.AddValue("_attributeReturnsNewIfNotFound", _attributeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttribute", _alwaysFetchAttribute);
			info.AddValue("_alreadyFetchedAttribute", _alreadyFetchedAttribute);
			info.AddValue("_logo1", (!this.MarkedForDeletion?_logo1:null));
			info.AddValue("_logo1ReturnsNewIfNotFound", _logo1ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLogo1", _alwaysFetchLogo1);
			info.AddValue("_alreadyFetchedLogo1", _alreadyFetchedLogo1);
			info.AddValue("_logo2", (!this.MarkedForDeletion?_logo2:null));
			info.AddValue("_logo2ReturnsNewIfNotFound", _logo2ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchLogo2", _alwaysFetchLogo2);
			info.AddValue("_alreadyFetchedLogo2", _alreadyFetchedLogo2);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Attribute":
					_alreadyFetchedAttribute = true;
					this.Attribute = (AttributeEntity)entity;
					break;
				case "Logo1":
					_alreadyFetchedLogo1 = true;
					this.Logo1 = (LogoEntity)entity;
					break;
				case "Logo2":
					_alreadyFetchedLogo2 = true;
					this.Logo2 = (LogoEntity)entity;
					break;
				case "AreaList":
					_alreadyFetchedAreaList = true;
					if(entity!=null)
					{
						this.AreaList.Add((AreaListEntity)entity);
					}
					break;
				case "AttributeValueList":
					_alreadyFetchedAttributeValueList = true;
					if(entity!=null)
					{
						this.AttributeValueList.Add((AttributeValueListEntity)entity);
					}
					break;
				case "BusinessRuleResultUserGroup":
					_alreadyFetchedBusinessRuleResultUserGroup = true;
					if(entity!=null)
					{
						this.BusinessRuleResultUserGroup.Add((BusinessRuleResultEntity)entity);
					}
					break;
				case "Choices":
					_alreadyFetchedChoices = true;
					if(entity!=null)
					{
						this.Choices.Add((ChoiceEntity)entity);
					}
					break;
				case "DirectionAttributeValueFareMatrixEntries":
					_alreadyFetchedDirectionAttributeValueFareMatrixEntries = true;
					if(entity!=null)
					{
						this.DirectionAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "DistanceAttributeValueFareMatrixEntries":
					_alreadyFetchedDistanceAttributeValueFareMatrixEntries = true;
					if(entity!=null)
					{
						this.DistanceAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "LineFilterAttributeValueFareMatrixEntries":
					_alreadyFetchedLineFilterAttributeValueFareMatrixEntries = true;
					if(entity!=null)
					{
						this.LineFilterAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "TariffAttributeValueFareMatrixEntries":
					_alreadyFetchedTariffAttributeValueFareMatrixEntries = true;
					if(entity!=null)
					{
						this.TariffAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "UseDaysAttributeFareMatrixEntries":
					_alreadyFetchedUseDaysAttributeFareMatrixEntries = true;
					if(entity!=null)
					{
						this.UseDaysAttributeFareMatrixEntries.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "FareStageLists":
					_alreadyFetchedFareStageLists = true;
					if(entity!=null)
					{
						this.FareStageLists.Add((FareStageListEntity)entity);
					}
					break;
				case "FareTableEntries":
					_alreadyFetchedFareTableEntries = true;
					if(entity!=null)
					{
						this.FareTableEntries.Add((FareTableEntryEntity)entity);
					}
					break;
				case "Key1AttributeTransforms":
					_alreadyFetchedKey1AttributeTransforms = true;
					if(entity!=null)
					{
						this.Key1AttributeTransforms.Add((KeyAttributeTransfromEntity)entity);
					}
					break;
				case "Key2AttributeTransforms":
					_alreadyFetchedKey2AttributeTransforms = true;
					if(entity!=null)
					{
						this.Key2AttributeTransforms.Add((KeyAttributeTransfromEntity)entity);
					}
					break;
				case "ParameterAttributeValues":
					_alreadyFetchedParameterAttributeValues = true;
					if(entity!=null)
					{
						this.ParameterAttributeValues.Add((ParameterAttributeValueEntity)entity);
					}
					break;
				case "TariffAttributeRoutes":
					_alreadyFetchedTariffAttributeRoutes = true;
					if(entity!=null)
					{
						this.TariffAttributeRoutes.Add((RouteEntity)entity);
					}
					break;
				case "TicketGroupAttributeRoutes":
					_alreadyFetchedTicketGroupAttributeRoutes = true;
					if(entity!=null)
					{
						this.TicketGroupAttributeRoutes.Add((RouteEntity)entity);
					}
					break;
				case "VdvProduct":
					_alreadyFetchedVdvProduct = true;
					if(entity!=null)
					{
						this.VdvProduct.Add((VdvProductEntity)entity);
					}
					break;
				case "TicketCollectionViaAttributeValueList":
					_alreadyFetchedTicketCollectionViaAttributeValueList = true;
					if(entity!=null)
					{
						this.TicketCollectionViaAttributeValueList.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Attribute":
					SetupSyncAttribute(relatedEntity);
					break;
				case "Logo1":
					SetupSyncLogo1(relatedEntity);
					break;
				case "Logo2":
					SetupSyncLogo2(relatedEntity);
					break;
				case "AreaList":
					_areaList.Add((AreaListEntity)relatedEntity);
					break;
				case "AttributeValueList":
					_attributeValueList.Add((AttributeValueListEntity)relatedEntity);
					break;
				case "BusinessRuleResultUserGroup":
					_businessRuleResultUserGroup.Add((BusinessRuleResultEntity)relatedEntity);
					break;
				case "Choices":
					_choices.Add((ChoiceEntity)relatedEntity);
					break;
				case "DirectionAttributeValueFareMatrixEntries":
					_directionAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "DistanceAttributeValueFareMatrixEntries":
					_distanceAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "LineFilterAttributeValueFareMatrixEntries":
					_lineFilterAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "TariffAttributeValueFareMatrixEntries":
					_tariffAttributeValueFareMatrixEntries.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "UseDaysAttributeFareMatrixEntries":
					_useDaysAttributeFareMatrixEntries.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "FareStageLists":
					_fareStageLists.Add((FareStageListEntity)relatedEntity);
					break;
				case "FareTableEntries":
					_fareTableEntries.Add((FareTableEntryEntity)relatedEntity);
					break;
				case "Key1AttributeTransforms":
					_key1AttributeTransforms.Add((KeyAttributeTransfromEntity)relatedEntity);
					break;
				case "Key2AttributeTransforms":
					_key2AttributeTransforms.Add((KeyAttributeTransfromEntity)relatedEntity);
					break;
				case "ParameterAttributeValues":
					_parameterAttributeValues.Add((ParameterAttributeValueEntity)relatedEntity);
					break;
				case "TariffAttributeRoutes":
					_tariffAttributeRoutes.Add((RouteEntity)relatedEntity);
					break;
				case "TicketGroupAttributeRoutes":
					_ticketGroupAttributeRoutes.Add((RouteEntity)relatedEntity);
					break;
				case "VdvProduct":
					_vdvProduct.Add((VdvProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Attribute":
					DesetupSyncAttribute(false, true);
					break;
				case "Logo1":
					DesetupSyncLogo1(false, true);
					break;
				case "Logo2":
					DesetupSyncLogo2(false, true);
					break;
				case "AreaList":
					this.PerformRelatedEntityRemoval(_areaList, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeValueList":
					this.PerformRelatedEntityRemoval(_attributeValueList, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleResultUserGroup":
					this.PerformRelatedEntityRemoval(_businessRuleResultUserGroup, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Choices":
					this.PerformRelatedEntityRemoval(_choices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DirectionAttributeValueFareMatrixEntries":
					this.PerformRelatedEntityRemoval(_directionAttributeValueFareMatrixEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DistanceAttributeValueFareMatrixEntries":
					this.PerformRelatedEntityRemoval(_distanceAttributeValueFareMatrixEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "LineFilterAttributeValueFareMatrixEntries":
					this.PerformRelatedEntityRemoval(_lineFilterAttributeValueFareMatrixEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TariffAttributeValueFareMatrixEntries":
					this.PerformRelatedEntityRemoval(_tariffAttributeValueFareMatrixEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UseDaysAttributeFareMatrixEntries":
					this.PerformRelatedEntityRemoval(_useDaysAttributeFareMatrixEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareStageLists":
					this.PerformRelatedEntityRemoval(_fareStageLists, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareTableEntries":
					this.PerformRelatedEntityRemoval(_fareTableEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Key1AttributeTransforms":
					this.PerformRelatedEntityRemoval(_key1AttributeTransforms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Key2AttributeTransforms":
					this.PerformRelatedEntityRemoval(_key2AttributeTransforms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterAttributeValues":
					this.PerformRelatedEntityRemoval(_parameterAttributeValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TariffAttributeRoutes":
					this.PerformRelatedEntityRemoval(_tariffAttributeRoutes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketGroupAttributeRoutes":
					this.PerformRelatedEntityRemoval(_ticketGroupAttributeRoutes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProduct":
					this.PerformRelatedEntityRemoval(_vdvProduct, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attribute!=null)
			{
				toReturn.Add(_attribute);
			}
			if(_logo1!=null)
			{
				toReturn.Add(_logo1);
			}
			if(_logo2!=null)
			{
				toReturn.Add(_logo2);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_areaList);
			toReturn.Add(_attributeValueList);
			toReturn.Add(_businessRuleResultUserGroup);
			toReturn.Add(_choices);
			toReturn.Add(_directionAttributeValueFareMatrixEntries);
			toReturn.Add(_distanceAttributeValueFareMatrixEntries);
			toReturn.Add(_lineFilterAttributeValueFareMatrixEntries);
			toReturn.Add(_tariffAttributeValueFareMatrixEntries);
			toReturn.Add(_useDaysAttributeFareMatrixEntries);
			toReturn.Add(_fareStageLists);
			toReturn.Add(_fareTableEntries);
			toReturn.Add(_key1AttributeTransforms);
			toReturn.Add(_key2AttributeTransforms);
			toReturn.Add(_parameterAttributeValues);
			toReturn.Add(_tariffAttributeRoutes);
			toReturn.Add(_ticketGroupAttributeRoutes);
			toReturn.Add(_vdvProduct);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="attributeID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAttributeIDNumber(Nullable<System.Int64> attributeID, Nullable<System.Int64> number)
		{
			return FetchUsingUCAttributeIDNumber( attributeID,  number, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="attributeID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAttributeIDNumber(Nullable<System.Int64> attributeID, Nullable<System.Int64> number, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAttributeIDNumber( attributeID,  number, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="attributeID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAttributeIDNumber(Nullable<System.Int64> attributeID, Nullable<System.Int64> number, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAttributeIDNumber( attributeID,  number, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="attributeID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="number">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAttributeIDNumber(Nullable<System.Int64> attributeID, Nullable<System.Int64> number, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((AttributeValueDAO)CreateDAOInstance()).FetchAttributeValueUsingUCAttributeIDNumber(this, this.Transaction, attributeID, number, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeValueID)
		{
			return FetchUsingPK(attributeValueID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeValueID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(attributeValueID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(attributeValueID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 attributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(attributeValueID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AttributeValueID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AttributeValueRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch)
		{
			return GetMultiAreaList(forceFetch, _areaList.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaList(forceFetch, _areaList.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaList(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaList || forceFetch || _alwaysFetchAreaList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaList);
				_areaList.SuppressClearInGetMulti=!forceFetch;
				_areaList.EntityFactoryToUse = entityFactoryToUse;
				_areaList.GetMultiManyToOne(null, null, this, null, filter);
				_areaList.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaList = true;
			}
			return _areaList;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaList'. These settings will be taken into account
		/// when the property AreaList is requested or GetMultiAreaList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaList.SortClauses=sortClauses;
			_areaList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch)
		{
			return GetMultiAttributeValueList(forceFetch, _attributeValueList.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeValueList(forceFetch, _attributeValueList.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeValueList(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueListCollection GetMultiAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeValueList || forceFetch || _alwaysFetchAttributeValueList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeValueList);
				_attributeValueList.SuppressClearInGetMulti=!forceFetch;
				_attributeValueList.EntityFactoryToUse = entityFactoryToUse;
				_attributeValueList.GetMultiManyToOne(this, null, filter);
				_attributeValueList.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeValueList = true;
			}
			return _attributeValueList;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeValueList'. These settings will be taken into account
		/// when the property AttributeValueList is requested or GetMultiAttributeValueList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeValueList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeValueList.SortClauses=sortClauses;
			_attributeValueList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResultUserGroup(bool forceFetch)
		{
			return GetMultiBusinessRuleResultUserGroup(forceFetch, _businessRuleResultUserGroup.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResultUserGroup(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleResultUserGroup(forceFetch, _businessRuleResultUserGroup.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResultUserGroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleResultUserGroup(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResultUserGroup(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleResultUserGroup || forceFetch || _alwaysFetchBusinessRuleResultUserGroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleResultUserGroup);
				_businessRuleResultUserGroup.SuppressClearInGetMulti=!forceFetch;
				_businessRuleResultUserGroup.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleResultUserGroup.GetMultiManyToOne(this, null, null, null, null, filter);
				_businessRuleResultUserGroup.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleResultUserGroup = true;
			}
			return _businessRuleResultUserGroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleResultUserGroup'. These settings will be taken into account
		/// when the property BusinessRuleResultUserGroup is requested or GetMultiBusinessRuleResultUserGroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleResultUserGroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleResultUserGroup.SortClauses=sortClauses;
			_businessRuleResultUserGroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChoices || forceFetch || _alwaysFetchChoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_choices);
				_choices.SuppressClearInGetMulti=!forceFetch;
				_choices.EntityFactoryToUse = entityFactoryToUse;
				_choices.GetMultiManyToOne(this, null, null, null, filter);
				_choices.SuppressClearInGetMulti=false;
				_alreadyFetchedChoices = true;
			}
			return _choices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Choices'. These settings will be taken into account
		/// when the property Choices is requested or GetMultiChoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_choices.SortClauses=sortClauses;
			_choices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDirectionAttributeValueFareMatrixEntries(bool forceFetch)
		{
			return GetMultiDirectionAttributeValueFareMatrixEntries(forceFetch, _directionAttributeValueFareMatrixEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDirectionAttributeValueFareMatrixEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDirectionAttributeValueFareMatrixEntries(forceFetch, _directionAttributeValueFareMatrixEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDirectionAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDirectionAttributeValueFareMatrixEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDirectionAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDirectionAttributeValueFareMatrixEntries || forceFetch || _alwaysFetchDirectionAttributeValueFareMatrixEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_directionAttributeValueFareMatrixEntries);
				_directionAttributeValueFareMatrixEntries.SuppressClearInGetMulti=!forceFetch;
				_directionAttributeValueFareMatrixEntries.EntityFactoryToUse = entityFactoryToUse;
				_directionAttributeValueFareMatrixEntries.GetMultiManyToOne(this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_directionAttributeValueFareMatrixEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedDirectionAttributeValueFareMatrixEntries = true;
			}
			return _directionAttributeValueFareMatrixEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'DirectionAttributeValueFareMatrixEntries'. These settings will be taken into account
		/// when the property DirectionAttributeValueFareMatrixEntries is requested or GetMultiDirectionAttributeValueFareMatrixEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDirectionAttributeValueFareMatrixEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_directionAttributeValueFareMatrixEntries.SortClauses=sortClauses;
			_directionAttributeValueFareMatrixEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDistanceAttributeValueFareMatrixEntries(bool forceFetch)
		{
			return GetMultiDistanceAttributeValueFareMatrixEntries(forceFetch, _distanceAttributeValueFareMatrixEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDistanceAttributeValueFareMatrixEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDistanceAttributeValueFareMatrixEntries(forceFetch, _distanceAttributeValueFareMatrixEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDistanceAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDistanceAttributeValueFareMatrixEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiDistanceAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDistanceAttributeValueFareMatrixEntries || forceFetch || _alwaysFetchDistanceAttributeValueFareMatrixEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_distanceAttributeValueFareMatrixEntries);
				_distanceAttributeValueFareMatrixEntries.SuppressClearInGetMulti=!forceFetch;
				_distanceAttributeValueFareMatrixEntries.EntityFactoryToUse = entityFactoryToUse;
				_distanceAttributeValueFareMatrixEntries.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, filter);
				_distanceAttributeValueFareMatrixEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedDistanceAttributeValueFareMatrixEntries = true;
			}
			return _distanceAttributeValueFareMatrixEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'DistanceAttributeValueFareMatrixEntries'. These settings will be taken into account
		/// when the property DistanceAttributeValueFareMatrixEntries is requested or GetMultiDistanceAttributeValueFareMatrixEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDistanceAttributeValueFareMatrixEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_distanceAttributeValueFareMatrixEntries.SortClauses=sortClauses;
			_distanceAttributeValueFareMatrixEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiLineFilterAttributeValueFareMatrixEntries(bool forceFetch)
		{
			return GetMultiLineFilterAttributeValueFareMatrixEntries(forceFetch, _lineFilterAttributeValueFareMatrixEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiLineFilterAttributeValueFareMatrixEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLineFilterAttributeValueFareMatrixEntries(forceFetch, _lineFilterAttributeValueFareMatrixEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiLineFilterAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLineFilterAttributeValueFareMatrixEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiLineFilterAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLineFilterAttributeValueFareMatrixEntries || forceFetch || _alwaysFetchLineFilterAttributeValueFareMatrixEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_lineFilterAttributeValueFareMatrixEntries);
				_lineFilterAttributeValueFareMatrixEntries.SuppressClearInGetMulti=!forceFetch;
				_lineFilterAttributeValueFareMatrixEntries.EntityFactoryToUse = entityFactoryToUse;
				_lineFilterAttributeValueFareMatrixEntries.GetMultiManyToOne(null, null, this, null, null, null, null, null, null, null, null, null, filter);
				_lineFilterAttributeValueFareMatrixEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedLineFilterAttributeValueFareMatrixEntries = true;
			}
			return _lineFilterAttributeValueFareMatrixEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'LineFilterAttributeValueFareMatrixEntries'. These settings will be taken into account
		/// when the property LineFilterAttributeValueFareMatrixEntries is requested or GetMultiLineFilterAttributeValueFareMatrixEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLineFilterAttributeValueFareMatrixEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_lineFilterAttributeValueFareMatrixEntries.SortClauses=sortClauses;
			_lineFilterAttributeValueFareMatrixEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiTariffAttributeValueFareMatrixEntries(bool forceFetch)
		{
			return GetMultiTariffAttributeValueFareMatrixEntries(forceFetch, _tariffAttributeValueFareMatrixEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiTariffAttributeValueFareMatrixEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffAttributeValueFareMatrixEntries(forceFetch, _tariffAttributeValueFareMatrixEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiTariffAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffAttributeValueFareMatrixEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiTariffAttributeValueFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffAttributeValueFareMatrixEntries || forceFetch || _alwaysFetchTariffAttributeValueFareMatrixEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffAttributeValueFareMatrixEntries);
				_tariffAttributeValueFareMatrixEntries.SuppressClearInGetMulti=!forceFetch;
				_tariffAttributeValueFareMatrixEntries.EntityFactoryToUse = entityFactoryToUse;
				_tariffAttributeValueFareMatrixEntries.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_tariffAttributeValueFareMatrixEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffAttributeValueFareMatrixEntries = true;
			}
			return _tariffAttributeValueFareMatrixEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'TariffAttributeValueFareMatrixEntries'. These settings will be taken into account
		/// when the property TariffAttributeValueFareMatrixEntries is requested or GetMultiTariffAttributeValueFareMatrixEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffAttributeValueFareMatrixEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffAttributeValueFareMatrixEntries.SortClauses=sortClauses;
			_tariffAttributeValueFareMatrixEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiUseDaysAttributeFareMatrixEntries(bool forceFetch)
		{
			return GetMultiUseDaysAttributeFareMatrixEntries(forceFetch, _useDaysAttributeFareMatrixEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiUseDaysAttributeFareMatrixEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUseDaysAttributeFareMatrixEntries(forceFetch, _useDaysAttributeFareMatrixEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiUseDaysAttributeFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUseDaysAttributeFareMatrixEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiUseDaysAttributeFareMatrixEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUseDaysAttributeFareMatrixEntries || forceFetch || _alwaysFetchUseDaysAttributeFareMatrixEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_useDaysAttributeFareMatrixEntries);
				_useDaysAttributeFareMatrixEntries.SuppressClearInGetMulti=!forceFetch;
				_useDaysAttributeFareMatrixEntries.EntityFactoryToUse = entityFactoryToUse;
				_useDaysAttributeFareMatrixEntries.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_useDaysAttributeFareMatrixEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedUseDaysAttributeFareMatrixEntries = true;
			}
			return _useDaysAttributeFareMatrixEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'UseDaysAttributeFareMatrixEntries'. These settings will be taken into account
		/// when the property UseDaysAttributeFareMatrixEntries is requested or GetMultiUseDaysAttributeFareMatrixEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUseDaysAttributeFareMatrixEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_useDaysAttributeFareMatrixEntries.SortClauses=sortClauses;
			_useDaysAttributeFareMatrixEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareStageListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch)
		{
			return GetMultiFareStageLists(forceFetch, _fareStageLists.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareStageListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareStageLists(forceFetch, _fareStageLists.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareStageLists(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareStageLists || forceFetch || _alwaysFetchFareStageLists) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareStageLists);
				_fareStageLists.SuppressClearInGetMulti=!forceFetch;
				_fareStageLists.EntityFactoryToUse = entityFactoryToUse;
				_fareStageLists.GetMultiManyToOne(this, null, filter);
				_fareStageLists.SuppressClearInGetMulti=false;
				_alreadyFetchedFareStageLists = true;
			}
			return _fareStageLists;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareStageLists'. These settings will be taken into account
		/// when the property FareStageLists is requested or GetMultiFareStageLists is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareStageLists(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareStageLists.SortClauses=sortClauses;
			_fareStageLists.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTableEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTableEntries || forceFetch || _alwaysFetchFareTableEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTableEntries);
				_fareTableEntries.SuppressClearInGetMulti=!forceFetch;
				_fareTableEntries.EntityFactoryToUse = entityFactoryToUse;
				_fareTableEntries.GetMultiManyToOne(this, null, null, filter);
				_fareTableEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTableEntries = true;
			}
			return _fareTableEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTableEntries'. These settings will be taken into account
		/// when the property FareTableEntries is requested or GetMultiFareTableEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTableEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTableEntries.SortClauses=sortClauses;
			_fareTableEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey1AttributeTransforms(bool forceFetch)
		{
			return GetMultiKey1AttributeTransforms(forceFetch, _key1AttributeTransforms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey1AttributeTransforms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiKey1AttributeTransforms(forceFetch, _key1AttributeTransforms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey1AttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiKey1AttributeTransforms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey1AttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedKey1AttributeTransforms || forceFetch || _alwaysFetchKey1AttributeTransforms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_key1AttributeTransforms);
				_key1AttributeTransforms.SuppressClearInGetMulti=!forceFetch;
				_key1AttributeTransforms.EntityFactoryToUse = entityFactoryToUse;
				_key1AttributeTransforms.GetMultiManyToOne(null, this, null, null, null, filter);
				_key1AttributeTransforms.SuppressClearInGetMulti=false;
				_alreadyFetchedKey1AttributeTransforms = true;
			}
			return _key1AttributeTransforms;
		}

		/// <summary> Sets the collection parameters for the collection for 'Key1AttributeTransforms'. These settings will be taken into account
		/// when the property Key1AttributeTransforms is requested or GetMultiKey1AttributeTransforms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersKey1AttributeTransforms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_key1AttributeTransforms.SortClauses=sortClauses;
			_key1AttributeTransforms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey2AttributeTransforms(bool forceFetch)
		{
			return GetMultiKey2AttributeTransforms(forceFetch, _key2AttributeTransforms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey2AttributeTransforms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiKey2AttributeTransforms(forceFetch, _key2AttributeTransforms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey2AttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiKey2AttributeTransforms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKey2AttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedKey2AttributeTransforms || forceFetch || _alwaysFetchKey2AttributeTransforms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_key2AttributeTransforms);
				_key2AttributeTransforms.SuppressClearInGetMulti=!forceFetch;
				_key2AttributeTransforms.EntityFactoryToUse = entityFactoryToUse;
				_key2AttributeTransforms.GetMultiManyToOne(null, null, this, null, null, filter);
				_key2AttributeTransforms.SuppressClearInGetMulti=false;
				_alreadyFetchedKey2AttributeTransforms = true;
			}
			return _key2AttributeTransforms;
		}

		/// <summary> Sets the collection parameters for the collection for 'Key2AttributeTransforms'. These settings will be taken into account
		/// when the property Key2AttributeTransforms is requested or GetMultiKey2AttributeTransforms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersKey2AttributeTransforms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_key2AttributeTransforms.SortClauses=sortClauses;
			_key2AttributeTransforms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterAttributeValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterAttributeValues || forceFetch || _alwaysFetchParameterAttributeValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterAttributeValues);
				_parameterAttributeValues.SuppressClearInGetMulti=!forceFetch;
				_parameterAttributeValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterAttributeValues.GetMultiManyToOne(this, null, null, filter);
				_parameterAttributeValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterAttributeValues = true;
			}
			return _parameterAttributeValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterAttributeValues'. These settings will be taken into account
		/// when the property ParameterAttributeValues is requested or GetMultiParameterAttributeValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterAttributeValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterAttributeValues.SortClauses=sortClauses;
			_parameterAttributeValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTariffAttributeRoutes(bool forceFetch)
		{
			return GetMultiTariffAttributeRoutes(forceFetch, _tariffAttributeRoutes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTariffAttributeRoutes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffAttributeRoutes(forceFetch, _tariffAttributeRoutes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTariffAttributeRoutes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffAttributeRoutes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTariffAttributeRoutes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffAttributeRoutes || forceFetch || _alwaysFetchTariffAttributeRoutes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffAttributeRoutes);
				_tariffAttributeRoutes.SuppressClearInGetMulti=!forceFetch;
				_tariffAttributeRoutes.EntityFactoryToUse = entityFactoryToUse;
				_tariffAttributeRoutes.GetMultiManyToOne(this, null, null, null, null, null, null, filter);
				_tariffAttributeRoutes.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffAttributeRoutes = true;
			}
			return _tariffAttributeRoutes;
		}

		/// <summary> Sets the collection parameters for the collection for 'TariffAttributeRoutes'. These settings will be taken into account
		/// when the property TariffAttributeRoutes is requested or GetMultiTariffAttributeRoutes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffAttributeRoutes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffAttributeRoutes.SortClauses=sortClauses;
			_tariffAttributeRoutes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTicketGroupAttributeRoutes(bool forceFetch)
		{
			return GetMultiTicketGroupAttributeRoutes(forceFetch, _ticketGroupAttributeRoutes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTicketGroupAttributeRoutes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketGroupAttributeRoutes(forceFetch, _ticketGroupAttributeRoutes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTicketGroupAttributeRoutes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketGroupAttributeRoutes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiTicketGroupAttributeRoutes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketGroupAttributeRoutes || forceFetch || _alwaysFetchTicketGroupAttributeRoutes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketGroupAttributeRoutes);
				_ticketGroupAttributeRoutes.SuppressClearInGetMulti=!forceFetch;
				_ticketGroupAttributeRoutes.EntityFactoryToUse = entityFactoryToUse;
				_ticketGroupAttributeRoutes.GetMultiManyToOne(null, this, null, null, null, null, null, filter);
				_ticketGroupAttributeRoutes.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketGroupAttributeRoutes = true;
			}
			return _ticketGroupAttributeRoutes;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketGroupAttributeRoutes'. These settings will be taken into account
		/// when the property TicketGroupAttributeRoutes is requested or GetMultiTicketGroupAttributeRoutes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketGroupAttributeRoutes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketGroupAttributeRoutes.SortClauses=sortClauses;
			_ticketGroupAttributeRoutes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProduct(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProduct || forceFetch || _alwaysFetchVdvProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProduct);
				_vdvProduct.SuppressClearInGetMulti=!forceFetch;
				_vdvProduct.EntityFactoryToUse = entityFactoryToUse;
				_vdvProduct.GetMultiManyToOne(this, null, null, null, null, null, null, null, filter);
				_vdvProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProduct = true;
			}
			return _vdvProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProduct'. These settings will be taken into account
		/// when the property VdvProduct is requested or GetMultiVdvProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProduct.SortClauses=sortClauses;
			_vdvProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaAttributeValueList(bool forceFetch)
		{
			return GetMultiTicketCollectionViaAttributeValueList(forceFetch, _ticketCollectionViaAttributeValueList.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaAttributeValueList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaAttributeValueList || forceFetch || _alwaysFetchTicketCollectionViaAttributeValueList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaAttributeValueList);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(AttributeValueFields.AttributeValueID, ComparisonOperator.Equal, this.AttributeValueID, "AttributeValueEntity__"));
				_ticketCollectionViaAttributeValueList.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaAttributeValueList.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaAttributeValueList.GetMulti(filter, GetRelationsForField("TicketCollectionViaAttributeValueList"));
				_ticketCollectionViaAttributeValueList.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaAttributeValueList = true;
			}
			return _ticketCollectionViaAttributeValueList;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaAttributeValueList'. These settings will be taken into account
		/// when the property TicketCollectionViaAttributeValueList is requested or GetMultiTicketCollectionViaAttributeValueList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaAttributeValueList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaAttributeValueList.SortClauses=sortClauses;
			_ticketCollectionViaAttributeValueList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public AttributeEntity GetSingleAttribute()
		{
			return GetSingleAttribute(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public virtual AttributeEntity GetSingleAttribute(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttribute || forceFetch || _alwaysFetchAttribute) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeEntityUsingAttributeID);
				AttributeEntity newEntity = new AttributeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Attribute = newEntity;
				_alreadyFetchedAttribute = fetchResult;
			}
			return _attribute;
		}


		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public LogoEntity GetSingleLogo1()
		{
			return GetSingleLogo1(false);
		}

		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public virtual LogoEntity GetSingleLogo1(bool forceFetch)
		{
			if( ( !_alreadyFetchedLogo1 || forceFetch || _alwaysFetchLogo1) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LogoEntityUsingLogo1ID);
				LogoEntity newEntity = new LogoEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Logo1ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LogoEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_logo1ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Logo1 = newEntity;
				_alreadyFetchedLogo1 = fetchResult;
			}
			return _logo1;
		}


		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public LogoEntity GetSingleLogo2()
		{
			return GetSingleLogo2(false);
		}

		/// <summary> Retrieves the related entity of type 'LogoEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'LogoEntity' which is related to this entity.</returns>
		public virtual LogoEntity GetSingleLogo2(bool forceFetch)
		{
			if( ( !_alreadyFetchedLogo2 || forceFetch || _alwaysFetchLogo2) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.LogoEntityUsingLogo2ID);
				LogoEntity newEntity = new LogoEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Logo2ID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (LogoEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_logo2ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Logo2 = newEntity;
				_alreadyFetchedLogo2 = fetchResult;
			}
			return _logo2;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Attribute", _attribute);
			toReturn.Add("Logo1", _logo1);
			toReturn.Add("Logo2", _logo2);
			toReturn.Add("AreaList", _areaList);
			toReturn.Add("AttributeValueList", _attributeValueList);
			toReturn.Add("BusinessRuleResultUserGroup", _businessRuleResultUserGroup);
			toReturn.Add("Choices", _choices);
			toReturn.Add("DirectionAttributeValueFareMatrixEntries", _directionAttributeValueFareMatrixEntries);
			toReturn.Add("DistanceAttributeValueFareMatrixEntries", _distanceAttributeValueFareMatrixEntries);
			toReturn.Add("LineFilterAttributeValueFareMatrixEntries", _lineFilterAttributeValueFareMatrixEntries);
			toReturn.Add("TariffAttributeValueFareMatrixEntries", _tariffAttributeValueFareMatrixEntries);
			toReturn.Add("UseDaysAttributeFareMatrixEntries", _useDaysAttributeFareMatrixEntries);
			toReturn.Add("FareStageLists", _fareStageLists);
			toReturn.Add("FareTableEntries", _fareTableEntries);
			toReturn.Add("Key1AttributeTransforms", _key1AttributeTransforms);
			toReturn.Add("Key2AttributeTransforms", _key2AttributeTransforms);
			toReturn.Add("ParameterAttributeValues", _parameterAttributeValues);
			toReturn.Add("TariffAttributeRoutes", _tariffAttributeRoutes);
			toReturn.Add("TicketGroupAttributeRoutes", _ticketGroupAttributeRoutes);
			toReturn.Add("VdvProduct", _vdvProduct);
			toReturn.Add("TicketCollectionViaAttributeValueList", _ticketCollectionViaAttributeValueList);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="validator">The validator object for this AttributeValueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 attributeValueID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(attributeValueID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_areaList = new VarioSL.Entities.CollectionClasses.AreaListCollection();
			_areaList.SetContainingEntityInfo(this, "AttributeValue");

			_attributeValueList = new VarioSL.Entities.CollectionClasses.AttributeValueListCollection();
			_attributeValueList.SetContainingEntityInfo(this, "AttributeValue");

			_businessRuleResultUserGroup = new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection();
			_businessRuleResultUserGroup.SetContainingEntityInfo(this, "UserGroup");

			_choices = new VarioSL.Entities.CollectionClasses.ChoiceCollection();
			_choices.SetContainingEntityInfo(this, "AttributeValue");

			_directionAttributeValueFareMatrixEntries = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_directionAttributeValueFareMatrixEntries.SetContainingEntityInfo(this, "AttributeValueDirection");

			_distanceAttributeValueFareMatrixEntries = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_distanceAttributeValueFareMatrixEntries.SetContainingEntityInfo(this, "AttributeValueDistance");

			_lineFilterAttributeValueFareMatrixEntries = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_lineFilterAttributeValueFareMatrixEntries.SetContainingEntityInfo(this, "AttributeValueLineFilter");

			_tariffAttributeValueFareMatrixEntries = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_tariffAttributeValueFareMatrixEntries.SetContainingEntityInfo(this, "AttributeValueTariff");

			_useDaysAttributeFareMatrixEntries = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_useDaysAttributeFareMatrixEntries.SetContainingEntityInfo(this, "AttributeValueUseDays");

			_fareStageLists = new VarioSL.Entities.CollectionClasses.FareStageListCollection();
			_fareStageLists.SetContainingEntityInfo(this, "Attributevalue");

			_fareTableEntries = new VarioSL.Entities.CollectionClasses.FareTableEntryCollection();
			_fareTableEntries.SetContainingEntityInfo(this, "AttributeValue");

			_key1AttributeTransforms = new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection();
			_key1AttributeTransforms.SetContainingEntityInfo(this, "AttributeValueFrom");

			_key2AttributeTransforms = new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection();
			_key2AttributeTransforms.SetContainingEntityInfo(this, "AttributeValueTo");

			_parameterAttributeValues = new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection();
			_parameterAttributeValues.SetContainingEntityInfo(this, "AttributeValue");

			_tariffAttributeRoutes = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_tariffAttributeRoutes.SetContainingEntityInfo(this, "TariffAttributeValue");

			_ticketGroupAttributeRoutes = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_ticketGroupAttributeRoutes.SetContainingEntityInfo(this, "TicketGroupAttributeValue");

			_vdvProduct = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProduct.SetContainingEntityInfo(this, "DistanceAttributeValue");
			_ticketCollectionViaAttributeValueList = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_attributeReturnsNewIfNotFound = false;
			_logo1ReturnsNewIfNotFound = false;
			_logo2ReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnumerationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EtickLayoutText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FistLayoutText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Logo1ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Logo2ID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Restricted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecondLayoutText", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.AttributeEntityUsingAttributeIDStatic, true, signalRelatedEntity, "AttributeValues", resetFKFields, new int[] { (int)AttributeValueFieldIndex.AttributeID } );		
			_attribute = null;
		}
		
		/// <summary> setups the sync logic for member _attribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttribute(IEntityCore relatedEntity)
		{
			if(_attribute!=relatedEntity)
			{		
				DesetupSyncAttribute(true, true);
				_attribute = (AttributeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.AttributeEntityUsingAttributeIDStatic, true, ref _alreadyFetchedAttribute, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _logo1</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLogo1(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _logo1, new PropertyChangedEventHandler( OnLogo1PropertyChanged ), "Logo1", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.LogoEntityUsingLogo1IDStatic, true, signalRelatedEntity, "AttributeValue", resetFKFields, new int[] { (int)AttributeValueFieldIndex.Logo1ID } );		
			_logo1 = null;
		}
		
		/// <summary> setups the sync logic for member _logo1</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLogo1(IEntityCore relatedEntity)
		{
			if(_logo1!=relatedEntity)
			{		
				DesetupSyncLogo1(true, true);
				_logo1 = (LogoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _logo1, new PropertyChangedEventHandler( OnLogo1PropertyChanged ), "Logo1", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.LogoEntityUsingLogo1IDStatic, true, ref _alreadyFetchedLogo1, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLogo1PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _logo2</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLogo2(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _logo2, new PropertyChangedEventHandler( OnLogo2PropertyChanged ), "Logo2", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.LogoEntityUsingLogo2IDStatic, true, signalRelatedEntity, "AttributeValue_", resetFKFields, new int[] { (int)AttributeValueFieldIndex.Logo2ID } );		
			_logo2 = null;
		}
		
		/// <summary> setups the sync logic for member _logo2</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLogo2(IEntityCore relatedEntity)
		{
			if(_logo2!=relatedEntity)
			{		
				DesetupSyncLogo2(true, true);
				_logo2 = (LogoEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _logo2, new PropertyChangedEventHandler( OnLogo2PropertyChanged ), "Logo2", VarioSL.Entities.RelationClasses.StaticAttributeValueRelations.LogoEntityUsingLogo2IDStatic, true, ref _alreadyFetchedLogo2, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLogo2PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="attributeValueID">PK value for AttributeValue which data should be fetched into this AttributeValue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 attributeValueID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AttributeValueFieldIndex.AttributeValueID].ForcedCurrentValueWrite(attributeValueID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAttributeValueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AttributeValueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AttributeValueRelations Relations
		{
			get	{ return new AttributeValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaList
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListCollection(), (IEntityRelation)GetRelationsForField("AreaList")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.AreaListEntity, 0, null, null, null, "AreaList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValueList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueList
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueListCollection(), (IEntityRelation)GetRelationsForField("AttributeValueList")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.AttributeValueListEntity, 0, null, null, null, "AttributeValueList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleResultUserGroup
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleResultUserGroup")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, 0, null, null, null, "BusinessRuleResultUserGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Choice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ChoiceCollection(), (IEntityRelation)GetRelationsForField("Choices")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.ChoiceEntity, 0, null, null, null, "Choices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDirectionAttributeValueFareMatrixEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("DirectionAttributeValueFareMatrixEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "DirectionAttributeValueFareMatrixEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDistanceAttributeValueFareMatrixEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("DistanceAttributeValueFareMatrixEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "DistanceAttributeValueFareMatrixEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLineFilterAttributeValueFareMatrixEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("LineFilterAttributeValueFareMatrixEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "LineFilterAttributeValueFareMatrixEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffAttributeValueFareMatrixEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("TariffAttributeValueFareMatrixEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "TariffAttributeValueFareMatrixEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUseDaysAttributeFareMatrixEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("UseDaysAttributeFareMatrixEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "UseDaysAttributeFareMatrixEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageLists
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageListCollection(), (IEntityRelation)GetRelationsForField("FareStageLists")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareStageListEntity, 0, null, null, null, "FareStageLists", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTableEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTableEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableEntryCollection(), (IEntityRelation)GetRelationsForField("FareTableEntries")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.FareTableEntryEntity, 0, null, null, null, "FareTableEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeyAttributeTransfrom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKey1AttributeTransforms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection(), (IEntityRelation)GetRelationsForField("Key1AttributeTransforms")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, 0, null, null, null, "Key1AttributeTransforms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeyAttributeTransfrom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKey2AttributeTransforms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection(), (IEntityRelation)GetRelationsForField("Key2AttributeTransforms")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, 0, null, null, null, "Key2AttributeTransforms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterAttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterAttributeValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection(), (IEntityRelation)GetRelationsForField("ParameterAttributeValues")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.ParameterAttributeValueEntity, 0, null, null, null, "ParameterAttributeValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffAttributeRoutes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("TariffAttributeRoutes")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "TariffAttributeRoutes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketGroupAttributeRoutes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("TicketGroupAttributeRoutes")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "TicketGroupAttributeRoutes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProduct
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProduct")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaAttributeValueList
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AttributeValueListEntityUsingAttributeValueID;
				intermediateRelation.SetAliases(string.Empty, "AttributeValueList_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaAttributeValueList"), "TicketCollectionViaAttributeValueList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attribute'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttribute
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeCollection(), (IEntityRelation)GetRelationsForField("Attribute")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.AttributeEntity, 0, null, null, null, "Attribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Logo'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLogo1
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LogoCollection(), (IEntityRelation)GetRelationsForField("Logo1")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.LogoEntity, 0, null, null, null, "Logo1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Logo'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLogo2
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LogoCollection(), (IEntityRelation)GetRelationsForField("Logo2")[0], (int)VarioSL.Entities.EntityType.AttributeValueEntity, (int)VarioSL.Entities.EntityType.LogoEntity, 0, null, null, null, "Logo2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeID property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeValueFieldIndex.AttributeID, false); }
			set	{ SetValue((int)AttributeValueFieldIndex.AttributeID, value, true); }
		}

		/// <summary> The AttributeValueID property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."ATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 AttributeValueID
		{
			get { return (System.Int64)GetValue((int)AttributeValueFieldIndex.AttributeValueID, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.AttributeValueID, value, true); }
		}

		/// <summary> The Description property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.Description, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.Description, value, true); }
		}

		/// <summary> The DisplayText property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."DISPLAYTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayText
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.DisplayText, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.DisplayText, value, true); }
		}

		/// <summary> The EnumerationValue property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."ENUMERATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EnumerationValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeValueFieldIndex.EnumerationValue, false); }
			set	{ SetValue((int)AttributeValueFieldIndex.EnumerationValue, value, true); }
		}

		/// <summary> The EtickLayoutText property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."ETICKETLAYOUTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EtickLayoutText
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.EtickLayoutText, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.EtickLayoutText, value, true); }
		}

		/// <summary> The FistLayoutText property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."FIRSTLAYOUTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FistLayoutText
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.FistLayoutText, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.FistLayoutText, value, true); }
		}

		/// <summary> The Logo1ID property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."LOGOID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Logo1ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeValueFieldIndex.Logo1ID, false); }
			set	{ SetValue((int)AttributeValueFieldIndex.Logo1ID, value, true); }
		}

		/// <summary> The Logo2ID property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."LOGO2ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Logo2ID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeValueFieldIndex.Logo2ID, false); }
			set	{ SetValue((int)AttributeValueFieldIndex.Logo2ID, value, true); }
		}

		/// <summary> The Name property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.Name, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)AttributeValueFieldIndex.Number, false); }
			set	{ SetValue((int)AttributeValueFieldIndex.Number, value, true); }
		}

		/// <summary> The Restricted property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."RESTRICTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Restricted
		{
			get { return (System.Int16)GetValue((int)AttributeValueFieldIndex.Restricted, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.Restricted, value, true); }
		}

		/// <summary> The SecondLayoutText property of the Entity AttributeValue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_ATTRIBUTEVALUE"."SECONDLAYOUTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SecondLayoutText
		{
			get { return (System.String)GetValue((int)AttributeValueFieldIndex.SecondLayoutText, true); }
			set	{ SetValue((int)AttributeValueFieldIndex.SecondLayoutText, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection AreaList
		{
			get	{ return GetMultiAreaList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaList. When set to true, AreaList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaList is accessed. You can always execute/ a forced fetch by calling GetMultiAreaList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaList
		{
			get	{ return _alwaysFetchAreaList; }
			set	{ _alwaysFetchAreaList = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaList already has been fetched. Setting this property to false when AreaList has been fetched
		/// will clear the AreaList collection well. Setting this property to true while AreaList hasn't been fetched disables lazy loading for AreaList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaList
		{
			get { return _alreadyFetchedAreaList;}
			set 
			{
				if(_alreadyFetchedAreaList && !value && (_areaList != null))
				{
					_areaList.Clear();
				}
				_alreadyFetchedAreaList = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeValueListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeValueList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueListCollection AttributeValueList
		{
			get	{ return GetMultiAttributeValueList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueList. When set to true, AttributeValueList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueList is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeValueList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueList
		{
			get	{ return _alwaysFetchAttributeValueList; }
			set	{ _alwaysFetchAttributeValueList = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueList already has been fetched. Setting this property to false when AttributeValueList has been fetched
		/// will clear the AttributeValueList collection well. Setting this property to true while AttributeValueList hasn't been fetched disables lazy loading for AttributeValueList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueList
		{
			get { return _alreadyFetchedAttributeValueList;}
			set 
			{
				if(_alreadyFetchedAttributeValueList && !value && (_attributeValueList != null))
				{
					_attributeValueList.Clear();
				}
				_alreadyFetchedAttributeValueList = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleResultUserGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection BusinessRuleResultUserGroup
		{
			get	{ return GetMultiBusinessRuleResultUserGroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleResultUserGroup. When set to true, BusinessRuleResultUserGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleResultUserGroup is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleResultUserGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleResultUserGroup
		{
			get	{ return _alwaysFetchBusinessRuleResultUserGroup; }
			set	{ _alwaysFetchBusinessRuleResultUserGroup = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleResultUserGroup already has been fetched. Setting this property to false when BusinessRuleResultUserGroup has been fetched
		/// will clear the BusinessRuleResultUserGroup collection well. Setting this property to true while BusinessRuleResultUserGroup hasn't been fetched disables lazy loading for BusinessRuleResultUserGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleResultUserGroup
		{
			get { return _alreadyFetchedBusinessRuleResultUserGroup;}
			set 
			{
				if(_alreadyFetchedBusinessRuleResultUserGroup && !value && (_businessRuleResultUserGroup != null))
				{
					_businessRuleResultUserGroup.Clear();
				}
				_alreadyFetchedBusinessRuleResultUserGroup = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection Choices
		{
			get	{ return GetMultiChoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Choices. When set to true, Choices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Choices is accessed. You can always execute/ a forced fetch by calling GetMultiChoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChoices
		{
			get	{ return _alwaysFetchChoices; }
			set	{ _alwaysFetchChoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Choices already has been fetched. Setting this property to false when Choices has been fetched
		/// will clear the Choices collection well. Setting this property to true while Choices hasn't been fetched disables lazy loading for Choices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChoices
		{
			get { return _alreadyFetchedChoices;}
			set 
			{
				if(_alreadyFetchedChoices && !value && (_choices != null))
				{
					_choices.Clear();
				}
				_alreadyFetchedChoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDirectionAttributeValueFareMatrixEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection DirectionAttributeValueFareMatrixEntries
		{
			get	{ return GetMultiDirectionAttributeValueFareMatrixEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DirectionAttributeValueFareMatrixEntries. When set to true, DirectionAttributeValueFareMatrixEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DirectionAttributeValueFareMatrixEntries is accessed. You can always execute/ a forced fetch by calling GetMultiDirectionAttributeValueFareMatrixEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDirectionAttributeValueFareMatrixEntries
		{
			get	{ return _alwaysFetchDirectionAttributeValueFareMatrixEntries; }
			set	{ _alwaysFetchDirectionAttributeValueFareMatrixEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DirectionAttributeValueFareMatrixEntries already has been fetched. Setting this property to false when DirectionAttributeValueFareMatrixEntries has been fetched
		/// will clear the DirectionAttributeValueFareMatrixEntries collection well. Setting this property to true while DirectionAttributeValueFareMatrixEntries hasn't been fetched disables lazy loading for DirectionAttributeValueFareMatrixEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDirectionAttributeValueFareMatrixEntries
		{
			get { return _alreadyFetchedDirectionAttributeValueFareMatrixEntries;}
			set 
			{
				if(_alreadyFetchedDirectionAttributeValueFareMatrixEntries && !value && (_directionAttributeValueFareMatrixEntries != null))
				{
					_directionAttributeValueFareMatrixEntries.Clear();
				}
				_alreadyFetchedDirectionAttributeValueFareMatrixEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDistanceAttributeValueFareMatrixEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection DistanceAttributeValueFareMatrixEntries
		{
			get	{ return GetMultiDistanceAttributeValueFareMatrixEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DistanceAttributeValueFareMatrixEntries. When set to true, DistanceAttributeValueFareMatrixEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DistanceAttributeValueFareMatrixEntries is accessed. You can always execute/ a forced fetch by calling GetMultiDistanceAttributeValueFareMatrixEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDistanceAttributeValueFareMatrixEntries
		{
			get	{ return _alwaysFetchDistanceAttributeValueFareMatrixEntries; }
			set	{ _alwaysFetchDistanceAttributeValueFareMatrixEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DistanceAttributeValueFareMatrixEntries already has been fetched. Setting this property to false when DistanceAttributeValueFareMatrixEntries has been fetched
		/// will clear the DistanceAttributeValueFareMatrixEntries collection well. Setting this property to true while DistanceAttributeValueFareMatrixEntries hasn't been fetched disables lazy loading for DistanceAttributeValueFareMatrixEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDistanceAttributeValueFareMatrixEntries
		{
			get { return _alreadyFetchedDistanceAttributeValueFareMatrixEntries;}
			set 
			{
				if(_alreadyFetchedDistanceAttributeValueFareMatrixEntries && !value && (_distanceAttributeValueFareMatrixEntries != null))
				{
					_distanceAttributeValueFareMatrixEntries.Clear();
				}
				_alreadyFetchedDistanceAttributeValueFareMatrixEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLineFilterAttributeValueFareMatrixEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection LineFilterAttributeValueFareMatrixEntries
		{
			get	{ return GetMultiLineFilterAttributeValueFareMatrixEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LineFilterAttributeValueFareMatrixEntries. When set to true, LineFilterAttributeValueFareMatrixEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LineFilterAttributeValueFareMatrixEntries is accessed. You can always execute/ a forced fetch by calling GetMultiLineFilterAttributeValueFareMatrixEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLineFilterAttributeValueFareMatrixEntries
		{
			get	{ return _alwaysFetchLineFilterAttributeValueFareMatrixEntries; }
			set	{ _alwaysFetchLineFilterAttributeValueFareMatrixEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LineFilterAttributeValueFareMatrixEntries already has been fetched. Setting this property to false when LineFilterAttributeValueFareMatrixEntries has been fetched
		/// will clear the LineFilterAttributeValueFareMatrixEntries collection well. Setting this property to true while LineFilterAttributeValueFareMatrixEntries hasn't been fetched disables lazy loading for LineFilterAttributeValueFareMatrixEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLineFilterAttributeValueFareMatrixEntries
		{
			get { return _alreadyFetchedLineFilterAttributeValueFareMatrixEntries;}
			set 
			{
				if(_alreadyFetchedLineFilterAttributeValueFareMatrixEntries && !value && (_lineFilterAttributeValueFareMatrixEntries != null))
				{
					_lineFilterAttributeValueFareMatrixEntries.Clear();
				}
				_alreadyFetchedLineFilterAttributeValueFareMatrixEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffAttributeValueFareMatrixEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection TariffAttributeValueFareMatrixEntries
		{
			get	{ return GetMultiTariffAttributeValueFareMatrixEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TariffAttributeValueFareMatrixEntries. When set to true, TariffAttributeValueFareMatrixEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TariffAttributeValueFareMatrixEntries is accessed. You can always execute/ a forced fetch by calling GetMultiTariffAttributeValueFareMatrixEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffAttributeValueFareMatrixEntries
		{
			get	{ return _alwaysFetchTariffAttributeValueFareMatrixEntries; }
			set	{ _alwaysFetchTariffAttributeValueFareMatrixEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TariffAttributeValueFareMatrixEntries already has been fetched. Setting this property to false when TariffAttributeValueFareMatrixEntries has been fetched
		/// will clear the TariffAttributeValueFareMatrixEntries collection well. Setting this property to true while TariffAttributeValueFareMatrixEntries hasn't been fetched disables lazy loading for TariffAttributeValueFareMatrixEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffAttributeValueFareMatrixEntries
		{
			get { return _alreadyFetchedTariffAttributeValueFareMatrixEntries;}
			set 
			{
				if(_alreadyFetchedTariffAttributeValueFareMatrixEntries && !value && (_tariffAttributeValueFareMatrixEntries != null))
				{
					_tariffAttributeValueFareMatrixEntries.Clear();
				}
				_alreadyFetchedTariffAttributeValueFareMatrixEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUseDaysAttributeFareMatrixEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection UseDaysAttributeFareMatrixEntries
		{
			get	{ return GetMultiUseDaysAttributeFareMatrixEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UseDaysAttributeFareMatrixEntries. When set to true, UseDaysAttributeFareMatrixEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UseDaysAttributeFareMatrixEntries is accessed. You can always execute/ a forced fetch by calling GetMultiUseDaysAttributeFareMatrixEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUseDaysAttributeFareMatrixEntries
		{
			get	{ return _alwaysFetchUseDaysAttributeFareMatrixEntries; }
			set	{ _alwaysFetchUseDaysAttributeFareMatrixEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UseDaysAttributeFareMatrixEntries already has been fetched. Setting this property to false when UseDaysAttributeFareMatrixEntries has been fetched
		/// will clear the UseDaysAttributeFareMatrixEntries collection well. Setting this property to true while UseDaysAttributeFareMatrixEntries hasn't been fetched disables lazy loading for UseDaysAttributeFareMatrixEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUseDaysAttributeFareMatrixEntries
		{
			get { return _alreadyFetchedUseDaysAttributeFareMatrixEntries;}
			set 
			{
				if(_alreadyFetchedUseDaysAttributeFareMatrixEntries && !value && (_useDaysAttributeFareMatrixEntries != null))
				{
					_useDaysAttributeFareMatrixEntries.Clear();
				}
				_alreadyFetchedUseDaysAttributeFareMatrixEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareStageLists()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareStageListCollection FareStageLists
		{
			get	{ return GetMultiFareStageLists(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageLists. When set to true, FareStageLists is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageLists is accessed. You can always execute/ a forced fetch by calling GetMultiFareStageLists(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageLists
		{
			get	{ return _alwaysFetchFareStageLists; }
			set	{ _alwaysFetchFareStageLists = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageLists already has been fetched. Setting this property to false when FareStageLists has been fetched
		/// will clear the FareStageLists collection well. Setting this property to true while FareStageLists hasn't been fetched disables lazy loading for FareStageLists</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageLists
		{
			get { return _alreadyFetchedFareStageLists;}
			set 
			{
				if(_alreadyFetchedFareStageLists && !value && (_fareStageLists != null))
				{
					_fareStageLists.Clear();
				}
				_alreadyFetchedFareStageLists = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTableEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection FareTableEntries
		{
			get	{ return GetMultiFareTableEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTableEntries. When set to true, FareTableEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTableEntries is accessed. You can always execute/ a forced fetch by calling GetMultiFareTableEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTableEntries
		{
			get	{ return _alwaysFetchFareTableEntries; }
			set	{ _alwaysFetchFareTableEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTableEntries already has been fetched. Setting this property to false when FareTableEntries has been fetched
		/// will clear the FareTableEntries collection well. Setting this property to true while FareTableEntries hasn't been fetched disables lazy loading for FareTableEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTableEntries
		{
			get { return _alreadyFetchedFareTableEntries;}
			set 
			{
				if(_alreadyFetchedFareTableEntries && !value && (_fareTableEntries != null))
				{
					_fareTableEntries.Clear();
				}
				_alreadyFetchedFareTableEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiKey1AttributeTransforms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection Key1AttributeTransforms
		{
			get	{ return GetMultiKey1AttributeTransforms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Key1AttributeTransforms. When set to true, Key1AttributeTransforms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Key1AttributeTransforms is accessed. You can always execute/ a forced fetch by calling GetMultiKey1AttributeTransforms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKey1AttributeTransforms
		{
			get	{ return _alwaysFetchKey1AttributeTransforms; }
			set	{ _alwaysFetchKey1AttributeTransforms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Key1AttributeTransforms already has been fetched. Setting this property to false when Key1AttributeTransforms has been fetched
		/// will clear the Key1AttributeTransforms collection well. Setting this property to true while Key1AttributeTransforms hasn't been fetched disables lazy loading for Key1AttributeTransforms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKey1AttributeTransforms
		{
			get { return _alreadyFetchedKey1AttributeTransforms;}
			set 
			{
				if(_alreadyFetchedKey1AttributeTransforms && !value && (_key1AttributeTransforms != null))
				{
					_key1AttributeTransforms.Clear();
				}
				_alreadyFetchedKey1AttributeTransforms = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiKey2AttributeTransforms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection Key2AttributeTransforms
		{
			get	{ return GetMultiKey2AttributeTransforms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Key2AttributeTransforms. When set to true, Key2AttributeTransforms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Key2AttributeTransforms is accessed. You can always execute/ a forced fetch by calling GetMultiKey2AttributeTransforms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKey2AttributeTransforms
		{
			get	{ return _alwaysFetchKey2AttributeTransforms; }
			set	{ _alwaysFetchKey2AttributeTransforms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Key2AttributeTransforms already has been fetched. Setting this property to false when Key2AttributeTransforms has been fetched
		/// will clear the Key2AttributeTransforms collection well. Setting this property to true while Key2AttributeTransforms hasn't been fetched disables lazy loading for Key2AttributeTransforms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKey2AttributeTransforms
		{
			get { return _alreadyFetchedKey2AttributeTransforms;}
			set 
			{
				if(_alreadyFetchedKey2AttributeTransforms && !value && (_key2AttributeTransforms != null))
				{
					_key2AttributeTransforms.Clear();
				}
				_alreadyFetchedKey2AttributeTransforms = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterAttributeValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection ParameterAttributeValues
		{
			get	{ return GetMultiParameterAttributeValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterAttributeValues. When set to true, ParameterAttributeValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterAttributeValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterAttributeValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterAttributeValues
		{
			get	{ return _alwaysFetchParameterAttributeValues; }
			set	{ _alwaysFetchParameterAttributeValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterAttributeValues already has been fetched. Setting this property to false when ParameterAttributeValues has been fetched
		/// will clear the ParameterAttributeValues collection well. Setting this property to true while ParameterAttributeValues hasn't been fetched disables lazy loading for ParameterAttributeValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterAttributeValues
		{
			get { return _alreadyFetchedParameterAttributeValues;}
			set 
			{
				if(_alreadyFetchedParameterAttributeValues && !value && (_parameterAttributeValues != null))
				{
					_parameterAttributeValues.Clear();
				}
				_alreadyFetchedParameterAttributeValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffAttributeRoutes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection TariffAttributeRoutes
		{
			get	{ return GetMultiTariffAttributeRoutes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TariffAttributeRoutes. When set to true, TariffAttributeRoutes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TariffAttributeRoutes is accessed. You can always execute/ a forced fetch by calling GetMultiTariffAttributeRoutes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffAttributeRoutes
		{
			get	{ return _alwaysFetchTariffAttributeRoutes; }
			set	{ _alwaysFetchTariffAttributeRoutes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TariffAttributeRoutes already has been fetched. Setting this property to false when TariffAttributeRoutes has been fetched
		/// will clear the TariffAttributeRoutes collection well. Setting this property to true while TariffAttributeRoutes hasn't been fetched disables lazy loading for TariffAttributeRoutes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffAttributeRoutes
		{
			get { return _alreadyFetchedTariffAttributeRoutes;}
			set 
			{
				if(_alreadyFetchedTariffAttributeRoutes && !value && (_tariffAttributeRoutes != null))
				{
					_tariffAttributeRoutes.Clear();
				}
				_alreadyFetchedTariffAttributeRoutes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketGroupAttributeRoutes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection TicketGroupAttributeRoutes
		{
			get	{ return GetMultiTicketGroupAttributeRoutes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketGroupAttributeRoutes. When set to true, TicketGroupAttributeRoutes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketGroupAttributeRoutes is accessed. You can always execute/ a forced fetch by calling GetMultiTicketGroupAttributeRoutes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketGroupAttributeRoutes
		{
			get	{ return _alwaysFetchTicketGroupAttributeRoutes; }
			set	{ _alwaysFetchTicketGroupAttributeRoutes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketGroupAttributeRoutes already has been fetched. Setting this property to false when TicketGroupAttributeRoutes has been fetched
		/// will clear the TicketGroupAttributeRoutes collection well. Setting this property to true while TicketGroupAttributeRoutes hasn't been fetched disables lazy loading for TicketGroupAttributeRoutes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketGroupAttributeRoutes
		{
			get { return _alreadyFetchedTicketGroupAttributeRoutes;}
			set 
			{
				if(_alreadyFetchedTicketGroupAttributeRoutes && !value && (_ticketGroupAttributeRoutes != null))
				{
					_ticketGroupAttributeRoutes.Clear();
				}
				_alreadyFetchedTicketGroupAttributeRoutes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProduct
		{
			get	{ return GetMultiVdvProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProduct. When set to true, VdvProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProduct is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProduct
		{
			get	{ return _alwaysFetchVdvProduct; }
			set	{ _alwaysFetchVdvProduct = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProduct already has been fetched. Setting this property to false when VdvProduct has been fetched
		/// will clear the VdvProduct collection well. Setting this property to true while VdvProduct hasn't been fetched disables lazy loading for VdvProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProduct
		{
			get { return _alreadyFetchedVdvProduct;}
			set 
			{
				if(_alreadyFetchedVdvProduct && !value && (_vdvProduct != null))
				{
					_vdvProduct.Clear();
				}
				_alreadyFetchedVdvProduct = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaAttributeValueList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaAttributeValueList
		{
			get { return GetMultiTicketCollectionViaAttributeValueList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaAttributeValueList. When set to true, TicketCollectionViaAttributeValueList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaAttributeValueList is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaAttributeValueList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaAttributeValueList
		{
			get	{ return _alwaysFetchTicketCollectionViaAttributeValueList; }
			set	{ _alwaysFetchTicketCollectionViaAttributeValueList = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaAttributeValueList already has been fetched. Setting this property to false when TicketCollectionViaAttributeValueList has been fetched
		/// will clear the TicketCollectionViaAttributeValueList collection well. Setting this property to true while TicketCollectionViaAttributeValueList hasn't been fetched disables lazy loading for TicketCollectionViaAttributeValueList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaAttributeValueList
		{
			get { return _alreadyFetchedTicketCollectionViaAttributeValueList;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaAttributeValueList && !value && (_ticketCollectionViaAttributeValueList != null))
				{
					_ticketCollectionViaAttributeValueList.Clear();
				}
				_alreadyFetchedTicketCollectionViaAttributeValueList = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AttributeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttribute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeEntity Attribute
		{
			get	{ return GetSingleAttribute(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttribute(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeValues", "Attribute", _attribute, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Attribute. When set to true, Attribute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Attribute is accessed. You can always execute a forced fetch by calling GetSingleAttribute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttribute
		{
			get	{ return _alwaysFetchAttribute; }
			set	{ _alwaysFetchAttribute = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Attribute already has been fetched. Setting this property to false when Attribute has been fetched
		/// will set Attribute to null as well. Setting this property to true while Attribute hasn't been fetched disables lazy loading for Attribute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttribute
		{
			get { return _alreadyFetchedAttribute;}
			set 
			{
				if(_alreadyFetchedAttribute && !value)
				{
					this.Attribute = null;
				}
				_alreadyFetchedAttribute = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Attribute is not found
		/// in the database. When set to true, Attribute will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeReturnsNewIfNotFound
		{
			get	{ return _attributeReturnsNewIfNotFound; }
			set { _attributeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LogoEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLogo1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LogoEntity Logo1
		{
			get	{ return GetSingleLogo1(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLogo1(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeValue", "Logo1", _logo1, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Logo1. When set to true, Logo1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Logo1 is accessed. You can always execute a forced fetch by calling GetSingleLogo1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLogo1
		{
			get	{ return _alwaysFetchLogo1; }
			set	{ _alwaysFetchLogo1 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Logo1 already has been fetched. Setting this property to false when Logo1 has been fetched
		/// will set Logo1 to null as well. Setting this property to true while Logo1 hasn't been fetched disables lazy loading for Logo1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLogo1
		{
			get { return _alreadyFetchedLogo1;}
			set 
			{
				if(_alreadyFetchedLogo1 && !value)
				{
					this.Logo1 = null;
				}
				_alreadyFetchedLogo1 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Logo1 is not found
		/// in the database. When set to true, Logo1 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool Logo1ReturnsNewIfNotFound
		{
			get	{ return _logo1ReturnsNewIfNotFound; }
			set { _logo1ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'LogoEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleLogo2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual LogoEntity Logo2
		{
			get	{ return GetSingleLogo2(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncLogo2(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AttributeValue_", "Logo2", _logo2, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Logo2. When set to true, Logo2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Logo2 is accessed. You can always execute a forced fetch by calling GetSingleLogo2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLogo2
		{
			get	{ return _alwaysFetchLogo2; }
			set	{ _alwaysFetchLogo2 = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Logo2 already has been fetched. Setting this property to false when Logo2 has been fetched
		/// will set Logo2 to null as well. Setting this property to true while Logo2 hasn't been fetched disables lazy loading for Logo2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLogo2
		{
			get { return _alreadyFetchedLogo2;}
			set 
			{
				if(_alreadyFetchedLogo2 && !value)
				{
					this.Logo2 = null;
				}
				_alreadyFetchedLogo2 = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Logo2 is not found
		/// in the database. When set to true, Logo2 will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool Logo2ReturnsNewIfNotFound
		{
			get	{ return _logo2ReturnsNewIfNotFound; }
			set { _logo2ReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AttributeValueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
