﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Contract'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ContractEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardEventCollection	_cardEvents;
		private bool	_alwaysFetchCardEvents, _alreadyFetchedCardEvents;
		private VarioSL.Entities.CollectionClasses.CardHolderCollection	_cardHolders;
		private bool	_alwaysFetchCardHolders, _alreadyFetchedCardHolders;
		private VarioSL.Entities.CollectionClasses.CardToContractCollection	_cardsToContracts;
		private bool	_alwaysFetchCardsToContracts, _alreadyFetchedCardsToContracts;
		private VarioSL.Entities.CollectionClasses.CommentCollection	_slComments;
		private bool	_alwaysFetchSlComments, _alreadyFetchedSlComments;
		private VarioSL.Entities.CollectionClasses.ContractAddressCollection	_contractAddresses;
		private bool	_alwaysFetchContractAddresses, _alreadyFetchedContractAddresses;
		private VarioSL.Entities.CollectionClasses.ContractHistoryCollection	_contractHistoryItems;
		private bool	_alwaysFetchContractHistoryItems, _alreadyFetchedContractHistoryItems;
		private VarioSL.Entities.CollectionClasses.ContractLinkCollection	_targetContracts;
		private bool	_alwaysFetchTargetContracts, _alreadyFetchedTargetContracts;
		private VarioSL.Entities.CollectionClasses.ContractLinkCollection	_sourceContracts;
		private bool	_alwaysFetchSourceContracts, _alreadyFetchedSourceContracts;
		private VarioSL.Entities.CollectionClasses.ContractTerminationCollection	_contractTerminations;
		private bool	_alwaysFetchContractTerminations, _alreadyFetchedContractTerminations;
		private VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection	_contractToPaymentMethods;
		private bool	_alwaysFetchContractToPaymentMethods, _alreadyFetchedContractToPaymentMethods;
		private VarioSL.Entities.CollectionClasses.ContractToProviderCollection	_contractToProviders;
		private bool	_alwaysFetchContractToProviders, _alreadyFetchedContractToProviders;
		private VarioSL.Entities.CollectionClasses.ContractWorkItemCollection	_workItems;
		private bool	_alwaysFetchWorkItems, _alreadyFetchedWorkItems;
		private VarioSL.Entities.CollectionClasses.CustomerAccountCollection	_customerAccounts;
		private bool	_alwaysFetchCustomerAccounts, _alreadyFetchedCustomerAccounts;
		private VarioSL.Entities.CollectionClasses.DunningProcessCollection	_dunningProcesses;
		private bool	_alwaysFetchDunningProcesses, _alreadyFetchedDunningProcesses;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidents;
		private bool	_alwaysFetchFareEvasionIncidents, _alreadyFetchedFareEvasionIncidents;
		private VarioSL.Entities.CollectionClasses.InvoiceCollection	_invoices;
		private bool	_alwaysFetchInvoices, _alreadyFetchedInvoices;
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private VarioSL.Entities.CollectionClasses.JobBulkImportCollection	_jobBulkImports;
		private bool	_alwaysFetchJobBulkImports, _alreadyFetchedJobBulkImports;
		private VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection	_notificationMessageOwners;
		private bool	_alwaysFetchNotificationMessageOwners, _alreadyFetchedNotificationMessageOwners;
		private VarioSL.Entities.CollectionClasses.OrderCollection	_orders;
		private bool	_alwaysFetchOrders, _alreadyFetchedOrders;
		private VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection	_organizationParticipants;
		private bool	_alwaysFetchOrganizationParticipants, _alreadyFetchedOrganizationParticipants;
		private VarioSL.Entities.CollectionClasses.ParticipantGroupCollection	_participantGroups;
		private bool	_alwaysFetchParticipantGroups, _alreadyFetchedParticipantGroups;
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.PaymentOptionCollection	_paymentOptions;
		private bool	_alwaysFetchPaymentOptions, _alreadyFetchedPaymentOptions;
		private VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection	_paymentProviderAccounts;
		private bool	_alwaysFetchPaymentProviderAccounts, _alreadyFetchedPaymentProviderAccounts;
		private VarioSL.Entities.CollectionClasses.PhotographCollection	_photographs;
		private bool	_alwaysFetchPhotographs, _alreadyFetchedPhotographs;
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private VarioSL.Entities.CollectionClasses.ProductTerminationCollection	_productTerminations;
		private bool	_alwaysFetchProductTerminations, _alreadyFetchedProductTerminations;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_secondarySales;
		private bool	_alwaysFetchSecondarySales, _alreadyFetchedSecondarySales;
		private VarioSL.Entities.CollectionClasses.StatementOfAccountCollection	_slStatementofaccounts;
		private bool	_alwaysFetchSlStatementofaccounts, _alreadyFetchedSlStatementofaccounts;
		private VarioSL.Entities.CollectionClasses.TicketAssignmentCollection	_ticketAssignments;
		private bool	_alwaysFetchTicketAssignments, _alreadyFetchedTicketAssignments;
		private VarioSL.Entities.CollectionClasses.AddressCollection _addresses;
		private bool	_alwaysFetchAddresses, _alreadyFetchedAddresses;
		private VarioSL.Entities.CollectionClasses.CardCollection _cards;
		private bool	_alwaysFetchCards, _alreadyFetchedCards;
		private VarioSL.Entities.CollectionClasses.NotificationMessageCollection _notificationMessages;
		private bool	_alwaysFetchNotificationMessages, _alreadyFetchedNotificationMessages;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private ClientEntity _vanpoolClient;
		private bool	_alwaysFetchVanpoolClient, _alreadyFetchedVanpoolClient, _vanpoolClientReturnsNewIfNotFound;
		private AccountingModeEntity _accountingMode;
		private bool	_alwaysFetchAccountingMode, _alreadyFetchedAccountingMode, _accountingModeReturnsNewIfNotFound;
		private OrganizationEntity _organization;
		private bool	_alwaysFetchOrganization, _alreadyFetchedOrganization, _organizationReturnsNewIfNotFound;
		private PaymentModalityEntity _paymentModality;
		private bool	_alwaysFetchPaymentModality, _alreadyFetchedPaymentModality, _paymentModalityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name VanpoolClient</summary>
			public static readonly string VanpoolClient = "VanpoolClient";
			/// <summary>Member name AccountingMode</summary>
			public static readonly string AccountingMode = "AccountingMode";
			/// <summary>Member name Organization</summary>
			public static readonly string Organization = "Organization";
			/// <summary>Member name CardEvents</summary>
			public static readonly string CardEvents = "CardEvents";
			/// <summary>Member name CardHolders</summary>
			public static readonly string CardHolders = "CardHolders";
			/// <summary>Member name CardsToContracts</summary>
			public static readonly string CardsToContracts = "CardsToContracts";
			/// <summary>Member name SlComments</summary>
			public static readonly string SlComments = "SlComments";
			/// <summary>Member name ContractAddresses</summary>
			public static readonly string ContractAddresses = "ContractAddresses";
			/// <summary>Member name ContractHistoryItems</summary>
			public static readonly string ContractHistoryItems = "ContractHistoryItems";
			/// <summary>Member name TargetContracts</summary>
			public static readonly string TargetContracts = "TargetContracts";
			/// <summary>Member name SourceContracts</summary>
			public static readonly string SourceContracts = "SourceContracts";
			/// <summary>Member name ContractTerminations</summary>
			public static readonly string ContractTerminations = "ContractTerminations";
			/// <summary>Member name ContractToPaymentMethods</summary>
			public static readonly string ContractToPaymentMethods = "ContractToPaymentMethods";
			/// <summary>Member name ContractToProviders</summary>
			public static readonly string ContractToProviders = "ContractToProviders";
			/// <summary>Member name WorkItems</summary>
			public static readonly string WorkItems = "WorkItems";
			/// <summary>Member name CustomerAccounts</summary>
			public static readonly string CustomerAccounts = "CustomerAccounts";
			/// <summary>Member name DunningProcesses</summary>
			public static readonly string DunningProcesses = "DunningProcesses";
			/// <summary>Member name FareEvasionIncidents</summary>
			public static readonly string FareEvasionIncidents = "FareEvasionIncidents";
			/// <summary>Member name Invoices</summary>
			public static readonly string Invoices = "Invoices";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
			/// <summary>Member name JobBulkImports</summary>
			public static readonly string JobBulkImports = "JobBulkImports";
			/// <summary>Member name NotificationMessageOwners</summary>
			public static readonly string NotificationMessageOwners = "NotificationMessageOwners";
			/// <summary>Member name Orders</summary>
			public static readonly string Orders = "Orders";
			/// <summary>Member name OrganizationParticipants</summary>
			public static readonly string OrganizationParticipants = "OrganizationParticipants";
			/// <summary>Member name ParticipantGroups</summary>
			public static readonly string ParticipantGroups = "ParticipantGroups";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name PaymentOptions</summary>
			public static readonly string PaymentOptions = "PaymentOptions";
			/// <summary>Member name PaymentProviderAccounts</summary>
			public static readonly string PaymentProviderAccounts = "PaymentProviderAccounts";
			/// <summary>Member name Photographs</summary>
			public static readonly string Photographs = "Photographs";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
			/// <summary>Member name ProductTerminations</summary>
			public static readonly string ProductTerminations = "ProductTerminations";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name SecondarySales</summary>
			public static readonly string SecondarySales = "SecondarySales";
			/// <summary>Member name SlStatementofaccounts</summary>
			public static readonly string SlStatementofaccounts = "SlStatementofaccounts";
			/// <summary>Member name TicketAssignments</summary>
			public static readonly string TicketAssignments = "TicketAssignments";
			/// <summary>Member name Addresses</summary>
			public static readonly string Addresses = "Addresses";
			/// <summary>Member name Cards</summary>
			public static readonly string Cards = "Cards";
			/// <summary>Member name NotificationMessages</summary>
			public static readonly string NotificationMessages = "NotificationMessages";
			/// <summary>Member name PaymentModality</summary>
			public static readonly string PaymentModality = "PaymentModality";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ContractEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ContractEntity() :base("ContractEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		public ContractEntity(System.Int64 contractID):base("ContractEntity")
		{
			InitClassFetch(contractID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ContractEntity(System.Int64 contractID, IPrefetchPath prefetchPathToUse):base("ContractEntity")
		{
			InitClassFetch(contractID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="validator">The custom validator object for this ContractEntity</param>
		public ContractEntity(System.Int64 contractID, IValidator validator):base("ContractEntity")
		{
			InitClassFetch(contractID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ContractEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardEvents = (VarioSL.Entities.CollectionClasses.CardEventCollection)info.GetValue("_cardEvents", typeof(VarioSL.Entities.CollectionClasses.CardEventCollection));
			_alwaysFetchCardEvents = info.GetBoolean("_alwaysFetchCardEvents");
			_alreadyFetchedCardEvents = info.GetBoolean("_alreadyFetchedCardEvents");

			_cardHolders = (VarioSL.Entities.CollectionClasses.CardHolderCollection)info.GetValue("_cardHolders", typeof(VarioSL.Entities.CollectionClasses.CardHolderCollection));
			_alwaysFetchCardHolders = info.GetBoolean("_alwaysFetchCardHolders");
			_alreadyFetchedCardHolders = info.GetBoolean("_alreadyFetchedCardHolders");

			_cardsToContracts = (VarioSL.Entities.CollectionClasses.CardToContractCollection)info.GetValue("_cardsToContracts", typeof(VarioSL.Entities.CollectionClasses.CardToContractCollection));
			_alwaysFetchCardsToContracts = info.GetBoolean("_alwaysFetchCardsToContracts");
			_alreadyFetchedCardsToContracts = info.GetBoolean("_alreadyFetchedCardsToContracts");

			_slComments = (VarioSL.Entities.CollectionClasses.CommentCollection)info.GetValue("_slComments", typeof(VarioSL.Entities.CollectionClasses.CommentCollection));
			_alwaysFetchSlComments = info.GetBoolean("_alwaysFetchSlComments");
			_alreadyFetchedSlComments = info.GetBoolean("_alreadyFetchedSlComments");

			_contractAddresses = (VarioSL.Entities.CollectionClasses.ContractAddressCollection)info.GetValue("_contractAddresses", typeof(VarioSL.Entities.CollectionClasses.ContractAddressCollection));
			_alwaysFetchContractAddresses = info.GetBoolean("_alwaysFetchContractAddresses");
			_alreadyFetchedContractAddresses = info.GetBoolean("_alreadyFetchedContractAddresses");

			_contractHistoryItems = (VarioSL.Entities.CollectionClasses.ContractHistoryCollection)info.GetValue("_contractHistoryItems", typeof(VarioSL.Entities.CollectionClasses.ContractHistoryCollection));
			_alwaysFetchContractHistoryItems = info.GetBoolean("_alwaysFetchContractHistoryItems");
			_alreadyFetchedContractHistoryItems = info.GetBoolean("_alreadyFetchedContractHistoryItems");

			_targetContracts = (VarioSL.Entities.CollectionClasses.ContractLinkCollection)info.GetValue("_targetContracts", typeof(VarioSL.Entities.CollectionClasses.ContractLinkCollection));
			_alwaysFetchTargetContracts = info.GetBoolean("_alwaysFetchTargetContracts");
			_alreadyFetchedTargetContracts = info.GetBoolean("_alreadyFetchedTargetContracts");

			_sourceContracts = (VarioSL.Entities.CollectionClasses.ContractLinkCollection)info.GetValue("_sourceContracts", typeof(VarioSL.Entities.CollectionClasses.ContractLinkCollection));
			_alwaysFetchSourceContracts = info.GetBoolean("_alwaysFetchSourceContracts");
			_alreadyFetchedSourceContracts = info.GetBoolean("_alreadyFetchedSourceContracts");

			_contractTerminations = (VarioSL.Entities.CollectionClasses.ContractTerminationCollection)info.GetValue("_contractTerminations", typeof(VarioSL.Entities.CollectionClasses.ContractTerminationCollection));
			_alwaysFetchContractTerminations = info.GetBoolean("_alwaysFetchContractTerminations");
			_alreadyFetchedContractTerminations = info.GetBoolean("_alreadyFetchedContractTerminations");

			_contractToPaymentMethods = (VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection)info.GetValue("_contractToPaymentMethods", typeof(VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection));
			_alwaysFetchContractToPaymentMethods = info.GetBoolean("_alwaysFetchContractToPaymentMethods");
			_alreadyFetchedContractToPaymentMethods = info.GetBoolean("_alreadyFetchedContractToPaymentMethods");

			_contractToProviders = (VarioSL.Entities.CollectionClasses.ContractToProviderCollection)info.GetValue("_contractToProviders", typeof(VarioSL.Entities.CollectionClasses.ContractToProviderCollection));
			_alwaysFetchContractToProviders = info.GetBoolean("_alwaysFetchContractToProviders");
			_alreadyFetchedContractToProviders = info.GetBoolean("_alreadyFetchedContractToProviders");

			_workItems = (VarioSL.Entities.CollectionClasses.ContractWorkItemCollection)info.GetValue("_workItems", typeof(VarioSL.Entities.CollectionClasses.ContractWorkItemCollection));
			_alwaysFetchWorkItems = info.GetBoolean("_alwaysFetchWorkItems");
			_alreadyFetchedWorkItems = info.GetBoolean("_alreadyFetchedWorkItems");

			_customerAccounts = (VarioSL.Entities.CollectionClasses.CustomerAccountCollection)info.GetValue("_customerAccounts", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountCollection));
			_alwaysFetchCustomerAccounts = info.GetBoolean("_alwaysFetchCustomerAccounts");
			_alreadyFetchedCustomerAccounts = info.GetBoolean("_alreadyFetchedCustomerAccounts");

			_dunningProcesses = (VarioSL.Entities.CollectionClasses.DunningProcessCollection)info.GetValue("_dunningProcesses", typeof(VarioSL.Entities.CollectionClasses.DunningProcessCollection));
			_alwaysFetchDunningProcesses = info.GetBoolean("_alwaysFetchDunningProcesses");
			_alreadyFetchedDunningProcesses = info.GetBoolean("_alreadyFetchedDunningProcesses");

			_fareEvasionIncidents = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidents", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidents = info.GetBoolean("_alwaysFetchFareEvasionIncidents");
			_alreadyFetchedFareEvasionIncidents = info.GetBoolean("_alreadyFetchedFareEvasionIncidents");

			_invoices = (VarioSL.Entities.CollectionClasses.InvoiceCollection)info.GetValue("_invoices", typeof(VarioSL.Entities.CollectionClasses.InvoiceCollection));
			_alwaysFetchInvoices = info.GetBoolean("_alwaysFetchInvoices");
			_alreadyFetchedInvoices = info.GetBoolean("_alreadyFetchedInvoices");

			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");

			_jobBulkImports = (VarioSL.Entities.CollectionClasses.JobBulkImportCollection)info.GetValue("_jobBulkImports", typeof(VarioSL.Entities.CollectionClasses.JobBulkImportCollection));
			_alwaysFetchJobBulkImports = info.GetBoolean("_alwaysFetchJobBulkImports");
			_alreadyFetchedJobBulkImports = info.GetBoolean("_alreadyFetchedJobBulkImports");

			_notificationMessageOwners = (VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection)info.GetValue("_notificationMessageOwners", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection));
			_alwaysFetchNotificationMessageOwners = info.GetBoolean("_alwaysFetchNotificationMessageOwners");
			_alreadyFetchedNotificationMessageOwners = info.GetBoolean("_alreadyFetchedNotificationMessageOwners");

			_orders = (VarioSL.Entities.CollectionClasses.OrderCollection)info.GetValue("_orders", typeof(VarioSL.Entities.CollectionClasses.OrderCollection));
			_alwaysFetchOrders = info.GetBoolean("_alwaysFetchOrders");
			_alreadyFetchedOrders = info.GetBoolean("_alreadyFetchedOrders");

			_organizationParticipants = (VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection)info.GetValue("_organizationParticipants", typeof(VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection));
			_alwaysFetchOrganizationParticipants = info.GetBoolean("_alwaysFetchOrganizationParticipants");
			_alreadyFetchedOrganizationParticipants = info.GetBoolean("_alreadyFetchedOrganizationParticipants");

			_participantGroups = (VarioSL.Entities.CollectionClasses.ParticipantGroupCollection)info.GetValue("_participantGroups", typeof(VarioSL.Entities.CollectionClasses.ParticipantGroupCollection));
			_alwaysFetchParticipantGroups = info.GetBoolean("_alwaysFetchParticipantGroups");
			_alreadyFetchedParticipantGroups = info.GetBoolean("_alreadyFetchedParticipantGroups");

			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_paymentOptions = (VarioSL.Entities.CollectionClasses.PaymentOptionCollection)info.GetValue("_paymentOptions", typeof(VarioSL.Entities.CollectionClasses.PaymentOptionCollection));
			_alwaysFetchPaymentOptions = info.GetBoolean("_alwaysFetchPaymentOptions");
			_alreadyFetchedPaymentOptions = info.GetBoolean("_alreadyFetchedPaymentOptions");

			_paymentProviderAccounts = (VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection)info.GetValue("_paymentProviderAccounts", typeof(VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection));
			_alwaysFetchPaymentProviderAccounts = info.GetBoolean("_alwaysFetchPaymentProviderAccounts");
			_alreadyFetchedPaymentProviderAccounts = info.GetBoolean("_alreadyFetchedPaymentProviderAccounts");

			_photographs = (VarioSL.Entities.CollectionClasses.PhotographCollection)info.GetValue("_photographs", typeof(VarioSL.Entities.CollectionClasses.PhotographCollection));
			_alwaysFetchPhotographs = info.GetBoolean("_alwaysFetchPhotographs");
			_alreadyFetchedPhotographs = info.GetBoolean("_alreadyFetchedPhotographs");

			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");

			_productTerminations = (VarioSL.Entities.CollectionClasses.ProductTerminationCollection)info.GetValue("_productTerminations", typeof(VarioSL.Entities.CollectionClasses.ProductTerminationCollection));
			_alwaysFetchProductTerminations = info.GetBoolean("_alwaysFetchProductTerminations");
			_alreadyFetchedProductTerminations = info.GetBoolean("_alreadyFetchedProductTerminations");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_secondarySales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_secondarySales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSecondarySales = info.GetBoolean("_alwaysFetchSecondarySales");
			_alreadyFetchedSecondarySales = info.GetBoolean("_alreadyFetchedSecondarySales");

			_slStatementofaccounts = (VarioSL.Entities.CollectionClasses.StatementOfAccountCollection)info.GetValue("_slStatementofaccounts", typeof(VarioSL.Entities.CollectionClasses.StatementOfAccountCollection));
			_alwaysFetchSlStatementofaccounts = info.GetBoolean("_alwaysFetchSlStatementofaccounts");
			_alreadyFetchedSlStatementofaccounts = info.GetBoolean("_alreadyFetchedSlStatementofaccounts");

			_ticketAssignments = (VarioSL.Entities.CollectionClasses.TicketAssignmentCollection)info.GetValue("_ticketAssignments", typeof(VarioSL.Entities.CollectionClasses.TicketAssignmentCollection));
			_alwaysFetchTicketAssignments = info.GetBoolean("_alwaysFetchTicketAssignments");
			_alreadyFetchedTicketAssignments = info.GetBoolean("_alreadyFetchedTicketAssignments");
			_addresses = (VarioSL.Entities.CollectionClasses.AddressCollection)info.GetValue("_addresses", typeof(VarioSL.Entities.CollectionClasses.AddressCollection));
			_alwaysFetchAddresses = info.GetBoolean("_alwaysFetchAddresses");
			_alreadyFetchedAddresses = info.GetBoolean("_alreadyFetchedAddresses");

			_cards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCards = info.GetBoolean("_alwaysFetchCards");
			_alreadyFetchedCards = info.GetBoolean("_alreadyFetchedCards");

			_notificationMessages = (VarioSL.Entities.CollectionClasses.NotificationMessageCollection)info.GetValue("_notificationMessages", typeof(VarioSL.Entities.CollectionClasses.NotificationMessageCollection));
			_alwaysFetchNotificationMessages = info.GetBoolean("_alwaysFetchNotificationMessages");
			_alreadyFetchedNotificationMessages = info.GetBoolean("_alreadyFetchedNotificationMessages");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_vanpoolClient = (ClientEntity)info.GetValue("_vanpoolClient", typeof(ClientEntity));
			if(_vanpoolClient!=null)
			{
				_vanpoolClient.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vanpoolClientReturnsNewIfNotFound = info.GetBoolean("_vanpoolClientReturnsNewIfNotFound");
			_alwaysFetchVanpoolClient = info.GetBoolean("_alwaysFetchVanpoolClient");
			_alreadyFetchedVanpoolClient = info.GetBoolean("_alreadyFetchedVanpoolClient");

			_accountingMode = (AccountingModeEntity)info.GetValue("_accountingMode", typeof(AccountingModeEntity));
			if(_accountingMode!=null)
			{
				_accountingMode.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_accountingModeReturnsNewIfNotFound = info.GetBoolean("_accountingModeReturnsNewIfNotFound");
			_alwaysFetchAccountingMode = info.GetBoolean("_alwaysFetchAccountingMode");
			_alreadyFetchedAccountingMode = info.GetBoolean("_alreadyFetchedAccountingMode");

			_organization = (OrganizationEntity)info.GetValue("_organization", typeof(OrganizationEntity));
			if(_organization!=null)
			{
				_organization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_organizationReturnsNewIfNotFound = info.GetBoolean("_organizationReturnsNewIfNotFound");
			_alwaysFetchOrganization = info.GetBoolean("_alwaysFetchOrganization");
			_alreadyFetchedOrganization = info.GetBoolean("_alreadyFetchedOrganization");
			_paymentModality = (PaymentModalityEntity)info.GetValue("_paymentModality", typeof(PaymentModalityEntity));
			if(_paymentModality!=null)
			{
				_paymentModality.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentModalityReturnsNewIfNotFound = info.GetBoolean("_paymentModalityReturnsNewIfNotFound");
			_alwaysFetchPaymentModality = info.GetBoolean("_alwaysFetchPaymentModality");
			_alreadyFetchedPaymentModality = info.GetBoolean("_alreadyFetchedPaymentModality");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ContractFieldIndex)fieldIndex)
			{
				case ContractFieldIndex.AccountingModeID:
					DesetupSyncAccountingMode(true, false);
					_alreadyFetchedAccountingMode = false;
					break;
				case ContractFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ContractFieldIndex.OrganizationID:
					DesetupSyncOrganization(true, false);
					_alreadyFetchedOrganization = false;
					break;
				case ContractFieldIndex.PaymentModalityID:
					DesetupSyncPaymentModality(true, false);
					_alreadyFetchedPaymentModality = false;
					break;
				case ContractFieldIndex.VanpoolClientID:
					DesetupSyncVanpoolClient(true, false);
					_alreadyFetchedVanpoolClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardEvents = (_cardEvents.Count > 0);
			_alreadyFetchedCardHolders = (_cardHolders.Count > 0);
			_alreadyFetchedCardsToContracts = (_cardsToContracts.Count > 0);
			_alreadyFetchedSlComments = (_slComments.Count > 0);
			_alreadyFetchedContractAddresses = (_contractAddresses.Count > 0);
			_alreadyFetchedContractHistoryItems = (_contractHistoryItems.Count > 0);
			_alreadyFetchedTargetContracts = (_targetContracts.Count > 0);
			_alreadyFetchedSourceContracts = (_sourceContracts.Count > 0);
			_alreadyFetchedContractTerminations = (_contractTerminations.Count > 0);
			_alreadyFetchedContractToPaymentMethods = (_contractToPaymentMethods.Count > 0);
			_alreadyFetchedContractToProviders = (_contractToProviders.Count > 0);
			_alreadyFetchedWorkItems = (_workItems.Count > 0);
			_alreadyFetchedCustomerAccounts = (_customerAccounts.Count > 0);
			_alreadyFetchedDunningProcesses = (_dunningProcesses.Count > 0);
			_alreadyFetchedFareEvasionIncidents = (_fareEvasionIncidents.Count > 0);
			_alreadyFetchedInvoices = (_invoices.Count > 0);
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedJobBulkImports = (_jobBulkImports.Count > 0);
			_alreadyFetchedNotificationMessageOwners = (_notificationMessageOwners.Count > 0);
			_alreadyFetchedOrders = (_orders.Count > 0);
			_alreadyFetchedOrganizationParticipants = (_organizationParticipants.Count > 0);
			_alreadyFetchedParticipantGroups = (_participantGroups.Count > 0);
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedPaymentOptions = (_paymentOptions.Count > 0);
			_alreadyFetchedPaymentProviderAccounts = (_paymentProviderAccounts.Count > 0);
			_alreadyFetchedPhotographs = (_photographs.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedProductTerminations = (_productTerminations.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedSecondarySales = (_secondarySales.Count > 0);
			_alreadyFetchedSlStatementofaccounts = (_slStatementofaccounts.Count > 0);
			_alreadyFetchedTicketAssignments = (_ticketAssignments.Count > 0);
			_alreadyFetchedAddresses = (_addresses.Count > 0);
			_alreadyFetchedCards = (_cards.Count > 0);
			_alreadyFetchedNotificationMessages = (_notificationMessages.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedVanpoolClient = (_vanpoolClient != null);
			_alreadyFetchedAccountingMode = (_accountingMode != null);
			_alreadyFetchedOrganization = (_organization != null);
			_alreadyFetchedPaymentModality = (_paymentModality != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "VanpoolClient":
					toReturn.Add(Relations.ClientEntityUsingVanpoolClientID);
					break;
				case "AccountingMode":
					toReturn.Add(Relations.AccountingModeEntityUsingAccountingModeID);
					break;
				case "Organization":
					toReturn.Add(Relations.OrganizationEntityUsingOrganizationID);
					break;
				case "CardEvents":
					toReturn.Add(Relations.CardEventEntityUsingContractID);
					break;
				case "CardHolders":
					toReturn.Add(Relations.CardHolderEntityUsingContractID);
					break;
				case "CardsToContracts":
					toReturn.Add(Relations.CardToContractEntityUsingContractID);
					break;
				case "SlComments":
					toReturn.Add(Relations.CommentEntityUsingContractID);
					break;
				case "ContractAddresses":
					toReturn.Add(Relations.ContractAddressEntityUsingContractID);
					break;
				case "ContractHistoryItems":
					toReturn.Add(Relations.ContractHistoryEntityUsingContractID);
					break;
				case "TargetContracts":
					toReturn.Add(Relations.ContractLinkEntityUsingSourceContractID);
					break;
				case "SourceContracts":
					toReturn.Add(Relations.ContractLinkEntityUsingTargetContractID);
					break;
				case "ContractTerminations":
					toReturn.Add(Relations.ContractTerminationEntityUsingContractID);
					break;
				case "ContractToPaymentMethods":
					toReturn.Add(Relations.ContractToPaymentMethodEntityUsingContractID);
					break;
				case "ContractToProviders":
					toReturn.Add(Relations.ContractToProviderEntityUsingContractID);
					break;
				case "WorkItems":
					toReturn.Add(Relations.ContractWorkItemEntityUsingContractID);
					break;
				case "CustomerAccounts":
					toReturn.Add(Relations.CustomerAccountEntityUsingContractID);
					break;
				case "DunningProcesses":
					toReturn.Add(Relations.DunningProcessEntityUsingContractID);
					break;
				case "FareEvasionIncidents":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingContractID);
					break;
				case "Invoices":
					toReturn.Add(Relations.InvoiceEntityUsingContractID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingContractID);
					break;
				case "JobBulkImports":
					toReturn.Add(Relations.JobBulkImportEntityUsingContractID);
					break;
				case "NotificationMessageOwners":
					toReturn.Add(Relations.NotificationMessageOwnerEntityUsingContractID);
					break;
				case "Orders":
					toReturn.Add(Relations.OrderEntityUsingContractID);
					break;
				case "OrganizationParticipants":
					toReturn.Add(Relations.OrganizationParticipantEntityUsingContractID);
					break;
				case "ParticipantGroups":
					toReturn.Add(Relations.ParticipantGroupEntityUsingContractID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingContractID);
					break;
				case "PaymentOptions":
					toReturn.Add(Relations.PaymentOptionEntityUsingContractID);
					break;
				case "PaymentProviderAccounts":
					toReturn.Add(Relations.PaymentProviderAccountEntityUsingContractID);
					break;
				case "Photographs":
					toReturn.Add(Relations.PhotographEntityUsingContractid);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingContractID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingContractID);
					break;
				case "ProductTerminations":
					toReturn.Add(Relations.ProductTerminationEntityUsingContractID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingContractID);
					break;
				case "SecondarySales":
					toReturn.Add(Relations.SaleEntityUsingSecondaryContractID);
					break;
				case "SlStatementofaccounts":
					toReturn.Add(Relations.StatementOfAccountEntityUsingContractID);
					break;
				case "TicketAssignments":
					toReturn.Add(Relations.TicketAssignmentEntityUsingContractID);
					break;
				case "Addresses":
					toReturn.Add(Relations.ContractAddressEntityUsingContractID, "ContractEntity__", "ContractAddress_", JoinHint.None);
					toReturn.Add(ContractAddressEntity.Relations.AddressEntityUsingAddressID, "ContractAddress_", string.Empty, JoinHint.None);
					break;
				case "Cards":
					toReturn.Add(Relations.CardToContractEntityUsingContractID, "ContractEntity__", "CardToContract_", JoinHint.None);
					toReturn.Add(CardToContractEntity.Relations.CardEntityUsingCardID, "CardToContract_", string.Empty, JoinHint.None);
					break;
				case "NotificationMessages":
					toReturn.Add(Relations.NotificationMessageOwnerEntityUsingContractID, "ContractEntity__", "NotificationMessageOwner_", JoinHint.None);
					toReturn.Add(NotificationMessageOwnerEntity.Relations.NotificationMessageEntityUsingNotificationMessageID, "NotificationMessageOwner_", string.Empty, JoinHint.None);
					break;
				case "PaymentModality":
					toReturn.Add(Relations.PaymentModalityEntityUsingPaymentModalityID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardEvents", (!this.MarkedForDeletion?_cardEvents:null));
			info.AddValue("_alwaysFetchCardEvents", _alwaysFetchCardEvents);
			info.AddValue("_alreadyFetchedCardEvents", _alreadyFetchedCardEvents);
			info.AddValue("_cardHolders", (!this.MarkedForDeletion?_cardHolders:null));
			info.AddValue("_alwaysFetchCardHolders", _alwaysFetchCardHolders);
			info.AddValue("_alreadyFetchedCardHolders", _alreadyFetchedCardHolders);
			info.AddValue("_cardsToContracts", (!this.MarkedForDeletion?_cardsToContracts:null));
			info.AddValue("_alwaysFetchCardsToContracts", _alwaysFetchCardsToContracts);
			info.AddValue("_alreadyFetchedCardsToContracts", _alreadyFetchedCardsToContracts);
			info.AddValue("_slComments", (!this.MarkedForDeletion?_slComments:null));
			info.AddValue("_alwaysFetchSlComments", _alwaysFetchSlComments);
			info.AddValue("_alreadyFetchedSlComments", _alreadyFetchedSlComments);
			info.AddValue("_contractAddresses", (!this.MarkedForDeletion?_contractAddresses:null));
			info.AddValue("_alwaysFetchContractAddresses", _alwaysFetchContractAddresses);
			info.AddValue("_alreadyFetchedContractAddresses", _alreadyFetchedContractAddresses);
			info.AddValue("_contractHistoryItems", (!this.MarkedForDeletion?_contractHistoryItems:null));
			info.AddValue("_alwaysFetchContractHistoryItems", _alwaysFetchContractHistoryItems);
			info.AddValue("_alreadyFetchedContractHistoryItems", _alreadyFetchedContractHistoryItems);
			info.AddValue("_targetContracts", (!this.MarkedForDeletion?_targetContracts:null));
			info.AddValue("_alwaysFetchTargetContracts", _alwaysFetchTargetContracts);
			info.AddValue("_alreadyFetchedTargetContracts", _alreadyFetchedTargetContracts);
			info.AddValue("_sourceContracts", (!this.MarkedForDeletion?_sourceContracts:null));
			info.AddValue("_alwaysFetchSourceContracts", _alwaysFetchSourceContracts);
			info.AddValue("_alreadyFetchedSourceContracts", _alreadyFetchedSourceContracts);
			info.AddValue("_contractTerminations", (!this.MarkedForDeletion?_contractTerminations:null));
			info.AddValue("_alwaysFetchContractTerminations", _alwaysFetchContractTerminations);
			info.AddValue("_alreadyFetchedContractTerminations", _alreadyFetchedContractTerminations);
			info.AddValue("_contractToPaymentMethods", (!this.MarkedForDeletion?_contractToPaymentMethods:null));
			info.AddValue("_alwaysFetchContractToPaymentMethods", _alwaysFetchContractToPaymentMethods);
			info.AddValue("_alreadyFetchedContractToPaymentMethods", _alreadyFetchedContractToPaymentMethods);
			info.AddValue("_contractToProviders", (!this.MarkedForDeletion?_contractToProviders:null));
			info.AddValue("_alwaysFetchContractToProviders", _alwaysFetchContractToProviders);
			info.AddValue("_alreadyFetchedContractToProviders", _alreadyFetchedContractToProviders);
			info.AddValue("_workItems", (!this.MarkedForDeletion?_workItems:null));
			info.AddValue("_alwaysFetchWorkItems", _alwaysFetchWorkItems);
			info.AddValue("_alreadyFetchedWorkItems", _alreadyFetchedWorkItems);
			info.AddValue("_customerAccounts", (!this.MarkedForDeletion?_customerAccounts:null));
			info.AddValue("_alwaysFetchCustomerAccounts", _alwaysFetchCustomerAccounts);
			info.AddValue("_alreadyFetchedCustomerAccounts", _alreadyFetchedCustomerAccounts);
			info.AddValue("_dunningProcesses", (!this.MarkedForDeletion?_dunningProcesses:null));
			info.AddValue("_alwaysFetchDunningProcesses", _alwaysFetchDunningProcesses);
			info.AddValue("_alreadyFetchedDunningProcesses", _alreadyFetchedDunningProcesses);
			info.AddValue("_fareEvasionIncidents", (!this.MarkedForDeletion?_fareEvasionIncidents:null));
			info.AddValue("_alwaysFetchFareEvasionIncidents", _alwaysFetchFareEvasionIncidents);
			info.AddValue("_alreadyFetchedFareEvasionIncidents", _alreadyFetchedFareEvasionIncidents);
			info.AddValue("_invoices", (!this.MarkedForDeletion?_invoices:null));
			info.AddValue("_alwaysFetchInvoices", _alwaysFetchInvoices);
			info.AddValue("_alreadyFetchedInvoices", _alreadyFetchedInvoices);
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_jobBulkImports", (!this.MarkedForDeletion?_jobBulkImports:null));
			info.AddValue("_alwaysFetchJobBulkImports", _alwaysFetchJobBulkImports);
			info.AddValue("_alreadyFetchedJobBulkImports", _alreadyFetchedJobBulkImports);
			info.AddValue("_notificationMessageOwners", (!this.MarkedForDeletion?_notificationMessageOwners:null));
			info.AddValue("_alwaysFetchNotificationMessageOwners", _alwaysFetchNotificationMessageOwners);
			info.AddValue("_alreadyFetchedNotificationMessageOwners", _alreadyFetchedNotificationMessageOwners);
			info.AddValue("_orders", (!this.MarkedForDeletion?_orders:null));
			info.AddValue("_alwaysFetchOrders", _alwaysFetchOrders);
			info.AddValue("_alreadyFetchedOrders", _alreadyFetchedOrders);
			info.AddValue("_organizationParticipants", (!this.MarkedForDeletion?_organizationParticipants:null));
			info.AddValue("_alwaysFetchOrganizationParticipants", _alwaysFetchOrganizationParticipants);
			info.AddValue("_alreadyFetchedOrganizationParticipants", _alreadyFetchedOrganizationParticipants);
			info.AddValue("_participantGroups", (!this.MarkedForDeletion?_participantGroups:null));
			info.AddValue("_alwaysFetchParticipantGroups", _alwaysFetchParticipantGroups);
			info.AddValue("_alreadyFetchedParticipantGroups", _alreadyFetchedParticipantGroups);
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_paymentOptions", (!this.MarkedForDeletion?_paymentOptions:null));
			info.AddValue("_alwaysFetchPaymentOptions", _alwaysFetchPaymentOptions);
			info.AddValue("_alreadyFetchedPaymentOptions", _alreadyFetchedPaymentOptions);
			info.AddValue("_paymentProviderAccounts", (!this.MarkedForDeletion?_paymentProviderAccounts:null));
			info.AddValue("_alwaysFetchPaymentProviderAccounts", _alwaysFetchPaymentProviderAccounts);
			info.AddValue("_alreadyFetchedPaymentProviderAccounts", _alreadyFetchedPaymentProviderAccounts);
			info.AddValue("_photographs", (!this.MarkedForDeletion?_photographs:null));
			info.AddValue("_alwaysFetchPhotographs", _alwaysFetchPhotographs);
			info.AddValue("_alreadyFetchedPhotographs", _alreadyFetchedPhotographs);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_productTerminations", (!this.MarkedForDeletion?_productTerminations:null));
			info.AddValue("_alwaysFetchProductTerminations", _alwaysFetchProductTerminations);
			info.AddValue("_alreadyFetchedProductTerminations", _alreadyFetchedProductTerminations);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_secondarySales", (!this.MarkedForDeletion?_secondarySales:null));
			info.AddValue("_alwaysFetchSecondarySales", _alwaysFetchSecondarySales);
			info.AddValue("_alreadyFetchedSecondarySales", _alreadyFetchedSecondarySales);
			info.AddValue("_slStatementofaccounts", (!this.MarkedForDeletion?_slStatementofaccounts:null));
			info.AddValue("_alwaysFetchSlStatementofaccounts", _alwaysFetchSlStatementofaccounts);
			info.AddValue("_alreadyFetchedSlStatementofaccounts", _alreadyFetchedSlStatementofaccounts);
			info.AddValue("_ticketAssignments", (!this.MarkedForDeletion?_ticketAssignments:null));
			info.AddValue("_alwaysFetchTicketAssignments", _alwaysFetchTicketAssignments);
			info.AddValue("_alreadyFetchedTicketAssignments", _alreadyFetchedTicketAssignments);
			info.AddValue("_addresses", (!this.MarkedForDeletion?_addresses:null));
			info.AddValue("_alwaysFetchAddresses", _alwaysFetchAddresses);
			info.AddValue("_alreadyFetchedAddresses", _alreadyFetchedAddresses);
			info.AddValue("_cards", (!this.MarkedForDeletion?_cards:null));
			info.AddValue("_alwaysFetchCards", _alwaysFetchCards);
			info.AddValue("_alreadyFetchedCards", _alreadyFetchedCards);
			info.AddValue("_notificationMessages", (!this.MarkedForDeletion?_notificationMessages:null));
			info.AddValue("_alwaysFetchNotificationMessages", _alwaysFetchNotificationMessages);
			info.AddValue("_alreadyFetchedNotificationMessages", _alreadyFetchedNotificationMessages);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_vanpoolClient", (!this.MarkedForDeletion?_vanpoolClient:null));
			info.AddValue("_vanpoolClientReturnsNewIfNotFound", _vanpoolClientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVanpoolClient", _alwaysFetchVanpoolClient);
			info.AddValue("_alreadyFetchedVanpoolClient", _alreadyFetchedVanpoolClient);
			info.AddValue("_accountingMode", (!this.MarkedForDeletion?_accountingMode:null));
			info.AddValue("_accountingModeReturnsNewIfNotFound", _accountingModeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAccountingMode", _alwaysFetchAccountingMode);
			info.AddValue("_alreadyFetchedAccountingMode", _alreadyFetchedAccountingMode);
			info.AddValue("_organization", (!this.MarkedForDeletion?_organization:null));
			info.AddValue("_organizationReturnsNewIfNotFound", _organizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrganization", _alwaysFetchOrganization);
			info.AddValue("_alreadyFetchedOrganization", _alreadyFetchedOrganization);

			info.AddValue("_paymentModality", (!this.MarkedForDeletion?_paymentModality:null));
			info.AddValue("_paymentModalityReturnsNewIfNotFound", _paymentModalityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentModality", _alwaysFetchPaymentModality);
			info.AddValue("_alreadyFetchedPaymentModality", _alreadyFetchedPaymentModality);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "VanpoolClient":
					_alreadyFetchedVanpoolClient = true;
					this.VanpoolClient = (ClientEntity)entity;
					break;
				case "AccountingMode":
					_alreadyFetchedAccountingMode = true;
					this.AccountingMode = (AccountingModeEntity)entity;
					break;
				case "Organization":
					_alreadyFetchedOrganization = true;
					this.Organization = (OrganizationEntity)entity;
					break;
				case "CardEvents":
					_alreadyFetchedCardEvents = true;
					if(entity!=null)
					{
						this.CardEvents.Add((CardEventEntity)entity);
					}
					break;
				case "CardHolders":
					_alreadyFetchedCardHolders = true;
					if(entity!=null)
					{
						this.CardHolders.Add((CardHolderEntity)entity);
					}
					break;
				case "CardsToContracts":
					_alreadyFetchedCardsToContracts = true;
					if(entity!=null)
					{
						this.CardsToContracts.Add((CardToContractEntity)entity);
					}
					break;
				case "SlComments":
					_alreadyFetchedSlComments = true;
					if(entity!=null)
					{
						this.SlComments.Add((CommentEntity)entity);
					}
					break;
				case "ContractAddresses":
					_alreadyFetchedContractAddresses = true;
					if(entity!=null)
					{
						this.ContractAddresses.Add((ContractAddressEntity)entity);
					}
					break;
				case "ContractHistoryItems":
					_alreadyFetchedContractHistoryItems = true;
					if(entity!=null)
					{
						this.ContractHistoryItems.Add((ContractHistoryEntity)entity);
					}
					break;
				case "TargetContracts":
					_alreadyFetchedTargetContracts = true;
					if(entity!=null)
					{
						this.TargetContracts.Add((ContractLinkEntity)entity);
					}
					break;
				case "SourceContracts":
					_alreadyFetchedSourceContracts = true;
					if(entity!=null)
					{
						this.SourceContracts.Add((ContractLinkEntity)entity);
					}
					break;
				case "ContractTerminations":
					_alreadyFetchedContractTerminations = true;
					if(entity!=null)
					{
						this.ContractTerminations.Add((ContractTerminationEntity)entity);
					}
					break;
				case "ContractToPaymentMethods":
					_alreadyFetchedContractToPaymentMethods = true;
					if(entity!=null)
					{
						this.ContractToPaymentMethods.Add((ContractToPaymentMethodEntity)entity);
					}
					break;
				case "ContractToProviders":
					_alreadyFetchedContractToProviders = true;
					if(entity!=null)
					{
						this.ContractToProviders.Add((ContractToProviderEntity)entity);
					}
					break;
				case "WorkItems":
					_alreadyFetchedWorkItems = true;
					if(entity!=null)
					{
						this.WorkItems.Add((ContractWorkItemEntity)entity);
					}
					break;
				case "CustomerAccounts":
					_alreadyFetchedCustomerAccounts = true;
					if(entity!=null)
					{
						this.CustomerAccounts.Add((CustomerAccountEntity)entity);
					}
					break;
				case "DunningProcesses":
					_alreadyFetchedDunningProcesses = true;
					if(entity!=null)
					{
						this.DunningProcesses.Add((DunningProcessEntity)entity);
					}
					break;
				case "FareEvasionIncidents":
					_alreadyFetchedFareEvasionIncidents = true;
					if(entity!=null)
					{
						this.FareEvasionIncidents.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "Invoices":
					_alreadyFetchedInvoices = true;
					if(entity!=null)
					{
						this.Invoices.Add((InvoiceEntity)entity);
					}
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				case "JobBulkImports":
					_alreadyFetchedJobBulkImports = true;
					if(entity!=null)
					{
						this.JobBulkImports.Add((JobBulkImportEntity)entity);
					}
					break;
				case "NotificationMessageOwners":
					_alreadyFetchedNotificationMessageOwners = true;
					if(entity!=null)
					{
						this.NotificationMessageOwners.Add((NotificationMessageOwnerEntity)entity);
					}
					break;
				case "Orders":
					_alreadyFetchedOrders = true;
					if(entity!=null)
					{
						this.Orders.Add((OrderEntity)entity);
					}
					break;
				case "OrganizationParticipants":
					_alreadyFetchedOrganizationParticipants = true;
					if(entity!=null)
					{
						this.OrganizationParticipants.Add((OrganizationParticipantEntity)entity);
					}
					break;
				case "ParticipantGroups":
					_alreadyFetchedParticipantGroups = true;
					if(entity!=null)
					{
						this.ParticipantGroups.Add((ParticipantGroupEntity)entity);
					}
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "PaymentOptions":
					_alreadyFetchedPaymentOptions = true;
					if(entity!=null)
					{
						this.PaymentOptions.Add((PaymentOptionEntity)entity);
					}
					break;
				case "PaymentProviderAccounts":
					_alreadyFetchedPaymentProviderAccounts = true;
					if(entity!=null)
					{
						this.PaymentProviderAccounts.Add((PaymentProviderAccountEntity)entity);
					}
					break;
				case "Photographs":
					_alreadyFetchedPhotographs = true;
					if(entity!=null)
					{
						this.Photographs.Add((PhotographEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				case "ProductTerminations":
					_alreadyFetchedProductTerminations = true;
					if(entity!=null)
					{
						this.ProductTerminations.Add((ProductTerminationEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "SecondarySales":
					_alreadyFetchedSecondarySales = true;
					if(entity!=null)
					{
						this.SecondarySales.Add((SaleEntity)entity);
					}
					break;
				case "SlStatementofaccounts":
					_alreadyFetchedSlStatementofaccounts = true;
					if(entity!=null)
					{
						this.SlStatementofaccounts.Add((StatementOfAccountEntity)entity);
					}
					break;
				case "TicketAssignments":
					_alreadyFetchedTicketAssignments = true;
					if(entity!=null)
					{
						this.TicketAssignments.Add((TicketAssignmentEntity)entity);
					}
					break;
				case "Addresses":
					_alreadyFetchedAddresses = true;
					if(entity!=null)
					{
						this.Addresses.Add((AddressEntity)entity);
					}
					break;
				case "Cards":
					_alreadyFetchedCards = true;
					if(entity!=null)
					{
						this.Cards.Add((CardEntity)entity);
					}
					break;
				case "NotificationMessages":
					_alreadyFetchedNotificationMessages = true;
					if(entity!=null)
					{
						this.NotificationMessages.Add((NotificationMessageEntity)entity);
					}
					break;
				case "PaymentModality":
					_alreadyFetchedPaymentModality = true;
					this.PaymentModality = (PaymentModalityEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "VanpoolClient":
					SetupSyncVanpoolClient(relatedEntity);
					break;
				case "AccountingMode":
					SetupSyncAccountingMode(relatedEntity);
					break;
				case "Organization":
					SetupSyncOrganization(relatedEntity);
					break;
				case "CardEvents":
					_cardEvents.Add((CardEventEntity)relatedEntity);
					break;
				case "CardHolders":
					_cardHolders.Add((CardHolderEntity)relatedEntity);
					break;
				case "CardsToContracts":
					_cardsToContracts.Add((CardToContractEntity)relatedEntity);
					break;
				case "SlComments":
					_slComments.Add((CommentEntity)relatedEntity);
					break;
				case "ContractAddresses":
					_contractAddresses.Add((ContractAddressEntity)relatedEntity);
					break;
				case "ContractHistoryItems":
					_contractHistoryItems.Add((ContractHistoryEntity)relatedEntity);
					break;
				case "TargetContracts":
					_targetContracts.Add((ContractLinkEntity)relatedEntity);
					break;
				case "SourceContracts":
					_sourceContracts.Add((ContractLinkEntity)relatedEntity);
					break;
				case "ContractTerminations":
					_contractTerminations.Add((ContractTerminationEntity)relatedEntity);
					break;
				case "ContractToPaymentMethods":
					_contractToPaymentMethods.Add((ContractToPaymentMethodEntity)relatedEntity);
					break;
				case "ContractToProviders":
					_contractToProviders.Add((ContractToProviderEntity)relatedEntity);
					break;
				case "WorkItems":
					_workItems.Add((ContractWorkItemEntity)relatedEntity);
					break;
				case "CustomerAccounts":
					_customerAccounts.Add((CustomerAccountEntity)relatedEntity);
					break;
				case "DunningProcesses":
					_dunningProcesses.Add((DunningProcessEntity)relatedEntity);
					break;
				case "FareEvasionIncidents":
					_fareEvasionIncidents.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "Invoices":
					_invoices.Add((InvoiceEntity)relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				case "JobBulkImports":
					_jobBulkImports.Add((JobBulkImportEntity)relatedEntity);
					break;
				case "NotificationMessageOwners":
					_notificationMessageOwners.Add((NotificationMessageOwnerEntity)relatedEntity);
					break;
				case "Orders":
					_orders.Add((OrderEntity)relatedEntity);
					break;
				case "OrganizationParticipants":
					_organizationParticipants.Add((OrganizationParticipantEntity)relatedEntity);
					break;
				case "ParticipantGroups":
					_participantGroups.Add((ParticipantGroupEntity)relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "PaymentOptions":
					_paymentOptions.Add((PaymentOptionEntity)relatedEntity);
					break;
				case "PaymentProviderAccounts":
					_paymentProviderAccounts.Add((PaymentProviderAccountEntity)relatedEntity);
					break;
				case "Photographs":
					_photographs.Add((PhotographEntity)relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				case "ProductTerminations":
					_productTerminations.Add((ProductTerminationEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "SecondarySales":
					_secondarySales.Add((SaleEntity)relatedEntity);
					break;
				case "SlStatementofaccounts":
					_slStatementofaccounts.Add((StatementOfAccountEntity)relatedEntity);
					break;
				case "TicketAssignments":
					_ticketAssignments.Add((TicketAssignmentEntity)relatedEntity);
					break;
				case "PaymentModality":
					SetupSyncPaymentModality(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "VanpoolClient":
					DesetupSyncVanpoolClient(false, true);
					break;
				case "AccountingMode":
					DesetupSyncAccountingMode(false, true);
					break;
				case "Organization":
					DesetupSyncOrganization(false, true);
					break;
				case "CardEvents":
					this.PerformRelatedEntityRemoval(_cardEvents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardHolders":
					this.PerformRelatedEntityRemoval(_cardHolders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardsToContracts":
					this.PerformRelatedEntityRemoval(_cardsToContracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SlComments":
					this.PerformRelatedEntityRemoval(_slComments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractAddresses":
					this.PerformRelatedEntityRemoval(_contractAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractHistoryItems":
					this.PerformRelatedEntityRemoval(_contractHistoryItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TargetContracts":
					this.PerformRelatedEntityRemoval(_targetContracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SourceContracts":
					this.PerformRelatedEntityRemoval(_sourceContracts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractTerminations":
					this.PerformRelatedEntityRemoval(_contractTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractToPaymentMethods":
					this.PerformRelatedEntityRemoval(_contractToPaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContractToProviders":
					this.PerformRelatedEntityRemoval(_contractToProviders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "WorkItems":
					this.PerformRelatedEntityRemoval(_workItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomerAccounts":
					this.PerformRelatedEntityRemoval(_customerAccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningProcesses":
					this.PerformRelatedEntityRemoval(_dunningProcesses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidents":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Invoices":
					this.PerformRelatedEntityRemoval(_invoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "JobBulkImports":
					this.PerformRelatedEntityRemoval(_jobBulkImports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "NotificationMessageOwners":
					this.PerformRelatedEntityRemoval(_notificationMessageOwners, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Orders":
					this.PerformRelatedEntityRemoval(_orders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrganizationParticipants":
					this.PerformRelatedEntityRemoval(_organizationParticipants, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParticipantGroups":
					this.PerformRelatedEntityRemoval(_participantGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentOptions":
					this.PerformRelatedEntityRemoval(_paymentOptions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentProviderAccounts":
					this.PerformRelatedEntityRemoval(_paymentProviderAccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Photographs":
					this.PerformRelatedEntityRemoval(_photographs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductTerminations":
					this.PerformRelatedEntityRemoval(_productTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SecondarySales":
					this.PerformRelatedEntityRemoval(_secondarySales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SlStatementofaccounts":
					this.PerformRelatedEntityRemoval(_slStatementofaccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketAssignments":
					this.PerformRelatedEntityRemoval(_ticketAssignments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentModality":
					DesetupSyncPaymentModality(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_vanpoolClient!=null)
			{
				toReturn.Add(_vanpoolClient);
			}
			if(_accountingMode!=null)
			{
				toReturn.Add(_accountingMode);
			}
			if(_organization!=null)
			{
				toReturn.Add(_organization);
			}
			if(_paymentModality!=null)
			{
				toReturn.Add(_paymentModality);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardEvents);
			toReturn.Add(_cardHolders);
			toReturn.Add(_cardsToContracts);
			toReturn.Add(_slComments);
			toReturn.Add(_contractAddresses);
			toReturn.Add(_contractHistoryItems);
			toReturn.Add(_targetContracts);
			toReturn.Add(_sourceContracts);
			toReturn.Add(_contractTerminations);
			toReturn.Add(_contractToPaymentMethods);
			toReturn.Add(_contractToProviders);
			toReturn.Add(_workItems);
			toReturn.Add(_customerAccounts);
			toReturn.Add(_dunningProcesses);
			toReturn.Add(_fareEvasionIncidents);
			toReturn.Add(_invoices);
			toReturn.Add(_jobs);
			toReturn.Add(_jobBulkImports);
			toReturn.Add(_notificationMessageOwners);
			toReturn.Add(_orders);
			toReturn.Add(_organizationParticipants);
			toReturn.Add(_participantGroups);
			toReturn.Add(_paymentJournals);
			toReturn.Add(_paymentOptions);
			toReturn.Add(_paymentProviderAccounts);
			toReturn.Add(_photographs);
			toReturn.Add(_postings);
			toReturn.Add(_products);
			toReturn.Add(_productTerminations);
			toReturn.Add(_sales);
			toReturn.Add(_secondarySales);
			toReturn.Add(_slStatementofaccounts);
			toReturn.Add(_ticketAssignments);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentModalityID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentModalityID(Nullable<System.Int64> paymentModalityID)
		{
			return FetchUsingUCPaymentModalityID( paymentModalityID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentModalityID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentModalityID(Nullable<System.Int64> paymentModalityID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPaymentModalityID( paymentModalityID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentModalityID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentModalityID(Nullable<System.Int64> paymentModalityID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPaymentModalityID( paymentModalityID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentModalityID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentModalityID(Nullable<System.Int64> paymentModalityID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ContractDAO)CreateDAOInstance()).FetchContractUsingUCPaymentModalityID(this, this.Transaction, paymentModalityID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID)
		{
			return FetchUsingPK(contractID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(contractID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(contractID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 contractID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(contractID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ContractID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ContractRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEventEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch)
		{
			return GetMultiCardEvents(forceFetch, _cardEvents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEventEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardEvents(forceFetch, _cardEvents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardEvents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardEventCollection GetMultiCardEvents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardEvents || forceFetch || _alwaysFetchCardEvents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardEvents);
				_cardEvents.SuppressClearInGetMulti=!forceFetch;
				_cardEvents.EntityFactoryToUse = entityFactoryToUse;
				_cardEvents.GetMultiManyToOne(null, this, filter);
				_cardEvents.SuppressClearInGetMulti=false;
				_alreadyFetchedCardEvents = true;
			}
			return _cardEvents;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardEvents'. These settings will be taken into account
		/// when the property CardEvents is requested or GetMultiCardEvents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardEvents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardEvents.SortClauses=sortClauses;
			_cardEvents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch)
		{
			return GetMultiCardHolders(forceFetch, _cardHolders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardHolders(forceFetch, _cardHolders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardHolders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardHolders || forceFetch || _alwaysFetchCardHolders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardHolders);
				_cardHolders.SuppressClearInGetMulti=!forceFetch;
				_cardHolders.EntityFactoryToUse = entityFactoryToUse;
				_cardHolders.GetMultiManyToOne(this, null, filter);
				_cardHolders.SuppressClearInGetMulti=false;
				_alreadyFetchedCardHolders = true;
			}
			return _cardHolders;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardHolders'. These settings will be taken into account
		/// when the property CardHolders is requested or GetMultiCardHolders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardHolders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardHolders.SortClauses=sortClauses;
			_cardHolders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardToContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch)
		{
			return GetMultiCardsToContracts(forceFetch, _cardsToContracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardToContractEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardsToContracts(forceFetch, _cardsToContracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardsToContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardToContractCollection GetMultiCardsToContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardsToContracts || forceFetch || _alwaysFetchCardsToContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardsToContracts);
				_cardsToContracts.SuppressClearInGetMulti=!forceFetch;
				_cardsToContracts.EntityFactoryToUse = entityFactoryToUse;
				_cardsToContracts.GetMultiManyToOne(null, this, filter);
				_cardsToContracts.SuppressClearInGetMulti=false;
				_alreadyFetchedCardsToContracts = true;
			}
			return _cardsToContracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardsToContracts'. These settings will be taken into account
		/// when the property CardsToContracts is requested or GetMultiCardsToContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardsToContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardsToContracts.SortClauses=sortClauses;
			_cardsToContracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CommentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CommentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CommentCollection GetMultiSlComments(bool forceFetch)
		{
			return GetMultiSlComments(forceFetch, _slComments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CommentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CommentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CommentCollection GetMultiSlComments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSlComments(forceFetch, _slComments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CommentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CommentCollection GetMultiSlComments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSlComments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CommentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CommentCollection GetMultiSlComments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSlComments || forceFetch || _alwaysFetchSlComments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_slComments);
				_slComments.SuppressClearInGetMulti=!forceFetch;
				_slComments.EntityFactoryToUse = entityFactoryToUse;
				_slComments.GetMultiManyToOne(null, this, filter);
				_slComments.SuppressClearInGetMulti=false;
				_alreadyFetchedSlComments = true;
			}
			return _slComments;
		}

		/// <summary> Sets the collection parameters for the collection for 'SlComments'. These settings will be taken into account
		/// when the property SlComments is requested or GetMultiSlComments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSlComments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_slComments.SortClauses=sortClauses;
			_slComments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch)
		{
			return GetMultiContractAddresses(forceFetch, _contractAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractAddresses(forceFetch, _contractAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractAddressCollection GetMultiContractAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractAddresses || forceFetch || _alwaysFetchContractAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractAddresses);
				_contractAddresses.SuppressClearInGetMulti=!forceFetch;
				_contractAddresses.EntityFactoryToUse = entityFactoryToUse;
				_contractAddresses.GetMultiManyToOne(null, this, filter);
				_contractAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedContractAddresses = true;
			}
			return _contractAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractAddresses'. These settings will be taken into account
		/// when the property ContractAddresses is requested or GetMultiContractAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractAddresses.SortClauses=sortClauses;
			_contractAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractHistoryCollection GetMultiContractHistoryItems(bool forceFetch)
		{
			return GetMultiContractHistoryItems(forceFetch, _contractHistoryItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractHistoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractHistoryCollection GetMultiContractHistoryItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractHistoryItems(forceFetch, _contractHistoryItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractHistoryCollection GetMultiContractHistoryItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractHistoryItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractHistoryCollection GetMultiContractHistoryItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractHistoryItems || forceFetch || _alwaysFetchContractHistoryItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractHistoryItems);
				_contractHistoryItems.SuppressClearInGetMulti=!forceFetch;
				_contractHistoryItems.EntityFactoryToUse = entityFactoryToUse;
				_contractHistoryItems.GetMultiManyToOne(this, filter);
				_contractHistoryItems.SuppressClearInGetMulti=false;
				_alreadyFetchedContractHistoryItems = true;
			}
			return _contractHistoryItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractHistoryItems'. These settings will be taken into account
		/// when the property ContractHistoryItems is requested or GetMultiContractHistoryItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractHistoryItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractHistoryItems.SortClauses=sortClauses;
			_contractHistoryItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiTargetContracts(bool forceFetch)
		{
			return GetMultiTargetContracts(forceFetch, _targetContracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiTargetContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTargetContracts(forceFetch, _targetContracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiTargetContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTargetContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiTargetContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTargetContracts || forceFetch || _alwaysFetchTargetContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_targetContracts);
				_targetContracts.SuppressClearInGetMulti=!forceFetch;
				_targetContracts.EntityFactoryToUse = entityFactoryToUse;
				_targetContracts.GetMultiManyToOne(this, null, filter);
				_targetContracts.SuppressClearInGetMulti=false;
				_alreadyFetchedTargetContracts = true;
			}
			return _targetContracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'TargetContracts'. These settings will be taken into account
		/// when the property TargetContracts is requested or GetMultiTargetContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTargetContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_targetContracts.SortClauses=sortClauses;
			_targetContracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiSourceContracts(bool forceFetch)
		{
			return GetMultiSourceContracts(forceFetch, _sourceContracts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractLinkEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiSourceContracts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSourceContracts(forceFetch, _sourceContracts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiSourceContracts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSourceContracts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractLinkCollection GetMultiSourceContracts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSourceContracts || forceFetch || _alwaysFetchSourceContracts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sourceContracts);
				_sourceContracts.SuppressClearInGetMulti=!forceFetch;
				_sourceContracts.EntityFactoryToUse = entityFactoryToUse;
				_sourceContracts.GetMultiManyToOne(null, this, filter);
				_sourceContracts.SuppressClearInGetMulti=false;
				_alreadyFetchedSourceContracts = true;
			}
			return _sourceContracts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SourceContracts'. These settings will be taken into account
		/// when the property SourceContracts is requested or GetMultiSourceContracts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSourceContracts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sourceContracts.SortClauses=sortClauses;
			_sourceContracts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch)
		{
			return GetMultiContractTerminations(forceFetch, _contractTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractTerminations(forceFetch, _contractTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractTerminationCollection GetMultiContractTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractTerminations || forceFetch || _alwaysFetchContractTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractTerminations);
				_contractTerminations.SuppressClearInGetMulti=!forceFetch;
				_contractTerminations.EntityFactoryToUse = entityFactoryToUse;
				_contractTerminations.GetMultiManyToOne(null, this, null, filter);
				_contractTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedContractTerminations = true;
			}
			return _contractTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractTerminations'. These settings will be taken into account
		/// when the property ContractTerminations is requested or GetMultiContractTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractTerminations.SortClauses=sortClauses;
			_contractTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch)
		{
			return GetMultiContractToPaymentMethods(forceFetch, _contractToPaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractToPaymentMethods(forceFetch, _contractToPaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractToPaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection GetMultiContractToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractToPaymentMethods || forceFetch || _alwaysFetchContractToPaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractToPaymentMethods);
				_contractToPaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_contractToPaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_contractToPaymentMethods.GetMultiManyToOne(null, this, filter);
				_contractToPaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedContractToPaymentMethods = true;
			}
			return _contractToPaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractToPaymentMethods'. These settings will be taken into account
		/// when the property ContractToPaymentMethods is requested or GetMultiContractToPaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractToPaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractToPaymentMethods.SortClauses=sortClauses;
			_contractToPaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractToProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch)
		{
			return GetMultiContractToProviders(forceFetch, _contractToProviders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractToProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContractToProviders(forceFetch, _contractToProviders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContractToProviders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractToProviderCollection GetMultiContractToProviders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContractToProviders || forceFetch || _alwaysFetchContractToProviders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contractToProviders);
				_contractToProviders.SuppressClearInGetMulti=!forceFetch;
				_contractToProviders.EntityFactoryToUse = entityFactoryToUse;
				_contractToProviders.GetMultiManyToOne(this, null, filter);
				_contractToProviders.SuppressClearInGetMulti=false;
				_alreadyFetchedContractToProviders = true;
			}
			return _contractToProviders;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContractToProviders'. These settings will be taken into account
		/// when the property ContractToProviders is requested or GetMultiContractToProviders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContractToProviders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contractToProviders.SortClauses=sortClauses;
			_contractToProviders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ContractWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ContractWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractWorkItemCollection GetMultiWorkItems(bool forceFetch)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ContractWorkItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ContractWorkItemCollection GetMultiWorkItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiWorkItems(forceFetch, _workItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ContractWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ContractWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiWorkItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ContractWorkItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ContractWorkItemCollection GetMultiWorkItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedWorkItems || forceFetch || _alwaysFetchWorkItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_workItems);
				_workItems.SuppressClearInGetMulti=!forceFetch;
				_workItems.EntityFactoryToUse = entityFactoryToUse;
				_workItems.GetMultiManyToOne(null, this, null, filter);
				_workItems.SuppressClearInGetMulti=false;
				_alreadyFetchedWorkItems = true;
			}
			return _workItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'WorkItems'. These settings will be taken into account
		/// when the property WorkItems is requested or GetMultiWorkItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersWorkItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_workItems.SortClauses=sortClauses;
			_workItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch)
		{
			return GetMultiCustomerAccounts(forceFetch, _customerAccounts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccounts(forceFetch, _customerAccounts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccounts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccounts || forceFetch || _alwaysFetchCustomerAccounts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccounts);
				_customerAccounts.SuppressClearInGetMulti=!forceFetch;
				_customerAccounts.EntityFactoryToUse = entityFactoryToUse;
				_customerAccounts.GetMultiManyToOne(this, null, null, null, filter);
				_customerAccounts.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccounts = true;
			}
			return _customerAccounts;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccounts'. These settings will be taken into account
		/// when the property CustomerAccounts is requested or GetMultiCustomerAccounts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccounts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccounts.SortClauses=sortClauses;
			_customerAccounts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningProcessEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch)
		{
			return GetMultiDunningProcesses(forceFetch, _dunningProcesses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningProcessEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningProcesses(forceFetch, _dunningProcesses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningProcesses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningProcesses || forceFetch || _alwaysFetchDunningProcesses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningProcesses);
				_dunningProcesses.SuppressClearInGetMulti=!forceFetch;
				_dunningProcesses.EntityFactoryToUse = entityFactoryToUse;
				_dunningProcesses.GetMultiManyToOne(this, filter);
				_dunningProcesses.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningProcesses = true;
			}
			return _dunningProcesses;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningProcesses'. These settings will be taken into account
		/// when the property DunningProcesses is requested or GetMultiDunningProcesses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningProcesses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningProcesses.SortClauses=sortClauses;
			_dunningProcesses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidents || forceFetch || _alwaysFetchFareEvasionIncidents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidents);
				_fareEvasionIncidents.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidents.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidents.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_fareEvasionIncidents.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidents = true;
			}
			return _fareEvasionIncidents;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidents'. These settings will be taken into account
		/// when the property FareEvasionIncidents is requested or GetMultiFareEvasionIncidents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidents.SortClauses=sortClauses;
			_fareEvasionIncidents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoices(forceFetch, _invoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection GetMultiInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoices || forceFetch || _alwaysFetchInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoices);
				_invoices.SuppressClearInGetMulti=!forceFetch;
				_invoices.EntityFactoryToUse = entityFactoryToUse;
				_invoices.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_invoices.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoices = true;
			}
			return _invoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Invoices'. These settings will be taken into account
		/// when the property Invoices is requested or GetMultiInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoices.SortClauses=sortClauses;
			_invoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobBulkImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobBulkImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobBulkImportCollection GetMultiJobBulkImports(bool forceFetch)
		{
			return GetMultiJobBulkImports(forceFetch, _jobBulkImports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobBulkImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobBulkImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobBulkImportCollection GetMultiJobBulkImports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobBulkImports(forceFetch, _jobBulkImports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobBulkImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobBulkImportCollection GetMultiJobBulkImports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobBulkImports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobBulkImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobBulkImportCollection GetMultiJobBulkImports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobBulkImports || forceFetch || _alwaysFetchJobBulkImports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobBulkImports);
				_jobBulkImports.SuppressClearInGetMulti=!forceFetch;
				_jobBulkImports.EntityFactoryToUse = entityFactoryToUse;
				_jobBulkImports.GetMultiManyToOne(this, filter);
				_jobBulkImports.SuppressClearInGetMulti=false;
				_alreadyFetchedJobBulkImports = true;
			}
			return _jobBulkImports;
		}

		/// <summary> Sets the collection parameters for the collection for 'JobBulkImports'. These settings will be taken into account
		/// when the property JobBulkImports is requested or GetMultiJobBulkImports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobBulkImports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobBulkImports.SortClauses=sortClauses;
			_jobBulkImports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageOwnerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch)
		{
			return GetMultiNotificationMessageOwners(forceFetch, _notificationMessageOwners.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageOwnerEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiNotificationMessageOwners(forceFetch, _notificationMessageOwners.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiNotificationMessageOwners(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection GetMultiNotificationMessageOwners(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedNotificationMessageOwners || forceFetch || _alwaysFetchNotificationMessageOwners) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessageOwners);
				_notificationMessageOwners.SuppressClearInGetMulti=!forceFetch;
				_notificationMessageOwners.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessageOwners.GetMultiManyToOne(this, null, filter);
				_notificationMessageOwners.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessageOwners = true;
			}
			return _notificationMessageOwners;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessageOwners'. These settings will be taken into account
		/// when the property NotificationMessageOwners is requested or GetMultiNotificationMessageOwners is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessageOwners(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessageOwners.SortClauses=sortClauses;
			_notificationMessageOwners.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrders(forceFetch, _orders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection GetMultiOrders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrders || forceFetch || _alwaysFetchOrders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orders);
				_orders.SuppressClearInGetMulti=!forceFetch;
				_orders.EntityFactoryToUse = entityFactoryToUse;
				_orders.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_orders.SuppressClearInGetMulti=false;
				_alreadyFetchedOrders = true;
			}
			return _orders;
		}

		/// <summary> Sets the collection parameters for the collection for 'Orders'. These settings will be taken into account
		/// when the property Orders is requested or GetMultiOrders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orders.SortClauses=sortClauses;
			_orders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrganizationParticipantEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrganizationParticipants(forceFetch, _organizationParticipants.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrganizationParticipants(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection GetMultiOrganizationParticipants(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrganizationParticipants || forceFetch || _alwaysFetchOrganizationParticipants) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_organizationParticipants);
				_organizationParticipants.SuppressClearInGetMulti=!forceFetch;
				_organizationParticipants.EntityFactoryToUse = entityFactoryToUse;
				_organizationParticipants.GetMultiManyToOne(null, this, null, null, filter);
				_organizationParticipants.SuppressClearInGetMulti=false;
				_alreadyFetchedOrganizationParticipants = true;
			}
			return _organizationParticipants;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrganizationParticipants'. These settings will be taken into account
		/// when the property OrganizationParticipants is requested or GetMultiOrganizationParticipants is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrganizationParticipants(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_organizationParticipants.SortClauses=sortClauses;
			_organizationParticipants.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParticipantGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParticipantGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParticipantGroupCollection GetMultiParticipantGroups(bool forceFetch)
		{
			return GetMultiParticipantGroups(forceFetch, _participantGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParticipantGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParticipantGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParticipantGroupCollection GetMultiParticipantGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParticipantGroups(forceFetch, _participantGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParticipantGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParticipantGroupCollection GetMultiParticipantGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParticipantGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParticipantGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParticipantGroupCollection GetMultiParticipantGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParticipantGroups || forceFetch || _alwaysFetchParticipantGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_participantGroups);
				_participantGroups.SuppressClearInGetMulti=!forceFetch;
				_participantGroups.EntityFactoryToUse = entityFactoryToUse;
				_participantGroups.GetMultiManyToOne(this, filter);
				_participantGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedParticipantGroups = true;
			}
			return _participantGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParticipantGroups'. These settings will be taken into account
		/// when the property ParticipantGroups is requested or GetMultiParticipantGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParticipantGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_participantGroups.SortClauses=sortClauses;
			_participantGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(null, this, null, null, null, null, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch)
		{
			return GetMultiPaymentOptions(forceFetch, _paymentOptions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentOptionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentOptions(forceFetch, _paymentOptions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentOptions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentOptionCollection GetMultiPaymentOptions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentOptions || forceFetch || _alwaysFetchPaymentOptions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentOptions);
				_paymentOptions.SuppressClearInGetMulti=!forceFetch;
				_paymentOptions.EntityFactoryToUse = entityFactoryToUse;
				_paymentOptions.GetMultiManyToOne(null, this, filter);
				_paymentOptions.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentOptions = true;
			}
			return _paymentOptions;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentOptions'. These settings will be taken into account
		/// when the property PaymentOptions is requested or GetMultiPaymentOptions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentOptions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentOptions.SortClauses=sortClauses;
			_paymentOptions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentProviderAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection GetMultiPaymentProviderAccounts(bool forceFetch)
		{
			return GetMultiPaymentProviderAccounts(forceFetch, _paymentProviderAccounts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentProviderAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection GetMultiPaymentProviderAccounts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentProviderAccounts(forceFetch, _paymentProviderAccounts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection GetMultiPaymentProviderAccounts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentProviderAccounts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentProviderAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection GetMultiPaymentProviderAccounts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentProviderAccounts || forceFetch || _alwaysFetchPaymentProviderAccounts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentProviderAccounts);
				_paymentProviderAccounts.SuppressClearInGetMulti=!forceFetch;
				_paymentProviderAccounts.EntityFactoryToUse = entityFactoryToUse;
				_paymentProviderAccounts.GetMultiManyToOne(this, filter);
				_paymentProviderAccounts.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentProviderAccounts = true;
			}
			return _paymentProviderAccounts;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentProviderAccounts'. These settings will be taken into account
		/// when the property PaymentProviderAccounts is requested or GetMultiPaymentProviderAccounts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentProviderAccounts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentProviderAccounts.SortClauses=sortClauses;
			_paymentProviderAccounts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PhotographEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch)
		{
			return GetMultiPhotographs(forceFetch, _photographs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PhotographEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPhotographs(forceFetch, _photographs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPhotographs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPhotographs || forceFetch || _alwaysFetchPhotographs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_photographs);
				_photographs.SuppressClearInGetMulti=!forceFetch;
				_photographs.EntityFactoryToUse = entityFactoryToUse;
				_photographs.GetMultiManyToOne(this, null, filter);
				_photographs.SuppressClearInGetMulti=false;
				_alreadyFetchedPhotographs = true;
			}
			return _photographs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Photographs'. These settings will be taken into account
		/// when the property Photographs is requested or GetMultiPhotographs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPhotographs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_photographs.SortClauses=sortClauses;
			_photographs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductTerminations || forceFetch || _alwaysFetchProductTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productTerminations);
				_productTerminations.SuppressClearInGetMulti=!forceFetch;
				_productTerminations.EntityFactoryToUse = entityFactoryToUse;
				_productTerminations.GetMultiManyToOne(null, this, null, null, null, filter);
				_productTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedProductTerminations = true;
			}
			return _productTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductTerminations'. These settings will be taken into account
		/// when the property ProductTerminations is requested or GetMultiProductTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productTerminations.SortClauses=sortClauses;
			_productTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSecondarySales(bool forceFetch)
		{
			return GetMultiSecondarySales(forceFetch, _secondarySales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSecondarySales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSecondarySales(forceFetch, _secondarySales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSecondarySales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSecondarySales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSecondarySales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSecondarySales || forceFetch || _alwaysFetchSecondarySales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_secondarySales);
				_secondarySales.SuppressClearInGetMulti=!forceFetch;
				_secondarySales.EntityFactoryToUse = entityFactoryToUse;
				_secondarySales.GetMultiManyToOne(null, null, null, this, null, null, null, null, filter);
				_secondarySales.SuppressClearInGetMulti=false;
				_alreadyFetchedSecondarySales = true;
			}
			return _secondarySales;
		}

		/// <summary> Sets the collection parameters for the collection for 'SecondarySales'. These settings will be taken into account
		/// when the property SecondarySales is requested or GetMultiSecondarySales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSecondarySales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_secondarySales.SortClauses=sortClauses;
			_secondarySales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StatementOfAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch)
		{
			return GetMultiSlStatementofaccounts(forceFetch, _slStatementofaccounts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StatementOfAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSlStatementofaccounts(forceFetch, _slStatementofaccounts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSlStatementofaccounts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSlStatementofaccounts || forceFetch || _alwaysFetchSlStatementofaccounts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_slStatementofaccounts);
				_slStatementofaccounts.SuppressClearInGetMulti=!forceFetch;
				_slStatementofaccounts.EntityFactoryToUse = entityFactoryToUse;
				_slStatementofaccounts.GetMultiManyToOne(this, null, filter);
				_slStatementofaccounts.SuppressClearInGetMulti=false;
				_alreadyFetchedSlStatementofaccounts = true;
			}
			return _slStatementofaccounts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SlStatementofaccounts'. These settings will be taken into account
		/// when the property SlStatementofaccounts is requested or GetMultiSlStatementofaccounts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSlStatementofaccounts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_slStatementofaccounts.SortClauses=sortClauses;
			_slStatementofaccounts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketAssignmentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketAssignments(forceFetch, _ticketAssignments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketAssignments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection GetMultiTicketAssignments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketAssignments || forceFetch || _alwaysFetchTicketAssignments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketAssignments);
				_ticketAssignments.SuppressClearInGetMulti=!forceFetch;
				_ticketAssignments.EntityFactoryToUse = entityFactoryToUse;
				_ticketAssignments.GetMultiManyToOne(null, null, this, null, null, filter);
				_ticketAssignments.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketAssignments = true;
			}
			return _ticketAssignments;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketAssignments'. These settings will be taken into account
		/// when the property TicketAssignments is requested or GetMultiTicketAssignments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketAssignments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketAssignments.SortClauses=sortClauses;
			_ticketAssignments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch)
		{
			return GetMultiAddresses(forceFetch, _addresses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAddresses || forceFetch || _alwaysFetchAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addresses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ContractFields.ContractID, ComparisonOperator.Equal, this.ContractID, "ContractEntity__"));
				_addresses.SuppressClearInGetMulti=!forceFetch;
				_addresses.EntityFactoryToUse = entityFactoryToUse;
				_addresses.GetMulti(filter, GetRelationsForField("Addresses"));
				_addresses.SuppressClearInGetMulti=false;
				_alreadyFetchedAddresses = true;
			}
			return _addresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'Addresses'. These settings will be taken into account
		/// when the property Addresses is requested or GetMultiAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addresses.SortClauses=sortClauses;
			_addresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCards || forceFetch || _alwaysFetchCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cards);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ContractFields.ContractID, ComparisonOperator.Equal, this.ContractID, "ContractEntity__"));
				_cards.SuppressClearInGetMulti=!forceFetch;
				_cards.EntityFactoryToUse = entityFactoryToUse;
				_cards.GetMulti(filter, GetRelationsForField("Cards"));
				_cards.SuppressClearInGetMulti=false;
				_alreadyFetchedCards = true;
			}
			return _cards;
		}

		/// <summary> Sets the collection parameters for the collection for 'Cards'. These settings will be taken into account
		/// when the property Cards is requested or GetMultiCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cards.SortClauses=sortClauses;
			_cards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'NotificationMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch)
		{
			return GetMultiNotificationMessages(forceFetch, _notificationMessages.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.NotificationMessageCollection GetMultiNotificationMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedNotificationMessages || forceFetch || _alwaysFetchNotificationMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_notificationMessages);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ContractFields.ContractID, ComparisonOperator.Equal, this.ContractID, "ContractEntity__"));
				_notificationMessages.SuppressClearInGetMulti=!forceFetch;
				_notificationMessages.EntityFactoryToUse = entityFactoryToUse;
				_notificationMessages.GetMulti(filter, GetRelationsForField("NotificationMessages"));
				_notificationMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedNotificationMessages = true;
			}
			return _notificationMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'NotificationMessages'. These settings will be taken into account
		/// when the property NotificationMessages is requested or GetMultiNotificationMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersNotificationMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_notificationMessages.SortClauses=sortClauses;
			_notificationMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleVanpoolClient()
		{
			return GetSingleVanpoolClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleVanpoolClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedVanpoolClient || forceFetch || _alwaysFetchVanpoolClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingVanpoolClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VanpoolClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vanpoolClientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VanpoolClient = newEntity;
				_alreadyFetchedVanpoolClient = fetchResult;
			}
			return _vanpoolClient;
		}


		/// <summary> Retrieves the related entity of type 'AccountingModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AccountingModeEntity' which is related to this entity.</returns>
		public AccountingModeEntity GetSingleAccountingMode()
		{
			return GetSingleAccountingMode(false);
		}

		/// <summary> Retrieves the related entity of type 'AccountingModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AccountingModeEntity' which is related to this entity.</returns>
		public virtual AccountingModeEntity GetSingleAccountingMode(bool forceFetch)
		{
			if( ( !_alreadyFetchedAccountingMode || forceFetch || _alwaysFetchAccountingMode) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AccountingModeEntityUsingAccountingModeID);
				AccountingModeEntity newEntity = new AccountingModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AccountingModeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AccountingModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_accountingModeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AccountingMode = newEntity;
				_alreadyFetchedAccountingMode = fetchResult;
			}
			return _accountingMode;
		}


		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public OrganizationEntity GetSingleOrganization()
		{
			return GetSingleOrganization(false);
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public virtual OrganizationEntity GetSingleOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrganization || forceFetch || _alwaysFetchOrganization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationEntityUsingOrganizationID);
				OrganizationEntity newEntity = new OrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrganizationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_organizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Organization = newEntity;
				_alreadyFetchedOrganization = fetchResult;
			}
			return _organization;
		}

		/// <summary> Retrieves the related entity of type 'PaymentModalityEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentModalityEntity' which is related to this entity.</returns>
		public PaymentModalityEntity GetSinglePaymentModality()
		{
			return GetSinglePaymentModality(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentModalityEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentModalityEntity' which is related to this entity.</returns>
		public virtual PaymentModalityEntity GetSinglePaymentModality(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentModality || forceFetch || _alwaysFetchPaymentModality) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentModalityEntityUsingPaymentModalityID);
				PaymentModalityEntity newEntity = new PaymentModalityEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentModalityID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentModalityEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentModalityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentModality = newEntity;
				_alreadyFetchedPaymentModality = fetchResult;
			}
			return _paymentModality;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("VanpoolClient", _vanpoolClient);
			toReturn.Add("AccountingMode", _accountingMode);
			toReturn.Add("Organization", _organization);
			toReturn.Add("CardEvents", _cardEvents);
			toReturn.Add("CardHolders", _cardHolders);
			toReturn.Add("CardsToContracts", _cardsToContracts);
			toReturn.Add("SlComments", _slComments);
			toReturn.Add("ContractAddresses", _contractAddresses);
			toReturn.Add("ContractHistoryItems", _contractHistoryItems);
			toReturn.Add("TargetContracts", _targetContracts);
			toReturn.Add("SourceContracts", _sourceContracts);
			toReturn.Add("ContractTerminations", _contractTerminations);
			toReturn.Add("ContractToPaymentMethods", _contractToPaymentMethods);
			toReturn.Add("ContractToProviders", _contractToProviders);
			toReturn.Add("WorkItems", _workItems);
			toReturn.Add("CustomerAccounts", _customerAccounts);
			toReturn.Add("DunningProcesses", _dunningProcesses);
			toReturn.Add("FareEvasionIncidents", _fareEvasionIncidents);
			toReturn.Add("Invoices", _invoices);
			toReturn.Add("Jobs", _jobs);
			toReturn.Add("JobBulkImports", _jobBulkImports);
			toReturn.Add("NotificationMessageOwners", _notificationMessageOwners);
			toReturn.Add("Orders", _orders);
			toReturn.Add("OrganizationParticipants", _organizationParticipants);
			toReturn.Add("ParticipantGroups", _participantGroups);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("PaymentOptions", _paymentOptions);
			toReturn.Add("PaymentProviderAccounts", _paymentProviderAccounts);
			toReturn.Add("Photographs", _photographs);
			toReturn.Add("Postings", _postings);
			toReturn.Add("Products", _products);
			toReturn.Add("ProductTerminations", _productTerminations);
			toReturn.Add("Sales", _sales);
			toReturn.Add("SecondarySales", _secondarySales);
			toReturn.Add("SlStatementofaccounts", _slStatementofaccounts);
			toReturn.Add("TicketAssignments", _ticketAssignments);
			toReturn.Add("Addresses", _addresses);
			toReturn.Add("Cards", _cards);
			toReturn.Add("NotificationMessages", _notificationMessages);
			toReturn.Add("PaymentModality", _paymentModality);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="validator">The validator object for this ContractEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 contractID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(contractID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardEvents = new VarioSL.Entities.CollectionClasses.CardEventCollection();
			_cardEvents.SetContainingEntityInfo(this, "Contract");

			_cardHolders = new VarioSL.Entities.CollectionClasses.CardHolderCollection();
			_cardHolders.SetContainingEntityInfo(this, "Contract");

			_cardsToContracts = new VarioSL.Entities.CollectionClasses.CardToContractCollection();
			_cardsToContracts.SetContainingEntityInfo(this, "Contract");

			_slComments = new VarioSL.Entities.CollectionClasses.CommentCollection();
			_slComments.SetContainingEntityInfo(this, "Contract");

			_contractAddresses = new VarioSL.Entities.CollectionClasses.ContractAddressCollection();
			_contractAddresses.SetContainingEntityInfo(this, "Contract");

			_contractHistoryItems = new VarioSL.Entities.CollectionClasses.ContractHistoryCollection();
			_contractHistoryItems.SetContainingEntityInfo(this, "Contract");

			_targetContracts = new VarioSL.Entities.CollectionClasses.ContractLinkCollection();
			_targetContracts.SetContainingEntityInfo(this, "SourceContract");

			_sourceContracts = new VarioSL.Entities.CollectionClasses.ContractLinkCollection();
			_sourceContracts.SetContainingEntityInfo(this, "TargetContract");

			_contractTerminations = new VarioSL.Entities.CollectionClasses.ContractTerminationCollection();
			_contractTerminations.SetContainingEntityInfo(this, "Contract");

			_contractToPaymentMethods = new VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection();
			_contractToPaymentMethods.SetContainingEntityInfo(this, "Contract");

			_contractToProviders = new VarioSL.Entities.CollectionClasses.ContractToProviderCollection();
			_contractToProviders.SetContainingEntityInfo(this, "Contract");

			_workItems = new VarioSL.Entities.CollectionClasses.ContractWorkItemCollection();
			_workItems.SetContainingEntityInfo(this, "Contract");

			_customerAccounts = new VarioSL.Entities.CollectionClasses.CustomerAccountCollection();
			_customerAccounts.SetContainingEntityInfo(this, "Contract");

			_dunningProcesses = new VarioSL.Entities.CollectionClasses.DunningProcessCollection();
			_dunningProcesses.SetContainingEntityInfo(this, "Contract");

			_fareEvasionIncidents = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidents.SetContainingEntityInfo(this, "Contract");

			_invoices = new VarioSL.Entities.CollectionClasses.InvoiceCollection();
			_invoices.SetContainingEntityInfo(this, "Contract");

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "Contract");

			_jobBulkImports = new VarioSL.Entities.CollectionClasses.JobBulkImportCollection();
			_jobBulkImports.SetContainingEntityInfo(this, "Contract");

			_notificationMessageOwners = new VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection();
			_notificationMessageOwners.SetContainingEntityInfo(this, "Contract");

			_orders = new VarioSL.Entities.CollectionClasses.OrderCollection();
			_orders.SetContainingEntityInfo(this, "Contract");

			_organizationParticipants = new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection();
			_organizationParticipants.SetContainingEntityInfo(this, "Contract");

			_participantGroups = new VarioSL.Entities.CollectionClasses.ParticipantGroupCollection();
			_participantGroups.SetContainingEntityInfo(this, "Contract");

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "Contract");

			_paymentOptions = new VarioSL.Entities.CollectionClasses.PaymentOptionCollection();
			_paymentOptions.SetContainingEntityInfo(this, "Contract");

			_paymentProviderAccounts = new VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection();
			_paymentProviderAccounts.SetContainingEntityInfo(this, "Contract");

			_photographs = new VarioSL.Entities.CollectionClasses.PhotographCollection();
			_photographs.SetContainingEntityInfo(this, "Contract");

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "Contract");

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Contract");

			_productTerminations = new VarioSL.Entities.CollectionClasses.ProductTerminationCollection();
			_productTerminations.SetContainingEntityInfo(this, "Contract");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "Contract");

			_secondarySales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_secondarySales.SetContainingEntityInfo(this, "SecondaryContract");

			_slStatementofaccounts = new VarioSL.Entities.CollectionClasses.StatementOfAccountCollection();
			_slStatementofaccounts.SetContainingEntityInfo(this, "Contract");

			_ticketAssignments = new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection();
			_ticketAssignments.SetContainingEntityInfo(this, "Contract");
			_addresses = new VarioSL.Entities.CollectionClasses.AddressCollection();
			_cards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_notificationMessages = new VarioSL.Entities.CollectionClasses.NotificationMessageCollection();
			_clientReturnsNewIfNotFound = false;
			_vanpoolClientReturnsNewIfNotFound = false;
			_accountingModeReturnsNewIfNotFound = false;
			_organizationReturnsNewIfNotFound = false;
			_paymentModalityReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountingModeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdditionalText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientAuthenticationToken", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentModalityID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VanpoolClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VanpoolGroupID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticContractRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Contracts", resetFKFields, new int[] { (int)ContractFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticContractRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _vanpoolClient</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVanpoolClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vanpoolClient, new PropertyChangedEventHandler( OnVanpoolClientPropertyChanged ), "VanpoolClient", VarioSL.Entities.RelationClasses.StaticContractRelations.ClientEntityUsingVanpoolClientIDStatic, true, signalRelatedEntity, "VanpoolContracts", resetFKFields, new int[] { (int)ContractFieldIndex.VanpoolClientID } );		
			_vanpoolClient = null;
		}
		
		/// <summary> setups the sync logic for member _vanpoolClient</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVanpoolClient(IEntityCore relatedEntity)
		{
			if(_vanpoolClient!=relatedEntity)
			{		
				DesetupSyncVanpoolClient(true, true);
				_vanpoolClient = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vanpoolClient, new PropertyChangedEventHandler( OnVanpoolClientPropertyChanged ), "VanpoolClient", VarioSL.Entities.RelationClasses.StaticContractRelations.ClientEntityUsingVanpoolClientIDStatic, true, ref _alreadyFetchedVanpoolClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVanpoolClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _accountingMode</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAccountingMode(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _accountingMode, new PropertyChangedEventHandler( OnAccountingModePropertyChanged ), "AccountingMode", VarioSL.Entities.RelationClasses.StaticContractRelations.AccountingModeEntityUsingAccountingModeIDStatic, true, signalRelatedEntity, "Contracts", resetFKFields, new int[] { (int)ContractFieldIndex.AccountingModeID } );		
			_accountingMode = null;
		}
		
		/// <summary> setups the sync logic for member _accountingMode</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAccountingMode(IEntityCore relatedEntity)
		{
			if(_accountingMode!=relatedEntity)
			{		
				DesetupSyncAccountingMode(true, true);
				_accountingMode = (AccountingModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _accountingMode, new PropertyChangedEventHandler( OnAccountingModePropertyChanged ), "AccountingMode", VarioSL.Entities.RelationClasses.StaticContractRelations.AccountingModeEntityUsingAccountingModeIDStatic, true, ref _alreadyFetchedAccountingMode, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAccountingModePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _organization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticContractRelations.OrganizationEntityUsingOrganizationIDStatic, true, signalRelatedEntity, "Contracts", resetFKFields, new int[] { (int)ContractFieldIndex.OrganizationID } );		
			_organization = null;
		}
		
		/// <summary> setups the sync logic for member _organization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrganization(IEntityCore relatedEntity)
		{
			if(_organization!=relatedEntity)
			{		
				DesetupSyncOrganization(true, true);
				_organization = (OrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticContractRelations.OrganizationEntityUsingOrganizationIDStatic, true, ref _alreadyFetchedOrganization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentModality</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentModality(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentModality, new PropertyChangedEventHandler( OnPaymentModalityPropertyChanged ), "PaymentModality", VarioSL.Entities.RelationClasses.StaticContractRelations.PaymentModalityEntityUsingPaymentModalityIDStatic, true, signalRelatedEntity, "Contract", resetFKFields, new int[] { (int)ContractFieldIndex.PaymentModalityID } );
			_paymentModality = null;
		}
	
		/// <summary> setups the sync logic for member _paymentModality</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentModality(IEntityCore relatedEntity)
		{
			if(_paymentModality!=relatedEntity)
			{
				DesetupSyncPaymentModality(true, true);
				_paymentModality = (PaymentModalityEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentModality, new PropertyChangedEventHandler( OnPaymentModalityPropertyChanged ), "PaymentModality", VarioSL.Entities.RelationClasses.StaticContractRelations.PaymentModalityEntityUsingPaymentModalityIDStatic, true, ref _alreadyFetchedPaymentModality, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentModalityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="contractID">PK value for Contract which data should be fetched into this Contract object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 contractID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ContractFieldIndex.ContractID].ForcedCurrentValueWrite(contractID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateContractDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ContractEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ContractRelations Relations
		{
			get	{ return new ContractRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardEvent' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardEvents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardEventCollection(), (IEntityRelation)GetRelationsForField("CardEvents")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CardEventEntity, 0, null, null, null, "CardEvents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolders")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardToContract' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardsToContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardToContractCollection(), (IEntityRelation)GetRelationsForField("CardsToContracts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CardToContractEntity, 0, null, null, null, "CardsToContracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Comment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSlComments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CommentCollection(), (IEntityRelation)GetRelationsForField("SlComments")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CommentEntity, 0, null, null, null, "SlComments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractAddressCollection(), (IEntityRelation)GetRelationsForField("ContractAddresses")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractAddressEntity, 0, null, null, null, "ContractAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractHistoryItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractHistoryCollection(), (IEntityRelation)GetRelationsForField("ContractHistoryItems")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractHistoryEntity, 0, null, null, null, "ContractHistoryItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractLink' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTargetContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractLinkCollection(), (IEntityRelation)GetRelationsForField("TargetContracts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractLinkEntity, 0, null, null, null, "TargetContracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractLink' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSourceContracts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractLinkCollection(), (IEntityRelation)GetRelationsForField("SourceContracts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractLinkEntity, 0, null, null, null, "SourceContracts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractTerminationCollection(), (IEntityRelation)GetRelationsForField("ContractTerminations")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractTerminationEntity, 0, null, null, null, "ContractTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractToPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractToPaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("ContractToPaymentMethods")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractToPaymentMethodEntity, 0, null, null, null, "ContractToPaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractToProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContractToProviders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractToProviderCollection(), (IEntityRelation)GetRelationsForField("ContractToProviders")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractToProviderEntity, 0, null, null, null, "ContractToProviders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ContractWorkItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractWorkItemCollection(), (IEntityRelation)GetRelationsForField("WorkItems")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ContractWorkItemEntity, 0, null, null, null, "WorkItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccounts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountCollection(), (IEntityRelation)GetRelationsForField("CustomerAccounts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CustomerAccountEntity, 0, null, null, null, "CustomerAccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningProcess' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningProcesses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningProcessCollection(), (IEntityRelation)GetRelationsForField("DunningProcesses")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.DunningProcessEntity, 0, null, null, null, "DunningProcesses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidents")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoices")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'JobBulkImport' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobBulkImports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobBulkImportCollection(), (IEntityRelation)GetRelationsForField("JobBulkImports")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.JobBulkImportEntity, 0, null, null, null, "JobBulkImports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessageOwner' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessageOwners
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection(), (IEntityRelation)GetRelationsForField("NotificationMessageOwners")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.NotificationMessageOwnerEntity, 0, null, null, null, "NotificationMessageOwners", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Orders")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Orders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrganizationParticipant' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganizationParticipants
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection(), (IEntityRelation)GetRelationsForField("OrganizationParticipants")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.OrganizationParticipantEntity, 0, null, null, null, "OrganizationParticipants", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParticipantGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParticipantGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParticipantGroupCollection(), (IEntityRelation)GetRelationsForField("ParticipantGroups")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ParticipantGroupEntity, 0, null, null, null, "ParticipantGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOptions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOptions")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOptions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentProviderAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentProviderAccounts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection(), (IEntityRelation)GetRelationsForField("PaymentProviderAccounts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PaymentProviderAccountEntity, 0, null, null, null, "PaymentProviderAccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Photograph' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPhotographs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PhotographCollection(), (IEntityRelation)GetRelationsForField("Photographs")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PhotographEntity, 0, null, null, null, "Photographs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductTerminationCollection(), (IEntityRelation)GetRelationsForField("ProductTerminations")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ProductTerminationEntity, 0, null, null, null, "ProductTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSecondarySales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("SecondarySales")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "SecondarySales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StatementOfAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSlStatementofaccounts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StatementOfAccountCollection(), (IEntityRelation)GetRelationsForField("SlStatementofaccounts")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.StatementOfAccountEntity, 0, null, null, null, "SlStatementofaccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketAssignment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketAssignments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketAssignmentCollection(), (IEntityRelation)GetRelationsForField("TicketAssignments")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.TicketAssignmentEntity, 0, null, null, null, "TicketAssignments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddresses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ContractAddressEntityUsingContractID;
				intermediateRelation.SetAliases(string.Empty, "ContractAddress_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, GetRelationsForField("Addresses"), "Addresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCards
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CardToContractEntityUsingContractID;
				intermediateRelation.SetAliases(string.Empty, "CardToContract_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, GetRelationsForField("Cards"), "Cards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'NotificationMessage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNotificationMessages
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.NotificationMessageOwnerEntityUsingContractID;
				intermediateRelation.SetAliases(string.Empty, "NotificationMessageOwner_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NotificationMessageCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.NotificationMessageEntity, 0, null, null, GetRelationsForField("NotificationMessages"), "NotificationMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVanpoolClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("VanpoolClient")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "VanpoolClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AccountingMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAccountingMode
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AccountingModeCollection(), (IEntityRelation)GetRelationsForField("AccountingMode")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.AccountingModeEntity, 0, null, null, null, "AccountingMode", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organization")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentModality'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentModality
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentModalityCollection(), (IEntityRelation)GetRelationsForField("PaymentModality")[0], (int)VarioSL.Entities.EntityType.ContractEntity, (int)VarioSL.Entities.EntityType.PaymentModalityEntity, 0, null, null, null, "PaymentModality", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountingModeID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."ACCOUNTINGMODEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountingModeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractFieldIndex.AccountingModeID, false); }
			set	{ SetValue((int)ContractFieldIndex.AccountingModeID, value, true); }
		}

		/// <summary> The AdditionalText property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."ADDITIONALTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdditionalText
		{
			get { return (System.String)GetValue((int)ContractFieldIndex.AdditionalText, true); }
			set	{ SetValue((int)ContractFieldIndex.AdditionalText, value, true); }
		}

		/// <summary> The ClientAuthenticationToken property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."CLIENTAUTHENTICATIONTOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 120<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientAuthenticationToken
		{
			get { return (System.String)GetValue((int)ContractFieldIndex.ClientAuthenticationToken, true); }
			set	{ SetValue((int)ContractFieldIndex.ClientAuthenticationToken, value, true); }
		}

		/// <summary> The ClientID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractFieldIndex.ClientID, false); }
			set	{ SetValue((int)ContractFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)ContractFieldIndex.ContractID, true); }
			set	{ SetValue((int)ContractFieldIndex.ContractID, value, true); }
		}

		/// <summary> The ContractNumber property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."CONTRACTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ContractNumber
		{
			get { return (System.String)GetValue((int)ContractFieldIndex.ContractNumber, true); }
			set	{ SetValue((int)ContractFieldIndex.ContractNumber, value, true); }
		}

		/// <summary> The ContractType property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."CONTRACTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ContractType> ContractType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ContractType>)GetValue((int)ContractFieldIndex.ContractType, false); }
			set	{ SetValue((int)ContractFieldIndex.ContractType, value, true); }
		}

		/// <summary> The LastModified property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ContractFieldIndex.LastModified, true); }
			set	{ SetValue((int)ContractFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ContractFieldIndex.LastUser, true); }
			set	{ SetValue((int)ContractFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrganizationID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."ORGANIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrganizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractFieldIndex.OrganizationID, false); }
			set	{ SetValue((int)ContractFieldIndex.OrganizationID, value, true); }
		}

		/// <summary> The PaymentModalityID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."PAYMENTMODALITYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentModalityID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractFieldIndex.PaymentModalityID, false); }
			set	{ SetValue((int)ContractFieldIndex.PaymentModalityID, value, true); }
		}

		/// <summary> The State property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ContractState State
		{
			get { return (VarioSL.Entities.Enumerations.ContractState)GetValue((int)ContractFieldIndex.State, true); }
			set	{ SetValue((int)ContractFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ContractFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ContractFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)ContractFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)ContractFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)ContractFieldIndex.ValidTo, true); }
			set	{ SetValue((int)ContractFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The VanpoolClientID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."VANPOOLCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VanpoolClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ContractFieldIndex.VanpoolClientID, false); }
			set	{ SetValue((int)ContractFieldIndex.VanpoolClientID, value, true); }
		}

		/// <summary> The VanpoolGroupID property of the Entity Contract<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONTRACT"."VANPOOLGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VanpoolGroupID
		{
			get { return (System.String)GetValue((int)ContractFieldIndex.VanpoolGroupID, true); }
			set	{ SetValue((int)ContractFieldIndex.VanpoolGroupID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardEventEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardEvents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardEventCollection CardEvents
		{
			get	{ return GetMultiCardEvents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardEvents. When set to true, CardEvents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardEvents is accessed. You can always execute/ a forced fetch by calling GetMultiCardEvents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardEvents
		{
			get	{ return _alwaysFetchCardEvents; }
			set	{ _alwaysFetchCardEvents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardEvents already has been fetched. Setting this property to false when CardEvents has been fetched
		/// will clear the CardEvents collection well. Setting this property to true while CardEvents hasn't been fetched disables lazy loading for CardEvents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardEvents
		{
			get { return _alreadyFetchedCardEvents;}
			set 
			{
				if(_alreadyFetchedCardEvents && !value && (_cardEvents != null))
				{
					_cardEvents.Clear();
				}
				_alreadyFetchedCardEvents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardHolders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardHolderCollection CardHolders
		{
			get	{ return GetMultiCardHolders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolders. When set to true, CardHolders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolders is accessed. You can always execute/ a forced fetch by calling GetMultiCardHolders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolders
		{
			get	{ return _alwaysFetchCardHolders; }
			set	{ _alwaysFetchCardHolders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolders already has been fetched. Setting this property to false when CardHolders has been fetched
		/// will clear the CardHolders collection well. Setting this property to true while CardHolders hasn't been fetched disables lazy loading for CardHolders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolders
		{
			get { return _alreadyFetchedCardHolders;}
			set 
			{
				if(_alreadyFetchedCardHolders && !value && (_cardHolders != null))
				{
					_cardHolders.Clear();
				}
				_alreadyFetchedCardHolders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardToContractEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardsToContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardToContractCollection CardsToContracts
		{
			get	{ return GetMultiCardsToContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardsToContracts. When set to true, CardsToContracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardsToContracts is accessed. You can always execute/ a forced fetch by calling GetMultiCardsToContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardsToContracts
		{
			get	{ return _alwaysFetchCardsToContracts; }
			set	{ _alwaysFetchCardsToContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardsToContracts already has been fetched. Setting this property to false when CardsToContracts has been fetched
		/// will clear the CardsToContracts collection well. Setting this property to true while CardsToContracts hasn't been fetched disables lazy loading for CardsToContracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardsToContracts
		{
			get { return _alreadyFetchedCardsToContracts;}
			set 
			{
				if(_alreadyFetchedCardsToContracts && !value && (_cardsToContracts != null))
				{
					_cardsToContracts.Clear();
				}
				_alreadyFetchedCardsToContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CommentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSlComments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CommentCollection SlComments
		{
			get	{ return GetMultiSlComments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SlComments. When set to true, SlComments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SlComments is accessed. You can always execute/ a forced fetch by calling GetMultiSlComments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSlComments
		{
			get	{ return _alwaysFetchSlComments; }
			set	{ _alwaysFetchSlComments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SlComments already has been fetched. Setting this property to false when SlComments has been fetched
		/// will clear the SlComments collection well. Setting this property to true while SlComments hasn't been fetched disables lazy loading for SlComments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSlComments
		{
			get { return _alreadyFetchedSlComments;}
			set 
			{
				if(_alreadyFetchedSlComments && !value && (_slComments != null))
				{
					_slComments.Clear();
				}
				_alreadyFetchedSlComments = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractAddressCollection ContractAddresses
		{
			get	{ return GetMultiContractAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractAddresses. When set to true, ContractAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiContractAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractAddresses
		{
			get	{ return _alwaysFetchContractAddresses; }
			set	{ _alwaysFetchContractAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractAddresses already has been fetched. Setting this property to false when ContractAddresses has been fetched
		/// will clear the ContractAddresses collection well. Setting this property to true while ContractAddresses hasn't been fetched disables lazy loading for ContractAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractAddresses
		{
			get { return _alreadyFetchedContractAddresses;}
			set 
			{
				if(_alreadyFetchedContractAddresses && !value && (_contractAddresses != null))
				{
					_contractAddresses.Clear();
				}
				_alreadyFetchedContractAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractHistoryItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractHistoryCollection ContractHistoryItems
		{
			get	{ return GetMultiContractHistoryItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractHistoryItems. When set to true, ContractHistoryItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractHistoryItems is accessed. You can always execute/ a forced fetch by calling GetMultiContractHistoryItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractHistoryItems
		{
			get	{ return _alwaysFetchContractHistoryItems; }
			set	{ _alwaysFetchContractHistoryItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractHistoryItems already has been fetched. Setting this property to false when ContractHistoryItems has been fetched
		/// will clear the ContractHistoryItems collection well. Setting this property to true while ContractHistoryItems hasn't been fetched disables lazy loading for ContractHistoryItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractHistoryItems
		{
			get { return _alreadyFetchedContractHistoryItems;}
			set 
			{
				if(_alreadyFetchedContractHistoryItems && !value && (_contractHistoryItems != null))
				{
					_contractHistoryItems.Clear();
				}
				_alreadyFetchedContractHistoryItems = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTargetContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractLinkCollection TargetContracts
		{
			get	{ return GetMultiTargetContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TargetContracts. When set to true, TargetContracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TargetContracts is accessed. You can always execute/ a forced fetch by calling GetMultiTargetContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTargetContracts
		{
			get	{ return _alwaysFetchTargetContracts; }
			set	{ _alwaysFetchTargetContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TargetContracts already has been fetched. Setting this property to false when TargetContracts has been fetched
		/// will clear the TargetContracts collection well. Setting this property to true while TargetContracts hasn't been fetched disables lazy loading for TargetContracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTargetContracts
		{
			get { return _alreadyFetchedTargetContracts;}
			set 
			{
				if(_alreadyFetchedTargetContracts && !value && (_targetContracts != null))
				{
					_targetContracts.Clear();
				}
				_alreadyFetchedTargetContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractLinkEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSourceContracts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractLinkCollection SourceContracts
		{
			get	{ return GetMultiSourceContracts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SourceContracts. When set to true, SourceContracts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SourceContracts is accessed. You can always execute/ a forced fetch by calling GetMultiSourceContracts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSourceContracts
		{
			get	{ return _alwaysFetchSourceContracts; }
			set	{ _alwaysFetchSourceContracts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SourceContracts already has been fetched. Setting this property to false when SourceContracts has been fetched
		/// will clear the SourceContracts collection well. Setting this property to true while SourceContracts hasn't been fetched disables lazy loading for SourceContracts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSourceContracts
		{
			get { return _alreadyFetchedSourceContracts;}
			set 
			{
				if(_alreadyFetchedSourceContracts && !value && (_sourceContracts != null))
				{
					_sourceContracts.Clear();
				}
				_alreadyFetchedSourceContracts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractTerminationCollection ContractTerminations
		{
			get	{ return GetMultiContractTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractTerminations. When set to true, ContractTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiContractTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractTerminations
		{
			get	{ return _alwaysFetchContractTerminations; }
			set	{ _alwaysFetchContractTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractTerminations already has been fetched. Setting this property to false when ContractTerminations has been fetched
		/// will clear the ContractTerminations collection well. Setting this property to true while ContractTerminations hasn't been fetched disables lazy loading for ContractTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractTerminations
		{
			get { return _alreadyFetchedContractTerminations;}
			set 
			{
				if(_alreadyFetchedContractTerminations && !value && (_contractTerminations != null))
				{
					_contractTerminations.Clear();
				}
				_alreadyFetchedContractTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractToPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractToPaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractToPaymentMethodCollection ContractToPaymentMethods
		{
			get	{ return GetMultiContractToPaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractToPaymentMethods. When set to true, ContractToPaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractToPaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiContractToPaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractToPaymentMethods
		{
			get	{ return _alwaysFetchContractToPaymentMethods; }
			set	{ _alwaysFetchContractToPaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractToPaymentMethods already has been fetched. Setting this property to false when ContractToPaymentMethods has been fetched
		/// will clear the ContractToPaymentMethods collection well. Setting this property to true while ContractToPaymentMethods hasn't been fetched disables lazy loading for ContractToPaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractToPaymentMethods
		{
			get { return _alreadyFetchedContractToPaymentMethods;}
			set 
			{
				if(_alreadyFetchedContractToPaymentMethods && !value && (_contractToPaymentMethods != null))
				{
					_contractToPaymentMethods.Clear();
				}
				_alreadyFetchedContractToPaymentMethods = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractToProviderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContractToProviders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractToProviderCollection ContractToProviders
		{
			get	{ return GetMultiContractToProviders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContractToProviders. When set to true, ContractToProviders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContractToProviders is accessed. You can always execute/ a forced fetch by calling GetMultiContractToProviders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContractToProviders
		{
			get	{ return _alwaysFetchContractToProviders; }
			set	{ _alwaysFetchContractToProviders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContractToProviders already has been fetched. Setting this property to false when ContractToProviders has been fetched
		/// will clear the ContractToProviders collection well. Setting this property to true while ContractToProviders hasn't been fetched disables lazy loading for ContractToProviders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContractToProviders
		{
			get { return _alreadyFetchedContractToProviders;}
			set 
			{
				if(_alreadyFetchedContractToProviders && !value && (_contractToProviders != null))
				{
					_contractToProviders.Clear();
				}
				_alreadyFetchedContractToProviders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ContractWorkItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiWorkItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ContractWorkItemCollection WorkItems
		{
			get	{ return GetMultiWorkItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItems. When set to true, WorkItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItems is accessed. You can always execute/ a forced fetch by calling GetMultiWorkItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItems
		{
			get	{ return _alwaysFetchWorkItems; }
			set	{ _alwaysFetchWorkItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItems already has been fetched. Setting this property to false when WorkItems has been fetched
		/// will clear the WorkItems collection well. Setting this property to true while WorkItems hasn't been fetched disables lazy loading for WorkItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItems
		{
			get { return _alreadyFetchedWorkItems;}
			set 
			{
				if(_alreadyFetchedWorkItems && !value && (_workItems != null))
				{
					_workItems.Clear();
				}
				_alreadyFetchedWorkItems = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccounts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountCollection CustomerAccounts
		{
			get	{ return GetMultiCustomerAccounts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccounts. When set to true, CustomerAccounts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccounts is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccounts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccounts
		{
			get	{ return _alwaysFetchCustomerAccounts; }
			set	{ _alwaysFetchCustomerAccounts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccounts already has been fetched. Setting this property to false when CustomerAccounts has been fetched
		/// will clear the CustomerAccounts collection well. Setting this property to true while CustomerAccounts hasn't been fetched disables lazy loading for CustomerAccounts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccounts
		{
			get { return _alreadyFetchedCustomerAccounts;}
			set 
			{
				if(_alreadyFetchedCustomerAccounts && !value && (_customerAccounts != null))
				{
					_customerAccounts.Clear();
				}
				_alreadyFetchedCustomerAccounts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningProcesses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningProcessCollection DunningProcesses
		{
			get	{ return GetMultiDunningProcesses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningProcesses. When set to true, DunningProcesses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningProcesses is accessed. You can always execute/ a forced fetch by calling GetMultiDunningProcesses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningProcesses
		{
			get	{ return _alwaysFetchDunningProcesses; }
			set	{ _alwaysFetchDunningProcesses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningProcesses already has been fetched. Setting this property to false when DunningProcesses has been fetched
		/// will clear the DunningProcesses collection well. Setting this property to true while DunningProcesses hasn't been fetched disables lazy loading for DunningProcesses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningProcesses
		{
			get { return _alreadyFetchedDunningProcesses;}
			set 
			{
				if(_alreadyFetchedDunningProcesses && !value && (_dunningProcesses != null))
				{
					_dunningProcesses.Clear();
				}
				_alreadyFetchedDunningProcesses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidents
		{
			get	{ return GetMultiFareEvasionIncidents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidents. When set to true, FareEvasionIncidents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidents is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidents
		{
			get	{ return _alwaysFetchFareEvasionIncidents; }
			set	{ _alwaysFetchFareEvasionIncidents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidents already has been fetched. Setting this property to false when FareEvasionIncidents has been fetched
		/// will clear the FareEvasionIncidents collection well. Setting this property to true while FareEvasionIncidents hasn't been fetched disables lazy loading for FareEvasionIncidents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidents
		{
			get { return _alreadyFetchedFareEvasionIncidents;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidents && !value && (_fareEvasionIncidents != null))
				{
					_fareEvasionIncidents.Clear();
				}
				_alreadyFetchedFareEvasionIncidents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceCollection Invoices
		{
			get	{ return GetMultiInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Invoices. When set to true, Invoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoices is accessed. You can always execute/ a forced fetch by calling GetMultiInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoices
		{
			get	{ return _alwaysFetchInvoices; }
			set	{ _alwaysFetchInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoices already has been fetched. Setting this property to false when Invoices has been fetched
		/// will clear the Invoices collection well. Setting this property to true while Invoices hasn't been fetched disables lazy loading for Invoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoices
		{
			get { return _alreadyFetchedInvoices;}
			set 
			{
				if(_alreadyFetchedInvoices && !value && (_invoices != null))
				{
					_invoices.Clear();
				}
				_alreadyFetchedInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobBulkImportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobBulkImports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobBulkImportCollection JobBulkImports
		{
			get	{ return GetMultiJobBulkImports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for JobBulkImports. When set to true, JobBulkImports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time JobBulkImports is accessed. You can always execute/ a forced fetch by calling GetMultiJobBulkImports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobBulkImports
		{
			get	{ return _alwaysFetchJobBulkImports; }
			set	{ _alwaysFetchJobBulkImports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property JobBulkImports already has been fetched. Setting this property to false when JobBulkImports has been fetched
		/// will clear the JobBulkImports collection well. Setting this property to true while JobBulkImports hasn't been fetched disables lazy loading for JobBulkImports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobBulkImports
		{
			get { return _alreadyFetchedJobBulkImports;}
			set 
			{
				if(_alreadyFetchedJobBulkImports && !value && (_jobBulkImports != null))
				{
					_jobBulkImports.Clear();
				}
				_alreadyFetchedJobBulkImports = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'NotificationMessageOwnerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessageOwners()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageOwnerCollection NotificationMessageOwners
		{
			get	{ return GetMultiNotificationMessageOwners(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessageOwners. When set to true, NotificationMessageOwners is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessageOwners is accessed. You can always execute/ a forced fetch by calling GetMultiNotificationMessageOwners(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessageOwners
		{
			get	{ return _alwaysFetchNotificationMessageOwners; }
			set	{ _alwaysFetchNotificationMessageOwners = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessageOwners already has been fetched. Setting this property to false when NotificationMessageOwners has been fetched
		/// will clear the NotificationMessageOwners collection well. Setting this property to true while NotificationMessageOwners hasn't been fetched disables lazy loading for NotificationMessageOwners</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessageOwners
		{
			get { return _alreadyFetchedNotificationMessageOwners;}
			set 
			{
				if(_alreadyFetchedNotificationMessageOwners && !value && (_notificationMessageOwners != null))
				{
					_notificationMessageOwners.Clear();
				}
				_alreadyFetchedNotificationMessageOwners = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderCollection Orders
		{
			get	{ return GetMultiOrders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Orders. When set to true, Orders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Orders is accessed. You can always execute/ a forced fetch by calling GetMultiOrders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrders
		{
			get	{ return _alwaysFetchOrders; }
			set	{ _alwaysFetchOrders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Orders already has been fetched. Setting this property to false when Orders has been fetched
		/// will clear the Orders collection well. Setting this property to true while Orders hasn't been fetched disables lazy loading for Orders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrders
		{
			get { return _alreadyFetchedOrders;}
			set 
			{
				if(_alreadyFetchedOrders && !value && (_orders != null))
				{
					_orders.Clear();
				}
				_alreadyFetchedOrders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrganizationParticipantEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrganizationParticipants()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrganizationParticipantCollection OrganizationParticipants
		{
			get	{ return GetMultiOrganizationParticipants(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrganizationParticipants. When set to true, OrganizationParticipants is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrganizationParticipants is accessed. You can always execute/ a forced fetch by calling GetMultiOrganizationParticipants(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganizationParticipants
		{
			get	{ return _alwaysFetchOrganizationParticipants; }
			set	{ _alwaysFetchOrganizationParticipants = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrganizationParticipants already has been fetched. Setting this property to false when OrganizationParticipants has been fetched
		/// will clear the OrganizationParticipants collection well. Setting this property to true while OrganizationParticipants hasn't been fetched disables lazy loading for OrganizationParticipants</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganizationParticipants
		{
			get { return _alreadyFetchedOrganizationParticipants;}
			set 
			{
				if(_alreadyFetchedOrganizationParticipants && !value && (_organizationParticipants != null))
				{
					_organizationParticipants.Clear();
				}
				_alreadyFetchedOrganizationParticipants = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParticipantGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParticipantGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParticipantGroupCollection ParticipantGroups
		{
			get	{ return GetMultiParticipantGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParticipantGroups. When set to true, ParticipantGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParticipantGroups is accessed. You can always execute/ a forced fetch by calling GetMultiParticipantGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParticipantGroups
		{
			get	{ return _alwaysFetchParticipantGroups; }
			set	{ _alwaysFetchParticipantGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParticipantGroups already has been fetched. Setting this property to false when ParticipantGroups has been fetched
		/// will clear the ParticipantGroups collection well. Setting this property to true while ParticipantGroups hasn't been fetched disables lazy loading for ParticipantGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParticipantGroups
		{
			get { return _alreadyFetchedParticipantGroups;}
			set 
			{
				if(_alreadyFetchedParticipantGroups && !value && (_participantGroups != null))
				{
					_participantGroups.Clear();
				}
				_alreadyFetchedParticipantGroups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentOptionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentOptions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentOptionCollection PaymentOptions
		{
			get	{ return GetMultiPaymentOptions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOptions. When set to true, PaymentOptions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOptions is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentOptions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOptions
		{
			get	{ return _alwaysFetchPaymentOptions; }
			set	{ _alwaysFetchPaymentOptions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOptions already has been fetched. Setting this property to false when PaymentOptions has been fetched
		/// will clear the PaymentOptions collection well. Setting this property to true while PaymentOptions hasn't been fetched disables lazy loading for PaymentOptions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOptions
		{
			get { return _alreadyFetchedPaymentOptions;}
			set 
			{
				if(_alreadyFetchedPaymentOptions && !value && (_paymentOptions != null))
				{
					_paymentOptions.Clear();
				}
				_alreadyFetchedPaymentOptions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentProviderAccountEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentProviderAccounts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentProviderAccountCollection PaymentProviderAccounts
		{
			get	{ return GetMultiPaymentProviderAccounts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentProviderAccounts. When set to true, PaymentProviderAccounts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentProviderAccounts is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentProviderAccounts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentProviderAccounts
		{
			get	{ return _alwaysFetchPaymentProviderAccounts; }
			set	{ _alwaysFetchPaymentProviderAccounts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentProviderAccounts already has been fetched. Setting this property to false when PaymentProviderAccounts has been fetched
		/// will clear the PaymentProviderAccounts collection well. Setting this property to true while PaymentProviderAccounts hasn't been fetched disables lazy loading for PaymentProviderAccounts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentProviderAccounts
		{
			get { return _alreadyFetchedPaymentProviderAccounts;}
			set 
			{
				if(_alreadyFetchedPaymentProviderAccounts && !value && (_paymentProviderAccounts != null))
				{
					_paymentProviderAccounts.Clear();
				}
				_alreadyFetchedPaymentProviderAccounts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPhotographs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PhotographCollection Photographs
		{
			get	{ return GetMultiPhotographs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Photographs. When set to true, Photographs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Photographs is accessed. You can always execute/ a forced fetch by calling GetMultiPhotographs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPhotographs
		{
			get	{ return _alwaysFetchPhotographs; }
			set	{ _alwaysFetchPhotographs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Photographs already has been fetched. Setting this property to false when Photographs has been fetched
		/// will clear the Photographs collection well. Setting this property to true while Photographs hasn't been fetched disables lazy loading for Photographs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPhotographs
		{
			get { return _alreadyFetchedPhotographs;}
			set 
			{
				if(_alreadyFetchedPhotographs && !value && (_photographs != null))
				{
					_photographs.Clear();
				}
				_alreadyFetchedPhotographs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection ProductTerminations
		{
			get	{ return GetMultiProductTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTerminations. When set to true, ProductTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiProductTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTerminations
		{
			get	{ return _alwaysFetchProductTerminations; }
			set	{ _alwaysFetchProductTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTerminations already has been fetched. Setting this property to false when ProductTerminations has been fetched
		/// will clear the ProductTerminations collection well. Setting this property to true while ProductTerminations hasn't been fetched disables lazy loading for ProductTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTerminations
		{
			get { return _alreadyFetchedProductTerminations;}
			set 
			{
				if(_alreadyFetchedProductTerminations && !value && (_productTerminations != null))
				{
					_productTerminations.Clear();
				}
				_alreadyFetchedProductTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSecondarySales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection SecondarySales
		{
			get	{ return GetMultiSecondarySales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SecondarySales. When set to true, SecondarySales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SecondarySales is accessed. You can always execute/ a forced fetch by calling GetMultiSecondarySales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSecondarySales
		{
			get	{ return _alwaysFetchSecondarySales; }
			set	{ _alwaysFetchSecondarySales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SecondarySales already has been fetched. Setting this property to false when SecondarySales has been fetched
		/// will clear the SecondarySales collection well. Setting this property to true while SecondarySales hasn't been fetched disables lazy loading for SecondarySales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSecondarySales
		{
			get { return _alreadyFetchedSecondarySales;}
			set 
			{
				if(_alreadyFetchedSecondarySales && !value && (_secondarySales != null))
				{
					_secondarySales.Clear();
				}
				_alreadyFetchedSecondarySales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSlStatementofaccounts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StatementOfAccountCollection SlStatementofaccounts
		{
			get	{ return GetMultiSlStatementofaccounts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SlStatementofaccounts. When set to true, SlStatementofaccounts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SlStatementofaccounts is accessed. You can always execute/ a forced fetch by calling GetMultiSlStatementofaccounts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSlStatementofaccounts
		{
			get	{ return _alwaysFetchSlStatementofaccounts; }
			set	{ _alwaysFetchSlStatementofaccounts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SlStatementofaccounts already has been fetched. Setting this property to false when SlStatementofaccounts has been fetched
		/// will clear the SlStatementofaccounts collection well. Setting this property to true while SlStatementofaccounts hasn't been fetched disables lazy loading for SlStatementofaccounts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSlStatementofaccounts
		{
			get { return _alreadyFetchedSlStatementofaccounts;}
			set 
			{
				if(_alreadyFetchedSlStatementofaccounts && !value && (_slStatementofaccounts != null))
				{
					_slStatementofaccounts.Clear();
				}
				_alreadyFetchedSlStatementofaccounts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketAssignmentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketAssignments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketAssignmentCollection TicketAssignments
		{
			get	{ return GetMultiTicketAssignments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketAssignments. When set to true, TicketAssignments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketAssignments is accessed. You can always execute/ a forced fetch by calling GetMultiTicketAssignments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketAssignments
		{
			get	{ return _alwaysFetchTicketAssignments; }
			set	{ _alwaysFetchTicketAssignments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketAssignments already has been fetched. Setting this property to false when TicketAssignments has been fetched
		/// will clear the TicketAssignments collection well. Setting this property to true while TicketAssignments hasn't been fetched disables lazy loading for TicketAssignments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketAssignments
		{
			get { return _alreadyFetchedTicketAssignments;}
			set 
			{
				if(_alreadyFetchedTicketAssignments && !value && (_ticketAssignments != null))
				{
					_ticketAssignments.Clear();
				}
				_alreadyFetchedTicketAssignments = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressCollection Addresses
		{
			get { return GetMultiAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Addresses. When set to true, Addresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Addresses is accessed. You can always execute a forced fetch by calling GetMultiAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddresses
		{
			get	{ return _alwaysFetchAddresses; }
			set	{ _alwaysFetchAddresses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Addresses already has been fetched. Setting this property to false when Addresses has been fetched
		/// will clear the Addresses collection well. Setting this property to true while Addresses hasn't been fetched disables lazy loading for Addresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddresses
		{
			get { return _alreadyFetchedAddresses;}
			set 
			{
				if(_alreadyFetchedAddresses && !value && (_addresses != null))
				{
					_addresses.Clear();
				}
				_alreadyFetchedAddresses = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection Cards
		{
			get { return GetMultiCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Cards. When set to true, Cards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Cards is accessed. You can always execute a forced fetch by calling GetMultiCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCards
		{
			get	{ return _alwaysFetchCards; }
			set	{ _alwaysFetchCards = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Cards already has been fetched. Setting this property to false when Cards has been fetched
		/// will clear the Cards collection well. Setting this property to true while Cards hasn't been fetched disables lazy loading for Cards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCards
		{
			get { return _alreadyFetchedCards;}
			set 
			{
				if(_alreadyFetchedCards && !value && (_cards != null))
				{
					_cards.Clear();
				}
				_alreadyFetchedCards = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'NotificationMessageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiNotificationMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.NotificationMessageCollection NotificationMessages
		{
			get { return GetMultiNotificationMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for NotificationMessages. When set to true, NotificationMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time NotificationMessages is accessed. You can always execute a forced fetch by calling GetMultiNotificationMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNotificationMessages
		{
			get	{ return _alwaysFetchNotificationMessages; }
			set	{ _alwaysFetchNotificationMessages = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property NotificationMessages already has been fetched. Setting this property to false when NotificationMessages has been fetched
		/// will clear the NotificationMessages collection well. Setting this property to true while NotificationMessages hasn't been fetched disables lazy loading for NotificationMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNotificationMessages
		{
			get { return _alreadyFetchedNotificationMessages;}
			set 
			{
				if(_alreadyFetchedNotificationMessages && !value && (_notificationMessages != null))
				{
					_notificationMessages.Clear();
				}
				_alreadyFetchedNotificationMessages = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Contracts", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVanpoolClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity VanpoolClient
		{
			get	{ return GetSingleVanpoolClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVanpoolClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VanpoolContracts", "VanpoolClient", _vanpoolClient, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VanpoolClient. When set to true, VanpoolClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VanpoolClient is accessed. You can always execute a forced fetch by calling GetSingleVanpoolClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVanpoolClient
		{
			get	{ return _alwaysFetchVanpoolClient; }
			set	{ _alwaysFetchVanpoolClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VanpoolClient already has been fetched. Setting this property to false when VanpoolClient has been fetched
		/// will set VanpoolClient to null as well. Setting this property to true while VanpoolClient hasn't been fetched disables lazy loading for VanpoolClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVanpoolClient
		{
			get { return _alreadyFetchedVanpoolClient;}
			set 
			{
				if(_alreadyFetchedVanpoolClient && !value)
				{
					this.VanpoolClient = null;
				}
				_alreadyFetchedVanpoolClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VanpoolClient is not found
		/// in the database. When set to true, VanpoolClient will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VanpoolClientReturnsNewIfNotFound
		{
			get	{ return _vanpoolClientReturnsNewIfNotFound; }
			set { _vanpoolClientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AccountingModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAccountingMode()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AccountingModeEntity AccountingMode
		{
			get	{ return GetSingleAccountingMode(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAccountingMode(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Contracts", "AccountingMode", _accountingMode, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AccountingMode. When set to true, AccountingMode is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AccountingMode is accessed. You can always execute a forced fetch by calling GetSingleAccountingMode(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAccountingMode
		{
			get	{ return _alwaysFetchAccountingMode; }
			set	{ _alwaysFetchAccountingMode = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AccountingMode already has been fetched. Setting this property to false when AccountingMode has been fetched
		/// will set AccountingMode to null as well. Setting this property to true while AccountingMode hasn't been fetched disables lazy loading for AccountingMode</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAccountingMode
		{
			get { return _alreadyFetchedAccountingMode;}
			set 
			{
				if(_alreadyFetchedAccountingMode && !value)
				{
					this.AccountingMode = null;
				}
				_alreadyFetchedAccountingMode = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AccountingMode is not found
		/// in the database. When set to true, AccountingMode will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AccountingModeReturnsNewIfNotFound
		{
			get	{ return _accountingModeReturnsNewIfNotFound; }
			set { _accountingModeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationEntity Organization
		{
			get	{ return GetSingleOrganization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrganization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Contracts", "Organization", _organization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Organization. When set to true, Organization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organization is accessed. You can always execute a forced fetch by calling GetSingleOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganization
		{
			get	{ return _alwaysFetchOrganization; }
			set	{ _alwaysFetchOrganization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organization already has been fetched. Setting this property to false when Organization has been fetched
		/// will set Organization to null as well. Setting this property to true while Organization hasn't been fetched disables lazy loading for Organization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganization
		{
			get { return _alreadyFetchedOrganization;}
			set 
			{
				if(_alreadyFetchedOrganization && !value)
				{
					this.Organization = null;
				}
				_alreadyFetchedOrganization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Organization is not found
		/// in the database. When set to true, Organization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrganizationReturnsNewIfNotFound
		{
			get	{ return _organizationReturnsNewIfNotFound; }
			set { _organizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentModalityEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentModality()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentModalityEntity PaymentModality
		{
			get	{ return GetSinglePaymentModality(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentModality(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentModality !=null);
						DesetupSyncPaymentModality(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentModality");
						}
					}
					else
					{
						if(_paymentModality!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Contract");
							SetupSyncPaymentModality(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentModality. When set to true, PaymentModality is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentModality is accessed. You can always execute a forced fetch by calling GetSinglePaymentModality(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentModality
		{
			get	{ return _alwaysFetchPaymentModality; }
			set	{ _alwaysFetchPaymentModality = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentModality already has been fetched. Setting this property to false when PaymentModality has been fetched
		/// will set PaymentModality to null as well. Setting this property to true while PaymentModality hasn't been fetched disables lazy loading for PaymentModality</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentModality
		{
			get { return _alreadyFetchedPaymentModality;}
			set 
			{
				if(_alreadyFetchedPaymentModality && !value)
				{
					this.PaymentModality = null;
				}
				_alreadyFetchedPaymentModality = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentModality is not found
		/// in the database. When set to true, PaymentModality will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentModalityReturnsNewIfNotFound
		{
			get	{ return _paymentModalityReturnsNewIfNotFound; }
			set	{ _paymentModalityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ContractEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
