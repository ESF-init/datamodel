﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PickupLocation'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PickupLocationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection	_pickupLocationToTickets;
		private bool	_alwaysFetchPickupLocationToTickets, _alreadyFetchedPickupLocationToTickets;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PickupLocationToTickets</summary>
			public static readonly string PickupLocationToTickets = "PickupLocationToTickets";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PickupLocationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PickupLocationEntity() :base("PickupLocationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		public PickupLocationEntity(System.Int64 pickupLocationID):base("PickupLocationEntity")
		{
			InitClassFetch(pickupLocationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PickupLocationEntity(System.Int64 pickupLocationID, IPrefetchPath prefetchPathToUse):base("PickupLocationEntity")
		{
			InitClassFetch(pickupLocationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="validator">The custom validator object for this PickupLocationEntity</param>
		public PickupLocationEntity(System.Int64 pickupLocationID, IValidator validator):base("PickupLocationEntity")
		{
			InitClassFetch(pickupLocationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PickupLocationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_pickupLocationToTickets = (VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection)info.GetValue("_pickupLocationToTickets", typeof(VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection));
			_alwaysFetchPickupLocationToTickets = info.GetBoolean("_alwaysFetchPickupLocationToTickets");
			_alreadyFetchedPickupLocationToTickets = info.GetBoolean("_alreadyFetchedPickupLocationToTickets");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPickupLocationToTickets = (_pickupLocationToTickets.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PickupLocationToTickets":
					toReturn.Add(Relations.PickupLocationToTicketEntityUsingPickupLocationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_pickupLocationToTickets", (!this.MarkedForDeletion?_pickupLocationToTickets:null));
			info.AddValue("_alwaysFetchPickupLocationToTickets", _alwaysFetchPickupLocationToTickets);
			info.AddValue("_alreadyFetchedPickupLocationToTickets", _alreadyFetchedPickupLocationToTickets);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PickupLocationToTickets":
					_alreadyFetchedPickupLocationToTickets = true;
					if(entity!=null)
					{
						this.PickupLocationToTickets.Add((PickupLocationToTicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PickupLocationToTickets":
					_pickupLocationToTickets.Add((PickupLocationToTicketEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PickupLocationToTickets":
					this.PerformRelatedEntityRemoval(_pickupLocationToTickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_pickupLocationToTickets);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pickupLocationID)
		{
			return FetchUsingPK(pickupLocationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pickupLocationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pickupLocationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pickupLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pickupLocationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pickupLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pickupLocationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PickupLocationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PickupLocationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PickupLocationToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PickupLocationToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection GetMultiPickupLocationToTickets(bool forceFetch)
		{
			return GetMultiPickupLocationToTickets(forceFetch, _pickupLocationToTickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PickupLocationToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PickupLocationToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection GetMultiPickupLocationToTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPickupLocationToTickets(forceFetch, _pickupLocationToTickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PickupLocationToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection GetMultiPickupLocationToTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPickupLocationToTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PickupLocationToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection GetMultiPickupLocationToTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPickupLocationToTickets || forceFetch || _alwaysFetchPickupLocationToTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pickupLocationToTickets);
				_pickupLocationToTickets.SuppressClearInGetMulti=!forceFetch;
				_pickupLocationToTickets.EntityFactoryToUse = entityFactoryToUse;
				_pickupLocationToTickets.GetMultiManyToOne(this, filter);
				_pickupLocationToTickets.SuppressClearInGetMulti=false;
				_alreadyFetchedPickupLocationToTickets = true;
			}
			return _pickupLocationToTickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'PickupLocationToTickets'. These settings will be taken into account
		/// when the property PickupLocationToTickets is requested or GetMultiPickupLocationToTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPickupLocationToTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pickupLocationToTickets.SortClauses=sortClauses;
			_pickupLocationToTickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PickupLocationToTickets", _pickupLocationToTickets);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="validator">The validator object for this PickupLocationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 pickupLocationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pickupLocationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_pickupLocationToTickets = new VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection();
			_pickupLocationToTickets.SetContainingEntityInfo(this, "PickupLocation");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressField1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressField2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContactPerson", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Coordinates", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PickupLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostalCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Region", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Street", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StreetNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pickupLocationID">PK value for PickupLocation which data should be fetched into this PickupLocation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 pickupLocationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PickupLocationFieldIndex.PickupLocationID].ForcedCurrentValueWrite(pickupLocationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePickupLocationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PickupLocationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PickupLocationRelations Relations
		{
			get	{ return new PickupLocationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PickupLocationToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPickupLocationToTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection(), (IEntityRelation)GetRelationsForField("PickupLocationToTickets")[0], (int)VarioSL.Entities.EntityType.PickupLocationEntity, (int)VarioSL.Entities.EntityType.PickupLocationToTicketEntity, 0, null, null, null, "PickupLocationToTickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AddressField1 property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."ADDRESSFIELD1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressField1
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.AddressField1, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.AddressField1, value, true); }
		}

		/// <summary> The AddressField2 property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."ADDRESSFIELD2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AddressField2
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.AddressField2, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.AddressField2, value, true); }
		}

		/// <summary> The City property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."CITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String City
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.City, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.City, value, true); }
		}

		/// <summary> The ContactPerson property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."CONTACTPERSON"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ContactPerson
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.ContactPerson, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.ContactPerson, value, true); }
		}

		/// <summary> The Coordinates property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."COORDINATES"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Coordinates
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.Coordinates, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.Coordinates, value, true); }
		}

		/// <summary> The LastModified property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PickupLocationFieldIndex.LastModified, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.LastUser, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrganizationName property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."ORGANIZATIONNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationName
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.OrganizationName, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.OrganizationName, value, true); }
		}

		/// <summary> The PhoneNumber property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."PHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhoneNumber
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.PhoneNumber, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.PhoneNumber, value, true); }
		}

		/// <summary> The PickupLocationID property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."PICKUPLOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PickupLocationID
		{
			get { return (System.Int64)GetValue((int)PickupLocationFieldIndex.PickupLocationID, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.PickupLocationID, value, true); }
		}

		/// <summary> The PostalCode property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."POSTALCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostalCode
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.PostalCode, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.PostalCode, value, true); }
		}

		/// <summary> The Region property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."REGION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Region
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.Region, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.Region, value, true); }
		}

		/// <summary> The Street property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."STREET"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Street
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.Street, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.Street, value, true); }
		}

		/// <summary> The StreetNumber property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."STREETNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StreetNumber
		{
			get { return (System.String)GetValue((int)PickupLocationFieldIndex.StreetNumber, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.StreetNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PickupLocation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PICKUPLOCATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PickupLocationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PickupLocationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PickupLocationToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPickupLocationToTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PickupLocationToTicketCollection PickupLocationToTickets
		{
			get	{ return GetMultiPickupLocationToTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PickupLocationToTickets. When set to true, PickupLocationToTickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PickupLocationToTickets is accessed. You can always execute/ a forced fetch by calling GetMultiPickupLocationToTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPickupLocationToTickets
		{
			get	{ return _alwaysFetchPickupLocationToTickets; }
			set	{ _alwaysFetchPickupLocationToTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PickupLocationToTickets already has been fetched. Setting this property to false when PickupLocationToTickets has been fetched
		/// will clear the PickupLocationToTickets collection well. Setting this property to true while PickupLocationToTickets hasn't been fetched disables lazy loading for PickupLocationToTickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPickupLocationToTickets
		{
			get { return _alreadyFetchedPickupLocationToTickets;}
			set 
			{
				if(_alreadyFetchedPickupLocationToTickets && !value && (_pickupLocationToTickets != null))
				{
					_pickupLocationToTickets.Clear();
				}
				_alreadyFetchedPickupLocationToTickets = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PickupLocationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
