﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AuditRegister'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AuditRegisterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection	_auditRegisterValues;
		private bool	_alwaysFetchAuditRegisterValues, _alreadyFetchedAuditRegisterValues;
		private ShiftInventoryEntity _shiftInventory;
		private bool	_alwaysFetchShiftInventory, _alreadyFetchedShiftInventory, _shiftInventoryReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ShiftInventory</summary>
			public static readonly string ShiftInventory = "ShiftInventory";
			/// <summary>Member name AuditRegisterValues</summary>
			public static readonly string AuditRegisterValues = "AuditRegisterValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AuditRegisterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AuditRegisterEntity() :base("AuditRegisterEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		public AuditRegisterEntity(System.Int64 auditRegisterID):base("AuditRegisterEntity")
		{
			InitClassFetch(auditRegisterID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AuditRegisterEntity(System.Int64 auditRegisterID, IPrefetchPath prefetchPathToUse):base("AuditRegisterEntity")
		{
			InitClassFetch(auditRegisterID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="validator">The custom validator object for this AuditRegisterEntity</param>
		public AuditRegisterEntity(System.Int64 auditRegisterID, IValidator validator):base("AuditRegisterEntity")
		{
			InitClassFetch(auditRegisterID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AuditRegisterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_auditRegisterValues = (VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection)info.GetValue("_auditRegisterValues", typeof(VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection));
			_alwaysFetchAuditRegisterValues = info.GetBoolean("_alwaysFetchAuditRegisterValues");
			_alreadyFetchedAuditRegisterValues = info.GetBoolean("_alreadyFetchedAuditRegisterValues");
			_shiftInventory = (ShiftInventoryEntity)info.GetValue("_shiftInventory", typeof(ShiftInventoryEntity));
			if(_shiftInventory!=null)
			{
				_shiftInventory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_shiftInventoryReturnsNewIfNotFound = info.GetBoolean("_shiftInventoryReturnsNewIfNotFound");
			_alwaysFetchShiftInventory = info.GetBoolean("_alwaysFetchShiftInventory");
			_alreadyFetchedShiftInventory = info.GetBoolean("_alreadyFetchedShiftInventory");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AuditRegisterFieldIndex)fieldIndex)
			{
				case AuditRegisterFieldIndex.ShiftID:
					DesetupSyncShiftInventory(true, false);
					_alreadyFetchedShiftInventory = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAuditRegisterValues = (_auditRegisterValues.Count > 0);
			_alreadyFetchedShiftInventory = (_shiftInventory != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ShiftInventory":
					toReturn.Add(Relations.ShiftInventoryEntityUsingShiftID);
					break;
				case "AuditRegisterValues":
					toReturn.Add(Relations.AuditRegisterValueEntityUsingAuditRegisterID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_auditRegisterValues", (!this.MarkedForDeletion?_auditRegisterValues:null));
			info.AddValue("_alwaysFetchAuditRegisterValues", _alwaysFetchAuditRegisterValues);
			info.AddValue("_alreadyFetchedAuditRegisterValues", _alreadyFetchedAuditRegisterValues);
			info.AddValue("_shiftInventory", (!this.MarkedForDeletion?_shiftInventory:null));
			info.AddValue("_shiftInventoryReturnsNewIfNotFound", _shiftInventoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchShiftInventory", _alwaysFetchShiftInventory);
			info.AddValue("_alreadyFetchedShiftInventory", _alreadyFetchedShiftInventory);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ShiftInventory":
					_alreadyFetchedShiftInventory = true;
					this.ShiftInventory = (ShiftInventoryEntity)entity;
					break;
				case "AuditRegisterValues":
					_alreadyFetchedAuditRegisterValues = true;
					if(entity!=null)
					{
						this.AuditRegisterValues.Add((AuditRegisterValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ShiftInventory":
					SetupSyncShiftInventory(relatedEntity);
					break;
				case "AuditRegisterValues":
					_auditRegisterValues.Add((AuditRegisterValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ShiftInventory":
					DesetupSyncShiftInventory(false, true);
					break;
				case "AuditRegisterValues":
					this.PerformRelatedEntityRemoval(_auditRegisterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_shiftInventory!=null)
			{
				toReturn.Add(_shiftInventory);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_auditRegisterValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterID)
		{
			return FetchUsingPK(auditRegisterID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(auditRegisterID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(auditRegisterID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 auditRegisterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(auditRegisterID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AuditRegisterID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AuditRegisterRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AuditRegisterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection GetMultiAuditRegisterValues(bool forceFetch)
		{
			return GetMultiAuditRegisterValues(forceFetch, _auditRegisterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AuditRegisterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection GetMultiAuditRegisterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAuditRegisterValues(forceFetch, _auditRegisterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection GetMultiAuditRegisterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAuditRegisterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection GetMultiAuditRegisterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAuditRegisterValues || forceFetch || _alwaysFetchAuditRegisterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_auditRegisterValues);
				_auditRegisterValues.SuppressClearInGetMulti=!forceFetch;
				_auditRegisterValues.EntityFactoryToUse = entityFactoryToUse;
				_auditRegisterValues.GetMultiManyToOne(this, filter);
				_auditRegisterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedAuditRegisterValues = true;
			}
			return _auditRegisterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'AuditRegisterValues'. These settings will be taken into account
		/// when the property AuditRegisterValues is requested or GetMultiAuditRegisterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAuditRegisterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_auditRegisterValues.SortClauses=sortClauses;
			_auditRegisterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ShiftInventoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ShiftInventoryEntity' which is related to this entity.</returns>
		public ShiftInventoryEntity GetSingleShiftInventory()
		{
			return GetSingleShiftInventory(false);
		}

		/// <summary> Retrieves the related entity of type 'ShiftInventoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ShiftInventoryEntity' which is related to this entity.</returns>
		public virtual ShiftInventoryEntity GetSingleShiftInventory(bool forceFetch)
		{
			if( ( !_alreadyFetchedShiftInventory || forceFetch || _alwaysFetchShiftInventory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ShiftInventoryEntityUsingShiftID);
				ShiftInventoryEntity newEntity = new ShiftInventoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ShiftID);
				}
				if(fetchResult)
				{
					newEntity = (ShiftInventoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_shiftInventoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ShiftInventory = newEntity;
				_alreadyFetchedShiftInventory = fetchResult;
			}
			return _shiftInventory;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ShiftInventory", _shiftInventory);
			toReturn.Add("AuditRegisterValues", _auditRegisterValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="validator">The validator object for this AuditRegisterEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 auditRegisterID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(auditRegisterID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_auditRegisterValues = new VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection();
			_auditRegisterValues.SetContainingEntityInfo(this, "AuditRegister");
			_shiftInventoryReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuditRegisterID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _shiftInventory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncShiftInventory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _shiftInventory, new PropertyChangedEventHandler( OnShiftInventoryPropertyChanged ), "ShiftInventory", VarioSL.Entities.RelationClasses.StaticAuditRegisterRelations.ShiftInventoryEntityUsingShiftIDStatic, true, signalRelatedEntity, "AuditRegisters", resetFKFields, new int[] { (int)AuditRegisterFieldIndex.ShiftID } );		
			_shiftInventory = null;
		}
		
		/// <summary> setups the sync logic for member _shiftInventory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncShiftInventory(IEntityCore relatedEntity)
		{
			if(_shiftInventory!=relatedEntity)
			{		
				DesetupSyncShiftInventory(true, true);
				_shiftInventory = (ShiftInventoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _shiftInventory, new PropertyChangedEventHandler( OnShiftInventoryPropertyChanged ), "ShiftInventory", VarioSL.Entities.RelationClasses.StaticAuditRegisterRelations.ShiftInventoryEntityUsingShiftIDStatic, true, ref _alreadyFetchedShiftInventory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnShiftInventoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="auditRegisterID">PK value for AuditRegister which data should be fetched into this AuditRegister object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 auditRegisterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AuditRegisterFieldIndex.AuditRegisterID].ForcedCurrentValueWrite(auditRegisterID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAuditRegisterDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AuditRegisterEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AuditRegisterRelations Relations
		{
			get	{ return new AuditRegisterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AuditRegisterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAuditRegisterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection(), (IEntityRelation)GetRelationsForField("AuditRegisterValues")[0], (int)VarioSL.Entities.EntityType.AuditRegisterEntity, (int)VarioSL.Entities.EntityType.AuditRegisterValueEntity, 0, null, null, null, "AuditRegisterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ShiftInventory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShiftInventory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ShiftInventoryCollection(), (IEntityRelation)GetRelationsForField("ShiftInventory")[0], (int)VarioSL.Entities.EntityType.AuditRegisterEntity, (int)VarioSL.Entities.EntityType.ShiftInventoryEntity, 0, null, null, null, "ShiftInventory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AuditRegisterID property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."AUDITREGISTERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 AuditRegisterID
		{
			get { return (System.Int64)GetValue((int)AuditRegisterFieldIndex.AuditRegisterID, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.AuditRegisterID, value, true); }
		}

		/// <summary> The LastModified property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AuditRegisterFieldIndex.LastModified, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)AuditRegisterFieldIndex.LastUser, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.LastUser, value, true); }
		}

		/// <summary> The ReportDateTime property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."REPORTDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ReportDateTime
		{
			get { return (System.DateTime)GetValue((int)AuditRegisterFieldIndex.ReportDateTime, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.ReportDateTime, value, true); }
		}

		/// <summary> The ShiftID property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."SHIFTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ShiftID
		{
			get { return (System.String)GetValue((int)AuditRegisterFieldIndex.ShiftID, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.ShiftID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity AuditRegister<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_AUDITREGISTER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)AuditRegisterFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)AuditRegisterFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AuditRegisterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAuditRegisterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AuditRegisterValueCollection AuditRegisterValues
		{
			get	{ return GetMultiAuditRegisterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AuditRegisterValues. When set to true, AuditRegisterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AuditRegisterValues is accessed. You can always execute/ a forced fetch by calling GetMultiAuditRegisterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAuditRegisterValues
		{
			get	{ return _alwaysFetchAuditRegisterValues; }
			set	{ _alwaysFetchAuditRegisterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AuditRegisterValues already has been fetched. Setting this property to false when AuditRegisterValues has been fetched
		/// will clear the AuditRegisterValues collection well. Setting this property to true while AuditRegisterValues hasn't been fetched disables lazy loading for AuditRegisterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAuditRegisterValues
		{
			get { return _alreadyFetchedAuditRegisterValues;}
			set 
			{
				if(_alreadyFetchedAuditRegisterValues && !value && (_auditRegisterValues != null))
				{
					_auditRegisterValues.Clear();
				}
				_alreadyFetchedAuditRegisterValues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ShiftInventoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleShiftInventory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ShiftInventoryEntity ShiftInventory
		{
			get	{ return GetSingleShiftInventory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncShiftInventory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AuditRegisters", "ShiftInventory", _shiftInventory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ShiftInventory. When set to true, ShiftInventory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ShiftInventory is accessed. You can always execute a forced fetch by calling GetSingleShiftInventory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShiftInventory
		{
			get	{ return _alwaysFetchShiftInventory; }
			set	{ _alwaysFetchShiftInventory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ShiftInventory already has been fetched. Setting this property to false when ShiftInventory has been fetched
		/// will set ShiftInventory to null as well. Setting this property to true while ShiftInventory hasn't been fetched disables lazy loading for ShiftInventory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShiftInventory
		{
			get { return _alreadyFetchedShiftInventory;}
			set 
			{
				if(_alreadyFetchedShiftInventory && !value)
				{
					this.ShiftInventory = null;
				}
				_alreadyFetchedShiftInventory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ShiftInventory is not found
		/// in the database. When set to true, ShiftInventory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ShiftInventoryReturnsNewIfNotFound
		{
			get	{ return _shiftInventoryReturnsNewIfNotFound; }
			set { _shiftInventoryReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AuditRegisterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
