﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Posting'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PostingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DunningToPostingCollection	_dunningToPostings;
		private bool	_alwaysFetchDunningToPostings, _alreadyFetchedDunningToPostings;
		private VarioSL.Entities.CollectionClasses.DunningProcessCollection _dunningProcesses;
		private bool	_alwaysFetchDunningProcesses, _alreadyFetchedDunningProcesses;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private VarioSettlementEntity _varioSettlement;
		private bool	_alwaysFetchVarioSettlement, _alreadyFetchedVarioSettlement, _varioSettlementReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private InvoiceEntity _invoice;
		private bool	_alwaysFetchInvoice, _alreadyFetchedInvoice, _invoiceReturnsNewIfNotFound;
		private InvoiceEntryEntity _invoiceEntry;
		private bool	_alwaysFetchInvoiceEntry, _alreadyFetchedInvoiceEntry, _invoiceEntryReturnsNewIfNotFound;
		private PostingKeyEntity _postingKey;
		private bool	_alwaysFetchPostingKey, _alreadyFetchedPostingKey, _postingKeyReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name VarioSettlement</summary>
			public static readonly string VarioSettlement = "VarioSettlement";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Invoice</summary>
			public static readonly string Invoice = "Invoice";
			/// <summary>Member name InvoiceEntry</summary>
			public static readonly string InvoiceEntry = "InvoiceEntry";
			/// <summary>Member name PostingKey</summary>
			public static readonly string PostingKey = "PostingKey";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name DunningToPostings</summary>
			public static readonly string DunningToPostings = "DunningToPostings";
			/// <summary>Member name DunningProcesses</summary>
			public static readonly string DunningProcesses = "DunningProcesses";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PostingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PostingEntity() :base("PostingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		public PostingEntity(System.Int64 postingID):base("PostingEntity")
		{
			InitClassFetch(postingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PostingEntity(System.Int64 postingID, IPrefetchPath prefetchPathToUse):base("PostingEntity")
		{
			InitClassFetch(postingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="validator">The custom validator object for this PostingEntity</param>
		public PostingEntity(System.Int64 postingID, IValidator validator):base("PostingEntity")
		{
			InitClassFetch(postingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PostingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningToPostings = (VarioSL.Entities.CollectionClasses.DunningToPostingCollection)info.GetValue("_dunningToPostings", typeof(VarioSL.Entities.CollectionClasses.DunningToPostingCollection));
			_alwaysFetchDunningToPostings = info.GetBoolean("_alwaysFetchDunningToPostings");
			_alreadyFetchedDunningToPostings = info.GetBoolean("_alreadyFetchedDunningToPostings");
			_dunningProcesses = (VarioSL.Entities.CollectionClasses.DunningProcessCollection)info.GetValue("_dunningProcesses", typeof(VarioSL.Entities.CollectionClasses.DunningProcessCollection));
			_alwaysFetchDunningProcesses = info.GetBoolean("_alwaysFetchDunningProcesses");
			_alreadyFetchedDunningProcesses = info.GetBoolean("_alreadyFetchedDunningProcesses");
			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_varioSettlement = (VarioSettlementEntity)info.GetValue("_varioSettlement", typeof(VarioSettlementEntity));
			if(_varioSettlement!=null)
			{
				_varioSettlement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_varioSettlementReturnsNewIfNotFound = info.GetBoolean("_varioSettlementReturnsNewIfNotFound");
			_alwaysFetchVarioSettlement = info.GetBoolean("_alwaysFetchVarioSettlement");
			_alreadyFetchedVarioSettlement = info.GetBoolean("_alreadyFetchedVarioSettlement");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_invoice = (InvoiceEntity)info.GetValue("_invoice", typeof(InvoiceEntity));
			if(_invoice!=null)
			{
				_invoice.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoiceReturnsNewIfNotFound = info.GetBoolean("_invoiceReturnsNewIfNotFound");
			_alwaysFetchInvoice = info.GetBoolean("_alwaysFetchInvoice");
			_alreadyFetchedInvoice = info.GetBoolean("_alreadyFetchedInvoice");

			_invoiceEntry = (InvoiceEntryEntity)info.GetValue("_invoiceEntry", typeof(InvoiceEntryEntity));
			if(_invoiceEntry!=null)
			{
				_invoiceEntry.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoiceEntryReturnsNewIfNotFound = info.GetBoolean("_invoiceEntryReturnsNewIfNotFound");
			_alwaysFetchInvoiceEntry = info.GetBoolean("_alwaysFetchInvoiceEntry");
			_alreadyFetchedInvoiceEntry = info.GetBoolean("_alreadyFetchedInvoiceEntry");

			_postingKey = (PostingKeyEntity)info.GetValue("_postingKey", typeof(PostingKeyEntity));
			if(_postingKey!=null)
			{
				_postingKey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_postingKeyReturnsNewIfNotFound = info.GetBoolean("_postingKeyReturnsNewIfNotFound");
			_alwaysFetchPostingKey = info.GetBoolean("_alwaysFetchPostingKey");
			_alreadyFetchedPostingKey = info.GetBoolean("_alreadyFetchedPostingKey");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PostingFieldIndex)fieldIndex)
			{
				case PostingFieldIndex.AccountSettlementId:
					DesetupSyncVarioSettlement(true, false);
					_alreadyFetchedVarioSettlement = false;
					break;
				case PostingFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case PostingFieldIndex.InvoiceEntryID:
					DesetupSyncInvoiceEntry(true, false);
					_alreadyFetchedInvoiceEntry = false;
					break;
				case PostingFieldIndex.InvoiceID:
					DesetupSyncInvoice(true, false);
					_alreadyFetchedInvoice = false;
					break;
				case PostingFieldIndex.PostingKeyID:
					DesetupSyncPostingKey(true, false);
					_alreadyFetchedPostingKey = false;
					break;
				case PostingFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case PostingFieldIndex.UserID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningToPostings = (_dunningToPostings.Count > 0);
			_alreadyFetchedDunningProcesses = (_dunningProcesses.Count > 0);
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedVarioSettlement = (_varioSettlement != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedInvoice = (_invoice != null);
			_alreadyFetchedInvoiceEntry = (_invoiceEntry != null);
			_alreadyFetchedPostingKey = (_postingKey != null);
			_alreadyFetchedProduct = (_product != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "VarioSettlement":
					toReturn.Add(Relations.VarioSettlementEntityUsingAccountSettlementId);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Invoice":
					toReturn.Add(Relations.InvoiceEntityUsingInvoiceID);
					break;
				case "InvoiceEntry":
					toReturn.Add(Relations.InvoiceEntryEntityUsingInvoiceEntryID);
					break;
				case "PostingKey":
					toReturn.Add(Relations.PostingKeyEntityUsingPostingKeyID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "DunningToPostings":
					toReturn.Add(Relations.DunningToPostingEntityUsingPostingID);
					break;
				case "DunningProcesses":
					toReturn.Add(Relations.DunningToPostingEntityUsingPostingID, "PostingEntity__", "DunningToPosting_", JoinHint.None);
					toReturn.Add(DunningToPostingEntity.Relations.DunningProcessEntityUsingDunningProcessID, "DunningToPosting_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningToPostings", (!this.MarkedForDeletion?_dunningToPostings:null));
			info.AddValue("_alwaysFetchDunningToPostings", _alwaysFetchDunningToPostings);
			info.AddValue("_alreadyFetchedDunningToPostings", _alreadyFetchedDunningToPostings);
			info.AddValue("_dunningProcesses", (!this.MarkedForDeletion?_dunningProcesses:null));
			info.AddValue("_alwaysFetchDunningProcesses", _alwaysFetchDunningProcesses);
			info.AddValue("_alreadyFetchedDunningProcesses", _alreadyFetchedDunningProcesses);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_varioSettlement", (!this.MarkedForDeletion?_varioSettlement:null));
			info.AddValue("_varioSettlementReturnsNewIfNotFound", _varioSettlementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVarioSettlement", _alwaysFetchVarioSettlement);
			info.AddValue("_alreadyFetchedVarioSettlement", _alreadyFetchedVarioSettlement);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_invoice", (!this.MarkedForDeletion?_invoice:null));
			info.AddValue("_invoiceReturnsNewIfNotFound", _invoiceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoice", _alwaysFetchInvoice);
			info.AddValue("_alreadyFetchedInvoice", _alreadyFetchedInvoice);
			info.AddValue("_invoiceEntry", (!this.MarkedForDeletion?_invoiceEntry:null));
			info.AddValue("_invoiceEntryReturnsNewIfNotFound", _invoiceEntryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoiceEntry", _alwaysFetchInvoiceEntry);
			info.AddValue("_alreadyFetchedInvoiceEntry", _alreadyFetchedInvoiceEntry);
			info.AddValue("_postingKey", (!this.MarkedForDeletion?_postingKey:null));
			info.AddValue("_postingKeyReturnsNewIfNotFound", _postingKeyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPostingKey", _alwaysFetchPostingKey);
			info.AddValue("_alreadyFetchedPostingKey", _alreadyFetchedPostingKey);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "VarioSettlement":
					_alreadyFetchedVarioSettlement = true;
					this.VarioSettlement = (VarioSettlementEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Invoice":
					_alreadyFetchedInvoice = true;
					this.Invoice = (InvoiceEntity)entity;
					break;
				case "InvoiceEntry":
					_alreadyFetchedInvoiceEntry = true;
					this.InvoiceEntry = (InvoiceEntryEntity)entity;
					break;
				case "PostingKey":
					_alreadyFetchedPostingKey = true;
					this.PostingKey = (PostingKeyEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "DunningToPostings":
					_alreadyFetchedDunningToPostings = true;
					if(entity!=null)
					{
						this.DunningToPostings.Add((DunningToPostingEntity)entity);
					}
					break;
				case "DunningProcesses":
					_alreadyFetchedDunningProcesses = true;
					if(entity!=null)
					{
						this.DunningProcesses.Add((DunningProcessEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "VarioSettlement":
					SetupSyncVarioSettlement(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Invoice":
					SetupSyncInvoice(relatedEntity);
					break;
				case "InvoiceEntry":
					SetupSyncInvoiceEntry(relatedEntity);
					break;
				case "PostingKey":
					SetupSyncPostingKey(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "DunningToPostings":
					_dunningToPostings.Add((DunningToPostingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "VarioSettlement":
					DesetupSyncVarioSettlement(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Invoice":
					DesetupSyncInvoice(false, true);
					break;
				case "InvoiceEntry":
					DesetupSyncInvoiceEntry(false, true);
					break;
				case "PostingKey":
					DesetupSyncPostingKey(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "DunningToPostings":
					this.PerformRelatedEntityRemoval(_dunningToPostings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_varioSettlement!=null)
			{
				toReturn.Add(_varioSettlement);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_invoice!=null)
			{
				toReturn.Add(_invoice);
			}
			if(_invoiceEntry!=null)
			{
				toReturn.Add(_invoiceEntry);
			}
			if(_postingKey!=null)
			{
				toReturn.Add(_postingKey);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_dunningToPostings);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingID)
		{
			return FetchUsingPK(postingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(postingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(postingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 postingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(postingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PostingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PostingRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToPostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch)
		{
			return GetMultiDunningToPostings(forceFetch, _dunningToPostings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToPostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToPostings(forceFetch, _dunningToPostings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToPostingCollection GetMultiDunningToPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToPostings || forceFetch || _alwaysFetchDunningToPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToPostings);
				_dunningToPostings.SuppressClearInGetMulti=!forceFetch;
				_dunningToPostings.EntityFactoryToUse = entityFactoryToUse;
				_dunningToPostings.GetMultiManyToOne(null, this, filter);
				_dunningToPostings.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToPostings = true;
			}
			return _dunningToPostings;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToPostings'. These settings will be taken into account
		/// when the property DunningToPostings is requested or GetMultiDunningToPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToPostings.SortClauses=sortClauses;
			_dunningToPostings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningProcessEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch)
		{
			return GetMultiDunningProcesses(forceFetch, _dunningProcesses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDunningProcesses || forceFetch || _alwaysFetchDunningProcesses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningProcesses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PostingFields.PostingID, ComparisonOperator.Equal, this.PostingID, "PostingEntity__"));
				_dunningProcesses.SuppressClearInGetMulti=!forceFetch;
				_dunningProcesses.EntityFactoryToUse = entityFactoryToUse;
				_dunningProcesses.GetMulti(filter, GetRelationsForField("DunningProcesses"));
				_dunningProcesses.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningProcesses = true;
			}
			return _dunningProcesses;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningProcesses'. These settings will be taken into account
		/// when the property DunningProcesses is requested or GetMultiDunningProcesses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningProcesses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningProcesses.SortClauses=sortClauses;
			_dunningProcesses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID);
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'VarioSettlementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VarioSettlementEntity' which is related to this entity.</returns>
		public VarioSettlementEntity GetSingleVarioSettlement()
		{
			return GetSingleVarioSettlement(false);
		}

		/// <summary> Retrieves the related entity of type 'VarioSettlementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VarioSettlementEntity' which is related to this entity.</returns>
		public virtual VarioSettlementEntity GetSingleVarioSettlement(bool forceFetch)
		{
			if( ( !_alreadyFetchedVarioSettlement || forceFetch || _alwaysFetchVarioSettlement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VarioSettlementEntityUsingAccountSettlementId);
				VarioSettlementEntity newEntity = new VarioSettlementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AccountSettlementId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VarioSettlementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_varioSettlementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VarioSettlement = newEntity;
				_alreadyFetchedVarioSettlement = fetchResult;
			}
			return _varioSettlement;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public InvoiceEntity GetSingleInvoice()
		{
			return GetSingleInvoice(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoiceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoiceEntity' which is related to this entity.</returns>
		public virtual InvoiceEntity GetSingleInvoice(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoice || forceFetch || _alwaysFetchInvoice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoiceEntityUsingInvoiceID);
				InvoiceEntity newEntity = new InvoiceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoiceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InvoiceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoiceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Invoice = newEntity;
				_alreadyFetchedInvoice = fetchResult;
			}
			return _invoice;
		}


		/// <summary> Retrieves the related entity of type 'InvoiceEntryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoiceEntryEntity' which is related to this entity.</returns>
		public InvoiceEntryEntity GetSingleInvoiceEntry()
		{
			return GetSingleInvoiceEntry(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoiceEntryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoiceEntryEntity' which is related to this entity.</returns>
		public virtual InvoiceEntryEntity GetSingleInvoiceEntry(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoiceEntry || forceFetch || _alwaysFetchInvoiceEntry) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoiceEntryEntityUsingInvoiceEntryID);
				InvoiceEntryEntity newEntity = new InvoiceEntryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoiceEntryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InvoiceEntryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoiceEntryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InvoiceEntry = newEntity;
				_alreadyFetchedInvoiceEntry = fetchResult;
			}
			return _invoiceEntry;
		}


		/// <summary> Retrieves the related entity of type 'PostingKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PostingKeyEntity' which is related to this entity.</returns>
		public PostingKeyEntity GetSinglePostingKey()
		{
			return GetSinglePostingKey(false);
		}

		/// <summary> Retrieves the related entity of type 'PostingKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PostingKeyEntity' which is related to this entity.</returns>
		public virtual PostingKeyEntity GetSinglePostingKey(bool forceFetch)
		{
			if( ( !_alreadyFetchedPostingKey || forceFetch || _alwaysFetchPostingKey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PostingKeyEntityUsingPostingKeyID);
				PostingKeyEntity newEntity = new PostingKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PostingKeyID);
				}
				if(fetchResult)
				{
					newEntity = (PostingKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_postingKeyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PostingKey = newEntity;
				_alreadyFetchedPostingKey = fetchResult;
			}
			return _postingKey;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserList", _userList);
			toReturn.Add("VarioSettlement", _varioSettlement);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Invoice", _invoice);
			toReturn.Add("InvoiceEntry", _invoiceEntry);
			toReturn.Add("PostingKey", _postingKey);
			toReturn.Add("Product", _product);
			toReturn.Add("DunningToPostings", _dunningToPostings);
			toReturn.Add("DunningProcesses", _dunningProcesses);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="validator">The validator object for this PostingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 postingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(postingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_dunningToPostings = new VarioSL.Entities.CollectionClasses.DunningToPostingCollection();
			_dunningToPostings.SetContainingEntityInfo(this, "Posting");
			_dunningProcesses = new VarioSL.Entities.CollectionClasses.DunningProcessCollection();
			_userListReturnsNewIfNotFound = false;
			_varioSettlementReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_invoiceReturnsNewIfNotFound = false;
			_invoiceEntryReturnsNewIfNotFound = false;
			_postingKeyReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountSettlementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancelPostingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceEntryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValueDateTime", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticPostingRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.UserID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticPostingRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _varioSettlement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVarioSettlement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _varioSettlement, new PropertyChangedEventHandler( OnVarioSettlementPropertyChanged ), "VarioSettlement", VarioSL.Entities.RelationClasses.StaticPostingRelations.VarioSettlementEntityUsingAccountSettlementIdStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.AccountSettlementId } );		
			_varioSettlement = null;
		}
		
		/// <summary> setups the sync logic for member _varioSettlement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVarioSettlement(IEntityCore relatedEntity)
		{
			if(_varioSettlement!=relatedEntity)
			{		
				DesetupSyncVarioSettlement(true, true);
				_varioSettlement = (VarioSettlementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _varioSettlement, new PropertyChangedEventHandler( OnVarioSettlementPropertyChanged ), "VarioSettlement", VarioSL.Entities.RelationClasses.StaticPostingRelations.VarioSettlementEntityUsingAccountSettlementIdStatic, true, ref _alreadyFetchedVarioSettlement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVarioSettlementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPostingRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticPostingRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoice</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticPostingRelations.InvoiceEntityUsingInvoiceIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.InvoiceID } );		
			_invoice = null;
		}
		
		/// <summary> setups the sync logic for member _invoice</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoice(IEntityCore relatedEntity)
		{
			if(_invoice!=relatedEntity)
			{		
				DesetupSyncInvoice(true, true);
				_invoice = (InvoiceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoice, new PropertyChangedEventHandler( OnInvoicePropertyChanged ), "Invoice", VarioSL.Entities.RelationClasses.StaticPostingRelations.InvoiceEntityUsingInvoiceIDStatic, true, ref _alreadyFetchedInvoice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoiceEntry</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoiceEntry(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoiceEntry, new PropertyChangedEventHandler( OnInvoiceEntryPropertyChanged ), "InvoiceEntry", VarioSL.Entities.RelationClasses.StaticPostingRelations.InvoiceEntryEntityUsingInvoiceEntryIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.InvoiceEntryID } );		
			_invoiceEntry = null;
		}
		
		/// <summary> setups the sync logic for member _invoiceEntry</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoiceEntry(IEntityCore relatedEntity)
		{
			if(_invoiceEntry!=relatedEntity)
			{		
				DesetupSyncInvoiceEntry(true, true);
				_invoiceEntry = (InvoiceEntryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoiceEntry, new PropertyChangedEventHandler( OnInvoiceEntryPropertyChanged ), "InvoiceEntry", VarioSL.Entities.RelationClasses.StaticPostingRelations.InvoiceEntryEntityUsingInvoiceEntryIDStatic, true, ref _alreadyFetchedInvoiceEntry, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoiceEntryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _postingKey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPostingKey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _postingKey, new PropertyChangedEventHandler( OnPostingKeyPropertyChanged ), "PostingKey", VarioSL.Entities.RelationClasses.StaticPostingRelations.PostingKeyEntityUsingPostingKeyIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.PostingKeyID } );		
			_postingKey = null;
		}
		
		/// <summary> setups the sync logic for member _postingKey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPostingKey(IEntityCore relatedEntity)
		{
			if(_postingKey!=relatedEntity)
			{		
				DesetupSyncPostingKey(true, true);
				_postingKey = (PostingKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _postingKey, new PropertyChangedEventHandler( OnPostingKeyPropertyChanged ), "PostingKey", VarioSL.Entities.RelationClasses.StaticPostingRelations.PostingKeyEntityUsingPostingKeyIDStatic, true, ref _alreadyFetchedPostingKey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPostingKeyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticPostingRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "Postings", resetFKFields, new int[] { (int)PostingFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticPostingRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="postingID">PK value for Posting which data should be fetched into this Posting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 postingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PostingFieldIndex.PostingID].ForcedCurrentValueWrite(postingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePostingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PostingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PostingRelations Relations
		{
			get	{ return new PostingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToPosting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToPostingCollection(), (IEntityRelation)GetRelationsForField("DunningToPostings")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.DunningToPostingEntity, 0, null, null, null, "DunningToPostings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningProcess'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningProcesses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DunningToPostingEntityUsingPostingID;
				intermediateRelation.SetAliases(string.Empty, "DunningToPosting_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningProcessCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.DunningProcessEntity, 0, null, null, GetRelationsForField("DunningProcesses"), "DunningProcesses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VarioSettlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVarioSettlement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VarioSettlementCollection(), (IEntityRelation)GetRelationsForField("VarioSettlement")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.VarioSettlementEntity, 0, null, null, null, "VarioSettlement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceCollection(), (IEntityRelation)GetRelationsForField("Invoice")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.InvoiceEntity, 0, null, null, null, "Invoice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InvoiceEntry'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoiceEntry
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceEntryCollection(), (IEntityRelation)GetRelationsForField("InvoiceEntry")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.InvoiceEntryEntity, 0, null, null, null, "InvoiceEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PostingKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostingKey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingKeyCollection(), (IEntityRelation)GetRelationsForField("PostingKey")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.PostingKeyEntity, 0, null, null, null, "PostingKey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.PostingEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountSettlementId property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."ACCSETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AccountSettlementId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.AccountSettlementId, false); }
			set	{ SetValue((int)PostingFieldIndex.AccountSettlementId, value, true); }
		}

		/// <summary> The CancelPostingID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."CANCELPOSTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancelPostingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.CancelPostingID, false); }
			set	{ SetValue((int)PostingFieldIndex.CancelPostingID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)PostingFieldIndex.ContractID, true); }
			set	{ SetValue((int)PostingFieldIndex.ContractID, value, true); }
		}

		/// <summary> The Description property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PostingFieldIndex.Description, true); }
			set	{ SetValue((int)PostingFieldIndex.Description, value, true); }
		}

		/// <summary> The InvoiceEntryID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."INVOICEENTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoiceEntryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.InvoiceEntryID, false); }
			set	{ SetValue((int)PostingFieldIndex.InvoiceEntryID, value, true); }
		}

		/// <summary> The InvoiceID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."INVOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoiceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.InvoiceID, false); }
			set	{ SetValue((int)PostingFieldIndex.InvoiceID, value, true); }
		}

		/// <summary> The LastModified property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PostingFieldIndex.LastModified, true); }
			set	{ SetValue((int)PostingFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PostingFieldIndex.LastUser, true); }
			set	{ SetValue((int)PostingFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PostingAmount property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."POSTINGAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PostingAmount
		{
			get { return (System.Int64)GetValue((int)PostingFieldIndex.PostingAmount, true); }
			set	{ SetValue((int)PostingFieldIndex.PostingAmount, value, true); }
		}

		/// <summary> The PostingDate property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."POSTINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PostingDate
		{
			get { return (System.DateTime)GetValue((int)PostingFieldIndex.PostingDate, true); }
			set	{ SetValue((int)PostingFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."POSTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PostingID
		{
			get { return (System.Int64)GetValue((int)PostingFieldIndex.PostingID, true); }
			set	{ SetValue((int)PostingFieldIndex.PostingID, value, true); }
		}

		/// <summary> The PostingKeyID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."POSTINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PostingKeyID
		{
			get { return (System.Int64)GetValue((int)PostingFieldIndex.PostingKeyID, true); }
			set	{ SetValue((int)PostingFieldIndex.PostingKeyID, value, true); }
		}

		/// <summary> The ProductID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.ProductID, false); }
			set	{ SetValue((int)PostingFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ReceiptNumber property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."RECEIPTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReceiptNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)PostingFieldIndex.ReceiptNumber, false); }
			set	{ SetValue((int)PostingFieldIndex.ReceiptNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PostingFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PostingFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UserID property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 UserID
		{
			get { return (System.Int64)GetValue((int)PostingFieldIndex.UserID, true); }
			set	{ SetValue((int)PostingFieldIndex.UserID, value, true); }
		}

		/// <summary> The ValueDateTime property of the Entity Posting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_POSTING"."VALUEDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValueDateTime
		{
			get { return (System.DateTime)GetValue((int)PostingFieldIndex.ValueDateTime, true); }
			set	{ SetValue((int)PostingFieldIndex.ValueDateTime, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DunningToPostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToPostingCollection DunningToPostings
		{
			get	{ return GetMultiDunningToPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToPostings. When set to true, DunningToPostings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToPostings is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToPostings
		{
			get	{ return _alwaysFetchDunningToPostings; }
			set	{ _alwaysFetchDunningToPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToPostings already has been fetched. Setting this property to false when DunningToPostings has been fetched
		/// will clear the DunningToPostings collection well. Setting this property to true while DunningToPostings hasn't been fetched disables lazy loading for DunningToPostings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToPostings
		{
			get { return _alreadyFetchedDunningToPostings;}
			set 
			{
				if(_alreadyFetchedDunningToPostings && !value && (_dunningToPostings != null))
				{
					_dunningToPostings.Clear();
				}
				_alreadyFetchedDunningToPostings = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningProcesses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningProcessCollection DunningProcesses
		{
			get { return GetMultiDunningProcesses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningProcesses. When set to true, DunningProcesses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningProcesses is accessed. You can always execute a forced fetch by calling GetMultiDunningProcesses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningProcesses
		{
			get	{ return _alwaysFetchDunningProcesses; }
			set	{ _alwaysFetchDunningProcesses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningProcesses already has been fetched. Setting this property to false when DunningProcesses has been fetched
		/// will clear the DunningProcesses collection well. Setting this property to true while DunningProcesses hasn't been fetched disables lazy loading for DunningProcesses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningProcesses
		{
			get { return _alreadyFetchedDunningProcesses;}
			set 
			{
				if(_alreadyFetchedDunningProcesses && !value && (_dunningProcesses != null))
				{
					_dunningProcesses.Clear();
				}
				_alreadyFetchedDunningProcesses = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VarioSettlementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVarioSettlement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VarioSettlementEntity VarioSettlement
		{
			get	{ return GetSingleVarioSettlement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVarioSettlement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "VarioSettlement", _varioSettlement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VarioSettlement. When set to true, VarioSettlement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VarioSettlement is accessed. You can always execute a forced fetch by calling GetSingleVarioSettlement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVarioSettlement
		{
			get	{ return _alwaysFetchVarioSettlement; }
			set	{ _alwaysFetchVarioSettlement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VarioSettlement already has been fetched. Setting this property to false when VarioSettlement has been fetched
		/// will set VarioSettlement to null as well. Setting this property to true while VarioSettlement hasn't been fetched disables lazy loading for VarioSettlement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVarioSettlement
		{
			get { return _alreadyFetchedVarioSettlement;}
			set 
			{
				if(_alreadyFetchedVarioSettlement && !value)
				{
					this.VarioSettlement = null;
				}
				_alreadyFetchedVarioSettlement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VarioSettlement is not found
		/// in the database. When set to true, VarioSettlement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VarioSettlementReturnsNewIfNotFound
		{
			get	{ return _varioSettlementReturnsNewIfNotFound; }
			set { _varioSettlementReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoiceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoiceEntity Invoice
		{
			get	{ return GetSingleInvoice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "Invoice", _invoice, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Invoice. When set to true, Invoice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoice is accessed. You can always execute a forced fetch by calling GetSingleInvoice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoice
		{
			get	{ return _alwaysFetchInvoice; }
			set	{ _alwaysFetchInvoice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoice already has been fetched. Setting this property to false when Invoice has been fetched
		/// will set Invoice to null as well. Setting this property to true while Invoice hasn't been fetched disables lazy loading for Invoice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoice
		{
			get { return _alreadyFetchedInvoice;}
			set 
			{
				if(_alreadyFetchedInvoice && !value)
				{
					this.Invoice = null;
				}
				_alreadyFetchedInvoice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Invoice is not found
		/// in the database. When set to true, Invoice will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoiceReturnsNewIfNotFound
		{
			get	{ return _invoiceReturnsNewIfNotFound; }
			set { _invoiceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoiceEntryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoiceEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoiceEntryEntity InvoiceEntry
		{
			get	{ return GetSingleInvoiceEntry(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoiceEntry(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "InvoiceEntry", _invoiceEntry, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InvoiceEntry. When set to true, InvoiceEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InvoiceEntry is accessed. You can always execute a forced fetch by calling GetSingleInvoiceEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoiceEntry
		{
			get	{ return _alwaysFetchInvoiceEntry; }
			set	{ _alwaysFetchInvoiceEntry = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InvoiceEntry already has been fetched. Setting this property to false when InvoiceEntry has been fetched
		/// will set InvoiceEntry to null as well. Setting this property to true while InvoiceEntry hasn't been fetched disables lazy loading for InvoiceEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoiceEntry
		{
			get { return _alreadyFetchedInvoiceEntry;}
			set 
			{
				if(_alreadyFetchedInvoiceEntry && !value)
				{
					this.InvoiceEntry = null;
				}
				_alreadyFetchedInvoiceEntry = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InvoiceEntry is not found
		/// in the database. When set to true, InvoiceEntry will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoiceEntryReturnsNewIfNotFound
		{
			get	{ return _invoiceEntryReturnsNewIfNotFound; }
			set { _invoiceEntryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PostingKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePostingKey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PostingKeyEntity PostingKey
		{
			get	{ return GetSinglePostingKey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPostingKey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "PostingKey", _postingKey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PostingKey. When set to true, PostingKey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PostingKey is accessed. You can always execute a forced fetch by calling GetSinglePostingKey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostingKey
		{
			get	{ return _alwaysFetchPostingKey; }
			set	{ _alwaysFetchPostingKey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PostingKey already has been fetched. Setting this property to false when PostingKey has been fetched
		/// will set PostingKey to null as well. Setting this property to true while PostingKey hasn't been fetched disables lazy loading for PostingKey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostingKey
		{
			get { return _alreadyFetchedPostingKey;}
			set 
			{
				if(_alreadyFetchedPostingKey && !value)
				{
					this.PostingKey = null;
				}
				_alreadyFetchedPostingKey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PostingKey is not found
		/// in the database. When set to true, PostingKey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PostingKeyReturnsNewIfNotFound
		{
			get	{ return _postingKeyReturnsNewIfNotFound; }
			set { _postingKeyReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Postings", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PostingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
