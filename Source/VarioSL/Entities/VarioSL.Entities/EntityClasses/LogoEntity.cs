﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Logo'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class LogoEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AttributeValueCollection	_attributeValue;
		private bool	_alwaysFetchAttributeValue, _alreadyFetchedAttributeValue;
		private VarioSL.Entities.CollectionClasses.AttributeValueCollection	_attributeValue_;
		private bool	_alwaysFetchAttributeValue_, _alreadyFetchedAttributeValue_;
		private VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection	_fixedBitmapLayoutObjects;
		private bool	_alwaysFetchFixedBitmapLayoutObjects, _alreadyFetchedFixedBitmapLayoutObjects;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name AttributeValue</summary>
			public static readonly string AttributeValue = "AttributeValue";
			/// <summary>Member name AttributeValue_</summary>
			public static readonly string AttributeValue_ = "AttributeValue_";
			/// <summary>Member name FixedBitmapLayoutObjects</summary>
			public static readonly string FixedBitmapLayoutObjects = "FixedBitmapLayoutObjects";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LogoEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public LogoEntity() :base("LogoEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		public LogoEntity(System.Int64 logoID):base("LogoEntity")
		{
			InitClassFetch(logoID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LogoEntity(System.Int64 logoID, IPrefetchPath prefetchPathToUse):base("LogoEntity")
		{
			InitClassFetch(logoID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="validator">The custom validator object for this LogoEntity</param>
		public LogoEntity(System.Int64 logoID, IValidator validator):base("LogoEntity")
		{
			InitClassFetch(logoID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LogoEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeValue = (VarioSL.Entities.CollectionClasses.AttributeValueCollection)info.GetValue("_attributeValue", typeof(VarioSL.Entities.CollectionClasses.AttributeValueCollection));
			_alwaysFetchAttributeValue = info.GetBoolean("_alwaysFetchAttributeValue");
			_alreadyFetchedAttributeValue = info.GetBoolean("_alreadyFetchedAttributeValue");

			_attributeValue_ = (VarioSL.Entities.CollectionClasses.AttributeValueCollection)info.GetValue("_attributeValue_", typeof(VarioSL.Entities.CollectionClasses.AttributeValueCollection));
			_alwaysFetchAttributeValue_ = info.GetBoolean("_alwaysFetchAttributeValue_");
			_alreadyFetchedAttributeValue_ = info.GetBoolean("_alreadyFetchedAttributeValue_");

			_fixedBitmapLayoutObjects = (VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection)info.GetValue("_fixedBitmapLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection));
			_alwaysFetchFixedBitmapLayoutObjects = info.GetBoolean("_alwaysFetchFixedBitmapLayoutObjects");
			_alreadyFetchedFixedBitmapLayoutObjects = info.GetBoolean("_alreadyFetchedFixedBitmapLayoutObjects");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LogoFieldIndex)fieldIndex)
			{
				case LogoFieldIndex.TariffId:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeValue = (_attributeValue.Count > 0);
			_alreadyFetchedAttributeValue_ = (_attributeValue_.Count > 0);
			_alreadyFetchedFixedBitmapLayoutObjects = (_fixedBitmapLayoutObjects.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffId);
					break;
				case "AttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingLogo1ID);
					break;
				case "AttributeValue_":
					toReturn.Add(Relations.AttributeValueEntityUsingLogo2ID);
					break;
				case "FixedBitmapLayoutObjects":
					toReturn.Add(Relations.FixedBitmapLayoutObjectEntityUsingLogoID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeValue", (!this.MarkedForDeletion?_attributeValue:null));
			info.AddValue("_alwaysFetchAttributeValue", _alwaysFetchAttributeValue);
			info.AddValue("_alreadyFetchedAttributeValue", _alreadyFetchedAttributeValue);
			info.AddValue("_attributeValue_", (!this.MarkedForDeletion?_attributeValue_:null));
			info.AddValue("_alwaysFetchAttributeValue_", _alwaysFetchAttributeValue_);
			info.AddValue("_alreadyFetchedAttributeValue_", _alreadyFetchedAttributeValue_);
			info.AddValue("_fixedBitmapLayoutObjects", (!this.MarkedForDeletion?_fixedBitmapLayoutObjects:null));
			info.AddValue("_alwaysFetchFixedBitmapLayoutObjects", _alwaysFetchFixedBitmapLayoutObjects);
			info.AddValue("_alreadyFetchedFixedBitmapLayoutObjects", _alreadyFetchedFixedBitmapLayoutObjects);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "AttributeValue":
					_alreadyFetchedAttributeValue = true;
					if(entity!=null)
					{
						this.AttributeValue.Add((AttributeValueEntity)entity);
					}
					break;
				case "AttributeValue_":
					_alreadyFetchedAttributeValue_ = true;
					if(entity!=null)
					{
						this.AttributeValue_.Add((AttributeValueEntity)entity);
					}
					break;
				case "FixedBitmapLayoutObjects":
					_alreadyFetchedFixedBitmapLayoutObjects = true;
					if(entity!=null)
					{
						this.FixedBitmapLayoutObjects.Add((FixedBitmapLayoutObjectEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "AttributeValue":
					_attributeValue.Add((AttributeValueEntity)relatedEntity);
					break;
				case "AttributeValue_":
					_attributeValue_.Add((AttributeValueEntity)relatedEntity);
					break;
				case "FixedBitmapLayoutObjects":
					_fixedBitmapLayoutObjects.Add((FixedBitmapLayoutObjectEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "AttributeValue":
					this.PerformRelatedEntityRemoval(_attributeValue, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeValue_":
					this.PerformRelatedEntityRemoval(_attributeValue_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FixedBitmapLayoutObjects":
					this.PerformRelatedEntityRemoval(_fixedBitmapLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attributeValue);
			toReturn.Add(_attributeValue_);
			toReturn.Add(_fixedBitmapLayoutObjects);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 logoID)
		{
			return FetchUsingPK(logoID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 logoID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(logoID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 logoID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(logoID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 logoID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(logoID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LogoID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LogoRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue(bool forceFetch)
		{
			return GetMultiAttributeValue(forceFetch, _attributeValue.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeValue(forceFetch, _attributeValue.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeValue(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeValue || forceFetch || _alwaysFetchAttributeValue) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeValue);
				_attributeValue.SuppressClearInGetMulti=!forceFetch;
				_attributeValue.EntityFactoryToUse = entityFactoryToUse;
				_attributeValue.GetMultiManyToOne(null, this, null, filter);
				_attributeValue.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeValue = true;
			}
			return _attributeValue;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeValue'. These settings will be taken into account
		/// when the property AttributeValue is requested or GetMultiAttributeValue is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeValue(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeValue.SortClauses=sortClauses;
			_attributeValue.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue_(bool forceFetch)
		{
			return GetMultiAttributeValue_(forceFetch, _attributeValue_.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue_(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeValue_(forceFetch, _attributeValue_.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeValue_(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection GetMultiAttributeValue_(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeValue_ || forceFetch || _alwaysFetchAttributeValue_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeValue_);
				_attributeValue_.SuppressClearInGetMulti=!forceFetch;
				_attributeValue_.EntityFactoryToUse = entityFactoryToUse;
				_attributeValue_.GetMultiManyToOne(null, null, this, filter);
				_attributeValue_.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeValue_ = true;
			}
			return _attributeValue_;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeValue_'. These settings will be taken into account
		/// when the property AttributeValue_ is requested or GetMultiAttributeValue_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeValue_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeValue_.SortClauses=sortClauses;
			_attributeValue_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FixedBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, _fixedBitmapLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FixedBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, _fixedBitmapLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFixedBitmapLayoutObjects || forceFetch || _alwaysFetchFixedBitmapLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fixedBitmapLayoutObjects);
				_fixedBitmapLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_fixedBitmapLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_fixedBitmapLayoutObjects.GetMultiManyToOne(null, this, filter);
				_fixedBitmapLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedFixedBitmapLayoutObjects = true;
			}
			return _fixedBitmapLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'FixedBitmapLayoutObjects'. These settings will be taken into account
		/// when the property FixedBitmapLayoutObjects is requested or GetMultiFixedBitmapLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFixedBitmapLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fixedBitmapLayoutObjects.SortClauses=sortClauses;
			_fixedBitmapLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffId);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("AttributeValue", _attributeValue);
			toReturn.Add("AttributeValue_", _attributeValue_);
			toReturn.Add("FixedBitmapLayoutObjects", _fixedBitmapLayoutObjects);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="validator">The validator object for this LogoEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 logoID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(logoID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attributeValue = new VarioSL.Entities.CollectionClasses.AttributeValueCollection();
			_attributeValue.SetContainingEntityInfo(this, "Logo1");

			_attributeValue_ = new VarioSL.Entities.CollectionClasses.AttributeValueCollection();
			_attributeValue_.SetContainingEntityInfo(this, "Logo2");

			_fixedBitmapLayoutObjects = new VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection();
			_fixedBitmapLayoutObjects.SetContainingEntityInfo(this, "Logo");
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Bitmap", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LogoID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalImage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLogoRelations.TariffEntityUsingTariffIdStatic, true, signalRelatedEntity, "Logos", resetFKFields, new int[] { (int)LogoFieldIndex.TariffId } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLogoRelations.TariffEntityUsingTariffIdStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="logoID">PK value for Logo which data should be fetched into this Logo object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 logoID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LogoFieldIndex.LogoID].ForcedCurrentValueWrite(logoID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLogoDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LogoEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LogoRelations Relations
		{
			get	{ return new LogoRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValue
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValue")[0], (int)VarioSL.Entities.EntityType.LogoEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValue_
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValue_")[0], (int)VarioSL.Entities.EntityType.LogoEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValue_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FixedBitmapLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFixedBitmapLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("FixedBitmapLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LogoEntity, (int)VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity, 0, null, null, null, "FixedBitmapLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.LogoEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Bitmap property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."BITMAP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] Bitmap
		{
			get { return (System.Byte[])GetValue((int)LogoFieldIndex.Bitmap, true); }
			set	{ SetValue((int)LogoFieldIndex.Bitmap, value, true); }
		}

		/// <summary> The Description property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)LogoFieldIndex.Description, true); }
			set	{ SetValue((int)LogoFieldIndex.Description, value, true); }
		}

		/// <summary> The LogoID property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."LOGOID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LogoID
		{
			get { return (System.Int64)GetValue((int)LogoFieldIndex.LogoID, true); }
			set	{ SetValue((int)LogoFieldIndex.LogoID, value, true); }
		}

		/// <summary> The Name property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)LogoFieldIndex.Name, true); }
			set	{ SetValue((int)LogoFieldIndex.Name, value, true); }
		}

		/// <summary> The OriginalImage property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."ORIGINALIMAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] OriginalImage
		{
			get { return (System.Byte[])GetValue((int)LogoFieldIndex.OriginalImage, true); }
			set	{ SetValue((int)LogoFieldIndex.OriginalImage, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)LogoFieldIndex.OwnerClientID, false); }
			set	{ SetValue((int)LogoFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The TariffId property of the Entity Logo<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LOGO"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffId
		{
			get { return (Nullable<System.Int64>)GetValue((int)LogoFieldIndex.TariffId, false); }
			set	{ SetValue((int)LogoFieldIndex.TariffId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection AttributeValue
		{
			get	{ return GetMultiAttributeValue(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValue. When set to true, AttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValue is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValue
		{
			get	{ return _alwaysFetchAttributeValue; }
			set	{ _alwaysFetchAttributeValue = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValue already has been fetched. Setting this property to false when AttributeValue has been fetched
		/// will clear the AttributeValue collection well. Setting this property to true while AttributeValue hasn't been fetched disables lazy loading for AttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValue
		{
			get { return _alreadyFetchedAttributeValue;}
			set 
			{
				if(_alreadyFetchedAttributeValue && !value && (_attributeValue != null))
				{
					_attributeValue.Clear();
				}
				_alreadyFetchedAttributeValue = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeValue_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeValueCollection AttributeValue_
		{
			get	{ return GetMultiAttributeValue_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValue_. When set to true, AttributeValue_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValue_ is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeValue_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValue_
		{
			get	{ return _alwaysFetchAttributeValue_; }
			set	{ _alwaysFetchAttributeValue_ = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValue_ already has been fetched. Setting this property to false when AttributeValue_ has been fetched
		/// will clear the AttributeValue_ collection well. Setting this property to true while AttributeValue_ hasn't been fetched disables lazy loading for AttributeValue_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValue_
		{
			get { return _alreadyFetchedAttributeValue_;}
			set 
			{
				if(_alreadyFetchedAttributeValue_ && !value && (_attributeValue_ != null))
				{
					_attributeValue_.Clear();
				}
				_alreadyFetchedAttributeValue_ = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFixedBitmapLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection FixedBitmapLayoutObjects
		{
			get	{ return GetMultiFixedBitmapLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FixedBitmapLayoutObjects. When set to true, FixedBitmapLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FixedBitmapLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiFixedBitmapLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFixedBitmapLayoutObjects
		{
			get	{ return _alwaysFetchFixedBitmapLayoutObjects; }
			set	{ _alwaysFetchFixedBitmapLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FixedBitmapLayoutObjects already has been fetched. Setting this property to false when FixedBitmapLayoutObjects has been fetched
		/// will clear the FixedBitmapLayoutObjects collection well. Setting this property to true while FixedBitmapLayoutObjects hasn't been fetched disables lazy loading for FixedBitmapLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFixedBitmapLayoutObjects
		{
			get { return _alreadyFetchedFixedBitmapLayoutObjects;}
			set 
			{
				if(_alreadyFetchedFixedBitmapLayoutObjects && !value && (_fixedBitmapLayoutObjects != null))
				{
					_fixedBitmapLayoutObjects.Clear();
				}
				_alreadyFetchedFixedBitmapLayoutObjects = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Logos", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.LogoEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
