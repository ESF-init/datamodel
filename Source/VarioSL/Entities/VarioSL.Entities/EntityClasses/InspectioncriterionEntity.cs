﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'InspectionCriterion'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class InspectionCriterionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private InspectionReportEntity _inspectionReport;
		private bool	_alwaysFetchInspectionReport, _alreadyFetchedInspectionReport, _inspectionReportReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name InspectionReport</summary>
			public static readonly string InspectionReport = "InspectionReport";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static InspectionCriterionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public InspectionCriterionEntity() :base("InspectionCriterionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		public InspectionCriterionEntity(System.Int64 inspectionCriterionID):base("InspectionCriterionEntity")
		{
			InitClassFetch(inspectionCriterionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public InspectionCriterionEntity(System.Int64 inspectionCriterionID, IPrefetchPath prefetchPathToUse):base("InspectionCriterionEntity")
		{
			InitClassFetch(inspectionCriterionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="validator">The custom validator object for this InspectionCriterionEntity</param>
		public InspectionCriterionEntity(System.Int64 inspectionCriterionID, IValidator validator):base("InspectionCriterionEntity")
		{
			InitClassFetch(inspectionCriterionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InspectionCriterionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_inspectionReport = (InspectionReportEntity)info.GetValue("_inspectionReport", typeof(InspectionReportEntity));
			if(_inspectionReport!=null)
			{
				_inspectionReport.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_inspectionReportReturnsNewIfNotFound = info.GetBoolean("_inspectionReportReturnsNewIfNotFound");
			_alwaysFetchInspectionReport = info.GetBoolean("_alwaysFetchInspectionReport");
			_alreadyFetchedInspectionReport = info.GetBoolean("_alreadyFetchedInspectionReport");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((InspectionCriterionFieldIndex)fieldIndex)
			{
				case InspectionCriterionFieldIndex.InspectionReportID:
					DesetupSyncInspectionReport(true, false);
					_alreadyFetchedInspectionReport = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedInspectionReport = (_inspectionReport != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "InspectionReport":
					toReturn.Add(Relations.InspectionReportEntityUsingInspectionReportID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_inspectionReport", (!this.MarkedForDeletion?_inspectionReport:null));
			info.AddValue("_inspectionReportReturnsNewIfNotFound", _inspectionReportReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInspectionReport", _alwaysFetchInspectionReport);
			info.AddValue("_alreadyFetchedInspectionReport", _alreadyFetchedInspectionReport);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "InspectionReport":
					_alreadyFetchedInspectionReport = true;
					this.InspectionReport = (InspectionReportEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "InspectionReport":
					SetupSyncInspectionReport(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "InspectionReport":
					DesetupSyncInspectionReport(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_inspectionReport!=null)
			{
				toReturn.Add(_inspectionReport);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionCriterionID)
		{
			return FetchUsingPK(inspectionCriterionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionCriterionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(inspectionCriterionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionCriterionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(inspectionCriterionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionCriterionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(inspectionCriterionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.InspectionCriterionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new InspectionCriterionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'InspectionReportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InspectionReportEntity' which is related to this entity.</returns>
		public InspectionReportEntity GetSingleInspectionReport()
		{
			return GetSingleInspectionReport(false);
		}

		/// <summary> Retrieves the related entity of type 'InspectionReportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InspectionReportEntity' which is related to this entity.</returns>
		public virtual InspectionReportEntity GetSingleInspectionReport(bool forceFetch)
		{
			if( ( !_alreadyFetchedInspectionReport || forceFetch || _alwaysFetchInspectionReport) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InspectionReportEntityUsingInspectionReportID);
				InspectionReportEntity newEntity = new InspectionReportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InspectionReportID);
				}
				if(fetchResult)
				{
					newEntity = (InspectionReportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_inspectionReportReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InspectionReport = newEntity;
				_alreadyFetchedInspectionReport = fetchResult;
			}
			return _inspectionReport;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("InspectionReport", _inspectionReport);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="validator">The validator object for this InspectionCriterionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 inspectionCriterionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(inspectionCriterionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_inspectionReportReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Criterion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CriterionComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionCriterionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionResult", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _inspectionReport</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInspectionReport(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _inspectionReport, new PropertyChangedEventHandler( OnInspectionReportPropertyChanged ), "InspectionReport", VarioSL.Entities.RelationClasses.StaticInspectionCriterionRelations.InspectionReportEntityUsingInspectionReportIDStatic, true, signalRelatedEntity, "SlInspectioncriterions", resetFKFields, new int[] { (int)InspectionCriterionFieldIndex.InspectionReportID } );		
			_inspectionReport = null;
		}
		
		/// <summary> setups the sync logic for member _inspectionReport</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInspectionReport(IEntityCore relatedEntity)
		{
			if(_inspectionReport!=relatedEntity)
			{		
				DesetupSyncInspectionReport(true, true);
				_inspectionReport = (InspectionReportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _inspectionReport, new PropertyChangedEventHandler( OnInspectionReportPropertyChanged ), "InspectionReport", VarioSL.Entities.RelationClasses.StaticInspectionCriterionRelations.InspectionReportEntityUsingInspectionReportIDStatic, true, ref _alreadyFetchedInspectionReport, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInspectionReportPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="inspectionCriterionID">PK value for InspectionCriterion which data should be fetched into this InspectionCriterion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 inspectionCriterionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)InspectionCriterionFieldIndex.InspectionCriterionID].ForcedCurrentValueWrite(inspectionCriterionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateInspectionCriterionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new InspectionCriterionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static InspectionCriterionRelations Relations
		{
			get	{ return new InspectionCriterionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InspectionReport'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInspectionReport
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InspectionReportCollection(), (IEntityRelation)GetRelationsForField("InspectionReport")[0], (int)VarioSL.Entities.EntityType.InspectionCriterionEntity, (int)VarioSL.Entities.EntityType.InspectionReportEntity, 0, null, null, null, "InspectionReport", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Criterion property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."CRITERION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Criterion
		{
			get { return (System.String)GetValue((int)InspectionCriterionFieldIndex.Criterion, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.Criterion, value, true); }
		}

		/// <summary> The CriterionComment property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."CRITERIONCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CriterionComment
		{
			get { return (System.String)GetValue((int)InspectionCriterionFieldIndex.CriterionComment, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.CriterionComment, value, true); }
		}

		/// <summary> The InspectionCriterionID property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."INSPECTIONCRITERIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 InspectionCriterionID
		{
			get { return (System.Int64)GetValue((int)InspectionCriterionFieldIndex.InspectionCriterionID, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.InspectionCriterionID, value, true); }
		}

		/// <summary> The InspectionReportID property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."INSPECTIONREPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InspectionReportID
		{
			get { return (System.Int64)GetValue((int)InspectionCriterionFieldIndex.InspectionReportID, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.InspectionReportID, value, true); }
		}

		/// <summary> The InspectionResult property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."INSPECTIONRESULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.InspectionResult InspectionResult
		{
			get { return (VarioSL.Entities.Enumerations.InspectionResult)GetValue((int)InspectionCriterionFieldIndex.InspectionResult, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.InspectionResult, value, true); }
		}

		/// <summary> The LastModified property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)InspectionCriterionFieldIndex.LastModified, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)InspectionCriterionFieldIndex.LastUser, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity InspectionCriterion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONCRITERION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)InspectionCriterionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)InspectionCriterionFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'InspectionReportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInspectionReport()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InspectionReportEntity InspectionReport
		{
			get	{ return GetSingleInspectionReport(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInspectionReport(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SlInspectioncriterions", "InspectionReport", _inspectionReport, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InspectionReport. When set to true, InspectionReport is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InspectionReport is accessed. You can always execute a forced fetch by calling GetSingleInspectionReport(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInspectionReport
		{
			get	{ return _alwaysFetchInspectionReport; }
			set	{ _alwaysFetchInspectionReport = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InspectionReport already has been fetched. Setting this property to false when InspectionReport has been fetched
		/// will set InspectionReport to null as well. Setting this property to true while InspectionReport hasn't been fetched disables lazy loading for InspectionReport</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInspectionReport
		{
			get { return _alreadyFetchedInspectionReport;}
			set 
			{
				if(_alreadyFetchedInspectionReport && !value)
				{
					this.InspectionReport = null;
				}
				_alreadyFetchedInspectionReport = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InspectionReport is not found
		/// in the database. When set to true, InspectionReport will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InspectionReportReturnsNewIfNotFound
		{
			get	{ return _inspectionReportReturnsNewIfNotFound; }
			set { _inspectionReportReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.InspectionCriterionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
