﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BookingItem'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BookingItemEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.BookingItemCollection	_bookingItems;
		private bool	_alwaysFetchBookingItems, _alreadyFetchedBookingItems;
		private VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection	_bookingItemPayments;
		private bool	_alwaysFetchBookingItemPayments, _alreadyFetchedBookingItemPayments;
		private VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection	_emailEventResendLocks;
		private bool	_alwaysFetchEmailEventResendLocks, _alreadyFetchedEmailEventResendLocks;
		private BookingEntity _booking;
		private bool	_alwaysFetchBooking, _alreadyFetchedBooking, _bookingReturnsNewIfNotFound;
		private BookingItemEntity _bookingItem;
		private bool	_alwaysFetchBookingItem, _alreadyFetchedBookingItem, _bookingItemReturnsNewIfNotFound;
		private MobilityProviderEntity _mobilityProvider;
		private bool	_alwaysFetchMobilityProvider, _alreadyFetchedMobilityProvider, _mobilityProviderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Booking</summary>
			public static readonly string Booking = "Booking";
			/// <summary>Member name BookingItem</summary>
			public static readonly string BookingItem = "BookingItem";
			/// <summary>Member name MobilityProvider</summary>
			public static readonly string MobilityProvider = "MobilityProvider";
			/// <summary>Member name BookingItems</summary>
			public static readonly string BookingItems = "BookingItems";
			/// <summary>Member name BookingItemPayments</summary>
			public static readonly string BookingItemPayments = "BookingItemPayments";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BookingItemEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BookingItemEntity() :base("BookingItemEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		public BookingItemEntity(System.Int64 bookingItemID):base("BookingItemEntity")
		{
			InitClassFetch(bookingItemID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BookingItemEntity(System.Int64 bookingItemID, IPrefetchPath prefetchPathToUse):base("BookingItemEntity")
		{
			InitClassFetch(bookingItemID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="validator">The custom validator object for this BookingItemEntity</param>
		public BookingItemEntity(System.Int64 bookingItemID, IValidator validator):base("BookingItemEntity")
		{
			InitClassFetch(bookingItemID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BookingItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_bookingItems = (VarioSL.Entities.CollectionClasses.BookingItemCollection)info.GetValue("_bookingItems", typeof(VarioSL.Entities.CollectionClasses.BookingItemCollection));
			_alwaysFetchBookingItems = info.GetBoolean("_alwaysFetchBookingItems");
			_alreadyFetchedBookingItems = info.GetBoolean("_alreadyFetchedBookingItems");

			_bookingItemPayments = (VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection)info.GetValue("_bookingItemPayments", typeof(VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection));
			_alwaysFetchBookingItemPayments = info.GetBoolean("_alwaysFetchBookingItemPayments");
			_alreadyFetchedBookingItemPayments = info.GetBoolean("_alreadyFetchedBookingItemPayments");

			_emailEventResendLocks = (VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection)info.GetValue("_emailEventResendLocks", typeof(VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection));
			_alwaysFetchEmailEventResendLocks = info.GetBoolean("_alwaysFetchEmailEventResendLocks");
			_alreadyFetchedEmailEventResendLocks = info.GetBoolean("_alreadyFetchedEmailEventResendLocks");
			_booking = (BookingEntity)info.GetValue("_booking", typeof(BookingEntity));
			if(_booking!=null)
			{
				_booking.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bookingReturnsNewIfNotFound = info.GetBoolean("_bookingReturnsNewIfNotFound");
			_alwaysFetchBooking = info.GetBoolean("_alwaysFetchBooking");
			_alreadyFetchedBooking = info.GetBoolean("_alreadyFetchedBooking");

			_bookingItem = (BookingItemEntity)info.GetValue("_bookingItem", typeof(BookingItemEntity));
			if(_bookingItem!=null)
			{
				_bookingItem.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bookingItemReturnsNewIfNotFound = info.GetBoolean("_bookingItemReturnsNewIfNotFound");
			_alwaysFetchBookingItem = info.GetBoolean("_alwaysFetchBookingItem");
			_alreadyFetchedBookingItem = info.GetBoolean("_alreadyFetchedBookingItem");

			_mobilityProvider = (MobilityProviderEntity)info.GetValue("_mobilityProvider", typeof(MobilityProviderEntity));
			if(_mobilityProvider!=null)
			{
				_mobilityProvider.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mobilityProviderReturnsNewIfNotFound = info.GetBoolean("_mobilityProviderReturnsNewIfNotFound");
			_alwaysFetchMobilityProvider = info.GetBoolean("_alwaysFetchMobilityProvider");
			_alreadyFetchedMobilityProvider = info.GetBoolean("_alreadyFetchedMobilityProvider");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BookingItemFieldIndex)fieldIndex)
			{
				case BookingItemFieldIndex.BookingID:
					DesetupSyncBooking(true, false);
					_alreadyFetchedBooking = false;
					break;
				case BookingItemFieldIndex.CancellationID:
					DesetupSyncBookingItem(true, false);
					_alreadyFetchedBookingItem = false;
					break;
				case BookingItemFieldIndex.MobilityProviderID:
					DesetupSyncMobilityProvider(true, false);
					_alreadyFetchedMobilityProvider = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBookingItems = (_bookingItems.Count > 0);
			_alreadyFetchedBookingItemPayments = (_bookingItemPayments.Count > 0);
			_alreadyFetchedEmailEventResendLocks = (_emailEventResendLocks.Count > 0);
			_alreadyFetchedBooking = (_booking != null);
			_alreadyFetchedBookingItem = (_bookingItem != null);
			_alreadyFetchedMobilityProvider = (_mobilityProvider != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Booking":
					toReturn.Add(Relations.BookingEntityUsingBookingID);
					break;
				case "BookingItem":
					toReturn.Add(Relations.BookingItemEntityUsingBookingItemIDCancellationID);
					break;
				case "MobilityProvider":
					toReturn.Add(Relations.MobilityProviderEntityUsingMobilityProviderID);
					break;
				case "BookingItems":
					toReturn.Add(Relations.BookingItemEntityUsingCancellationID);
					break;
				case "BookingItemPayments":
					toReturn.Add(Relations.BookingItemPaymentEntityUsingBookingItemID);
					break;
				case "EmailEventResendLocks":
					toReturn.Add(Relations.EmailEventResendLockEntityUsingBookingItemID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_bookingItems", (!this.MarkedForDeletion?_bookingItems:null));
			info.AddValue("_alwaysFetchBookingItems", _alwaysFetchBookingItems);
			info.AddValue("_alreadyFetchedBookingItems", _alreadyFetchedBookingItems);
			info.AddValue("_bookingItemPayments", (!this.MarkedForDeletion?_bookingItemPayments:null));
			info.AddValue("_alwaysFetchBookingItemPayments", _alwaysFetchBookingItemPayments);
			info.AddValue("_alreadyFetchedBookingItemPayments", _alreadyFetchedBookingItemPayments);
			info.AddValue("_emailEventResendLocks", (!this.MarkedForDeletion?_emailEventResendLocks:null));
			info.AddValue("_alwaysFetchEmailEventResendLocks", _alwaysFetchEmailEventResendLocks);
			info.AddValue("_alreadyFetchedEmailEventResendLocks", _alreadyFetchedEmailEventResendLocks);
			info.AddValue("_booking", (!this.MarkedForDeletion?_booking:null));
			info.AddValue("_bookingReturnsNewIfNotFound", _bookingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBooking", _alwaysFetchBooking);
			info.AddValue("_alreadyFetchedBooking", _alreadyFetchedBooking);
			info.AddValue("_bookingItem", (!this.MarkedForDeletion?_bookingItem:null));
			info.AddValue("_bookingItemReturnsNewIfNotFound", _bookingItemReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBookingItem", _alwaysFetchBookingItem);
			info.AddValue("_alreadyFetchedBookingItem", _alreadyFetchedBookingItem);
			info.AddValue("_mobilityProvider", (!this.MarkedForDeletion?_mobilityProvider:null));
			info.AddValue("_mobilityProviderReturnsNewIfNotFound", _mobilityProviderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMobilityProvider", _alwaysFetchMobilityProvider);
			info.AddValue("_alreadyFetchedMobilityProvider", _alreadyFetchedMobilityProvider);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Booking":
					_alreadyFetchedBooking = true;
					this.Booking = (BookingEntity)entity;
					break;
				case "BookingItem":
					_alreadyFetchedBookingItem = true;
					this.BookingItem = (BookingItemEntity)entity;
					break;
				case "MobilityProvider":
					_alreadyFetchedMobilityProvider = true;
					this.MobilityProvider = (MobilityProviderEntity)entity;
					break;
				case "BookingItems":
					_alreadyFetchedBookingItems = true;
					if(entity!=null)
					{
						this.BookingItems.Add((BookingItemEntity)entity);
					}
					break;
				case "BookingItemPayments":
					_alreadyFetchedBookingItemPayments = true;
					if(entity!=null)
					{
						this.BookingItemPayments.Add((BookingItemPaymentEntity)entity);
					}
					break;
				case "EmailEventResendLocks":
					_alreadyFetchedEmailEventResendLocks = true;
					if(entity!=null)
					{
						this.EmailEventResendLocks.Add((EmailEventResendLockEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Booking":
					SetupSyncBooking(relatedEntity);
					break;
				case "BookingItem":
					SetupSyncBookingItem(relatedEntity);
					break;
				case "MobilityProvider":
					SetupSyncMobilityProvider(relatedEntity);
					break;
				case "BookingItems":
					_bookingItems.Add((BookingItemEntity)relatedEntity);
					break;
				case "BookingItemPayments":
					_bookingItemPayments.Add((BookingItemPaymentEntity)relatedEntity);
					break;
				case "EmailEventResendLocks":
					_emailEventResendLocks.Add((EmailEventResendLockEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Booking":
					DesetupSyncBooking(false, true);
					break;
				case "BookingItem":
					DesetupSyncBookingItem(false, true);
					break;
				case "MobilityProvider":
					DesetupSyncMobilityProvider(false, true);
					break;
				case "BookingItems":
					this.PerformRelatedEntityRemoval(_bookingItems, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BookingItemPayments":
					this.PerformRelatedEntityRemoval(_bookingItemPayments, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailEventResendLocks":
					this.PerformRelatedEntityRemoval(_emailEventResendLocks, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_booking!=null)
			{
				toReturn.Add(_booking);
			}
			if(_bookingItem!=null)
			{
				toReturn.Add(_bookingItem);
			}
			if(_mobilityProvider!=null)
			{
				toReturn.Add(_mobilityProvider);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_bookingItems);
			toReturn.Add(_bookingItemPayments);
			toReturn.Add(_emailEventResendLocks);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemID)
		{
			return FetchUsingPK(bookingItemID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(bookingItemID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(bookingItemID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bookingItemID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(bookingItemID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BookingItemID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BookingItemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch)
		{
			return GetMultiBookingItems(forceFetch, _bookingItems.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBookingItems(forceFetch, _bookingItems.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBookingItems(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemCollection GetMultiBookingItems(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBookingItems || forceFetch || _alwaysFetchBookingItems) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bookingItems);
				_bookingItems.SuppressClearInGetMulti=!forceFetch;
				_bookingItems.EntityFactoryToUse = entityFactoryToUse;
				_bookingItems.GetMultiManyToOne(null, this, null, filter);
				_bookingItems.SuppressClearInGetMulti=false;
				_alreadyFetchedBookingItems = true;
			}
			return _bookingItems;
		}

		/// <summary> Sets the collection parameters for the collection for 'BookingItems'. These settings will be taken into account
		/// when the property BookingItems is requested or GetMultiBookingItems is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBookingItems(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bookingItems.SortClauses=sortClauses;
			_bookingItems.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BookingItemPaymentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemPaymentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection GetMultiBookingItemPayments(bool forceFetch)
		{
			return GetMultiBookingItemPayments(forceFetch, _bookingItemPayments.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemPaymentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BookingItemPaymentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection GetMultiBookingItemPayments(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBookingItemPayments(forceFetch, _bookingItemPayments.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemPaymentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection GetMultiBookingItemPayments(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBookingItemPayments(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BookingItemPaymentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection GetMultiBookingItemPayments(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBookingItemPayments || forceFetch || _alwaysFetchBookingItemPayments) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bookingItemPayments);
				_bookingItemPayments.SuppressClearInGetMulti=!forceFetch;
				_bookingItemPayments.EntityFactoryToUse = entityFactoryToUse;
				_bookingItemPayments.GetMultiManyToOne(this, filter);
				_bookingItemPayments.SuppressClearInGetMulti=false;
				_alreadyFetchedBookingItemPayments = true;
			}
			return _bookingItemPayments;
		}

		/// <summary> Sets the collection parameters for the collection for 'BookingItemPayments'. These settings will be taken into account
		/// when the property BookingItemPayments is requested or GetMultiBookingItemPayments is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBookingItemPayments(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bookingItemPayments.SortClauses=sortClauses;
			_bookingItemPayments.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailEventResendLocks(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailEventResendLocks || forceFetch || _alwaysFetchEmailEventResendLocks) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventResendLocks);
				_emailEventResendLocks.SuppressClearInGetMulti=!forceFetch;
				_emailEventResendLocks.EntityFactoryToUse = entityFactoryToUse;
				_emailEventResendLocks.GetMultiManyToOne(null, this, null, null, null, filter);
				_emailEventResendLocks.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventResendLocks = true;
			}
			return _emailEventResendLocks;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventResendLocks'. These settings will be taken into account
		/// when the property EmailEventResendLocks is requested or GetMultiEmailEventResendLocks is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventResendLocks(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventResendLocks.SortClauses=sortClauses;
			_emailEventResendLocks.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'BookingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BookingEntity' which is related to this entity.</returns>
		public BookingEntity GetSingleBooking()
		{
			return GetSingleBooking(false);
		}

		/// <summary> Retrieves the related entity of type 'BookingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BookingEntity' which is related to this entity.</returns>
		public virtual BookingEntity GetSingleBooking(bool forceFetch)
		{
			if( ( !_alreadyFetchedBooking || forceFetch || _alwaysFetchBooking) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BookingEntityUsingBookingID);
				BookingEntity newEntity = new BookingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BookingID);
				}
				if(fetchResult)
				{
					newEntity = (BookingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bookingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Booking = newEntity;
				_alreadyFetchedBooking = fetchResult;
			}
			return _booking;
		}


		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public BookingItemEntity GetSingleBookingItem()
		{
			return GetSingleBookingItem(false);
		}

		/// <summary> Retrieves the related entity of type 'BookingItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BookingItemEntity' which is related to this entity.</returns>
		public virtual BookingItemEntity GetSingleBookingItem(bool forceFetch)
		{
			if( ( !_alreadyFetchedBookingItem || forceFetch || _alwaysFetchBookingItem) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BookingItemEntityUsingBookingItemIDCancellationID);
				BookingItemEntity newEntity = new BookingItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CancellationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BookingItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bookingItemReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BookingItem = newEntity;
				_alreadyFetchedBookingItem = fetchResult;
			}
			return _bookingItem;
		}


		/// <summary> Retrieves the related entity of type 'MobilityProviderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MobilityProviderEntity' which is related to this entity.</returns>
		public MobilityProviderEntity GetSingleMobilityProvider()
		{
			return GetSingleMobilityProvider(false);
		}

		/// <summary> Retrieves the related entity of type 'MobilityProviderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MobilityProviderEntity' which is related to this entity.</returns>
		public virtual MobilityProviderEntity GetSingleMobilityProvider(bool forceFetch)
		{
			if( ( !_alreadyFetchedMobilityProvider || forceFetch || _alwaysFetchMobilityProvider) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MobilityProviderEntityUsingMobilityProviderID);
				MobilityProviderEntity newEntity = new MobilityProviderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MobilityProviderID);
				}
				if(fetchResult)
				{
					newEntity = (MobilityProviderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mobilityProviderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MobilityProvider = newEntity;
				_alreadyFetchedMobilityProvider = fetchResult;
			}
			return _mobilityProvider;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Booking", _booking);
			toReturn.Add("BookingItem", _bookingItem);
			toReturn.Add("MobilityProvider", _mobilityProvider);
			toReturn.Add("BookingItems", _bookingItems);
			toReturn.Add("BookingItemPayments", _bookingItemPayments);
			toReturn.Add("EmailEventResendLocks", _emailEventResendLocks);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="validator">The validator object for this BookingItemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 bookingItemID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(bookingItemID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_bookingItems = new VarioSL.Entities.CollectionClasses.BookingItemCollection();
			_bookingItems.SetContainingEntityInfo(this, "BookingItem");

			_bookingItemPayments = new VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection();
			_bookingItemPayments.SetContainingEntityInfo(this, "BookingItem");

			_emailEventResendLocks = new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection();
			_emailEventResendLocks.SetContainingEntityInfo(this, "BookingItem");
			_bookingReturnsNewIfNotFound = false;
			_bookingItemReturnsNewIfNotFound = false;
			_mobilityProviderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Barcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BikeType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingItemID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingOpen", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingTypeTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndPlace", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MobilityProviderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OptionalFeatureData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OptionalFeatureReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OptionalFeatureText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurposeReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceivedReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RentalState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RentalStateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartPlace", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleReference", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _booking</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBooking(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _booking, new PropertyChangedEventHandler( OnBookingPropertyChanged ), "Booking", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.BookingEntityUsingBookingIDStatic, true, signalRelatedEntity, "BookingItems", resetFKFields, new int[] { (int)BookingItemFieldIndex.BookingID } );		
			_booking = null;
		}
		
		/// <summary> setups the sync logic for member _booking</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBooking(IEntityCore relatedEntity)
		{
			if(_booking!=relatedEntity)
			{		
				DesetupSyncBooking(true, true);
				_booking = (BookingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _booking, new PropertyChangedEventHandler( OnBookingPropertyChanged ), "Booking", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.BookingEntityUsingBookingIDStatic, true, ref _alreadyFetchedBooking, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBookingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bookingItem</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBookingItem(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.BookingItemEntityUsingBookingItemIDCancellationIDStatic, true, signalRelatedEntity, "BookingItems", resetFKFields, new int[] { (int)BookingItemFieldIndex.CancellationID } );		
			_bookingItem = null;
		}
		
		/// <summary> setups the sync logic for member _bookingItem</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBookingItem(IEntityCore relatedEntity)
		{
			if(_bookingItem!=relatedEntity)
			{		
				DesetupSyncBookingItem(true, true);
				_bookingItem = (BookingItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bookingItem, new PropertyChangedEventHandler( OnBookingItemPropertyChanged ), "BookingItem", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.BookingItemEntityUsingBookingItemIDCancellationIDStatic, true, ref _alreadyFetchedBookingItem, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBookingItemPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mobilityProvider</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMobilityProvider(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mobilityProvider, new PropertyChangedEventHandler( OnMobilityProviderPropertyChanged ), "MobilityProvider", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.MobilityProviderEntityUsingMobilityProviderIDStatic, true, signalRelatedEntity, "BookingItems", resetFKFields, new int[] { (int)BookingItemFieldIndex.MobilityProviderID } );		
			_mobilityProvider = null;
		}
		
		/// <summary> setups the sync logic for member _mobilityProvider</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMobilityProvider(IEntityCore relatedEntity)
		{
			if(_mobilityProvider!=relatedEntity)
			{		
				DesetupSyncMobilityProvider(true, true);
				_mobilityProvider = (MobilityProviderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mobilityProvider, new PropertyChangedEventHandler( OnMobilityProviderPropertyChanged ), "MobilityProvider", VarioSL.Entities.RelationClasses.StaticBookingItemRelations.MobilityProviderEntityUsingMobilityProviderIDStatic, true, ref _alreadyFetchedMobilityProvider, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMobilityProviderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="bookingItemID">PK value for BookingItem which data should be fetched into this BookingItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 bookingItemID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BookingItemFieldIndex.BookingItemID].ForcedCurrentValueWrite(bookingItemID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBookingItemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BookingItemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BookingItemRelations Relations
		{
			get	{ return new BookingItemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItems
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemCollection(), (IEntityRelation)GetRelationsForField("BookingItems")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.BookingItemEntity, 0, null, null, null, "BookingItems", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItemPayment' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItemPayments
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection(), (IEntityRelation)GetRelationsForField("BookingItemPayments")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.BookingItemPaymentEntity, 0, null, null, null, "BookingItemPayments", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEventResendLock' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventResendLocks
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection(), (IEntityRelation)GetRelationsForField("EmailEventResendLocks")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, 0, null, null, null, "EmailEventResendLocks", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Booking'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBooking
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingCollection(), (IEntityRelation)GetRelationsForField("Booking")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.BookingEntity, 0, null, null, null, "Booking", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BookingItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBookingItem
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BookingItemCollection(), (IEntityRelation)GetRelationsForField("BookingItem")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.BookingItemEntity, 0, null, null, null, "BookingItem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MobilityProvider'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMobilityProvider
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MobilityProviderCollection(), (IEntityRelation)GetRelationsForField("MobilityProvider")[0], (int)VarioSL.Entities.EntityType.BookingItemEntity, (int)VarioSL.Entities.EntityType.MobilityProviderEntity, 0, null, null, null, "MobilityProvider", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)BookingItemFieldIndex.Amount, true); }
			set	{ SetValue((int)BookingItemFieldIndex.Amount, value, true); }
		}

		/// <summary> The Barcode property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BARCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Clob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Barcode
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.Barcode, true); }
			set	{ SetValue((int)BookingItemFieldIndex.Barcode, value, true); }
		}

		/// <summary> The BikeType property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BIKETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BikeType
		{
			get { return (Nullable<System.Int64>)GetValue((int)BookingItemFieldIndex.BikeType, false); }
			set	{ SetValue((int)BookingItemFieldIndex.BikeType, value, true); }
		}

		/// <summary> The BookingID property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BOOKINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BookingID
		{
			get { return (System.Int64)GetValue((int)BookingItemFieldIndex.BookingID, true); }
			set	{ SetValue((int)BookingItemFieldIndex.BookingID, value, true); }
		}

		/// <summary> The BookingItemID property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BOOKINGITEMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 BookingItemID
		{
			get { return (System.Int64)GetValue((int)BookingItemFieldIndex.BookingItemID, true); }
			set	{ SetValue((int)BookingItemFieldIndex.BookingItemID, value, true); }
		}

		/// <summary> The BookingOpen property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BOOKINGOPEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> BookingOpen
		{
			get { return (Nullable<System.Boolean>)GetValue((int)BookingItemFieldIndex.BookingOpen, false); }
			set	{ SetValue((int)BookingItemFieldIndex.BookingOpen, value, true); }
		}

		/// <summary> The BookingType property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BOOKINGTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.BookingType> BookingType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.BookingType>)GetValue((int)BookingItemFieldIndex.BookingType, false); }
			set	{ SetValue((int)BookingItemFieldIndex.BookingType, value, true); }
		}

		/// <summary> The BookingTypeTime property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."BOOKINGTYPETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BookingTypeTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BookingItemFieldIndex.BookingTypeTime, false); }
			set	{ SetValue((int)BookingItemFieldIndex.BookingTypeTime, value, true); }
		}

		/// <summary> The CancellationID property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."CANCELLATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BookingItemFieldIndex.CancellationID, false); }
			set	{ SetValue((int)BookingItemFieldIndex.CancellationID, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BookingItemFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)BookingItemFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The EndPlace property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."ENDPLACE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EndPlace
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.EndPlace, true); }
			set	{ SetValue((int)BookingItemFieldIndex.EndPlace, value, true); }
		}

		/// <summary> The EndTime property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."ENDTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EndTime
		{
			get { return (System.DateTime)GetValue((int)BookingItemFieldIndex.EndTime, true); }
			set	{ SetValue((int)BookingItemFieldIndex.EndTime, value, true); }
		}

		/// <summary> The LastModified property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)BookingItemFieldIndex.LastModified, true); }
			set	{ SetValue((int)BookingItemFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.LastUser, true); }
			set	{ SetValue((int)BookingItemFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Latitude property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."LATITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Latitude
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.Latitude, true); }
			set	{ SetValue((int)BookingItemFieldIndex.Latitude, value, true); }
		}

		/// <summary> The Longitude property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."LONGITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Longitude
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.Longitude, true); }
			set	{ SetValue((int)BookingItemFieldIndex.Longitude, value, true); }
		}

		/// <summary> The MobilityProviderID property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."MOBILITYPROVIDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 MobilityProviderID
		{
			get { return (System.Int64)GetValue((int)BookingItemFieldIndex.MobilityProviderID, true); }
			set	{ SetValue((int)BookingItemFieldIndex.MobilityProviderID, value, true); }
		}

		/// <summary> The OptionalFeatureData property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."OPTIONALFEATUREDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Clob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OptionalFeatureData
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.OptionalFeatureData, true); }
			set	{ SetValue((int)BookingItemFieldIndex.OptionalFeatureData, value, true); }
		}

		/// <summary> The OptionalFeatureReference property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."OPTIONALFEATUREREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OptionalFeatureReference
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.OptionalFeatureReference, true); }
			set	{ SetValue((int)BookingItemFieldIndex.OptionalFeatureReference, value, true); }
		}

		/// <summary> The OptionalFeatureText property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."OPTIONALFEATURETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OptionalFeatureText
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.OptionalFeatureText, true); }
			set	{ SetValue((int)BookingItemFieldIndex.OptionalFeatureText, value, true); }
		}

		/// <summary> The PaymentReference property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."PAYMENTREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PaymentReference
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.PaymentReference, true); }
			set	{ SetValue((int)BookingItemFieldIndex.PaymentReference, value, true); }
		}

		/// <summary> The ProductID property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductID
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.ProductID, true); }
			set	{ SetValue((int)BookingItemFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ProductName property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."PRODUCTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 80<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductName
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.ProductName, true); }
			set	{ SetValue((int)BookingItemFieldIndex.ProductName, value, true); }
		}

		/// <summary> The ProductType property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."PRODUCTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ProductType ProductType
		{
			get { return (VarioSL.Entities.Enumerations.ProductType)GetValue((int)BookingItemFieldIndex.ProductType, true); }
			set	{ SetValue((int)BookingItemFieldIndex.ProductType, value, true); }
		}

		/// <summary> The PurposeReference property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."PURPOSEREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PurposeReference
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.PurposeReference, true); }
			set	{ SetValue((int)BookingItemFieldIndex.PurposeReference, value, true); }
		}

		/// <summary> The ReceivedReference property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."RECEIVEDREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceivedReference
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.ReceivedReference, true); }
			set	{ SetValue((int)BookingItemFieldIndex.ReceivedReference, value, true); }
		}

		/// <summary> The RentalState property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."RENTALSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.RentalState> RentalState
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.RentalState>)GetValue((int)BookingItemFieldIndex.RentalState, false); }
			set	{ SetValue((int)BookingItemFieldIndex.RentalState, value, true); }
		}

		/// <summary> The RentalStateTime property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."RENTALSTATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime RentalStateTime
		{
			get { return (System.DateTime)GetValue((int)BookingItemFieldIndex.RentalStateTime, true); }
			set	{ SetValue((int)BookingItemFieldIndex.RentalStateTime, value, true); }
		}

		/// <summary> The SaleType property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."SALETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SaleType SaleType
		{
			get { return (VarioSL.Entities.Enumerations.SaleType)GetValue((int)BookingItemFieldIndex.SaleType, true); }
			set	{ SetValue((int)BookingItemFieldIndex.SaleType, value, true); }
		}

		/// <summary> The StartPlace property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."STARTPLACE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StartPlace
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.StartPlace, true); }
			set	{ SetValue((int)BookingItemFieldIndex.StartPlace, value, true); }
		}

		/// <summary> The StartTime property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."STARTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime StartTime
		{
			get { return (System.DateTime)GetValue((int)BookingItemFieldIndex.StartTime, true); }
			set	{ SetValue((int)BookingItemFieldIndex.StartTime, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)BookingItemFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)BookingItemFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VehicleReference property of the Entity BookingItem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_BOOKINGITEM"."VEHICLEREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VehicleReference
		{
			get { return (System.String)GetValue((int)BookingItemFieldIndex.VehicleReference, true); }
			set	{ SetValue((int)BookingItemFieldIndex.VehicleReference, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'BookingItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBookingItems()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemCollection BookingItems
		{
			get	{ return GetMultiBookingItems(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItems. When set to true, BookingItems is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItems is accessed. You can always execute/ a forced fetch by calling GetMultiBookingItems(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItems
		{
			get	{ return _alwaysFetchBookingItems; }
			set	{ _alwaysFetchBookingItems = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItems already has been fetched. Setting this property to false when BookingItems has been fetched
		/// will clear the BookingItems collection well. Setting this property to true while BookingItems hasn't been fetched disables lazy loading for BookingItems</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItems
		{
			get { return _alreadyFetchedBookingItems;}
			set 
			{
				if(_alreadyFetchedBookingItems && !value && (_bookingItems != null))
				{
					_bookingItems.Clear();
				}
				_alreadyFetchedBookingItems = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BookingItemPaymentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBookingItemPayments()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BookingItemPaymentCollection BookingItemPayments
		{
			get	{ return GetMultiBookingItemPayments(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItemPayments. When set to true, BookingItemPayments is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItemPayments is accessed. You can always execute/ a forced fetch by calling GetMultiBookingItemPayments(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItemPayments
		{
			get	{ return _alwaysFetchBookingItemPayments; }
			set	{ _alwaysFetchBookingItemPayments = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItemPayments already has been fetched. Setting this property to false when BookingItemPayments has been fetched
		/// will clear the BookingItemPayments collection well. Setting this property to true while BookingItemPayments hasn't been fetched disables lazy loading for BookingItemPayments</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItemPayments
		{
			get { return _alreadyFetchedBookingItemPayments;}
			set 
			{
				if(_alreadyFetchedBookingItemPayments && !value && (_bookingItemPayments != null))
				{
					_bookingItemPayments.Clear();
				}
				_alreadyFetchedBookingItemPayments = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventResendLocks()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection EmailEventResendLocks
		{
			get	{ return GetMultiEmailEventResendLocks(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventResendLocks. When set to true, EmailEventResendLocks is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventResendLocks is accessed. You can always execute/ a forced fetch by calling GetMultiEmailEventResendLocks(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventResendLocks
		{
			get	{ return _alwaysFetchEmailEventResendLocks; }
			set	{ _alwaysFetchEmailEventResendLocks = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventResendLocks already has been fetched. Setting this property to false when EmailEventResendLocks has been fetched
		/// will clear the EmailEventResendLocks collection well. Setting this property to true while EmailEventResendLocks hasn't been fetched disables lazy loading for EmailEventResendLocks</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventResendLocks
		{
			get { return _alreadyFetchedEmailEventResendLocks;}
			set 
			{
				if(_alreadyFetchedEmailEventResendLocks && !value && (_emailEventResendLocks != null))
				{
					_emailEventResendLocks.Clear();
				}
				_alreadyFetchedEmailEventResendLocks = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'BookingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBooking()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BookingEntity Booking
		{
			get	{ return GetSingleBooking(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBooking(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BookingItems", "Booking", _booking, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Booking. When set to true, Booking is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Booking is accessed. You can always execute a forced fetch by calling GetSingleBooking(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBooking
		{
			get	{ return _alwaysFetchBooking; }
			set	{ _alwaysFetchBooking = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Booking already has been fetched. Setting this property to false when Booking has been fetched
		/// will set Booking to null as well. Setting this property to true while Booking hasn't been fetched disables lazy loading for Booking</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBooking
		{
			get { return _alreadyFetchedBooking;}
			set 
			{
				if(_alreadyFetchedBooking && !value)
				{
					this.Booking = null;
				}
				_alreadyFetchedBooking = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Booking is not found
		/// in the database. When set to true, Booking will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BookingReturnsNewIfNotFound
		{
			get	{ return _bookingReturnsNewIfNotFound; }
			set { _bookingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BookingItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBookingItem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BookingItemEntity BookingItem
		{
			get	{ return GetSingleBookingItem(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBookingItem(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BookingItems", "BookingItem", _bookingItem, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BookingItem. When set to true, BookingItem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BookingItem is accessed. You can always execute a forced fetch by calling GetSingleBookingItem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBookingItem
		{
			get	{ return _alwaysFetchBookingItem; }
			set	{ _alwaysFetchBookingItem = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BookingItem already has been fetched. Setting this property to false when BookingItem has been fetched
		/// will set BookingItem to null as well. Setting this property to true while BookingItem hasn't been fetched disables lazy loading for BookingItem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBookingItem
		{
			get { return _alreadyFetchedBookingItem;}
			set 
			{
				if(_alreadyFetchedBookingItem && !value)
				{
					this.BookingItem = null;
				}
				_alreadyFetchedBookingItem = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BookingItem is not found
		/// in the database. When set to true, BookingItem will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BookingItemReturnsNewIfNotFound
		{
			get	{ return _bookingItemReturnsNewIfNotFound; }
			set { _bookingItemReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MobilityProviderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMobilityProvider()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MobilityProviderEntity MobilityProvider
		{
			get	{ return GetSingleMobilityProvider(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMobilityProvider(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BookingItems", "MobilityProvider", _mobilityProvider, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MobilityProvider. When set to true, MobilityProvider is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MobilityProvider is accessed. You can always execute a forced fetch by calling GetSingleMobilityProvider(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMobilityProvider
		{
			get	{ return _alwaysFetchMobilityProvider; }
			set	{ _alwaysFetchMobilityProvider = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MobilityProvider already has been fetched. Setting this property to false when MobilityProvider has been fetched
		/// will set MobilityProvider to null as well. Setting this property to true while MobilityProvider hasn't been fetched disables lazy loading for MobilityProvider</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMobilityProvider
		{
			get { return _alreadyFetchedMobilityProvider;}
			set 
			{
				if(_alreadyFetchedMobilityProvider && !value)
				{
					this.MobilityProvider = null;
				}
				_alreadyFetchedMobilityProvider = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MobilityProvider is not found
		/// in the database. When set to true, MobilityProvider will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MobilityProviderReturnsNewIfNotFound
		{
			get	{ return _mobilityProviderReturnsNewIfNotFound; }
			set { _mobilityProviderReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BookingItemEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
