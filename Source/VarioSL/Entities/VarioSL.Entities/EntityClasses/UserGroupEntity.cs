﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserGroup'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class UserGroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.UserGroupRightCollection	_userGroupRights;
		private bool	_alwaysFetchUserGroupRights, _alreadyFetchedUserGroupRights;
		private VarioSL.Entities.CollectionClasses.UserIsMemberCollection	_userIsMembers;
		private bool	_alwaysFetchUserIsMembers, _alreadyFetchedUserIsMembers;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name UserGroupRights</summary>
			public static readonly string UserGroupRights = "UserGroupRights";
			/// <summary>Member name UserIsMembers</summary>
			public static readonly string UserIsMembers = "UserIsMembers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UserGroupEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public UserGroupEntity() :base("UserGroupEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		public UserGroupEntity(System.Int64 userGroupID):base("UserGroupEntity")
		{
			InitClassFetch(userGroupID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserGroupEntity(System.Int64 userGroupID, IPrefetchPath prefetchPathToUse):base("UserGroupEntity")
		{
			InitClassFetch(userGroupID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="validator">The custom validator object for this UserGroupEntity</param>
		public UserGroupEntity(System.Int64 userGroupID, IValidator validator):base("UserGroupEntity")
		{
			InitClassFetch(userGroupID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserGroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_userGroupRights = (VarioSL.Entities.CollectionClasses.UserGroupRightCollection)info.GetValue("_userGroupRights", typeof(VarioSL.Entities.CollectionClasses.UserGroupRightCollection));
			_alwaysFetchUserGroupRights = info.GetBoolean("_alwaysFetchUserGroupRights");
			_alreadyFetchedUserGroupRights = info.GetBoolean("_alreadyFetchedUserGroupRights");

			_userIsMembers = (VarioSL.Entities.CollectionClasses.UserIsMemberCollection)info.GetValue("_userIsMembers", typeof(VarioSL.Entities.CollectionClasses.UserIsMemberCollection));
			_alwaysFetchUserIsMembers = info.GetBoolean("_alwaysFetchUserIsMembers");
			_alreadyFetchedUserIsMembers = info.GetBoolean("_alreadyFetchedUserIsMembers");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUserGroupRights = (_userGroupRights.Count > 0);
			_alreadyFetchedUserIsMembers = (_userIsMembers.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "UserGroupRights":
					toReturn.Add(Relations.UserGroupRightEntityUsingUserGroupID);
					break;
				case "UserIsMembers":
					toReturn.Add(Relations.UserIsMemberEntityUsingUserGroupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_userGroupRights", (!this.MarkedForDeletion?_userGroupRights:null));
			info.AddValue("_alwaysFetchUserGroupRights", _alwaysFetchUserGroupRights);
			info.AddValue("_alreadyFetchedUserGroupRights", _alreadyFetchedUserGroupRights);
			info.AddValue("_userIsMembers", (!this.MarkedForDeletion?_userIsMembers:null));
			info.AddValue("_alwaysFetchUserIsMembers", _alwaysFetchUserIsMembers);
			info.AddValue("_alreadyFetchedUserIsMembers", _alreadyFetchedUserIsMembers);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "UserGroupRights":
					_alreadyFetchedUserGroupRights = true;
					if(entity!=null)
					{
						this.UserGroupRights.Add((UserGroupRightEntity)entity);
					}
					break;
				case "UserIsMembers":
					_alreadyFetchedUserIsMembers = true;
					if(entity!=null)
					{
						this.UserIsMembers.Add((UserIsMemberEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "UserGroupRights":
					_userGroupRights.Add((UserGroupRightEntity)relatedEntity);
					break;
				case "UserIsMembers":
					_userIsMembers.Add((UserIsMemberEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "UserGroupRights":
					this.PerformRelatedEntityRemoval(_userGroupRights, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserIsMembers":
					this.PerformRelatedEntityRemoval(_userIsMembers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_userGroupRights);
			toReturn.Add(_userIsMembers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupID)
		{
			return FetchUsingPK(userGroupID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(userGroupID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(userGroupID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 userGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(userGroupID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UserGroupID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UserGroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserGroupRightEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch)
		{
			return GetMultiUserGroupRights(forceFetch, _userGroupRights.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserGroupRightEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserGroupRights(forceFetch, _userGroupRights.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserGroupRights(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserGroupRightCollection GetMultiUserGroupRights(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserGroupRights || forceFetch || _alwaysFetchUserGroupRights) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userGroupRights);
				_userGroupRights.SuppressClearInGetMulti=!forceFetch;
				_userGroupRights.EntityFactoryToUse = entityFactoryToUse;
				_userGroupRights.GetMultiManyToOne(this, null, filter);
				_userGroupRights.SuppressClearInGetMulti=false;
				_alreadyFetchedUserGroupRights = true;
			}
			return _userGroupRights;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserGroupRights'. These settings will be taken into account
		/// when the property UserGroupRights is requested or GetMultiUserGroupRights is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserGroupRights(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userGroupRights.SortClauses=sortClauses;
			_userGroupRights.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserIsMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch)
		{
			return GetMultiUserIsMembers(forceFetch, _userIsMembers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserIsMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserIsMembers(forceFetch, _userIsMembers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserIsMembers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserIsMemberCollection GetMultiUserIsMembers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserIsMembers || forceFetch || _alwaysFetchUserIsMembers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userIsMembers);
				_userIsMembers.SuppressClearInGetMulti=!forceFetch;
				_userIsMembers.EntityFactoryToUse = entityFactoryToUse;
				_userIsMembers.GetMultiManyToOne(this, null, filter);
				_userIsMembers.SuppressClearInGetMulti=false;
				_alreadyFetchedUserIsMembers = true;
			}
			return _userIsMembers;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserIsMembers'. These settings will be taken into account
		/// when the property UserIsMembers is requested or GetMultiUserIsMembers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserIsMembers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userIsMembers.SortClauses=sortClauses;
			_userIsMembers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("UserGroupRights", _userGroupRights);
			toReturn.Add("UserIsMembers", _userIsMembers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="validator">The validator object for this UserGroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 userGroupID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(userGroupID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_userGroupRights = new VarioSL.Entities.CollectionClasses.UserGroupRightCollection();
			_userGroupRights.SetContainingEntityInfo(this, "UserGroup");

			_userIsMembers = new VarioSL.Entities.CollectionClasses.UserIsMemberCollection();
			_userIsMembers.SetContainingEntityInfo(this, "UserGroup");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="userGroupID">PK value for UserGroup which data should be fetched into this UserGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 userGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UserGroupFieldIndex.UserGroupID].ForcedCurrentValueWrite(userGroupID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUserGroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UserGroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UserGroupRelations Relations
		{
			get	{ return new UserGroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserGroupRight' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserGroupRights
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserGroupRightCollection(), (IEntityRelation)GetRelationsForField("UserGroupRights")[0], (int)VarioSL.Entities.EntityType.UserGroupEntity, (int)VarioSL.Entities.EntityType.UserGroupRightEntity, 0, null, null, null, "UserGroupRights", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserIsMember' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserIsMembers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserIsMemberCollection(), (IEntityRelation)GetRelationsForField("UserIsMembers")[0], (int)VarioSL.Entities.EntityType.UserGroupEntity, (int)VarioSL.Entities.EntityType.UserIsMemberEntity, 0, null, null, null, "UserIsMembers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity UserGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUP"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)UserGroupFieldIndex.ClientID, false); }
			set	{ SetValue((int)UserGroupFieldIndex.ClientID, value, true); }
		}

		/// <summary> The UserGroupDescription property of the Entity UserGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUP"."USERGROUPDESCR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserGroupDescription
		{
			get { return (System.String)GetValue((int)UserGroupFieldIndex.UserGroupDescription, true); }
			set	{ SetValue((int)UserGroupFieldIndex.UserGroupDescription, value, true); }
		}

		/// <summary> The UserGroupID property of the Entity UserGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUP"."USERGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 UserGroupID
		{
			get { return (System.Int64)GetValue((int)UserGroupFieldIndex.UserGroupID, true); }
			set	{ SetValue((int)UserGroupFieldIndex.UserGroupID, value, true); }
		}

		/// <summary> The UserGroupName property of the Entity UserGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "USER_GROUP"."USERGROUPNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UserGroupName
		{
			get { return (System.String)GetValue((int)UserGroupFieldIndex.UserGroupName, true); }
			set	{ SetValue((int)UserGroupFieldIndex.UserGroupName, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'UserGroupRightEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserGroupRights()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserGroupRightCollection UserGroupRights
		{
			get	{ return GetMultiUserGroupRights(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserGroupRights. When set to true, UserGroupRights is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserGroupRights is accessed. You can always execute/ a forced fetch by calling GetMultiUserGroupRights(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserGroupRights
		{
			get	{ return _alwaysFetchUserGroupRights; }
			set	{ _alwaysFetchUserGroupRights = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserGroupRights already has been fetched. Setting this property to false when UserGroupRights has been fetched
		/// will clear the UserGroupRights collection well. Setting this property to true while UserGroupRights hasn't been fetched disables lazy loading for UserGroupRights</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserGroupRights
		{
			get { return _alreadyFetchedUserGroupRights;}
			set 
			{
				if(_alreadyFetchedUserGroupRights && !value && (_userGroupRights != null))
				{
					_userGroupRights.Clear();
				}
				_alreadyFetchedUserGroupRights = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserIsMemberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserIsMembers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserIsMemberCollection UserIsMembers
		{
			get	{ return GetMultiUserIsMembers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserIsMembers. When set to true, UserIsMembers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserIsMembers is accessed. You can always execute/ a forced fetch by calling GetMultiUserIsMembers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserIsMembers
		{
			get	{ return _alwaysFetchUserIsMembers; }
			set	{ _alwaysFetchUserIsMembers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserIsMembers already has been fetched. Setting this property to false when UserIsMembers has been fetched
		/// will clear the UserIsMembers collection well. Setting this property to true while UserIsMembers hasn't been fetched disables lazy loading for UserIsMembers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserIsMembers
		{
			get { return _alreadyFetchedUserIsMembers;}
			set 
			{
				if(_alreadyFetchedUserIsMembers && !value && (_userIsMembers != null))
				{
					_userIsMembers.Clear();
				}
				_alreadyFetchedUserIsMembers = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.UserGroupEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
