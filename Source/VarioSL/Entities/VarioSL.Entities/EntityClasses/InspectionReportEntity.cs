﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'InspectionReport'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class InspectionReportEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidents;
		private bool	_alwaysFetchFareEvasionIncidents, _alreadyFetchedFareEvasionIncidents;
		private VarioSL.Entities.CollectionClasses.InspectionCriterionCollection	_slInspectioncriterions;
		private bool	_alwaysFetchSlInspectioncriterions, _alreadyFetchedSlInspectioncriterions;
		private VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection	_transactionToInspections;
		private bool	_alwaysFetchTransactionToInspections, _alreadyFetchedTransactionToInspections;
		private FareEvasionInspectionEntity _fareEvasionInspection;
		private bool	_alwaysFetchFareEvasionInspection, _alreadyFetchedFareEvasionInspection, _fareEvasionInspectionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FareEvasionInspection</summary>
			public static readonly string FareEvasionInspection = "FareEvasionInspection";
			/// <summary>Member name FareEvasionIncidents</summary>
			public static readonly string FareEvasionIncidents = "FareEvasionIncidents";
			/// <summary>Member name SlInspectioncriterions</summary>
			public static readonly string SlInspectioncriterions = "SlInspectioncriterions";
			/// <summary>Member name TransactionToInspections</summary>
			public static readonly string TransactionToInspections = "TransactionToInspections";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static InspectionReportEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public InspectionReportEntity() :base("InspectionReportEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		public InspectionReportEntity(System.Int64 inspectionReportID):base("InspectionReportEntity")
		{
			InitClassFetch(inspectionReportID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public InspectionReportEntity(System.Int64 inspectionReportID, IPrefetchPath prefetchPathToUse):base("InspectionReportEntity")
		{
			InitClassFetch(inspectionReportID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="validator">The custom validator object for this InspectionReportEntity</param>
		public InspectionReportEntity(System.Int64 inspectionReportID, IValidator validator):base("InspectionReportEntity")
		{
			InitClassFetch(inspectionReportID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InspectionReportEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareEvasionIncidents = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidents", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidents = info.GetBoolean("_alwaysFetchFareEvasionIncidents");
			_alreadyFetchedFareEvasionIncidents = info.GetBoolean("_alreadyFetchedFareEvasionIncidents");

			_slInspectioncriterions = (VarioSL.Entities.CollectionClasses.InspectionCriterionCollection)info.GetValue("_slInspectioncriterions", typeof(VarioSL.Entities.CollectionClasses.InspectionCriterionCollection));
			_alwaysFetchSlInspectioncriterions = info.GetBoolean("_alwaysFetchSlInspectioncriterions");
			_alreadyFetchedSlInspectioncriterions = info.GetBoolean("_alreadyFetchedSlInspectioncriterions");

			_transactionToInspections = (VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection)info.GetValue("_transactionToInspections", typeof(VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection));
			_alwaysFetchTransactionToInspections = info.GetBoolean("_alwaysFetchTransactionToInspections");
			_alreadyFetchedTransactionToInspections = info.GetBoolean("_alreadyFetchedTransactionToInspections");
			_fareEvasionInspection = (FareEvasionInspectionEntity)info.GetValue("_fareEvasionInspection", typeof(FareEvasionInspectionEntity));
			if(_fareEvasionInspection!=null)
			{
				_fareEvasionInspection.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionInspectionReturnsNewIfNotFound = info.GetBoolean("_fareEvasionInspectionReturnsNewIfNotFound");
			_alwaysFetchFareEvasionInspection = info.GetBoolean("_alwaysFetchFareEvasionInspection");
			_alreadyFetchedFareEvasionInspection = info.GetBoolean("_alreadyFetchedFareEvasionInspection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((InspectionReportFieldIndex)fieldIndex)
			{
				case InspectionReportFieldIndex.FareEvasionInspectionID:
					DesetupSyncFareEvasionInspection(true, false);
					_alreadyFetchedFareEvasionInspection = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareEvasionIncidents = (_fareEvasionIncidents.Count > 0);
			_alreadyFetchedSlInspectioncriterions = (_slInspectioncriterions.Count > 0);
			_alreadyFetchedTransactionToInspections = (_transactionToInspections.Count > 0);
			_alreadyFetchedFareEvasionInspection = (_fareEvasionInspection != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FareEvasionInspection":
					toReturn.Add(Relations.FareEvasionInspectionEntityUsingFareEvasionInspectionID);
					break;
				case "FareEvasionIncidents":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingInspectionReportID);
					break;
				case "SlInspectioncriterions":
					toReturn.Add(Relations.InspectionCriterionEntityUsingInspectionReportID);
					break;
				case "TransactionToInspections":
					toReturn.Add(Relations.TransactionToInspectionEntityUsingInspectionReportID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareEvasionIncidents", (!this.MarkedForDeletion?_fareEvasionIncidents:null));
			info.AddValue("_alwaysFetchFareEvasionIncidents", _alwaysFetchFareEvasionIncidents);
			info.AddValue("_alreadyFetchedFareEvasionIncidents", _alreadyFetchedFareEvasionIncidents);
			info.AddValue("_slInspectioncriterions", (!this.MarkedForDeletion?_slInspectioncriterions:null));
			info.AddValue("_alwaysFetchSlInspectioncriterions", _alwaysFetchSlInspectioncriterions);
			info.AddValue("_alreadyFetchedSlInspectioncriterions", _alreadyFetchedSlInspectioncriterions);
			info.AddValue("_transactionToInspections", (!this.MarkedForDeletion?_transactionToInspections:null));
			info.AddValue("_alwaysFetchTransactionToInspections", _alwaysFetchTransactionToInspections);
			info.AddValue("_alreadyFetchedTransactionToInspections", _alreadyFetchedTransactionToInspections);
			info.AddValue("_fareEvasionInspection", (!this.MarkedForDeletion?_fareEvasionInspection:null));
			info.AddValue("_fareEvasionInspectionReturnsNewIfNotFound", _fareEvasionInspectionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionInspection", _alwaysFetchFareEvasionInspection);
			info.AddValue("_alreadyFetchedFareEvasionInspection", _alreadyFetchedFareEvasionInspection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FareEvasionInspection":
					_alreadyFetchedFareEvasionInspection = true;
					this.FareEvasionInspection = (FareEvasionInspectionEntity)entity;
					break;
				case "FareEvasionIncidents":
					_alreadyFetchedFareEvasionIncidents = true;
					if(entity!=null)
					{
						this.FareEvasionIncidents.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "SlInspectioncriterions":
					_alreadyFetchedSlInspectioncriterions = true;
					if(entity!=null)
					{
						this.SlInspectioncriterions.Add((InspectionCriterionEntity)entity);
					}
					break;
				case "TransactionToInspections":
					_alreadyFetchedTransactionToInspections = true;
					if(entity!=null)
					{
						this.TransactionToInspections.Add((TransactionToInspectionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FareEvasionInspection":
					SetupSyncFareEvasionInspection(relatedEntity);
					break;
				case "FareEvasionIncidents":
					_fareEvasionIncidents.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "SlInspectioncriterions":
					_slInspectioncriterions.Add((InspectionCriterionEntity)relatedEntity);
					break;
				case "TransactionToInspections":
					_transactionToInspections.Add((TransactionToInspectionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FareEvasionInspection":
					DesetupSyncFareEvasionInspection(false, true);
					break;
				case "FareEvasionIncidents":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidents, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SlInspectioncriterions":
					this.PerformRelatedEntityRemoval(_slInspectioncriterions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionToInspections":
					this.PerformRelatedEntityRemoval(_transactionToInspections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fareEvasionInspection!=null)
			{
				toReturn.Add(_fareEvasionInspection);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareEvasionIncidents);
			toReturn.Add(_slInspectioncriterions);
			toReturn.Add(_transactionToInspections);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionReportID)
		{
			return FetchUsingPK(inspectionReportID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionReportID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(inspectionReportID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionReportID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(inspectionReportID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 inspectionReportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(inspectionReportID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.InspectionReportID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new InspectionReportRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidents(forceFetch, _fareEvasionIncidents.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidents(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidents(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidents || forceFetch || _alwaysFetchFareEvasionIncidents) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidents);
				_fareEvasionIncidents.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidents.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidents.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_fareEvasionIncidents.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidents = true;
			}
			return _fareEvasionIncidents;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidents'. These settings will be taken into account
		/// when the property FareEvasionIncidents is requested or GetMultiFareEvasionIncidents is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidents(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidents.SortClauses=sortClauses;
			_fareEvasionIncidents.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InspectionCriterionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InspectionCriterionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InspectionCriterionCollection GetMultiSlInspectioncriterions(bool forceFetch)
		{
			return GetMultiSlInspectioncriterions(forceFetch, _slInspectioncriterions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InspectionCriterionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InspectionCriterionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InspectionCriterionCollection GetMultiSlInspectioncriterions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSlInspectioncriterions(forceFetch, _slInspectioncriterions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InspectionCriterionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InspectionCriterionCollection GetMultiSlInspectioncriterions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSlInspectioncriterions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InspectionCriterionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InspectionCriterionCollection GetMultiSlInspectioncriterions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSlInspectioncriterions || forceFetch || _alwaysFetchSlInspectioncriterions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_slInspectioncriterions);
				_slInspectioncriterions.SuppressClearInGetMulti=!forceFetch;
				_slInspectioncriterions.EntityFactoryToUse = entityFactoryToUse;
				_slInspectioncriterions.GetMultiManyToOne(this, filter);
				_slInspectioncriterions.SuppressClearInGetMulti=false;
				_alreadyFetchedSlInspectioncriterions = true;
			}
			return _slInspectioncriterions;
		}

		/// <summary> Sets the collection parameters for the collection for 'SlInspectioncriterions'. These settings will be taken into account
		/// when the property SlInspectioncriterions is requested or GetMultiSlInspectioncriterions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSlInspectioncriterions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_slInspectioncriterions.SortClauses=sortClauses;
			_slInspectioncriterions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionToInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch)
		{
			return GetMultiTransactionToInspections(forceFetch, _transactionToInspections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionToInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionToInspections(forceFetch, _transactionToInspections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionToInspections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionToInspections || forceFetch || _alwaysFetchTransactionToInspections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionToInspections);
				_transactionToInspections.SuppressClearInGetMulti=!forceFetch;
				_transactionToInspections.EntityFactoryToUse = entityFactoryToUse;
				_transactionToInspections.GetMultiManyToOne(this, null, filter);
				_transactionToInspections.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionToInspections = true;
			}
			return _transactionToInspections;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionToInspections'. These settings will be taken into account
		/// when the property TransactionToInspections is requested or GetMultiTransactionToInspections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionToInspections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionToInspections.SortClauses=sortClauses;
			_transactionToInspections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionInspectionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionInspectionEntity' which is related to this entity.</returns>
		public FareEvasionInspectionEntity GetSingleFareEvasionInspection()
		{
			return GetSingleFareEvasionInspection(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionInspectionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionInspectionEntity' which is related to this entity.</returns>
		public virtual FareEvasionInspectionEntity GetSingleFareEvasionInspection(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionInspection || forceFetch || _alwaysFetchFareEvasionInspection) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionInspectionEntityUsingFareEvasionInspectionID);
				FareEvasionInspectionEntity newEntity = new FareEvasionInspectionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionInspectionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionInspectionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionInspectionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionInspection = newEntity;
				_alreadyFetchedFareEvasionInspection = fetchResult;
			}
			return _fareEvasionInspection;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FareEvasionInspection", _fareEvasionInspection);
			toReturn.Add("FareEvasionIncidents", _fareEvasionIncidents);
			toReturn.Add("SlInspectioncriterions", _slInspectioncriterions);
			toReturn.Add("TransactionToInspections", _transactionToInspections);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="validator">The validator object for this InspectionReportEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 inspectionReportID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(inspectionReportID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareEvasionIncidents = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidents.SetContainingEntityInfo(this, "InspectionReport");

			_slInspectioncriterions = new VarioSL.Entities.CollectionClasses.InspectionCriterionCollection();
			_slInspectioncriterions.SetContainingEntityInfo(this, "InspectionReport");

			_transactionToInspections = new VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection();
			_transactionToInspections.SetContainingEntityInfo(this, "InspectionReport");
			_fareEvasionInspectionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DriverNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionInspectionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionStart", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectorNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProcessed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RawData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Report", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Route", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupervisorComment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fareEvasionInspection</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionInspection(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionInspection, new PropertyChangedEventHandler( OnFareEvasionInspectionPropertyChanged ), "FareEvasionInspection", VarioSL.Entities.RelationClasses.StaticInspectionReportRelations.FareEvasionInspectionEntityUsingFareEvasionInspectionIDStatic, true, signalRelatedEntity, "InspectionReports", resetFKFields, new int[] { (int)InspectionReportFieldIndex.FareEvasionInspectionID } );		
			_fareEvasionInspection = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionInspection</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionInspection(IEntityCore relatedEntity)
		{
			if(_fareEvasionInspection!=relatedEntity)
			{		
				DesetupSyncFareEvasionInspection(true, true);
				_fareEvasionInspection = (FareEvasionInspectionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionInspection, new PropertyChangedEventHandler( OnFareEvasionInspectionPropertyChanged ), "FareEvasionInspection", VarioSL.Entities.RelationClasses.StaticInspectionReportRelations.FareEvasionInspectionEntityUsingFareEvasionInspectionIDStatic, true, ref _alreadyFetchedFareEvasionInspection, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionInspectionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="inspectionReportID">PK value for InspectionReport which data should be fetched into this InspectionReport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 inspectionReportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)InspectionReportFieldIndex.InspectionReportID].ForcedCurrentValueWrite(inspectionReportID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateInspectionReportDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new InspectionReportEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static InspectionReportRelations Relations
		{
			get	{ return new InspectionReportRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidents
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidents")[0], (int)VarioSL.Entities.EntityType.InspectionReportEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidents", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InspectionCriterion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSlInspectioncriterions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InspectionCriterionCollection(), (IEntityRelation)GetRelationsForField("SlInspectioncriterions")[0], (int)VarioSL.Entities.EntityType.InspectionReportEntity, (int)VarioSL.Entities.EntityType.InspectionCriterionEntity, 0, null, null, null, "SlInspectioncriterions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionToInspection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionToInspections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection(), (IEntityRelation)GetRelationsForField("TransactionToInspections")[0], (int)VarioSL.Entities.EntityType.InspectionReportEntity, (int)VarioSL.Entities.EntityType.TransactionToInspectionEntity, 0, null, null, null, "TransactionToInspections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionInspection'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionInspection
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionInspectionCollection(), (IEntityRelation)GetRelationsForField("FareEvasionInspection")[0], (int)VarioSL.Entities.EntityType.InspectionReportEntity, (int)VarioSL.Entities.EntityType.FareEvasionInspectionEntity, 0, null, null, null, "FareEvasionInspection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Direction property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."DIRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Direction
		{
			get { return (Nullable<System.Int32>)GetValue((int)InspectionReportFieldIndex.Direction, false); }
			set	{ SetValue((int)InspectionReportFieldIndex.Direction, value, true); }
		}

		/// <summary> The DriverNumber property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."DRIVERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DriverNumber
		{
			get { return (System.Int32)GetValue((int)InspectionReportFieldIndex.DriverNumber, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.DriverNumber, value, true); }
		}

		/// <summary> The FareEvasionInspectionID property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."FAREEVASIONINSPECTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareEvasionInspectionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InspectionReportFieldIndex.FareEvasionInspectionID, false); }
			set	{ SetValue((int)InspectionReportFieldIndex.FareEvasionInspectionID, value, true); }
		}

		/// <summary> The InspectionComment property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTIONCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InspectionComment
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.InspectionComment, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectionComment, value, true); }
		}

		/// <summary> The InspectionCount property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTIONCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InspectionCount
		{
			get { return (Nullable<System.Int32>)GetValue((int)InspectionReportFieldIndex.InspectionCount, false); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectionCount, value, true); }
		}

		/// <summary> The InspectionEnd property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTIONEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InspectionEnd
		{
			get { return (System.DateTime)GetValue((int)InspectionReportFieldIndex.InspectionEnd, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectionEnd, value, true); }
		}

		/// <summary> The InspectionReportID property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTIONREPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 InspectionReportID
		{
			get { return (System.Int64)GetValue((int)InspectionReportFieldIndex.InspectionReportID, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectionReportID, value, true); }
		}

		/// <summary> The InspectionStart property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTIONSTART"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InspectionStart
		{
			get { return (System.DateTime)GetValue((int)InspectionReportFieldIndex.InspectionStart, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectionStart, value, true); }
		}

		/// <summary> The InspectorNumber property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."INSPECTORNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 InspectorNumber
		{
			get { return (System.Int32)GetValue((int)InspectionReportFieldIndex.InspectorNumber, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.InspectorNumber, value, true); }
		}

		/// <summary> The IsProcessed property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."ISPROCESSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProcessed
		{
			get { return (System.Boolean)GetValue((int)InspectionReportFieldIndex.IsProcessed, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.IsProcessed, value, true); }
		}

		/// <summary> The LastModified property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)InspectionReportFieldIndex.LastModified, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.LastUser, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Line property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."LINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Line
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.Line, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.Line, value, true); }
		}

		/// <summary> The LocationNumber property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."LOCATIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LocationNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)InspectionReportFieldIndex.LocationNumber, false); }
			set	{ SetValue((int)InspectionReportFieldIndex.LocationNumber, value, true); }
		}

		/// <summary> The OperatorID property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OperatorID
		{
			get { return (System.Int64)GetValue((int)InspectionReportFieldIndex.OperatorID, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The RawData property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."RAWDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String RawData
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.RawData, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.RawData, value, true); }
		}

		/// <summary> The Report property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."REPORT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Report
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.Report, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.Report, value, true); }
		}

		/// <summary> The Route property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."ROUTE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Route
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.Route, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.Route, value, true); }
		}

		/// <summary> The SupervisorComment property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."SUPERVISORCOMMENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SupervisorComment
		{
			get { return (System.String)GetValue((int)InspectionReportFieldIndex.SupervisorComment, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.SupervisorComment, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)InspectionReportFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)InspectionReportFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VehicleNumber property of the Entity InspectionReport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INSPECTIONREPORT"."VEHICLENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VehicleNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)InspectionReportFieldIndex.VehicleNumber, false); }
			set	{ SetValue((int)InspectionReportFieldIndex.VehicleNumber, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidents()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidents
		{
			get	{ return GetMultiFareEvasionIncidents(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidents. When set to true, FareEvasionIncidents is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidents is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidents(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidents
		{
			get	{ return _alwaysFetchFareEvasionIncidents; }
			set	{ _alwaysFetchFareEvasionIncidents = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidents already has been fetched. Setting this property to false when FareEvasionIncidents has been fetched
		/// will clear the FareEvasionIncidents collection well. Setting this property to true while FareEvasionIncidents hasn't been fetched disables lazy loading for FareEvasionIncidents</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidents
		{
			get { return _alreadyFetchedFareEvasionIncidents;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidents && !value && (_fareEvasionIncidents != null))
				{
					_fareEvasionIncidents.Clear();
				}
				_alreadyFetchedFareEvasionIncidents = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InspectionCriterionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSlInspectioncriterions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InspectionCriterionCollection SlInspectioncriterions
		{
			get	{ return GetMultiSlInspectioncriterions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SlInspectioncriterions. When set to true, SlInspectioncriterions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SlInspectioncriterions is accessed. You can always execute/ a forced fetch by calling GetMultiSlInspectioncriterions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSlInspectioncriterions
		{
			get	{ return _alwaysFetchSlInspectioncriterions; }
			set	{ _alwaysFetchSlInspectioncriterions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SlInspectioncriterions already has been fetched. Setting this property to false when SlInspectioncriterions has been fetched
		/// will clear the SlInspectioncriterions collection well. Setting this property to true while SlInspectioncriterions hasn't been fetched disables lazy loading for SlInspectioncriterions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSlInspectioncriterions
		{
			get { return _alreadyFetchedSlInspectioncriterions;}
			set 
			{
				if(_alreadyFetchedSlInspectioncriterions && !value && (_slInspectioncriterions != null))
				{
					_slInspectioncriterions.Clear();
				}
				_alreadyFetchedSlInspectioncriterions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionToInspections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection TransactionToInspections
		{
			get	{ return GetMultiTransactionToInspections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionToInspections. When set to true, TransactionToInspections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionToInspections is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionToInspections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionToInspections
		{
			get	{ return _alwaysFetchTransactionToInspections; }
			set	{ _alwaysFetchTransactionToInspections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionToInspections already has been fetched. Setting this property to false when TransactionToInspections has been fetched
		/// will clear the TransactionToInspections collection well. Setting this property to true while TransactionToInspections hasn't been fetched disables lazy loading for TransactionToInspections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionToInspections
		{
			get { return _alreadyFetchedTransactionToInspections;}
			set 
			{
				if(_alreadyFetchedTransactionToInspections && !value && (_transactionToInspections != null))
				{
					_transactionToInspections.Clear();
				}
				_alreadyFetchedTransactionToInspections = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionInspectionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionInspection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionInspectionEntity FareEvasionInspection
		{
			get	{ return GetSingleFareEvasionInspection(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionInspection(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "InspectionReports", "FareEvasionInspection", _fareEvasionInspection, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionInspection. When set to true, FareEvasionInspection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionInspection is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionInspection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionInspection
		{
			get	{ return _alwaysFetchFareEvasionInspection; }
			set	{ _alwaysFetchFareEvasionInspection = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionInspection already has been fetched. Setting this property to false when FareEvasionInspection has been fetched
		/// will set FareEvasionInspection to null as well. Setting this property to true while FareEvasionInspection hasn't been fetched disables lazy loading for FareEvasionInspection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionInspection
		{
			get { return _alreadyFetchedFareEvasionInspection;}
			set 
			{
				if(_alreadyFetchedFareEvasionInspection && !value)
				{
					this.FareEvasionInspection = null;
				}
				_alreadyFetchedFareEvasionInspection = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionInspection is not found
		/// in the database. When set to true, FareEvasionInspection will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionInspectionReturnsNewIfNotFound
		{
			get	{ return _fareEvasionInspectionReturnsNewIfNotFound; }
			set { _fareEvasionInspectionReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.InspectionReportEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
