﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SamModule'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SamModuleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection	_vdvKeyInfos;
		private bool	_alwaysFetchVdvKeyInfos, _alreadyFetchedVdvKeyInfos;
		private VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection	_vdvLoadKeyMessages;
		private bool	_alwaysFetchVdvLoadKeyMessages, _alreadyFetchedVdvLoadKeyMessages;
		private VarioSL.Entities.CollectionClasses.VdvSamStatusCollection	_vdvSamStatuses;
		private bool	_alwaysFetchVdvSamStatuses, _alreadyFetchedVdvSamStatuses;
		private DeviceEntity _device;
		private bool	_alwaysFetchDevice, _alreadyFetchedDevice, _deviceReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Device</summary>
			public static readonly string Device = "Device";
			/// <summary>Member name VdvKeyInfos</summary>
			public static readonly string VdvKeyInfos = "VdvKeyInfos";
			/// <summary>Member name VdvLoadKeyMessages</summary>
			public static readonly string VdvLoadKeyMessages = "VdvLoadKeyMessages";
			/// <summary>Member name VdvSamStatuses</summary>
			public static readonly string VdvSamStatuses = "VdvSamStatuses";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SamModuleEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SamModuleEntity() :base("SamModuleEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		public SamModuleEntity(System.Int64 samID):base("SamModuleEntity")
		{
			InitClassFetch(samID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SamModuleEntity(System.Int64 samID, IPrefetchPath prefetchPathToUse):base("SamModuleEntity")
		{
			InitClassFetch(samID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="validator">The custom validator object for this SamModuleEntity</param>
		public SamModuleEntity(System.Int64 samID, IValidator validator):base("SamModuleEntity")
		{
			InitClassFetch(samID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SamModuleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_vdvKeyInfos = (VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection)info.GetValue("_vdvKeyInfos", typeof(VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection));
			_alwaysFetchVdvKeyInfos = info.GetBoolean("_alwaysFetchVdvKeyInfos");
			_alreadyFetchedVdvKeyInfos = info.GetBoolean("_alreadyFetchedVdvKeyInfos");

			_vdvLoadKeyMessages = (VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection)info.GetValue("_vdvLoadKeyMessages", typeof(VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection));
			_alwaysFetchVdvLoadKeyMessages = info.GetBoolean("_alwaysFetchVdvLoadKeyMessages");
			_alreadyFetchedVdvLoadKeyMessages = info.GetBoolean("_alreadyFetchedVdvLoadKeyMessages");

			_vdvSamStatuses = (VarioSL.Entities.CollectionClasses.VdvSamStatusCollection)info.GetValue("_vdvSamStatuses", typeof(VarioSL.Entities.CollectionClasses.VdvSamStatusCollection));
			_alwaysFetchVdvSamStatuses = info.GetBoolean("_alwaysFetchVdvSamStatuses");
			_alreadyFetchedVdvSamStatuses = info.GetBoolean("_alreadyFetchedVdvSamStatuses");
			_device = (DeviceEntity)info.GetValue("_device", typeof(DeviceEntity));
			if(_device!=null)
			{
				_device.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceReturnsNewIfNotFound = info.GetBoolean("_deviceReturnsNewIfNotFound");
			_alwaysFetchDevice = info.GetBoolean("_alwaysFetchDevice");
			_alreadyFetchedDevice = info.GetBoolean("_alreadyFetchedDevice");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SamModuleFieldIndex)fieldIndex)
			{
				case SamModuleFieldIndex.DeviceID:
					DesetupSyncDevice(true, false);
					_alreadyFetchedDevice = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedVdvKeyInfos = (_vdvKeyInfos.Count > 0);
			_alreadyFetchedVdvLoadKeyMessages = (_vdvLoadKeyMessages.Count > 0);
			_alreadyFetchedVdvSamStatuses = (_vdvSamStatuses.Count > 0);
			_alreadyFetchedDevice = (_device != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Device":
					toReturn.Add(Relations.DeviceEntityUsingDeviceID);
					break;
				case "VdvKeyInfos":
					toReturn.Add(Relations.VdvKeyInfoEntityUsingSamID);
					break;
				case "VdvLoadKeyMessages":
					toReturn.Add(Relations.VdvLoadKeyMessageEntityUsingSamID);
					break;
				case "VdvSamStatuses":
					toReturn.Add(Relations.VdvSamStatusEntityUsingSamID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_vdvKeyInfos", (!this.MarkedForDeletion?_vdvKeyInfos:null));
			info.AddValue("_alwaysFetchVdvKeyInfos", _alwaysFetchVdvKeyInfos);
			info.AddValue("_alreadyFetchedVdvKeyInfos", _alreadyFetchedVdvKeyInfos);
			info.AddValue("_vdvLoadKeyMessages", (!this.MarkedForDeletion?_vdvLoadKeyMessages:null));
			info.AddValue("_alwaysFetchVdvLoadKeyMessages", _alwaysFetchVdvLoadKeyMessages);
			info.AddValue("_alreadyFetchedVdvLoadKeyMessages", _alreadyFetchedVdvLoadKeyMessages);
			info.AddValue("_vdvSamStatuses", (!this.MarkedForDeletion?_vdvSamStatuses:null));
			info.AddValue("_alwaysFetchVdvSamStatuses", _alwaysFetchVdvSamStatuses);
			info.AddValue("_alreadyFetchedVdvSamStatuses", _alreadyFetchedVdvSamStatuses);
			info.AddValue("_device", (!this.MarkedForDeletion?_device:null));
			info.AddValue("_deviceReturnsNewIfNotFound", _deviceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDevice", _alwaysFetchDevice);
			info.AddValue("_alreadyFetchedDevice", _alreadyFetchedDevice);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Device":
					_alreadyFetchedDevice = true;
					this.Device = (DeviceEntity)entity;
					break;
				case "VdvKeyInfos":
					_alreadyFetchedVdvKeyInfos = true;
					if(entity!=null)
					{
						this.VdvKeyInfos.Add((VdvKeyInfoEntity)entity);
					}
					break;
				case "VdvLoadKeyMessages":
					_alreadyFetchedVdvLoadKeyMessages = true;
					if(entity!=null)
					{
						this.VdvLoadKeyMessages.Add((VdvLoadKeyMessageEntity)entity);
					}
					break;
				case "VdvSamStatuses":
					_alreadyFetchedVdvSamStatuses = true;
					if(entity!=null)
					{
						this.VdvSamStatuses.Add((VdvSamStatusEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Device":
					SetupSyncDevice(relatedEntity);
					break;
				case "VdvKeyInfos":
					_vdvKeyInfos.Add((VdvKeyInfoEntity)relatedEntity);
					break;
				case "VdvLoadKeyMessages":
					_vdvLoadKeyMessages.Add((VdvLoadKeyMessageEntity)relatedEntity);
					break;
				case "VdvSamStatuses":
					_vdvSamStatuses.Add((VdvSamStatusEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Device":
					DesetupSyncDevice(false, true);
					break;
				case "VdvKeyInfos":
					this.PerformRelatedEntityRemoval(_vdvKeyInfos, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvLoadKeyMessages":
					this.PerformRelatedEntityRemoval(_vdvLoadKeyMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvSamStatuses":
					this.PerformRelatedEntityRemoval(_vdvSamStatuses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_device!=null)
			{
				toReturn.Add(_device);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_vdvKeyInfos);
			toReturn.Add(_vdvLoadKeyMessages);
			toReturn.Add(_vdvSamStatuses);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 samID)
		{
			return FetchUsingPK(samID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 samID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(samID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 samID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(samID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 samID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(samID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SamID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SamModuleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'VdvKeyInfoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeyInfoEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection GetMultiVdvKeyInfos(bool forceFetch)
		{
			return GetMultiVdvKeyInfos(forceFetch, _vdvKeyInfos.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeyInfoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeyInfoEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection GetMultiVdvKeyInfos(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvKeyInfos(forceFetch, _vdvKeyInfos.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeyInfoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection GetMultiVdvKeyInfos(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvKeyInfos(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeyInfoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection GetMultiVdvKeyInfos(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvKeyInfos || forceFetch || _alwaysFetchVdvKeyInfos) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvKeyInfos);
				_vdvKeyInfos.SuppressClearInGetMulti=!forceFetch;
				_vdvKeyInfos.EntityFactoryToUse = entityFactoryToUse;
				_vdvKeyInfos.GetMultiManyToOne(this, filter);
				_vdvKeyInfos.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvKeyInfos = true;
			}
			return _vdvKeyInfos;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvKeyInfos'. These settings will be taken into account
		/// when the property VdvKeyInfos is requested or GetMultiVdvKeyInfos is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvKeyInfos(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvKeyInfos.SortClauses=sortClauses;
			_vdvKeyInfos.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvLoadKeyMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvLoadKeyMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection GetMultiVdvLoadKeyMessages(bool forceFetch)
		{
			return GetMultiVdvLoadKeyMessages(forceFetch, _vdvLoadKeyMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLoadKeyMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvLoadKeyMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection GetMultiVdvLoadKeyMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvLoadKeyMessages(forceFetch, _vdvLoadKeyMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvLoadKeyMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection GetMultiVdvLoadKeyMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvLoadKeyMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLoadKeyMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection GetMultiVdvLoadKeyMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvLoadKeyMessages || forceFetch || _alwaysFetchVdvLoadKeyMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvLoadKeyMessages);
				_vdvLoadKeyMessages.SuppressClearInGetMulti=!forceFetch;
				_vdvLoadKeyMessages.EntityFactoryToUse = entityFactoryToUse;
				_vdvLoadKeyMessages.GetMultiManyToOne(this, filter);
				_vdvLoadKeyMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvLoadKeyMessages = true;
			}
			return _vdvLoadKeyMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvLoadKeyMessages'. These settings will be taken into account
		/// when the property VdvLoadKeyMessages is requested or GetMultiVdvLoadKeyMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvLoadKeyMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvLoadKeyMessages.SortClauses=sortClauses;
			_vdvLoadKeyMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvSamStatusEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvSamStatusEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvSamStatusCollection GetMultiVdvSamStatuses(bool forceFetch)
		{
			return GetMultiVdvSamStatuses(forceFetch, _vdvSamStatuses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvSamStatusEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvSamStatusEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvSamStatusCollection GetMultiVdvSamStatuses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvSamStatuses(forceFetch, _vdvSamStatuses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvSamStatusEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvSamStatusCollection GetMultiVdvSamStatuses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvSamStatuses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvSamStatusEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvSamStatusCollection GetMultiVdvSamStatuses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvSamStatuses || forceFetch || _alwaysFetchVdvSamStatuses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvSamStatuses);
				_vdvSamStatuses.SuppressClearInGetMulti=!forceFetch;
				_vdvSamStatuses.EntityFactoryToUse = entityFactoryToUse;
				_vdvSamStatuses.GetMultiManyToOne(null, this, filter);
				_vdvSamStatuses.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvSamStatuses = true;
			}
			return _vdvSamStatuses;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvSamStatuses'. These settings will be taken into account
		/// when the property VdvSamStatuses is requested or GetMultiVdvSamStatuses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvSamStatuses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvSamStatuses.SortClauses=sortClauses;
			_vdvSamStatuses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public DeviceEntity GetSingleDevice()
		{
			return GetSingleDevice(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceEntity' which is related to this entity.</returns>
		public virtual DeviceEntity GetSingleDevice(bool forceFetch)
		{
			if( ( !_alreadyFetchedDevice || forceFetch || _alwaysFetchDevice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceEntityUsingDeviceID);
				DeviceEntity newEntity = new DeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Device = newEntity;
				_alreadyFetchedDevice = fetchResult;
			}
			return _device;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Device", _device);
			toReturn.Add("VdvKeyInfos", _vdvKeyInfos);
			toReturn.Add("VdvLoadKeyMessages", _vdvLoadKeyMessages);
			toReturn.Add("VdvSamStatuses", _vdvSamStatuses);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="validator">The validator object for this SamModuleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 samID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(samID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_vdvKeyInfos = new VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection();
			_vdvKeyInfos.SetContainingEntityInfo(this, "SamModule");

			_vdvLoadKeyMessages = new VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection();
			_vdvLoadKeyMessages.SetContainingEntityInfo(this, "SamModule");

			_vdvSamStatuses = new VarioSL.Entities.CollectionClasses.VdvSamStatusCollection();
			_vdvSamStatuses.SetContainingEntityInfo(this, "SamModule");
			_deviceReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Counter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Info", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBlocked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LossDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LossDetectionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Slot", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTil", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _device</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDevice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _device, new PropertyChangedEventHandler( OnDevicePropertyChanged ), "Device", VarioSL.Entities.RelationClasses.StaticSamModuleRelations.DeviceEntityUsingDeviceIDStatic, true, signalRelatedEntity, "SamModules", resetFKFields, new int[] { (int)SamModuleFieldIndex.DeviceID } );		
			_device = null;
		}
		
		/// <summary> setups the sync logic for member _device</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDevice(IEntityCore relatedEntity)
		{
			if(_device!=relatedEntity)
			{		
				DesetupSyncDevice(true, true);
				_device = (DeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _device, new PropertyChangedEventHandler( OnDevicePropertyChanged ), "Device", VarioSL.Entities.RelationClasses.StaticSamModuleRelations.DeviceEntityUsingDeviceIDStatic, true, ref _alreadyFetchedDevice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDevicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="samID">PK value for SamModule which data should be fetched into this SamModule object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 samID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SamModuleFieldIndex.SamID].ForcedCurrentValueWrite(samID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSamModuleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SamModuleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SamModuleRelations Relations
		{
			get	{ return new SamModuleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvKeyInfo' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvKeyInfos
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection(), (IEntityRelation)GetRelationsForField("VdvKeyInfos")[0], (int)VarioSL.Entities.EntityType.SamModuleEntity, (int)VarioSL.Entities.EntityType.VdvKeyInfoEntity, 0, null, null, null, "VdvKeyInfos", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLoadKeyMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvLoadKeyMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection(), (IEntityRelation)GetRelationsForField("VdvLoadKeyMessages")[0], (int)VarioSL.Entities.EntityType.SamModuleEntity, (int)VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity, 0, null, null, null, "VdvLoadKeyMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvSamStatus' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvSamStatuses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvSamStatusCollection(), (IEntityRelation)GetRelationsForField("VdvSamStatuses")[0], (int)VarioSL.Entities.EntityType.SamModuleEntity, (int)VarioSL.Entities.EntityType.VdvSamStatusEntity, 0, null, null, null, "VdvSamStatuses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("Device")[0], (int)VarioSL.Entities.EntityType.SamModuleEntity, (int)VarioSL.Entities.EntityType.DeviceEntity, 0, null, null, null, "Device", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BlockingReason property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."BLOCKINGREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlockingReason
		{
			get { return (System.String)GetValue((int)SamModuleFieldIndex.BlockingReason, true); }
			set	{ SetValue((int)SamModuleFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The Counter property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."COUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Counter
		{
			get { return (Nullable<System.Int64>)GetValue((int)SamModuleFieldIndex.Counter, false); }
			set	{ SetValue((int)SamModuleFieldIndex.Counter, value, true); }
		}

		/// <summary> The DeviceID property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."DEVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SamModuleFieldIndex.DeviceID, false); }
			set	{ SetValue((int)SamModuleFieldIndex.DeviceID, value, true); }
		}

		/// <summary> The Info property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."INFO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Info
		{
			get { return (System.String)GetValue((int)SamModuleFieldIndex.Info, true); }
			set	{ SetValue((int)SamModuleFieldIndex.Info, value, true); }
		}

		/// <summary> The IsBlocked property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."ISBLOCKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBlocked
		{
			get { return (System.Boolean)GetValue((int)SamModuleFieldIndex.IsBlocked, true); }
			set	{ SetValue((int)SamModuleFieldIndex.IsBlocked, value, true); }
		}

		/// <summary> The LastData property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."LASTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastData
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SamModuleFieldIndex.LastData, false); }
			set	{ SetValue((int)SamModuleFieldIndex.LastData, value, true); }
		}

		/// <summary> The LossDate property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."LOSSDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LossDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SamModuleFieldIndex.LossDate, false); }
			set	{ SetValue((int)SamModuleFieldIndex.LossDate, value, true); }
		}

		/// <summary> The LossDetectionDate property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."LOSSDETECTIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LossDetectionDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SamModuleFieldIndex.LossDetectionDate, false); }
			set	{ SetValue((int)SamModuleFieldIndex.LossDetectionDate, value, true); }
		}

		/// <summary> The SamID property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."SAMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 SamID
		{
			get { return (System.Int64)GetValue((int)SamModuleFieldIndex.SamID, true); }
			set	{ SetValue((int)SamModuleFieldIndex.SamID, value, true); }
		}

		/// <summary> The SamNumber property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."SAMNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SamNumber
		{
			get { return (System.String)GetValue((int)SamModuleFieldIndex.SamNumber, true); }
			set	{ SetValue((int)SamModuleFieldIndex.SamNumber, value, true); }
		}

		/// <summary> The Slot property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."SLOT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Slot
		{
			get { return (Nullable<System.Int32>)GetValue((int)SamModuleFieldIndex.Slot, false); }
			set	{ SetValue((int)SamModuleFieldIndex.Slot, value, true); }
		}

		/// <summary> The State property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.SamModuleStatus> State
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.SamModuleStatus>)GetValue((int)SamModuleFieldIndex.State, false); }
			set	{ SetValue((int)SamModuleFieldIndex.State, value, true); }
		}

		/// <summary> The Type property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."TYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Type
		{
			get { return (System.String)GetValue((int)SamModuleFieldIndex.Type, true); }
			set	{ SetValue((int)SamModuleFieldIndex.Type, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SamModuleFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)SamModuleFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTil property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."VALIDTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidTil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SamModuleFieldIndex.ValidTil, false); }
			set	{ SetValue((int)SamModuleFieldIndex.ValidTil, value, true); }
		}

		/// <summary> The Version property of the Entity SamModule<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_SAM"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Version
		{
			get { return (System.String)GetValue((int)SamModuleFieldIndex.Version, true); }
			set	{ SetValue((int)SamModuleFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'VdvKeyInfoEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvKeyInfos()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeyInfoCollection VdvKeyInfos
		{
			get	{ return GetMultiVdvKeyInfos(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvKeyInfos. When set to true, VdvKeyInfos is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvKeyInfos is accessed. You can always execute/ a forced fetch by calling GetMultiVdvKeyInfos(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvKeyInfos
		{
			get	{ return _alwaysFetchVdvKeyInfos; }
			set	{ _alwaysFetchVdvKeyInfos = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvKeyInfos already has been fetched. Setting this property to false when VdvKeyInfos has been fetched
		/// will clear the VdvKeyInfos collection well. Setting this property to true while VdvKeyInfos hasn't been fetched disables lazy loading for VdvKeyInfos</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvKeyInfos
		{
			get { return _alreadyFetchedVdvKeyInfos;}
			set 
			{
				if(_alreadyFetchedVdvKeyInfos && !value && (_vdvKeyInfos != null))
				{
					_vdvKeyInfos.Clear();
				}
				_alreadyFetchedVdvKeyInfos = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvLoadKeyMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvLoadKeyMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvLoadKeyMessageCollection VdvLoadKeyMessages
		{
			get	{ return GetMultiVdvLoadKeyMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvLoadKeyMessages. When set to true, VdvLoadKeyMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvLoadKeyMessages is accessed. You can always execute/ a forced fetch by calling GetMultiVdvLoadKeyMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvLoadKeyMessages
		{
			get	{ return _alwaysFetchVdvLoadKeyMessages; }
			set	{ _alwaysFetchVdvLoadKeyMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvLoadKeyMessages already has been fetched. Setting this property to false when VdvLoadKeyMessages has been fetched
		/// will clear the VdvLoadKeyMessages collection well. Setting this property to true while VdvLoadKeyMessages hasn't been fetched disables lazy loading for VdvLoadKeyMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvLoadKeyMessages
		{
			get { return _alreadyFetchedVdvLoadKeyMessages;}
			set 
			{
				if(_alreadyFetchedVdvLoadKeyMessages && !value && (_vdvLoadKeyMessages != null))
				{
					_vdvLoadKeyMessages.Clear();
				}
				_alreadyFetchedVdvLoadKeyMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvSamStatusEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvSamStatuses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvSamStatusCollection VdvSamStatuses
		{
			get	{ return GetMultiVdvSamStatuses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvSamStatuses. When set to true, VdvSamStatuses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvSamStatuses is accessed. You can always execute/ a forced fetch by calling GetMultiVdvSamStatuses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvSamStatuses
		{
			get	{ return _alwaysFetchVdvSamStatuses; }
			set	{ _alwaysFetchVdvSamStatuses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvSamStatuses already has been fetched. Setting this property to false when VdvSamStatuses has been fetched
		/// will clear the VdvSamStatuses collection well. Setting this property to true while VdvSamStatuses hasn't been fetched disables lazy loading for VdvSamStatuses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvSamStatuses
		{
			get { return _alreadyFetchedVdvSamStatuses;}
			set 
			{
				if(_alreadyFetchedVdvSamStatuses && !value && (_vdvSamStatuses != null))
				{
					_vdvSamStatuses.Clear();
				}
				_alreadyFetchedVdvSamStatuses = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDevice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceEntity Device
		{
			get	{ return GetSingleDevice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDevice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SamModules", "Device", _device, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Device. When set to true, Device is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Device is accessed. You can always execute a forced fetch by calling GetSingleDevice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevice
		{
			get	{ return _alwaysFetchDevice; }
			set	{ _alwaysFetchDevice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Device already has been fetched. Setting this property to false when Device has been fetched
		/// will set Device to null as well. Setting this property to true while Device hasn't been fetched disables lazy loading for Device</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevice
		{
			get { return _alreadyFetchedDevice;}
			set 
			{
				if(_alreadyFetchedDevice && !value)
				{
					this.Device = null;
				}
				_alreadyFetchedDevice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Device is not found
		/// in the database. When set to true, Device will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceReturnsNewIfNotFound
		{
			get	{ return _deviceReturnsNewIfNotFound; }
			set { _deviceReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SamModuleEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
