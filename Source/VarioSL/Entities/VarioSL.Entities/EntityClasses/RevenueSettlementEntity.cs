﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RevenueSettlement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RevenueSettlementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection	_revenueRecognition;
		private bool	_alwaysFetchRevenueRecognition, _alreadyFetchedRevenueRecognition;
		private VarioSL.Entities.CollectionClasses.SettledRevenueCollection	_settledRevenues;
		private bool	_alwaysFetchSettledRevenues, _alreadyFetchedSettledRevenues;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private CloseoutPeriodEntity _closeoutPeriod;
		private bool	_alwaysFetchCloseoutPeriod, _alreadyFetchedCloseoutPeriod, _closeoutPeriodReturnsNewIfNotFound;
		private TransactionJournalEntity _transactionJournal;
		private bool	_alwaysFetchTransactionJournal, _alreadyFetchedTransactionJournal, _transactionJournalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name CloseoutPeriod</summary>
			public static readonly string CloseoutPeriod = "CloseoutPeriod";
			/// <summary>Member name TransactionJournal</summary>
			public static readonly string TransactionJournal = "TransactionJournal";
			/// <summary>Member name RevenueRecognition</summary>
			public static readonly string RevenueRecognition = "RevenueRecognition";
			/// <summary>Member name SettledRevenues</summary>
			public static readonly string SettledRevenues = "SettledRevenues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RevenueSettlementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RevenueSettlementEntity() :base("RevenueSettlementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		public RevenueSettlementEntity(System.Int64 revenueSettlementID):base("RevenueSettlementEntity")
		{
			InitClassFetch(revenueSettlementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RevenueSettlementEntity(System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse):base("RevenueSettlementEntity")
		{
			InitClassFetch(revenueSettlementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="validator">The custom validator object for this RevenueSettlementEntity</param>
		public RevenueSettlementEntity(System.Int64 revenueSettlementID, IValidator validator):base("RevenueSettlementEntity")
		{
			InitClassFetch(revenueSettlementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RevenueSettlementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_revenueRecognition = (VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection)info.GetValue("_revenueRecognition", typeof(VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection));
			_alwaysFetchRevenueRecognition = info.GetBoolean("_alwaysFetchRevenueRecognition");
			_alreadyFetchedRevenueRecognition = info.GetBoolean("_alreadyFetchedRevenueRecognition");

			_settledRevenues = (VarioSL.Entities.CollectionClasses.SettledRevenueCollection)info.GetValue("_settledRevenues", typeof(VarioSL.Entities.CollectionClasses.SettledRevenueCollection));
			_alwaysFetchSettledRevenues = info.GetBoolean("_alwaysFetchSettledRevenues");
			_alreadyFetchedSettledRevenues = info.GetBoolean("_alreadyFetchedSettledRevenues");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_closeoutPeriod = (CloseoutPeriodEntity)info.GetValue("_closeoutPeriod", typeof(CloseoutPeriodEntity));
			if(_closeoutPeriod!=null)
			{
				_closeoutPeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_closeoutPeriodReturnsNewIfNotFound = info.GetBoolean("_closeoutPeriodReturnsNewIfNotFound");
			_alwaysFetchCloseoutPeriod = info.GetBoolean("_alwaysFetchCloseoutPeriod");
			_alreadyFetchedCloseoutPeriod = info.GetBoolean("_alreadyFetchedCloseoutPeriod");

			_transactionJournal = (TransactionJournalEntity)info.GetValue("_transactionJournal", typeof(TransactionJournalEntity));
			if(_transactionJournal!=null)
			{
				_transactionJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transactionJournalReturnsNewIfNotFound = info.GetBoolean("_transactionJournalReturnsNewIfNotFound");
			_alwaysFetchTransactionJournal = info.GetBoolean("_alwaysFetchTransactionJournal");
			_alreadyFetchedTransactionJournal = info.GetBoolean("_alreadyFetchedTransactionJournal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RevenueSettlementFieldIndex)fieldIndex)
			{
				case RevenueSettlementFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case RevenueSettlementFieldIndex.CloseoutPeriodID:
					DesetupSyncCloseoutPeriod(true, false);
					_alreadyFetchedCloseoutPeriod = false;
					break;
				case RevenueSettlementFieldIndex.TransactionJournalID:
					DesetupSyncTransactionJournal(true, false);
					_alreadyFetchedTransactionJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRevenueRecognition = (_revenueRecognition.Count > 0);
			_alreadyFetchedSettledRevenues = (_settledRevenues.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedCloseoutPeriod = (_closeoutPeriod != null);
			_alreadyFetchedTransactionJournal = (_transactionJournal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "CloseoutPeriod":
					toReturn.Add(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
					break;
				case "TransactionJournal":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalID);
					break;
				case "RevenueRecognition":
					toReturn.Add(Relations.RevenueRecognitionEntityUsingRevenueSettlementID);
					break;
				case "SettledRevenues":
					toReturn.Add(Relations.SettledRevenueEntityUsingRevenueSettlementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_revenueRecognition", (!this.MarkedForDeletion?_revenueRecognition:null));
			info.AddValue("_alwaysFetchRevenueRecognition", _alwaysFetchRevenueRecognition);
			info.AddValue("_alreadyFetchedRevenueRecognition", _alreadyFetchedRevenueRecognition);
			info.AddValue("_settledRevenues", (!this.MarkedForDeletion?_settledRevenues:null));
			info.AddValue("_alwaysFetchSettledRevenues", _alwaysFetchSettledRevenues);
			info.AddValue("_alreadyFetchedSettledRevenues", _alreadyFetchedSettledRevenues);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_closeoutPeriod", (!this.MarkedForDeletion?_closeoutPeriod:null));
			info.AddValue("_closeoutPeriodReturnsNewIfNotFound", _closeoutPeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCloseoutPeriod", _alwaysFetchCloseoutPeriod);
			info.AddValue("_alreadyFetchedCloseoutPeriod", _alreadyFetchedCloseoutPeriod);
			info.AddValue("_transactionJournal", (!this.MarkedForDeletion?_transactionJournal:null));
			info.AddValue("_transactionJournalReturnsNewIfNotFound", _transactionJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransactionJournal", _alwaysFetchTransactionJournal);
			info.AddValue("_alreadyFetchedTransactionJournal", _alreadyFetchedTransactionJournal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "CloseoutPeriod":
					_alreadyFetchedCloseoutPeriod = true;
					this.CloseoutPeriod = (CloseoutPeriodEntity)entity;
					break;
				case "TransactionJournal":
					_alreadyFetchedTransactionJournal = true;
					this.TransactionJournal = (TransactionJournalEntity)entity;
					break;
				case "RevenueRecognition":
					_alreadyFetchedRevenueRecognition = true;
					if(entity!=null)
					{
						this.RevenueRecognition.Add((RevenueRecognitionEntity)entity);
					}
					break;
				case "SettledRevenues":
					_alreadyFetchedSettledRevenues = true;
					if(entity!=null)
					{
						this.SettledRevenues.Add((SettledRevenueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "CloseoutPeriod":
					SetupSyncCloseoutPeriod(relatedEntity);
					break;
				case "TransactionJournal":
					SetupSyncTransactionJournal(relatedEntity);
					break;
				case "RevenueRecognition":
					_revenueRecognition.Add((RevenueRecognitionEntity)relatedEntity);
					break;
				case "SettledRevenues":
					_settledRevenues.Add((SettledRevenueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "CloseoutPeriod":
					DesetupSyncCloseoutPeriod(false, true);
					break;
				case "TransactionJournal":
					DesetupSyncTransactionJournal(false, true);
					break;
				case "RevenueRecognition":
					this.PerformRelatedEntityRemoval(_revenueRecognition, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettledRevenues":
					this.PerformRelatedEntityRemoval(_settledRevenues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_closeoutPeriod!=null)
			{
				toReturn.Add(_closeoutPeriod);
			}
			if(_transactionJournal!=null)
			{
				toReturn.Add(_transactionJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_revenueRecognition);
			toReturn.Add(_settledRevenues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueSettlementID)
		{
			return FetchUsingPK(revenueSettlementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(revenueSettlementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(revenueSettlementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(revenueSettlementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RevenueSettlementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new RevenueSettlementRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognition(bool forceFetch)
		{
			return GetMultiRevenueRecognition(forceFetch, _revenueRecognition.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognition(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueRecognition(forceFetch, _revenueRecognition.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognition(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueRecognition(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognition(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueRecognition || forceFetch || _alwaysFetchRevenueRecognition) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueRecognition);
				_revenueRecognition.SuppressClearInGetMulti=!forceFetch;
				_revenueRecognition.EntityFactoryToUse = entityFactoryToUse;
				_revenueRecognition.GetMultiManyToOne(null, this, null, filter);
				_revenueRecognition.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueRecognition = true;
			}
			return _revenueRecognition;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueRecognition'. These settings will be taken into account
		/// when the property RevenueRecognition is requested or GetMultiRevenueRecognition is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueRecognition(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueRecognition.SortClauses=sortClauses;
			_revenueRecognition.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettledRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch)
		{
			return GetMultiSettledRevenues(forceFetch, _settledRevenues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettledRevenueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettledRevenues(forceFetch, _settledRevenues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettledRevenues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettledRevenueCollection GetMultiSettledRevenues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettledRevenues || forceFetch || _alwaysFetchSettledRevenues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settledRevenues);
				_settledRevenues.SuppressClearInGetMulti=!forceFetch;
				_settledRevenues.EntityFactoryToUse = entityFactoryToUse;
				_settledRevenues.GetMultiManyToOne(null, this, filter);
				_settledRevenues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettledRevenues = true;
			}
			return _settledRevenues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettledRevenues'. These settings will be taken into account
		/// when the property SettledRevenues is requested or GetMultiSettledRevenues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettledRevenues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settledRevenues.SortClauses=sortClauses;
			_settledRevenues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public CloseoutPeriodEntity GetSingleCloseoutPeriod()
		{
			return GetSingleCloseoutPeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public virtual CloseoutPeriodEntity GetSingleCloseoutPeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedCloseoutPeriod || forceFetch || _alwaysFetchCloseoutPeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
				CloseoutPeriodEntity newEntity = new CloseoutPeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CloseoutPeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CloseoutPeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_closeoutPeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CloseoutPeriod = newEntity;
				_alreadyFetchedCloseoutPeriod = fetchResult;
			}
			return _closeoutPeriod;
		}


		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleTransactionJournal()
		{
			return GetSingleTransactionJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleTransactionJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransactionJournal || forceFetch || _alwaysFetchTransactionJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalID);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transactionJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransactionJournal = newEntity;
				_alreadyFetchedTransactionJournal = fetchResult;
			}
			return _transactionJournal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("CloseoutPeriod", _closeoutPeriod);
			toReturn.Add("TransactionJournal", _transactionJournal);
			toReturn.Add("RevenueRecognition", _revenueRecognition);
			toReturn.Add("SettledRevenues", _settledRevenues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="validator">The validator object for this RevenueSettlementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 revenueSettlementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(revenueSettlementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_revenueRecognition = new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection();
			_revenueRecognition.SetContainingEntityInfo(this, "RevenueSettlement");

			_settledRevenues = new VarioSL.Entities.CollectionClasses.SettledRevenueCollection();
			_settledRevenues.SetContainingEntityInfo(this, "RevenueSettlement");
			_clientReturnsNewIfNotFound = false;
			_closeoutPeriodReturnsNewIfNotFound = false;
			_transactionJournalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostCenterKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Premium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementDistributionPolicy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapsCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "RevenueSettlements", resetFKFields, new int[] { (int)RevenueSettlementFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _closeoutPeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCloseoutPeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, signalRelatedEntity, "RevenueSettlements", resetFKFields, new int[] { (int)RevenueSettlementFieldIndex.CloseoutPeriodID } );		
			_closeoutPeriod = null;
		}
		
		/// <summary> setups the sync logic for member _closeoutPeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCloseoutPeriod(IEntityCore relatedEntity)
		{
			if(_closeoutPeriod!=relatedEntity)
			{		
				DesetupSyncCloseoutPeriod(true, true);
				_closeoutPeriod = (CloseoutPeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, ref _alreadyFetchedCloseoutPeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCloseoutPeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transactionJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransactionJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, signalRelatedEntity, "RevenueSettlements", resetFKFields, new int[] { (int)RevenueSettlementFieldIndex.TransactionJournalID } );		
			_transactionJournal = null;
		}
		
		/// <summary> setups the sync logic for member _transactionJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransactionJournal(IEntityCore relatedEntity)
		{
			if(_transactionJournal!=relatedEntity)
			{		
				DesetupSyncTransactionJournal(true, true);
				_transactionJournal = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transactionJournal, new PropertyChangedEventHandler( OnTransactionJournalPropertyChanged ), "TransactionJournal", VarioSL.Entities.RelationClasses.StaticRevenueSettlementRelations.TransactionJournalEntityUsingTransactionJournalIDStatic, true, ref _alreadyFetchedTransactionJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransactionJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="revenueSettlementID">PK value for RevenueSettlement which data should be fetched into this RevenueSettlement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)RevenueSettlementFieldIndex.RevenueSettlementID].ForcedCurrentValueWrite(revenueSettlementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRevenueSettlementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new RevenueSettlementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RevenueSettlementRelations Relations
		{
			get	{ return new RevenueSettlementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueRecognition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueRecognition
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection(), (IEntityRelation)GetRelationsForField("RevenueRecognition")[0], (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, 0, null, null, null, "RevenueRecognition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettledRevenue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettledRevenues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettledRevenueCollection(), (IEntityRelation)GetRelationsForField("SettledRevenues")[0], (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, (int)VarioSL.Entities.EntityType.SettledRevenueEntity, 0, null, null, null, "SettledRevenues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloseoutPeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloseoutPeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CloseoutPeriodCollection(), (IEntityRelation)GetRelationsForField("CloseoutPeriod")[0], (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, 0, null, null, null, "CloseoutPeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournal")[0], (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 8, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Amount
		{
			get { return (System.Decimal)GetValue((int)RevenueSettlementFieldIndex.Amount, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.Amount, value, true); }
		}

		/// <summary> The BaseFare property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."BASEFARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BaseFare
		{
			get { return (System.Int64)GetValue((int)RevenueSettlementFieldIndex.BaseFare, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The ClientID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)RevenueSettlementFieldIndex.ClientID, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."CLOSEOUTPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueSettlementFieldIndex.CloseoutPeriodID, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CostCenterKey property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."COSTCENTERKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CostCenterKey
		{
			get { return (System.String)GetValue((int)RevenueSettlementFieldIndex.CostCenterKey, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.CostCenterKey, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."CREDITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)RevenueSettlementFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The CustomerGroup property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."CUSTOMERGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueSettlementFieldIndex.CustomerGroup, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.CustomerGroup, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."DEBITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)RevenueSettlementFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)RevenueSettlementFieldIndex.LastModified, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)RevenueSettlementFieldIndex.LastUser, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."LINEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueSettlementFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OperatorID
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueSettlementFieldIndex.OperatorID, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The PostingDate property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."POSTINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)RevenueSettlementFieldIndex.PostingDate, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."POSTINGREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)RevenueSettlementFieldIndex.PostingReference, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The Premium property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."PREMIUM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Premium
		{
			get { return (System.Int64)GetValue((int)RevenueSettlementFieldIndex.Premium, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.Premium, value, true); }
		}

		/// <summary> The RevenueSettlementID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."REVENUESETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 RevenueSettlementID
		{
			get { return (System.Int64)GetValue((int)RevenueSettlementFieldIndex.RevenueSettlementID, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.RevenueSettlementID, value, true); }
		}

		/// <summary> The SettlementDistributionPolicy property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."SETTLEMENTDISTRIBUTIONPOLICY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SettlementDistributionPolicy SettlementDistributionPolicy
		{
			get { return (VarioSL.Entities.Enumerations.SettlementDistributionPolicy)GetValue((int)RevenueSettlementFieldIndex.SettlementDistributionPolicy, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.SettlementDistributionPolicy, value, true); }
		}

		/// <summary> The SettlementType property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."SETTLEMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SettlementType SettlementType
		{
			get { return (VarioSL.Entities.Enumerations.SettlementType)GetValue((int)RevenueSettlementFieldIndex.SettlementType, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.SettlementType, value, true); }
		}

		/// <summary> The TapsCount property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."TAPSCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TapsCount
		{
			get { return (System.Int64)GetValue((int)RevenueSettlementFieldIndex.TapsCount, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.TapsCount, value, true); }
		}

		/// <summary> The TicketNumber property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."TICKETNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevenueSettlementFieldIndex.TicketNumber, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.TicketNumber, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)RevenueSettlementFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueSettlementFieldIndex.TransactionJournalID, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The TransitAccountID property of the Entity RevenueSettlement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_REVENUESETTLEMENT"."TRANSITACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransitAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)RevenueSettlementFieldIndex.TransitAccountID, false); }
			set	{ SetValue((int)RevenueSettlementFieldIndex.TransitAccountID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueRecognition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection RevenueRecognition
		{
			get	{ return GetMultiRevenueRecognition(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueRecognition. When set to true, RevenueRecognition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueRecognition is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueRecognition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueRecognition
		{
			get	{ return _alwaysFetchRevenueRecognition; }
			set	{ _alwaysFetchRevenueRecognition = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueRecognition already has been fetched. Setting this property to false when RevenueRecognition has been fetched
		/// will clear the RevenueRecognition collection well. Setting this property to true while RevenueRecognition hasn't been fetched disables lazy loading for RevenueRecognition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueRecognition
		{
			get { return _alreadyFetchedRevenueRecognition;}
			set 
			{
				if(_alreadyFetchedRevenueRecognition && !value && (_revenueRecognition != null))
				{
					_revenueRecognition.Clear();
				}
				_alreadyFetchedRevenueRecognition = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettledRevenueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettledRevenues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettledRevenueCollection SettledRevenues
		{
			get	{ return GetMultiSettledRevenues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettledRevenues. When set to true, SettledRevenues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettledRevenues is accessed. You can always execute/ a forced fetch by calling GetMultiSettledRevenues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettledRevenues
		{
			get	{ return _alwaysFetchSettledRevenues; }
			set	{ _alwaysFetchSettledRevenues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettledRevenues already has been fetched. Setting this property to false when SettledRevenues has been fetched
		/// will clear the SettledRevenues collection well. Setting this property to true while SettledRevenues hasn't been fetched disables lazy loading for SettledRevenues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettledRevenues
		{
			get { return _alreadyFetchedSettledRevenues;}
			set 
			{
				if(_alreadyFetchedSettledRevenues && !value && (_settledRevenues != null))
				{
					_settledRevenues.Clear();
				}
				_alreadyFetchedSettledRevenues = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueSettlements", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CloseoutPeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCloseoutPeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CloseoutPeriodEntity CloseoutPeriod
		{
			get	{ return GetSingleCloseoutPeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCloseoutPeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueSettlements", "CloseoutPeriod", _closeoutPeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CloseoutPeriod. When set to true, CloseoutPeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloseoutPeriod is accessed. You can always execute a forced fetch by calling GetSingleCloseoutPeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloseoutPeriod
		{
			get	{ return _alwaysFetchCloseoutPeriod; }
			set	{ _alwaysFetchCloseoutPeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloseoutPeriod already has been fetched. Setting this property to false when CloseoutPeriod has been fetched
		/// will set CloseoutPeriod to null as well. Setting this property to true while CloseoutPeriod hasn't been fetched disables lazy loading for CloseoutPeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloseoutPeriod
		{
			get { return _alreadyFetchedCloseoutPeriod;}
			set 
			{
				if(_alreadyFetchedCloseoutPeriod && !value)
				{
					this.CloseoutPeriod = null;
				}
				_alreadyFetchedCloseoutPeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CloseoutPeriod is not found
		/// in the database. When set to true, CloseoutPeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CloseoutPeriodReturnsNewIfNotFound
		{
			get	{ return _closeoutPeriodReturnsNewIfNotFound; }
			set { _closeoutPeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransactionJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity TransactionJournal
		{
			get	{ return GetSingleTransactionJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransactionJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "RevenueSettlements", "TransactionJournal", _transactionJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournal. When set to true, TransactionJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournal is accessed. You can always execute a forced fetch by calling GetSingleTransactionJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournal
		{
			get	{ return _alwaysFetchTransactionJournal; }
			set	{ _alwaysFetchTransactionJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournal already has been fetched. Setting this property to false when TransactionJournal has been fetched
		/// will set TransactionJournal to null as well. Setting this property to true while TransactionJournal hasn't been fetched disables lazy loading for TransactionJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournal
		{
			get { return _alreadyFetchedTransactionJournal;}
			set 
			{
				if(_alreadyFetchedTransactionJournal && !value)
				{
					this.TransactionJournal = null;
				}
				_alreadyFetchedTransactionJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransactionJournal is not found
		/// in the database. When set to true, TransactionJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransactionJournalReturnsNewIfNotFound
		{
			get	{ return _transactionJournalReturnsNewIfNotFound; }
			set { _transactionJournalReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.RevenueSettlementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
