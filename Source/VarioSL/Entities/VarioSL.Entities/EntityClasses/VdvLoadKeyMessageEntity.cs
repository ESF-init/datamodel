﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvLoadKeyMessage'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvLoadKeyMessageEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private SamModuleEntity _samModule;
		private bool	_alwaysFetchSamModule, _alreadyFetchedSamModule, _samModuleReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SamModule</summary>
			public static readonly string SamModule = "SamModule";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvLoadKeyMessageEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvLoadKeyMessageEntity() :base("VdvLoadKeyMessageEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		public VdvLoadKeyMessageEntity(System.Int64 messageID):base("VdvLoadKeyMessageEntity")
		{
			InitClassFetch(messageID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvLoadKeyMessageEntity(System.Int64 messageID, IPrefetchPath prefetchPathToUse):base("VdvLoadKeyMessageEntity")
		{
			InitClassFetch(messageID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="validator">The custom validator object for this VdvLoadKeyMessageEntity</param>
		public VdvLoadKeyMessageEntity(System.Int64 messageID, IValidator validator):base("VdvLoadKeyMessageEntity")
		{
			InitClassFetch(messageID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvLoadKeyMessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_samModule = (SamModuleEntity)info.GetValue("_samModule", typeof(SamModuleEntity));
			if(_samModule!=null)
			{
				_samModule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_samModuleReturnsNewIfNotFound = info.GetBoolean("_samModuleReturnsNewIfNotFound");
			_alwaysFetchSamModule = info.GetBoolean("_alwaysFetchSamModule");
			_alreadyFetchedSamModule = info.GetBoolean("_alreadyFetchedSamModule");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VdvLoadKeyMessageFieldIndex)fieldIndex)
			{
				case VdvLoadKeyMessageFieldIndex.SamID:
					DesetupSyncSamModule(true, false);
					_alreadyFetchedSamModule = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSamModule = (_samModule != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SamModule":
					toReturn.Add(Relations.SamModuleEntityUsingSamID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_samModule", (!this.MarkedForDeletion?_samModule:null));
			info.AddValue("_samModuleReturnsNewIfNotFound", _samModuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSamModule", _alwaysFetchSamModule);
			info.AddValue("_alreadyFetchedSamModule", _alreadyFetchedSamModule);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SamModule":
					_alreadyFetchedSamModule = true;
					this.SamModule = (SamModuleEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SamModule":
					SetupSyncSamModule(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SamModule":
					DesetupSyncSamModule(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_samModule!=null)
			{
				toReturn.Add(_samModule);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID)
		{
			return FetchUsingPK(messageID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messageID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messageID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messageID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessageID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvLoadKeyMessageRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'SamModuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SamModuleEntity' which is related to this entity.</returns>
		public SamModuleEntity GetSingleSamModule()
		{
			return GetSingleSamModule(false);
		}

		/// <summary> Retrieves the related entity of type 'SamModuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SamModuleEntity' which is related to this entity.</returns>
		public virtual SamModuleEntity GetSingleSamModule(bool forceFetch)
		{
			if( ( !_alreadyFetchedSamModule || forceFetch || _alwaysFetchSamModule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SamModuleEntityUsingSamID);
				SamModuleEntity newEntity = new SamModuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SamID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SamModuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_samModuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SamModule = newEntity;
				_alreadyFetchedSamModule = fetchResult;
			}
			return _samModule;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SamModule", _samModule);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="validator">The validator object for this VdvLoadKeyMessageEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 messageID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messageID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_samModuleReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadKeyCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadKeyNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadKeyResult", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoadKeyStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SamID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _samModule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSamModule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _samModule, new PropertyChangedEventHandler( OnSamModulePropertyChanged ), "SamModule", VarioSL.Entities.RelationClasses.StaticVdvLoadKeyMessageRelations.SamModuleEntityUsingSamIDStatic, true, signalRelatedEntity, "VdvLoadKeyMessages", resetFKFields, new int[] { (int)VdvLoadKeyMessageFieldIndex.SamID } );		
			_samModule = null;
		}
		
		/// <summary> setups the sync logic for member _samModule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSamModule(IEntityCore relatedEntity)
		{
			if(_samModule!=relatedEntity)
			{		
				DesetupSyncSamModule(true, true);
				_samModule = (SamModuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _samModule, new PropertyChangedEventHandler( OnSamModulePropertyChanged ), "SamModule", VarioSL.Entities.RelationClasses.StaticVdvLoadKeyMessageRelations.SamModuleEntityUsingSamIDStatic, true, ref _alreadyFetchedSamModule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSamModulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messageID">PK value for VdvLoadKeyMessage which data should be fetched into this VdvLoadKeyMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 messageID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvLoadKeyMessageFieldIndex.MessageID].ForcedCurrentValueWrite(messageID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvLoadKeyMessageDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvLoadKeyMessageEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvLoadKeyMessageRelations Relations
		{
			get	{ return new VdvLoadKeyMessageRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SamModule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSamModule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SamModuleCollection(), (IEntityRelation)GetRelationsForField("SamModule")[0], (int)VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity, (int)VarioSL.Entities.EntityType.SamModuleEntity, 0, null, null, null, "SamModule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LoadKeyCounter property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."LOADKEYCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LoadKeyCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyCounter, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyCounter, value, true); }
		}

		/// <summary> The LoadKeyNumber property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."LOADKEYNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LoadKeyNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyNumber, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyNumber, value, true); }
		}

		/// <summary> The LoadKeyResult property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."LOADKEYRESULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CryptogramErrorType> LoadKeyResult
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CryptogramErrorType>)GetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyResult, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyResult, value, true); }
		}

		/// <summary> The LoadKeyStatus property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."LOADKEYSTATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CryptogramLoadStatus> LoadKeyStatus
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CryptogramLoadStatus>)GetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyStatus, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.LoadKeyStatus, value, true); }
		}

		/// <summary> The MessageDate property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."MESSAGEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> MessageDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VdvLoadKeyMessageFieldIndex.MessageDate, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.MessageDate, value, true); }
		}

		/// <summary> The MessageID property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."MESSAGEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 MessageID
		{
			get { return (System.Int64)GetValue((int)VdvLoadKeyMessageFieldIndex.MessageID, true); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.MessageID, value, true); }
		}

		/// <summary> The SamID property of the Entity VdvLoadKeyMessage<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VDV_LOADKEYMESSAGE"."SAMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SamID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvLoadKeyMessageFieldIndex.SamID, false); }
			set	{ SetValue((int)VdvLoadKeyMessageFieldIndex.SamID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'SamModuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSamModule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SamModuleEntity SamModule
		{
			get	{ return GetSingleSamModule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSamModule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvLoadKeyMessages", "SamModule", _samModule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SamModule. When set to true, SamModule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SamModule is accessed. You can always execute a forced fetch by calling GetSingleSamModule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSamModule
		{
			get	{ return _alwaysFetchSamModule; }
			set	{ _alwaysFetchSamModule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SamModule already has been fetched. Setting this property to false when SamModule has been fetched
		/// will set SamModule to null as well. Setting this property to true while SamModule hasn't been fetched disables lazy loading for SamModule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSamModule
		{
			get { return _alreadyFetchedSamModule;}
			set 
			{
				if(_alreadyFetchedSamModule && !value)
				{
					this.SamModule = null;
				}
				_alreadyFetchedSamModule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SamModule is not found
		/// in the database. When set to true, SamModule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SamModuleReturnsNewIfNotFound
		{
			get	{ return _samModuleReturnsNewIfNotFound; }
			set { _samModuleReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
