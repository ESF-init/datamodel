﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TransactionExtension'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TransactionExtensionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TransactionEntity _mainTransaction;
		private bool	_alwaysFetchMainTransaction, _alreadyFetchedMainTransaction, _mainTransactionReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name MainTransaction</summary>
			public static readonly string MainTransaction = "MainTransaction";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TransactionExtensionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionExtensionEntity() :base("TransactionExtensionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		public TransactionExtensionEntity(System.Int64 transactionID):base("TransactionExtensionEntity")
		{
			InitClassFetch(transactionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TransactionExtensionEntity(System.Int64 transactionID, IPrefetchPath prefetchPathToUse):base("TransactionExtensionEntity")
		{
			InitClassFetch(transactionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="validator">The custom validator object for this TransactionExtensionEntity</param>
		public TransactionExtensionEntity(System.Int64 transactionID, IValidator validator):base("TransactionExtensionEntity")
		{
			InitClassFetch(transactionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionExtensionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_mainTransaction = (TransactionEntity)info.GetValue("_mainTransaction", typeof(TransactionEntity));
			if(_mainTransaction!=null)
			{
				_mainTransaction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mainTransactionReturnsNewIfNotFound = info.GetBoolean("_mainTransactionReturnsNewIfNotFound");
			_alwaysFetchMainTransaction = info.GetBoolean("_alwaysFetchMainTransaction");
			_alreadyFetchedMainTransaction = info.GetBoolean("_alreadyFetchedMainTransaction");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TransactionExtensionFieldIndex)fieldIndex)
			{
				case TransactionExtensionFieldIndex.TransactionID:
					DesetupSyncMainTransaction(true, false);
					_alreadyFetchedMainTransaction = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMainTransaction = (_mainTransaction != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "MainTransaction":
					toReturn.Add(Relations.TransactionEntityUsingTransactionID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			info.AddValue("_mainTransaction", (!this.MarkedForDeletion?_mainTransaction:null));
			info.AddValue("_mainTransactionReturnsNewIfNotFound", _mainTransactionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMainTransaction", _alwaysFetchMainTransaction);
			info.AddValue("_alreadyFetchedMainTransaction", _alreadyFetchedMainTransaction);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "MainTransaction":
					_alreadyFetchedMainTransaction = true;
					this.MainTransaction = (TransactionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "MainTransaction":
					SetupSyncMainTransaction(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "MainTransaction":
					DesetupSyncMainTransaction(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_mainTransaction!=null)
			{
				toReturn.Add(_mainTransaction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID)
		{
			return FetchUsingPK(transactionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(transactionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(transactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TransactionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TransactionExtensionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public TransactionEntity GetSingleMainTransaction()
		{
			return GetSingleMainTransaction(false);
		}
		
		/// <summary> Retrieves the related entity of type 'TransactionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionEntity' which is related to this entity.</returns>
		public virtual TransactionEntity GetSingleMainTransaction(bool forceFetch)
		{
			if( ( !_alreadyFetchedMainTransaction || forceFetch || _alwaysFetchMainTransaction) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionEntityUsingTransactionID);
				TransactionEntity newEntity = new TransactionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransactionID);
				}
				if(fetchResult)
				{
					newEntity = (TransactionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mainTransactionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MainTransaction = newEntity;
				_alreadyFetchedMainTransaction = fetchResult;
			}
			return _mainTransaction;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("MainTransaction", _mainTransaction);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="validator">The validator object for this TransactionExtensionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 transactionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(transactionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_mainTransactionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Note", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Tripcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Trippurpose", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _mainTransaction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMainTransaction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mainTransaction, new PropertyChangedEventHandler( OnMainTransactionPropertyChanged ), "MainTransaction", VarioSL.Entities.RelationClasses.StaticTransactionExtensionRelations.TransactionEntityUsingTransactionIDStatic, true, signalRelatedEntity, "TransactionExtension", false, new int[] { (int)TransactionExtensionFieldIndex.TransactionID } );
			_mainTransaction = null;
		}
	
		/// <summary> setups the sync logic for member _mainTransaction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMainTransaction(IEntityCore relatedEntity)
		{
			if(_mainTransaction!=relatedEntity)
			{
				DesetupSyncMainTransaction(true, true);
				_mainTransaction = (TransactionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mainTransaction, new PropertyChangedEventHandler( OnMainTransactionPropertyChanged ), "MainTransaction", VarioSL.Entities.RelationClasses.StaticTransactionExtensionRelations.TransactionEntityUsingTransactionIDStatic, true, ref _alreadyFetchedMainTransaction, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMainTransactionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="transactionID">PK value for TransactionExtension which data should be fetched into this TransactionExtension object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 transactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TransactionExtensionFieldIndex.TransactionID].ForcedCurrentValueWrite(transactionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTransactionExtensionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TransactionExtensionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TransactionExtensionRelations Relations
		{
			get	{ return new TransactionExtensionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Transaction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMainTransaction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionCollection(), (IEntityRelation)GetRelationsForField("MainTransaction")[0], (int)VarioSL.Entities.EntityType.TransactionExtensionEntity, (int)VarioSL.Entities.EntityType.TransactionEntity, 0, null, null, null, "MainTransaction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Code property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."CODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Code
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.Code, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Code, value, true); }
		}

		/// <summary> The Latitude property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."LATITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 12, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Latitude
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionExtensionFieldIndex.Latitude, false); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Latitude, value, true); }
		}

		/// <summary> The Longitude property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."LONGITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 12, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Longitude
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionExtensionFieldIndex.Longitude, false); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Longitude, value, true); }
		}

		/// <summary> The Note property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."NOTE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Note
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.Note, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Note, value, true); }
		}

		/// <summary> The RouteCode property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."ROUTECODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RouteCode
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.RouteCode, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.RouteCode, value, true); }
		}

		/// <summary> The TicketReference property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."TICKETREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TicketReference
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.TicketReference, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.TicketReference, value, true); }
		}

		/// <summary> The TransactionID property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TransactionID
		{
			get { return (System.Int64)GetValue((int)TransactionExtensionFieldIndex.TransactionID, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The Tripcode property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."TRIPCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Tripcode
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.Tripcode, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Tripcode, value, true); }
		}

		/// <summary> The Trippurpose property of the Entity TransactionExtension<br/><br/></summary>
		/// <remarks>Mapped on  table field: "RM_EXT_TRANSACTION"."TRIPPURPOSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Trippurpose
		{
			get { return (System.String)GetValue((int)TransactionExtensionFieldIndex.Trippurpose, true); }
			set	{ SetValue((int)TransactionExtensionFieldIndex.Trippurpose, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'TransactionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMainTransaction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionEntity MainTransaction
		{
			get	{ return GetSingleMainTransaction(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncMainTransaction(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_mainTransaction !=null);
						DesetupSyncMainTransaction(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("MainTransaction");
						}
					}
					else
					{
						if(_mainTransaction!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TransactionExtension");
							SetupSyncMainTransaction(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MainTransaction. When set to true, MainTransaction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MainTransaction is accessed. You can always execute a forced fetch by calling GetSingleMainTransaction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMainTransaction
		{
			get	{ return _alwaysFetchMainTransaction; }
			set	{ _alwaysFetchMainTransaction = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property MainTransaction already has been fetched. Setting this property to false when MainTransaction has been fetched
		/// will set MainTransaction to null as well. Setting this property to true while MainTransaction hasn't been fetched disables lazy loading for MainTransaction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMainTransaction
		{
			get { return _alreadyFetchedMainTransaction;}
			set 
			{
				if(_alreadyFetchedMainTransaction && !value)
				{
					this.MainTransaction = null;
				}
				_alreadyFetchedMainTransaction = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MainTransaction is not found
		/// in the database. When set to true, MainTransaction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MainTransactionReturnsNewIfNotFound
		{
			get	{ return _mainTransactionReturnsNewIfNotFound; }
			set	{ _mainTransactionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TransactionExtensionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
