﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TransactionJournal'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TransactionJournalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ApportionCollection	_apportions;
		private bool	_alwaysFetchApportions, _alreadyFetchedApportions;
		private VarioSL.Entities.CollectionClasses.CappingJournalCollection	_cappingJournals;
		private bool	_alwaysFetchCappingJournals, _alreadyFetchedCappingJournals;
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection	_revenueRecognitions;
		private bool	_alwaysFetchRevenueRecognitions, _alreadyFetchedRevenueRecognitions;
		private VarioSL.Entities.CollectionClasses.RevenueSettlementCollection	_revenueSettlements;
		private bool	_alwaysFetchRevenueSettlements, _alreadyFetchedRevenueSettlements;
		private VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection	_ruleViolationToTransactions;
		private bool	_alwaysFetchRuleViolationToTransactions, _alreadyFetchedRuleViolationToTransactions;

		private VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection	_transactionJournalCorrections;
		private bool	_alwaysFetchTransactionJournalCorrections, _alreadyFetchedTransactionJournalCorrections;
		private VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection	_transactionToInspections;
		private bool	_alwaysFetchTransactionToInspections, _alreadyFetchedTransactionToInspections;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private TicketEntity _appliedPassTicket;
		private bool	_alwaysFetchAppliedPassTicket, _alreadyFetchedAppliedPassTicket, _appliedPassTicketReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private TicketEntity _ticket_;
		private bool	_alwaysFetchTicket_, _alreadyFetchedTicket_, _ticket_ReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private CardEntity _card_;
		private bool	_alwaysFetchCard_, _alreadyFetchedCard_, _card_ReturnsNewIfNotFound;
		private CardEntity _tokenCard;
		private bool	_alwaysFetchTokenCard, _alreadyFetchedTokenCard, _tokenCardReturnsNewIfNotFound;
		private CardHolderEntity _cardHolder;
		private bool	_alwaysFetchCardHolder, _alreadyFetchedCardHolder, _cardHolderReturnsNewIfNotFound;
		private CreditCardAuthorizationEntity _creditCardAuthorization;
		private bool	_alwaysFetchCreditCardAuthorization, _alreadyFetchedCreditCardAuthorization, _creditCardAuthorizationReturnsNewIfNotFound;
		private ProductEntity _appliedPassProduct;
		private bool	_alwaysFetchAppliedPassProduct, _alreadyFetchedAppliedPassProduct, _appliedPassProductReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;
		private SaleEntity _sale;
		private bool	_alwaysFetchSale, _alreadyFetchedSale, _saleReturnsNewIfNotFound;
		private TransactionJournalEntity _cancellationReferenceTransaction;
		private bool	_alwaysFetchCancellationReferenceTransaction, _alreadyFetchedCancellationReferenceTransaction, _cancellationReferenceTransactionReturnsNewIfNotFound;
		private SalesRevenueEntity _salesRevenue;
		private bool	_alwaysFetchSalesRevenue, _alreadyFetchedSalesRevenue, _salesRevenueReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name AppliedPassTicket</summary>
			public static readonly string AppliedPassTicket = "AppliedPassTicket";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name Ticket_</summary>
			public static readonly string Ticket_ = "Ticket_";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Card_</summary>
			public static readonly string Card_ = "Card_";
			/// <summary>Member name TokenCard</summary>
			public static readonly string TokenCard = "TokenCard";
			/// <summary>Member name CardHolder</summary>
			public static readonly string CardHolder = "CardHolder";
			/// <summary>Member name CreditCardAuthorization</summary>
			public static readonly string CreditCardAuthorization = "CreditCardAuthorization";
			/// <summary>Member name AppliedPassProduct</summary>
			public static readonly string AppliedPassProduct = "AppliedPassProduct";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name Sale</summary>
			public static readonly string Sale = "Sale";
			/// <summary>Member name CancellationReferenceTransaction</summary>
			public static readonly string CancellationReferenceTransaction = "CancellationReferenceTransaction";
			/// <summary>Member name Apportions</summary>
			public static readonly string Apportions = "Apportions";
			/// <summary>Member name CappingJournals</summary>
			public static readonly string CappingJournals = "CappingJournals";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name RevenueRecognitions</summary>
			public static readonly string RevenueRecognitions = "RevenueRecognitions";
			/// <summary>Member name RevenueSettlements</summary>
			public static readonly string RevenueSettlements = "RevenueSettlements";
			/// <summary>Member name RuleViolationToTransactions</summary>
			public static readonly string RuleViolationToTransactions = "RuleViolationToTransactions";

			/// <summary>Member name TransactionJournalCorrections</summary>
			public static readonly string TransactionJournalCorrections = "TransactionJournalCorrections";
			/// <summary>Member name TransactionToInspections</summary>
			public static readonly string TransactionToInspections = "TransactionToInspections";
			/// <summary>Member name SalesRevenue</summary>
			public static readonly string SalesRevenue = "SalesRevenue";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TransactionJournalEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionJournalEntity() :base("TransactionJournalEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		public TransactionJournalEntity(System.Int64 transactionJournalID):base("TransactionJournalEntity")
		{
			InitClassFetch(transactionJournalID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TransactionJournalEntity(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse):base("TransactionJournalEntity")
		{
			InitClassFetch(transactionJournalID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="validator">The custom validator object for this TransactionJournalEntity</param>
		public TransactionJournalEntity(System.Int64 transactionJournalID, IValidator validator):base("TransactionJournalEntity")
		{
			InitClassFetch(transactionJournalID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionJournalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportions = (VarioSL.Entities.CollectionClasses.ApportionCollection)info.GetValue("_apportions", typeof(VarioSL.Entities.CollectionClasses.ApportionCollection));
			_alwaysFetchApportions = info.GetBoolean("_alwaysFetchApportions");
			_alreadyFetchedApportions = info.GetBoolean("_alreadyFetchedApportions");

			_cappingJournals = (VarioSL.Entities.CollectionClasses.CappingJournalCollection)info.GetValue("_cappingJournals", typeof(VarioSL.Entities.CollectionClasses.CappingJournalCollection));
			_alwaysFetchCappingJournals = info.GetBoolean("_alwaysFetchCappingJournals");
			_alreadyFetchedCappingJournals = info.GetBoolean("_alreadyFetchedCappingJournals");

			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_revenueRecognitions = (VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection)info.GetValue("_revenueRecognitions", typeof(VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection));
			_alwaysFetchRevenueRecognitions = info.GetBoolean("_alwaysFetchRevenueRecognitions");
			_alreadyFetchedRevenueRecognitions = info.GetBoolean("_alreadyFetchedRevenueRecognitions");

			_revenueSettlements = (VarioSL.Entities.CollectionClasses.RevenueSettlementCollection)info.GetValue("_revenueSettlements", typeof(VarioSL.Entities.CollectionClasses.RevenueSettlementCollection));
			_alwaysFetchRevenueSettlements = info.GetBoolean("_alwaysFetchRevenueSettlements");
			_alreadyFetchedRevenueSettlements = info.GetBoolean("_alreadyFetchedRevenueSettlements");

			_ruleViolationToTransactions = (VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection)info.GetValue("_ruleViolationToTransactions", typeof(VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection));
			_alwaysFetchRuleViolationToTransactions = info.GetBoolean("_alwaysFetchRuleViolationToTransactions");
			_alreadyFetchedRuleViolationToTransactions = info.GetBoolean("_alreadyFetchedRuleViolationToTransactions");


			_transactionJournalCorrections = (VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection)info.GetValue("_transactionJournalCorrections", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection));
			_alwaysFetchTransactionJournalCorrections = info.GetBoolean("_alwaysFetchTransactionJournalCorrections");
			_alreadyFetchedTransactionJournalCorrections = info.GetBoolean("_alreadyFetchedTransactionJournalCorrections");

			_transactionToInspections = (VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection)info.GetValue("_transactionToInspections", typeof(VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection));
			_alwaysFetchTransactionToInspections = info.GetBoolean("_alwaysFetchTransactionToInspections");
			_alreadyFetchedTransactionToInspections = info.GetBoolean("_alreadyFetchedTransactionToInspections");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_appliedPassTicket = (TicketEntity)info.GetValue("_appliedPassTicket", typeof(TicketEntity));
			if(_appliedPassTicket!=null)
			{
				_appliedPassTicket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_appliedPassTicketReturnsNewIfNotFound = info.GetBoolean("_appliedPassTicketReturnsNewIfNotFound");
			_alwaysFetchAppliedPassTicket = info.GetBoolean("_alwaysFetchAppliedPassTicket");
			_alreadyFetchedAppliedPassTicket = info.GetBoolean("_alreadyFetchedAppliedPassTicket");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_ticket_ = (TicketEntity)info.GetValue("_ticket_", typeof(TicketEntity));
			if(_ticket_!=null)
			{
				_ticket_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticket_ReturnsNewIfNotFound = info.GetBoolean("_ticket_ReturnsNewIfNotFound");
			_alwaysFetchTicket_ = info.GetBoolean("_alwaysFetchTicket_");
			_alreadyFetchedTicket_ = info.GetBoolean("_alreadyFetchedTicket_");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_card_ = (CardEntity)info.GetValue("_card_", typeof(CardEntity));
			if(_card_!=null)
			{
				_card_.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_card_ReturnsNewIfNotFound = info.GetBoolean("_card_ReturnsNewIfNotFound");
			_alwaysFetchCard_ = info.GetBoolean("_alwaysFetchCard_");
			_alreadyFetchedCard_ = info.GetBoolean("_alreadyFetchedCard_");

			_tokenCard = (CardEntity)info.GetValue("_tokenCard", typeof(CardEntity));
			if(_tokenCard!=null)
			{
				_tokenCard.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tokenCardReturnsNewIfNotFound = info.GetBoolean("_tokenCardReturnsNewIfNotFound");
			_alwaysFetchTokenCard = info.GetBoolean("_alwaysFetchTokenCard");
			_alreadyFetchedTokenCard = info.GetBoolean("_alreadyFetchedTokenCard");

			_cardHolder = (CardHolderEntity)info.GetValue("_cardHolder", typeof(CardHolderEntity));
			if(_cardHolder!=null)
			{
				_cardHolder.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderReturnsNewIfNotFound = info.GetBoolean("_cardHolderReturnsNewIfNotFound");
			_alwaysFetchCardHolder = info.GetBoolean("_alwaysFetchCardHolder");
			_alreadyFetchedCardHolder = info.GetBoolean("_alreadyFetchedCardHolder");

			_creditCardAuthorization = (CreditCardAuthorizationEntity)info.GetValue("_creditCardAuthorization", typeof(CreditCardAuthorizationEntity));
			if(_creditCardAuthorization!=null)
			{
				_creditCardAuthorization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_creditCardAuthorizationReturnsNewIfNotFound = info.GetBoolean("_creditCardAuthorizationReturnsNewIfNotFound");
			_alwaysFetchCreditCardAuthorization = info.GetBoolean("_alwaysFetchCreditCardAuthorization");
			_alreadyFetchedCreditCardAuthorization = info.GetBoolean("_alreadyFetchedCreditCardAuthorization");

			_appliedPassProduct = (ProductEntity)info.GetValue("_appliedPassProduct", typeof(ProductEntity));
			if(_appliedPassProduct!=null)
			{
				_appliedPassProduct.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_appliedPassProductReturnsNewIfNotFound = info.GetBoolean("_appliedPassProductReturnsNewIfNotFound");
			_alwaysFetchAppliedPassProduct = info.GetBoolean("_alwaysFetchAppliedPassProduct");
			_alreadyFetchedAppliedPassProduct = info.GetBoolean("_alreadyFetchedAppliedPassProduct");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");

			_sale = (SaleEntity)info.GetValue("_sale", typeof(SaleEntity));
			if(_sale!=null)
			{
				_sale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_saleReturnsNewIfNotFound = info.GetBoolean("_saleReturnsNewIfNotFound");
			_alwaysFetchSale = info.GetBoolean("_alwaysFetchSale");
			_alreadyFetchedSale = info.GetBoolean("_alreadyFetchedSale");

			_cancellationReferenceTransaction = (TransactionJournalEntity)info.GetValue("_cancellationReferenceTransaction", typeof(TransactionJournalEntity));
			if(_cancellationReferenceTransaction!=null)
			{
				_cancellationReferenceTransaction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cancellationReferenceTransactionReturnsNewIfNotFound = info.GetBoolean("_cancellationReferenceTransactionReturnsNewIfNotFound");
			_alwaysFetchCancellationReferenceTransaction = info.GetBoolean("_alwaysFetchCancellationReferenceTransaction");
			_alreadyFetchedCancellationReferenceTransaction = info.GetBoolean("_alreadyFetchedCancellationReferenceTransaction");
			_salesRevenue = (SalesRevenueEntity)info.GetValue("_salesRevenue", typeof(SalesRevenueEntity));
			if(_salesRevenue!=null)
			{
				_salesRevenue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_salesRevenueReturnsNewIfNotFound = info.GetBoolean("_salesRevenueReturnsNewIfNotFound");
			_alwaysFetchSalesRevenue = info.GetBoolean("_alwaysFetchSalesRevenue");
			_alreadyFetchedSalesRevenue = info.GetBoolean("_alreadyFetchedSalesRevenue");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TransactionJournalFieldIndex)fieldIndex)
			{
				case TransactionJournalFieldIndex.AppliedPassProductID:
					DesetupSyncAppliedPassProduct(true, false);
					_alreadyFetchedAppliedPassProduct = false;
					break;
				case TransactionJournalFieldIndex.AppliedPassTicketID:
					DesetupSyncAppliedPassTicket(true, false);
					_alreadyFetchedAppliedPassTicket = false;
					break;
				case TransactionJournalFieldIndex.CancellationReference:
					DesetupSyncCancellationReferenceTransaction(true, false);
					_alreadyFetchedCancellationReferenceTransaction = false;
					break;
				case TransactionJournalFieldIndex.CardHolderID:
					DesetupSyncCardHolder(true, false);
					_alreadyFetchedCardHolder = false;
					break;
				case TransactionJournalFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case TransactionJournalFieldIndex.CreditCardAuthorizationID:
					DesetupSyncCreditCardAuthorization(true, false);
					_alreadyFetchedCreditCardAuthorization = false;
					break;
				case TransactionJournalFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case TransactionJournalFieldIndex.SaleID:
					DesetupSyncSale(true, false);
					_alreadyFetchedSale = false;
					break;
				case TransactionJournalFieldIndex.SalesChannelID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case TransactionJournalFieldIndex.TaxTicketID:
					DesetupSyncTicket_(true, false);
					_alreadyFetchedTicket_ = false;
					break;
				case TransactionJournalFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				case TransactionJournalFieldIndex.TokenCardID:
					DesetupSyncTokenCard(true, false);
					_alreadyFetchedTokenCard = false;
					break;
				case TransactionJournalFieldIndex.Transferredfromcardid:
					DesetupSyncCard_(true, false);
					_alreadyFetchedCard_ = false;
					break;
				case TransactionJournalFieldIndex.TransitAccountID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportions = (_apportions.Count > 0);
			_alreadyFetchedCappingJournals = (_cappingJournals.Count > 0);
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedRevenueRecognitions = (_revenueRecognitions.Count > 0);
			_alreadyFetchedRevenueSettlements = (_revenueSettlements.Count > 0);
			_alreadyFetchedRuleViolationToTransactions = (_ruleViolationToTransactions.Count > 0);
			_alreadyFetchedTransactionJournalCorrections = (_transactionJournalCorrections.Count > 0);
			_alreadyFetchedTransactionToInspections = (_transactionToInspections.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedAppliedPassTicket = (_appliedPassTicket != null);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedTicket_ = (_ticket_ != null);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedCard_ = (_card_ != null);
			_alreadyFetchedTokenCard = (_tokenCard != null);
			_alreadyFetchedCardHolder = (_cardHolder != null);
			_alreadyFetchedCreditCardAuthorization = (_creditCardAuthorization != null);
			_alreadyFetchedAppliedPassProduct = (_appliedPassProduct != null);
			_alreadyFetchedProduct = (_product != null);
			_alreadyFetchedSale = (_sale != null);
			_alreadyFetchedCancellationReferenceTransaction = (_cancellationReferenceTransaction != null);
			_alreadyFetchedSalesRevenue = (_salesRevenue != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingSalesChannelID);
					break;
				case "AppliedPassTicket":
					toReturn.Add(Relations.TicketEntityUsingAppliedPassTicketID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "Ticket_":
					toReturn.Add(Relations.TicketEntityUsingTaxTicketID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingTransitAccountID);
					break;
				case "Card_":
					toReturn.Add(Relations.CardEntityUsingTransferredfromcardid);
					break;
				case "TokenCard":
					toReturn.Add(Relations.CardEntityUsingTokenCardID);
					break;
				case "CardHolder":
					toReturn.Add(Relations.CardHolderEntityUsingCardHolderID);
					break;
				case "CreditCardAuthorization":
					toReturn.Add(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
					break;
				case "AppliedPassProduct":
					toReturn.Add(Relations.ProductEntityUsingAppliedPassProductID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "Sale":
					toReturn.Add(Relations.SaleEntityUsingSaleID);
					break;
				case "CancellationReferenceTransaction":
					toReturn.Add(Relations.TransactionJournalEntityUsingTransactionJournalIDCancellationReference);
					break;
				case "Apportions":
					toReturn.Add(Relations.ApportionEntityUsingTransactionJournalID);
					break;
				case "CappingJournals":
					toReturn.Add(Relations.CappingJournalEntityUsingTransactionJournalID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingTransactionJournalID);
					break;
				case "RevenueRecognitions":
					toReturn.Add(Relations.RevenueRecognitionEntityUsingTransactionJournalID);
					break;
				case "RevenueSettlements":
					toReturn.Add(Relations.RevenueSettlementEntityUsingTransactionJournalID);
					break;
				case "RuleViolationToTransactions":
					toReturn.Add(Relations.RuleViolationToTransactionEntityUsingTransactionJournalID);
					break;
				case "TransactionJournalCorrections":
					toReturn.Add(Relations.TransactionJournalCorrectionEntityUsingTransactionJournalID);
					break;
				case "TransactionToInspections":
					toReturn.Add(Relations.TransactionToInspectionEntityUsingTransactionJournalID);
					break;
				case "SalesRevenue":
					toReturn.Add(Relations.SalesRevenueEntityUsingTransactionJournalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportions", (!this.MarkedForDeletion?_apportions:null));
			info.AddValue("_alwaysFetchApportions", _alwaysFetchApportions);
			info.AddValue("_alreadyFetchedApportions", _alreadyFetchedApportions);
			info.AddValue("_cappingJournals", (!this.MarkedForDeletion?_cappingJournals:null));
			info.AddValue("_alwaysFetchCappingJournals", _alwaysFetchCappingJournals);
			info.AddValue("_alreadyFetchedCappingJournals", _alreadyFetchedCappingJournals);
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_revenueRecognitions", (!this.MarkedForDeletion?_revenueRecognitions:null));
			info.AddValue("_alwaysFetchRevenueRecognitions", _alwaysFetchRevenueRecognitions);
			info.AddValue("_alreadyFetchedRevenueRecognitions", _alreadyFetchedRevenueRecognitions);
			info.AddValue("_revenueSettlements", (!this.MarkedForDeletion?_revenueSettlements:null));
			info.AddValue("_alwaysFetchRevenueSettlements", _alwaysFetchRevenueSettlements);
			info.AddValue("_alreadyFetchedRevenueSettlements", _alreadyFetchedRevenueSettlements);
			info.AddValue("_ruleViolationToTransactions", (!this.MarkedForDeletion?_ruleViolationToTransactions:null));
			info.AddValue("_alwaysFetchRuleViolationToTransactions", _alwaysFetchRuleViolationToTransactions);
			info.AddValue("_alreadyFetchedRuleViolationToTransactions", _alreadyFetchedRuleViolationToTransactions);
			info.AddValue("_transactionJournalCorrections", (!this.MarkedForDeletion?_transactionJournalCorrections:null));
			info.AddValue("_alwaysFetchTransactionJournalCorrections", _alwaysFetchTransactionJournalCorrections);
			info.AddValue("_alreadyFetchedTransactionJournalCorrections", _alreadyFetchedTransactionJournalCorrections);
			info.AddValue("_transactionToInspections", (!this.MarkedForDeletion?_transactionToInspections:null));
			info.AddValue("_alwaysFetchTransactionToInspections", _alwaysFetchTransactionToInspections);
			info.AddValue("_alreadyFetchedTransactionToInspections", _alreadyFetchedTransactionToInspections);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_appliedPassTicket", (!this.MarkedForDeletion?_appliedPassTicket:null));
			info.AddValue("_appliedPassTicketReturnsNewIfNotFound", _appliedPassTicketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAppliedPassTicket", _alwaysFetchAppliedPassTicket);
			info.AddValue("_alreadyFetchedAppliedPassTicket", _alreadyFetchedAppliedPassTicket);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_ticket_", (!this.MarkedForDeletion?_ticket_:null));
			info.AddValue("_ticket_ReturnsNewIfNotFound", _ticket_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket_", _alwaysFetchTicket_);
			info.AddValue("_alreadyFetchedTicket_", _alreadyFetchedTicket_);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_card_", (!this.MarkedForDeletion?_card_:null));
			info.AddValue("_card_ReturnsNewIfNotFound", _card_ReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard_", _alwaysFetchCard_);
			info.AddValue("_alreadyFetchedCard_", _alreadyFetchedCard_);
			info.AddValue("_tokenCard", (!this.MarkedForDeletion?_tokenCard:null));
			info.AddValue("_tokenCardReturnsNewIfNotFound", _tokenCardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTokenCard", _alwaysFetchTokenCard);
			info.AddValue("_alreadyFetchedTokenCard", _alreadyFetchedTokenCard);
			info.AddValue("_cardHolder", (!this.MarkedForDeletion?_cardHolder:null));
			info.AddValue("_cardHolderReturnsNewIfNotFound", _cardHolderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolder", _alwaysFetchCardHolder);
			info.AddValue("_alreadyFetchedCardHolder", _alreadyFetchedCardHolder);
			info.AddValue("_creditCardAuthorization", (!this.MarkedForDeletion?_creditCardAuthorization:null));
			info.AddValue("_creditCardAuthorizationReturnsNewIfNotFound", _creditCardAuthorizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCreditCardAuthorization", _alwaysFetchCreditCardAuthorization);
			info.AddValue("_alreadyFetchedCreditCardAuthorization", _alreadyFetchedCreditCardAuthorization);
			info.AddValue("_appliedPassProduct", (!this.MarkedForDeletion?_appliedPassProduct:null));
			info.AddValue("_appliedPassProductReturnsNewIfNotFound", _appliedPassProductReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAppliedPassProduct", _alwaysFetchAppliedPassProduct);
			info.AddValue("_alreadyFetchedAppliedPassProduct", _alreadyFetchedAppliedPassProduct);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);
			info.AddValue("_sale", (!this.MarkedForDeletion?_sale:null));
			info.AddValue("_saleReturnsNewIfNotFound", _saleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSale", _alwaysFetchSale);
			info.AddValue("_alreadyFetchedSale", _alreadyFetchedSale);
			info.AddValue("_cancellationReferenceTransaction", (!this.MarkedForDeletion?_cancellationReferenceTransaction:null));
			info.AddValue("_cancellationReferenceTransactionReturnsNewIfNotFound", _cancellationReferenceTransactionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCancellationReferenceTransaction", _alwaysFetchCancellationReferenceTransaction);
			info.AddValue("_alreadyFetchedCancellationReferenceTransaction", _alreadyFetchedCancellationReferenceTransaction);

			info.AddValue("_salesRevenue", (!this.MarkedForDeletion?_salesRevenue:null));
			info.AddValue("_salesRevenueReturnsNewIfNotFound", _salesRevenueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSalesRevenue", _alwaysFetchSalesRevenue);
			info.AddValue("_alreadyFetchedSalesRevenue", _alreadyFetchedSalesRevenue);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "AppliedPassTicket":
					_alreadyFetchedAppliedPassTicket = true;
					this.AppliedPassTicket = (TicketEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "Ticket_":
					_alreadyFetchedTicket_ = true;
					this.Ticket_ = (TicketEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Card_":
					_alreadyFetchedCard_ = true;
					this.Card_ = (CardEntity)entity;
					break;
				case "TokenCard":
					_alreadyFetchedTokenCard = true;
					this.TokenCard = (CardEntity)entity;
					break;
				case "CardHolder":
					_alreadyFetchedCardHolder = true;
					this.CardHolder = (CardHolderEntity)entity;
					break;
				case "CreditCardAuthorization":
					_alreadyFetchedCreditCardAuthorization = true;
					this.CreditCardAuthorization = (CreditCardAuthorizationEntity)entity;
					break;
				case "AppliedPassProduct":
					_alreadyFetchedAppliedPassProduct = true;
					this.AppliedPassProduct = (ProductEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "Sale":
					_alreadyFetchedSale = true;
					this.Sale = (SaleEntity)entity;
					break;
				case "CancellationReferenceTransaction":
					_alreadyFetchedCancellationReferenceTransaction = true;
					this.CancellationReferenceTransaction = (TransactionJournalEntity)entity;
					break;
				case "Apportions":
					_alreadyFetchedApportions = true;
					if(entity!=null)
					{
						this.Apportions.Add((ApportionEntity)entity);
					}
					break;
				case "CappingJournals":
					_alreadyFetchedCappingJournals = true;
					if(entity!=null)
					{
						this.CappingJournals.Add((CappingJournalEntity)entity);
					}
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "RevenueRecognitions":
					_alreadyFetchedRevenueRecognitions = true;
					if(entity!=null)
					{
						this.RevenueRecognitions.Add((RevenueRecognitionEntity)entity);
					}
					break;
				case "RevenueSettlements":
					_alreadyFetchedRevenueSettlements = true;
					if(entity!=null)
					{
						this.RevenueSettlements.Add((RevenueSettlementEntity)entity);
					}
					break;
				case "RuleViolationToTransactions":
					_alreadyFetchedRuleViolationToTransactions = true;
					if(entity!=null)
					{
						this.RuleViolationToTransactions.Add((RuleViolationToTransactionEntity)entity);
					}
					break;
				case "TransactionJournalCorrections":
					_alreadyFetchedTransactionJournalCorrections = true;
					if(entity!=null)
					{
						this.TransactionJournalCorrections.Add((TransactionJournalCorrectionEntity)entity);
					}
					break;
				case "TransactionToInspections":
					_alreadyFetchedTransactionToInspections = true;
					if(entity!=null)
					{
						this.TransactionToInspections.Add((TransactionToInspectionEntity)entity);
					}
					break;
				case "SalesRevenue":
					_alreadyFetchedSalesRevenue = true;
					this.SalesRevenue = (SalesRevenueEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "AppliedPassTicket":
					SetupSyncAppliedPassTicket(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "Ticket_":
					SetupSyncTicket_(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Card_":
					SetupSyncCard_(relatedEntity);
					break;
				case "TokenCard":
					SetupSyncTokenCard(relatedEntity);
					break;
				case "CardHolder":
					SetupSyncCardHolder(relatedEntity);
					break;
				case "CreditCardAuthorization":
					SetupSyncCreditCardAuthorization(relatedEntity);
					break;
				case "AppliedPassProduct":
					SetupSyncAppliedPassProduct(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "Sale":
					SetupSyncSale(relatedEntity);
					break;
				case "CancellationReferenceTransaction":
					SetupSyncCancellationReferenceTransaction(relatedEntity);
					break;
				case "Apportions":
					_apportions.Add((ApportionEntity)relatedEntity);
					break;
				case "CappingJournals":
					_cappingJournals.Add((CappingJournalEntity)relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "RevenueRecognitions":
					_revenueRecognitions.Add((RevenueRecognitionEntity)relatedEntity);
					break;
				case "RevenueSettlements":
					_revenueSettlements.Add((RevenueSettlementEntity)relatedEntity);
					break;
				case "RuleViolationToTransactions":
					_ruleViolationToTransactions.Add((RuleViolationToTransactionEntity)relatedEntity);
					break;
				case "TransactionJournalCorrections":
					_transactionJournalCorrections.Add((TransactionJournalCorrectionEntity)relatedEntity);
					break;
				case "TransactionToInspections":
					_transactionToInspections.Add((TransactionToInspectionEntity)relatedEntity);
					break;
				case "SalesRevenue":
					SetupSyncSalesRevenue(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "AppliedPassTicket":
					DesetupSyncAppliedPassTicket(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "Ticket_":
					DesetupSyncTicket_(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Card_":
					DesetupSyncCard_(false, true);
					break;
				case "TokenCard":
					DesetupSyncTokenCard(false, true);
					break;
				case "CardHolder":
					DesetupSyncCardHolder(false, true);
					break;
				case "CreditCardAuthorization":
					DesetupSyncCreditCardAuthorization(false, true);
					break;
				case "AppliedPassProduct":
					DesetupSyncAppliedPassProduct(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "Sale":
					DesetupSyncSale(false, true);
					break;
				case "CancellationReferenceTransaction":
					DesetupSyncCancellationReferenceTransaction(false, true);
					break;
				case "Apportions":
					this.PerformRelatedEntityRemoval(_apportions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CappingJournals":
					this.PerformRelatedEntityRemoval(_cappingJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevenueRecognitions":
					this.PerformRelatedEntityRemoval(_revenueRecognitions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevenueSettlements":
					this.PerformRelatedEntityRemoval(_revenueSettlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolationToTransactions":
					this.PerformRelatedEntityRemoval(_ruleViolationToTransactions, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournalCorrections":
					this.PerformRelatedEntityRemoval(_transactionJournalCorrections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionToInspections":
					this.PerformRelatedEntityRemoval(_transactionToInspections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SalesRevenue":
					DesetupSyncSalesRevenue(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_salesRevenue!=null)
			{
				toReturn.Add(_salesRevenue);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_appliedPassTicket!=null)
			{
				toReturn.Add(_appliedPassTicket);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_ticket_!=null)
			{
				toReturn.Add(_ticket_);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_card_!=null)
			{
				toReturn.Add(_card_);
			}
			if(_tokenCard!=null)
			{
				toReturn.Add(_tokenCard);
			}
			if(_cardHolder!=null)
			{
				toReturn.Add(_cardHolder);
			}
			if(_creditCardAuthorization!=null)
			{
				toReturn.Add(_creditCardAuthorization);
			}
			if(_appliedPassProduct!=null)
			{
				toReturn.Add(_appliedPassProduct);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			if(_sale!=null)
			{
				toReturn.Add(_sale);
			}
			if(_cancellationReferenceTransaction!=null)
			{
				toReturn.Add(_cancellationReferenceTransaction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_apportions);
			toReturn.Add(_cappingJournals);
			toReturn.Add(_paymentJournals);
			toReturn.Add(_revenueRecognitions);
			toReturn.Add(_revenueSettlements);
			toReturn.Add(_ruleViolationToTransactions);

			toReturn.Add(_transactionJournalCorrections);
			toReturn.Add(_transactionToInspections);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionJournalID)
		{
			return FetchUsingPK(transactionJournalID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(transactionJournalID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(transactionJournalID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(transactionJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TransactionJournalID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TransactionJournalRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ApportionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiApportions(forceFetch, _apportions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiApportions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection GetMultiApportions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedApportions || forceFetch || _alwaysFetchApportions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_apportions);
				_apportions.SuppressClearInGetMulti=!forceFetch;
				_apportions.EntityFactoryToUse = entityFactoryToUse;
				_apportions.GetMultiManyToOne(null, null, this, filter);
				_apportions.SuppressClearInGetMulti=false;
				_alreadyFetchedApportions = true;
			}
			return _apportions;
		}

		/// <summary> Sets the collection parameters for the collection for 'Apportions'. These settings will be taken into account
		/// when the property Apportions is requested or GetMultiApportions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersApportions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_apportions.SortClauses=sortClauses;
			_apportions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CappingJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CappingJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CappingJournalCollection GetMultiCappingJournals(bool forceFetch)
		{
			return GetMultiCappingJournals(forceFetch, _cappingJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CappingJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CappingJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CappingJournalCollection GetMultiCappingJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCappingJournals(forceFetch, _cappingJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CappingJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CappingJournalCollection GetMultiCappingJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCappingJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CappingJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CappingJournalCollection GetMultiCappingJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCappingJournals || forceFetch || _alwaysFetchCappingJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cappingJournals);
				_cappingJournals.SuppressClearInGetMulti=!forceFetch;
				_cappingJournals.EntityFactoryToUse = entityFactoryToUse;
				_cappingJournals.GetMultiManyToOne(this, filter);
				_cappingJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedCappingJournals = true;
			}
			return _cappingJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'CappingJournals'. These settings will be taken into account
		/// when the property CappingJournals is requested or GetMultiCappingJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCappingJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cappingJournals.SortClauses=sortClauses;
			_cappingJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch)
		{
			return GetMultiRevenueRecognitions(forceFetch, _revenueRecognitions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueRecognitionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueRecognitions(forceFetch, _revenueRecognitions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueRecognitions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection GetMultiRevenueRecognitions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueRecognitions || forceFetch || _alwaysFetchRevenueRecognitions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueRecognitions);
				_revenueRecognitions.SuppressClearInGetMulti=!forceFetch;
				_revenueRecognitions.EntityFactoryToUse = entityFactoryToUse;
				_revenueRecognitions.GetMultiManyToOne(null, null, this, filter);
				_revenueRecognitions.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueRecognitions = true;
			}
			return _revenueRecognitions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueRecognitions'. These settings will be taken into account
		/// when the property RevenueRecognitions is requested or GetMultiRevenueRecognitions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueRecognitions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueRecognitions.SortClauses=sortClauses;
			_revenueRecognitions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RevenueSettlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRevenueSettlements(forceFetch, _revenueSettlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRevenueSettlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection GetMultiRevenueSettlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRevenueSettlements || forceFetch || _alwaysFetchRevenueSettlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_revenueSettlements);
				_revenueSettlements.SuppressClearInGetMulti=!forceFetch;
				_revenueSettlements.EntityFactoryToUse = entityFactoryToUse;
				_revenueSettlements.GetMultiManyToOne(null, null, this, filter);
				_revenueSettlements.SuppressClearInGetMulti=false;
				_alreadyFetchedRevenueSettlements = true;
			}
			return _revenueSettlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'RevenueSettlements'. These settings will be taken into account
		/// when the property RevenueSettlements is requested or GetMultiRevenueSettlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRevenueSettlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_revenueSettlements.SortClauses=sortClauses;
			_revenueSettlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationToTransactionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, _ruleViolationToTransactions.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolationToTransactions(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection GetMultiRuleViolationToTransactions(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolationToTransactions || forceFetch || _alwaysFetchRuleViolationToTransactions) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolationToTransactions);
				_ruleViolationToTransactions.SuppressClearInGetMulti=!forceFetch;
				_ruleViolationToTransactions.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolationToTransactions.GetMultiManyToOne(null, null, null, this, filter);
				_ruleViolationToTransactions.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolationToTransactions = true;
			}
			return _ruleViolationToTransactions;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolationToTransactions'. These settings will be taken into account
		/// when the property RuleViolationToTransactions is requested or GetMultiRuleViolationToTransactions is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolationToTransactions(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolationToTransactions.SortClauses=sortClauses;
			_ruleViolationToTransactions.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalCorrectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalCorrectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection GetMultiTransactionJournalCorrections(bool forceFetch)
		{
			return GetMultiTransactionJournalCorrections(forceFetch, _transactionJournalCorrections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalCorrectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalCorrectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection GetMultiTransactionJournalCorrections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournalCorrections(forceFetch, _transactionJournalCorrections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalCorrectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection GetMultiTransactionJournalCorrections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournalCorrections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalCorrectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection GetMultiTransactionJournalCorrections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournalCorrections || forceFetch || _alwaysFetchTransactionJournalCorrections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournalCorrections);
				_transactionJournalCorrections.SuppressClearInGetMulti=!forceFetch;
				_transactionJournalCorrections.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournalCorrections.GetMultiManyToOne(this, filter);
				_transactionJournalCorrections.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournalCorrections = true;
			}
			return _transactionJournalCorrections;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournalCorrections'. These settings will be taken into account
		/// when the property TransactionJournalCorrections is requested or GetMultiTransactionJournalCorrections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournalCorrections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournalCorrections.SortClauses=sortClauses;
			_transactionJournalCorrections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionToInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch)
		{
			return GetMultiTransactionToInspections(forceFetch, _transactionToInspections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionToInspectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionToInspections(forceFetch, _transactionToInspections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionToInspections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection GetMultiTransactionToInspections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionToInspections || forceFetch || _alwaysFetchTransactionToInspections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionToInspections);
				_transactionToInspections.SuppressClearInGetMulti=!forceFetch;
				_transactionToInspections.EntityFactoryToUse = entityFactoryToUse;
				_transactionToInspections.GetMultiManyToOne(null, this, filter);
				_transactionToInspections.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionToInspections = true;
			}
			return _transactionToInspections;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionToInspections'. These settings will be taken into account
		/// when the property TransactionToInspections is requested or GetMultiTransactionToInspections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionToInspections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionToInspections.SortClauses=sortClauses;
			_transactionToInspections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingSalesChannelID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SalesChannelID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleAppliedPassTicket()
		{
			return GetSingleAppliedPassTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleAppliedPassTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedAppliedPassTicket || forceFetch || _alwaysFetchAppliedPassTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingAppliedPassTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AppliedPassTicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_appliedPassTicketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AppliedPassTicket = newEntity;
				_alreadyFetchedAppliedPassTicket = fetchResult;
			}
			return _appliedPassTicket;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket_()
		{
			return GetSingleTicket_(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket_(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket_ || forceFetch || _alwaysFetchTicket_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTaxTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TaxTicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticket_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket_ = newEntity;
				_alreadyFetchedTicket_ = fetchResult;
			}
			return _ticket_;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingTransitAccountID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransitAccountID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard_()
		{
			return GetSingleCard_(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard_(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard_ || forceFetch || _alwaysFetchCard_) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingTransferredfromcardid);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Transferredfromcardid.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_card_ReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card_ = newEntity;
				_alreadyFetchedCard_ = fetchResult;
			}
			return _card_;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleTokenCard()
		{
			return GetSingleTokenCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleTokenCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedTokenCard || forceFetch || _alwaysFetchTokenCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingTokenCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TokenCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tokenCardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TokenCard = newEntity;
				_alreadyFetchedTokenCard = fetchResult;
			}
			return _tokenCard;
		}


		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public CardHolderEntity GetSingleCardHolder()
		{
			return GetSingleCardHolder(false);
		}

		/// <summary> Retrieves the related entity of type 'CardHolderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderEntity' which is related to this entity.</returns>
		public virtual CardHolderEntity GetSingleCardHolder(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolder || forceFetch || _alwaysFetchCardHolder) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderEntityUsingCardHolderID);
				CardHolderEntity newEntity = (CardHolderEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.CardHolderEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = CardHolderEntity.FetchPolymorphic(this.Transaction, this.CardHolderID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolder = newEntity;
				_alreadyFetchedCardHolder = fetchResult;
			}
			return _cardHolder;
		}


		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public CreditCardAuthorizationEntity GetSingleCreditCardAuthorization()
		{
			return GetSingleCreditCardAuthorization(false);
		}

		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public virtual CreditCardAuthorizationEntity GetSingleCreditCardAuthorization(bool forceFetch)
		{
			if( ( !_alreadyFetchedCreditCardAuthorization || forceFetch || _alwaysFetchCreditCardAuthorization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
				CreditCardAuthorizationEntity newEntity = new CreditCardAuthorizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CreditCardAuthorizationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CreditCardAuthorizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_creditCardAuthorizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CreditCardAuthorization = newEntity;
				_alreadyFetchedCreditCardAuthorization = fetchResult;
			}
			return _creditCardAuthorization;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleAppliedPassProduct()
		{
			return GetSingleAppliedPassProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleAppliedPassProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedAppliedPassProduct || forceFetch || _alwaysFetchAppliedPassProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingAppliedPassProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AppliedPassProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_appliedPassProductReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AppliedPassProduct = newEntity;
				_alreadyFetchedAppliedPassProduct = fetchResult;
			}
			return _appliedPassProduct;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleSale()
		{
			return GetSingleSale(false);
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedSale || forceFetch || _alwaysFetchSale) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingSaleID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SaleID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_saleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Sale = newEntity;
				_alreadyFetchedSale = fetchResult;
			}
			return _sale;
		}


		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public TransactionJournalEntity GetSingleCancellationReferenceTransaction()
		{
			return GetSingleCancellationReferenceTransaction(false);
		}

		/// <summary> Retrieves the related entity of type 'TransactionJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransactionJournalEntity' which is related to this entity.</returns>
		public virtual TransactionJournalEntity GetSingleCancellationReferenceTransaction(bool forceFetch)
		{
			if( ( !_alreadyFetchedCancellationReferenceTransaction || forceFetch || _alwaysFetchCancellationReferenceTransaction) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransactionJournalEntityUsingTransactionJournalIDCancellationReference);
				TransactionJournalEntity newEntity = new TransactionJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CancellationReference.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransactionJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cancellationReferenceTransactionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CancellationReferenceTransaction = newEntity;
				_alreadyFetchedCancellationReferenceTransaction = fetchResult;
			}
			return _cancellationReferenceTransaction;
		}

		/// <summary> Retrieves the related entity of type 'SalesRevenueEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SalesRevenueEntity' which is related to this entity.</returns>
		public SalesRevenueEntity GetSingleSalesRevenue()
		{
			return GetSingleSalesRevenue(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SalesRevenueEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SalesRevenueEntity' which is related to this entity.</returns>
		public virtual SalesRevenueEntity GetSingleSalesRevenue(bool forceFetch)
		{
			if( ( !_alreadyFetchedSalesRevenue || forceFetch || _alwaysFetchSalesRevenue) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SalesRevenueEntityUsingTransactionJournalID);
				SalesRevenueEntity newEntity = new SalesRevenueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCTransactionJournalID(this.TransactionJournalID);
				}
				if(fetchResult)
				{
					newEntity = (SalesRevenueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_salesRevenueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SalesRevenue = newEntity;
				_alreadyFetchedSalesRevenue = fetchResult;
			}
			return _salesRevenue;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("AppliedPassTicket", _appliedPassTicket);
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("Ticket_", _ticket_);
			toReturn.Add("Card", _card);
			toReturn.Add("Card_", _card_);
			toReturn.Add("TokenCard", _tokenCard);
			toReturn.Add("CardHolder", _cardHolder);
			toReturn.Add("CreditCardAuthorization", _creditCardAuthorization);
			toReturn.Add("AppliedPassProduct", _appliedPassProduct);
			toReturn.Add("Product", _product);
			toReturn.Add("Sale", _sale);
			toReturn.Add("CancellationReferenceTransaction", _cancellationReferenceTransaction);
			toReturn.Add("Apportions", _apportions);
			toReturn.Add("CappingJournals", _cappingJournals);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("RevenueRecognitions", _revenueRecognitions);
			toReturn.Add("RevenueSettlements", _revenueSettlements);
			toReturn.Add("RuleViolationToTransactions", _ruleViolationToTransactions);
			toReturn.Add("TransactionJournalCorrections", _transactionJournalCorrections);
			toReturn.Add("TransactionToInspections", _transactionToInspections);
			toReturn.Add("SalesRevenue", _salesRevenue);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="validator">The validator object for this TransactionJournalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 transactionJournalID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(transactionJournalID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_apportions = new VarioSL.Entities.CollectionClasses.ApportionCollection();
			_apportions.SetContainingEntityInfo(this, "TransactionJournal");

			_cappingJournals = new VarioSL.Entities.CollectionClasses.CappingJournalCollection();
			_cappingJournals.SetContainingEntityInfo(this, "TransactionJournal");

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "TransactionJournal");

			_revenueRecognitions = new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection();
			_revenueRecognitions.SetContainingEntityInfo(this, "TransactionJournal");

			_revenueSettlements = new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection();
			_revenueSettlements.SetContainingEntityInfo(this, "TransactionJournal");

			_ruleViolationToTransactions = new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection();
			_ruleViolationToTransactions.SetContainingEntityInfo(this, "TransactionJournal");

			_transactionJournalCorrections = new VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection();
			_transactionJournalCorrections.SetContainingEntityInfo(this, "TransactionJournal");

			_transactionToInspections = new VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection();
			_transactionToInspections.SetContainingEntityInfo(this, "TransactionJournal");
			_clientReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_appliedPassTicketReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			_ticket_ReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			_card_ReturnsNewIfNotFound = false;
			_tokenCardReturnsNewIfNotFound = false;
			_cardHolderReturnsNewIfNotFound = false;
			_creditCardAuthorizationReturnsNewIfNotFound = false;
			_appliedPassProductReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			_saleReturnsNewIfNotFound = false;
			_cancellationReferenceTransactionReturnsNewIfNotFound = false;
			_salesRevenueReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlightDuration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AppliedPassProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AppliedPassTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardHolderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CheckOutTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletionState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CorrectedLine", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostCenterKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CourseNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DestinationZone", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceBookingNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTicketName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTransactionIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareMediaType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareScaleName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FillLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GeolocationLatitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GeolocationLongitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InsertTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JourneyStartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NoTrip", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalDeviceResultType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalOnlineResultType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OriginalSingleFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Properties", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseBalance2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseCredit2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoundTripTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RouteNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmartTapCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SmartTapID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxLocationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxRate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TokenCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Transferredfromcardid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripSerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripValueFloor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripValueFloor2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VanpoolGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhitelistVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WhitelistVersionCreated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Zone", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.DeviceClassEntityUsingSalesChannelIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.SalesChannelID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.DeviceClassEntityUsingSalesChannelIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _appliedPassTicket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAppliedPassTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _appliedPassTicket, new PropertyChangedEventHandler( OnAppliedPassTicketPropertyChanged ), "AppliedPassTicket", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingAppliedPassTicketIDStatic, true, signalRelatedEntity, "TransactionJournalsForAppliedPassTicket", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.AppliedPassTicketID } );		
			_appliedPassTicket = null;
		}
		
		/// <summary> setups the sync logic for member _appliedPassTicket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAppliedPassTicket(IEntityCore relatedEntity)
		{
			if(_appliedPassTicket!=relatedEntity)
			{		
				DesetupSyncAppliedPassTicket(true, true);
				_appliedPassTicket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _appliedPassTicket, new PropertyChangedEventHandler( OnAppliedPassTicketPropertyChanged ), "AppliedPassTicket", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingAppliedPassTicketIDStatic, true, ref _alreadyFetchedAppliedPassTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAppliedPassTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket_, new PropertyChangedEventHandler( OnTicket_PropertyChanged ), "Ticket_", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingTaxTicketIDStatic, true, signalRelatedEntity, "TransactionJournals_", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.TaxTicketID } );		
			_ticket_ = null;
		}
		
		/// <summary> setups the sync logic for member _ticket_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket_(IEntityCore relatedEntity)
		{
			if(_ticket_!=relatedEntity)
			{		
				DesetupSyncTicket_(true, true);
				_ticket_ = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket_, new PropertyChangedEventHandler( OnTicket_PropertyChanged ), "Ticket_", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TicketEntityUsingTaxTicketIDStatic, true, ref _alreadyFetchedTicket_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicket_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTransitAccountIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.TransitAccountID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTransitAccountIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card_</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard_(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card_, new PropertyChangedEventHandler( OnCard_PropertyChanged ), "Card_", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTransferredfromcardidStatic, true, signalRelatedEntity, "TransactionJournals_", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.Transferredfromcardid } );		
			_card_ = null;
		}
		
		/// <summary> setups the sync logic for member _card_</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard_(IEntityCore relatedEntity)
		{
			if(_card_!=relatedEntity)
			{		
				DesetupSyncCard_(true, true);
				_card_ = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card_, new PropertyChangedEventHandler( OnCard_PropertyChanged ), "Card_", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTransferredfromcardidStatic, true, ref _alreadyFetchedCard_, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCard_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tokenCard</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTokenCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tokenCard, new PropertyChangedEventHandler( OnTokenCardPropertyChanged ), "TokenCard", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTokenCardIDStatic, true, signalRelatedEntity, "TokenTransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.TokenCardID } );		
			_tokenCard = null;
		}
		
		/// <summary> setups the sync logic for member _tokenCard</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTokenCard(IEntityCore relatedEntity)
		{
			if(_tokenCard!=relatedEntity)
			{		
				DesetupSyncTokenCard(true, true);
				_tokenCard = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tokenCard, new PropertyChangedEventHandler( OnTokenCardPropertyChanged ), "TokenCard", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardEntityUsingTokenCardIDStatic, true, ref _alreadyFetchedTokenCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTokenCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardHolder</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardHolderEntityUsingCardHolderIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.CardHolderID } );		
			_cardHolder = null;
		}
		
		/// <summary> setups the sync logic for member _cardHolder</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolder(IEntityCore relatedEntity)
		{
			if(_cardHolder!=relatedEntity)
			{		
				DesetupSyncCardHolder(true, true);
				_cardHolder = (CardHolderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolder, new PropertyChangedEventHandler( OnCardHolderPropertyChanged ), "CardHolder", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CardHolderEntityUsingCardHolderIDStatic, true, ref _alreadyFetchedCardHolder, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _creditCardAuthorization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCreditCardAuthorization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.CreditCardAuthorizationID } );		
			_creditCardAuthorization = null;
		}
		
		/// <summary> setups the sync logic for member _creditCardAuthorization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCreditCardAuthorization(IEntityCore relatedEntity)
		{
			if(_creditCardAuthorization!=relatedEntity)
			{		
				DesetupSyncCreditCardAuthorization(true, true);
				_creditCardAuthorization = (CreditCardAuthorizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, ref _alreadyFetchedCreditCardAuthorization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCreditCardAuthorizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _appliedPassProduct</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAppliedPassProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _appliedPassProduct, new PropertyChangedEventHandler( OnAppliedPassProductPropertyChanged ), "AppliedPassProduct", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ProductEntityUsingAppliedPassProductIDStatic, true, signalRelatedEntity, "TransactionJournalsForAppledPass", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.AppliedPassProductID } );		
			_appliedPassProduct = null;
		}
		
		/// <summary> setups the sync logic for member _appliedPassProduct</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAppliedPassProduct(IEntityCore relatedEntity)
		{
			if(_appliedPassProduct!=relatedEntity)
			{		
				DesetupSyncAppliedPassProduct(true, true);
				_appliedPassProduct = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _appliedPassProduct, new PropertyChangedEventHandler( OnAppliedPassProductPropertyChanged ), "AppliedPassProduct", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ProductEntityUsingAppliedPassProductIDStatic, true, ref _alreadyFetchedAppliedPassProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAppliedPassProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _sale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.SaleEntityUsingSaleIDStatic, true, signalRelatedEntity, "TransactionJournals", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.SaleID } );		
			_sale = null;
		}
		
		/// <summary> setups the sync logic for member _sale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSale(IEntityCore relatedEntity)
		{
			if(_sale!=relatedEntity)
			{		
				DesetupSyncSale(true, true);
				_sale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.SaleEntityUsingSaleIDStatic, true, ref _alreadyFetchedSale, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cancellationReferenceTransaction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCancellationReferenceTransaction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cancellationReferenceTransaction, new PropertyChangedEventHandler( OnCancellationReferenceTransactionPropertyChanged ), "CancellationReferenceTransaction", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TransactionJournalEntityUsingTransactionJournalIDCancellationReferenceStatic, true, signalRelatedEntity, "", resetFKFields, new int[] { (int)TransactionJournalFieldIndex.CancellationReference } );		
			_cancellationReferenceTransaction = null;
		}
		
		/// <summary> setups the sync logic for member _cancellationReferenceTransaction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCancellationReferenceTransaction(IEntityCore relatedEntity)
		{
			if(_cancellationReferenceTransaction!=relatedEntity)
			{		
				DesetupSyncCancellationReferenceTransaction(true, true);
				_cancellationReferenceTransaction = (TransactionJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cancellationReferenceTransaction, new PropertyChangedEventHandler( OnCancellationReferenceTransactionPropertyChanged ), "CancellationReferenceTransaction", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.TransactionJournalEntityUsingTransactionJournalIDCancellationReferenceStatic, true, ref _alreadyFetchedCancellationReferenceTransaction, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCancellationReferenceTransactionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _salesRevenue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSalesRevenue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _salesRevenue, new PropertyChangedEventHandler( OnSalesRevenuePropertyChanged ), "SalesRevenue", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.SalesRevenueEntityUsingTransactionJournalIDStatic, false, signalRelatedEntity, "TransactionJournal", false, new int[] { (int)TransactionJournalFieldIndex.TransactionJournalID } );
			_salesRevenue = null;
		}
	
		/// <summary> setups the sync logic for member _salesRevenue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSalesRevenue(IEntityCore relatedEntity)
		{
			if(_salesRevenue!=relatedEntity)
			{
				DesetupSyncSalesRevenue(true, true);
				_salesRevenue = (SalesRevenueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _salesRevenue, new PropertyChangedEventHandler( OnSalesRevenuePropertyChanged ), "SalesRevenue", VarioSL.Entities.RelationClasses.StaticTransactionJournalRelations.SalesRevenueEntityUsingTransactionJournalIDStatic, false, ref _alreadyFetchedSalesRevenue, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalesRevenuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="transactionJournalID">PK value for TransactionJournal which data should be fetched into this TransactionJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 transactionJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TransactionJournalFieldIndex.TransactionJournalID].ForcedCurrentValueWrite(transactionJournalID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTransactionJournalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TransactionJournalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TransactionJournalRelations Relations
		{
			get	{ return new TransactionJournalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Apportion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionCollection(), (IEntityRelation)GetRelationsForField("Apportions")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.ApportionEntity, 0, null, null, null, "Apportions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CappingJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCappingJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CappingJournalCollection(), (IEntityRelation)GetRelationsForField("CappingJournals")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CappingJournalEntity, 0, null, null, null, "CappingJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueRecognition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueRecognitions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection(), (IEntityRelation)GetRelationsForField("RevenueRecognitions")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, 0, null, null, null, "RevenueRecognitions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueSettlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueSettlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection(), (IEntityRelation)GetRelationsForField("RevenueSettlements")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, 0, null, null, null, "RevenueSettlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolationToTransaction' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolationToTransactions
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection(), (IEntityRelation)GetRelationsForField("RuleViolationToTransactions")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.RuleViolationToTransactionEntity, 0, null, null, null, "RuleViolationToTransactions", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournalCorrection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournalCorrections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection(), (IEntityRelation)GetRelationsForField("TransactionJournalCorrections")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TransactionJournalCorrectionEntity, 0, null, null, null, "TransactionJournalCorrections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionToInspection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionToInspections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection(), (IEntityRelation)GetRelationsForField("TransactionToInspections")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TransactionToInspectionEntity, 0, null, null, null, "TransactionToInspections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAppliedPassTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("AppliedPassTicket")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "AppliedPassTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket_
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket_")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard_
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card_")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTokenCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("TokenCard")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "TokenCard", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), (IEntityRelation)GetRelationsForField("CardHolder")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, null, "CardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorization")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, 0, null, null, null, "CreditCardAuthorization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAppliedPassProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("AppliedPassProduct")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "AppliedPassProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sale")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCancellationReferenceTransaction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("CancellationReferenceTransaction")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "CancellationReferenceTransaction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SalesRevenue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSalesRevenue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SalesRevenueCollection(), (IEntityRelation)GetRelationsForField("SalesRevenue")[0], (int)VarioSL.Entities.EntityType.TransactionJournalEntity, (int)VarioSL.Entities.EntityType.SalesRevenueEntity, 0, null, null, null, "SalesRevenue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlightDuration property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ALIGHTDURATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AlightDuration
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.AlightDuration, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.AlightDuration, value, true); }
		}

		/// <summary> The AppliedPassProductID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."APPLIEDPASSPRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AppliedPassProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.AppliedPassProductID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.AppliedPassProductID, value, true); }
		}

		/// <summary> The AppliedPassTicketID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."APPLIEDPASSTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AppliedPassTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.AppliedPassTicketID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.AppliedPassTicketID, value, true); }
		}

		/// <summary> The BlockNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."BLOCKNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BlockNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.BlockNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.BlockNumber, value, true); }
		}

		/// <summary> The BoardingGuid property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."BOARDINGGUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BoardingGuid
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.BoardingGuid, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.BoardingGuid, value, true); }
		}

		/// <summary> The BoardingTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."BOARDINGTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BoardingTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.BoardingTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.BoardingTime, value, true); }
		}

		/// <summary> The CancellationReference property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CANCELLATIONREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationReference
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.CancellationReference, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CancellationReference, value, true); }
		}

		/// <summary> The CancellationReferenceGuid property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CANCELLATIONREFERENCEGUID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CancellationReferenceGuid
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.CancellationReferenceGuid, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CancellationReferenceGuid, value, true); }
		}

		/// <summary> The CardHolderID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CARDHOLDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardHolderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.CardHolderID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CardHolderID, value, true); }
		}

		/// <summary> The CheckOutTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CHECKOUTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CheckOutTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.CheckOutTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CheckOutTime, value, true); }
		}

		/// <summary> The ClientID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)TransactionJournalFieldIndex.ClientID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CompletionState property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."COMPLETIONSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TransactionCompletionState CompletionState
		{
			get { return (VarioSL.Entities.Enumerations.TransactionCompletionState)GetValue((int)TransactionJournalFieldIndex.CompletionState, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CompletionState, value, true); }
		}

		/// <summary> The CorrectedLine property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CORRECTEDLINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CorrectedLine
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.CorrectedLine, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CorrectedLine, value, true); }
		}

		/// <summary> The CostCenterKey property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."COSTCENTERKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CostCenterKey
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.CostCenterKey, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CostCenterKey, value, true); }
		}

		/// <summary> The CourseNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."COURSENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CourseNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.CourseNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CourseNumber, value, true); }
		}

		/// <summary> The CreditCardAuthorizationID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CREDITCARDAUTHORIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreditCardAuthorizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.CreditCardAuthorizationID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CreditCardAuthorizationID, value, true); }
		}

		/// <summary> The CustomerGroup property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."CUSTOMERGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CustomerGroup
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.CustomerGroup, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.CustomerGroup, value, true); }
		}

		/// <summary> The DebtorNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DEBTORNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DebtorNumber
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.DebtorNumber, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.DebtorNumber, value, true); }
		}

		/// <summary> The DestinationZone property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DESTINATIONZONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DestinationZone
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.DestinationZone, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.DestinationZone, value, true); }
		}

		/// <summary> The DeviceBookingNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DEVICEBOOKINGNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceBookingNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.DeviceBookingNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.DeviceBookingNumber, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DEVICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The DeviceTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DEVICETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DeviceTime
		{
			get { return (System.DateTime)GetValue((int)TransactionJournalFieldIndex.DeviceTime, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.DeviceTime, value, true); }
		}

		/// <summary> The Direction property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DIRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Direction
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.Direction, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Direction, value, true); }
		}

		/// <summary> The Distance property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Distance
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.Distance, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Distance, value, true); }
		}

		/// <summary> The Duration property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."DURATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Duration
		{
			get { return (System.Int64)GetValue((int)TransactionJournalFieldIndex.Duration, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Duration, value, true); }
		}

		/// <summary> The ExternalTicketName property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."EXTERNALTICKETNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalTicketName
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.ExternalTicketName, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ExternalTicketName, value, true); }
		}

		/// <summary> The ExternalTransactionIdentifier property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."EXTERNALTRANSACTIONIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 64<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalTransactionIdentifier
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.ExternalTransactionIdentifier, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ExternalTransactionIdentifier, value, true); }
		}

		/// <summary> The FareAmount property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."FAREAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FareAmount
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.FareAmount, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.FareAmount, value, true); }
		}

		/// <summary> The FareMediaID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."FAREMEDIAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FareMediaID
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.FareMediaID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.FareMediaID, value, true); }
		}

		/// <summary> The FareMediaType property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."FAREMEDIATYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FareMediaType
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.FareMediaType, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.FareMediaType, value, true); }
		}

		/// <summary> The FareScaleName property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."FARESCALENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FareScaleName
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.FareScaleName, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.FareScaleName, value, true); }
		}

		/// <summary> The FillLevel property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."FILLLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FillLevel
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.FillLevel, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.FillLevel, value, true); }
		}

		/// <summary> The GeolocationLatitude property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."GEOLOCATIONLATITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GeolocationLatitude
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.GeolocationLatitude, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.GeolocationLatitude, value, true); }
		}

		/// <summary> The GeolocationLongitude property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."GEOLOCATIONLONGITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GeolocationLongitude
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.GeolocationLongitude, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.GeolocationLongitude, value, true); }
		}

		/// <summary> The GroupSize property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."GROUPSIZE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GroupSize
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.GroupSize, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.GroupSize, value, true); }
		}

		/// <summary> The InsertTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."INSERTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> InsertTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.InsertTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.InsertTime, value, true); }
		}

		/// <summary> The JourneyStartTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."JOURNEYSTARTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> JourneyStartTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.JourneyStartTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.JourneyStartTime, value, true); }
		}

		/// <summary> The LastModified property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)TransactionJournalFieldIndex.LastModified, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.LastUser, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Line property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."LINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Line
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.Line, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Line, value, true); }
		}

		/// <summary> The MountingPlateNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."MOUNTINGPLATENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MountingPlateNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.MountingPlateNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.MountingPlateNumber, value, true); }
		}

		/// <summary> The Notes property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."NOTES"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.Notes, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Notes, value, true); }
		}

		/// <summary> The NoTrip property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."NOTRIP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NoTrip
		{
			get { return (System.Boolean)GetValue((int)TransactionJournalFieldIndex.NoTrip, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.NoTrip, value, true); }
		}

		/// <summary> The OperatorID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OperatorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.OperatorID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The OriginalDeviceResultType property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ORIGINALDEVICERESULTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.FarePaymentResultType> OriginalDeviceResultType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.FarePaymentResultType>)GetValue((int)TransactionJournalFieldIndex.OriginalDeviceResultType, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.OriginalDeviceResultType, value, true); }
		}

		/// <summary> The OriginalOnlineResultType property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ORIGINALONLINERESULTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.FarePaymentResultType> OriginalOnlineResultType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.FarePaymentResultType>)GetValue((int)TransactionJournalFieldIndex.OriginalOnlineResultType, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.OriginalOnlineResultType, value, true); }
		}

		/// <summary> The OriginalSingleFare property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ORIGINALSINGLEFARE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OriginalSingleFare
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.OriginalSingleFare, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.OriginalSingleFare, value, true); }
		}

		/// <summary> The ProductID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.ProductID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ProductID2 property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PRODUCTID2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID2
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.ProductID2, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ProductID2, value, true); }
		}

		/// <summary> The Properties property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PROPERTIES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TransactionJournalProperties> Properties
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TransactionJournalProperties>)GetValue((int)TransactionJournalFieldIndex.Properties, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Properties, value, true); }
		}

		/// <summary> The PurseBalance property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PURSEBALANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseBalance
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.PurseBalance, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.PurseBalance, value, true); }
		}

		/// <summary> The PurseBalance2 property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PURSEBALANCE2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseBalance2
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.PurseBalance2, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.PurseBalance2, value, true); }
		}

		/// <summary> The PurseCredit property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PURSECREDIT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseCredit
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.PurseCredit, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.PurseCredit, value, true); }
		}

		/// <summary> The PurseCredit2 property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."PURSECREDIT2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseCredit2
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.PurseCredit2, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.PurseCredit2, value, true); }
		}

		/// <summary> The ResultSource property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."RESULTSOURCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.ResultSource> ResultSource
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.ResultSource>)GetValue((int)TransactionJournalFieldIndex.ResultSource, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ResultSource, value, true); }
		}

		/// <summary> The ResultType property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."RESULTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.FarePaymentResultType ResultType
		{
			get { return (VarioSL.Entities.Enumerations.FarePaymentResultType)GetValue((int)TransactionJournalFieldIndex.ResultType, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ResultType, value, true); }
		}

		/// <summary> The RoundTripTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ROUNDTRIPTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoundTripTime
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.RoundTripTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.RoundTripTime, value, true); }
		}

		/// <summary> The RouteCode property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ROUTECODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RouteCode
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.RouteCode, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.RouteCode, value, true); }
		}

		/// <summary> The RouteNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ROUTENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RouteNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.RouteNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.RouteNumber, value, true); }
		}

		/// <summary> The SaleID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."SALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SaleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.SaleID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.SaleID, value, true); }
		}

		/// <summary> The SalesChannelID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."SALESCHANNELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SalesChannelID
		{
			get { return (System.Int64)GetValue((int)TransactionJournalFieldIndex.SalesChannelID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.SalesChannelID, value, true); }
		}

		/// <summary> The ShiftBegin property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."SHIFTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ShiftBegin
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.ShiftBegin, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ShiftBegin, value, true); }
		}

		/// <summary> The SmartTapCounter property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."SMARTTAPCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SmartTapCounter
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.SmartTapCounter, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.SmartTapCounter, value, true); }
		}

		/// <summary> The SmartTapID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."SMARTTAPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SmartTapID
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.SmartTapID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.SmartTapID, value, true); }
		}

		/// <summary> The StopCode property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."STOPCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StopCode
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.StopCode, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.StopCode, value, true); }
		}

		/// <summary> The StopNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."STOPNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> StopNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.StopNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.StopNumber, value, true); }
		}

		/// <summary> The TariffDate property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TARIFFDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TariffDate
		{
			get { return (System.DateTime)GetValue((int)TransactionJournalFieldIndex.TariffDate, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TariffDate, value, true); }
		}

		/// <summary> The TariffVersion property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TARIFFVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TariffVersion
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.TariffVersion, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TariffVersion, value, true); }
		}

		/// <summary> The TaxAmount property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TAXAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxAmount
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TaxAmount, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TaxAmount, value, true); }
		}

		/// <summary> The TaxLocationCode property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TAXLOCATIONCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TaxLocationCode
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.TaxLocationCode, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TaxLocationCode, value, true); }
		}

		/// <summary> The TaxRate property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TAXRATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 3, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> TaxRate
		{
			get { return (Nullable<System.Double>)GetValue((int)TransactionJournalFieldIndex.TaxRate, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TaxRate, value, true); }
		}

		/// <summary> The TaxTicketID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TAXTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TaxTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.TaxTicketID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TaxTicketID, value, true); }
		}

		/// <summary> The TicketID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.TicketID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TokenCardID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TOKENCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TokenCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.TokenCardID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TokenCardID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)TransactionJournalFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TransactionID
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.TransactionID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TransactionJournalID
		{
			get { return (System.Int64)GetValue((int)TransactionJournalFieldIndex.TransactionJournalID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The TransactionNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TransactionNumber
		{
			get { return (System.Int32)GetValue((int)TransactionJournalFieldIndex.TransactionNumber, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionNumber, value, true); }
		}

		/// <summary> The TransactionTime property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TransactionTime
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TransactionTime, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionTime, value, true); }
		}

		/// <summary> The TransactionType property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSACTIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TransactionType TransactionType
		{
			get { return (VarioSL.Entities.Enumerations.TransactionType)GetValue((int)TransactionJournalFieldIndex.TransactionType, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransactionType, value, true); }
		}

		/// <summary> The Transferredfromcardid property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSFERREDFROMCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Transferredfromcardid
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.Transferredfromcardid, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Transferredfromcardid, value, true); }
		}

		/// <summary> The TransitAccountID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRANSITACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransitAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.TransitAccountID, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransitAccountID, value, true); }
		}

		/// <summary> The TransportState property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."RESPONSESTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TransactionTransportState TransportState
		{
			get { return (VarioSL.Entities.Enumerations.TransactionTransportState)GetValue((int)TransactionJournalFieldIndex.TransportState, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TransportState, value, true); }
		}

		/// <summary> The TripCounter property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripCounter
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripCounter, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripCounter, value, true); }
		}

		/// <summary> The TripSerialNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPSERIALNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripSerialNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripSerialNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripSerialNumber, value, true); }
		}

		/// <summary> The TripTicketInternalNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPTICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripTicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripTicketInternalNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripTicketInternalNumber, value, true); }
		}

		/// <summary> The TripValue property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripValue, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripValue, value, true); }
		}

		/// <summary> The TripValue2 property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPVALUE2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripValue2
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripValue2, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripValue2, value, true); }
		}

		/// <summary> The TripValueFloor property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPVALUEFLOOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripValueFloor
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripValueFloor, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripValueFloor, value, true); }
		}

		/// <summary> The TripValueFloor2 property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."TRIPVALUEFLOOR2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripValueFloor2
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.TripValueFloor2, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.TripValueFloor2, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TransactionJournalFieldIndex.ValidTo, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The VanpoolGroupID property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."VANPOOLGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VanpoolGroupID
		{
			get { return (System.String)GetValue((int)TransactionJournalFieldIndex.VanpoolGroupID, true); }
			set	{ SetValue((int)TransactionJournalFieldIndex.VanpoolGroupID, value, true); }
		}

		/// <summary> The VehicleNumber property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."VEHICLENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VehicleNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.VehicleNumber, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.VehicleNumber, value, true); }
		}

		/// <summary> The WhitelistVersion property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."WHITELISTVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> WhitelistVersion
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.WhitelistVersion, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.WhitelistVersion, value, true); }
		}

		/// <summary> The WhitelistVersionCreated property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."WHITELISTVERSIONCREATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> WhitelistVersionCreated
		{
			get { return (Nullable<System.Int64>)GetValue((int)TransactionJournalFieldIndex.WhitelistVersionCreated, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.WhitelistVersionCreated, value, true); }
		}

		/// <summary> The Zone property of the Entity TransactionJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_TRANSACTIONJOURNAL"."ZONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Zone
		{
			get { return (Nullable<System.Int32>)GetValue((int)TransactionJournalFieldIndex.Zone, false); }
			set	{ SetValue((int)TransactionJournalFieldIndex.Zone, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ApportionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiApportions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ApportionCollection Apportions
		{
			get	{ return GetMultiApportions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Apportions. When set to true, Apportions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Apportions is accessed. You can always execute/ a forced fetch by calling GetMultiApportions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportions
		{
			get	{ return _alwaysFetchApportions; }
			set	{ _alwaysFetchApportions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Apportions already has been fetched. Setting this property to false when Apportions has been fetched
		/// will clear the Apportions collection well. Setting this property to true while Apportions hasn't been fetched disables lazy loading for Apportions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportions
		{
			get { return _alreadyFetchedApportions;}
			set 
			{
				if(_alreadyFetchedApportions && !value && (_apportions != null))
				{
					_apportions.Clear();
				}
				_alreadyFetchedApportions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CappingJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCappingJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CappingJournalCollection CappingJournals
		{
			get	{ return GetMultiCappingJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CappingJournals. When set to true, CappingJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CappingJournals is accessed. You can always execute/ a forced fetch by calling GetMultiCappingJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCappingJournals
		{
			get	{ return _alwaysFetchCappingJournals; }
			set	{ _alwaysFetchCappingJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CappingJournals already has been fetched. Setting this property to false when CappingJournals has been fetched
		/// will clear the CappingJournals collection well. Setting this property to true while CappingJournals hasn't been fetched disables lazy loading for CappingJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCappingJournals
		{
			get { return _alreadyFetchedCappingJournals;}
			set 
			{
				if(_alreadyFetchedCappingJournals && !value && (_cappingJournals != null))
				{
					_cappingJournals.Clear();
				}
				_alreadyFetchedCappingJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RevenueRecognitionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueRecognitions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection RevenueRecognitions
		{
			get	{ return GetMultiRevenueRecognitions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueRecognitions. When set to true, RevenueRecognitions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueRecognitions is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueRecognitions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueRecognitions
		{
			get	{ return _alwaysFetchRevenueRecognitions; }
			set	{ _alwaysFetchRevenueRecognitions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueRecognitions already has been fetched. Setting this property to false when RevenueRecognitions has been fetched
		/// will clear the RevenueRecognitions collection well. Setting this property to true while RevenueRecognitions hasn't been fetched disables lazy loading for RevenueRecognitions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueRecognitions
		{
			get { return _alreadyFetchedRevenueRecognitions;}
			set 
			{
				if(_alreadyFetchedRevenueRecognitions && !value && (_revenueRecognitions != null))
				{
					_revenueRecognitions.Clear();
				}
				_alreadyFetchedRevenueRecognitions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RevenueSettlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRevenueSettlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RevenueSettlementCollection RevenueSettlements
		{
			get	{ return GetMultiRevenueSettlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueSettlements. When set to true, RevenueSettlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueSettlements is accessed. You can always execute/ a forced fetch by calling GetMultiRevenueSettlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueSettlements
		{
			get	{ return _alwaysFetchRevenueSettlements; }
			set	{ _alwaysFetchRevenueSettlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueSettlements already has been fetched. Setting this property to false when RevenueSettlements has been fetched
		/// will clear the RevenueSettlements collection well. Setting this property to true while RevenueSettlements hasn't been fetched disables lazy loading for RevenueSettlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueSettlements
		{
			get { return _alreadyFetchedRevenueSettlements;}
			set 
			{
				if(_alreadyFetchedRevenueSettlements && !value && (_revenueSettlements != null))
				{
					_revenueSettlements.Clear();
				}
				_alreadyFetchedRevenueSettlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationToTransactionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolationToTransactions()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationToTransactionCollection RuleViolationToTransactions
		{
			get	{ return GetMultiRuleViolationToTransactions(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolationToTransactions. When set to true, RuleViolationToTransactions is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolationToTransactions is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolationToTransactions(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolationToTransactions
		{
			get	{ return _alwaysFetchRuleViolationToTransactions; }
			set	{ _alwaysFetchRuleViolationToTransactions = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolationToTransactions already has been fetched. Setting this property to false when RuleViolationToTransactions has been fetched
		/// will clear the RuleViolationToTransactions collection well. Setting this property to true while RuleViolationToTransactions hasn't been fetched disables lazy loading for RuleViolationToTransactions</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolationToTransactions
		{
			get { return _alreadyFetchedRuleViolationToTransactions;}
			set 
			{
				if(_alreadyFetchedRuleViolationToTransactions && !value && (_ruleViolationToTransactions != null))
				{
					_ruleViolationToTransactions.Clear();
				}
				_alreadyFetchedRuleViolationToTransactions = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalCorrectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournalCorrections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCorrectionCollection TransactionJournalCorrections
		{
			get	{ return GetMultiTransactionJournalCorrections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournalCorrections. When set to true, TransactionJournalCorrections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournalCorrections is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournalCorrections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournalCorrections
		{
			get	{ return _alwaysFetchTransactionJournalCorrections; }
			set	{ _alwaysFetchTransactionJournalCorrections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournalCorrections already has been fetched. Setting this property to false when TransactionJournalCorrections has been fetched
		/// will clear the TransactionJournalCorrections collection well. Setting this property to true while TransactionJournalCorrections hasn't been fetched disables lazy loading for TransactionJournalCorrections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournalCorrections
		{
			get { return _alreadyFetchedTransactionJournalCorrections;}
			set 
			{
				if(_alreadyFetchedTransactionJournalCorrections && !value && (_transactionJournalCorrections != null))
				{
					_transactionJournalCorrections.Clear();
				}
				_alreadyFetchedTransactionJournalCorrections = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionToInspectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionToInspections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionToInspectionCollection TransactionToInspections
		{
			get	{ return GetMultiTransactionToInspections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionToInspections. When set to true, TransactionToInspections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionToInspections is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionToInspections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionToInspections
		{
			get	{ return _alwaysFetchTransactionToInspections; }
			set	{ _alwaysFetchTransactionToInspections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionToInspections already has been fetched. Setting this property to false when TransactionToInspections has been fetched
		/// will clear the TransactionToInspections collection well. Setting this property to true while TransactionToInspections hasn't been fetched disables lazy loading for TransactionToInspections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionToInspections
		{
			get { return _alreadyFetchedTransactionToInspections;}
			set 
			{
				if(_alreadyFetchedTransactionToInspections && !value && (_transactionToInspections != null))
				{
					_transactionToInspections.Clear();
				}
				_alreadyFetchedTransactionToInspections = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAppliedPassTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity AppliedPassTicket
		{
			get	{ return GetSingleAppliedPassTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAppliedPassTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournalsForAppliedPassTicket", "AppliedPassTicket", _appliedPassTicket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AppliedPassTicket. When set to true, AppliedPassTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AppliedPassTicket is accessed. You can always execute a forced fetch by calling GetSingleAppliedPassTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAppliedPassTicket
		{
			get	{ return _alwaysFetchAppliedPassTicket; }
			set	{ _alwaysFetchAppliedPassTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AppliedPassTicket already has been fetched. Setting this property to false when AppliedPassTicket has been fetched
		/// will set AppliedPassTicket to null as well. Setting this property to true while AppliedPassTicket hasn't been fetched disables lazy loading for AppliedPassTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAppliedPassTicket
		{
			get { return _alreadyFetchedAppliedPassTicket;}
			set 
			{
				if(_alreadyFetchedAppliedPassTicket && !value)
				{
					this.AppliedPassTicket = null;
				}
				_alreadyFetchedAppliedPassTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AppliedPassTicket is not found
		/// in the database. When set to true, AppliedPassTicket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AppliedPassTicketReturnsNewIfNotFound
		{
			get	{ return _appliedPassTicketReturnsNewIfNotFound; }
			set { _appliedPassTicketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket_
		{
			get	{ return GetSingleTicket_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals_", "Ticket_", _ticket_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket_. When set to true, Ticket_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket_ is accessed. You can always execute a forced fetch by calling GetSingleTicket_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket_
		{
			get	{ return _alwaysFetchTicket_; }
			set	{ _alwaysFetchTicket_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket_ already has been fetched. Setting this property to false when Ticket_ has been fetched
		/// will set Ticket_ to null as well. Setting this property to true while Ticket_ hasn't been fetched disables lazy loading for Ticket_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket_
		{
			get { return _alreadyFetchedTicket_;}
			set 
			{
				if(_alreadyFetchedTicket_ && !value)
				{
					this.Ticket_ = null;
				}
				_alreadyFetchedTicket_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket_ is not found
		/// in the database. When set to true, Ticket_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool Ticket_ReturnsNewIfNotFound
		{
			get	{ return _ticket_ReturnsNewIfNotFound; }
			set { _ticket_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card_
		{
			get	{ return GetSingleCard_(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard_(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals_", "Card_", _card_, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card_. When set to true, Card_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card_ is accessed. You can always execute a forced fetch by calling GetSingleCard_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard_
		{
			get	{ return _alwaysFetchCard_; }
			set	{ _alwaysFetchCard_ = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card_ already has been fetched. Setting this property to false when Card_ has been fetched
		/// will set Card_ to null as well. Setting this property to true while Card_ hasn't been fetched disables lazy loading for Card_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard_
		{
			get { return _alreadyFetchedCard_;}
			set 
			{
				if(_alreadyFetchedCard_ && !value)
				{
					this.Card_ = null;
				}
				_alreadyFetchedCard_ = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card_ is not found
		/// in the database. When set to true, Card_ will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool Card_ReturnsNewIfNotFound
		{
			get	{ return _card_ReturnsNewIfNotFound; }
			set { _card_ReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTokenCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity TokenCard
		{
			get	{ return GetSingleTokenCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTokenCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TokenTransactionJournals", "TokenCard", _tokenCard, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TokenCard. When set to true, TokenCard is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TokenCard is accessed. You can always execute a forced fetch by calling GetSingleTokenCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTokenCard
		{
			get	{ return _alwaysFetchTokenCard; }
			set	{ _alwaysFetchTokenCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TokenCard already has been fetched. Setting this property to false when TokenCard has been fetched
		/// will set TokenCard to null as well. Setting this property to true while TokenCard hasn't been fetched disables lazy loading for TokenCard</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTokenCard
		{
			get { return _alreadyFetchedTokenCard;}
			set 
			{
				if(_alreadyFetchedTokenCard && !value)
				{
					this.TokenCard = null;
				}
				_alreadyFetchedTokenCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TokenCard is not found
		/// in the database. When set to true, TokenCard will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TokenCardReturnsNewIfNotFound
		{
			get	{ return _tokenCardReturnsNewIfNotFound; }
			set { _tokenCardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardHolderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderEntity CardHolder
		{
			get	{ return GetSingleCardHolder(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardHolder(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "CardHolder", _cardHolder, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolder. When set to true, CardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolder is accessed. You can always execute a forced fetch by calling GetSingleCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolder
		{
			get	{ return _alwaysFetchCardHolder; }
			set	{ _alwaysFetchCardHolder = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolder already has been fetched. Setting this property to false when CardHolder has been fetched
		/// will set CardHolder to null as well. Setting this property to true while CardHolder hasn't been fetched disables lazy loading for CardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolder
		{
			get { return _alreadyFetchedCardHolder;}
			set 
			{
				if(_alreadyFetchedCardHolder && !value)
				{
					this.CardHolder = null;
				}
				_alreadyFetchedCardHolder = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolder is not found
		/// in the database. When set to true, CardHolder will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderReturnsNewIfNotFound
		{
			get	{ return _cardHolderReturnsNewIfNotFound; }
			set { _cardHolderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CreditCardAuthorizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCreditCardAuthorization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CreditCardAuthorizationEntity CreditCardAuthorization
		{
			get	{ return GetSingleCreditCardAuthorization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCreditCardAuthorization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "CreditCardAuthorization", _creditCardAuthorization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorization. When set to true, CreditCardAuthorization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorization is accessed. You can always execute a forced fetch by calling GetSingleCreditCardAuthorization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorization
		{
			get	{ return _alwaysFetchCreditCardAuthorization; }
			set	{ _alwaysFetchCreditCardAuthorization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorization already has been fetched. Setting this property to false when CreditCardAuthorization has been fetched
		/// will set CreditCardAuthorization to null as well. Setting this property to true while CreditCardAuthorization hasn't been fetched disables lazy loading for CreditCardAuthorization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorization
		{
			get { return _alreadyFetchedCreditCardAuthorization;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorization && !value)
				{
					this.CreditCardAuthorization = null;
				}
				_alreadyFetchedCreditCardAuthorization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CreditCardAuthorization is not found
		/// in the database. When set to true, CreditCardAuthorization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CreditCardAuthorizationReturnsNewIfNotFound
		{
			get	{ return _creditCardAuthorizationReturnsNewIfNotFound; }
			set { _creditCardAuthorizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAppliedPassProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity AppliedPassProduct
		{
			get	{ return GetSingleAppliedPassProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAppliedPassProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournalsForAppledPass", "AppliedPassProduct", _appliedPassProduct, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AppliedPassProduct. When set to true, AppliedPassProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AppliedPassProduct is accessed. You can always execute a forced fetch by calling GetSingleAppliedPassProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAppliedPassProduct
		{
			get	{ return _alwaysFetchAppliedPassProduct; }
			set	{ _alwaysFetchAppliedPassProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AppliedPassProduct already has been fetched. Setting this property to false when AppliedPassProduct has been fetched
		/// will set AppliedPassProduct to null as well. Setting this property to true while AppliedPassProduct hasn't been fetched disables lazy loading for AppliedPassProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAppliedPassProduct
		{
			get { return _alreadyFetchedAppliedPassProduct;}
			set 
			{
				if(_alreadyFetchedAppliedPassProduct && !value)
				{
					this.AppliedPassProduct = null;
				}
				_alreadyFetchedAppliedPassProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AppliedPassProduct is not found
		/// in the database. When set to true, AppliedPassProduct will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AppliedPassProductReturnsNewIfNotFound
		{
			get	{ return _appliedPassProductReturnsNewIfNotFound; }
			set { _appliedPassProductReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity Sale
		{
			get	{ return GetSingleSale(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSale(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TransactionJournals", "Sale", _sale, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Sale. When set to true, Sale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sale is accessed. You can always execute a forced fetch by calling GetSingleSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSale
		{
			get	{ return _alwaysFetchSale; }
			set	{ _alwaysFetchSale = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sale already has been fetched. Setting this property to false when Sale has been fetched
		/// will set Sale to null as well. Setting this property to true while Sale hasn't been fetched disables lazy loading for Sale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSale
		{
			get { return _alreadyFetchedSale;}
			set 
			{
				if(_alreadyFetchedSale && !value)
				{
					this.Sale = null;
				}
				_alreadyFetchedSale = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Sale is not found
		/// in the database. When set to true, Sale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SaleReturnsNewIfNotFound
		{
			get	{ return _saleReturnsNewIfNotFound; }
			set { _saleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransactionJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCancellationReferenceTransaction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransactionJournalEntity CancellationReferenceTransaction
		{
			get	{ return GetSingleCancellationReferenceTransaction(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCancellationReferenceTransaction(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "", "CancellationReferenceTransaction", _cancellationReferenceTransaction, false); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CancellationReferenceTransaction. When set to true, CancellationReferenceTransaction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CancellationReferenceTransaction is accessed. You can always execute a forced fetch by calling GetSingleCancellationReferenceTransaction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCancellationReferenceTransaction
		{
			get	{ return _alwaysFetchCancellationReferenceTransaction; }
			set	{ _alwaysFetchCancellationReferenceTransaction = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CancellationReferenceTransaction already has been fetched. Setting this property to false when CancellationReferenceTransaction has been fetched
		/// will set CancellationReferenceTransaction to null as well. Setting this property to true while CancellationReferenceTransaction hasn't been fetched disables lazy loading for CancellationReferenceTransaction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCancellationReferenceTransaction
		{
			get { return _alreadyFetchedCancellationReferenceTransaction;}
			set 
			{
				if(_alreadyFetchedCancellationReferenceTransaction && !value)
				{
					this.CancellationReferenceTransaction = null;
				}
				_alreadyFetchedCancellationReferenceTransaction = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CancellationReferenceTransaction is not found
		/// in the database. When set to true, CancellationReferenceTransaction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CancellationReferenceTransactionReturnsNewIfNotFound
		{
			get	{ return _cancellationReferenceTransactionReturnsNewIfNotFound; }
			set { _cancellationReferenceTransactionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SalesRevenueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSalesRevenue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SalesRevenueEntity SalesRevenue
		{
			get	{ return GetSingleSalesRevenue(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSalesRevenue(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_salesRevenue !=null);
						DesetupSyncSalesRevenue(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("SalesRevenue");
						}
					}
					else
					{
						if(_salesRevenue!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "TransactionJournal");
							SetupSyncSalesRevenue(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SalesRevenue. When set to true, SalesRevenue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SalesRevenue is accessed. You can always execute a forced fetch by calling GetSingleSalesRevenue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSalesRevenue
		{
			get	{ return _alwaysFetchSalesRevenue; }
			set	{ _alwaysFetchSalesRevenue = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property SalesRevenue already has been fetched. Setting this property to false when SalesRevenue has been fetched
		/// will set SalesRevenue to null as well. Setting this property to true while SalesRevenue hasn't been fetched disables lazy loading for SalesRevenue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSalesRevenue
		{
			get { return _alreadyFetchedSalesRevenue;}
			set 
			{
				if(_alreadyFetchedSalesRevenue && !value)
				{
					this.SalesRevenue = null;
				}
				_alreadyFetchedSalesRevenue = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SalesRevenue is not found
		/// in the database. When set to true, SalesRevenue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SalesRevenueReturnsNewIfNotFound
		{
			get	{ return _salesRevenueReturnsNewIfNotFound; }
			set	{ _salesRevenueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TransactionJournalEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
