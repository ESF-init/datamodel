﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ApportionmentResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ApportionmentResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ApportionmentEntity _apportionment;
		private bool	_alwaysFetchApportionment, _alreadyFetchedApportionment, _apportionmentReturnsNewIfNotFound;
		private ClientEntity _acquirerClient;
		private bool	_alwaysFetchAcquirerClient, _alreadyFetchedAcquirerClient, _acquirerClientReturnsNewIfNotFound;
		private ClientEntity _fromClient;
		private bool	_alwaysFetchFromClient, _alreadyFetchedFromClient, _fromClientReturnsNewIfNotFound;
		private ClientEntity _toClient;
		private bool	_alwaysFetchToClient, _alreadyFetchedToClient, _toClientReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Apportionment</summary>
			public static readonly string Apportionment = "Apportionment";
			/// <summary>Member name AcquirerClient</summary>
			public static readonly string AcquirerClient = "AcquirerClient";
			/// <summary>Member name FromClient</summary>
			public static readonly string FromClient = "FromClient";
			/// <summary>Member name ToClient</summary>
			public static readonly string ToClient = "ToClient";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ApportionmentResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ApportionmentResultEntity() :base("ApportionmentResultEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		public ApportionmentResultEntity(System.Int64 apportionmentResultID):base("ApportionmentResultEntity")
		{
			InitClassFetch(apportionmentResultID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ApportionmentResultEntity(System.Int64 apportionmentResultID, IPrefetchPath prefetchPathToUse):base("ApportionmentResultEntity")
		{
			InitClassFetch(apportionmentResultID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="validator">The custom validator object for this ApportionmentResultEntity</param>
		public ApportionmentResultEntity(System.Int64 apportionmentResultID, IValidator validator):base("ApportionmentResultEntity")
		{
			InitClassFetch(apportionmentResultID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApportionmentResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_apportionment = (ApportionmentEntity)info.GetValue("_apportionment", typeof(ApportionmentEntity));
			if(_apportionment!=null)
			{
				_apportionment.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_apportionmentReturnsNewIfNotFound = info.GetBoolean("_apportionmentReturnsNewIfNotFound");
			_alwaysFetchApportionment = info.GetBoolean("_alwaysFetchApportionment");
			_alreadyFetchedApportionment = info.GetBoolean("_alreadyFetchedApportionment");

			_acquirerClient = (ClientEntity)info.GetValue("_acquirerClient", typeof(ClientEntity));
			if(_acquirerClient!=null)
			{
				_acquirerClient.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_acquirerClientReturnsNewIfNotFound = info.GetBoolean("_acquirerClientReturnsNewIfNotFound");
			_alwaysFetchAcquirerClient = info.GetBoolean("_alwaysFetchAcquirerClient");
			_alreadyFetchedAcquirerClient = info.GetBoolean("_alreadyFetchedAcquirerClient");

			_fromClient = (ClientEntity)info.GetValue("_fromClient", typeof(ClientEntity));
			if(_fromClient!=null)
			{
				_fromClient.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fromClientReturnsNewIfNotFound = info.GetBoolean("_fromClientReturnsNewIfNotFound");
			_alwaysFetchFromClient = info.GetBoolean("_alwaysFetchFromClient");
			_alreadyFetchedFromClient = info.GetBoolean("_alreadyFetchedFromClient");

			_toClient = (ClientEntity)info.GetValue("_toClient", typeof(ClientEntity));
			if(_toClient!=null)
			{
				_toClient.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_toClientReturnsNewIfNotFound = info.GetBoolean("_toClientReturnsNewIfNotFound");
			_alwaysFetchToClient = info.GetBoolean("_alwaysFetchToClient");
			_alreadyFetchedToClient = info.GetBoolean("_alreadyFetchedToClient");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ApportionmentResultFieldIndex)fieldIndex)
			{
				case ApportionmentResultFieldIndex.AcquirerID:
					DesetupSyncAcquirerClient(true, false);
					_alreadyFetchedAcquirerClient = false;
					break;
				case ApportionmentResultFieldIndex.ApportionmentID:
					DesetupSyncApportionment(true, false);
					_alreadyFetchedApportionment = false;
					break;
				case ApportionmentResultFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case ApportionmentResultFieldIndex.FromClientID:
					DesetupSyncFromClient(true, false);
					_alreadyFetchedFromClient = false;
					break;
				case ApportionmentResultFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				case ApportionmentResultFieldIndex.ToClientID:
					DesetupSyncToClient(true, false);
					_alreadyFetchedToClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedApportionment = (_apportionment != null);
			_alreadyFetchedAcquirerClient = (_acquirerClient != null);
			_alreadyFetchedFromClient = (_fromClient != null);
			_alreadyFetchedToClient = (_toClient != null);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedCard = (_card != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Apportionment":
					toReturn.Add(Relations.ApportionmentEntityUsingApportionmentID);
					break;
				case "AcquirerClient":
					toReturn.Add(Relations.ClientEntityUsingAcquirerID);
					break;
				case "FromClient":
					toReturn.Add(Relations.ClientEntityUsingFromClientID);
					break;
				case "ToClient":
					toReturn.Add(Relations.ClientEntityUsingToClientID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_apportionment", (!this.MarkedForDeletion?_apportionment:null));
			info.AddValue("_apportionmentReturnsNewIfNotFound", _apportionmentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchApportionment", _alwaysFetchApportionment);
			info.AddValue("_alreadyFetchedApportionment", _alreadyFetchedApportionment);
			info.AddValue("_acquirerClient", (!this.MarkedForDeletion?_acquirerClient:null));
			info.AddValue("_acquirerClientReturnsNewIfNotFound", _acquirerClientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAcquirerClient", _alwaysFetchAcquirerClient);
			info.AddValue("_alreadyFetchedAcquirerClient", _alreadyFetchedAcquirerClient);
			info.AddValue("_fromClient", (!this.MarkedForDeletion?_fromClient:null));
			info.AddValue("_fromClientReturnsNewIfNotFound", _fromClientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFromClient", _alwaysFetchFromClient);
			info.AddValue("_alreadyFetchedFromClient", _alreadyFetchedFromClient);
			info.AddValue("_toClient", (!this.MarkedForDeletion?_toClient:null));
			info.AddValue("_toClientReturnsNewIfNotFound", _toClientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchToClient", _alwaysFetchToClient);
			info.AddValue("_alreadyFetchedToClient", _alreadyFetchedToClient);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Apportionment":
					_alreadyFetchedApportionment = true;
					this.Apportionment = (ApportionmentEntity)entity;
					break;
				case "AcquirerClient":
					_alreadyFetchedAcquirerClient = true;
					this.AcquirerClient = (ClientEntity)entity;
					break;
				case "FromClient":
					_alreadyFetchedFromClient = true;
					this.FromClient = (ClientEntity)entity;
					break;
				case "ToClient":
					_alreadyFetchedToClient = true;
					this.ToClient = (ClientEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Apportionment":
					SetupSyncApportionment(relatedEntity);
					break;
				case "AcquirerClient":
					SetupSyncAcquirerClient(relatedEntity);
					break;
				case "FromClient":
					SetupSyncFromClient(relatedEntity);
					break;
				case "ToClient":
					SetupSyncToClient(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Apportionment":
					DesetupSyncApportionment(false, true);
					break;
				case "AcquirerClient":
					DesetupSyncAcquirerClient(false, true);
					break;
				case "FromClient":
					DesetupSyncFromClient(false, true);
					break;
				case "ToClient":
					DesetupSyncToClient(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_apportionment!=null)
			{
				toReturn.Add(_apportionment);
			}
			if(_acquirerClient!=null)
			{
				toReturn.Add(_acquirerClient);
			}
			if(_fromClient!=null)
			{
				toReturn.Add(_fromClient);
			}
			if(_toClient!=null)
			{
				toReturn.Add(_toClient);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 apportionmentResultID)
		{
			return FetchUsingPK(apportionmentResultID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 apportionmentResultID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(apportionmentResultID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 apportionmentResultID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(apportionmentResultID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 apportionmentResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(apportionmentResultID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ApportionmentResultID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ApportionmentResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ApportionmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ApportionmentEntity' which is related to this entity.</returns>
		public ApportionmentEntity GetSingleApportionment()
		{
			return GetSingleApportionment(false);
		}

		/// <summary> Retrieves the related entity of type 'ApportionmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ApportionmentEntity' which is related to this entity.</returns>
		public virtual ApportionmentEntity GetSingleApportionment(bool forceFetch)
		{
			if( ( !_alreadyFetchedApportionment || forceFetch || _alwaysFetchApportionment) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ApportionmentEntityUsingApportionmentID);
				ApportionmentEntity newEntity = new ApportionmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ApportionmentID);
				}
				if(fetchResult)
				{
					newEntity = (ApportionmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_apportionmentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Apportionment = newEntity;
				_alreadyFetchedApportionment = fetchResult;
			}
			return _apportionment;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleAcquirerClient()
		{
			return GetSingleAcquirerClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleAcquirerClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedAcquirerClient || forceFetch || _alwaysFetchAcquirerClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingAcquirerID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AcquirerID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_acquirerClientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AcquirerClient = newEntity;
				_alreadyFetchedAcquirerClient = fetchResult;
			}
			return _acquirerClient;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleFromClient()
		{
			return GetSingleFromClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleFromClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedFromClient || forceFetch || _alwaysFetchFromClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingFromClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FromClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fromClientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FromClient = newEntity;
				_alreadyFetchedFromClient = fetchResult;
			}
			return _fromClient;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleToClient()
		{
			return GetSingleToClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleToClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedToClient || forceFetch || _alwaysFetchToClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingToClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ToClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_toClientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ToClient = newEntity;
				_alreadyFetchedToClient = fetchResult;
			}
			return _toClient;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID);
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Apportionment", _apportionment);
			toReturn.Add("AcquirerClient", _acquirerClient);
			toReturn.Add("FromClient", _fromClient);
			toReturn.Add("ToClient", _toClient);
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("Card", _card);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="validator">The validator object for this ApportionmentResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 apportionmentResultID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(apportionmentResultID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_apportionmentReturnsNewIfNotFound = false;
			_acquirerClientReturnsNewIfNotFound = false;
			_fromClientReturnsNewIfNotFound = false;
			_toClientReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AcquirerID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApportionmentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApportionmentResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JourneyReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TikNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _apportionment</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncApportionment(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _apportionment, new PropertyChangedEventHandler( OnApportionmentPropertyChanged ), "Apportionment", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ApportionmentEntityUsingApportionmentIDStatic, true, signalRelatedEntity, "ApportionmentResults", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.ApportionmentID } );		
			_apportionment = null;
		}
		
		/// <summary> setups the sync logic for member _apportionment</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncApportionment(IEntityCore relatedEntity)
		{
			if(_apportionment!=relatedEntity)
			{		
				DesetupSyncApportionment(true, true);
				_apportionment = (ApportionmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _apportionment, new PropertyChangedEventHandler( OnApportionmentPropertyChanged ), "Apportionment", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ApportionmentEntityUsingApportionmentIDStatic, true, ref _alreadyFetchedApportionment, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnApportionmentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _acquirerClient</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAcquirerClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _acquirerClient, new PropertyChangedEventHandler( OnAcquirerClientPropertyChanged ), "AcquirerClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingAcquirerIDStatic, true, signalRelatedEntity, "ApportionmentResults", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.AcquirerID } );		
			_acquirerClient = null;
		}
		
		/// <summary> setups the sync logic for member _acquirerClient</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAcquirerClient(IEntityCore relatedEntity)
		{
			if(_acquirerClient!=relatedEntity)
			{		
				DesetupSyncAcquirerClient(true, true);
				_acquirerClient = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _acquirerClient, new PropertyChangedEventHandler( OnAcquirerClientPropertyChanged ), "AcquirerClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingAcquirerIDStatic, true, ref _alreadyFetchedAcquirerClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAcquirerClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fromClient</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFromClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fromClient, new PropertyChangedEventHandler( OnFromClientPropertyChanged ), "FromClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingFromClientIDStatic, true, signalRelatedEntity, "ApportionmentResults1", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.FromClientID } );		
			_fromClient = null;
		}
		
		/// <summary> setups the sync logic for member _fromClient</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFromClient(IEntityCore relatedEntity)
		{
			if(_fromClient!=relatedEntity)
			{		
				DesetupSyncFromClient(true, true);
				_fromClient = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fromClient, new PropertyChangedEventHandler( OnFromClientPropertyChanged ), "FromClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingFromClientIDStatic, true, ref _alreadyFetchedFromClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFromClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _toClient</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncToClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _toClient, new PropertyChangedEventHandler( OnToClientPropertyChanged ), "ToClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingToClientIDStatic, true, signalRelatedEntity, "ApportionmentResults2", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.ToClientID } );		
			_toClient = null;
		}
		
		/// <summary> setups the sync logic for member _toClient</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncToClient(IEntityCore relatedEntity)
		{
			if(_toClient!=relatedEntity)
			{		
				DesetupSyncToClient(true, true);
				_toClient = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _toClient, new PropertyChangedEventHandler( OnToClientPropertyChanged ), "ToClient", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.ClientEntityUsingToClientIDStatic, true, ref _alreadyFetchedToClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnToClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "ApportionmentResults", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "ApportionmentResults", resetFKFields, new int[] { (int)ApportionmentResultFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticApportionmentResultRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="apportionmentResultID">PK value for ApportionmentResult which data should be fetched into this ApportionmentResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 apportionmentResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ApportionmentResultFieldIndex.ApportionmentResultID].ForcedCurrentValueWrite(apportionmentResultID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateApportionmentResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ApportionmentResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ApportionmentResultRelations Relations
		{
			get	{ return new ApportionmentResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Apportionment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathApportionment
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ApportionmentCollection(), (IEntityRelation)GetRelationsForField("Apportionment")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.ApportionmentEntity, 0, null, null, null, "Apportionment", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAcquirerClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("AcquirerClient")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "AcquirerClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFromClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("FromClient")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "FromClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathToClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ToClient")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "ToClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.ApportionmentResultEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AcquirerID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."ACQUIRERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AcquirerID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionmentResultFieldIndex.AcquirerID, false); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.AcquirerID, value, true); }
		}

		/// <summary> The Amount property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Amount
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionmentResultFieldIndex.Amount, false); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.Amount, value, true); }
		}

		/// <summary> The ApportionmentID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."APPORTIONMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ApportionmentID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.ApportionmentID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.ApportionmentID, value, true); }
		}

		/// <summary> The ApportionmentResultID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."APPORTIONMENTRESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ApportionmentResultID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.ApportionmentResultID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.ApportionmentResultID, value, true); }
		}

		/// <summary> The CardID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.CardID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.CardID, value, true); }
		}

		/// <summary> The DataRowCreationDate property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApportionmentResultFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The Direction property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."DIRECTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Direction
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.Direction, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.Direction, value, true); }
		}

		/// <summary> The FromClientID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."FROMCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FromClientID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.FromClientID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.FromClientID, value, true); }
		}

		/// <summary> The FromTransactionID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."FROMTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FromTransactionID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.FromTransactionID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.FromTransactionID, value, true); }
		}

		/// <summary> The JourneyReference property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."JOURNEYREF"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 JourneyReference
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.JourneyReference, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.JourneyReference, value, true); }
		}

		/// <summary> The LastModified property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ApportionmentResultFieldIndex.LastModified, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ApportionmentResultFieldIndex.LastUser, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TicketID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionmentResultFieldIndex.TicketID, false); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TikNumber property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."TIKNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TikNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionmentResultFieldIndex.TikNumber, false); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.TikNumber, value, true); }
		}

		/// <summary> The ToClientID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."TOCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ToClientID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.ToClientID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.ToClientID, value, true); }
		}

		/// <summary> The ToTransactionID property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."TOTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ToTransactionID
		{
			get { return (System.Int64)GetValue((int)ApportionmentResultFieldIndex.ToTransactionID, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.ToTransactionID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ApportionmentResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_APPORTIONMENTRESULT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ApportionmentResultFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ApportionmentResultFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ApportionmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleApportionment()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ApportionmentEntity Apportionment
		{
			get	{ return GetSingleApportionment(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncApportionment(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults", "Apportionment", _apportionment, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Apportionment. When set to true, Apportionment is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Apportionment is accessed. You can always execute a forced fetch by calling GetSingleApportionment(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchApportionment
		{
			get	{ return _alwaysFetchApportionment; }
			set	{ _alwaysFetchApportionment = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Apportionment already has been fetched. Setting this property to false when Apportionment has been fetched
		/// will set Apportionment to null as well. Setting this property to true while Apportionment hasn't been fetched disables lazy loading for Apportionment</summary>
		[Browsable(false)]
		public bool AlreadyFetchedApportionment
		{
			get { return _alreadyFetchedApportionment;}
			set 
			{
				if(_alreadyFetchedApportionment && !value)
				{
					this.Apportionment = null;
				}
				_alreadyFetchedApportionment = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Apportionment is not found
		/// in the database. When set to true, Apportionment will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ApportionmentReturnsNewIfNotFound
		{
			get	{ return _apportionmentReturnsNewIfNotFound; }
			set { _apportionmentReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAcquirerClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity AcquirerClient
		{
			get	{ return GetSingleAcquirerClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAcquirerClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults", "AcquirerClient", _acquirerClient, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AcquirerClient. When set to true, AcquirerClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AcquirerClient is accessed. You can always execute a forced fetch by calling GetSingleAcquirerClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAcquirerClient
		{
			get	{ return _alwaysFetchAcquirerClient; }
			set	{ _alwaysFetchAcquirerClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AcquirerClient already has been fetched. Setting this property to false when AcquirerClient has been fetched
		/// will set AcquirerClient to null as well. Setting this property to true while AcquirerClient hasn't been fetched disables lazy loading for AcquirerClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAcquirerClient
		{
			get { return _alreadyFetchedAcquirerClient;}
			set 
			{
				if(_alreadyFetchedAcquirerClient && !value)
				{
					this.AcquirerClient = null;
				}
				_alreadyFetchedAcquirerClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AcquirerClient is not found
		/// in the database. When set to true, AcquirerClient will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AcquirerClientReturnsNewIfNotFound
		{
			get	{ return _acquirerClientReturnsNewIfNotFound; }
			set { _acquirerClientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFromClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity FromClient
		{
			get	{ return GetSingleFromClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFromClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults1", "FromClient", _fromClient, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FromClient. When set to true, FromClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FromClient is accessed. You can always execute a forced fetch by calling GetSingleFromClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFromClient
		{
			get	{ return _alwaysFetchFromClient; }
			set	{ _alwaysFetchFromClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FromClient already has been fetched. Setting this property to false when FromClient has been fetched
		/// will set FromClient to null as well. Setting this property to true while FromClient hasn't been fetched disables lazy loading for FromClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFromClient
		{
			get { return _alreadyFetchedFromClient;}
			set 
			{
				if(_alreadyFetchedFromClient && !value)
				{
					this.FromClient = null;
				}
				_alreadyFetchedFromClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FromClient is not found
		/// in the database. When set to true, FromClient will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FromClientReturnsNewIfNotFound
		{
			get	{ return _fromClientReturnsNewIfNotFound; }
			set { _fromClientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleToClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity ToClient
		{
			get	{ return GetSingleToClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncToClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults2", "ToClient", _toClient, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ToClient. When set to true, ToClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ToClient is accessed. You can always execute a forced fetch by calling GetSingleToClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchToClient
		{
			get	{ return _alwaysFetchToClient; }
			set	{ _alwaysFetchToClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ToClient already has been fetched. Setting this property to false when ToClient has been fetched
		/// will set ToClient to null as well. Setting this property to true while ToClient hasn't been fetched disables lazy loading for ToClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedToClient
		{
			get { return _alreadyFetchedToClient;}
			set 
			{
				if(_alreadyFetchedToClient && !value)
				{
					this.ToClient = null;
				}
				_alreadyFetchedToClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ToClient is not found
		/// in the database. When set to true, ToClient will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ToClientReturnsNewIfNotFound
		{
			get	{ return _toClientReturnsNewIfNotFound; }
			set { _toClientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ApportionmentResults", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ApportionmentResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
