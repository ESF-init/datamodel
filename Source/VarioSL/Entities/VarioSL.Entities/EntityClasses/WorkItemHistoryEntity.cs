﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WorkItemHistory'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class WorkItemHistoryEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private WorkItemEntity _workItem;
		private bool	_alwaysFetchWorkItem, _alreadyFetchedWorkItem, _workItemReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name WorkItem</summary>
			public static readonly string WorkItem = "WorkItem";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static WorkItemHistoryEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public WorkItemHistoryEntity() :base("WorkItemHistoryEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		public WorkItemHistoryEntity(System.Int64 workItemHistoryID):base("WorkItemHistoryEntity")
		{
			InitClassFetch(workItemHistoryID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WorkItemHistoryEntity(System.Int64 workItemHistoryID, IPrefetchPath prefetchPathToUse):base("WorkItemHistoryEntity")
		{
			InitClassFetch(workItemHistoryID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="validator">The custom validator object for this WorkItemHistoryEntity</param>
		public WorkItemHistoryEntity(System.Int64 workItemHistoryID, IValidator validator):base("WorkItemHistoryEntity")
		{
			InitClassFetch(workItemHistoryID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WorkItemHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_workItem = (WorkItemEntity)info.GetValue("_workItem", typeof(WorkItemEntity));
			if(_workItem!=null)
			{
				_workItem.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_workItemReturnsNewIfNotFound = info.GetBoolean("_workItemReturnsNewIfNotFound");
			_alwaysFetchWorkItem = info.GetBoolean("_alwaysFetchWorkItem");
			_alreadyFetchedWorkItem = info.GetBoolean("_alreadyFetchedWorkItem");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((WorkItemHistoryFieldIndex)fieldIndex)
			{
				case WorkItemHistoryFieldIndex.WorkItemID:
					DesetupSyncWorkItem(true, false);
					_alreadyFetchedWorkItem = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedWorkItem = (_workItem != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "WorkItem":
					toReturn.Add(Relations.WorkItemEntityUsingWorkItemID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_workItem", (!this.MarkedForDeletion?_workItem:null));
			info.AddValue("_workItemReturnsNewIfNotFound", _workItemReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchWorkItem", _alwaysFetchWorkItem);
			info.AddValue("_alreadyFetchedWorkItem", _alreadyFetchedWorkItem);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "WorkItem":
					_alreadyFetchedWorkItem = true;
					this.WorkItem = (WorkItemEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "WorkItem":
					SetupSyncWorkItem(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "WorkItem":
					DesetupSyncWorkItem(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_workItem!=null)
			{
				toReturn.Add(_workItem);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemHistoryID)
		{
			return FetchUsingPK(workItemHistoryID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemHistoryID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(workItemHistoryID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(workItemHistoryID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 workItemHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(workItemHistoryID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.WorkItemHistoryID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new WorkItemHistoryRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'WorkItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'WorkItemEntity' which is related to this entity.</returns>
		public WorkItemEntity GetSingleWorkItem()
		{
			return GetSingleWorkItem(false);
		}

		/// <summary> Retrieves the related entity of type 'WorkItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'WorkItemEntity' which is related to this entity.</returns>
		public virtual WorkItemEntity GetSingleWorkItem(bool forceFetch)
		{
			if( ( !_alreadyFetchedWorkItem || forceFetch || _alwaysFetchWorkItem) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.WorkItemEntityUsingWorkItemID);
				WorkItemEntity newEntity = (WorkItemEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.WorkItemEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = WorkItemEntity.FetchPolymorphic(this.Transaction, this.WorkItemID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (WorkItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_workItemReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.WorkItem = newEntity;
				_alreadyFetchedWorkItem = fetchResult;
			}
			return _workItem;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("WorkItem", _workItem);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="validator">The validator object for this WorkItemHistoryEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 workItemHistoryID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(workItemHistoryID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_workItemReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Text", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemHistoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WorkItemID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _workItem</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWorkItem(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _workItem, new PropertyChangedEventHandler( OnWorkItemPropertyChanged ), "WorkItem", VarioSL.Entities.RelationClasses.StaticWorkItemHistoryRelations.WorkItemEntityUsingWorkItemIDStatic, true, signalRelatedEntity, "WorkItemHistory", resetFKFields, new int[] { (int)WorkItemHistoryFieldIndex.WorkItemID } );		
			_workItem = null;
		}
		
		/// <summary> setups the sync logic for member _workItem</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWorkItem(IEntityCore relatedEntity)
		{
			if(_workItem!=relatedEntity)
			{		
				DesetupSyncWorkItem(true, true);
				_workItem = (WorkItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _workItem, new PropertyChangedEventHandler( OnWorkItemPropertyChanged ), "WorkItem", VarioSL.Entities.RelationClasses.StaticWorkItemHistoryRelations.WorkItemEntityUsingWorkItemIDStatic, true, ref _alreadyFetchedWorkItem, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWorkItemPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="workItemHistoryID">PK value for WorkItemHistory which data should be fetched into this WorkItemHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 workItemHistoryID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)WorkItemHistoryFieldIndex.WorkItemHistoryID].ForcedCurrentValueWrite(workItemHistoryID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateWorkItemHistoryDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new WorkItemHistoryEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static WorkItemHistoryRelations Relations
		{
			get	{ return new WorkItemHistoryRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'WorkItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathWorkItem
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.WorkItemCollection(), (IEntityRelation)GetRelationsForField("WorkItem")[0], (int)VarioSL.Entities.EntityType.WorkItemHistoryEntity, (int)VarioSL.Entities.EntityType.WorkItemEntity, 0, null, null, null, "WorkItem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The LastModified property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)WorkItemHistoryFieldIndex.LastModified, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)WorkItemHistoryFieldIndex.LastUser, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Text property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."TEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Text
		{
			get { return (System.String)GetValue((int)WorkItemHistoryFieldIndex.Text, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.Text, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)WorkItemHistoryFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The WorkItemHistoryID property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."WORKITEMHISTORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 WorkItemHistoryID
		{
			get { return (System.Int64)GetValue((int)WorkItemHistoryFieldIndex.WorkItemHistoryID, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.WorkItemHistoryID, value, true); }
		}

		/// <summary> The WorkItemID property of the Entity WorkItemHistory<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_WORKITEMHISTORY"."WORKITEMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 WorkItemID
		{
			get { return (System.Int64)GetValue((int)WorkItemHistoryFieldIndex.WorkItemID, true); }
			set	{ SetValue((int)WorkItemHistoryFieldIndex.WorkItemID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'WorkItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleWorkItem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual WorkItemEntity WorkItem
		{
			get	{ return GetSingleWorkItem(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncWorkItem(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "WorkItemHistory", "WorkItem", _workItem, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for WorkItem. When set to true, WorkItem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time WorkItem is accessed. You can always execute a forced fetch by calling GetSingleWorkItem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchWorkItem
		{
			get	{ return _alwaysFetchWorkItem; }
			set	{ _alwaysFetchWorkItem = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property WorkItem already has been fetched. Setting this property to false when WorkItem has been fetched
		/// will set WorkItem to null as well. Setting this property to true while WorkItem hasn't been fetched disables lazy loading for WorkItem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedWorkItem
		{
			get { return _alreadyFetchedWorkItem;}
			set 
			{
				if(_alreadyFetchedWorkItem && !value)
				{
					this.WorkItem = null;
				}
				_alreadyFetchedWorkItem = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property WorkItem is not found
		/// in the database. When set to true, WorkItem will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool WorkItemReturnsNewIfNotFound
		{
			get	{ return _workItemReturnsNewIfNotFound; }
			set { _workItemReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.WorkItemHistoryEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
