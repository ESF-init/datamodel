﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BusinessRuleClause'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BusinessRuleClauseEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private BusinessRuleEntity _businessRule;
		private bool	_alwaysFetchBusinessRule, _alreadyFetchedBusinessRule, _businessRuleReturnsNewIfNotFound;
		private BusinessRuleConditionEntity _businessRuleCondition;
		private bool	_alwaysFetchBusinessRuleCondition, _alreadyFetchedBusinessRuleCondition, _businessRuleConditionReturnsNewIfNotFound;
		private BusinessRuleResultEntity _businessRuleResult;
		private bool	_alwaysFetchBusinessRuleResult, _alreadyFetchedBusinessRuleResult, _businessRuleResultReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BusinessRule</summary>
			public static readonly string BusinessRule = "BusinessRule";
			/// <summary>Member name BusinessRuleCondition</summary>
			public static readonly string BusinessRuleCondition = "BusinessRuleCondition";
			/// <summary>Member name BusinessRuleResult</summary>
			public static readonly string BusinessRuleResult = "BusinessRuleResult";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BusinessRuleClauseEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BusinessRuleClauseEntity() :base("BusinessRuleClauseEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		public BusinessRuleClauseEntity(System.Int64 businessRuleClauseID):base("BusinessRuleClauseEntity")
		{
			InitClassFetch(businessRuleClauseID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BusinessRuleClauseEntity(System.Int64 businessRuleClauseID, IPrefetchPath prefetchPathToUse):base("BusinessRuleClauseEntity")
		{
			InitClassFetch(businessRuleClauseID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="validator">The custom validator object for this BusinessRuleClauseEntity</param>
		public BusinessRuleClauseEntity(System.Int64 businessRuleClauseID, IValidator validator):base("BusinessRuleClauseEntity")
		{
			InitClassFetch(businessRuleClauseID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinessRuleClauseEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_businessRule = (BusinessRuleEntity)info.GetValue("_businessRule", typeof(BusinessRuleEntity));
			if(_businessRule!=null)
			{
				_businessRule.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleReturnsNewIfNotFound = info.GetBoolean("_businessRuleReturnsNewIfNotFound");
			_alwaysFetchBusinessRule = info.GetBoolean("_alwaysFetchBusinessRule");
			_alreadyFetchedBusinessRule = info.GetBoolean("_alreadyFetchedBusinessRule");

			_businessRuleCondition = (BusinessRuleConditionEntity)info.GetValue("_businessRuleCondition", typeof(BusinessRuleConditionEntity));
			if(_businessRuleCondition!=null)
			{
				_businessRuleCondition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleConditionReturnsNewIfNotFound = info.GetBoolean("_businessRuleConditionReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleCondition = info.GetBoolean("_alwaysFetchBusinessRuleCondition");
			_alreadyFetchedBusinessRuleCondition = info.GetBoolean("_alreadyFetchedBusinessRuleCondition");

			_businessRuleResult = (BusinessRuleResultEntity)info.GetValue("_businessRuleResult", typeof(BusinessRuleResultEntity));
			if(_businessRuleResult!=null)
			{
				_businessRuleResult.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleResultReturnsNewIfNotFound = info.GetBoolean("_businessRuleResultReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleResult = info.GetBoolean("_alwaysFetchBusinessRuleResult");
			_alreadyFetchedBusinessRuleResult = info.GetBoolean("_alreadyFetchedBusinessRuleResult");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BusinessRuleClauseFieldIndex)fieldIndex)
			{
				case BusinessRuleClauseFieldIndex.BusinessRuleConditionID:
					DesetupSyncBusinessRuleCondition(true, false);
					_alreadyFetchedBusinessRuleCondition = false;
					break;
				case BusinessRuleClauseFieldIndex.BusinessRuleID:
					DesetupSyncBusinessRule(true, false);
					_alreadyFetchedBusinessRule = false;
					break;
				case BusinessRuleClauseFieldIndex.BusinessruleResultID:
					DesetupSyncBusinessRuleResult(true, false);
					_alreadyFetchedBusinessRuleResult = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBusinessRule = (_businessRule != null);
			_alreadyFetchedBusinessRuleCondition = (_businessRuleCondition != null);
			_alreadyFetchedBusinessRuleResult = (_businessRuleResult != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BusinessRule":
					toReturn.Add(Relations.BusinessRuleEntityUsingBusinessRuleID);
					break;
				case "BusinessRuleCondition":
					toReturn.Add(Relations.BusinessRuleConditionEntityUsingBusinessRuleConditionID);
					break;
				case "BusinessRuleResult":
					toReturn.Add(Relations.BusinessRuleResultEntityUsingBusinessruleResultID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_businessRule", (!this.MarkedForDeletion?_businessRule:null));
			info.AddValue("_businessRuleReturnsNewIfNotFound", _businessRuleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRule", _alwaysFetchBusinessRule);
			info.AddValue("_alreadyFetchedBusinessRule", _alreadyFetchedBusinessRule);
			info.AddValue("_businessRuleCondition", (!this.MarkedForDeletion?_businessRuleCondition:null));
			info.AddValue("_businessRuleConditionReturnsNewIfNotFound", _businessRuleConditionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleCondition", _alwaysFetchBusinessRuleCondition);
			info.AddValue("_alreadyFetchedBusinessRuleCondition", _alreadyFetchedBusinessRuleCondition);
			info.AddValue("_businessRuleResult", (!this.MarkedForDeletion?_businessRuleResult:null));
			info.AddValue("_businessRuleResultReturnsNewIfNotFound", _businessRuleResultReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleResult", _alwaysFetchBusinessRuleResult);
			info.AddValue("_alreadyFetchedBusinessRuleResult", _alreadyFetchedBusinessRuleResult);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BusinessRule":
					_alreadyFetchedBusinessRule = true;
					this.BusinessRule = (BusinessRuleEntity)entity;
					break;
				case "BusinessRuleCondition":
					_alreadyFetchedBusinessRuleCondition = true;
					this.BusinessRuleCondition = (BusinessRuleConditionEntity)entity;
					break;
				case "BusinessRuleResult":
					_alreadyFetchedBusinessRuleResult = true;
					this.BusinessRuleResult = (BusinessRuleResultEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BusinessRule":
					SetupSyncBusinessRule(relatedEntity);
					break;
				case "BusinessRuleCondition":
					SetupSyncBusinessRuleCondition(relatedEntity);
					break;
				case "BusinessRuleResult":
					SetupSyncBusinessRuleResult(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BusinessRule":
					DesetupSyncBusinessRule(false, true);
					break;
				case "BusinessRuleCondition":
					DesetupSyncBusinessRuleCondition(false, true);
					break;
				case "BusinessRuleResult":
					DesetupSyncBusinessRuleResult(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_businessRule!=null)
			{
				toReturn.Add(_businessRule);
			}
			if(_businessRuleCondition!=null)
			{
				toReturn.Add(_businessRuleCondition);
			}
			if(_businessRuleResult!=null)
			{
				toReturn.Add(_businessRuleResult);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleClauseID)
		{
			return FetchUsingPK(businessRuleClauseID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleClauseID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(businessRuleClauseID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleClauseID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(businessRuleClauseID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleClauseID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(businessRuleClauseID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BusinessRuleClauseID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BusinessRuleClauseRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public BusinessRuleEntity GetSingleBusinessRule()
		{
			return GetSingleBusinessRule(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleEntity' which is related to this entity.</returns>
		public virtual BusinessRuleEntity GetSingleBusinessRule(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRule || forceFetch || _alwaysFetchBusinessRule) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleEntityUsingBusinessRuleID);
				BusinessRuleEntity newEntity = new BusinessRuleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessRuleID);
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRule = newEntity;
				_alreadyFetchedBusinessRule = fetchResult;
			}
			return _businessRule;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleConditionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleConditionEntity' which is related to this entity.</returns>
		public BusinessRuleConditionEntity GetSingleBusinessRuleCondition()
		{
			return GetSingleBusinessRuleCondition(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleConditionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleConditionEntity' which is related to this entity.</returns>
		public virtual BusinessRuleConditionEntity GetSingleBusinessRuleCondition(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleCondition || forceFetch || _alwaysFetchBusinessRuleCondition) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleConditionEntityUsingBusinessRuleConditionID);
				BusinessRuleConditionEntity newEntity = new BusinessRuleConditionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessRuleConditionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleConditionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleConditionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleCondition = newEntity;
				_alreadyFetchedBusinessRuleCondition = fetchResult;
			}
			return _businessRuleCondition;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleResultEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleResultEntity' which is related to this entity.</returns>
		public BusinessRuleResultEntity GetSingleBusinessRuleResult()
		{
			return GetSingleBusinessRuleResult(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleResultEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleResultEntity' which is related to this entity.</returns>
		public virtual BusinessRuleResultEntity GetSingleBusinessRuleResult(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleResult || forceFetch || _alwaysFetchBusinessRuleResult) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleResultEntityUsingBusinessruleResultID);
				BusinessRuleResultEntity newEntity = new BusinessRuleResultEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessruleResultID);
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleResultEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleResultReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleResult = newEntity;
				_alreadyFetchedBusinessRuleResult = fetchResult;
			}
			return _businessRuleResult;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BusinessRule", _businessRule);
			toReturn.Add("BusinessRuleCondition", _businessRuleCondition);
			toReturn.Add("BusinessRuleResult", _businessRuleResult);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="validator">The validator object for this BusinessRuleClauseEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 businessRuleClauseID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(businessRuleClauseID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_businessRuleReturnsNewIfNotFound = false;
			_businessRuleConditionReturnsNewIfNotFound = false;
			_businessRuleResultReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Accumulate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleClauseID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleConditionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessruleResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _businessRule</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRule(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRule, new PropertyChangedEventHandler( OnBusinessRulePropertyChanged ), "BusinessRule", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleEntityUsingBusinessRuleIDStatic, true, signalRelatedEntity, "BusinessRuleClause", resetFKFields, new int[] { (int)BusinessRuleClauseFieldIndex.BusinessRuleID } );		
			_businessRule = null;
		}
		
		/// <summary> setups the sync logic for member _businessRule</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRule(IEntityCore relatedEntity)
		{
			if(_businessRule!=relatedEntity)
			{		
				DesetupSyncBusinessRule(true, true);
				_businessRule = (BusinessRuleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRule, new PropertyChangedEventHandler( OnBusinessRulePropertyChanged ), "BusinessRule", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleEntityUsingBusinessRuleIDStatic, true, ref _alreadyFetchedBusinessRule, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRulePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleCondition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleCondition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleCondition, new PropertyChangedEventHandler( OnBusinessRuleConditionPropertyChanged ), "BusinessRuleCondition", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleConditionEntityUsingBusinessRuleConditionIDStatic, true, signalRelatedEntity, "BusinessRuleClause", resetFKFields, new int[] { (int)BusinessRuleClauseFieldIndex.BusinessRuleConditionID } );		
			_businessRuleCondition = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleCondition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleCondition(IEntityCore relatedEntity)
		{
			if(_businessRuleCondition!=relatedEntity)
			{		
				DesetupSyncBusinessRuleCondition(true, true);
				_businessRuleCondition = (BusinessRuleConditionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleCondition, new PropertyChangedEventHandler( OnBusinessRuleConditionPropertyChanged ), "BusinessRuleCondition", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleConditionEntityUsingBusinessRuleConditionIDStatic, true, ref _alreadyFetchedBusinessRuleCondition, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleConditionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleResult</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleResult(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleResult, new PropertyChangedEventHandler( OnBusinessRuleResultPropertyChanged ), "BusinessRuleResult", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleResultEntityUsingBusinessruleResultIDStatic, true, signalRelatedEntity, "BusinessRuleClause", resetFKFields, new int[] { (int)BusinessRuleClauseFieldIndex.BusinessruleResultID } );		
			_businessRuleResult = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleResult</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleResult(IEntityCore relatedEntity)
		{
			if(_businessRuleResult!=relatedEntity)
			{		
				DesetupSyncBusinessRuleResult(true, true);
				_businessRuleResult = (BusinessRuleResultEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleResult, new PropertyChangedEventHandler( OnBusinessRuleResultPropertyChanged ), "BusinessRuleResult", VarioSL.Entities.RelationClasses.StaticBusinessRuleClauseRelations.BusinessRuleResultEntityUsingBusinessruleResultIDStatic, true, ref _alreadyFetchedBusinessRuleResult, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleResultPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="businessRuleClauseID">PK value for BusinessRuleClause which data should be fetched into this BusinessRuleClause object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 businessRuleClauseID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BusinessRuleClauseFieldIndex.BusinessRuleClauseID].ForcedCurrentValueWrite(businessRuleClauseID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBusinessRuleClauseDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BusinessRuleClauseEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BusinessRuleClauseRelations Relations
		{
			get	{ return new BusinessRuleClauseRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRule
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRule")[0], (int)VarioSL.Entities.EntityType.BusinessRuleClauseEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleCondition'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleCondition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleCondition")[0], (int)VarioSL.Entities.EntityType.BusinessRuleClauseEntity, (int)VarioSL.Entities.EntityType.BusinessRuleConditionEntity, 0, null, null, null, "BusinessRuleCondition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleResult'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleResult
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleResult")[0], (int)VarioSL.Entities.EntityType.BusinessRuleClauseEntity, (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, 0, null, null, null, "BusinessRuleResult", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Accumulate property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."ACCUMULATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Accumulate
		{
			get { return (System.Boolean)GetValue((int)BusinessRuleClauseFieldIndex.Accumulate, true); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.Accumulate, value, true); }
		}

		/// <summary> The BusinessRuleClauseID property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."BUSINESSRULECLAUSEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 BusinessRuleClauseID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleClauseID, true); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleClauseID, value, true); }
		}

		/// <summary> The BusinessRuleConditionID property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."BUSINESSRULECONDITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BusinessRuleConditionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleConditionID, false); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleConditionID, value, true); }
		}

		/// <summary> The BusinessRuleID property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."BUSINESSRULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BusinessRuleID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleID, true); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.BusinessRuleID, value, true); }
		}

		/// <summary> The BusinessruleResultID property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."BUSINESSRULERESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 BusinessruleResultID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleClauseFieldIndex.BusinessruleResultID, true); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.BusinessruleResultID, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity BusinessRuleClause<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULECLAUSE"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderNumber
		{
			get { return (System.Int64)GetValue((int)BusinessRuleClauseFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)BusinessRuleClauseFieldIndex.OrderNumber, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BusinessRuleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleEntity BusinessRule
		{
			get	{ return GetSingleBusinessRule(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRule(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleClause", "BusinessRule", _businessRule, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRule. When set to true, BusinessRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRule is accessed. You can always execute a forced fetch by calling GetSingleBusinessRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRule
		{
			get	{ return _alwaysFetchBusinessRule; }
			set	{ _alwaysFetchBusinessRule = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRule already has been fetched. Setting this property to false when BusinessRule has been fetched
		/// will set BusinessRule to null as well. Setting this property to true while BusinessRule hasn't been fetched disables lazy loading for BusinessRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRule
		{
			get { return _alreadyFetchedBusinessRule;}
			set 
			{
				if(_alreadyFetchedBusinessRule && !value)
				{
					this.BusinessRule = null;
				}
				_alreadyFetchedBusinessRule = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRule is not found
		/// in the database. When set to true, BusinessRule will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleReturnsNewIfNotFound
		{
			get	{ return _businessRuleReturnsNewIfNotFound; }
			set { _businessRuleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleConditionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleCondition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleConditionEntity BusinessRuleCondition
		{
			get	{ return GetSingleBusinessRuleCondition(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleCondition(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleClause", "BusinessRuleCondition", _businessRuleCondition, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleCondition. When set to true, BusinessRuleCondition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleCondition is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleCondition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleCondition
		{
			get	{ return _alwaysFetchBusinessRuleCondition; }
			set	{ _alwaysFetchBusinessRuleCondition = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleCondition already has been fetched. Setting this property to false when BusinessRuleCondition has been fetched
		/// will set BusinessRuleCondition to null as well. Setting this property to true while BusinessRuleCondition hasn't been fetched disables lazy loading for BusinessRuleCondition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleCondition
		{
			get { return _alreadyFetchedBusinessRuleCondition;}
			set 
			{
				if(_alreadyFetchedBusinessRuleCondition && !value)
				{
					this.BusinessRuleCondition = null;
				}
				_alreadyFetchedBusinessRuleCondition = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleCondition is not found
		/// in the database. When set to true, BusinessRuleCondition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleConditionReturnsNewIfNotFound
		{
			get	{ return _businessRuleConditionReturnsNewIfNotFound; }
			set { _businessRuleConditionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleResultEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleResult()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleResultEntity BusinessRuleResult
		{
			get	{ return GetSingleBusinessRuleResult(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleResult(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleClause", "BusinessRuleResult", _businessRuleResult, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleResult. When set to true, BusinessRuleResult is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleResult is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleResult(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleResult
		{
			get	{ return _alwaysFetchBusinessRuleResult; }
			set	{ _alwaysFetchBusinessRuleResult = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleResult already has been fetched. Setting this property to false when BusinessRuleResult has been fetched
		/// will set BusinessRuleResult to null as well. Setting this property to true while BusinessRuleResult hasn't been fetched disables lazy loading for BusinessRuleResult</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleResult
		{
			get { return _alreadyFetchedBusinessRuleResult;}
			set 
			{
				if(_alreadyFetchedBusinessRuleResult && !value)
				{
					this.BusinessRuleResult = null;
				}
				_alreadyFetchedBusinessRuleResult = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleResult is not found
		/// in the database. When set to true, BusinessRuleResult will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleResultReturnsNewIfNotFound
		{
			get	{ return _businessRuleResultReturnsNewIfNotFound; }
			set { _businessRuleResultReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BusinessRuleClauseEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
