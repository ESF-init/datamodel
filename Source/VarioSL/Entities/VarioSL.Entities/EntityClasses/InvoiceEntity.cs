﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Invoice'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class InvoiceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection	_dunningToInvoices;
		private bool	_alwaysFetchDunningToInvoices, _alreadyFetchedDunningToInvoices;
		private VarioSL.Entities.CollectionClasses.InvoiceEntryCollection	_invoiceEntries;
		private bool	_alwaysFetchInvoiceEntries, _alreadyFetchedInvoiceEntries;
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.ProductTerminationCollection	_productTerminations;
		private bool	_alwaysFetchProductTerminations, _alreadyFetchedProductTerminations;
		private VarioSL.Entities.CollectionClasses.StatementOfAccountCollection	_slStatementofaccounts;
		private bool	_alwaysFetchSlStatementofaccounts, _alreadyFetchedSlStatementofaccounts;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private UserListEntity _userList;
		private bool	_alwaysFetchUserList, _alreadyFetchedUserList, _userListReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private InvoicingEntity _invoicing;
		private bool	_alwaysFetchInvoicing, _alreadyFetchedInvoicing, _invoicingReturnsNewIfNotFound;
		private PaymentOptionEntity _paymentOption;
		private bool	_alwaysFetchPaymentOption, _alreadyFetchedPaymentOption, _paymentOptionReturnsNewIfNotFound;
		private SchoolYearEntity _schoolYear;
		private bool	_alwaysFetchSchoolYear, _alreadyFetchedSchoolYear, _schoolYearReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name UserList</summary>
			public static readonly string UserList = "UserList";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Invoicing</summary>
			public static readonly string Invoicing = "Invoicing";
			/// <summary>Member name PaymentOption</summary>
			public static readonly string PaymentOption = "PaymentOption";
			/// <summary>Member name SchoolYear</summary>
			public static readonly string SchoolYear = "SchoolYear";
			/// <summary>Member name DunningToInvoices</summary>
			public static readonly string DunningToInvoices = "DunningToInvoices";
			/// <summary>Member name InvoiceEntries</summary>
			public static readonly string InvoiceEntries = "InvoiceEntries";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name ProductTerminations</summary>
			public static readonly string ProductTerminations = "ProductTerminations";
			/// <summary>Member name SlStatementofaccounts</summary>
			public static readonly string SlStatementofaccounts = "SlStatementofaccounts";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static InvoiceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public InvoiceEntity() :base("InvoiceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		public InvoiceEntity(System.Int64 invoiceID):base("InvoiceEntity")
		{
			InitClassFetch(invoiceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public InvoiceEntity(System.Int64 invoiceID, IPrefetchPath prefetchPathToUse):base("InvoiceEntity")
		{
			InitClassFetch(invoiceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="validator">The custom validator object for this InvoiceEntity</param>
		public InvoiceEntity(System.Int64 invoiceID, IValidator validator):base("InvoiceEntity")
		{
			InitClassFetch(invoiceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InvoiceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningToInvoices = (VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection)info.GetValue("_dunningToInvoices", typeof(VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection));
			_alwaysFetchDunningToInvoices = info.GetBoolean("_alwaysFetchDunningToInvoices");
			_alreadyFetchedDunningToInvoices = info.GetBoolean("_alreadyFetchedDunningToInvoices");

			_invoiceEntries = (VarioSL.Entities.CollectionClasses.InvoiceEntryCollection)info.GetValue("_invoiceEntries", typeof(VarioSL.Entities.CollectionClasses.InvoiceEntryCollection));
			_alwaysFetchInvoiceEntries = info.GetBoolean("_alwaysFetchInvoiceEntries");
			_alreadyFetchedInvoiceEntries = info.GetBoolean("_alreadyFetchedInvoiceEntries");

			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_productTerminations = (VarioSL.Entities.CollectionClasses.ProductTerminationCollection)info.GetValue("_productTerminations", typeof(VarioSL.Entities.CollectionClasses.ProductTerminationCollection));
			_alwaysFetchProductTerminations = info.GetBoolean("_alwaysFetchProductTerminations");
			_alreadyFetchedProductTerminations = info.GetBoolean("_alreadyFetchedProductTerminations");

			_slStatementofaccounts = (VarioSL.Entities.CollectionClasses.StatementOfAccountCollection)info.GetValue("_slStatementofaccounts", typeof(VarioSL.Entities.CollectionClasses.StatementOfAccountCollection));
			_alwaysFetchSlStatementofaccounts = info.GetBoolean("_alwaysFetchSlStatementofaccounts");
			_alreadyFetchedSlStatementofaccounts = info.GetBoolean("_alreadyFetchedSlStatementofaccounts");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_userList = (UserListEntity)info.GetValue("_userList", typeof(UserListEntity));
			if(_userList!=null)
			{
				_userList.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_userListReturnsNewIfNotFound = info.GetBoolean("_userListReturnsNewIfNotFound");
			_alwaysFetchUserList = info.GetBoolean("_alwaysFetchUserList");
			_alreadyFetchedUserList = info.GetBoolean("_alreadyFetchedUserList");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_invoicing = (InvoicingEntity)info.GetValue("_invoicing", typeof(InvoicingEntity));
			if(_invoicing!=null)
			{
				_invoicing.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_invoicingReturnsNewIfNotFound = info.GetBoolean("_invoicingReturnsNewIfNotFound");
			_alwaysFetchInvoicing = info.GetBoolean("_alwaysFetchInvoicing");
			_alreadyFetchedInvoicing = info.GetBoolean("_alreadyFetchedInvoicing");

			_paymentOption = (PaymentOptionEntity)info.GetValue("_paymentOption", typeof(PaymentOptionEntity));
			if(_paymentOption!=null)
			{
				_paymentOption.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentOptionReturnsNewIfNotFound = info.GetBoolean("_paymentOptionReturnsNewIfNotFound");
			_alwaysFetchPaymentOption = info.GetBoolean("_alwaysFetchPaymentOption");
			_alreadyFetchedPaymentOption = info.GetBoolean("_alreadyFetchedPaymentOption");

			_schoolYear = (SchoolYearEntity)info.GetValue("_schoolYear", typeof(SchoolYearEntity));
			if(_schoolYear!=null)
			{
				_schoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_schoolYearReturnsNewIfNotFound = info.GetBoolean("_schoolYearReturnsNewIfNotFound");
			_alwaysFetchSchoolYear = info.GetBoolean("_alwaysFetchSchoolYear");
			_alreadyFetchedSchoolYear = info.GetBoolean("_alreadyFetchedSchoolYear");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((InvoiceFieldIndex)fieldIndex)
			{
				case InvoiceFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case InvoiceFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case InvoiceFieldIndex.InvoicingID:
					DesetupSyncInvoicing(true, false);
					_alreadyFetchedInvoicing = false;
					break;
				case InvoiceFieldIndex.SchoolYearID:
					DesetupSyncSchoolYear(true, false);
					_alreadyFetchedSchoolYear = false;
					break;
				case InvoiceFieldIndex.UsedPaymentOptionID:
					DesetupSyncPaymentOption(true, false);
					_alreadyFetchedPaymentOption = false;
					break;
				case InvoiceFieldIndex.UserID:
					DesetupSyncUserList(true, false);
					_alreadyFetchedUserList = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningToInvoices = (_dunningToInvoices.Count > 0);
			_alreadyFetchedInvoiceEntries = (_invoiceEntries.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedProductTerminations = (_productTerminations.Count > 0);
			_alreadyFetchedSlStatementofaccounts = (_slStatementofaccounts.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedUserList = (_userList != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedInvoicing = (_invoicing != null);
			_alreadyFetchedPaymentOption = (_paymentOption != null);
			_alreadyFetchedSchoolYear = (_schoolYear != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "UserList":
					toReturn.Add(Relations.UserListEntityUsingUserID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Invoicing":
					toReturn.Add(Relations.InvoicingEntityUsingInvoicingID);
					break;
				case "PaymentOption":
					toReturn.Add(Relations.PaymentOptionEntityUsingUsedPaymentOptionID);
					break;
				case "SchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingSchoolYearID);
					break;
				case "DunningToInvoices":
					toReturn.Add(Relations.DunningToInvoiceEntityUsingInvoiceID);
					break;
				case "InvoiceEntries":
					toReturn.Add(Relations.InvoiceEntryEntityUsingInvoiceID);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingInvoiceID);
					break;
				case "ProductTerminations":
					toReturn.Add(Relations.ProductTerminationEntityUsingInvoiceID);
					break;
				case "SlStatementofaccounts":
					toReturn.Add(Relations.StatementOfAccountEntityUsingInvoiceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningToInvoices", (!this.MarkedForDeletion?_dunningToInvoices:null));
			info.AddValue("_alwaysFetchDunningToInvoices", _alwaysFetchDunningToInvoices);
			info.AddValue("_alreadyFetchedDunningToInvoices", _alreadyFetchedDunningToInvoices);
			info.AddValue("_invoiceEntries", (!this.MarkedForDeletion?_invoiceEntries:null));
			info.AddValue("_alwaysFetchInvoiceEntries", _alwaysFetchInvoiceEntries);
			info.AddValue("_alreadyFetchedInvoiceEntries", _alreadyFetchedInvoiceEntries);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_productTerminations", (!this.MarkedForDeletion?_productTerminations:null));
			info.AddValue("_alwaysFetchProductTerminations", _alwaysFetchProductTerminations);
			info.AddValue("_alreadyFetchedProductTerminations", _alreadyFetchedProductTerminations);
			info.AddValue("_slStatementofaccounts", (!this.MarkedForDeletion?_slStatementofaccounts:null));
			info.AddValue("_alwaysFetchSlStatementofaccounts", _alwaysFetchSlStatementofaccounts);
			info.AddValue("_alreadyFetchedSlStatementofaccounts", _alreadyFetchedSlStatementofaccounts);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_userList", (!this.MarkedForDeletion?_userList:null));
			info.AddValue("_userListReturnsNewIfNotFound", _userListReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUserList", _alwaysFetchUserList);
			info.AddValue("_alreadyFetchedUserList", _alreadyFetchedUserList);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_invoicing", (!this.MarkedForDeletion?_invoicing:null));
			info.AddValue("_invoicingReturnsNewIfNotFound", _invoicingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInvoicing", _alwaysFetchInvoicing);
			info.AddValue("_alreadyFetchedInvoicing", _alreadyFetchedInvoicing);
			info.AddValue("_paymentOption", (!this.MarkedForDeletion?_paymentOption:null));
			info.AddValue("_paymentOptionReturnsNewIfNotFound", _paymentOptionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentOption", _alwaysFetchPaymentOption);
			info.AddValue("_alreadyFetchedPaymentOption", _alreadyFetchedPaymentOption);
			info.AddValue("_schoolYear", (!this.MarkedForDeletion?_schoolYear:null));
			info.AddValue("_schoolYearReturnsNewIfNotFound", _schoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSchoolYear", _alwaysFetchSchoolYear);
			info.AddValue("_alreadyFetchedSchoolYear", _alreadyFetchedSchoolYear);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "UserList":
					_alreadyFetchedUserList = true;
					this.UserList = (UserListEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Invoicing":
					_alreadyFetchedInvoicing = true;
					this.Invoicing = (InvoicingEntity)entity;
					break;
				case "PaymentOption":
					_alreadyFetchedPaymentOption = true;
					this.PaymentOption = (PaymentOptionEntity)entity;
					break;
				case "SchoolYear":
					_alreadyFetchedSchoolYear = true;
					this.SchoolYear = (SchoolYearEntity)entity;
					break;
				case "DunningToInvoices":
					_alreadyFetchedDunningToInvoices = true;
					if(entity!=null)
					{
						this.DunningToInvoices.Add((DunningToInvoiceEntity)entity);
					}
					break;
				case "InvoiceEntries":
					_alreadyFetchedInvoiceEntries = true;
					if(entity!=null)
					{
						this.InvoiceEntries.Add((InvoiceEntryEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "ProductTerminations":
					_alreadyFetchedProductTerminations = true;
					if(entity!=null)
					{
						this.ProductTerminations.Add((ProductTerminationEntity)entity);
					}
					break;
				case "SlStatementofaccounts":
					_alreadyFetchedSlStatementofaccounts = true;
					if(entity!=null)
					{
						this.SlStatementofaccounts.Add((StatementOfAccountEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "UserList":
					SetupSyncUserList(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Invoicing":
					SetupSyncInvoicing(relatedEntity);
					break;
				case "PaymentOption":
					SetupSyncPaymentOption(relatedEntity);
					break;
				case "SchoolYear":
					SetupSyncSchoolYear(relatedEntity);
					break;
				case "DunningToInvoices":
					_dunningToInvoices.Add((DunningToInvoiceEntity)relatedEntity);
					break;
				case "InvoiceEntries":
					_invoiceEntries.Add((InvoiceEntryEntity)relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				case "ProductTerminations":
					_productTerminations.Add((ProductTerminationEntity)relatedEntity);
					break;
				case "SlStatementofaccounts":
					_slStatementofaccounts.Add((StatementOfAccountEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "UserList":
					DesetupSyncUserList(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Invoicing":
					DesetupSyncInvoicing(false, true);
					break;
				case "PaymentOption":
					DesetupSyncPaymentOption(false, true);
					break;
				case "SchoolYear":
					DesetupSyncSchoolYear(false, true);
					break;
				case "DunningToInvoices":
					this.PerformRelatedEntityRemoval(_dunningToInvoices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "InvoiceEntries":
					this.PerformRelatedEntityRemoval(_invoiceEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductTerminations":
					this.PerformRelatedEntityRemoval(_productTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SlStatementofaccounts":
					this.PerformRelatedEntityRemoval(_slStatementofaccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_userList!=null)
			{
				toReturn.Add(_userList);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_invoicing!=null)
			{
				toReturn.Add(_invoicing);
			}
			if(_paymentOption!=null)
			{
				toReturn.Add(_paymentOption);
			}
			if(_schoolYear!=null)
			{
				toReturn.Add(_schoolYear);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_dunningToInvoices);
			toReturn.Add(_invoiceEntries);
			toReturn.Add(_postings);
			toReturn.Add(_productTerminations);
			toReturn.Add(_slStatementofaccounts);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoiceID)
		{
			return FetchUsingPK(invoiceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoiceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(invoiceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(invoiceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 invoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(invoiceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.InvoiceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new InvoiceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToInvoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToInvoices(forceFetch, _dunningToInvoices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToInvoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection GetMultiDunningToInvoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToInvoices || forceFetch || _alwaysFetchDunningToInvoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToInvoices);
				_dunningToInvoices.SuppressClearInGetMulti=!forceFetch;
				_dunningToInvoices.EntityFactoryToUse = entityFactoryToUse;
				_dunningToInvoices.GetMultiManyToOne(null, null, this, filter);
				_dunningToInvoices.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToInvoices = true;
			}
			return _dunningToInvoices;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToInvoices'. These settings will be taken into account
		/// when the property DunningToInvoices is requested or GetMultiDunningToInvoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToInvoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToInvoices.SortClauses=sortClauses;
			_dunningToInvoices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceEntryCollection GetMultiInvoiceEntries(bool forceFetch)
		{
			return GetMultiInvoiceEntries(forceFetch, _invoiceEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'InvoiceEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceEntryCollection GetMultiInvoiceEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiInvoiceEntries(forceFetch, _invoiceEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.InvoiceEntryCollection GetMultiInvoiceEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiInvoiceEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'InvoiceEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceEntryCollection GetMultiInvoiceEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedInvoiceEntries || forceFetch || _alwaysFetchInvoiceEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_invoiceEntries);
				_invoiceEntries.SuppressClearInGetMulti=!forceFetch;
				_invoiceEntries.EntityFactoryToUse = entityFactoryToUse;
				_invoiceEntries.GetMultiManyToOne(this, filter);
				_invoiceEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedInvoiceEntries = true;
			}
			return _invoiceEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'InvoiceEntries'. These settings will be taken into account
		/// when the property InvoiceEntries is requested or GetMultiInvoiceEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersInvoiceEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_invoiceEntries.SortClauses=sortClauses;
			_invoiceEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductTerminations || forceFetch || _alwaysFetchProductTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productTerminations);
				_productTerminations.SuppressClearInGetMulti=!forceFetch;
				_productTerminations.EntityFactoryToUse = entityFactoryToUse;
				_productTerminations.GetMultiManyToOne(null, null, this, null, null, filter);
				_productTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedProductTerminations = true;
			}
			return _productTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductTerminations'. These settings will be taken into account
		/// when the property ProductTerminations is requested or GetMultiProductTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productTerminations.SortClauses=sortClauses;
			_productTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StatementOfAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch)
		{
			return GetMultiSlStatementofaccounts(forceFetch, _slStatementofaccounts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StatementOfAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSlStatementofaccounts(forceFetch, _slStatementofaccounts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSlStatementofaccounts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StatementOfAccountCollection GetMultiSlStatementofaccounts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSlStatementofaccounts || forceFetch || _alwaysFetchSlStatementofaccounts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_slStatementofaccounts);
				_slStatementofaccounts.SuppressClearInGetMulti=!forceFetch;
				_slStatementofaccounts.EntityFactoryToUse = entityFactoryToUse;
				_slStatementofaccounts.GetMultiManyToOne(null, this, filter);
				_slStatementofaccounts.SuppressClearInGetMulti=false;
				_alreadyFetchedSlStatementofaccounts = true;
			}
			return _slStatementofaccounts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SlStatementofaccounts'. These settings will be taken into account
		/// when the property SlStatementofaccounts is requested or GetMultiSlStatementofaccounts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSlStatementofaccounts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_slStatementofaccounts.SortClauses=sortClauses;
			_slStatementofaccounts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public UserListEntity GetSingleUserList()
		{
			return GetSingleUserList(false);
		}

		/// <summary> Retrieves the related entity of type 'UserListEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UserListEntity' which is related to this entity.</returns>
		public virtual UserListEntity GetSingleUserList(bool forceFetch)
		{
			if( ( !_alreadyFetchedUserList || forceFetch || _alwaysFetchUserList) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UserListEntityUsingUserID);
				UserListEntity newEntity = new UserListEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UserID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UserListEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_userListReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UserList = newEntity;
				_alreadyFetchedUserList = fetchResult;
			}
			return _userList;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID);
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'InvoicingEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InvoicingEntity' which is related to this entity.</returns>
		public InvoicingEntity GetSingleInvoicing()
		{
			return GetSingleInvoicing(false);
		}

		/// <summary> Retrieves the related entity of type 'InvoicingEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InvoicingEntity' which is related to this entity.</returns>
		public virtual InvoicingEntity GetSingleInvoicing(bool forceFetch)
		{
			if( ( !_alreadyFetchedInvoicing || forceFetch || _alwaysFetchInvoicing) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InvoicingEntityUsingInvoicingID);
				InvoicingEntity newEntity = new InvoicingEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InvoicingID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InvoicingEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_invoicingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Invoicing = newEntity;
				_alreadyFetchedInvoicing = fetchResult;
			}
			return _invoicing;
		}


		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public PaymentOptionEntity GetSinglePaymentOption()
		{
			return GetSinglePaymentOption(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentOptionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentOptionEntity' which is related to this entity.</returns>
		public virtual PaymentOptionEntity GetSinglePaymentOption(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentOption || forceFetch || _alwaysFetchPaymentOption) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentOptionEntityUsingUsedPaymentOptionID);
				PaymentOptionEntity newEntity = new PaymentOptionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UsedPaymentOptionID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentOptionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentOptionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentOption = newEntity;
				_alreadyFetchedPaymentOption = fetchResult;
			}
			return _paymentOption;
		}


		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSingleSchoolYear()
		{
			return GetSingleSchoolYear(false);
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSingleSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedSchoolYear || forceFetch || _alwaysFetchSchoolYear) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingSchoolYearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SchoolYearID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_schoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SchoolYear = newEntity;
				_alreadyFetchedSchoolYear = fetchResult;
			}
			return _schoolYear;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("UserList", _userList);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Invoicing", _invoicing);
			toReturn.Add("PaymentOption", _paymentOption);
			toReturn.Add("SchoolYear", _schoolYear);
			toReturn.Add("DunningToInvoices", _dunningToInvoices);
			toReturn.Add("InvoiceEntries", _invoiceEntries);
			toReturn.Add("Postings", _postings);
			toReturn.Add("ProductTerminations", _productTerminations);
			toReturn.Add("SlStatementofaccounts", _slStatementofaccounts);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="validator">The validator object for this InvoiceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 invoiceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(invoiceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_dunningToInvoices = new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection();
			_dunningToInvoices.SetContainingEntityInfo(this, "Invoice");

			_invoiceEntries = new VarioSL.Entities.CollectionClasses.InvoiceEntryCollection();
			_invoiceEntries.SetContainingEntityInfo(this, "Invoice");

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "Invoice");

			_productTerminations = new VarioSL.Entities.CollectionClasses.ProductTerminationCollection();
			_productTerminations.SetContainingEntityInfo(this, "Invoice");

			_slStatementofaccounts = new VarioSL.Entities.CollectionClasses.StatementOfAccountCollection();
			_slStatementofaccounts.SetContainingEntityInfo(this, "Invoice");
			_clientReturnsNewIfNotFound = false;
			_userListReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_invoicingReturnsNewIfNotFound = false;
			_paymentOptionReturnsNewIfNotFound = false;
			_schoolYearReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DueDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GeneratedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceEntryAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceEntryCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoiceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InvoicingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPaid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastPrintDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UsedPaymentOptionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _userList</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUserList(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.UserListEntityUsingUserIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.UserID } );		
			_userList = null;
		}
		
		/// <summary> setups the sync logic for member _userList</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUserList(IEntityCore relatedEntity)
		{
			if(_userList!=relatedEntity)
			{		
				DesetupSyncUserList(true, true);
				_userList = (UserListEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _userList, new PropertyChangedEventHandler( OnUserListPropertyChanged ), "UserList", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.UserListEntityUsingUserIDStatic, true, ref _alreadyFetchedUserList, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUserListPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _invoicing</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInvoicing(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _invoicing, new PropertyChangedEventHandler( OnInvoicingPropertyChanged ), "Invoicing", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.InvoicingEntityUsingInvoicingIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.InvoicingID } );		
			_invoicing = null;
		}
		
		/// <summary> setups the sync logic for member _invoicing</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInvoicing(IEntityCore relatedEntity)
		{
			if(_invoicing!=relatedEntity)
			{		
				DesetupSyncInvoicing(true, true);
				_invoicing = (InvoicingEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _invoicing, new PropertyChangedEventHandler( OnInvoicingPropertyChanged ), "Invoicing", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.InvoicingEntityUsingInvoicingIDStatic, true, ref _alreadyFetchedInvoicing, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInvoicingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentOption</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentOption(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.PaymentOptionEntityUsingUsedPaymentOptionIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.UsedPaymentOptionID } );		
			_paymentOption = null;
		}
		
		/// <summary> setups the sync logic for member _paymentOption</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentOption(IEntityCore relatedEntity)
		{
			if(_paymentOption!=relatedEntity)
			{		
				DesetupSyncPaymentOption(true, true);
				_paymentOption = (PaymentOptionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentOption, new PropertyChangedEventHandler( OnPaymentOptionPropertyChanged ), "PaymentOption", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.PaymentOptionEntityUsingUsedPaymentOptionIDStatic, true, ref _alreadyFetchedPaymentOption, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentOptionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _schoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, signalRelatedEntity, "Invoices", resetFKFields, new int[] { (int)InvoiceFieldIndex.SchoolYearID } );		
			_schoolYear = null;
		}
		
		/// <summary> setups the sync logic for member _schoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSchoolYear(IEntityCore relatedEntity)
		{
			if(_schoolYear!=relatedEntity)
			{		
				DesetupSyncSchoolYear(true, true);
				_schoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticInvoiceRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, ref _alreadyFetchedSchoolYear, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="invoiceID">PK value for Invoice which data should be fetched into this Invoice object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 invoiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)InvoiceFieldIndex.InvoiceID].ForcedCurrentValueWrite(invoiceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateInvoiceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new InvoiceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static InvoiceRelations Relations
		{
			get	{ return new InvoiceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToInvoice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToInvoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection(), (IEntityRelation)GetRelationsForField("DunningToInvoices")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.DunningToInvoiceEntity, 0, null, null, null, "DunningToInvoices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InvoiceEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoiceEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoiceEntryCollection(), (IEntityRelation)GetRelationsForField("InvoiceEntries")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.InvoiceEntryEntity, 0, null, null, null, "InvoiceEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductTerminationCollection(), (IEntityRelation)GetRelationsForField("ProductTerminations")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.ProductTerminationEntity, 0, null, null, null, "ProductTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StatementOfAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSlStatementofaccounts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StatementOfAccountCollection(), (IEntityRelation)GetRelationsForField("SlStatementofaccounts")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.StatementOfAccountEntity, 0, null, null, null, "SlStatementofaccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserList'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserList
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserListCollection(), (IEntityRelation)GetRelationsForField("UserList")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.UserListEntity, 0, null, null, null, "UserList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Invoicing'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInvoicing
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InvoicingCollection(), (IEntityRelation)GetRelationsForField("Invoicing")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.InvoicingEntity, 0, null, null, null, "Invoicing", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentOption'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentOption
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentOptionCollection(), (IEntityRelation)GetRelationsForField("PaymentOption")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.PaymentOptionEntity, 0, null, null, null, "PaymentOption", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("SchoolYear")[0], (int)VarioSL.Entities.EntityType.InvoiceEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "SchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoiceFieldIndex.ClientID, false); }
			set	{ SetValue((int)InvoiceFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ContractID
		{
			get { return (System.Int64)GetValue((int)InvoiceFieldIndex.ContractID, true); }
			set	{ SetValue((int)InvoiceFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreationDate
		{
			get { return (System.DateTime)GetValue((int)InvoiceFieldIndex.CreationDate, true); }
			set	{ SetValue((int)InvoiceFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The Description property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)InvoiceFieldIndex.Description, true); }
			set	{ SetValue((int)InvoiceFieldIndex.Description, value, true); }
		}

		/// <summary> The DueDate property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."DUEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DueDate
		{
			get { return (System.DateTime)GetValue((int)InvoiceFieldIndex.DueDate, true); }
			set	{ SetValue((int)InvoiceFieldIndex.DueDate, value, true); }
		}

		/// <summary> The FromDate property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."FROMDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FromDate
		{
			get { return (System.DateTime)GetValue((int)InvoiceFieldIndex.FromDate, true); }
			set	{ SetValue((int)InvoiceFieldIndex.FromDate, value, true); }
		}

		/// <summary> The GeneratedBy property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."GENERATEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 GeneratedBy
		{
			get { return (System.Int32)GetValue((int)InvoiceFieldIndex.GeneratedBy, true); }
			set	{ SetValue((int)InvoiceFieldIndex.GeneratedBy, value, true); }
		}

		/// <summary> The InvoiceEntryAmount property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICEENTRYAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InvoiceEntryAmount
		{
			get { return (System.Int64)GetValue((int)InvoiceFieldIndex.InvoiceEntryAmount, true); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoiceEntryAmount, value, true); }
		}

		/// <summary> The InvoiceEntryCount property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICEENTRYCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 InvoiceEntryCount
		{
			get { return (System.Int64)GetValue((int)InvoiceFieldIndex.InvoiceEntryCount, true); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoiceEntryCount, value, true); }
		}

		/// <summary> The InvoiceID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 InvoiceID
		{
			get { return (System.Int64)GetValue((int)InvoiceFieldIndex.InvoiceID, true); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoiceID, value, true); }
		}

		/// <summary> The InvoiceNumber property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String InvoiceNumber
		{
			get { return (System.String)GetValue((int)InvoiceFieldIndex.InvoiceNumber, true); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoiceNumber, value, true); }
		}

		/// <summary> The InvoiceType property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.InvoiceType InvoiceType
		{
			get { return (VarioSL.Entities.Enumerations.InvoiceType)GetValue((int)InvoiceFieldIndex.InvoiceType, true); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoiceType, value, true); }
		}

		/// <summary> The InvoicingID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."INVOICINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InvoicingID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoiceFieldIndex.InvoicingID, false); }
			set	{ SetValue((int)InvoiceFieldIndex.InvoicingID, value, true); }
		}

		/// <summary> The IsPaid property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."ISPAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsPaid
		{
			get { return (Nullable<System.Boolean>)GetValue((int)InvoiceFieldIndex.IsPaid, false); }
			set	{ SetValue((int)InvoiceFieldIndex.IsPaid, value, true); }
		}

		/// <summary> The LastModified property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)InvoiceFieldIndex.LastModified, true); }
			set	{ SetValue((int)InvoiceFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastPrintDate property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."LASTPRINTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastPrintDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)InvoiceFieldIndex.LastPrintDate, false); }
			set	{ SetValue((int)InvoiceFieldIndex.LastPrintDate, value, true); }
		}

		/// <summary> The LastUser property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)InvoiceFieldIndex.LastUser, true); }
			set	{ SetValue((int)InvoiceFieldIndex.LastUser, value, true); }
		}

		/// <summary> The SchoolYearID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."SCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SchoolYearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoiceFieldIndex.SchoolYearID, false); }
			set	{ SetValue((int)InvoiceFieldIndex.SchoolYearID, value, true); }
		}

		/// <summary> The ToDate property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."TODATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ToDate
		{
			get { return (System.DateTime)GetValue((int)InvoiceFieldIndex.ToDate, true); }
			set	{ SetValue((int)InvoiceFieldIndex.ToDate, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)InvoiceFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)InvoiceFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The UsedPaymentOptionID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."USEDPAYMENTOPTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UsedPaymentOptionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoiceFieldIndex.UsedPaymentOptionID, false); }
			set	{ SetValue((int)InvoiceFieldIndex.UsedPaymentOptionID, value, true); }
		}

		/// <summary> The UserID property of the Entity Invoice<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_INVOICE"."USERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UserID
		{
			get { return (Nullable<System.Int64>)GetValue((int)InvoiceFieldIndex.UserID, false); }
			set	{ SetValue((int)InvoiceFieldIndex.UserID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'DunningToInvoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToInvoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToInvoiceCollection DunningToInvoices
		{
			get	{ return GetMultiDunningToInvoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToInvoices. When set to true, DunningToInvoices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToInvoices is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToInvoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToInvoices
		{
			get	{ return _alwaysFetchDunningToInvoices; }
			set	{ _alwaysFetchDunningToInvoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToInvoices already has been fetched. Setting this property to false when DunningToInvoices has been fetched
		/// will clear the DunningToInvoices collection well. Setting this property to true while DunningToInvoices hasn't been fetched disables lazy loading for DunningToInvoices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToInvoices
		{
			get { return _alreadyFetchedDunningToInvoices;}
			set 
			{
				if(_alreadyFetchedDunningToInvoices && !value && (_dunningToInvoices != null))
				{
					_dunningToInvoices.Clear();
				}
				_alreadyFetchedDunningToInvoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'InvoiceEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiInvoiceEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.InvoiceEntryCollection InvoiceEntries
		{
			get	{ return GetMultiInvoiceEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for InvoiceEntries. When set to true, InvoiceEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InvoiceEntries is accessed. You can always execute/ a forced fetch by calling GetMultiInvoiceEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoiceEntries
		{
			get	{ return _alwaysFetchInvoiceEntries; }
			set	{ _alwaysFetchInvoiceEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property InvoiceEntries already has been fetched. Setting this property to false when InvoiceEntries has been fetched
		/// will clear the InvoiceEntries collection well. Setting this property to true while InvoiceEntries hasn't been fetched disables lazy loading for InvoiceEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoiceEntries
		{
			get { return _alreadyFetchedInvoiceEntries;}
			set 
			{
				if(_alreadyFetchedInvoiceEntries && !value && (_invoiceEntries != null))
				{
					_invoiceEntries.Clear();
				}
				_alreadyFetchedInvoiceEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection ProductTerminations
		{
			get	{ return GetMultiProductTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTerminations. When set to true, ProductTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiProductTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTerminations
		{
			get	{ return _alwaysFetchProductTerminations; }
			set	{ _alwaysFetchProductTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTerminations already has been fetched. Setting this property to false when ProductTerminations has been fetched
		/// will clear the ProductTerminations collection well. Setting this property to true while ProductTerminations hasn't been fetched disables lazy loading for ProductTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTerminations
		{
			get { return _alreadyFetchedProductTerminations;}
			set 
			{
				if(_alreadyFetchedProductTerminations && !value && (_productTerminations != null))
				{
					_productTerminations.Clear();
				}
				_alreadyFetchedProductTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StatementOfAccountEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSlStatementofaccounts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StatementOfAccountCollection SlStatementofaccounts
		{
			get	{ return GetMultiSlStatementofaccounts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SlStatementofaccounts. When set to true, SlStatementofaccounts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SlStatementofaccounts is accessed. You can always execute/ a forced fetch by calling GetMultiSlStatementofaccounts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSlStatementofaccounts
		{
			get	{ return _alwaysFetchSlStatementofaccounts; }
			set	{ _alwaysFetchSlStatementofaccounts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SlStatementofaccounts already has been fetched. Setting this property to false when SlStatementofaccounts has been fetched
		/// will clear the SlStatementofaccounts collection well. Setting this property to true while SlStatementofaccounts hasn't been fetched disables lazy loading for SlStatementofaccounts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSlStatementofaccounts
		{
			get { return _alreadyFetchedSlStatementofaccounts;}
			set 
			{
				if(_alreadyFetchedSlStatementofaccounts && !value && (_slStatementofaccounts != null))
				{
					_slStatementofaccounts.Clear();
				}
				_alreadyFetchedSlStatementofaccounts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UserListEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUserList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UserListEntity UserList
		{
			get	{ return GetSingleUserList(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUserList(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "UserList", _userList, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UserList. When set to true, UserList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserList is accessed. You can always execute a forced fetch by calling GetSingleUserList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserList
		{
			get	{ return _alwaysFetchUserList; }
			set	{ _alwaysFetchUserList = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserList already has been fetched. Setting this property to false when UserList has been fetched
		/// will set UserList to null as well. Setting this property to true while UserList hasn't been fetched disables lazy loading for UserList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserList
		{
			get { return _alreadyFetchedUserList;}
			set 
			{
				if(_alreadyFetchedUserList && !value)
				{
					this.UserList = null;
				}
				_alreadyFetchedUserList = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UserList is not found
		/// in the database. When set to true, UserList will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UserListReturnsNewIfNotFound
		{
			get	{ return _userListReturnsNewIfNotFound; }
			set { _userListReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InvoicingEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInvoicing()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InvoicingEntity Invoicing
		{
			get	{ return GetSingleInvoicing(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInvoicing(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "Invoicing", _invoicing, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Invoicing. When set to true, Invoicing is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Invoicing is accessed. You can always execute a forced fetch by calling GetSingleInvoicing(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInvoicing
		{
			get	{ return _alwaysFetchInvoicing; }
			set	{ _alwaysFetchInvoicing = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Invoicing already has been fetched. Setting this property to false when Invoicing has been fetched
		/// will set Invoicing to null as well. Setting this property to true while Invoicing hasn't been fetched disables lazy loading for Invoicing</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInvoicing
		{
			get { return _alreadyFetchedInvoicing;}
			set 
			{
				if(_alreadyFetchedInvoicing && !value)
				{
					this.Invoicing = null;
				}
				_alreadyFetchedInvoicing = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Invoicing is not found
		/// in the database. When set to true, Invoicing will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InvoicingReturnsNewIfNotFound
		{
			get	{ return _invoicingReturnsNewIfNotFound; }
			set { _invoicingReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentOptionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentOption()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentOptionEntity PaymentOption
		{
			get	{ return GetSinglePaymentOption(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentOption(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "PaymentOption", _paymentOption, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentOption. When set to true, PaymentOption is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentOption is accessed. You can always execute a forced fetch by calling GetSinglePaymentOption(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentOption
		{
			get	{ return _alwaysFetchPaymentOption; }
			set	{ _alwaysFetchPaymentOption = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentOption already has been fetched. Setting this property to false when PaymentOption has been fetched
		/// will set PaymentOption to null as well. Setting this property to true while PaymentOption hasn't been fetched disables lazy loading for PaymentOption</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentOption
		{
			get { return _alreadyFetchedPaymentOption;}
			set 
			{
				if(_alreadyFetchedPaymentOption && !value)
				{
					this.PaymentOption = null;
				}
				_alreadyFetchedPaymentOption = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentOption is not found
		/// in the database. When set to true, PaymentOption will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentOptionReturnsNewIfNotFound
		{
			get	{ return _paymentOptionReturnsNewIfNotFound; }
			set { _paymentOptionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity SchoolYear
		{
			get	{ return GetSingleSchoolYear(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSchoolYear(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Invoices", "SchoolYear", _schoolYear, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SchoolYear. When set to true, SchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SchoolYear is accessed. You can always execute a forced fetch by calling GetSingleSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSchoolYear
		{
			get	{ return _alwaysFetchSchoolYear; }
			set	{ _alwaysFetchSchoolYear = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SchoolYear already has been fetched. Setting this property to false when SchoolYear has been fetched
		/// will set SchoolYear to null as well. Setting this property to true while SchoolYear hasn't been fetched disables lazy loading for SchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSchoolYear
		{
			get { return _alreadyFetchedSchoolYear;}
			set 
			{
				if(_alreadyFetchedSchoolYear && !value)
				{
					this.SchoolYear = null;
				}
				_alreadyFetchedSchoolYear = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SchoolYear is not found
		/// in the database. When set to true, SchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SchoolYearReturnsNewIfNotFound
		{
			get	{ return _schoolYearReturnsNewIfNotFound; }
			set { _schoolYearReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.InvoiceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
