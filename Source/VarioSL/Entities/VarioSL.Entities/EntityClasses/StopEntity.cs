﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Stop'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class StopEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private NetEntity _net;
		private bool	_alwaysFetchNet, _alreadyFetchedNet, _netReturnsNewIfNotFound;
		private CoordinateEntity _coordinate;
		private bool	_alwaysFetchCoordinate, _alreadyFetchedCoordinate, _coordinateReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Net</summary>
			public static readonly string Net = "Net";
			/// <summary>Member name Coordinate</summary>
			public static readonly string Coordinate = "Coordinate";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static StopEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public StopEntity() :base("StopEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		public StopEntity(System.Int64 stopID):base("StopEntity")
		{
			InitClassFetch(stopID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public StopEntity(System.Int64 stopID, IPrefetchPath prefetchPathToUse):base("StopEntity")
		{
			InitClassFetch(stopID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="validator">The custom validator object for this StopEntity</param>
		public StopEntity(System.Int64 stopID, IValidator validator):base("StopEntity")
		{
			InitClassFetch(stopID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StopEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_net = (NetEntity)info.GetValue("_net", typeof(NetEntity));
			if(_net!=null)
			{
				_net.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_netReturnsNewIfNotFound = info.GetBoolean("_netReturnsNewIfNotFound");
			_alwaysFetchNet = info.GetBoolean("_alwaysFetchNet");
			_alreadyFetchedNet = info.GetBoolean("_alreadyFetchedNet");

			_coordinate = (CoordinateEntity)info.GetValue("_coordinate", typeof(CoordinateEntity));
			if(_coordinate!=null)
			{
				_coordinate.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_coordinateReturnsNewIfNotFound = info.GetBoolean("_coordinateReturnsNewIfNotFound");
			_alwaysFetchCoordinate = info.GetBoolean("_alwaysFetchCoordinate");
			_alreadyFetchedCoordinate = info.GetBoolean("_alreadyFetchedCoordinate");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((StopFieldIndex)fieldIndex)
			{
				case StopFieldIndex.CoordinateID:
					DesetupSyncCoordinate(true, false);
					_alreadyFetchedCoordinate = false;
					break;
				case StopFieldIndex.NetId:
					DesetupSyncNet(true, false);
					_alreadyFetchedNet = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedNet = (_net != null);
			_alreadyFetchedCoordinate = (_coordinate != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Net":
					toReturn.Add(Relations.NetEntityUsingNetId);
					break;
				case "Coordinate":
					toReturn.Add(Relations.CoordinateEntityUsingCoordinateID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_net", (!this.MarkedForDeletion?_net:null));
			info.AddValue("_netReturnsNewIfNotFound", _netReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNet", _alwaysFetchNet);
			info.AddValue("_alreadyFetchedNet", _alreadyFetchedNet);
			info.AddValue("_coordinate", (!this.MarkedForDeletion?_coordinate:null));
			info.AddValue("_coordinateReturnsNewIfNotFound", _coordinateReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCoordinate", _alwaysFetchCoordinate);
			info.AddValue("_alreadyFetchedCoordinate", _alreadyFetchedCoordinate);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Net":
					_alreadyFetchedNet = true;
					this.Net = (NetEntity)entity;
					break;
				case "Coordinate":
					_alreadyFetchedCoordinate = true;
					this.Coordinate = (CoordinateEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Net":
					SetupSyncNet(relatedEntity);
					break;
				case "Coordinate":
					SetupSyncCoordinate(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Net":
					DesetupSyncNet(false, true);
					break;
				case "Coordinate":
					DesetupSyncCoordinate(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_net!=null)
			{
				toReturn.Add(_net);
			}
			if(_coordinate!=null)
			{
				toReturn.Add(_coordinate);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stopID)
		{
			return FetchUsingPK(stopID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stopID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(stopID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stopID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(stopID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 stopID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(stopID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.StopID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new StopRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public NetEntity GetSingleNet()
		{
			return GetSingleNet(false);
		}

		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public virtual NetEntity GetSingleNet(bool forceFetch)
		{
			if( ( !_alreadyFetchedNet || forceFetch || _alwaysFetchNet) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NetEntityUsingNetId);
				NetEntity newEntity = new NetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NetId);
				}
				if(fetchResult)
				{
					newEntity = (NetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_netReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Net = newEntity;
				_alreadyFetchedNet = fetchResult;
			}
			return _net;
		}


		/// <summary> Retrieves the related entity of type 'CoordinateEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CoordinateEntity' which is related to this entity.</returns>
		public CoordinateEntity GetSingleCoordinate()
		{
			return GetSingleCoordinate(false);
		}

		/// <summary> Retrieves the related entity of type 'CoordinateEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CoordinateEntity' which is related to this entity.</returns>
		public virtual CoordinateEntity GetSingleCoordinate(bool forceFetch)
		{
			if( ( !_alreadyFetchedCoordinate || forceFetch || _alwaysFetchCoordinate) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CoordinateEntityUsingCoordinateID);
				CoordinateEntity newEntity = new CoordinateEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CoordinateID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CoordinateEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_coordinateReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Coordinate = newEntity;
				_alreadyFetchedCoordinate = fetchResult;
			}
			return _coordinate;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Net", _net);
			toReturn.Add("Coordinate", _coordinate);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="validator">The validator object for this StopEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 stopID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(stopID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_netReturnsNewIfNotFound = false;
			_coordinateReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdministrationDistrictCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdministrationDistrictName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdministrationDistrictNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CityDistrictName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CityDistrictNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CityNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CoordinateID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DepotID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FederalStateName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FederalStateNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Location", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegionCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegionName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RegionNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopLongName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopNameExternal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StopNoExtern", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffZoneNo1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffZoneNo2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffZoneNo3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffZoneNo4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketPrintName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketPrintShortName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _net</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNet(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticStopRelations.NetEntityUsingNetIdStatic, true, signalRelatedEntity, "Stops", resetFKFields, new int[] { (int)StopFieldIndex.NetId } );		
			_net = null;
		}
		
		/// <summary> setups the sync logic for member _net</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNet(IEntityCore relatedEntity)
		{
			if(_net!=relatedEntity)
			{		
				DesetupSyncNet(true, true);
				_net = (NetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticStopRelations.NetEntityUsingNetIdStatic, true, ref _alreadyFetchedNet, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _coordinate</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCoordinate(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _coordinate, new PropertyChangedEventHandler( OnCoordinatePropertyChanged ), "Coordinate", VarioSL.Entities.RelationClasses.StaticStopRelations.CoordinateEntityUsingCoordinateIDStatic, true, signalRelatedEntity, "Stops", resetFKFields, new int[] { (int)StopFieldIndex.CoordinateID } );		
			_coordinate = null;
		}
		
		/// <summary> setups the sync logic for member _coordinate</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCoordinate(IEntityCore relatedEntity)
		{
			if(_coordinate!=relatedEntity)
			{		
				DesetupSyncCoordinate(true, true);
				_coordinate = (CoordinateEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _coordinate, new PropertyChangedEventHandler( OnCoordinatePropertyChanged ), "Coordinate", VarioSL.Entities.RelationClasses.StaticStopRelations.CoordinateEntityUsingCoordinateIDStatic, true, ref _alreadyFetchedCoordinate, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCoordinatePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="stopID">PK value for Stop which data should be fetched into this Stop object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 stopID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)StopFieldIndex.StopID].ForcedCurrentValueWrite(stopID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateStopDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new StopEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static StopRelations Relations
		{
			get	{ return new StopRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Net'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNet
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NetCollection(), (IEntityRelation)GetRelationsForField("Net")[0], (int)VarioSL.Entities.EntityType.StopEntity, (int)VarioSL.Entities.EntityType.NetEntity, 0, null, null, null, "Net", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Coordinate'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCoordinate
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CoordinateCollection(), (IEntityRelation)GetRelationsForField("Coordinate")[0], (int)VarioSL.Entities.EntityType.StopEntity, (int)VarioSL.Entities.EntityType.CoordinateEntity, 0, null, null, null, "Coordinate", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdministrationDistrictCode property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."ADMINISTRATIONDISTRICTCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdministrationDistrictCode
		{
			get { return (System.String)GetValue((int)StopFieldIndex.AdministrationDistrictCode, true); }
			set	{ SetValue((int)StopFieldIndex.AdministrationDistrictCode, value, true); }
		}

		/// <summary> The AdministrationDistrictName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."ADMINISTRATIONDISTRICTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdministrationDistrictName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.AdministrationDistrictName, true); }
			set	{ SetValue((int)StopFieldIndex.AdministrationDistrictName, value, true); }
		}

		/// <summary> The AdministrationDistrictNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."ADMINISTRATIONDISTRICTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AdministrationDistrictNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.AdministrationDistrictNo, false); }
			set	{ SetValue((int)StopFieldIndex.AdministrationDistrictNo, value, true); }
		}

		/// <summary> The CityDistrictName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."CITYDISTRICTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CityDistrictName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.CityDistrictName, true); }
			set	{ SetValue((int)StopFieldIndex.CityDistrictName, value, true); }
		}

		/// <summary> The CityDistrictNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."CITYDISTRICTNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CityDistrictNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.CityDistrictNo, false); }
			set	{ SetValue((int)StopFieldIndex.CityDistrictNo, value, true); }
		}

		/// <summary> The CityName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."CITYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CityName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.CityName, true); }
			set	{ SetValue((int)StopFieldIndex.CityName, value, true); }
		}

		/// <summary> The CityNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."CITYNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CityNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.CityNo, false); }
			set	{ SetValue((int)StopFieldIndex.CityNo, value, true); }
		}

		/// <summary> The ClientID property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)StopFieldIndex.ClientID, true); }
			set	{ SetValue((int)StopFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CoordinateID property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."COORDINATEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CoordinateID
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.CoordinateID, false); }
			set	{ SetValue((int)StopFieldIndex.CoordinateID, value, true); }
		}

		/// <summary> The CountryCode property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."COUNTRYCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryCode
		{
			get { return (System.String)GetValue((int)StopFieldIndex.CountryCode, true); }
			set	{ SetValue((int)StopFieldIndex.CountryCode, value, true); }
		}

		/// <summary> The CountryName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."COUNTRYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CountryName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.CountryName, true); }
			set	{ SetValue((int)StopFieldIndex.CountryName, value, true); }
		}

		/// <summary> The CountryNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."COUNTRYNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CountryNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.CountryNo, false); }
			set	{ SetValue((int)StopFieldIndex.CountryNo, value, true); }
		}

		/// <summary> The DepotID property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."DEPOTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DepotID
		{
			get { return (System.Int64)GetValue((int)StopFieldIndex.DepotID, true); }
			set	{ SetValue((int)StopFieldIndex.DepotID, value, true); }
		}

		/// <summary> The FederalStateName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."FEDERALSTATENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FederalStateName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.FederalStateName, true); }
			set	{ SetValue((int)StopFieldIndex.FederalStateName, value, true); }
		}

		/// <summary> The FederalStateNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."FEDERALSTATENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FederalStateNo
		{
			get { return (System.String)GetValue((int)StopFieldIndex.FederalStateNo, true); }
			set	{ SetValue((int)StopFieldIndex.FederalStateNo, value, true); }
		}

		/// <summary> The Location property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."LOCATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Location
		{
			get { return (System.String)GetValue((int)StopFieldIndex.Location, true); }
			set	{ SetValue((int)StopFieldIndex.Location, value, true); }
		}

		/// <summary> The NetId property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."NETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 NetId
		{
			get { return (System.Int64)GetValue((int)StopFieldIndex.NetId, true); }
			set	{ SetValue((int)StopFieldIndex.NetId, value, true); }
		}

		/// <summary> The RegionCode property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."REGIONCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RegionCode
		{
			get { return (System.String)GetValue((int)StopFieldIndex.RegionCode, true); }
			set	{ SetValue((int)StopFieldIndex.RegionCode, value, true); }
		}

		/// <summary> The RegionName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."REGIONNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RegionName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.RegionName, true); }
			set	{ SetValue((int)StopFieldIndex.RegionName, value, true); }
		}

		/// <summary> The RegionNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."REGIONNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RegionNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.RegionNo, false); }
			set	{ SetValue((int)StopFieldIndex.RegionNo, value, true); }
		}

		/// <summary> The StopCode property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StopCode
		{
			get { return (System.String)GetValue((int)StopFieldIndex.StopCode, true); }
			set	{ SetValue((int)StopFieldIndex.StopCode, value, true); }
		}

		/// <summary> The StopID property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 StopID
		{
			get { return (System.Int64)GetValue((int)StopFieldIndex.StopID, true); }
			set	{ SetValue((int)StopFieldIndex.StopID, value, true); }
		}

		/// <summary> The StopLongName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPLONGNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StopLongName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.StopLongName, true); }
			set	{ SetValue((int)StopFieldIndex.StopLongName, value, true); }
		}

		/// <summary> The StopName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StopName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.StopName, true); }
			set	{ SetValue((int)StopFieldIndex.StopName, value, true); }
		}

		/// <summary> The StopNameExternal property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPNAMEEXTERN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StopNameExternal
		{
			get { return (System.String)GetValue((int)StopFieldIndex.StopNameExternal, true); }
			set	{ SetValue((int)StopFieldIndex.StopNameExternal, value, true); }
		}

		/// <summary> The StopNo property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 StopNo
		{
			get { return (System.Int64)GetValue((int)StopFieldIndex.StopNo, true); }
			set	{ SetValue((int)StopFieldIndex.StopNo, value, true); }
		}

		/// <summary> The StopNoExtern property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."STOPNOEXTERN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> StopNoExtern
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.StopNoExtern, false); }
			set	{ SetValue((int)StopFieldIndex.StopNoExtern, value, true); }
		}

		/// <summary> The TariffZoneNo1 property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TARIFZONENO1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffZoneNo1
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.TariffZoneNo1, false); }
			set	{ SetValue((int)StopFieldIndex.TariffZoneNo1, value, true); }
		}

		/// <summary> The TariffZoneNo2 property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TARIFZONENO2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffZoneNo2
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.TariffZoneNo2, false); }
			set	{ SetValue((int)StopFieldIndex.TariffZoneNo2, value, true); }
		}

		/// <summary> The TariffZoneNo3 property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TARIFZONENO3"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffZoneNo3
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.TariffZoneNo3, false); }
			set	{ SetValue((int)StopFieldIndex.TariffZoneNo3, value, true); }
		}

		/// <summary> The TariffZoneNo4 property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TARIFZONENO4"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffZoneNo4
		{
			get { return (Nullable<System.Int64>)GetValue((int)StopFieldIndex.TariffZoneNo4, false); }
			set	{ SetValue((int)StopFieldIndex.TariffZoneNo4, value, true); }
		}

		/// <summary> The TicketPrintName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TICKETPRINTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TicketPrintName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.TicketPrintName, true); }
			set	{ SetValue((int)StopFieldIndex.TicketPrintName, value, true); }
		}

		/// <summary> The TicketPrintShortName property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."TICKETPRINTSHORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TicketPrintShortName
		{
			get { return (System.String)GetValue((int)StopFieldIndex.TicketPrintShortName, true); }
			set	{ SetValue((int)StopFieldIndex.TicketPrintShortName, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)StopFieldIndex.ValidFrom, false); }
			set	{ SetValue((int)StopFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Stop<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_STOP"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)StopFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)StopFieldIndex.ValidUntil, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'NetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual NetEntity Net
		{
			get	{ return GetSingleNet(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNet(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Stops", "Net", _net, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Net. When set to true, Net is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Net is accessed. You can always execute a forced fetch by calling GetSingleNet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNet
		{
			get	{ return _alwaysFetchNet; }
			set	{ _alwaysFetchNet = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Net already has been fetched. Setting this property to false when Net has been fetched
		/// will set Net to null as well. Setting this property to true while Net hasn't been fetched disables lazy loading for Net</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNet
		{
			get { return _alreadyFetchedNet;}
			set 
			{
				if(_alreadyFetchedNet && !value)
				{
					this.Net = null;
				}
				_alreadyFetchedNet = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Net is not found
		/// in the database. When set to true, Net will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NetReturnsNewIfNotFound
		{
			get	{ return _netReturnsNewIfNotFound; }
			set { _netReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CoordinateEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCoordinate()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CoordinateEntity Coordinate
		{
			get	{ return GetSingleCoordinate(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCoordinate(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Stops", "Coordinate", _coordinate, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Coordinate. When set to true, Coordinate is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Coordinate is accessed. You can always execute a forced fetch by calling GetSingleCoordinate(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCoordinate
		{
			get	{ return _alwaysFetchCoordinate; }
			set	{ _alwaysFetchCoordinate = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Coordinate already has been fetched. Setting this property to false when Coordinate has been fetched
		/// will set Coordinate to null as well. Setting this property to true while Coordinate hasn't been fetched disables lazy loading for Coordinate</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCoordinate
		{
			get { return _alreadyFetchedCoordinate;}
			set 
			{
				if(_alreadyFetchedCoordinate && !value)
				{
					this.Coordinate = null;
				}
				_alreadyFetchedCoordinate = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Coordinate is not found
		/// in the database. When set to true, Coordinate will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CoordinateReturnsNewIfNotFound
		{
			get	{ return _coordinateReturnsNewIfNotFound; }
			set { _coordinateReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.StopEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
