﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderDetail'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class OrderDetailEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.JobCollection	_jobs;
		private bool	_alwaysFetchJobs, _alreadyFetchedJobs;
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection	_subsequentOrderDetails;
		private bool	_alwaysFetchSubsequentOrderDetails, _alreadyFetchedSubsequentOrderDetails;
		private VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection	_orderDetailToCardHolder;
		private bool	_alwaysFetchOrderDetailToCardHolder, _alreadyFetchedOrderDetailToCardHolder;
		private VarioSL.Entities.CollectionClasses.CardHolderCollection _cardHolders;
		private bool	_alwaysFetchCardHolders, _alreadyFetchedCardHolders;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private OrderEntity _order;
		private bool	_alwaysFetchOrder, _alreadyFetchedOrder, _orderReturnsNewIfNotFound;
		private OrderDetailEntity _requiredOrderDetail;
		private bool	_alwaysFetchRequiredOrderDetail, _alreadyFetchedRequiredOrderDetail, _requiredOrderDetailReturnsNewIfNotFound;
		private PaymentEntity _payment;
		private bool	_alwaysFetchPayment, _alreadyFetchedPayment, _paymentReturnsNewIfNotFound;
		private ProductEntity _product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct, _productReturnsNewIfNotFound;
		private SchoolYearEntity _schoolYear;
		private bool	_alwaysFetchSchoolYear, _alreadyFetchedSchoolYear, _schoolYearReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
			/// <summary>Member name RequiredOrderDetail</summary>
			public static readonly string RequiredOrderDetail = "RequiredOrderDetail";
			/// <summary>Member name Payment</summary>
			public static readonly string Payment = "Payment";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name SchoolYear</summary>
			public static readonly string SchoolYear = "SchoolYear";
			/// <summary>Member name Jobs</summary>
			public static readonly string Jobs = "Jobs";
			/// <summary>Member name SubsequentOrderDetails</summary>
			public static readonly string SubsequentOrderDetails = "SubsequentOrderDetails";
			/// <summary>Member name OrderDetailToCardHolder</summary>
			public static readonly string OrderDetailToCardHolder = "OrderDetailToCardHolder";
			/// <summary>Member name CardHolders</summary>
			public static readonly string CardHolders = "CardHolders";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderDetailEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public OrderDetailEntity() :base("OrderDetailEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		public OrderDetailEntity(System.Int64 orderDetailID):base("OrderDetailEntity")
		{
			InitClassFetch(orderDetailID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderDetailEntity(System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse):base("OrderDetailEntity")
		{
			InitClassFetch(orderDetailID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="validator">The custom validator object for this OrderDetailEntity</param>
		public OrderDetailEntity(System.Int64 orderDetailID, IValidator validator):base("OrderDetailEntity")
		{
			InitClassFetch(orderDetailID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderDetailEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_jobs = (VarioSL.Entities.CollectionClasses.JobCollection)info.GetValue("_jobs", typeof(VarioSL.Entities.CollectionClasses.JobCollection));
			_alwaysFetchJobs = info.GetBoolean("_alwaysFetchJobs");
			_alreadyFetchedJobs = info.GetBoolean("_alreadyFetchedJobs");

			_subsequentOrderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_subsequentOrderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchSubsequentOrderDetails = info.GetBoolean("_alwaysFetchSubsequentOrderDetails");
			_alreadyFetchedSubsequentOrderDetails = info.GetBoolean("_alreadyFetchedSubsequentOrderDetails");

			_orderDetailToCardHolder = (VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection)info.GetValue("_orderDetailToCardHolder", typeof(VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection));
			_alwaysFetchOrderDetailToCardHolder = info.GetBoolean("_alwaysFetchOrderDetailToCardHolder");
			_alreadyFetchedOrderDetailToCardHolder = info.GetBoolean("_alreadyFetchedOrderDetailToCardHolder");
			_cardHolders = (VarioSL.Entities.CollectionClasses.CardHolderCollection)info.GetValue("_cardHolders", typeof(VarioSL.Entities.CollectionClasses.CardHolderCollection));
			_alwaysFetchCardHolders = info.GetBoolean("_alwaysFetchCardHolders");
			_alreadyFetchedCardHolders = info.GetBoolean("_alreadyFetchedCardHolders");
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_order = (OrderEntity)info.GetValue("_order", typeof(OrderEntity));
			if(_order!=null)
			{
				_order.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderReturnsNewIfNotFound = info.GetBoolean("_orderReturnsNewIfNotFound");
			_alwaysFetchOrder = info.GetBoolean("_alwaysFetchOrder");
			_alreadyFetchedOrder = info.GetBoolean("_alreadyFetchedOrder");

			_requiredOrderDetail = (OrderDetailEntity)info.GetValue("_requiredOrderDetail", typeof(OrderDetailEntity));
			if(_requiredOrderDetail!=null)
			{
				_requiredOrderDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_requiredOrderDetailReturnsNewIfNotFound = info.GetBoolean("_requiredOrderDetailReturnsNewIfNotFound");
			_alwaysFetchRequiredOrderDetail = info.GetBoolean("_alwaysFetchRequiredOrderDetail");
			_alreadyFetchedRequiredOrderDetail = info.GetBoolean("_alreadyFetchedRequiredOrderDetail");

			_payment = (PaymentEntity)info.GetValue("_payment", typeof(PaymentEntity));
			if(_payment!=null)
			{
				_payment.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentReturnsNewIfNotFound = info.GetBoolean("_paymentReturnsNewIfNotFound");
			_alwaysFetchPayment = info.GetBoolean("_alwaysFetchPayment");
			_alreadyFetchedPayment = info.GetBoolean("_alreadyFetchedPayment");

			_product = (ProductEntity)info.GetValue("_product", typeof(ProductEntity));
			if(_product!=null)
			{
				_product.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productReturnsNewIfNotFound = info.GetBoolean("_productReturnsNewIfNotFound");
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");

			_schoolYear = (SchoolYearEntity)info.GetValue("_schoolYear", typeof(SchoolYearEntity));
			if(_schoolYear!=null)
			{
				_schoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_schoolYearReturnsNewIfNotFound = info.GetBoolean("_schoolYearReturnsNewIfNotFound");
			_alwaysFetchSchoolYear = info.GetBoolean("_alwaysFetchSchoolYear");
			_alreadyFetchedSchoolYear = info.GetBoolean("_alreadyFetchedSchoolYear");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderDetailFieldIndex)fieldIndex)
			{
				case OrderDetailFieldIndex.OrderID:
					DesetupSyncOrder(true, false);
					_alreadyFetchedOrder = false;
					break;
				case OrderDetailFieldIndex.PaymentID:
					DesetupSyncPayment(true, false);
					_alreadyFetchedPayment = false;
					break;
				case OrderDetailFieldIndex.ProductID:
					DesetupSyncProduct(true, false);
					_alreadyFetchedProduct = false;
					break;
				case OrderDetailFieldIndex.ReplacementCardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case OrderDetailFieldIndex.RequiredOrderDetailID:
					DesetupSyncRequiredOrderDetail(true, false);
					_alreadyFetchedRequiredOrderDetail = false;
					break;
				case OrderDetailFieldIndex.SchoolyearID:
					DesetupSyncSchoolYear(true, false);
					_alreadyFetchedSchoolYear = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedJobs = (_jobs.Count > 0);
			_alreadyFetchedSubsequentOrderDetails = (_subsequentOrderDetails.Count > 0);
			_alreadyFetchedOrderDetailToCardHolder = (_orderDetailToCardHolder.Count > 0);
			_alreadyFetchedCardHolders = (_cardHolders.Count > 0);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedOrder = (_order != null);
			_alreadyFetchedRequiredOrderDetail = (_requiredOrderDetail != null);
			_alreadyFetchedPayment = (_payment != null);
			_alreadyFetchedProduct = (_product != null);
			_alreadyFetchedSchoolYear = (_schoolYear != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingReplacementCardID);
					break;
				case "Order":
					toReturn.Add(Relations.OrderEntityUsingOrderID);
					break;
				case "RequiredOrderDetail":
					toReturn.Add(Relations.OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailID);
					break;
				case "Payment":
					toReturn.Add(Relations.PaymentEntityUsingPaymentID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingProductID);
					break;
				case "SchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingSchoolyearID);
					break;
				case "Jobs":
					toReturn.Add(Relations.JobEntityUsingOrderDetailID);
					break;
				case "SubsequentOrderDetails":
					toReturn.Add(Relations.OrderDetailEntityUsingRequiredOrderDetailID);
					break;
				case "OrderDetailToCardHolder":
					toReturn.Add(Relations.OrderDetailToCardHolderEntityUsingOrderDetailID);
					break;
				case "CardHolders":
					toReturn.Add(Relations.OrderDetailToCardHolderEntityUsingOrderDetailID, "OrderDetailEntity__", "OrderDetailToCardHolder_", JoinHint.None);
					toReturn.Add(OrderDetailToCardHolderEntity.Relations.CardHolderEntityUsingCardHolderID, "OrderDetailToCardHolder_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_jobs", (!this.MarkedForDeletion?_jobs:null));
			info.AddValue("_alwaysFetchJobs", _alwaysFetchJobs);
			info.AddValue("_alreadyFetchedJobs", _alreadyFetchedJobs);
			info.AddValue("_subsequentOrderDetails", (!this.MarkedForDeletion?_subsequentOrderDetails:null));
			info.AddValue("_alwaysFetchSubsequentOrderDetails", _alwaysFetchSubsequentOrderDetails);
			info.AddValue("_alreadyFetchedSubsequentOrderDetails", _alreadyFetchedSubsequentOrderDetails);
			info.AddValue("_orderDetailToCardHolder", (!this.MarkedForDeletion?_orderDetailToCardHolder:null));
			info.AddValue("_alwaysFetchOrderDetailToCardHolder", _alwaysFetchOrderDetailToCardHolder);
			info.AddValue("_alreadyFetchedOrderDetailToCardHolder", _alreadyFetchedOrderDetailToCardHolder);
			info.AddValue("_cardHolders", (!this.MarkedForDeletion?_cardHolders:null));
			info.AddValue("_alwaysFetchCardHolders", _alwaysFetchCardHolders);
			info.AddValue("_alreadyFetchedCardHolders", _alreadyFetchedCardHolders);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_order", (!this.MarkedForDeletion?_order:null));
			info.AddValue("_orderReturnsNewIfNotFound", _orderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrder", _alwaysFetchOrder);
			info.AddValue("_alreadyFetchedOrder", _alreadyFetchedOrder);
			info.AddValue("_requiredOrderDetail", (!this.MarkedForDeletion?_requiredOrderDetail:null));
			info.AddValue("_requiredOrderDetailReturnsNewIfNotFound", _requiredOrderDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRequiredOrderDetail", _alwaysFetchRequiredOrderDetail);
			info.AddValue("_alreadyFetchedRequiredOrderDetail", _alreadyFetchedRequiredOrderDetail);
			info.AddValue("_payment", (!this.MarkedForDeletion?_payment:null));
			info.AddValue("_paymentReturnsNewIfNotFound", _paymentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPayment", _alwaysFetchPayment);
			info.AddValue("_alreadyFetchedPayment", _alreadyFetchedPayment);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_productReturnsNewIfNotFound", _productReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);
			info.AddValue("_schoolYear", (!this.MarkedForDeletion?_schoolYear:null));
			info.AddValue("_schoolYearReturnsNewIfNotFound", _schoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSchoolYear", _alwaysFetchSchoolYear);
			info.AddValue("_alreadyFetchedSchoolYear", _alreadyFetchedSchoolYear);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Order":
					_alreadyFetchedOrder = true;
					this.Order = (OrderEntity)entity;
					break;
				case "RequiredOrderDetail":
					_alreadyFetchedRequiredOrderDetail = true;
					this.RequiredOrderDetail = (OrderDetailEntity)entity;
					break;
				case "Payment":
					_alreadyFetchedPayment = true;
					this.Payment = (PaymentEntity)entity;
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					this.Product = (ProductEntity)entity;
					break;
				case "SchoolYear":
					_alreadyFetchedSchoolYear = true;
					this.SchoolYear = (SchoolYearEntity)entity;
					break;
				case "Jobs":
					_alreadyFetchedJobs = true;
					if(entity!=null)
					{
						this.Jobs.Add((JobEntity)entity);
					}
					break;
				case "SubsequentOrderDetails":
					_alreadyFetchedSubsequentOrderDetails = true;
					if(entity!=null)
					{
						this.SubsequentOrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "OrderDetailToCardHolder":
					_alreadyFetchedOrderDetailToCardHolder = true;
					if(entity!=null)
					{
						this.OrderDetailToCardHolder.Add((OrderDetailToCardHolderEntity)entity);
					}
					break;
				case "CardHolders":
					_alreadyFetchedCardHolders = true;
					if(entity!=null)
					{
						this.CardHolders.Add((CardHolderEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Order":
					SetupSyncOrder(relatedEntity);
					break;
				case "RequiredOrderDetail":
					SetupSyncRequiredOrderDetail(relatedEntity);
					break;
				case "Payment":
					SetupSyncPayment(relatedEntity);
					break;
				case "Product":
					SetupSyncProduct(relatedEntity);
					break;
				case "SchoolYear":
					SetupSyncSchoolYear(relatedEntity);
					break;
				case "Jobs":
					_jobs.Add((JobEntity)relatedEntity);
					break;
				case "SubsequentOrderDetails":
					_subsequentOrderDetails.Add((OrderDetailEntity)relatedEntity);
					break;
				case "OrderDetailToCardHolder":
					_orderDetailToCardHolder.Add((OrderDetailToCardHolderEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Order":
					DesetupSyncOrder(false, true);
					break;
				case "RequiredOrderDetail":
					DesetupSyncRequiredOrderDetail(false, true);
					break;
				case "Payment":
					DesetupSyncPayment(false, true);
					break;
				case "Product":
					DesetupSyncProduct(false, true);
					break;
				case "SchoolYear":
					DesetupSyncSchoolYear(false, true);
					break;
				case "Jobs":
					this.PerformRelatedEntityRemoval(_jobs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SubsequentOrderDetails":
					this.PerformRelatedEntityRemoval(_subsequentOrderDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderDetailToCardHolder":
					this.PerformRelatedEntityRemoval(_orderDetailToCardHolder, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_order!=null)
			{
				toReturn.Add(_order);
			}
			if(_requiredOrderDetail!=null)
			{
				toReturn.Add(_requiredOrderDetail);
			}
			if(_payment!=null)
			{
				toReturn.Add(_payment);
			}
			if(_product!=null)
			{
				toReturn.Add(_product);
			}
			if(_schoolYear!=null)
			{
				toReturn.Add(_schoolYear);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_jobs);
			toReturn.Add(_subsequentOrderDetails);
			toReturn.Add(_orderDetailToCardHolder);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailID)
		{
			return FetchUsingPK(orderDetailID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderDetailID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderDetailID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderDetailID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderDetailRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobs(forceFetch, _jobs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection GetMultiJobs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobs || forceFetch || _alwaysFetchJobs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobs);
				_jobs.SuppressClearInGetMulti=!forceFetch;
				_jobs.EntityFactoryToUse = entityFactoryToUse;
				_jobs.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_jobs.SuppressClearInGetMulti=false;
				_alreadyFetchedJobs = true;
			}
			return _jobs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Jobs'. These settings will be taken into account
		/// when the property Jobs is requested or GetMultiJobs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobs.SortClauses=sortClauses;
			_jobs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiSubsequentOrderDetails(bool forceFetch)
		{
			return GetMultiSubsequentOrderDetails(forceFetch, _subsequentOrderDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiSubsequentOrderDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSubsequentOrderDetails(forceFetch, _subsequentOrderDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiSubsequentOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSubsequentOrderDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiSubsequentOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSubsequentOrderDetails || forceFetch || _alwaysFetchSubsequentOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_subsequentOrderDetails);
				_subsequentOrderDetails.SuppressClearInGetMulti=!forceFetch;
				_subsequentOrderDetails.EntityFactoryToUse = entityFactoryToUse;
				_subsequentOrderDetails.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_subsequentOrderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedSubsequentOrderDetails = true;
			}
			return _subsequentOrderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'SubsequentOrderDetails'. These settings will be taken into account
		/// when the property SubsequentOrderDetails is requested or GetMultiSubsequentOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSubsequentOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_subsequentOrderDetails.SortClauses=sortClauses;
			_subsequentOrderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, _orderDetailToCardHolder.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, _orderDetailToCardHolder.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetailToCardHolder || forceFetch || _alwaysFetchOrderDetailToCardHolder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetailToCardHolder);
				_orderDetailToCardHolder.SuppressClearInGetMulti=!forceFetch;
				_orderDetailToCardHolder.EntityFactoryToUse = entityFactoryToUse;
				_orderDetailToCardHolder.GetMultiManyToOne(null, this, filter);
				_orderDetailToCardHolder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetailToCardHolder = true;
			}
			return _orderDetailToCardHolder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetailToCardHolder'. These settings will be taken into account
		/// when the property OrderDetailToCardHolder is requested or GetMultiOrderDetailToCardHolder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetailToCardHolder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetailToCardHolder.SortClauses=sortClauses;
			_orderDetailToCardHolder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch)
		{
			return GetMultiCardHolders(forceFetch, _cardHolders.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardHolderCollection GetMultiCardHolders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCardHolders || forceFetch || _alwaysFetchCardHolders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardHolders);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(OrderDetailFields.OrderDetailID, ComparisonOperator.Equal, this.OrderDetailID, "OrderDetailEntity__"));
				_cardHolders.SuppressClearInGetMulti=!forceFetch;
				_cardHolders.EntityFactoryToUse = entityFactoryToUse;
				_cardHolders.GetMulti(filter, GetRelationsForField("CardHolders"));
				_cardHolders.SuppressClearInGetMulti=false;
				_alreadyFetchedCardHolders = true;
			}
			return _cardHolders;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardHolders'. These settings will be taken into account
		/// when the property CardHolders is requested or GetMultiCardHolders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardHolders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardHolders.SortClauses=sortClauses;
			_cardHolders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingReplacementCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReplacementCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrder()
		{
			return GetSingleOrder(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrder(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrder || forceFetch || _alwaysFetchOrder) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderID);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderID);
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Order = newEntity;
				_alreadyFetchedOrder = fetchResult;
			}
			return _order;
		}


		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public OrderDetailEntity GetSingleRequiredOrderDetail()
		{
			return GetSingleRequiredOrderDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderDetailEntity' which is related to this entity.</returns>
		public virtual OrderDetailEntity GetSingleRequiredOrderDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedRequiredOrderDetail || forceFetch || _alwaysFetchRequiredOrderDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailID);
				OrderDetailEntity newEntity = new OrderDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RequiredOrderDetailID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_requiredOrderDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RequiredOrderDetail = newEntity;
				_alreadyFetchedRequiredOrderDetail = fetchResult;
			}
			return _requiredOrderDetail;
		}


		/// <summary> Retrieves the related entity of type 'PaymentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentEntity' which is related to this entity.</returns>
		public PaymentEntity GetSinglePayment()
		{
			return GetSinglePayment(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentEntity' which is related to this entity.</returns>
		public virtual PaymentEntity GetSinglePayment(bool forceFetch)
		{
			if( ( !_alreadyFetchedPayment || forceFetch || _alwaysFetchPayment) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentEntityUsingPaymentID);
				PaymentEntity newEntity = new PaymentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Payment = newEntity;
				_alreadyFetchedPayment = fetchResult;
			}
			return _payment;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProduct()
		{
			return GetSingleProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Product = newEntity;
				_alreadyFetchedProduct = fetchResult;
			}
			return _product;
		}


		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSingleSchoolYear()
		{
			return GetSingleSchoolYear(false);
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSingleSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedSchoolYear || forceFetch || _alwaysFetchSchoolYear) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingSchoolyearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SchoolyearID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_schoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SchoolYear = newEntity;
				_alreadyFetchedSchoolYear = fetchResult;
			}
			return _schoolYear;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("Order", _order);
			toReturn.Add("RequiredOrderDetail", _requiredOrderDetail);
			toReturn.Add("Payment", _payment);
			toReturn.Add("Product", _product);
			toReturn.Add("SchoolYear", _schoolYear);
			toReturn.Add("Jobs", _jobs);
			toReturn.Add("SubsequentOrderDetails", _subsequentOrderDetails);
			toReturn.Add("OrderDetailToCardHolder", _orderDetailToCardHolder);
			toReturn.Add("CardHolders", _cardHolders);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="validator">The validator object for this OrderDetailEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 orderDetailID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderDetailID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_jobs = new VarioSL.Entities.CollectionClasses.JobCollection();
			_jobs.SetContainingEntityInfo(this, "OrderDetail");

			_subsequentOrderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_subsequentOrderDetails.SetContainingEntityInfo(this, "RequiredOrderDetail");

			_orderDetailToCardHolder = new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection();
			_orderDetailToCardHolder.SetContainingEntityInfo(this, "OrderDetail");
			_cardHolders = new VarioSL.Entities.CollectionClasses.CardHolderCollection();
			_cardReturnsNewIfNotFound = false;
			_orderReturnsNewIfNotFound = false;
			_requiredOrderDetailReturnsNewIfNotFound = false;
			_paymentReturnsNewIfNotFound = false;
			_productReturnsNewIfNotFound = false;
			_schoolYearReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardAssociationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPrintingType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FulfilledQuantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasSecondaryPrint", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCardHolderFinanced", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsProcessed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentIntervalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessingEndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProcessingStartDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Quantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReplacementCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequiredOrderDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolyearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.CardEntityUsingReplacementCardIDStatic, true, signalRelatedEntity, "OrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.ReplacementCardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.CardEntityUsingReplacementCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _order</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _order, new PropertyChangedEventHandler( OnOrderPropertyChanged ), "Order", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.OrderEntityUsingOrderIDStatic, true, signalRelatedEntity, "OrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.OrderID } );		
			_order = null;
		}
		
		/// <summary> setups the sync logic for member _order</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrder(IEntityCore relatedEntity)
		{
			if(_order!=relatedEntity)
			{		
				DesetupSyncOrder(true, true);
				_order = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _order, new PropertyChangedEventHandler( OnOrderPropertyChanged ), "Order", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.OrderEntityUsingOrderIDStatic, true, ref _alreadyFetchedOrder, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _requiredOrderDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRequiredOrderDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _requiredOrderDetail, new PropertyChangedEventHandler( OnRequiredOrderDetailPropertyChanged ), "RequiredOrderDetail", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailIDStatic, true, signalRelatedEntity, "SubsequentOrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.RequiredOrderDetailID } );		
			_requiredOrderDetail = null;
		}
		
		/// <summary> setups the sync logic for member _requiredOrderDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRequiredOrderDetail(IEntityCore relatedEntity)
		{
			if(_requiredOrderDetail!=relatedEntity)
			{		
				DesetupSyncRequiredOrderDetail(true, true);
				_requiredOrderDetail = (OrderDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _requiredOrderDetail, new PropertyChangedEventHandler( OnRequiredOrderDetailPropertyChanged ), "RequiredOrderDetail", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailIDStatic, true, ref _alreadyFetchedRequiredOrderDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRequiredOrderDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _payment</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPayment(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _payment, new PropertyChangedEventHandler( OnPaymentPropertyChanged ), "Payment", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.PaymentEntityUsingPaymentIDStatic, true, signalRelatedEntity, "OrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.PaymentID } );		
			_payment = null;
		}
		
		/// <summary> setups the sync logic for member _payment</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPayment(IEntityCore relatedEntity)
		{
			if(_payment!=relatedEntity)
			{		
				DesetupSyncPayment(true, true);
				_payment = (PaymentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _payment, new PropertyChangedEventHandler( OnPaymentPropertyChanged ), "Payment", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.PaymentEntityUsingPaymentIDStatic, true, ref _alreadyFetchedPayment, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _product</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.ProductEntityUsingProductIDStatic, true, signalRelatedEntity, "OrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.ProductID } );		
			_product = null;
		}
		
		/// <summary> setups the sync logic for member _product</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProduct(IEntityCore relatedEntity)
		{
			if(_product!=relatedEntity)
			{		
				DesetupSyncProduct(true, true);
				_product = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _product, new PropertyChangedEventHandler( OnProductPropertyChanged ), "Product", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.ProductEntityUsingProductIDStatic, true, ref _alreadyFetchedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _schoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.SchoolYearEntityUsingSchoolyearIDStatic, true, signalRelatedEntity, "OrderDetails", resetFKFields, new int[] { (int)OrderDetailFieldIndex.SchoolyearID } );		
			_schoolYear = null;
		}
		
		/// <summary> setups the sync logic for member _schoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSchoolYear(IEntityCore relatedEntity)
		{
			if(_schoolYear!=relatedEntity)
			{		
				DesetupSyncSchoolYear(true, true);
				_schoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticOrderDetailRelations.SchoolYearEntityUsingSchoolyearIDStatic, true, ref _alreadyFetchedSchoolYear, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderDetailID">PK value for OrderDetail which data should be fetched into this OrderDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 orderDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderDetailFieldIndex.OrderDetailID].ForcedCurrentValueWrite(orderDetailID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDetailDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderDetailEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderDetailRelations Relations
		{
			get	{ return new OrderDetailRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Jobs")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Jobs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSubsequentOrderDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("SubsequentOrderDetails")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "SubsequentOrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetailToCardHolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetailToCardHolder
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection(), (IEntityRelation)GetRelationsForField("OrderDetailToCardHolder")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity, 0, null, null, null, "OrderDetailToCardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolder'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolders
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderDetailToCardHolderEntityUsingOrderDetailID;
				intermediateRelation.SetAliases(string.Empty, "OrderDetailToCardHolder_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.CardHolderEntity, 0, null, null, GetRelationsForField("CardHolders"), "CardHolders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Order")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Order", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRequiredOrderDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("RequiredOrderDetail")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "RequiredOrderDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Payment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPayment
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentCollection(), (IEntityRelation)GetRelationsForField("Payment")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.PaymentEntity, 0, null, null, null, "Payment", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("SchoolYear")[0], (int)VarioSL.Entities.EntityType.OrderDetailEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "SchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CardAssociationType property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."CARDASSOCIATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CardOrderAssociationType> CardAssociationType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CardOrderAssociationType>)GetValue((int)OrderDetailFieldIndex.CardAssociationType, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.CardAssociationType, value, true); }
		}

		/// <summary> The CardPrintingType property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."CARDPRINTINGTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CardPrintingType CardPrintingType
		{
			get { return (VarioSL.Entities.Enumerations.CardPrintingType)GetValue((int)OrderDetailFieldIndex.CardPrintingType, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.CardPrintingType, value, true); }
		}

		/// <summary> The FulfilledQuantity property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."FULFILLEDQUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FulfilledQuantity
		{
			get { return (System.Int32)GetValue((int)OrderDetailFieldIndex.FulfilledQuantity, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.FulfilledQuantity, value, true); }
		}

		/// <summary> The HasSecondaryPrint property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."HASSECONDARYPRINT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> HasSecondaryPrint
		{
			get { return (Nullable<System.Boolean>)GetValue((int)OrderDetailFieldIndex.HasSecondaryPrint, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.HasSecondaryPrint, value, true); }
		}

		/// <summary> The IsCardHolderFinanced property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."ISCARDHOLDERFINANCED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCardHolderFinanced
		{
			get { return (System.Boolean)GetValue((int)OrderDetailFieldIndex.IsCardHolderFinanced, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.IsCardHolderFinanced, value, true); }
		}

		/// <summary> The IsProcessed property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."ISPROCESSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsProcessed
		{
			get { return (System.Boolean)GetValue((int)OrderDetailFieldIndex.IsProcessed, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.IsProcessed, value, true); }
		}

		/// <summary> The LastModified property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)OrderDetailFieldIndex.LastModified, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)OrderDetailFieldIndex.LastUser, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.LastUser, value, true); }
		}

		/// <summary> The OrderDetailID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."ORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 OrderDetailID
		{
			get { return (System.Int64)GetValue((int)OrderDetailFieldIndex.OrderDetailID, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.OrderDetailID, value, true); }
		}

		/// <summary> The OrderID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."ORDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OrderID
		{
			get { return (System.Int64)GetValue((int)OrderDetailFieldIndex.OrderID, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.OrderID, value, true); }
		}

		/// <summary> The PaymentID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."PAYMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailFieldIndex.PaymentID, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.PaymentID, value, true); }
		}

		/// <summary> The PaymentIntervalID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."PAYMENTINTERVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PaymentIntervalID
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderDetailFieldIndex.PaymentIntervalID, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.PaymentIntervalID, value, true); }
		}

		/// <summary> The ProcessingEndDate property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."PROCESSINGENDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProcessingEndDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderDetailFieldIndex.ProcessingEndDate, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.ProcessingEndDate, value, true); }
		}

		/// <summary> The ProcessingStartDate property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."PROCESSINGSTARTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ProcessingStartDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderDetailFieldIndex.ProcessingStartDate, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.ProcessingStartDate, value, true); }
		}

		/// <summary> The ProductID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductID
		{
			get { return (System.Int64)GetValue((int)OrderDetailFieldIndex.ProductID, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.ProductID, value, true); }
		}

		/// <summary> The Quantity property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."QUANTITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Quantity
		{
			get { return (System.Int32)GetValue((int)OrderDetailFieldIndex.Quantity, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.Quantity, value, true); }
		}

		/// <summary> The ReplacementCardID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."REPLACEMENTCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReplacementCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailFieldIndex.ReplacementCardID, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.ReplacementCardID, value, true); }
		}

		/// <summary> The RequiredOrderDetailID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."REQUIREDORDERDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RequiredOrderDetailID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailFieldIndex.RequiredOrderDetailID, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.RequiredOrderDetailID, value, true); }
		}

		/// <summary> The SchoolyearID property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."SCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SchoolyearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)OrderDetailFieldIndex.SchoolyearID, false); }
			set	{ SetValue((int)OrderDetailFieldIndex.SchoolyearID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity OrderDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ORDERDETAIL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)OrderDetailFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)OrderDetailFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'JobEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCollection Jobs
		{
			get	{ return GetMultiJobs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Jobs. When set to true, Jobs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Jobs is accessed. You can always execute/ a forced fetch by calling GetMultiJobs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobs
		{
			get	{ return _alwaysFetchJobs; }
			set	{ _alwaysFetchJobs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Jobs already has been fetched. Setting this property to false when Jobs has been fetched
		/// will clear the Jobs collection well. Setting this property to true while Jobs hasn't been fetched disables lazy loading for Jobs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobs
		{
			get { return _alreadyFetchedJobs;}
			set 
			{
				if(_alreadyFetchedJobs && !value && (_jobs != null))
				{
					_jobs.Clear();
				}
				_alreadyFetchedJobs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSubsequentOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection SubsequentOrderDetails
		{
			get	{ return GetMultiSubsequentOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SubsequentOrderDetails. When set to true, SubsequentOrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SubsequentOrderDetails is accessed. You can always execute/ a forced fetch by calling GetMultiSubsequentOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSubsequentOrderDetails
		{
			get	{ return _alwaysFetchSubsequentOrderDetails; }
			set	{ _alwaysFetchSubsequentOrderDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SubsequentOrderDetails already has been fetched. Setting this property to false when SubsequentOrderDetails has been fetched
		/// will clear the SubsequentOrderDetails collection well. Setting this property to true while SubsequentOrderDetails hasn't been fetched disables lazy loading for SubsequentOrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSubsequentOrderDetails
		{
			get { return _alreadyFetchedSubsequentOrderDetails;}
			set 
			{
				if(_alreadyFetchedSubsequentOrderDetails && !value && (_subsequentOrderDetails != null))
				{
					_subsequentOrderDetails.Clear();
				}
				_alreadyFetchedSubsequentOrderDetails = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetailToCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection OrderDetailToCardHolder
		{
			get	{ return GetMultiOrderDetailToCardHolder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetailToCardHolder. When set to true, OrderDetailToCardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetailToCardHolder is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetailToCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetailToCardHolder
		{
			get	{ return _alwaysFetchOrderDetailToCardHolder; }
			set	{ _alwaysFetchOrderDetailToCardHolder = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetailToCardHolder already has been fetched. Setting this property to false when OrderDetailToCardHolder has been fetched
		/// will clear the OrderDetailToCardHolder collection well. Setting this property to true while OrderDetailToCardHolder hasn't been fetched disables lazy loading for OrderDetailToCardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetailToCardHolder
		{
			get { return _alreadyFetchedOrderDetailToCardHolder;}
			set 
			{
				if(_alreadyFetchedOrderDetailToCardHolder && !value && (_orderDetailToCardHolder != null))
				{
					_orderDetailToCardHolder.Clear();
				}
				_alreadyFetchedOrderDetailToCardHolder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CardHolderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardHolders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardHolderCollection CardHolders
		{
			get { return GetMultiCardHolders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolders. When set to true, CardHolders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolders is accessed. You can always execute a forced fetch by calling GetMultiCardHolders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolders
		{
			get	{ return _alwaysFetchCardHolders; }
			set	{ _alwaysFetchCardHolders = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolders already has been fetched. Setting this property to false when CardHolders has been fetched
		/// will clear the CardHolders collection well. Setting this property to true while CardHolders hasn't been fetched disables lazy loading for CardHolders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolders
		{
			get { return _alreadyFetchedCardHolders;}
			set 
			{
				if(_alreadyFetchedCardHolders && !value && (_cardHolders != null))
				{
					_cardHolders.Clear();
				}
				_alreadyFetchedCardHolders = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetails", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get	{ return GetSingleOrder(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrder(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetails", "Order", _order, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Order. When set to true, Order is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Order is accessed. You can always execute a forced fetch by calling GetSingleOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrder
		{
			get	{ return _alwaysFetchOrder; }
			set	{ _alwaysFetchOrder = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Order already has been fetched. Setting this property to false when Order has been fetched
		/// will set Order to null as well. Setting this property to true while Order hasn't been fetched disables lazy loading for Order</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrder
		{
			get { return _alreadyFetchedOrder;}
			set 
			{
				if(_alreadyFetchedOrder && !value)
				{
					this.Order = null;
				}
				_alreadyFetchedOrder = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Order is not found
		/// in the database. When set to true, Order will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrderReturnsNewIfNotFound
		{
			get	{ return _orderReturnsNewIfNotFound; }
			set { _orderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRequiredOrderDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderDetailEntity RequiredOrderDetail
		{
			get	{ return GetSingleRequiredOrderDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRequiredOrderDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SubsequentOrderDetails", "RequiredOrderDetail", _requiredOrderDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RequiredOrderDetail. When set to true, RequiredOrderDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RequiredOrderDetail is accessed. You can always execute a forced fetch by calling GetSingleRequiredOrderDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRequiredOrderDetail
		{
			get	{ return _alwaysFetchRequiredOrderDetail; }
			set	{ _alwaysFetchRequiredOrderDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RequiredOrderDetail already has been fetched. Setting this property to false when RequiredOrderDetail has been fetched
		/// will set RequiredOrderDetail to null as well. Setting this property to true while RequiredOrderDetail hasn't been fetched disables lazy loading for RequiredOrderDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRequiredOrderDetail
		{
			get { return _alreadyFetchedRequiredOrderDetail;}
			set 
			{
				if(_alreadyFetchedRequiredOrderDetail && !value)
				{
					this.RequiredOrderDetail = null;
				}
				_alreadyFetchedRequiredOrderDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RequiredOrderDetail is not found
		/// in the database. When set to true, RequiredOrderDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RequiredOrderDetailReturnsNewIfNotFound
		{
			get	{ return _requiredOrderDetailReturnsNewIfNotFound; }
			set { _requiredOrderDetailReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePayment()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentEntity Payment
		{
			get	{ return GetSinglePayment(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPayment(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetails", "Payment", _payment, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Payment. When set to true, Payment is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Payment is accessed. You can always execute a forced fetch by calling GetSinglePayment(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPayment
		{
			get	{ return _alwaysFetchPayment; }
			set	{ _alwaysFetchPayment = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Payment already has been fetched. Setting this property to false when Payment has been fetched
		/// will set Payment to null as well. Setting this property to true while Payment hasn't been fetched disables lazy loading for Payment</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPayment
		{
			get { return _alreadyFetchedPayment;}
			set 
			{
				if(_alreadyFetchedPayment && !value)
				{
					this.Payment = null;
				}
				_alreadyFetchedPayment = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Payment is not found
		/// in the database. When set to true, Payment will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentReturnsNewIfNotFound
		{
			get	{ return _paymentReturnsNewIfNotFound; }
			set { _paymentReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity Product
		{
			get	{ return GetSingleProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetails", "Product", _product, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute a forced fetch by calling GetSingleProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will set Product to null as well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value)
				{
					this.Product = null;
				}
				_alreadyFetchedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Product is not found
		/// in the database. When set to true, Product will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductReturnsNewIfNotFound
		{
			get	{ return _productReturnsNewIfNotFound; }
			set { _productReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity SchoolYear
		{
			get	{ return GetSingleSchoolYear(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSchoolYear(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderDetails", "SchoolYear", _schoolYear, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SchoolYear. When set to true, SchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SchoolYear is accessed. You can always execute a forced fetch by calling GetSingleSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSchoolYear
		{
			get	{ return _alwaysFetchSchoolYear; }
			set	{ _alwaysFetchSchoolYear = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SchoolYear already has been fetched. Setting this property to false when SchoolYear has been fetched
		/// will set SchoolYear to null as well. Setting this property to true while SchoolYear hasn't been fetched disables lazy loading for SchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSchoolYear
		{
			get { return _alreadyFetchedSchoolYear;}
			set 
			{
				if(_alreadyFetchedSchoolYear && !value)
				{
					this.SchoolYear = null;
				}
				_alreadyFetchedSchoolYear = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SchoolYear is not found
		/// in the database. When set to true, SchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SchoolYearReturnsNewIfNotFound
		{
			get	{ return _schoolYearReturnsNewIfNotFound; }
			set { _schoolYearReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.OrderDetailEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
