﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementSetup'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementSetupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection	_settlementQuerySettings;
		private bool	_alwaysFetchSettlementQuerySettings, _alreadyFetchedSettlementQuerySettings;
		private VarioSL.Entities.CollectionClasses.SettlementRunCollection	_settlementRuns;
		private bool	_alwaysFetchSettlementRuns, _alreadyFetchedSettlementRuns;
		private SettlementAccountEntity _fromSettlementAccount;
		private bool	_alwaysFetchFromSettlementAccount, _alreadyFetchedFromSettlementAccount, _fromSettlementAccountReturnsNewIfNotFound;
		private SettlementAccountEntity _toSettlementAccount;
		private bool	_alwaysFetchToSettlementAccount, _alreadyFetchedToSettlementAccount, _toSettlementAccountReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FromSettlementAccount</summary>
			public static readonly string FromSettlementAccount = "FromSettlementAccount";
			/// <summary>Member name ToSettlementAccount</summary>
			public static readonly string ToSettlementAccount = "ToSettlementAccount";
			/// <summary>Member name SettlementQuerySettings</summary>
			public static readonly string SettlementQuerySettings = "SettlementQuerySettings";
			/// <summary>Member name SettlementRuns</summary>
			public static readonly string SettlementRuns = "SettlementRuns";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementSetupEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementSetupEntity() :base("SettlementSetupEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		public SettlementSetupEntity(System.Int64 settlementSetupID):base("SettlementSetupEntity")
		{
			InitClassFetch(settlementSetupID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementSetupEntity(System.Int64 settlementSetupID, IPrefetchPath prefetchPathToUse):base("SettlementSetupEntity")
		{
			InitClassFetch(settlementSetupID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="validator">The custom validator object for this SettlementSetupEntity</param>
		public SettlementSetupEntity(System.Int64 settlementSetupID, IValidator validator):base("SettlementSetupEntity")
		{
			InitClassFetch(settlementSetupID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementSetupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_settlementQuerySettings = (VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection)info.GetValue("_settlementQuerySettings", typeof(VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection));
			_alwaysFetchSettlementQuerySettings = info.GetBoolean("_alwaysFetchSettlementQuerySettings");
			_alreadyFetchedSettlementQuerySettings = info.GetBoolean("_alreadyFetchedSettlementQuerySettings");

			_settlementRuns = (VarioSL.Entities.CollectionClasses.SettlementRunCollection)info.GetValue("_settlementRuns", typeof(VarioSL.Entities.CollectionClasses.SettlementRunCollection));
			_alwaysFetchSettlementRuns = info.GetBoolean("_alwaysFetchSettlementRuns");
			_alreadyFetchedSettlementRuns = info.GetBoolean("_alreadyFetchedSettlementRuns");
			_fromSettlementAccount = (SettlementAccountEntity)info.GetValue("_fromSettlementAccount", typeof(SettlementAccountEntity));
			if(_fromSettlementAccount!=null)
			{
				_fromSettlementAccount.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fromSettlementAccountReturnsNewIfNotFound = info.GetBoolean("_fromSettlementAccountReturnsNewIfNotFound");
			_alwaysFetchFromSettlementAccount = info.GetBoolean("_alwaysFetchFromSettlementAccount");
			_alreadyFetchedFromSettlementAccount = info.GetBoolean("_alreadyFetchedFromSettlementAccount");

			_toSettlementAccount = (SettlementAccountEntity)info.GetValue("_toSettlementAccount", typeof(SettlementAccountEntity));
			if(_toSettlementAccount!=null)
			{
				_toSettlementAccount.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_toSettlementAccountReturnsNewIfNotFound = info.GetBoolean("_toSettlementAccountReturnsNewIfNotFound");
			_alwaysFetchToSettlementAccount = info.GetBoolean("_alwaysFetchToSettlementAccount");
			_alreadyFetchedToSettlementAccount = info.GetBoolean("_alreadyFetchedToSettlementAccount");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettlementSetupFieldIndex)fieldIndex)
			{
				case SettlementSetupFieldIndex.FromAccountID:
					DesetupSyncFromSettlementAccount(true, false);
					_alreadyFetchedFromSettlementAccount = false;
					break;
				case SettlementSetupFieldIndex.ToAccountID:
					DesetupSyncToSettlementAccount(true, false);
					_alreadyFetchedToSettlementAccount = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSettlementQuerySettings = (_settlementQuerySettings.Count > 0);
			_alreadyFetchedSettlementRuns = (_settlementRuns.Count > 0);
			_alreadyFetchedFromSettlementAccount = (_fromSettlementAccount != null);
			_alreadyFetchedToSettlementAccount = (_toSettlementAccount != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FromSettlementAccount":
					toReturn.Add(Relations.SettlementAccountEntityUsingFromAccountID);
					break;
				case "ToSettlementAccount":
					toReturn.Add(Relations.SettlementAccountEntityUsingToAccountID);
					break;
				case "SettlementQuerySettings":
					toReturn.Add(Relations.SettlementQuerySettingEntityUsingSettlementSetupID);
					break;
				case "SettlementRuns":
					toReturn.Add(Relations.SettlementRunEntityUsingSettlementSetupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_settlementQuerySettings", (!this.MarkedForDeletion?_settlementQuerySettings:null));
			info.AddValue("_alwaysFetchSettlementQuerySettings", _alwaysFetchSettlementQuerySettings);
			info.AddValue("_alreadyFetchedSettlementQuerySettings", _alreadyFetchedSettlementQuerySettings);
			info.AddValue("_settlementRuns", (!this.MarkedForDeletion?_settlementRuns:null));
			info.AddValue("_alwaysFetchSettlementRuns", _alwaysFetchSettlementRuns);
			info.AddValue("_alreadyFetchedSettlementRuns", _alreadyFetchedSettlementRuns);
			info.AddValue("_fromSettlementAccount", (!this.MarkedForDeletion?_fromSettlementAccount:null));
			info.AddValue("_fromSettlementAccountReturnsNewIfNotFound", _fromSettlementAccountReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFromSettlementAccount", _alwaysFetchFromSettlementAccount);
			info.AddValue("_alreadyFetchedFromSettlementAccount", _alreadyFetchedFromSettlementAccount);
			info.AddValue("_toSettlementAccount", (!this.MarkedForDeletion?_toSettlementAccount:null));
			info.AddValue("_toSettlementAccountReturnsNewIfNotFound", _toSettlementAccountReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchToSettlementAccount", _alwaysFetchToSettlementAccount);
			info.AddValue("_alreadyFetchedToSettlementAccount", _alreadyFetchedToSettlementAccount);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FromSettlementAccount":
					_alreadyFetchedFromSettlementAccount = true;
					this.FromSettlementAccount = (SettlementAccountEntity)entity;
					break;
				case "ToSettlementAccount":
					_alreadyFetchedToSettlementAccount = true;
					this.ToSettlementAccount = (SettlementAccountEntity)entity;
					break;
				case "SettlementQuerySettings":
					_alreadyFetchedSettlementQuerySettings = true;
					if(entity!=null)
					{
						this.SettlementQuerySettings.Add((SettlementQuerySettingEntity)entity);
					}
					break;
				case "SettlementRuns":
					_alreadyFetchedSettlementRuns = true;
					if(entity!=null)
					{
						this.SettlementRuns.Add((SettlementRunEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FromSettlementAccount":
					SetupSyncFromSettlementAccount(relatedEntity);
					break;
				case "ToSettlementAccount":
					SetupSyncToSettlementAccount(relatedEntity);
					break;
				case "SettlementQuerySettings":
					_settlementQuerySettings.Add((SettlementQuerySettingEntity)relatedEntity);
					break;
				case "SettlementRuns":
					_settlementRuns.Add((SettlementRunEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FromSettlementAccount":
					DesetupSyncFromSettlementAccount(false, true);
					break;
				case "ToSettlementAccount":
					DesetupSyncToSettlementAccount(false, true);
					break;
				case "SettlementQuerySettings":
					this.PerformRelatedEntityRemoval(_settlementQuerySettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementRuns":
					this.PerformRelatedEntityRemoval(_settlementRuns, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_fromSettlementAccount!=null)
			{
				toReturn.Add(_fromSettlementAccount);
			}
			if(_toSettlementAccount!=null)
			{
				toReturn.Add(_toSettlementAccount);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_settlementQuerySettings);
			toReturn.Add(_settlementRuns);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementSetupID)
		{
			return FetchUsingPK(settlementSetupID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementSetupID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementSetupID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementSetupID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementSetupID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementSetupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementSetupID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementSetupID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementSetupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch)
		{
			return GetMultiSettlementQuerySettings(forceFetch, _settlementQuerySettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQuerySettings(forceFetch, _settlementQuerySettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQuerySettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection GetMultiSettlementQuerySettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQuerySettings || forceFetch || _alwaysFetchSettlementQuerySettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQuerySettings);
				_settlementQuerySettings.SuppressClearInGetMulti=!forceFetch;
				_settlementQuerySettings.EntityFactoryToUse = entityFactoryToUse;
				_settlementQuerySettings.GetMultiManyToOne(null, this, filter);
				_settlementQuerySettings.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQuerySettings = true;
			}
			return _settlementQuerySettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQuerySettings'. These settings will be taken into account
		/// when the property SettlementQuerySettings is requested or GetMultiSettlementQuerySettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQuerySettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQuerySettings.SortClauses=sortClauses;
			_settlementQuerySettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunCollection GetMultiSettlementRuns(bool forceFetch)
		{
			return GetMultiSettlementRuns(forceFetch, _settlementRuns.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementRunEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunCollection GetMultiSettlementRuns(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementRuns(forceFetch, _settlementRuns.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementRunCollection GetMultiSettlementRuns(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementRuns(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementRunEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunCollection GetMultiSettlementRuns(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementRuns || forceFetch || _alwaysFetchSettlementRuns) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementRuns);
				_settlementRuns.SuppressClearInGetMulti=!forceFetch;
				_settlementRuns.EntityFactoryToUse = entityFactoryToUse;
				_settlementRuns.GetMultiManyToOne(this, filter);
				_settlementRuns.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementRuns = true;
			}
			return _settlementRuns;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementRuns'. These settings will be taken into account
		/// when the property SettlementRuns is requested or GetMultiSettlementRuns is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementRuns(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementRuns.SortClauses=sortClauses;
			_settlementRuns.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SettlementAccountEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementAccountEntity' which is related to this entity.</returns>
		public SettlementAccountEntity GetSingleFromSettlementAccount()
		{
			return GetSingleFromSettlementAccount(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementAccountEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementAccountEntity' which is related to this entity.</returns>
		public virtual SettlementAccountEntity GetSingleFromSettlementAccount(bool forceFetch)
		{
			if( ( !_alreadyFetchedFromSettlementAccount || forceFetch || _alwaysFetchFromSettlementAccount) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementAccountEntityUsingFromAccountID);
				SettlementAccountEntity newEntity = new SettlementAccountEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FromAccountID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SettlementAccountEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fromSettlementAccountReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FromSettlementAccount = newEntity;
				_alreadyFetchedFromSettlementAccount = fetchResult;
			}
			return _fromSettlementAccount;
		}


		/// <summary> Retrieves the related entity of type 'SettlementAccountEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementAccountEntity' which is related to this entity.</returns>
		public SettlementAccountEntity GetSingleToSettlementAccount()
		{
			return GetSingleToSettlementAccount(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementAccountEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementAccountEntity' which is related to this entity.</returns>
		public virtual SettlementAccountEntity GetSingleToSettlementAccount(bool forceFetch)
		{
			if( ( !_alreadyFetchedToSettlementAccount || forceFetch || _alwaysFetchToSettlementAccount) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementAccountEntityUsingToAccountID);
				SettlementAccountEntity newEntity = new SettlementAccountEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ToAccountID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SettlementAccountEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_toSettlementAccountReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ToSettlementAccount = newEntity;
				_alreadyFetchedToSettlementAccount = fetchResult;
			}
			return _toSettlementAccount;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FromSettlementAccount", _fromSettlementAccount);
			toReturn.Add("ToSettlementAccount", _toSettlementAccount);
			toReturn.Add("SettlementQuerySettings", _settlementQuerySettings);
			toReturn.Add("SettlementRuns", _settlementRuns);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="validator">The validator object for this SettlementSetupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementSetupID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementSetupID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_settlementQuerySettings = new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection();
			_settlementQuerySettings.SetContainingEntityInfo(this, "SettlementSetup");

			_settlementRuns = new VarioSL.Entities.CollectionClasses.SettlementRunCollection();
			_settlementRuns.SetContainingEntityInfo(this, "SettlementSetup");
			_fromSettlementAccountReturnsNewIfNotFound = false;
			_toSettlementAccountReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormulaString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FromAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScheduleRFC5545", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementSetupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ToAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _fromSettlementAccount</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFromSettlementAccount(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fromSettlementAccount, new PropertyChangedEventHandler( OnFromSettlementAccountPropertyChanged ), "FromSettlementAccount", VarioSL.Entities.RelationClasses.StaticSettlementSetupRelations.SettlementAccountEntityUsingFromAccountIDStatic, true, signalRelatedEntity, "FromSettlementSetups", resetFKFields, new int[] { (int)SettlementSetupFieldIndex.FromAccountID } );		
			_fromSettlementAccount = null;
		}
		
		/// <summary> setups the sync logic for member _fromSettlementAccount</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFromSettlementAccount(IEntityCore relatedEntity)
		{
			if(_fromSettlementAccount!=relatedEntity)
			{		
				DesetupSyncFromSettlementAccount(true, true);
				_fromSettlementAccount = (SettlementAccountEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fromSettlementAccount, new PropertyChangedEventHandler( OnFromSettlementAccountPropertyChanged ), "FromSettlementAccount", VarioSL.Entities.RelationClasses.StaticSettlementSetupRelations.SettlementAccountEntityUsingFromAccountIDStatic, true, ref _alreadyFetchedFromSettlementAccount, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFromSettlementAccountPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _toSettlementAccount</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncToSettlementAccount(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _toSettlementAccount, new PropertyChangedEventHandler( OnToSettlementAccountPropertyChanged ), "ToSettlementAccount", VarioSL.Entities.RelationClasses.StaticSettlementSetupRelations.SettlementAccountEntityUsingToAccountIDStatic, true, signalRelatedEntity, "ToSettlementSetups", resetFKFields, new int[] { (int)SettlementSetupFieldIndex.ToAccountID } );		
			_toSettlementAccount = null;
		}
		
		/// <summary> setups the sync logic for member _toSettlementAccount</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncToSettlementAccount(IEntityCore relatedEntity)
		{
			if(_toSettlementAccount!=relatedEntity)
			{		
				DesetupSyncToSettlementAccount(true, true);
				_toSettlementAccount = (SettlementAccountEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _toSettlementAccount, new PropertyChangedEventHandler( OnToSettlementAccountPropertyChanged ), "ToSettlementAccount", VarioSL.Entities.RelationClasses.StaticSettlementSetupRelations.SettlementAccountEntityUsingToAccountIDStatic, true, ref _alreadyFetchedToSettlementAccount, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnToSettlementAccountPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementSetupID">PK value for SettlementSetup which data should be fetched into this SettlementSetup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementSetupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementSetupFieldIndex.SettlementSetupID].ForcedCurrentValueWrite(settlementSetupID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementSetupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementSetupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementSetupRelations Relations
		{
			get	{ return new SettlementSetupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQuerySetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQuerySettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection(), (IEntityRelation)GetRelationsForField("SettlementQuerySettings")[0], (int)VarioSL.Entities.EntityType.SettlementSetupEntity, (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, 0, null, null, null, "SettlementQuerySettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementRun' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementRuns
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementRunCollection(), (IEntityRelation)GetRelationsForField("SettlementRuns")[0], (int)VarioSL.Entities.EntityType.SettlementSetupEntity, (int)VarioSL.Entities.EntityType.SettlementRunEntity, 0, null, null, null, "SettlementRuns", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementAccount'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFromSettlementAccount
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementAccountCollection(), (IEntityRelation)GetRelationsForField("FromSettlementAccount")[0], (int)VarioSL.Entities.EntityType.SettlementSetupEntity, (int)VarioSL.Entities.EntityType.SettlementAccountEntity, 0, null, null, null, "FromSettlementAccount", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementAccount'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathToSettlementAccount
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementAccountCollection(), (IEntityRelation)GetRelationsForField("ToSettlementAccount")[0], (int)VarioSL.Entities.EntityType.SettlementSetupEntity, (int)VarioSL.Entities.EntityType.SettlementAccountEntity, 0, null, null, null, "ToSettlementAccount", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The FormulaString property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."FORMULASTRING"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FormulaString
		{
			get { return (System.String)GetValue((int)SettlementSetupFieldIndex.FormulaString, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.FormulaString, value, true); }
		}

		/// <summary> The FromAccountID property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."FROMACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FromAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementSetupFieldIndex.FromAccountID, false); }
			set	{ SetValue((int)SettlementSetupFieldIndex.FromAccountID, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementSetupFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementSetupFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SettlementSetupFieldIndex.Name, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.Name, value, true); }
		}

		/// <summary> The ReleaseState property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."RELEASESTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SettlementReleaseState ReleaseState
		{
			get { return (VarioSL.Entities.Enumerations.SettlementReleaseState)GetValue((int)SettlementSetupFieldIndex.ReleaseState, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.ReleaseState, value, true); }
		}

		/// <summary> The ScheduleRFC5545 property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."SCHEDULERFC5545"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ScheduleRFC5545
		{
			get { return (System.String)GetValue((int)SettlementSetupFieldIndex.ScheduleRFC5545, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.ScheduleRFC5545, value, true); }
		}

		/// <summary> The SettlementSetupID property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."SETTLEMENTSETUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementSetupID
		{
			get { return (System.Int64)GetValue((int)SettlementSetupFieldIndex.SettlementSetupID, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.SettlementSetupID, value, true); }
		}

		/// <summary> The ToAccountID property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."TOACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ToAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementSetupFieldIndex.ToAccountID, false); }
			set	{ SetValue((int)SettlementSetupFieldIndex.ToAccountID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementSetupFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)SettlementSetupFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity SettlementSetup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTSETUP"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidUntil
		{
			get { return (System.DateTime)GetValue((int)SettlementSetupFieldIndex.ValidUntil, true); }
			set	{ SetValue((int)SettlementSetupFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQuerySettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingCollection SettlementQuerySettings
		{
			get	{ return GetMultiSettlementQuerySettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQuerySettings. When set to true, SettlementQuerySettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQuerySettings is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQuerySettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQuerySettings
		{
			get	{ return _alwaysFetchSettlementQuerySettings; }
			set	{ _alwaysFetchSettlementQuerySettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQuerySettings already has been fetched. Setting this property to false when SettlementQuerySettings has been fetched
		/// will clear the SettlementQuerySettings collection well. Setting this property to true while SettlementQuerySettings hasn't been fetched disables lazy loading for SettlementQuerySettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQuerySettings
		{
			get { return _alreadyFetchedSettlementQuerySettings;}
			set 
			{
				if(_alreadyFetchedSettlementQuerySettings && !value && (_settlementQuerySettings != null))
				{
					_settlementQuerySettings.Clear();
				}
				_alreadyFetchedSettlementQuerySettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementRunEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementRuns()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementRunCollection SettlementRuns
		{
			get	{ return GetMultiSettlementRuns(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementRuns. When set to true, SettlementRuns is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementRuns is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementRuns(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementRuns
		{
			get	{ return _alwaysFetchSettlementRuns; }
			set	{ _alwaysFetchSettlementRuns = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementRuns already has been fetched. Setting this property to false when SettlementRuns has been fetched
		/// will clear the SettlementRuns collection well. Setting this property to true while SettlementRuns hasn't been fetched disables lazy loading for SettlementRuns</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementRuns
		{
			get { return _alreadyFetchedSettlementRuns;}
			set 
			{
				if(_alreadyFetchedSettlementRuns && !value && (_settlementRuns != null))
				{
					_settlementRuns.Clear();
				}
				_alreadyFetchedSettlementRuns = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SettlementAccountEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFromSettlementAccount()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementAccountEntity FromSettlementAccount
		{
			get	{ return GetSingleFromSettlementAccount(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFromSettlementAccount(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FromSettlementSetups", "FromSettlementAccount", _fromSettlementAccount, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FromSettlementAccount. When set to true, FromSettlementAccount is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FromSettlementAccount is accessed. You can always execute a forced fetch by calling GetSingleFromSettlementAccount(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFromSettlementAccount
		{
			get	{ return _alwaysFetchFromSettlementAccount; }
			set	{ _alwaysFetchFromSettlementAccount = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FromSettlementAccount already has been fetched. Setting this property to false when FromSettlementAccount has been fetched
		/// will set FromSettlementAccount to null as well. Setting this property to true while FromSettlementAccount hasn't been fetched disables lazy loading for FromSettlementAccount</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFromSettlementAccount
		{
			get { return _alreadyFetchedFromSettlementAccount;}
			set 
			{
				if(_alreadyFetchedFromSettlementAccount && !value)
				{
					this.FromSettlementAccount = null;
				}
				_alreadyFetchedFromSettlementAccount = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FromSettlementAccount is not found
		/// in the database. When set to true, FromSettlementAccount will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FromSettlementAccountReturnsNewIfNotFound
		{
			get	{ return _fromSettlementAccountReturnsNewIfNotFound; }
			set { _fromSettlementAccountReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SettlementAccountEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleToSettlementAccount()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementAccountEntity ToSettlementAccount
		{
			get	{ return GetSingleToSettlementAccount(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncToSettlementAccount(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ToSettlementSetups", "ToSettlementAccount", _toSettlementAccount, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ToSettlementAccount. When set to true, ToSettlementAccount is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ToSettlementAccount is accessed. You can always execute a forced fetch by calling GetSingleToSettlementAccount(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchToSettlementAccount
		{
			get	{ return _alwaysFetchToSettlementAccount; }
			set	{ _alwaysFetchToSettlementAccount = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ToSettlementAccount already has been fetched. Setting this property to false when ToSettlementAccount has been fetched
		/// will set ToSettlementAccount to null as well. Setting this property to true while ToSettlementAccount hasn't been fetched disables lazy loading for ToSettlementAccount</summary>
		[Browsable(false)]
		public bool AlreadyFetchedToSettlementAccount
		{
			get { return _alreadyFetchedToSettlementAccount;}
			set 
			{
				if(_alreadyFetchedToSettlementAccount && !value)
				{
					this.ToSettlementAccount = null;
				}
				_alreadyFetchedToSettlementAccount = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ToSettlementAccount is not found
		/// in the database. When set to true, ToSettlementAccount will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ToSettlementAccountReturnsNewIfNotFound
		{
			get	{ return _toSettlementAccountReturnsNewIfNotFound; }
			set { _toSettlementAccountReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementSetupEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
