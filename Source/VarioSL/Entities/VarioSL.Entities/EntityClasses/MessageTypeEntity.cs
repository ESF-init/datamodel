﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MessageType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class MessageTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection	_customerAccountNotifications;
		private bool	_alwaysFetchCustomerAccountNotifications, _alreadyFetchedCustomerAccountNotifications;
		private VarioSL.Entities.CollectionClasses.EmailMessageCollection	_emailMessages;
		private bool	_alwaysFetchEmailMessages, _alreadyFetchedEmailMessages;
		private VarioSL.Entities.CollectionClasses.EventSettingCollection	_eventSettings;
		private bool	_alwaysFetchEventSettings, _alreadyFetchedEventSettings;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomerAccountNotifications</summary>
			public static readonly string CustomerAccountNotifications = "CustomerAccountNotifications";
			/// <summary>Member name EmailMessages</summary>
			public static readonly string EmailMessages = "EmailMessages";
			/// <summary>Member name EventSettings</summary>
			public static readonly string EventSettings = "EventSettings";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessageTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public MessageTypeEntity() :base("MessageTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		public MessageTypeEntity(System.Int32 enumerationValue):base("MessageTypeEntity")
		{
			InitClassFetch(enumerationValue, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MessageTypeEntity(System.Int32 enumerationValue, IPrefetchPath prefetchPathToUse):base("MessageTypeEntity")
		{
			InitClassFetch(enumerationValue, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="validator">The custom validator object for this MessageTypeEntity</param>
		public MessageTypeEntity(System.Int32 enumerationValue, IValidator validator):base("MessageTypeEntity")
		{
			InitClassFetch(enumerationValue, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customerAccountNotifications = (VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection)info.GetValue("_customerAccountNotifications", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection));
			_alwaysFetchCustomerAccountNotifications = info.GetBoolean("_alwaysFetchCustomerAccountNotifications");
			_alreadyFetchedCustomerAccountNotifications = info.GetBoolean("_alreadyFetchedCustomerAccountNotifications");

			_emailMessages = (VarioSL.Entities.CollectionClasses.EmailMessageCollection)info.GetValue("_emailMessages", typeof(VarioSL.Entities.CollectionClasses.EmailMessageCollection));
			_alwaysFetchEmailMessages = info.GetBoolean("_alwaysFetchEmailMessages");
			_alreadyFetchedEmailMessages = info.GetBoolean("_alreadyFetchedEmailMessages");

			_eventSettings = (VarioSL.Entities.CollectionClasses.EventSettingCollection)info.GetValue("_eventSettings", typeof(VarioSL.Entities.CollectionClasses.EventSettingCollection));
			_alwaysFetchEventSettings = info.GetBoolean("_alwaysFetchEventSettings");
			_alreadyFetchedEventSettings = info.GetBoolean("_alreadyFetchedEventSettings");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomerAccountNotifications = (_customerAccountNotifications.Count > 0);
			_alreadyFetchedEmailMessages = (_emailMessages.Count > 0);
			_alreadyFetchedEventSettings = (_eventSettings.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					toReturn.Add(Relations.CustomerAccountNotificationEntityUsingMessageTypeID);
					break;
				case "EmailMessages":
					toReturn.Add(Relations.EmailMessageEntityUsingMessageType);
					break;
				case "EventSettings":
					toReturn.Add(Relations.EventSettingEntityUsingMessageTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customerAccountNotifications", (!this.MarkedForDeletion?_customerAccountNotifications:null));
			info.AddValue("_alwaysFetchCustomerAccountNotifications", _alwaysFetchCustomerAccountNotifications);
			info.AddValue("_alreadyFetchedCustomerAccountNotifications", _alreadyFetchedCustomerAccountNotifications);
			info.AddValue("_emailMessages", (!this.MarkedForDeletion?_emailMessages:null));
			info.AddValue("_alwaysFetchEmailMessages", _alwaysFetchEmailMessages);
			info.AddValue("_alreadyFetchedEmailMessages", _alreadyFetchedEmailMessages);
			info.AddValue("_eventSettings", (!this.MarkedForDeletion?_eventSettings:null));
			info.AddValue("_alwaysFetchEventSettings", _alwaysFetchEventSettings);
			info.AddValue("_alreadyFetchedEventSettings", _alreadyFetchedEventSettings);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomerAccountNotifications":
					_alreadyFetchedCustomerAccountNotifications = true;
					if(entity!=null)
					{
						this.CustomerAccountNotifications.Add((CustomerAccountNotificationEntity)entity);
					}
					break;
				case "EmailMessages":
					_alreadyFetchedEmailMessages = true;
					if(entity!=null)
					{
						this.EmailMessages.Add((EmailMessageEntity)entity);
					}
					break;
				case "EventSettings":
					_alreadyFetchedEventSettings = true;
					if(entity!=null)
					{
						this.EventSettings.Add((EventSettingEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					_customerAccountNotifications.Add((CustomerAccountNotificationEntity)relatedEntity);
					break;
				case "EmailMessages":
					_emailMessages.Add((EmailMessageEntity)relatedEntity);
					break;
				case "EventSettings":
					_eventSettings.Add((EventSettingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomerAccountNotifications":
					this.PerformRelatedEntityRemoval(_customerAccountNotifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailMessages":
					this.PerformRelatedEntityRemoval(_emailMessages, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EventSettings":
					this.PerformRelatedEntityRemoval(_eventSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customerAccountNotifications);
			toReturn.Add(_emailMessages);
			toReturn.Add(_eventSettings);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 enumerationValue)
		{
			return FetchUsingPK(enumerationValue, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 enumerationValue, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(enumerationValue, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 enumerationValue, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(enumerationValue, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 enumerationValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(enumerationValue, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EnumerationValue, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessageTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, _customerAccountNotifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccountNotifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection GetMultiCustomerAccountNotifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccountNotifications || forceFetch || _alwaysFetchCustomerAccountNotifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccountNotifications);
				_customerAccountNotifications.SuppressClearInGetMulti=!forceFetch;
				_customerAccountNotifications.EntityFactoryToUse = entityFactoryToUse;
				_customerAccountNotifications.GetMultiManyToOne(null, null, this, filter);
				_customerAccountNotifications.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccountNotifications = true;
			}
			return _customerAccountNotifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccountNotifications'. These settings will be taken into account
		/// when the property CustomerAccountNotifications is requested or GetMultiCustomerAccountNotifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccountNotifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccountNotifications.SortClauses=sortClauses;
			_customerAccountNotifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailMessageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailMessages(forceFetch, _emailMessages.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailMessages(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection GetMultiEmailMessages(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailMessages || forceFetch || _alwaysFetchEmailMessages) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailMessages);
				_emailMessages.SuppressClearInGetMulti=!forceFetch;
				_emailMessages.EntityFactoryToUse = entityFactoryToUse;
				_emailMessages.GetMultiManyToOne(null, null, this, filter);
				_emailMessages.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailMessages = true;
			}
			return _emailMessages;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailMessages'. These settings will be taken into account
		/// when the property EmailMessages is requested or GetMultiEmailMessages is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailMessages(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailMessages.SortClauses=sortClauses;
			_emailMessages.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EventSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch)
		{
			return GetMultiEventSettings(forceFetch, _eventSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EventSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEventSettings(forceFetch, _eventSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEventSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EventSettingCollection GetMultiEventSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEventSettings || forceFetch || _alwaysFetchEventSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_eventSettings);
				_eventSettings.SuppressClearInGetMulti=!forceFetch;
				_eventSettings.EntityFactoryToUse = entityFactoryToUse;
				_eventSettings.GetMultiManyToOne(null, this, filter);
				_eventSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedEventSettings = true;
			}
			return _eventSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'EventSettings'. These settings will be taken into account
		/// when the property EventSettings is requested or GetMultiEventSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEventSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_eventSettings.SortClauses=sortClauses;
			_eventSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomerAccountNotifications", _customerAccountNotifications);
			toReturn.Add("EmailMessages", _emailMessages);
			toReturn.Add("EventSettings", _eventSettings);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="validator">The validator object for this MessageTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 enumerationValue, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(enumerationValue, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customerAccountNotifications = new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection();
			_customerAccountNotifications.SetContainingEntityInfo(this, "MessageType");

			_emailMessages = new VarioSL.Entities.CollectionClasses.EmailMessageCollection();
			_emailMessages.SetContainingEntityInfo(this, "EmailMessageType");

			_eventSettings = new VarioSL.Entities.CollectionClasses.EventSettingCollection();
			_eventSettings.SetContainingEntityInfo(this, "MessageType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnumerationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Literal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageFormat", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="enumerationValue">PK value for MessageType which data should be fetched into this MessageType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 enumerationValue, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessageTypeFieldIndex.EnumerationValue].ForcedCurrentValueWrite(enumerationValue);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessageTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessageTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessageTypeRelations Relations
		{
			get	{ return new MessageTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccountNotification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccountNotifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection(), (IEntityRelation)GetRelationsForField("CustomerAccountNotifications")[0], (int)VarioSL.Entities.EntityType.MessageTypeEntity, (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, 0, null, null, null, "CustomerAccountNotifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailMessages
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailMessageCollection(), (IEntityRelation)GetRelationsForField("EmailMessages")[0], (int)VarioSL.Entities.EntityType.MessageTypeEntity, (int)VarioSL.Entities.EntityType.EmailMessageEntity, 0, null, null, null, "EmailMessages", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EventSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEventSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EventSettingCollection(), (IEntityRelation)GetRelationsForField("EventSettings")[0], (int)VarioSL.Entities.EntityType.MessageTypeEntity, (int)VarioSL.Entities.EntityType.EventSettingEntity, 0, null, null, null, "EventSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity MessageType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGETYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)MessageTypeFieldIndex.Description, true); }
			set	{ SetValue((int)MessageTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The EnumerationValue property of the Entity MessageType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGETYPE"."ENUMERATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int32 EnumerationValue
		{
			get { return (System.Int32)GetValue((int)MessageTypeFieldIndex.EnumerationValue, true); }
			set	{ SetValue((int)MessageTypeFieldIndex.EnumerationValue, value, true); }
		}

		/// <summary> The Literal property of the Entity MessageType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGETYPE"."LITERAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Literal
		{
			get { return (System.String)GetValue((int)MessageTypeFieldIndex.Literal, true); }
			set	{ SetValue((int)MessageTypeFieldIndex.Literal, value, true); }
		}

		/// <summary> The MessageFormat property of the Entity MessageType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_MESSAGETYPE"."MESSAGEFORMAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.MessageFormat> MessageFormat
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.MessageFormat>)GetValue((int)MessageTypeFieldIndex.MessageFormat, false); }
			set	{ SetValue((int)MessageTypeFieldIndex.MessageFormat, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountNotificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccountNotifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountNotificationCollection CustomerAccountNotifications
		{
			get	{ return GetMultiCustomerAccountNotifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccountNotifications. When set to true, CustomerAccountNotifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccountNotifications is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccountNotifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccountNotifications
		{
			get	{ return _alwaysFetchCustomerAccountNotifications; }
			set	{ _alwaysFetchCustomerAccountNotifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccountNotifications already has been fetched. Setting this property to false when CustomerAccountNotifications has been fetched
		/// will clear the CustomerAccountNotifications collection well. Setting this property to true while CustomerAccountNotifications hasn't been fetched disables lazy loading for CustomerAccountNotifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccountNotifications
		{
			get { return _alreadyFetchedCustomerAccountNotifications;}
			set 
			{
				if(_alreadyFetchedCustomerAccountNotifications && !value && (_customerAccountNotifications != null))
				{
					_customerAccountNotifications.Clear();
				}
				_alreadyFetchedCustomerAccountNotifications = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailMessages()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailMessageCollection EmailMessages
		{
			get	{ return GetMultiEmailMessages(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailMessages. When set to true, EmailMessages is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailMessages is accessed. You can always execute/ a forced fetch by calling GetMultiEmailMessages(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailMessages
		{
			get	{ return _alwaysFetchEmailMessages; }
			set	{ _alwaysFetchEmailMessages = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailMessages already has been fetched. Setting this property to false when EmailMessages has been fetched
		/// will clear the EmailMessages collection well. Setting this property to true while EmailMessages hasn't been fetched disables lazy loading for EmailMessages</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailMessages
		{
			get { return _alreadyFetchedEmailMessages;}
			set 
			{
				if(_alreadyFetchedEmailMessages && !value && (_emailMessages != null))
				{
					_emailMessages.Clear();
				}
				_alreadyFetchedEmailMessages = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EventSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEventSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EventSettingCollection EventSettings
		{
			get	{ return GetMultiEventSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EventSettings. When set to true, EventSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EventSettings is accessed. You can always execute/ a forced fetch by calling GetMultiEventSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEventSettings
		{
			get	{ return _alwaysFetchEventSettings; }
			set	{ _alwaysFetchEventSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EventSettings already has been fetched. Setting this property to false when EventSettings has been fetched
		/// will clear the EventSettings collection well. Setting this property to true while EventSettings hasn't been fetched disables lazy loading for EventSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEventSettings
		{
			get { return _alreadyFetchedEventSettings;}
			set 
			{
				if(_alreadyFetchedEventSettings && !value && (_eventSettings != null))
				{
					_eventSettings.Clear();
				}
				_alreadyFetchedEventSettings = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.MessageTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
