﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection	_serviceIdToCards;
		private bool	_alwaysFetchServiceIdToCards, _alreadyFetchedServiceIdToCards;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection	_ticketSerialNumbers;
		private bool	_alwaysFetchTicketSerialNumbers, _alreadyFetchedTicketSerialNumbers;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ServiceIdToCards</summary>
			public static readonly string ServiceIdToCards = "ServiceIdToCards";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketSerialNumbers</summary>
			public static readonly string TicketSerialNumbers = "TicketSerialNumbers";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketTypeEntity() :base("TicketTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		public TicketTypeEntity(System.Int64 ticketTypeID):base("TicketTypeEntity")
		{
			InitClassFetch(ticketTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketTypeEntity(System.Int64 ticketTypeID, IPrefetchPath prefetchPathToUse):base("TicketTypeEntity")
		{
			InitClassFetch(ticketTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="validator">The custom validator object for this TicketTypeEntity</param>
		public TicketTypeEntity(System.Int64 ticketTypeID, IValidator validator):base("TicketTypeEntity")
		{
			InitClassFetch(ticketTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_serviceIdToCards = (VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection)info.GetValue("_serviceIdToCards", typeof(VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection));
			_alwaysFetchServiceIdToCards = info.GetBoolean("_alwaysFetchServiceIdToCards");
			_alreadyFetchedServiceIdToCards = info.GetBoolean("_alreadyFetchedServiceIdToCards");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketSerialNumbers = (VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection)info.GetValue("_ticketSerialNumbers", typeof(VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection));
			_alwaysFetchTicketSerialNumbers = info.GetBoolean("_alwaysFetchTicketSerialNumbers");
			_alreadyFetchedTicketSerialNumbers = info.GetBoolean("_alreadyFetchedTicketSerialNumbers");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedServiceIdToCards = (_serviceIdToCards.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketSerialNumbers = (_ticketSerialNumbers.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ServiceIdToCards":
					toReturn.Add(Relations.ServiceIdToCardEntityUsingTicketTypeID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingTicketTypeID);
					break;
				case "TicketSerialNumbers":
					toReturn.Add(Relations.TicketSerialNumberEntityUsingTicketTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_serviceIdToCards", (!this.MarkedForDeletion?_serviceIdToCards:null));
			info.AddValue("_alwaysFetchServiceIdToCards", _alwaysFetchServiceIdToCards);
			info.AddValue("_alreadyFetchedServiceIdToCards", _alreadyFetchedServiceIdToCards);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketSerialNumbers", (!this.MarkedForDeletion?_ticketSerialNumbers:null));
			info.AddValue("_alwaysFetchTicketSerialNumbers", _alwaysFetchTicketSerialNumbers);
			info.AddValue("_alreadyFetchedTicketSerialNumbers", _alreadyFetchedTicketSerialNumbers);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ServiceIdToCards":
					_alreadyFetchedServiceIdToCards = true;
					if(entity!=null)
					{
						this.ServiceIdToCards.Add((ServiceIdToCardEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketSerialNumbers":
					_alreadyFetchedTicketSerialNumbers = true;
					if(entity!=null)
					{
						this.TicketSerialNumbers.Add((TicketSerialNumberEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ServiceIdToCards":
					_serviceIdToCards.Add((ServiceIdToCardEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketSerialNumbers":
					_ticketSerialNumbers.Add((TicketSerialNumberEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ServiceIdToCards":
					this.PerformRelatedEntityRemoval(_serviceIdToCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketSerialNumbers":
					this.PerformRelatedEntityRemoval(_ticketSerialNumbers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_serviceIdToCards);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketSerialNumbers);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketTypeID)
		{
			return FetchUsingPK(ticketTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ServiceIdToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ServiceIdToCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection GetMultiServiceIdToCards(bool forceFetch)
		{
			return GetMultiServiceIdToCards(forceFetch, _serviceIdToCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceIdToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ServiceIdToCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection GetMultiServiceIdToCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiServiceIdToCards(forceFetch, _serviceIdToCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ServiceIdToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection GetMultiServiceIdToCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiServiceIdToCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceIdToCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection GetMultiServiceIdToCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedServiceIdToCards || forceFetch || _alwaysFetchServiceIdToCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_serviceIdToCards);
				_serviceIdToCards.SuppressClearInGetMulti=!forceFetch;
				_serviceIdToCards.EntityFactoryToUse = entityFactoryToUse;
				_serviceIdToCards.GetMultiManyToOne(this, filter);
				_serviceIdToCards.SuppressClearInGetMulti=false;
				_alreadyFetchedServiceIdToCards = true;
			}
			return _serviceIdToCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'ServiceIdToCards'. These settings will be taken into account
		/// when the property ServiceIdToCards is requested or GetMultiServiceIdToCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersServiceIdToCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_serviceIdToCards.SortClauses=sortClauses;
			_serviceIdToCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketSerialNumbers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketSerialNumbers || forceFetch || _alwaysFetchTicketSerialNumbers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketSerialNumbers);
				_ticketSerialNumbers.SuppressClearInGetMulti=!forceFetch;
				_ticketSerialNumbers.EntityFactoryToUse = entityFactoryToUse;
				_ticketSerialNumbers.GetMultiManyToOne(null, this, null, filter);
				_ticketSerialNumbers.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketSerialNumbers = true;
			}
			return _ticketSerialNumbers;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketSerialNumbers'. These settings will be taken into account
		/// when the property TicketSerialNumbers is requested or GetMultiTicketSerialNumbers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketSerialNumbers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketSerialNumbers.SortClauses=sortClauses;
			_ticketSerialNumbers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ServiceIdToCards", _serviceIdToCards);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketSerialNumbers", _ticketSerialNumbers);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="validator">The validator object for this TicketTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_serviceIdToCards = new VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection();
			_serviceIdToCards.SetContainingEntityInfo(this, "TicketType");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "TicketType");

			_ticketSerialNumbers = new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection();
			_ticketSerialNumbers.SetContainingEntityInfo(this, "TicketType");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EnumerationValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendBalancing", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendPayMask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EvendTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Literal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTypeID", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketTypeID">PK value for TicketType which data should be fetched into this TicketType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketTypeFieldIndex.TicketTypeID].ForcedCurrentValueWrite(ticketTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketTypeRelations Relations
		{
			get	{ return new TicketTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceIdToCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceIdToCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection(), (IEntityRelation)GetRelationsForField("ServiceIdToCards")[0], (int)VarioSL.Entities.EntityType.TicketTypeEntity, (int)VarioSL.Entities.EntityType.ServiceIdToCardEntity, 0, null, null, null, "ServiceIdToCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.TicketTypeEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketSerialNumber' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketSerialNumbers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection(), (IEntityRelation)GetRelationsForField("TicketSerialNumbers")[0], (int)VarioSL.Entities.EntityType.TicketTypeEntity, (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, 0, null, null, null, "TicketSerialNumbers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)TicketTypeFieldIndex.Description, true); }
			set	{ SetValue((int)TicketTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The EnumerationValue property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."ENUMERATIONVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.TicketType> EnumerationValue
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.TicketType>)GetValue((int)TicketTypeFieldIndex.EnumerationValue, false); }
			set	{ SetValue((int)TicketTypeFieldIndex.EnumerationValue, value, true); }
		}

		/// <summary> The EvendBalancing property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."EVENDBALANCING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendBalancing
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketTypeFieldIndex.EvendBalancing, false); }
			set	{ SetValue((int)TicketTypeFieldIndex.EvendBalancing, value, true); }
		}

		/// <summary> The EvendPayMask property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."EVENDPAYMASK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendPayMask
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketTypeFieldIndex.EvendPayMask, false); }
			set	{ SetValue((int)TicketTypeFieldIndex.EvendPayMask, value, true); }
		}

		/// <summary> The EvendTypeID property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."EVENDTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> EvendTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketTypeFieldIndex.EvendTypeID, false); }
			set	{ SetValue((int)TicketTypeFieldIndex.EvendTypeID, value, true); }
		}

		/// <summary> The Literal property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Literal
		{
			get { return (System.String)GetValue((int)TicketTypeFieldIndex.Literal, true); }
			set	{ SetValue((int)TicketTypeFieldIndex.Literal, value, true); }
		}

		/// <summary> The Name property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)TicketTypeFieldIndex.Name, true); }
			set	{ SetValue((int)TicketTypeFieldIndex.Name, value, true); }
		}

		/// <summary> The TicketTypeID property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."TICKETTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 TicketTypeID
		{
			get { return (System.Int64)GetValue((int)TicketTypeFieldIndex.TicketTypeID, true); }
			set	{ SetValue((int)TicketTypeFieldIndex.TicketTypeID, value, true); }
		}

		/// <summary> The TransactionTypeID property of the Entity TicketType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETTYPE"."TRANSACTIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketTypeFieldIndex.TransactionTypeID, false); }
			set	{ SetValue((int)TicketTypeFieldIndex.TransactionTypeID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ServiceIdToCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiServiceIdToCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ServiceIdToCardCollection ServiceIdToCards
		{
			get	{ return GetMultiServiceIdToCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceIdToCards. When set to true, ServiceIdToCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceIdToCards is accessed. You can always execute/ a forced fetch by calling GetMultiServiceIdToCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceIdToCards
		{
			get	{ return _alwaysFetchServiceIdToCards; }
			set	{ _alwaysFetchServiceIdToCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceIdToCards already has been fetched. Setting this property to false when ServiceIdToCards has been fetched
		/// will clear the ServiceIdToCards collection well. Setting this property to true while ServiceIdToCards hasn't been fetched disables lazy loading for ServiceIdToCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceIdToCards
		{
			get { return _alreadyFetchedServiceIdToCards;}
			set 
			{
				if(_alreadyFetchedServiceIdToCards && !value && (_serviceIdToCards != null))
				{
					_serviceIdToCards.Clear();
				}
				_alreadyFetchedServiceIdToCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketSerialNumbers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection TicketSerialNumbers
		{
			get	{ return GetMultiTicketSerialNumbers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketSerialNumbers. When set to true, TicketSerialNumbers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketSerialNumbers is accessed. You can always execute/ a forced fetch by calling GetMultiTicketSerialNumbers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketSerialNumbers
		{
			get	{ return _alwaysFetchTicketSerialNumbers; }
			set	{ _alwaysFetchTicketSerialNumbers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketSerialNumbers already has been fetched. Setting this property to false when TicketSerialNumbers has been fetched
		/// will clear the TicketSerialNumbers collection well. Setting this property to true while TicketSerialNumbers hasn't been fetched disables lazy loading for TicketSerialNumbers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketSerialNumbers
		{
			get { return _alreadyFetchedTicketSerialNumbers;}
			set 
			{
				if(_alreadyFetchedTicketSerialNumbers && !value && (_ticketSerialNumbers != null))
				{
					_ticketSerialNumbers.Clear();
				}
				_alreadyFetchedTicketSerialNumbers = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
