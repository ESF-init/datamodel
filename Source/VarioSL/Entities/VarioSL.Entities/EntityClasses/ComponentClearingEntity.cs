﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ComponentClearing'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ComponentClearingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AutomatEntity _automat;
		private bool	_alwaysFetchAutomat, _alreadyFetchedAutomat, _automatReturnsNewIfNotFound;
		private ComponentEntity _component;
		private bool	_alwaysFetchComponent, _alreadyFetchedComponent, _componentReturnsNewIfNotFound;
		private DebtorEntity _maintenanceStaff;
		private bool	_alwaysFetchMaintenanceStaff, _alreadyFetchedMaintenanceStaff, _maintenanceStaffReturnsNewIfNotFound;
		private DebtorEntity _personnelClearing;
		private bool	_alwaysFetchPersonnelClearing, _alreadyFetchedPersonnelClearing, _personnelClearingReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Automat</summary>
			public static readonly string Automat = "Automat";
			/// <summary>Member name Component</summary>
			public static readonly string Component = "Component";
			/// <summary>Member name MaintenanceStaff</summary>
			public static readonly string MaintenanceStaff = "MaintenanceStaff";
			/// <summary>Member name PersonnelClearing</summary>
			public static readonly string PersonnelClearing = "PersonnelClearing";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ComponentClearingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ComponentClearingEntity() :base("ComponentClearingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		public ComponentClearingEntity(System.Int64 componentClearingID):base("ComponentClearingEntity")
		{
			InitClassFetch(componentClearingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ComponentClearingEntity(System.Int64 componentClearingID, IPrefetchPath prefetchPathToUse):base("ComponentClearingEntity")
		{
			InitClassFetch(componentClearingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="validator">The custom validator object for this ComponentClearingEntity</param>
		public ComponentClearingEntity(System.Int64 componentClearingID, IValidator validator):base("ComponentClearingEntity")
		{
			InitClassFetch(componentClearingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ComponentClearingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_automat = (AutomatEntity)info.GetValue("_automat", typeof(AutomatEntity));
			if(_automat!=null)
			{
				_automat.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_automatReturnsNewIfNotFound = info.GetBoolean("_automatReturnsNewIfNotFound");
			_alwaysFetchAutomat = info.GetBoolean("_alwaysFetchAutomat");
			_alreadyFetchedAutomat = info.GetBoolean("_alreadyFetchedAutomat");

			_component = (ComponentEntity)info.GetValue("_component", typeof(ComponentEntity));
			if(_component!=null)
			{
				_component.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_componentReturnsNewIfNotFound = info.GetBoolean("_componentReturnsNewIfNotFound");
			_alwaysFetchComponent = info.GetBoolean("_alwaysFetchComponent");
			_alreadyFetchedComponent = info.GetBoolean("_alreadyFetchedComponent");

			_maintenanceStaff = (DebtorEntity)info.GetValue("_maintenanceStaff", typeof(DebtorEntity));
			if(_maintenanceStaff!=null)
			{
				_maintenanceStaff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_maintenanceStaffReturnsNewIfNotFound = info.GetBoolean("_maintenanceStaffReturnsNewIfNotFound");
			_alwaysFetchMaintenanceStaff = info.GetBoolean("_alwaysFetchMaintenanceStaff");
			_alreadyFetchedMaintenanceStaff = info.GetBoolean("_alreadyFetchedMaintenanceStaff");

			_personnelClearing = (DebtorEntity)info.GetValue("_personnelClearing", typeof(DebtorEntity));
			if(_personnelClearing!=null)
			{
				_personnelClearing.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personnelClearingReturnsNewIfNotFound = info.GetBoolean("_personnelClearingReturnsNewIfNotFound");
			_alwaysFetchPersonnelClearing = info.GetBoolean("_alwaysFetchPersonnelClearing");
			_alreadyFetchedPersonnelClearing = info.GetBoolean("_alreadyFetchedPersonnelClearing");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ComponentClearingFieldIndex)fieldIndex)
			{
				case ComponentClearingFieldIndex.AutomatID:
					DesetupSyncAutomat(true, false);
					_alreadyFetchedAutomat = false;
					break;
				case ComponentClearingFieldIndex.ComponentID:
					DesetupSyncComponent(true, false);
					_alreadyFetchedComponent = false;
					break;
				case ComponentClearingFieldIndex.MaintenanceStaffID:
					DesetupSyncMaintenanceStaff(true, false);
					_alreadyFetchedMaintenanceStaff = false;
					break;
				case ComponentClearingFieldIndex.PersonnelClearingID:
					DesetupSyncPersonnelClearing(true, false);
					_alreadyFetchedPersonnelClearing = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutomat = (_automat != null);
			_alreadyFetchedComponent = (_component != null);
			_alreadyFetchedMaintenanceStaff = (_maintenanceStaff != null);
			_alreadyFetchedPersonnelClearing = (_personnelClearing != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Automat":
					toReturn.Add(Relations.AutomatEntityUsingAutomatID);
					break;
				case "Component":
					toReturn.Add(Relations.ComponentEntityUsingComponentID);
					break;
				case "MaintenanceStaff":
					toReturn.Add(Relations.DebtorEntityUsingMaintenanceStaffID);
					break;
				case "PersonnelClearing":
					toReturn.Add(Relations.DebtorEntityUsingPersonnelClearingID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_automat", (!this.MarkedForDeletion?_automat:null));
			info.AddValue("_automatReturnsNewIfNotFound", _automatReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutomat", _alwaysFetchAutomat);
			info.AddValue("_alreadyFetchedAutomat", _alreadyFetchedAutomat);
			info.AddValue("_component", (!this.MarkedForDeletion?_component:null));
			info.AddValue("_componentReturnsNewIfNotFound", _componentReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchComponent", _alwaysFetchComponent);
			info.AddValue("_alreadyFetchedComponent", _alreadyFetchedComponent);
			info.AddValue("_maintenanceStaff", (!this.MarkedForDeletion?_maintenanceStaff:null));
			info.AddValue("_maintenanceStaffReturnsNewIfNotFound", _maintenanceStaffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMaintenanceStaff", _alwaysFetchMaintenanceStaff);
			info.AddValue("_alreadyFetchedMaintenanceStaff", _alreadyFetchedMaintenanceStaff);
			info.AddValue("_personnelClearing", (!this.MarkedForDeletion?_personnelClearing:null));
			info.AddValue("_personnelClearingReturnsNewIfNotFound", _personnelClearingReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPersonnelClearing", _alwaysFetchPersonnelClearing);
			info.AddValue("_alreadyFetchedPersonnelClearing", _alreadyFetchedPersonnelClearing);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Automat":
					_alreadyFetchedAutomat = true;
					this.Automat = (AutomatEntity)entity;
					break;
				case "Component":
					_alreadyFetchedComponent = true;
					this.Component = (ComponentEntity)entity;
					break;
				case "MaintenanceStaff":
					_alreadyFetchedMaintenanceStaff = true;
					this.MaintenanceStaff = (DebtorEntity)entity;
					break;
				case "PersonnelClearing":
					_alreadyFetchedPersonnelClearing = true;
					this.PersonnelClearing = (DebtorEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Automat":
					SetupSyncAutomat(relatedEntity);
					break;
				case "Component":
					SetupSyncComponent(relatedEntity);
					break;
				case "MaintenanceStaff":
					SetupSyncMaintenanceStaff(relatedEntity);
					break;
				case "PersonnelClearing":
					SetupSyncPersonnelClearing(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Automat":
					DesetupSyncAutomat(false, true);
					break;
				case "Component":
					DesetupSyncComponent(false, true);
					break;
				case "MaintenanceStaff":
					DesetupSyncMaintenanceStaff(false, true);
					break;
				case "PersonnelClearing":
					DesetupSyncPersonnelClearing(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_automat!=null)
			{
				toReturn.Add(_automat);
			}
			if(_component!=null)
			{
				toReturn.Add(_component);
			}
			if(_maintenanceStaff!=null)
			{
				toReturn.Add(_maintenanceStaff);
			}
			if(_personnelClearing!=null)
			{
				toReturn.Add(_personnelClearing);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentClearingID)
		{
			return FetchUsingPK(componentClearingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentClearingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(componentClearingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentClearingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(componentClearingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 componentClearingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(componentClearingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ComponentClearingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ComponentClearingRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public AutomatEntity GetSingleAutomat()
		{
			return GetSingleAutomat(false);
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public virtual AutomatEntity GetSingleAutomat(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutomat || forceFetch || _alwaysFetchAutomat) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutomatEntityUsingAutomatID);
				AutomatEntity newEntity = new AutomatEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutomatID);
				}
				if(fetchResult)
				{
					newEntity = (AutomatEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_automatReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Automat = newEntity;
				_alreadyFetchedAutomat = fetchResult;
			}
			return _automat;
		}


		/// <summary> Retrieves the related entity of type 'ComponentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ComponentEntity' which is related to this entity.</returns>
		public ComponentEntity GetSingleComponent()
		{
			return GetSingleComponent(false);
		}

		/// <summary> Retrieves the related entity of type 'ComponentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ComponentEntity' which is related to this entity.</returns>
		public virtual ComponentEntity GetSingleComponent(bool forceFetch)
		{
			if( ( !_alreadyFetchedComponent || forceFetch || _alwaysFetchComponent) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ComponentEntityUsingComponentID);
				ComponentEntity newEntity = new ComponentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ComponentID);
				}
				if(fetchResult)
				{
					newEntity = (ComponentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_componentReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Component = newEntity;
				_alreadyFetchedComponent = fetchResult;
			}
			return _component;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleMaintenanceStaff()
		{
			return GetSingleMaintenanceStaff(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleMaintenanceStaff(bool forceFetch)
		{
			if( ( !_alreadyFetchedMaintenanceStaff || forceFetch || _alwaysFetchMaintenanceStaff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingMaintenanceStaffID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MaintenanceStaffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_maintenanceStaffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MaintenanceStaff = newEntity;
				_alreadyFetchedMaintenanceStaff = fetchResult;
			}
			return _maintenanceStaff;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSinglePersonnelClearing()
		{
			return GetSinglePersonnelClearing(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSinglePersonnelClearing(bool forceFetch)
		{
			if( ( !_alreadyFetchedPersonnelClearing || forceFetch || _alwaysFetchPersonnelClearing) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingPersonnelClearingID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PersonnelClearingID);
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personnelClearingReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PersonnelClearing = newEntity;
				_alreadyFetchedPersonnelClearing = fetchResult;
			}
			return _personnelClearing;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Automat", _automat);
			toReturn.Add("Component", _component);
			toReturn.Add("MaintenanceStaff", _maintenanceStaff);
			toReturn.Add("PersonnelClearing", _personnelClearing);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="validator">The validator object for this ComponentClearingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 componentClearingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(componentClearingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_automatReturnsNewIfNotFound = false;
			_componentReturnsNewIfNotFound = false;
			_maintenanceStaffReturnsNewIfNotFound = false;
			_personnelClearingReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BookingNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClearingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentClearingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ComponentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountingAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FeedDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBooked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationMark", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoginDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaintenanceStaffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NumberOfCoins", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonnelClearingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReadingAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageMediumName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageMediumNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNo", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _automat</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutomat(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.AutomatEntityUsingAutomatIDStatic, true, signalRelatedEntity, "ComponentClearings", resetFKFields, new int[] { (int)ComponentClearingFieldIndex.AutomatID } );		
			_automat = null;
		}
		
		/// <summary> setups the sync logic for member _automat</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutomat(IEntityCore relatedEntity)
		{
			if(_automat!=relatedEntity)
			{		
				DesetupSyncAutomat(true, true);
				_automat = (AutomatEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.AutomatEntityUsingAutomatIDStatic, true, ref _alreadyFetchedAutomat, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutomatPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _component</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncComponent(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _component, new PropertyChangedEventHandler( OnComponentPropertyChanged ), "Component", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.ComponentEntityUsingComponentIDStatic, true, signalRelatedEntity, "ComponentClearings", resetFKFields, new int[] { (int)ComponentClearingFieldIndex.ComponentID } );		
			_component = null;
		}
		
		/// <summary> setups the sync logic for member _component</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncComponent(IEntityCore relatedEntity)
		{
			if(_component!=relatedEntity)
			{		
				DesetupSyncComponent(true, true);
				_component = (ComponentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _component, new PropertyChangedEventHandler( OnComponentPropertyChanged ), "Component", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.ComponentEntityUsingComponentIDStatic, true, ref _alreadyFetchedComponent, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnComponentPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _maintenanceStaff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMaintenanceStaff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _maintenanceStaff, new PropertyChangedEventHandler( OnMaintenanceStaffPropertyChanged ), "MaintenanceStaff", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.DebtorEntityUsingMaintenanceStaffIDStatic, true, signalRelatedEntity, "ComponentClearingsByMaintenanceStaff", resetFKFields, new int[] { (int)ComponentClearingFieldIndex.MaintenanceStaffID } );		
			_maintenanceStaff = null;
		}
		
		/// <summary> setups the sync logic for member _maintenanceStaff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMaintenanceStaff(IEntityCore relatedEntity)
		{
			if(_maintenanceStaff!=relatedEntity)
			{		
				DesetupSyncMaintenanceStaff(true, true);
				_maintenanceStaff = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _maintenanceStaff, new PropertyChangedEventHandler( OnMaintenanceStaffPropertyChanged ), "MaintenanceStaff", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.DebtorEntityUsingMaintenanceStaffIDStatic, true, ref _alreadyFetchedMaintenanceStaff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMaintenanceStaffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _personnelClearing</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPersonnelClearing(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _personnelClearing, new PropertyChangedEventHandler( OnPersonnelClearingPropertyChanged ), "PersonnelClearing", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.DebtorEntityUsingPersonnelClearingIDStatic, true, signalRelatedEntity, "ComponentClearingsByPersonnelClearing", resetFKFields, new int[] { (int)ComponentClearingFieldIndex.PersonnelClearingID } );		
			_personnelClearing = null;
		}
		
		/// <summary> setups the sync logic for member _personnelClearing</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPersonnelClearing(IEntityCore relatedEntity)
		{
			if(_personnelClearing!=relatedEntity)
			{		
				DesetupSyncPersonnelClearing(true, true);
				_personnelClearing = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _personnelClearing, new PropertyChangedEventHandler( OnPersonnelClearingPropertyChanged ), "PersonnelClearing", VarioSL.Entities.RelationClasses.StaticComponentClearingRelations.DebtorEntityUsingPersonnelClearingIDStatic, true, ref _alreadyFetchedPersonnelClearing, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonnelClearingPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="componentClearingID">PK value for ComponentClearing which data should be fetched into this ComponentClearing object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 componentClearingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ComponentClearingFieldIndex.ComponentClearingID].ForcedCurrentValueWrite(componentClearingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateComponentClearingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ComponentClearingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ComponentClearingRelations Relations
		{
			get	{ return new ComponentClearingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Automat'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomat
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutomatCollection(), (IEntityRelation)GetRelationsForField("Automat")[0], (int)VarioSL.Entities.EntityType.ComponentClearingEntity, (int)VarioSL.Entities.EntityType.AutomatEntity, 0, null, null, null, "Automat", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Component'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathComponent
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ComponentCollection(), (IEntityRelation)GetRelationsForField("Component")[0], (int)VarioSL.Entities.EntityType.ComponentClearingEntity, (int)VarioSL.Entities.EntityType.ComponentEntity, 0, null, null, null, "Component", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMaintenanceStaff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("MaintenanceStaff")[0], (int)VarioSL.Entities.EntityType.ComponentClearingEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "MaintenanceStaff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPersonnelClearing
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("PersonnelClearing")[0], (int)VarioSL.Entities.EntityType.ComponentClearingEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "PersonnelClearing", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutomatID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."AUTOMATID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 AutomatID
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.AutomatID, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.AutomatID, value, true); }
		}

		/// <summary> The BookingNo property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."BOOKINGNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BookingNo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentClearingFieldIndex.BookingNo, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.BookingNo, value, true); }
		}

		/// <summary> The CashUnitID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."CASHUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashUnitID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentClearingFieldIndex.CashUnitID, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.CashUnitID, value, true); }
		}

		/// <summary> The ClearingDate property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."CLEARINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ClearingDate
		{
			get { return (System.DateTime)GetValue((int)ComponentClearingFieldIndex.ClearingDate, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.ClearingDate, value, true); }
		}

		/// <summary> The ComponentClearingID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."COMPONENTCLEARINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ComponentClearingID
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.ComponentClearingID, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.ComponentClearingID, value, true); }
		}

		/// <summary> The ComponentID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."COMPONENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ComponentID
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.ComponentID, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.ComponentID, value, true); }
		}

		/// <summary> The CountingAmount property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."COUNTINGAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CountingAmount
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.CountingAmount, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.CountingAmount, value, true); }
		}

		/// <summary> The Description property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ComponentClearingFieldIndex.Description, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.Description, value, true); }
		}

		/// <summary> The FeedDate property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."FEEDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FeedDate
		{
			get { return (System.DateTime)GetValue((int)ComponentClearingFieldIndex.FeedDate, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.FeedDate, value, true); }
		}

		/// <summary> The IsBooked property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."ISBOOKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 IsBooked
		{
			get { return (System.Int16)GetValue((int)ComponentClearingFieldIndex.IsBooked, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.IsBooked, value, true); }
		}

		/// <summary> The LocationMark property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."LOCATIONMARK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LocationMark
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentClearingFieldIndex.LocationMark, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.LocationMark, value, true); }
		}

		/// <summary> The LoginDate property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."LOGINDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LoginDate
		{
			get { return (System.DateTime)GetValue((int)ComponentClearingFieldIndex.LoginDate, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.LoginDate, value, true); }
		}

		/// <summary> The MaintenanceStaffID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."MAINTENANCESTAFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaintenanceStaffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ComponentClearingFieldIndex.MaintenanceStaffID, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.MaintenanceStaffID, value, true); }
		}

		/// <summary> The MountingPlateNumber property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."MOUNTINGPLATENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MountingPlateNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentClearingFieldIndex.MountingPlateNumber, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.MountingPlateNumber, value, true); }
		}

		/// <summary> The NumberOfCoins property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."NUMBEROFCOINS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 NumberOfCoins
		{
			get { return (System.Int32)GetValue((int)ComponentClearingFieldIndex.NumberOfCoins, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.NumberOfCoins, value, true); }
		}

		/// <summary> The PersonnelClearingID property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."PERSONNELCLEARINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonnelClearingID
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.PersonnelClearingID, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.PersonnelClearingID, value, true); }
		}

		/// <summary> The ReadingAmount property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."READINGAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ReadingAmount
		{
			get { return (System.Int64)GetValue((int)ComponentClearingFieldIndex.ReadingAmount, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.ReadingAmount, value, true); }
		}

		/// <summary> The StorageMediumName property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."STORAGEMEDIUMNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StorageMediumName
		{
			get { return (System.String)GetValue((int)ComponentClearingFieldIndex.StorageMediumName, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.StorageMediumName, value, true); }
		}

		/// <summary> The StorageMediumNo property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."STORAGEMEDIUMNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String StorageMediumNo
		{
			get { return (System.String)GetValue((int)ComponentClearingFieldIndex.StorageMediumNo, true); }
			set	{ SetValue((int)ComponentClearingFieldIndex.StorageMediumNo, value, true); }
		}

		/// <summary> The VehicleNo property of the Entity ComponentClearing<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_COMPONENTCLEARING"."VEHICLENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VehicleNo
		{
			get { return (Nullable<System.Int32>)GetValue((int)ComponentClearingFieldIndex.VehicleNo, false); }
			set	{ SetValue((int)ComponentClearingFieldIndex.VehicleNo, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AutomatEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutomat()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutomatEntity Automat
		{
			get	{ return GetSingleAutomat(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutomat(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentClearings", "Automat", _automat, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Automat. When set to true, Automat is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Automat is accessed. You can always execute a forced fetch by calling GetSingleAutomat(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomat
		{
			get	{ return _alwaysFetchAutomat; }
			set	{ _alwaysFetchAutomat = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Automat already has been fetched. Setting this property to false when Automat has been fetched
		/// will set Automat to null as well. Setting this property to true while Automat hasn't been fetched disables lazy loading for Automat</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomat
		{
			get { return _alreadyFetchedAutomat;}
			set 
			{
				if(_alreadyFetchedAutomat && !value)
				{
					this.Automat = null;
				}
				_alreadyFetchedAutomat = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Automat is not found
		/// in the database. When set to true, Automat will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutomatReturnsNewIfNotFound
		{
			get	{ return _automatReturnsNewIfNotFound; }
			set { _automatReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ComponentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleComponent()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ComponentEntity Component
		{
			get	{ return GetSingleComponent(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncComponent(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentClearings", "Component", _component, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Component. When set to true, Component is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Component is accessed. You can always execute a forced fetch by calling GetSingleComponent(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchComponent
		{
			get	{ return _alwaysFetchComponent; }
			set	{ _alwaysFetchComponent = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Component already has been fetched. Setting this property to false when Component has been fetched
		/// will set Component to null as well. Setting this property to true while Component hasn't been fetched disables lazy loading for Component</summary>
		[Browsable(false)]
		public bool AlreadyFetchedComponent
		{
			get { return _alreadyFetchedComponent;}
			set 
			{
				if(_alreadyFetchedComponent && !value)
				{
					this.Component = null;
				}
				_alreadyFetchedComponent = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Component is not found
		/// in the database. When set to true, Component will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ComponentReturnsNewIfNotFound
		{
			get	{ return _componentReturnsNewIfNotFound; }
			set { _componentReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMaintenanceStaff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity MaintenanceStaff
		{
			get	{ return GetSingleMaintenanceStaff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMaintenanceStaff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentClearingsByMaintenanceStaff", "MaintenanceStaff", _maintenanceStaff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MaintenanceStaff. When set to true, MaintenanceStaff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MaintenanceStaff is accessed. You can always execute a forced fetch by calling GetSingleMaintenanceStaff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMaintenanceStaff
		{
			get	{ return _alwaysFetchMaintenanceStaff; }
			set	{ _alwaysFetchMaintenanceStaff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MaintenanceStaff already has been fetched. Setting this property to false when MaintenanceStaff has been fetched
		/// will set MaintenanceStaff to null as well. Setting this property to true while MaintenanceStaff hasn't been fetched disables lazy loading for MaintenanceStaff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMaintenanceStaff
		{
			get { return _alreadyFetchedMaintenanceStaff;}
			set 
			{
				if(_alreadyFetchedMaintenanceStaff && !value)
				{
					this.MaintenanceStaff = null;
				}
				_alreadyFetchedMaintenanceStaff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MaintenanceStaff is not found
		/// in the database. When set to true, MaintenanceStaff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MaintenanceStaffReturnsNewIfNotFound
		{
			get	{ return _maintenanceStaffReturnsNewIfNotFound; }
			set { _maintenanceStaffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePersonnelClearing()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity PersonnelClearing
		{
			get	{ return GetSinglePersonnelClearing(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPersonnelClearing(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ComponentClearingsByPersonnelClearing", "PersonnelClearing", _personnelClearing, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PersonnelClearing. When set to true, PersonnelClearing is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PersonnelClearing is accessed. You can always execute a forced fetch by calling GetSinglePersonnelClearing(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPersonnelClearing
		{
			get	{ return _alwaysFetchPersonnelClearing; }
			set	{ _alwaysFetchPersonnelClearing = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PersonnelClearing already has been fetched. Setting this property to false when PersonnelClearing has been fetched
		/// will set PersonnelClearing to null as well. Setting this property to true while PersonnelClearing hasn't been fetched disables lazy loading for PersonnelClearing</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPersonnelClearing
		{
			get { return _alreadyFetchedPersonnelClearing;}
			set 
			{
				if(_alreadyFetchedPersonnelClearing && !value)
				{
					this.PersonnelClearing = null;
				}
				_alreadyFetchedPersonnelClearing = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PersonnelClearing is not found
		/// in the database. When set to true, PersonnelClearing will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonnelClearingReturnsNewIfNotFound
		{
			get	{ return _personnelClearingReturnsNewIfNotFound; }
			set { _personnelClearingReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ComponentClearingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
