﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ReportJobResult'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ReportJobResultEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private ReportEntity _report;
		private bool	_alwaysFetchReport, _alreadyFetchedReport, _reportReturnsNewIfNotFound;
		private ReportJobEntity _reportJob;
		private bool	_alwaysFetchReportJob, _alreadyFetchedReportJob, _reportJobReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Report</summary>
			public static readonly string Report = "Report";
			/// <summary>Member name ReportJob</summary>
			public static readonly string ReportJob = "ReportJob";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportJobResultEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ReportJobResultEntity() :base("ReportJobResultEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		public ReportJobResultEntity(System.Int64 reportJobResultID):base("ReportJobResultEntity")
		{
			InitClassFetch(reportJobResultID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReportJobResultEntity(System.Int64 reportJobResultID, IPrefetchPath prefetchPathToUse):base("ReportJobResultEntity")
		{
			InitClassFetch(reportJobResultID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="validator">The custom validator object for this ReportJobResultEntity</param>
		public ReportJobResultEntity(System.Int64 reportJobResultID, IValidator validator):base("ReportJobResultEntity")
		{
			InitClassFetch(reportJobResultID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportJobResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_report = (ReportEntity)info.GetValue("_report", typeof(ReportEntity));
			if(_report!=null)
			{
				_report.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportReturnsNewIfNotFound = info.GetBoolean("_reportReturnsNewIfNotFound");
			_alwaysFetchReport = info.GetBoolean("_alwaysFetchReport");
			_alreadyFetchedReport = info.GetBoolean("_alreadyFetchedReport");

			_reportJob = (ReportJobEntity)info.GetValue("_reportJob", typeof(ReportJobEntity));
			if(_reportJob!=null)
			{
				_reportJob.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportJobReturnsNewIfNotFound = info.GetBoolean("_reportJobReturnsNewIfNotFound");
			_alwaysFetchReportJob = info.GetBoolean("_alwaysFetchReportJob");
			_alreadyFetchedReportJob = info.GetBoolean("_alreadyFetchedReportJob");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportJobResultFieldIndex)fieldIndex)
			{
				case ReportJobResultFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ReportJobResultFieldIndex.ReportID:
					DesetupSyncReport(true, false);
					_alreadyFetchedReport = false;
					break;
				case ReportJobResultFieldIndex.ReportJobID:
					DesetupSyncReportJob(true, false);
					_alreadyFetchedReportJob = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedReport = (_report != null);
			_alreadyFetchedReportJob = (_reportJob != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Report":
					toReturn.Add(Relations.ReportEntityUsingReportID);
					break;
				case "ReportJob":
					toReturn.Add(Relations.ReportJobEntityUsingReportJobID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_report", (!this.MarkedForDeletion?_report:null));
			info.AddValue("_reportReturnsNewIfNotFound", _reportReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReport", _alwaysFetchReport);
			info.AddValue("_alreadyFetchedReport", _alreadyFetchedReport);
			info.AddValue("_reportJob", (!this.MarkedForDeletion?_reportJob:null));
			info.AddValue("_reportJobReturnsNewIfNotFound", _reportJobReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportJob", _alwaysFetchReportJob);
			info.AddValue("_alreadyFetchedReportJob", _alreadyFetchedReportJob);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Report":
					_alreadyFetchedReport = true;
					this.Report = (ReportEntity)entity;
					break;
				case "ReportJob":
					_alreadyFetchedReportJob = true;
					this.ReportJob = (ReportJobEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Report":
					SetupSyncReport(relatedEntity);
					break;
				case "ReportJob":
					SetupSyncReportJob(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Report":
					DesetupSyncReport(false, true);
					break;
				case "ReportJob":
					DesetupSyncReportJob(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_report!=null)
			{
				toReturn.Add(_report);
			}
			if(_reportJob!=null)
			{
				toReturn.Add(_reportJob);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobResultID)
		{
			return FetchUsingPK(reportJobResultID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobResultID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportJobResultID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobResultID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportJobResultID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 reportJobResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportJobResultID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportJobResultID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportJobResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'ReportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportEntity' which is related to this entity.</returns>
		public ReportEntity GetSingleReport()
		{
			return GetSingleReport(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportEntity' which is related to this entity.</returns>
		public virtual ReportEntity GetSingleReport(bool forceFetch)
		{
			if( ( !_alreadyFetchedReport || forceFetch || _alwaysFetchReport) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportEntityUsingReportID);
				ReportEntity newEntity = new ReportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Report = newEntity;
				_alreadyFetchedReport = fetchResult;
			}
			return _report;
		}


		/// <summary> Retrieves the related entity of type 'ReportJobEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportJobEntity' which is related to this entity.</returns>
		public ReportJobEntity GetSingleReportJob()
		{
			return GetSingleReportJob(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportJobEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportJobEntity' which is related to this entity.</returns>
		public virtual ReportJobEntity GetSingleReportJob(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportJob || forceFetch || _alwaysFetchReportJob) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportJobEntityUsingReportJobID);
				ReportJobEntity newEntity = new ReportJobEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportJobID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportJobEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportJobReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportJob = newEntity;
				_alreadyFetchedReportJob = fetchResult;
			}
			return _reportJob;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Report", _report);
			toReturn.Add("ReportJob", _reportJob);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="validator">The validator object for this ReportJobResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 reportJobResultID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportJobResultID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientReturnsNewIfNotFound = false;
			_reportReturnsNewIfNotFound = false;
			_reportJobReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EndTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportJobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportJobResultID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Success", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ReportJobResults", resetFKFields, new int[] { (int)ReportJobResultFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _report</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReport(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _report, new PropertyChangedEventHandler( OnReportPropertyChanged ), "Report", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ReportEntityUsingReportIDStatic, true, signalRelatedEntity, "ReportJobResults", resetFKFields, new int[] { (int)ReportJobResultFieldIndex.ReportID } );		
			_report = null;
		}
		
		/// <summary> setups the sync logic for member _report</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReport(IEntityCore relatedEntity)
		{
			if(_report!=relatedEntity)
			{		
				DesetupSyncReport(true, true);
				_report = (ReportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _report, new PropertyChangedEventHandler( OnReportPropertyChanged ), "Report", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ReportEntityUsingReportIDStatic, true, ref _alreadyFetchedReport, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportJob</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportJob(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportJob, new PropertyChangedEventHandler( OnReportJobPropertyChanged ), "ReportJob", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ReportJobEntityUsingReportJobIDStatic, true, signalRelatedEntity, "ReportJobResults", resetFKFields, new int[] { (int)ReportJobResultFieldIndex.ReportJobID } );		
			_reportJob = null;
		}
		
		/// <summary> setups the sync logic for member _reportJob</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportJob(IEntityCore relatedEntity)
		{
			if(_reportJob!=relatedEntity)
			{		
				DesetupSyncReportJob(true, true);
				_reportJob = (ReportJobEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportJob, new PropertyChangedEventHandler( OnReportJobPropertyChanged ), "ReportJob", VarioSL.Entities.RelationClasses.StaticReportJobResultRelations.ReportJobEntityUsingReportJobIDStatic, true, ref _alreadyFetchedReportJob, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportJobPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportJobResultID">PK value for ReportJobResult which data should be fetched into this ReportJobResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 reportJobResultID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportJobResultFieldIndex.ReportJobResultID].ForcedCurrentValueWrite(reportJobResultID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportJobResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportJobResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportJobResultRelations Relations
		{
			get	{ return new ReportJobResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ReportJobResultEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReport
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), (IEntityRelation)GetRelationsForField("Report")[0], (int)VarioSL.Entities.EntityType.ReportJobResultEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, null, "Report", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportJob'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportJob
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportJobCollection(), (IEntityRelation)GetRelationsForField("ReportJob")[0], (int)VarioSL.Entities.EntityType.ReportJobResultEntity, (int)VarioSL.Entities.EntityType.ReportJobEntity, 0, null, null, null, "ReportJob", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportJobResultFieldIndex.ClientID, false); }
			set	{ SetValue((int)ReportJobResultFieldIndex.ClientID, value, true); }
		}

		/// <summary> The EndTime property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."ENDTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime EndTime
		{
			get { return (System.DateTime)GetValue((int)ReportJobResultFieldIndex.EndTime, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.EndTime, value, true); }
		}

		/// <summary> The FileName property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."FILENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String FileName
		{
			get { return (System.String)GetValue((int)ReportJobResultFieldIndex.FileName, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.FileName, value, true); }
		}

		/// <summary> The LastModified property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ReportJobResultFieldIndex.LastModified, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ReportJobResultFieldIndex.LastUser, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.LastUser, value, true); }
		}

		/// <summary> The ReportID property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."REPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReportID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportJobResultFieldIndex.ReportID, false); }
			set	{ SetValue((int)ReportJobResultFieldIndex.ReportID, value, true); }
		}

		/// <summary> The ReportJobID property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."REPORTJOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReportJobID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ReportJobResultFieldIndex.ReportJobID, false); }
			set	{ SetValue((int)ReportJobResultFieldIndex.ReportJobID, value, true); }
		}

		/// <summary> The ReportJobResultID property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."REPORTJOBRESULTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ReportJobResultID
		{
			get { return (System.Int64)GetValue((int)ReportJobResultFieldIndex.ReportJobResultID, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.ReportJobResultID, value, true); }
		}

		/// <summary> The StartTime property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."STARTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime StartTime
		{
			get { return (System.DateTime)GetValue((int)ReportJobResultFieldIndex.StartTime, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.StartTime, value, true); }
		}

		/// <summary> The Success property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."SUCCESS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Success
		{
			get { return (System.Boolean)GetValue((int)ReportJobResultFieldIndex.Success, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.Success, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ReportJobResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_REPORTJOBRESULT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ReportJobResultFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ReportJobResultFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobResults", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReport()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportEntity Report
		{
			get	{ return GetSingleReport(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReport(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobResults", "Report", _report, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Report. When set to true, Report is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Report is accessed. You can always execute a forced fetch by calling GetSingleReport(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReport
		{
			get	{ return _alwaysFetchReport; }
			set	{ _alwaysFetchReport = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Report already has been fetched. Setting this property to false when Report has been fetched
		/// will set Report to null as well. Setting this property to true while Report hasn't been fetched disables lazy loading for Report</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReport
		{
			get { return _alreadyFetchedReport;}
			set 
			{
				if(_alreadyFetchedReport && !value)
				{
					this.Report = null;
				}
				_alreadyFetchedReport = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Report is not found
		/// in the database. When set to true, Report will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReportReturnsNewIfNotFound
		{
			get	{ return _reportReturnsNewIfNotFound; }
			set { _reportReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportJobEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportJob()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ReportJobEntity ReportJob
		{
			get	{ return GetSingleReportJob(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReportJob(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportJobResults", "ReportJob", _reportJob, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportJob. When set to true, ReportJob is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportJob is accessed. You can always execute a forced fetch by calling GetSingleReportJob(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportJob
		{
			get	{ return _alwaysFetchReportJob; }
			set	{ _alwaysFetchReportJob = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportJob already has been fetched. Setting this property to false when ReportJob has been fetched
		/// will set ReportJob to null as well. Setting this property to true while ReportJob hasn't been fetched disables lazy loading for ReportJob</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportJob
		{
			get { return _alreadyFetchedReportJob;}
			set 
			{
				if(_alreadyFetchedReportJob && !value)
				{
					this.ReportJob = null;
				}
				_alreadyFetchedReportJob = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportJob is not found
		/// in the database. When set to true, ReportJob will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReportJobReturnsNewIfNotFound
		{
			get	{ return _reportJobReturnsNewIfNotFound; }
			set { _reportJobReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ReportJobResultEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
