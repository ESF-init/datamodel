﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardHolder'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardHolderEntity : PersonEntity
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardCollection	_cards;
		private bool	_alwaysFetchCards, _alreadyFetchedCards;
		private VarioSL.Entities.CollectionClasses.CardCollection	_participantCards;
		private bool	_alwaysFetchParticipantCards, _alreadyFetchedParticipantCards;
		private VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection	_orderDetailToCardHolder;
		private bool	_alwaysFetchOrderDetailToCardHolder, _alreadyFetchedOrderDetailToCardHolder;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection _orderDetails;
		private bool	_alwaysFetchOrderDetails, _alreadyFetchedOrderDetails;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private SchoolYearEntity _schoolYear;
		private bool	_alwaysFetchSchoolYear, _alreadyFetchedSchoolYear, _schoolYearReturnsNewIfNotFound;
		private AddressEntity _alternativeAddress;
		private bool	_alwaysFetchAlternativeAddress, _alreadyFetchedAlternativeAddress, _alternativeAddressReturnsNewIfNotFound;
		private CardHolderOrganizationEntity _cardHolderOrganization;
		private bool	_alwaysFetchCardHolderOrganization, _alreadyFetchedCardHolderOrganization, _cardHolderOrganizationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static new partial class MemberNames
		{
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name SchoolYear</summary>
			public static readonly string SchoolYear = "SchoolYear";
			/// <summary>Member name BankConnectionDatas</summary>
			public static readonly string BankConnectionDatas = "BankConnectionDatas";
			/// <summary>Member name Cards</summary>
			public static readonly string Cards = "Cards";
			/// <summary>Member name ParticipantCards</summary>
			public static readonly string ParticipantCards = "ParticipantCards";
			/// <summary>Member name CustomAttributeValueToPeople</summary>
			public static readonly string CustomAttributeValueToPeople = "CustomAttributeValueToPeople";
			/// <summary>Member name CustomerAccounts</summary>
			public static readonly string CustomerAccounts = "CustomerAccounts";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
			/// <summary>Member name Entitlements</summary>
			public static readonly string Entitlements = "Entitlements";
			/// <summary>Member name FareEvasionIncidentsAsGuardian</summary>
			public static readonly string FareEvasionIncidentsAsGuardian = "FareEvasionIncidentsAsGuardian";
			/// <summary>Member name FareEvasionIncidentsAsPerson</summary>
			public static readonly string FareEvasionIncidentsAsPerson = "FareEvasionIncidentsAsPerson";
			/// <summary>Member name OrderDetailToCardHolder</summary>
			public static readonly string OrderDetailToCardHolder = "OrderDetailToCardHolder";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name PersonAddresses</summary>
			public static readonly string PersonAddresses = "PersonAddresses";
			/// <summary>Member name Photographs</summary>
			public static readonly string Photographs = "Photographs";
			/// <summary>Member name RecipientGroupMembers</summary>
			public static readonly string RecipientGroupMembers = "RecipientGroupMembers";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name SurveyResponses</summary>
			public static readonly string SurveyResponses = "SurveyResponses";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name Addresses</summary>
			public static readonly string Addresses = "Addresses";
			/// <summary>Member name OrderDetails</summary>
			public static readonly string OrderDetails = "OrderDetails";
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
			/// <summary>Member name AlternativeAddress</summary>
			public static readonly string AlternativeAddress = "AlternativeAddress";
			/// <summary>Member name CardHolderOrganization</summary>
			public static readonly string CardHolderOrganization = "CardHolderOrganization";
			/// <summary>Member name ContactPersonOnOrganization</summary>
			public static readonly string ContactPersonOnOrganization = "ContactPersonOnOrganization";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardHolderEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardHolderEntity() 
		{
			InitClassEmpty(null);
			SetName("CardHolderEntity");
		}
		
		/// <summary>CTor</summary>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		public CardHolderEntity(System.Int64 personID):base(personID)
		{
			InitClassFetch(personID, null, null);
			SetName("CardHolderEntity");
		}

		/// <summary>CTor</summary>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardHolderEntity(System.Int64 personID, IPrefetchPath prefetchPathToUse):base(personID, prefetchPathToUse)
		{
			InitClassFetch(personID, null, prefetchPathToUse);
			SetName("CardHolderEntity");
		}

		/// <summary>CTor</summary>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		/// <param name="validator">The custom validator object for this CardHolderEntity</param>
		public CardHolderEntity(System.Int64 personID, IValidator validator):base(personID, validator)
		{
			InitClassFetch(personID, validator, null);
			SetName("CardHolderEntity");
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardHolderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCards = info.GetBoolean("_alwaysFetchCards");
			_alreadyFetchedCards = info.GetBoolean("_alreadyFetchedCards");

			_participantCards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_participantCards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchParticipantCards = info.GetBoolean("_alwaysFetchParticipantCards");
			_alreadyFetchedParticipantCards = info.GetBoolean("_alreadyFetchedParticipantCards");

			_orderDetailToCardHolder = (VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection)info.GetValue("_orderDetailToCardHolder", typeof(VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection));
			_alwaysFetchOrderDetailToCardHolder = info.GetBoolean("_alwaysFetchOrderDetailToCardHolder");
			_alreadyFetchedOrderDetailToCardHolder = info.GetBoolean("_alreadyFetchedOrderDetailToCardHolder");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");
			_orderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_orderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchOrderDetails = info.GetBoolean("_alwaysFetchOrderDetails");
			_alreadyFetchedOrderDetails = info.GetBoolean("_alreadyFetchedOrderDetails");
			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_schoolYear = (SchoolYearEntity)info.GetValue("_schoolYear", typeof(SchoolYearEntity));
			if(_schoolYear!=null)
			{
				_schoolYear.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_schoolYearReturnsNewIfNotFound = info.GetBoolean("_schoolYearReturnsNewIfNotFound");
			_alwaysFetchSchoolYear = info.GetBoolean("_alwaysFetchSchoolYear");
			_alreadyFetchedSchoolYear = info.GetBoolean("_alreadyFetchedSchoolYear");
			_alternativeAddress = (AddressEntity)info.GetValue("_alternativeAddress", typeof(AddressEntity));
			if(_alternativeAddress!=null)
			{
				_alternativeAddress.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_alternativeAddressReturnsNewIfNotFound = info.GetBoolean("_alternativeAddressReturnsNewIfNotFound");
			_alwaysFetchAlternativeAddress = info.GetBoolean("_alwaysFetchAlternativeAddress");
			_alreadyFetchedAlternativeAddress = info.GetBoolean("_alreadyFetchedAlternativeAddress");

			_cardHolderOrganization = (CardHolderOrganizationEntity)info.GetValue("_cardHolderOrganization", typeof(CardHolderOrganizationEntity));
			if(_cardHolderOrganization!=null)
			{
				_cardHolderOrganization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardHolderOrganizationReturnsNewIfNotFound = info.GetBoolean("_cardHolderOrganizationReturnsNewIfNotFound");
			_alwaysFetchCardHolderOrganization = info.GetBoolean("_alwaysFetchCardHolderOrganization");
			_alreadyFetchedCardHolderOrganization = info.GetBoolean("_alreadyFetchedCardHolderOrganization");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardHolderFieldIndex)fieldIndex)
			{
				case CardHolderFieldIndex.AlternativeAdressID:
					DesetupSyncAlternativeAddress(true, false);
					_alreadyFetchedAlternativeAddress = false;
					break;
				case CardHolderFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case CardHolderFieldIndex.SchoolYearID:
					DesetupSyncSchoolYear(true, false);
					_alreadyFetchedSchoolYear = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCards = (_cards.Count > 0);
			_alreadyFetchedParticipantCards = (_participantCards.Count > 0);
			_alreadyFetchedOrderDetailToCardHolder = (_orderDetailToCardHolder.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedOrderDetails = (_orderDetails.Count > 0);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedSchoolYear = (_schoolYear != null);
			_alreadyFetchedAlternativeAddress = (_alternativeAddress != null);
			_alreadyFetchedCardHolderOrganization = (_cardHolderOrganization != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static new RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "SchoolYear":
					toReturn.Add(Relations.SchoolYearEntityUsingSchoolYearID);
					break;
				case "Cards":
					toReturn.Add(Relations.CardEntityUsingCardHolderID);
					break;
				case "ParticipantCards":
					toReturn.Add(Relations.CardEntityUsingParticipantID);
					break;
				case "OrderDetailToCardHolder":
					toReturn.Add(Relations.OrderDetailToCardHolderEntityUsingCardHolderID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingCardHolderID);
					break;
				case "OrderDetails":
					toReturn.Add(Relations.OrderDetailToCardHolderEntityUsingCardHolderID, "CardHolderEntity__", "OrderDetailToCardHolder_", JoinHint.None);
					toReturn.Add(OrderDetailToCardHolderEntity.Relations.OrderDetailEntityUsingOrderDetailID, "OrderDetailToCardHolder_", string.Empty, JoinHint.None);
					break;
				case "AlternativeAddress":
					toReturn.Add(Relations.AddressEntityUsingAlternativeAdressID);
					break;
				case "CardHolderOrganization":
					toReturn.Add(Relations.CardHolderOrganizationEntityUsingCardHolderID);
					break;
				default:
					toReturn = PersonEntity.GetRelationsForField(fieldName);
					break;				
			}
			return toReturn;
		}

		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public new static IPredicateExpression GetEntityTypeFilter()
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("CardHolderEntity", false);
		}
		
		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public new static IPredicateExpression GetEntityTypeFilter(bool negate)
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("CardHolderEntity", negate);
		}

		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cards", (!this.MarkedForDeletion?_cards:null));
			info.AddValue("_alwaysFetchCards", _alwaysFetchCards);
			info.AddValue("_alreadyFetchedCards", _alreadyFetchedCards);
			info.AddValue("_participantCards", (!this.MarkedForDeletion?_participantCards:null));
			info.AddValue("_alwaysFetchParticipantCards", _alwaysFetchParticipantCards);
			info.AddValue("_alreadyFetchedParticipantCards", _alreadyFetchedParticipantCards);
			info.AddValue("_orderDetailToCardHolder", (!this.MarkedForDeletion?_orderDetailToCardHolder:null));
			info.AddValue("_alwaysFetchOrderDetailToCardHolder", _alwaysFetchOrderDetailToCardHolder);
			info.AddValue("_alreadyFetchedOrderDetailToCardHolder", _alreadyFetchedOrderDetailToCardHolder);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_orderDetails", (!this.MarkedForDeletion?_orderDetails:null));
			info.AddValue("_alwaysFetchOrderDetails", _alwaysFetchOrderDetails);
			info.AddValue("_alreadyFetchedOrderDetails", _alreadyFetchedOrderDetails);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_schoolYear", (!this.MarkedForDeletion?_schoolYear:null));
			info.AddValue("_schoolYearReturnsNewIfNotFound", _schoolYearReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSchoolYear", _alwaysFetchSchoolYear);
			info.AddValue("_alreadyFetchedSchoolYear", _alreadyFetchedSchoolYear);

			info.AddValue("_alternativeAddress", (!this.MarkedForDeletion?_alternativeAddress:null));
			info.AddValue("_alternativeAddressReturnsNewIfNotFound", _alternativeAddressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAlternativeAddress", _alwaysFetchAlternativeAddress);
			info.AddValue("_alreadyFetchedAlternativeAddress", _alreadyFetchedAlternativeAddress);

			info.AddValue("_cardHolderOrganization", (!this.MarkedForDeletion?_cardHolderOrganization:null));
			info.AddValue("_cardHolderOrganizationReturnsNewIfNotFound", _cardHolderOrganizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardHolderOrganization", _alwaysFetchCardHolderOrganization);
			info.AddValue("_alreadyFetchedCardHolderOrganization", _alreadyFetchedCardHolderOrganization);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "SchoolYear":
					_alreadyFetchedSchoolYear = true;
					this.SchoolYear = (SchoolYearEntity)entity;
					break;
				case "Cards":
					_alreadyFetchedCards = true;
					if(entity!=null)
					{
						this.Cards.Add((CardEntity)entity);
					}
					break;
				case "ParticipantCards":
					_alreadyFetchedParticipantCards = true;
					if(entity!=null)
					{
						this.ParticipantCards.Add((CardEntity)entity);
					}
					break;
				case "OrderDetailToCardHolder":
					_alreadyFetchedOrderDetailToCardHolder = true;
					if(entity!=null)
					{
						this.OrderDetailToCardHolder.Add((OrderDetailToCardHolderEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "OrderDetails":
					_alreadyFetchedOrderDetails = true;
					if(entity!=null)
					{
						this.OrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "AlternativeAddress":
					_alreadyFetchedAlternativeAddress = true;
					this.AlternativeAddress = (AddressEntity)entity;
					break;
				case "CardHolderOrganization":
					_alreadyFetchedCardHolderOrganization = true;
					this.CardHolderOrganization = (CardHolderOrganizationEntity)entity;
					break;
				default:
					base.SetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "SchoolYear":
					SetupSyncSchoolYear(relatedEntity);
					break;
				case "Cards":
					_cards.Add((CardEntity)relatedEntity);
					break;
				case "ParticipantCards":
					_participantCards.Add((CardEntity)relatedEntity);
					break;
				case "OrderDetailToCardHolder":
					_orderDetailToCardHolder.Add((OrderDetailToCardHolderEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "AlternativeAddress":
					SetupSyncAlternativeAddress(relatedEntity);
					break;
				case "CardHolderOrganization":
					SetupSyncCardHolderOrganization(relatedEntity);
					break;
				default:
					base.SetRelatedEntity(relatedEntity, fieldName);
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "SchoolYear":
					DesetupSyncSchoolYear(false, true);
					break;
				case "Cards":
					this.PerformRelatedEntityRemoval(_cards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParticipantCards":
					this.PerformRelatedEntityRemoval(_participantCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderDetailToCardHolder":
					this.PerformRelatedEntityRemoval(_orderDetailToCardHolder, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AlternativeAddress":
					DesetupSyncAlternativeAddress(false, true);
					break;
				case "CardHolderOrganization":
					DesetupSyncCardHolderOrganization(false, true);
					break;
				default:
					base.UnsetRelatedEntity(relatedEntity, fieldName, signalRelatedEntityManyToOne);
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardHolderOrganization!=null)
			{
				toReturn.Add(_cardHolderOrganization);
			}
			toReturn.AddRange(base.GetDependingRelatedEntities());
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_schoolYear!=null)
			{
				toReturn.Add(_schoolYear);
			}
			if(_alternativeAddress!=null)
			{
				toReturn.Add(_alternativeAddress);
			}
			toReturn.AddRange(base.GetDependentRelatedEntities());
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cards);
			toReturn.Add(_participantCards);
			toReturn.Add(_orderDetailToCardHolder);
			toReturn.Add(_transactionJournals);
			toReturn.AddRange(base.GetMemberEntityCollections());
			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlternativeAdressID(Nullable<System.Int64> alternativeAdressID)
		{
			return FetchUsingUCAlternativeAdressID( alternativeAdressID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlternativeAdressID(Nullable<System.Int64> alternativeAdressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAlternativeAdressID( alternativeAdressID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlternativeAdressID(Nullable<System.Int64> alternativeAdressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAlternativeAdressID( alternativeAdressID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAlternativeAdressID(Nullable<System.Int64> alternativeAdressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((CardHolderDAO)CreateDAOInstance()).FetchCardHolderUsingUCAlternativeAdressID(this, this.Transaction, alternativeAdressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary>Fetches the contents of this entity from the persistent storage using a polymorphic fetch on unique constraint, so the entity returned could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static CardHolderEntity FetchPolymorphicUsingUCAlternativeAdressID(ITransaction transactionToUse, Nullable<System.Int64> alternativeAdressID, Context contextToUse)
		{
			return FetchPolymorphicUsingUCAlternativeAdressID(transactionToUse, alternativeAdressID, contextToUse, null);
		}
		
		/// <summary>Fetches the contents of this entity from the persistent storage using a polymorphic fetch on unique constraint, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="alternativeAdressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static CardHolderEntity FetchPolymorphicUsingUCAlternativeAdressID(ITransaction transactionToUse, Nullable<System.Int64> alternativeAdressID, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return new CardHolderDAO().FetchCardHolderPolyUsingUCAlternativeAdressID(transactionToUse, alternativeAdressID, contextToUse, excludedIncludedFields);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static new CardHolderEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 personID, Context contextToUse)
		{
			return FetchPolymorphic(transactionToUse, personID, contextToUse, null);
		}
				
		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static new CardHolderEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 personID, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.CardHolderEntity);
			fields.ForcedValueWrite((int)CardHolderFieldIndex.PersonID, personID);
			return (CardHolderEntity)new CardHolderDAO().FetchExistingPolymorphic(transactionToUse, fields, contextToUse, excludedIncludedFields);
		}


		/// <summary>Determines whether this entity is a subType of the entity represented by the passed in enum value, which represents a value in the VarioSL.Entities.EntityType enum</summary>
		/// <param name="typeOfEntity">Type of entity.</param>
		/// <returns>true if the passed in type is a supertype of this entity, otherwise false</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckIfIsSubTypeOf(int typeOfEntity)
		{
			return InheritanceInfoProviderSingleton.GetInstance().CheckIfIsSubTypeOf("CardHolderEntity", ((VarioSL.Entities.EntityType)typeOfEntity).ToString());
		}
				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardHolderRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCards || forceFetch || _alwaysFetchCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cards);
				_cards.SuppressClearInGetMulti=!forceFetch;
				_cards.EntityFactoryToUse = entityFactoryToUse;
				_cards.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_cards.SuppressClearInGetMulti=false;
				_alreadyFetchedCards = true;
			}
			return _cards;
		}

		/// <summary> Sets the collection parameters for the collection for 'Cards'. These settings will be taken into account
		/// when the property Cards is requested or GetMultiCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cards.SortClauses=sortClauses;
			_cards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiParticipantCards(bool forceFetch)
		{
			return GetMultiParticipantCards(forceFetch, _participantCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiParticipantCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParticipantCards(forceFetch, _participantCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiParticipantCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParticipantCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiParticipantCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParticipantCards || forceFetch || _alwaysFetchParticipantCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_participantCards);
				_participantCards.SuppressClearInGetMulti=!forceFetch;
				_participantCards.EntityFactoryToUse = entityFactoryToUse;
				_participantCards.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_participantCards.SuppressClearInGetMulti=false;
				_alreadyFetchedParticipantCards = true;
			}
			return _participantCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParticipantCards'. These settings will be taken into account
		/// when the property ParticipantCards is requested or GetMultiParticipantCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParticipantCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_participantCards.SortClauses=sortClauses;
			_participantCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, _orderDetailToCardHolder.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailToCardHolderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, _orderDetailToCardHolder.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetailToCardHolder(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection GetMultiOrderDetailToCardHolder(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetailToCardHolder || forceFetch || _alwaysFetchOrderDetailToCardHolder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetailToCardHolder);
				_orderDetailToCardHolder.SuppressClearInGetMulti=!forceFetch;
				_orderDetailToCardHolder.EntityFactoryToUse = entityFactoryToUse;
				_orderDetailToCardHolder.GetMultiManyToOne(this, null, filter);
				_orderDetailToCardHolder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetailToCardHolder = true;
			}
			return _orderDetailToCardHolder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetailToCardHolder'. These settings will be taken into account
		/// when the property OrderDetailToCardHolder is requested or GetMultiOrderDetailToCardHolder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetailToCardHolder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetailToCardHolder.SortClauses=sortClauses;
			_orderDetailToCardHolder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderDetails || forceFetch || _alwaysFetchOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetails);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CardHolderFields.PersonID, ComparisonOperator.Equal, this.PersonID, "CardHolderEntity__"));
				_orderDetails.SuppressClearInGetMulti=!forceFetch;
				_orderDetails.EntityFactoryToUse = entityFactoryToUse;
				_orderDetails.GetMulti(filter, GetRelationsForField("OrderDetails"));
				_orderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetails = true;
			}
			return _orderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetails'. These settings will be taken into account
		/// when the property OrderDetails is requested or GetMultiOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetails.SortClauses=sortClauses;
			_orderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public SchoolYearEntity GetSingleSchoolYear()
		{
			return GetSingleSchoolYear(false);
		}

		/// <summary> Retrieves the related entity of type 'SchoolYearEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SchoolYearEntity' which is related to this entity.</returns>
		public virtual SchoolYearEntity GetSingleSchoolYear(bool forceFetch)
		{
			if( ( !_alreadyFetchedSchoolYear || forceFetch || _alwaysFetchSchoolYear) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SchoolYearEntityUsingSchoolYearID);
				SchoolYearEntity newEntity = new SchoolYearEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SchoolYearID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SchoolYearEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_schoolYearReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SchoolYear = newEntity;
				_alreadyFetchedSchoolYear = fetchResult;
			}
			return _schoolYear;
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleAlternativeAddress()
		{
			return GetSingleAlternativeAddress(false);
		}
		
		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleAlternativeAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAlternativeAddress || forceFetch || _alwaysFetchAlternativeAddress) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingAlternativeAdressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AlternativeAdressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_alternativeAddressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AlternativeAddress = newEntity;
				_alreadyFetchedAlternativeAddress = fetchResult;
			}
			return _alternativeAddress;
		}

		/// <summary> Retrieves the related entity of type 'CardHolderOrganizationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'CardHolderOrganizationEntity' which is related to this entity.</returns>
		public CardHolderOrganizationEntity GetSingleCardHolderOrganization()
		{
			return GetSingleCardHolderOrganization(false);
		}
		
		/// <summary> Retrieves the related entity of type 'CardHolderOrganizationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardHolderOrganizationEntity' which is related to this entity.</returns>
		public virtual CardHolderOrganizationEntity GetSingleCardHolderOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardHolderOrganization || forceFetch || _alwaysFetchCardHolderOrganization) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardHolderOrganizationEntityUsingCardHolderID);
				CardHolderOrganizationEntity newEntity = new CardHolderOrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCCardHolderID(this.PersonID);
				}
				if(fetchResult)
				{
					newEntity = (CardHolderOrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardHolderOrganizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardHolderOrganization = newEntity;
				_alreadyFetchedCardHolderOrganization = fetchResult;
			}
			return _cardHolderOrganization;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = base.GetRelatedData();
			toReturn.Add("Contract", _contract);
			toReturn.Add("SchoolYear", _schoolYear);
			toReturn.Add("Cards", _cards);
			toReturn.Add("ParticipantCards", _participantCards);
			toReturn.Add("OrderDetailToCardHolder", _orderDetailToCardHolder);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("OrderDetails", _orderDetails);
			toReturn.Add("AlternativeAddress", _alternativeAddress);
			toReturn.Add("CardHolderOrganization", _cardHolderOrganization);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			InitClassMembers();
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)CardHolderFieldIndex.TypeDiscriminator, "CardHolder");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="personID">PK value for CardHolder which data should be fetched into this CardHolder object</param>
		/// <param name="validator">The validator object for this CardHolderEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 personID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			InitClassMembers();	
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)CardHolderFieldIndex.TypeDiscriminator, "CardHolder");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_cards.SetContainingEntityInfo(this, "CardHolder");

			_participantCards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_participantCards.SetContainingEntityInfo(this, "Participant");

			_orderDetailToCardHolder = new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection();
			_orderDetailToCardHolder.SetContainingEntityInfo(this, "CardHolder");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "CardHolder");
			_orderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_contractReturnsNewIfNotFound = false;
			_schoolYearReturnsNewIfNotFound = false;
			_alternativeAddressReturnsNewIfNotFound = false;
			_cardHolderOrganizationReturnsNewIfNotFound = false;

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END

		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlternativeAdressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentAlarm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommentText5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Grade", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Municipality", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonalAssistent", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserGroupExpiry", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "CardHolders", resetFKFields, new int[] { (int)CardHolderFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _schoolYear</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSchoolYear(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, signalRelatedEntity, "CardHolder", resetFKFields, new int[] { (int)CardHolderFieldIndex.SchoolYearID } );		
			_schoolYear = null;
		}
		
		/// <summary> setups the sync logic for member _schoolYear</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSchoolYear(IEntityCore relatedEntity)
		{
			if(_schoolYear!=relatedEntity)
			{		
				DesetupSyncSchoolYear(true, true);
				_schoolYear = (SchoolYearEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _schoolYear, new PropertyChangedEventHandler( OnSchoolYearPropertyChanged ), "SchoolYear", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.SchoolYearEntityUsingSchoolYearIDStatic, true, ref _alreadyFetchedSchoolYear, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSchoolYearPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _alternativeAddress</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAlternativeAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _alternativeAddress, new PropertyChangedEventHandler( OnAlternativeAddressPropertyChanged ), "AlternativeAddress", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.AddressEntityUsingAlternativeAdressIDStatic, true, signalRelatedEntity, "CardHolder", resetFKFields, new int[] { (int)CardHolderFieldIndex.AlternativeAdressID } );
			_alternativeAddress = null;
		}
	
		/// <summary> setups the sync logic for member _alternativeAddress</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAlternativeAddress(IEntityCore relatedEntity)
		{
			if(_alternativeAddress!=relatedEntity)
			{
				DesetupSyncAlternativeAddress(true, true);
				_alternativeAddress = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _alternativeAddress, new PropertyChangedEventHandler( OnAlternativeAddressPropertyChanged ), "AlternativeAddress", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.AddressEntityUsingAlternativeAdressIDStatic, true, ref _alreadyFetchedAlternativeAddress, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAlternativeAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _cardHolderOrganization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardHolderOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardHolderOrganization, new PropertyChangedEventHandler( OnCardHolderOrganizationPropertyChanged ), "CardHolderOrganization", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.CardHolderOrganizationEntityUsingCardHolderIDStatic, false, signalRelatedEntity, "CardHolder", false, new int[] { (int)CardHolderFieldIndex.PersonID } );
			_cardHolderOrganization = null;
		}
	
		/// <summary> setups the sync logic for member _cardHolderOrganization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardHolderOrganization(IEntityCore relatedEntity)
		{
			if(_cardHolderOrganization!=relatedEntity)
			{
				DesetupSyncCardHolderOrganization(true, true);
				_cardHolderOrganization = (CardHolderOrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardHolderOrganization, new PropertyChangedEventHandler( OnCardHolderOrganizationPropertyChanged ), "CardHolderOrganization", VarioSL.Entities.RelationClasses.StaticCardHolderRelations.CardHolderOrganizationEntityUsingCardHolderIDStatic, false, ref _alreadyFetchedCardHolderOrganization, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardHolderOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardHolderDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardHolderEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public new static CardHolderRelations Relations
		{
			get	{ return new CardHolderRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public new static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Cards")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Cards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParticipantCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("ParticipantCards")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "ParticipantCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetailToCardHolder' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetailToCardHolder
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection(), (IEntityRelation)GetRelationsForField("OrderDetailToCardHolder")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity, 0, null, null, null, "OrderDetailToCardHolder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetails
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderDetailToCardHolderEntityUsingCardHolderID;
				intermediateRelation.SetAliases(string.Empty, "OrderDetailToCardHolder_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, GetRelationsForField("OrderDetails"), "OrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SchoolYear'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSchoolYear
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SchoolYearCollection(), (IEntityRelation)GetRelationsForField("SchoolYear")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.SchoolYearEntity, 0, null, null, null, "SchoolYear", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAlternativeAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("AlternativeAddress")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "AlternativeAddress", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardHolderOrganization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardHolderOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardHolderOrganizationCollection(), (IEntityRelation)GetRelationsForField("CardHolderOrganization")[0], (int)VarioSL.Entities.EntityType.CardHolderEntity, (int)VarioSL.Entities.EntityType.CardHolderOrganizationEntity, 0, null, null, null, "CardHolderOrganization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public new static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AlternativeAdressID property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ALTERNATIVEADRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AlternativeAdressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardHolderFieldIndex.AlternativeAdressID, false); }
			set	{ SetValue((int)CardHolderFieldIndex.AlternativeAdressID, value, true); }
		}

		/// <summary> The CommentAlarm property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTALARM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CommentAlarm
		{
			get { return (System.Boolean)GetValue((int)CardHolderFieldIndex.CommentAlarm, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentAlarm, value, true); }
		}

		/// <summary> The CommentText property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.CommentText, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentText, value, true); }
		}

		/// <summary> The CommentText2 property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTTEXT2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText2
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.CommentText2, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentText2, value, true); }
		}

		/// <summary> The CommentText3 property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTTEXT3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText3
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.CommentText3, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentText3, value, true); }
		}

		/// <summary> The CommentText4 property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTTEXT4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText4
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.CommentText4, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentText4, value, true); }
		}

		/// <summary> The CommentText5 property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."COMMENTTEXT5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CommentText5
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.CommentText5, true); }
			set	{ SetValue((int)CardHolderFieldIndex.CommentText5, value, true); }
		}

		/// <summary> The ContractID property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardHolderFieldIndex.ContractID, false); }
			set	{ SetValue((int)CardHolderFieldIndex.ContractID, value, true); }
		}

		/// <summary> The Grade property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."GRADE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Grade
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.Grade, true); }
			set	{ SetValue((int)CardHolderFieldIndex.Grade, value, true); }
		}

		/// <summary> The Municipality property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."MUNICIPALITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Municipality
		{
			get { return (System.String)GetValue((int)CardHolderFieldIndex.Municipality, true); }
			set	{ SetValue((int)CardHolderFieldIndex.Municipality, value, true); }
		}

		/// <summary> The PersonalAssistent property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."PERSONALASSISTENT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean PersonalAssistent
		{
			get { return (System.Boolean)GetValue((int)CardHolderFieldIndex.PersonalAssistent, true); }
			set	{ SetValue((int)CardHolderFieldIndex.PersonalAssistent, value, true); }
		}

		/// <summary> The SchoolYearID property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."SCHOOLYEARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SchoolYearID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CardHolderFieldIndex.SchoolYearID, false); }
			set	{ SetValue((int)CardHolderFieldIndex.SchoolYearID, value, true); }
		}

		/// <summary> The UserGroup property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."USERGROUP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UserGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)CardHolderFieldIndex.UserGroup, false); }
			set	{ SetValue((int)CardHolderFieldIndex.UserGroup, value, true); }
		}

		/// <summary> The UserGroupExpiry property of the Entity CardHolder<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."USERGROUPEXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UserGroupExpiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CardHolderFieldIndex.UserGroupExpiry, false); }
			set	{ SetValue((int)CardHolderFieldIndex.UserGroupExpiry, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection Cards
		{
			get	{ return GetMultiCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Cards. When set to true, Cards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Cards is accessed. You can always execute/ a forced fetch by calling GetMultiCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCards
		{
			get	{ return _alwaysFetchCards; }
			set	{ _alwaysFetchCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Cards already has been fetched. Setting this property to false when Cards has been fetched
		/// will clear the Cards collection well. Setting this property to true while Cards hasn't been fetched disables lazy loading for Cards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCards
		{
			get { return _alreadyFetchedCards;}
			set 
			{
				if(_alreadyFetchedCards && !value && (_cards != null))
				{
					_cards.Clear();
				}
				_alreadyFetchedCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParticipantCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection ParticipantCards
		{
			get	{ return GetMultiParticipantCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParticipantCards. When set to true, ParticipantCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParticipantCards is accessed. You can always execute/ a forced fetch by calling GetMultiParticipantCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParticipantCards
		{
			get	{ return _alwaysFetchParticipantCards; }
			set	{ _alwaysFetchParticipantCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParticipantCards already has been fetched. Setting this property to false when ParticipantCards has been fetched
		/// will clear the ParticipantCards collection well. Setting this property to true while ParticipantCards hasn't been fetched disables lazy loading for ParticipantCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParticipantCards
		{
			get { return _alreadyFetchedParticipantCards;}
			set 
			{
				if(_alreadyFetchedParticipantCards && !value && (_participantCards != null))
				{
					_participantCards.Clear();
				}
				_alreadyFetchedParticipantCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailToCardHolderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetailToCardHolder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailToCardHolderCollection OrderDetailToCardHolder
		{
			get	{ return GetMultiOrderDetailToCardHolder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetailToCardHolder. When set to true, OrderDetailToCardHolder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetailToCardHolder is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetailToCardHolder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetailToCardHolder
		{
			get	{ return _alwaysFetchOrderDetailToCardHolder; }
			set	{ _alwaysFetchOrderDetailToCardHolder = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetailToCardHolder already has been fetched. Setting this property to false when OrderDetailToCardHolder has been fetched
		/// will clear the OrderDetailToCardHolder collection well. Setting this property to true while OrderDetailToCardHolder hasn't been fetched disables lazy loading for OrderDetailToCardHolder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetailToCardHolder
		{
			get { return _alreadyFetchedOrderDetailToCardHolder;}
			set 
			{
				if(_alreadyFetchedOrderDetailToCardHolder && !value && (_orderDetailToCardHolder != null))
				{
					_orderDetailToCardHolder.Clear();
				}
				_alreadyFetchedOrderDetailToCardHolder = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection OrderDetails
		{
			get { return GetMultiOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetails. When set to true, OrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetails is accessed. You can always execute a forced fetch by calling GetMultiOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetails
		{
			get	{ return _alwaysFetchOrderDetails; }
			set	{ _alwaysFetchOrderDetails = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetails already has been fetched. Setting this property to false when OrderDetails has been fetched
		/// will clear the OrderDetails collection well. Setting this property to true while OrderDetails hasn't been fetched disables lazy loading for OrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetails
		{
			get { return _alreadyFetchedOrderDetails;}
			set 
			{
				if(_alreadyFetchedOrderDetails && !value && (_orderDetails != null))
				{
					_orderDetails.Clear();
				}
				_alreadyFetchedOrderDetails = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardHolders", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SchoolYearEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSchoolYear()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SchoolYearEntity SchoolYear
		{
			get	{ return GetSingleSchoolYear(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSchoolYear(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardHolder", "SchoolYear", _schoolYear, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SchoolYear. When set to true, SchoolYear is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SchoolYear is accessed. You can always execute a forced fetch by calling GetSingleSchoolYear(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSchoolYear
		{
			get	{ return _alwaysFetchSchoolYear; }
			set	{ _alwaysFetchSchoolYear = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SchoolYear already has been fetched. Setting this property to false when SchoolYear has been fetched
		/// will set SchoolYear to null as well. Setting this property to true while SchoolYear hasn't been fetched disables lazy loading for SchoolYear</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSchoolYear
		{
			get { return _alreadyFetchedSchoolYear;}
			set 
			{
				if(_alreadyFetchedSchoolYear && !value)
				{
					this.SchoolYear = null;
				}
				_alreadyFetchedSchoolYear = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SchoolYear is not found
		/// in the database. When set to true, SchoolYear will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SchoolYearReturnsNewIfNotFound
		{
			get	{ return _schoolYearReturnsNewIfNotFound; }
			set { _schoolYearReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAlternativeAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity AlternativeAddress
		{
			get	{ return GetSingleAlternativeAddress(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAlternativeAddress(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_alternativeAddress !=null);
						DesetupSyncAlternativeAddress(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("AlternativeAddress");
						}
					}
					else
					{
						if(_alternativeAddress!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CardHolder");
							SetupSyncAlternativeAddress(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AlternativeAddress. When set to true, AlternativeAddress is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AlternativeAddress is accessed. You can always execute a forced fetch by calling GetSingleAlternativeAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAlternativeAddress
		{
			get	{ return _alwaysFetchAlternativeAddress; }
			set	{ _alwaysFetchAlternativeAddress = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property AlternativeAddress already has been fetched. Setting this property to false when AlternativeAddress has been fetched
		/// will set AlternativeAddress to null as well. Setting this property to true while AlternativeAddress hasn't been fetched disables lazy loading for AlternativeAddress</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAlternativeAddress
		{
			get { return _alreadyFetchedAlternativeAddress;}
			set 
			{
				if(_alreadyFetchedAlternativeAddress && !value)
				{
					this.AlternativeAddress = null;
				}
				_alreadyFetchedAlternativeAddress = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AlternativeAddress is not found
		/// in the database. When set to true, AlternativeAddress will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AlternativeAddressReturnsNewIfNotFound
		{
			get	{ return _alternativeAddressReturnsNewIfNotFound; }
			set	{ _alternativeAddressReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'CardHolderOrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardHolderOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardHolderOrganizationEntity CardHolderOrganization
		{
			get	{ return GetSingleCardHolderOrganization(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncCardHolderOrganization(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_cardHolderOrganization !=null);
						DesetupSyncCardHolderOrganization(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("CardHolderOrganization");
						}
					}
					else
					{
						if(_cardHolderOrganization!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "CardHolder");
							SetupSyncCardHolderOrganization(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardHolderOrganization. When set to true, CardHolderOrganization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardHolderOrganization is accessed. You can always execute a forced fetch by calling GetSingleCardHolderOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardHolderOrganization
		{
			get	{ return _alwaysFetchCardHolderOrganization; }
			set	{ _alwaysFetchCardHolderOrganization = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property CardHolderOrganization already has been fetched. Setting this property to false when CardHolderOrganization has been fetched
		/// will set CardHolderOrganization to null as well. Setting this property to true while CardHolderOrganization hasn't been fetched disables lazy loading for CardHolderOrganization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardHolderOrganization
		{
			get { return _alreadyFetchedCardHolderOrganization;}
			set 
			{
				if(_alreadyFetchedCardHolderOrganization && !value)
				{
					this.CardHolderOrganization = null;
				}
				_alreadyFetchedCardHolderOrganization = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardHolderOrganization is not found
		/// in the database. When set to true, CardHolderOrganization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardHolderOrganizationReturnsNewIfNotFound
		{
			get	{ return _cardHolderOrganizationReturnsNewIfNotFound; }
			set	{ _cardHolderOrganizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return true;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.TargetPerEntityHierarchy;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardHolderEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
