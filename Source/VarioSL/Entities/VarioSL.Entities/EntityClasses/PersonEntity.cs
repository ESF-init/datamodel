﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Person'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PersonEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.BankConnectionDataCollection	_bankConnectionDatas;
		private bool	_alwaysFetchBankConnectionDatas, _alreadyFetchedBankConnectionDatas;
		private VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection	_customAttributeValueToPeople;
		private bool	_alwaysFetchCustomAttributeValueToPeople, _alreadyFetchedCustomAttributeValueToPeople;
		private VarioSL.Entities.CollectionClasses.CustomerAccountCollection	_customerAccounts;
		private bool	_alwaysFetchCustomerAccounts, _alreadyFetchedCustomerAccounts;
		private VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection	_emailEventResendLocks;
		private bool	_alwaysFetchEmailEventResendLocks, _alreadyFetchedEmailEventResendLocks;
		private VarioSL.Entities.CollectionClasses.EntitlementCollection	_entitlements;
		private bool	_alwaysFetchEntitlements, _alreadyFetchedEntitlements;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidentsAsGuardian;
		private bool	_alwaysFetchFareEvasionIncidentsAsGuardian, _alreadyFetchedFareEvasionIncidentsAsGuardian;
		private VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection	_fareEvasionIncidentsAsPerson;
		private bool	_alwaysFetchFareEvasionIncidentsAsPerson, _alreadyFetchedFareEvasionIncidentsAsPerson;
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.PersonAddressCollection	_personAddresses;
		private bool	_alwaysFetchPersonAddresses, _alreadyFetchedPersonAddresses;
		private VarioSL.Entities.CollectionClasses.PhotographCollection	_photographs;
		private bool	_alwaysFetchPhotographs, _alreadyFetchedPhotographs;
		private VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection	_recipientGroupMembers;
		private bool	_alwaysFetchRecipientGroupMembers, _alreadyFetchedRecipientGroupMembers;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.SurveyResponseCollection	_surveyResponses;
		private bool	_alwaysFetchSurveyResponses, _alreadyFetchedSurveyResponses;
		private VarioSL.Entities.CollectionClasses.AddressCollection _addresses;
		private bool	_alwaysFetchAddresses, _alreadyFetchedAddresses;
		private AddressEntity _address;
		private bool	_alwaysFetchAddress, _alreadyFetchedAddress, _addressReturnsNewIfNotFound;
		private OrganizationEntity _contactPersonOnOrganization;
		private bool	_alwaysFetchContactPersonOnOrganization, _alreadyFetchedContactPersonOnOrganization, _contactPersonOnOrganizationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BankConnectionDatas</summary>
			public static readonly string BankConnectionDatas = "BankConnectionDatas";
			/// <summary>Member name CustomAttributeValueToPeople</summary>
			public static readonly string CustomAttributeValueToPeople = "CustomAttributeValueToPeople";
			/// <summary>Member name CustomerAccounts</summary>
			public static readonly string CustomerAccounts = "CustomerAccounts";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
			/// <summary>Member name Entitlements</summary>
			public static readonly string Entitlements = "Entitlements";
			/// <summary>Member name FareEvasionIncidentsAsGuardian</summary>
			public static readonly string FareEvasionIncidentsAsGuardian = "FareEvasionIncidentsAsGuardian";
			/// <summary>Member name FareEvasionIncidentsAsPerson</summary>
			public static readonly string FareEvasionIncidentsAsPerson = "FareEvasionIncidentsAsPerson";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name PersonAddresses</summary>
			public static readonly string PersonAddresses = "PersonAddresses";
			/// <summary>Member name Photographs</summary>
			public static readonly string Photographs = "Photographs";
			/// <summary>Member name RecipientGroupMembers</summary>
			public static readonly string RecipientGroupMembers = "RecipientGroupMembers";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name SurveyResponses</summary>
			public static readonly string SurveyResponses = "SurveyResponses";
			/// <summary>Member name Addresses</summary>
			public static readonly string Addresses = "Addresses";
			/// <summary>Member name Address</summary>
			public static readonly string Address = "Address";
			/// <summary>Member name ContactPersonOnOrganization</summary>
			public static readonly string ContactPersonOnOrganization = "ContactPersonOnOrganization";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PersonEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PersonEntity() :base("PersonEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		public PersonEntity(System.Int64 personID):base("PersonEntity")
		{
			InitClassFetch(personID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PersonEntity(System.Int64 personID, IPrefetchPath prefetchPathToUse):base("PersonEntity")
		{
			InitClassFetch(personID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="validator">The custom validator object for this PersonEntity</param>
		public PersonEntity(System.Int64 personID, IValidator validator):base("PersonEntity")
		{
			InitClassFetch(personID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PersonEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_bankConnectionDatas = (VarioSL.Entities.CollectionClasses.BankConnectionDataCollection)info.GetValue("_bankConnectionDatas", typeof(VarioSL.Entities.CollectionClasses.BankConnectionDataCollection));
			_alwaysFetchBankConnectionDatas = info.GetBoolean("_alwaysFetchBankConnectionDatas");
			_alreadyFetchedBankConnectionDatas = info.GetBoolean("_alreadyFetchedBankConnectionDatas");

			_customAttributeValueToPeople = (VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection)info.GetValue("_customAttributeValueToPeople", typeof(VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection));
			_alwaysFetchCustomAttributeValueToPeople = info.GetBoolean("_alwaysFetchCustomAttributeValueToPeople");
			_alreadyFetchedCustomAttributeValueToPeople = info.GetBoolean("_alreadyFetchedCustomAttributeValueToPeople");

			_customerAccounts = (VarioSL.Entities.CollectionClasses.CustomerAccountCollection)info.GetValue("_customerAccounts", typeof(VarioSL.Entities.CollectionClasses.CustomerAccountCollection));
			_alwaysFetchCustomerAccounts = info.GetBoolean("_alwaysFetchCustomerAccounts");
			_alreadyFetchedCustomerAccounts = info.GetBoolean("_alreadyFetchedCustomerAccounts");

			_emailEventResendLocks = (VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection)info.GetValue("_emailEventResendLocks", typeof(VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection));
			_alwaysFetchEmailEventResendLocks = info.GetBoolean("_alwaysFetchEmailEventResendLocks");
			_alreadyFetchedEmailEventResendLocks = info.GetBoolean("_alreadyFetchedEmailEventResendLocks");

			_entitlements = (VarioSL.Entities.CollectionClasses.EntitlementCollection)info.GetValue("_entitlements", typeof(VarioSL.Entities.CollectionClasses.EntitlementCollection));
			_alwaysFetchEntitlements = info.GetBoolean("_alwaysFetchEntitlements");
			_alreadyFetchedEntitlements = info.GetBoolean("_alreadyFetchedEntitlements");

			_fareEvasionIncidentsAsGuardian = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidentsAsGuardian", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidentsAsGuardian = info.GetBoolean("_alwaysFetchFareEvasionIncidentsAsGuardian");
			_alreadyFetchedFareEvasionIncidentsAsGuardian = info.GetBoolean("_alreadyFetchedFareEvasionIncidentsAsGuardian");

			_fareEvasionIncidentsAsPerson = (VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection)info.GetValue("_fareEvasionIncidentsAsPerson", typeof(VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection));
			_alwaysFetchFareEvasionIncidentsAsPerson = info.GetBoolean("_alwaysFetchFareEvasionIncidentsAsPerson");
			_alreadyFetchedFareEvasionIncidentsAsPerson = info.GetBoolean("_alreadyFetchedFareEvasionIncidentsAsPerson");

			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_personAddresses = (VarioSL.Entities.CollectionClasses.PersonAddressCollection)info.GetValue("_personAddresses", typeof(VarioSL.Entities.CollectionClasses.PersonAddressCollection));
			_alwaysFetchPersonAddresses = info.GetBoolean("_alwaysFetchPersonAddresses");
			_alreadyFetchedPersonAddresses = info.GetBoolean("_alreadyFetchedPersonAddresses");

			_photographs = (VarioSL.Entities.CollectionClasses.PhotographCollection)info.GetValue("_photographs", typeof(VarioSL.Entities.CollectionClasses.PhotographCollection));
			_alwaysFetchPhotographs = info.GetBoolean("_alwaysFetchPhotographs");
			_alreadyFetchedPhotographs = info.GetBoolean("_alreadyFetchedPhotographs");

			_recipientGroupMembers = (VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection)info.GetValue("_recipientGroupMembers", typeof(VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection));
			_alwaysFetchRecipientGroupMembers = info.GetBoolean("_alwaysFetchRecipientGroupMembers");
			_alreadyFetchedRecipientGroupMembers = info.GetBoolean("_alreadyFetchedRecipientGroupMembers");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_surveyResponses = (VarioSL.Entities.CollectionClasses.SurveyResponseCollection)info.GetValue("_surveyResponses", typeof(VarioSL.Entities.CollectionClasses.SurveyResponseCollection));
			_alwaysFetchSurveyResponses = info.GetBoolean("_alwaysFetchSurveyResponses");
			_alreadyFetchedSurveyResponses = info.GetBoolean("_alreadyFetchedSurveyResponses");
			_addresses = (VarioSL.Entities.CollectionClasses.AddressCollection)info.GetValue("_addresses", typeof(VarioSL.Entities.CollectionClasses.AddressCollection));
			_alwaysFetchAddresses = info.GetBoolean("_alwaysFetchAddresses");
			_alreadyFetchedAddresses = info.GetBoolean("_alreadyFetchedAddresses");
			_address = (AddressEntity)info.GetValue("_address", typeof(AddressEntity));
			if(_address!=null)
			{
				_address.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_addressReturnsNewIfNotFound = info.GetBoolean("_addressReturnsNewIfNotFound");
			_alwaysFetchAddress = info.GetBoolean("_alwaysFetchAddress");
			_alreadyFetchedAddress = info.GetBoolean("_alreadyFetchedAddress");

			_contactPersonOnOrganization = (OrganizationEntity)info.GetValue("_contactPersonOnOrganization", typeof(OrganizationEntity));
			if(_contactPersonOnOrganization!=null)
			{
				_contactPersonOnOrganization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contactPersonOnOrganizationReturnsNewIfNotFound = info.GetBoolean("_contactPersonOnOrganizationReturnsNewIfNotFound");
			_alwaysFetchContactPersonOnOrganization = info.GetBoolean("_alwaysFetchContactPersonOnOrganization");
			_alreadyFetchedContactPersonOnOrganization = info.GetBoolean("_alreadyFetchedContactPersonOnOrganization");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PersonFieldIndex)fieldIndex)
			{
				case PersonFieldIndex.AddressID:
					DesetupSyncAddress(true, false);
					_alreadyFetchedAddress = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBankConnectionDatas = (_bankConnectionDatas.Count > 0);
			_alreadyFetchedCustomAttributeValueToPeople = (_customAttributeValueToPeople.Count > 0);
			_alreadyFetchedCustomerAccounts = (_customerAccounts.Count > 0);
			_alreadyFetchedEmailEventResendLocks = (_emailEventResendLocks.Count > 0);
			_alreadyFetchedEntitlements = (_entitlements.Count > 0);
			_alreadyFetchedFareEvasionIncidentsAsGuardian = (_fareEvasionIncidentsAsGuardian.Count > 0);
			_alreadyFetchedFareEvasionIncidentsAsPerson = (_fareEvasionIncidentsAsPerson.Count > 0);
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedPersonAddresses = (_personAddresses.Count > 0);
			_alreadyFetchedPhotographs = (_photographs.Count > 0);
			_alreadyFetchedRecipientGroupMembers = (_recipientGroupMembers.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedSurveyResponses = (_surveyResponses.Count > 0);
			_alreadyFetchedAddresses = (_addresses.Count > 0);
			_alreadyFetchedAddress = (_address != null);
			_alreadyFetchedContactPersonOnOrganization = (_contactPersonOnOrganization != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BankConnectionDatas":
					toReturn.Add(Relations.BankConnectionDataEntityUsingAccountOwnerID);
					break;
				case "CustomAttributeValueToPeople":
					toReturn.Add(Relations.CustomAttributeValueToPersonEntityUsingPersonID);
					break;
				case "CustomerAccounts":
					toReturn.Add(Relations.CustomerAccountEntityUsingPersonID);
					break;
				case "EmailEventResendLocks":
					toReturn.Add(Relations.EmailEventResendLockEntityUsingPersonID);
					break;
				case "Entitlements":
					toReturn.Add(Relations.EntitlementEntityUsingPersonID);
					break;
				case "FareEvasionIncidentsAsGuardian":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingGuardianID);
					break;
				case "FareEvasionIncidentsAsPerson":
					toReturn.Add(Relations.FareEvasionIncidentEntityUsingPersonID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingPersonID);
					break;
				case "PersonAddresses":
					toReturn.Add(Relations.PersonAddressEntityUsingPersonID);
					break;
				case "Photographs":
					toReturn.Add(Relations.PhotographEntityUsingPersonID);
					break;
				case "RecipientGroupMembers":
					toReturn.Add(Relations.RecipientGroupMemberEntityUsingPersonID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingPersonID);
					break;
				case "SurveyResponses":
					toReturn.Add(Relations.SurveyResponseEntityUsingPersonID);
					break;
				case "Addresses":
					toReturn.Add(Relations.PersonAddressEntityUsingPersonID, "PersonEntity__", "PersonAddress_", JoinHint.None);
					toReturn.Add(PersonAddressEntity.Relations.AddressEntityUsingAddressID, "PersonAddress_", string.Empty, JoinHint.None);
					break;
				case "Address":
					toReturn.Add(Relations.AddressEntityUsingAddressID);
					break;
				case "ContactPersonOnOrganization":
					toReturn.Add(Relations.OrganizationEntityUsingContactPersonID);
					break;
				default:
					break;				
			}
			return toReturn;
		}

		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter()
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("PersonEntity", false);
		}
		
		/// <summary>Gets a predicateexpression which filters on this entity</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <returns>ready to use predicateexpression</returns>
		/// <remarks>Only useful in entity fetches.</remarks>
		public  static IPredicateExpression GetEntityTypeFilter(bool negate)
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter("PersonEntity", negate);
		}

		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_bankConnectionDatas", (!this.MarkedForDeletion?_bankConnectionDatas:null));
			info.AddValue("_alwaysFetchBankConnectionDatas", _alwaysFetchBankConnectionDatas);
			info.AddValue("_alreadyFetchedBankConnectionDatas", _alreadyFetchedBankConnectionDatas);
			info.AddValue("_customAttributeValueToPeople", (!this.MarkedForDeletion?_customAttributeValueToPeople:null));
			info.AddValue("_alwaysFetchCustomAttributeValueToPeople", _alwaysFetchCustomAttributeValueToPeople);
			info.AddValue("_alreadyFetchedCustomAttributeValueToPeople", _alreadyFetchedCustomAttributeValueToPeople);
			info.AddValue("_customerAccounts", (!this.MarkedForDeletion?_customerAccounts:null));
			info.AddValue("_alwaysFetchCustomerAccounts", _alwaysFetchCustomerAccounts);
			info.AddValue("_alreadyFetchedCustomerAccounts", _alreadyFetchedCustomerAccounts);
			info.AddValue("_emailEventResendLocks", (!this.MarkedForDeletion?_emailEventResendLocks:null));
			info.AddValue("_alwaysFetchEmailEventResendLocks", _alwaysFetchEmailEventResendLocks);
			info.AddValue("_alreadyFetchedEmailEventResendLocks", _alreadyFetchedEmailEventResendLocks);
			info.AddValue("_entitlements", (!this.MarkedForDeletion?_entitlements:null));
			info.AddValue("_alwaysFetchEntitlements", _alwaysFetchEntitlements);
			info.AddValue("_alreadyFetchedEntitlements", _alreadyFetchedEntitlements);
			info.AddValue("_fareEvasionIncidentsAsGuardian", (!this.MarkedForDeletion?_fareEvasionIncidentsAsGuardian:null));
			info.AddValue("_alwaysFetchFareEvasionIncidentsAsGuardian", _alwaysFetchFareEvasionIncidentsAsGuardian);
			info.AddValue("_alreadyFetchedFareEvasionIncidentsAsGuardian", _alreadyFetchedFareEvasionIncidentsAsGuardian);
			info.AddValue("_fareEvasionIncidentsAsPerson", (!this.MarkedForDeletion?_fareEvasionIncidentsAsPerson:null));
			info.AddValue("_alwaysFetchFareEvasionIncidentsAsPerson", _alwaysFetchFareEvasionIncidentsAsPerson);
			info.AddValue("_alreadyFetchedFareEvasionIncidentsAsPerson", _alreadyFetchedFareEvasionIncidentsAsPerson);
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_personAddresses", (!this.MarkedForDeletion?_personAddresses:null));
			info.AddValue("_alwaysFetchPersonAddresses", _alwaysFetchPersonAddresses);
			info.AddValue("_alreadyFetchedPersonAddresses", _alreadyFetchedPersonAddresses);
			info.AddValue("_photographs", (!this.MarkedForDeletion?_photographs:null));
			info.AddValue("_alwaysFetchPhotographs", _alwaysFetchPhotographs);
			info.AddValue("_alreadyFetchedPhotographs", _alreadyFetchedPhotographs);
			info.AddValue("_recipientGroupMembers", (!this.MarkedForDeletion?_recipientGroupMembers:null));
			info.AddValue("_alwaysFetchRecipientGroupMembers", _alwaysFetchRecipientGroupMembers);
			info.AddValue("_alreadyFetchedRecipientGroupMembers", _alreadyFetchedRecipientGroupMembers);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_surveyResponses", (!this.MarkedForDeletion?_surveyResponses:null));
			info.AddValue("_alwaysFetchSurveyResponses", _alwaysFetchSurveyResponses);
			info.AddValue("_alreadyFetchedSurveyResponses", _alreadyFetchedSurveyResponses);
			info.AddValue("_addresses", (!this.MarkedForDeletion?_addresses:null));
			info.AddValue("_alwaysFetchAddresses", _alwaysFetchAddresses);
			info.AddValue("_alreadyFetchedAddresses", _alreadyFetchedAddresses);

			info.AddValue("_address", (!this.MarkedForDeletion?_address:null));
			info.AddValue("_addressReturnsNewIfNotFound", _addressReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAddress", _alwaysFetchAddress);
			info.AddValue("_alreadyFetchedAddress", _alreadyFetchedAddress);

			info.AddValue("_contactPersonOnOrganization", (!this.MarkedForDeletion?_contactPersonOnOrganization:null));
			info.AddValue("_contactPersonOnOrganizationReturnsNewIfNotFound", _contactPersonOnOrganizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContactPersonOnOrganization", _alwaysFetchContactPersonOnOrganization);
			info.AddValue("_alreadyFetchedContactPersonOnOrganization", _alreadyFetchedContactPersonOnOrganization);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BankConnectionDatas":
					_alreadyFetchedBankConnectionDatas = true;
					if(entity!=null)
					{
						this.BankConnectionDatas.Add((BankConnectionDataEntity)entity);
					}
					break;
				case "CustomAttributeValueToPeople":
					_alreadyFetchedCustomAttributeValueToPeople = true;
					if(entity!=null)
					{
						this.CustomAttributeValueToPeople.Add((CustomAttributeValueToPersonEntity)entity);
					}
					break;
				case "CustomerAccounts":
					_alreadyFetchedCustomerAccounts = true;
					if(entity!=null)
					{
						this.CustomerAccounts.Add((CustomerAccountEntity)entity);
					}
					break;
				case "EmailEventResendLocks":
					_alreadyFetchedEmailEventResendLocks = true;
					if(entity!=null)
					{
						this.EmailEventResendLocks.Add((EmailEventResendLockEntity)entity);
					}
					break;
				case "Entitlements":
					_alreadyFetchedEntitlements = true;
					if(entity!=null)
					{
						this.Entitlements.Add((EntitlementEntity)entity);
					}
					break;
				case "FareEvasionIncidentsAsGuardian":
					_alreadyFetchedFareEvasionIncidentsAsGuardian = true;
					if(entity!=null)
					{
						this.FareEvasionIncidentsAsGuardian.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "FareEvasionIncidentsAsPerson":
					_alreadyFetchedFareEvasionIncidentsAsPerson = true;
					if(entity!=null)
					{
						this.FareEvasionIncidentsAsPerson.Add((FareEvasionIncidentEntity)entity);
					}
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "PersonAddresses":
					_alreadyFetchedPersonAddresses = true;
					if(entity!=null)
					{
						this.PersonAddresses.Add((PersonAddressEntity)entity);
					}
					break;
				case "Photographs":
					_alreadyFetchedPhotographs = true;
					if(entity!=null)
					{
						this.Photographs.Add((PhotographEntity)entity);
					}
					break;
				case "RecipientGroupMembers":
					_alreadyFetchedRecipientGroupMembers = true;
					if(entity!=null)
					{
						this.RecipientGroupMembers.Add((RecipientGroupMemberEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "SurveyResponses":
					_alreadyFetchedSurveyResponses = true;
					if(entity!=null)
					{
						this.SurveyResponses.Add((SurveyResponseEntity)entity);
					}
					break;
				case "Addresses":
					_alreadyFetchedAddresses = true;
					if(entity!=null)
					{
						this.Addresses.Add((AddressEntity)entity);
					}
					break;
				case "Address":
					_alreadyFetchedAddress = true;
					this.Address = (AddressEntity)entity;
					break;
				case "ContactPersonOnOrganization":
					_alreadyFetchedContactPersonOnOrganization = true;
					this.ContactPersonOnOrganization = (OrganizationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BankConnectionDatas":
					_bankConnectionDatas.Add((BankConnectionDataEntity)relatedEntity);
					break;
				case "CustomAttributeValueToPeople":
					_customAttributeValueToPeople.Add((CustomAttributeValueToPersonEntity)relatedEntity);
					break;
				case "CustomerAccounts":
					_customerAccounts.Add((CustomerAccountEntity)relatedEntity);
					break;
				case "EmailEventResendLocks":
					_emailEventResendLocks.Add((EmailEventResendLockEntity)relatedEntity);
					break;
				case "Entitlements":
					_entitlements.Add((EntitlementEntity)relatedEntity);
					break;
				case "FareEvasionIncidentsAsGuardian":
					_fareEvasionIncidentsAsGuardian.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "FareEvasionIncidentsAsPerson":
					_fareEvasionIncidentsAsPerson.Add((FareEvasionIncidentEntity)relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "PersonAddresses":
					_personAddresses.Add((PersonAddressEntity)relatedEntity);
					break;
				case "Photographs":
					_photographs.Add((PhotographEntity)relatedEntity);
					break;
				case "RecipientGroupMembers":
					_recipientGroupMembers.Add((RecipientGroupMemberEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "SurveyResponses":
					_surveyResponses.Add((SurveyResponseEntity)relatedEntity);
					break;
				case "Address":
					SetupSyncAddress(relatedEntity);
					break;
				case "ContactPersonOnOrganization":
					SetupSyncContactPersonOnOrganization(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BankConnectionDatas":
					this.PerformRelatedEntityRemoval(_bankConnectionDatas, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomAttributeValueToPeople":
					this.PerformRelatedEntityRemoval(_customAttributeValueToPeople, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomerAccounts":
					this.PerformRelatedEntityRemoval(_customerAccounts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailEventResendLocks":
					this.PerformRelatedEntityRemoval(_emailEventResendLocks, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Entitlements":
					this.PerformRelatedEntityRemoval(_entitlements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidentsAsGuardian":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidentsAsGuardian, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionIncidentsAsPerson":
					this.PerformRelatedEntityRemoval(_fareEvasionIncidentsAsPerson, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PersonAddresses":
					this.PerformRelatedEntityRemoval(_personAddresses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Photographs":
					this.PerformRelatedEntityRemoval(_photographs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RecipientGroupMembers":
					this.PerformRelatedEntityRemoval(_recipientGroupMembers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResponses":
					this.PerformRelatedEntityRemoval(_surveyResponses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Address":
					DesetupSyncAddress(false, true);
					break;
				case "ContactPersonOnOrganization":
					DesetupSyncContactPersonOnOrganization(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_contactPersonOnOrganization!=null)
			{
				toReturn.Add(_contactPersonOnOrganization);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_address!=null)
			{
				toReturn.Add(_address);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_bankConnectionDatas);
			toReturn.Add(_customAttributeValueToPeople);
			toReturn.Add(_customerAccounts);
			toReturn.Add(_emailEventResendLocks);
			toReturn.Add(_entitlements);
			toReturn.Add(_fareEvasionIncidentsAsGuardian);
			toReturn.Add(_fareEvasionIncidentsAsPerson);
			toReturn.Add(_paymentJournals);
			toReturn.Add(_personAddresses);
			toReturn.Add(_photographs);
			toReturn.Add(_recipientGroupMembers);
			toReturn.Add(_sales);
			toReturn.Add(_surveyResponses);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID)
		{
			return FetchUsingUCAddressID( addressID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCAddressID( addressID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCAddressID(Nullable<System.Int64> addressID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PersonDAO)CreateDAOInstance()).FetchPersonUsingUCAddressID(this, this.Transaction, addressID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary>Fetches the contents of this entity from the persistent storage using a polymorphic fetch on unique constraint, so the entity returned could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static PersonEntity FetchPolymorphicUsingUCAddressID(ITransaction transactionToUse, Nullable<System.Int64> addressID, Context contextToUse)
		{
			return FetchPolymorphicUsingUCAddressID(transactionToUse, addressID, contextToUse, null);
		}
		
		/// <summary>Fetches the contents of this entity from the persistent storage using a polymorphic fetch on unique constraint, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="addressID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static PersonEntity FetchPolymorphicUsingUCAddressID(ITransaction transactionToUse, Nullable<System.Int64> addressID, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return new PersonDAO().FetchPersonPolyUsingUCAddressID(transactionToUse, addressID, contextToUse, excludedIncludedFields);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  PersonEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 personID, Context contextToUse)
		{
			return FetchPolymorphic(transactionToUse, personID, contextToUse, null);
		}
				
		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key specified in a polymorphic way, so the entity returned  could be of a subtype of the current entity or the current entity.</summary>
		/// <param name="transactionToUse">transaction to use during fetch</param>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="contextToUse">Context to use for fetch</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>Fetched entity of the type of this entity or a subtype, or an empty entity of that type if not found.</returns>
		/// <remarks>Creates a new instance, doesn't fill <i>this</i> entity instance</remarks>
		public static  PersonEntity FetchPolymorphic(ITransaction transactionToUse, System.Int64 personID, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.PersonEntity);
			fields.ForcedValueWrite((int)PersonFieldIndex.PersonID, personID);
			return (PersonEntity)new PersonDAO().FetchExistingPolymorphic(transactionToUse, fields, contextToUse, excludedIncludedFields);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 personID)
		{
			return FetchUsingPK(personID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 personID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(personID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(personID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(personID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PersonID, null, null, null);
		}

		/// <summary>Determines whether this entity is a subType of the entity represented by the passed in enum value, which represents a value in the VarioSL.Entities.EntityType enum</summary>
		/// <param name="typeOfEntity">Type of entity.</param>
		/// <returns>true if the passed in type is a supertype of this entity, otherwise false</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckIfIsSubTypeOf(int typeOfEntity)
		{
			return InheritanceInfoProviderSingleton.GetInstance().CheckIfIsSubTypeOf("PersonEntity", ((VarioSL.Entities.EntityType)typeOfEntity).ToString());
		}
				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PersonRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'BankConnectionDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BankConnectionDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BankConnectionDataCollection GetMultiBankConnectionDatas(bool forceFetch)
		{
			return GetMultiBankConnectionDatas(forceFetch, _bankConnectionDatas.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BankConnectionDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BankConnectionDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BankConnectionDataCollection GetMultiBankConnectionDatas(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBankConnectionDatas(forceFetch, _bankConnectionDatas.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BankConnectionDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BankConnectionDataCollection GetMultiBankConnectionDatas(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBankConnectionDatas(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BankConnectionDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BankConnectionDataCollection GetMultiBankConnectionDatas(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBankConnectionDatas || forceFetch || _alwaysFetchBankConnectionDatas) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_bankConnectionDatas);
				_bankConnectionDatas.SuppressClearInGetMulti=!forceFetch;
				_bankConnectionDatas.EntityFactoryToUse = entityFactoryToUse;
				_bankConnectionDatas.GetMultiManyToOne(this, filter);
				_bankConnectionDatas.SuppressClearInGetMulti=false;
				_alreadyFetchedBankConnectionDatas = true;
			}
			return _bankConnectionDatas;
		}

		/// <summary> Sets the collection parameters for the collection for 'BankConnectionDatas'. These settings will be taken into account
		/// when the property BankConnectionDatas is requested or GetMultiBankConnectionDatas is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBankConnectionDatas(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_bankConnectionDatas.SortClauses=sortClauses;
			_bankConnectionDatas.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueToPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, _customAttributeValueToPeople.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueToPersonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, _customAttributeValueToPeople.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomAttributeValueToPeople(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection GetMultiCustomAttributeValueToPeople(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomAttributeValueToPeople || forceFetch || _alwaysFetchCustomAttributeValueToPeople) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customAttributeValueToPeople);
				_customAttributeValueToPeople.SuppressClearInGetMulti=!forceFetch;
				_customAttributeValueToPeople.EntityFactoryToUse = entityFactoryToUse;
				_customAttributeValueToPeople.GetMultiManyToOne(null, this, filter);
				_customAttributeValueToPeople.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomAttributeValueToPeople = true;
			}
			return _customAttributeValueToPeople;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomAttributeValueToPeople'. These settings will be taken into account
		/// when the property CustomAttributeValueToPeople is requested or GetMultiCustomAttributeValueToPeople is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomAttributeValueToPeople(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customAttributeValueToPeople.SortClauses=sortClauses;
			_customAttributeValueToPeople.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch)
		{
			return GetMultiCustomerAccounts(forceFetch, _customerAccounts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomerAccountEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomerAccounts(forceFetch, _customerAccounts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomerAccounts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountCollection GetMultiCustomerAccounts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomerAccounts || forceFetch || _alwaysFetchCustomerAccounts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customerAccounts);
				_customerAccounts.SuppressClearInGetMulti=!forceFetch;
				_customerAccounts.EntityFactoryToUse = entityFactoryToUse;
				_customerAccounts.GetMultiManyToOne(null, null, null, this, filter);
				_customerAccounts.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomerAccounts = true;
			}
			return _customerAccounts;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomerAccounts'. These settings will be taken into account
		/// when the property CustomerAccounts is requested or GetMultiCustomerAccounts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomerAccounts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customerAccounts.SortClauses=sortClauses;
			_customerAccounts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailEventResendLocks(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailEventResendLocks || forceFetch || _alwaysFetchEmailEventResendLocks) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventResendLocks);
				_emailEventResendLocks.SuppressClearInGetMulti=!forceFetch;
				_emailEventResendLocks.EntityFactoryToUse = entityFactoryToUse;
				_emailEventResendLocks.GetMultiManyToOne(null, null, null, this, null, filter);
				_emailEventResendLocks.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventResendLocks = true;
			}
			return _emailEventResendLocks;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventResendLocks'. These settings will be taken into account
		/// when the property EmailEventResendLocks is requested or GetMultiEmailEventResendLocks is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventResendLocks(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventResendLocks.SortClauses=sortClauses;
			_emailEventResendLocks.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntitlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementCollection GetMultiEntitlements(bool forceFetch)
		{
			return GetMultiEntitlements(forceFetch, _entitlements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementCollection GetMultiEntitlements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntitlements(forceFetch, _entitlements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementCollection GetMultiEntitlements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntitlements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementCollection GetMultiEntitlements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntitlements || forceFetch || _alwaysFetchEntitlements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entitlements);
				_entitlements.SuppressClearInGetMulti=!forceFetch;
				_entitlements.EntityFactoryToUse = entityFactoryToUse;
				_entitlements.GetMultiManyToOne(null, this, filter);
				_entitlements.SuppressClearInGetMulti=false;
				_alreadyFetchedEntitlements = true;
			}
			return _entitlements;
		}

		/// <summary> Sets the collection parameters for the collection for 'Entitlements'. These settings will be taken into account
		/// when the property Entitlements is requested or GetMultiEntitlements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntitlements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entitlements.SortClauses=sortClauses;
			_entitlements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsGuardian(bool forceFetch)
		{
			return GetMultiFareEvasionIncidentsAsGuardian(forceFetch, _fareEvasionIncidentsAsGuardian.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsGuardian(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidentsAsGuardian(forceFetch, _fareEvasionIncidentsAsGuardian.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsGuardian(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidentsAsGuardian(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsGuardian(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidentsAsGuardian || forceFetch || _alwaysFetchFareEvasionIncidentsAsGuardian) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidentsAsGuardian);
				_fareEvasionIncidentsAsGuardian.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidentsAsGuardian.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidentsAsGuardian.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_fareEvasionIncidentsAsGuardian.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidentsAsGuardian = true;
			}
			return _fareEvasionIncidentsAsGuardian;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidentsAsGuardian'. These settings will be taken into account
		/// when the property FareEvasionIncidentsAsGuardian is requested or GetMultiFareEvasionIncidentsAsGuardian is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidentsAsGuardian(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidentsAsGuardian.SortClauses=sortClauses;
			_fareEvasionIncidentsAsGuardian.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsPerson(bool forceFetch)
		{
			return GetMultiFareEvasionIncidentsAsPerson(forceFetch, _fareEvasionIncidentsAsPerson.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionIncidentEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsPerson(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionIncidentsAsPerson(forceFetch, _fareEvasionIncidentsAsPerson.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsPerson(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionIncidentsAsPerson(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection GetMultiFareEvasionIncidentsAsPerson(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionIncidentsAsPerson || forceFetch || _alwaysFetchFareEvasionIncidentsAsPerson) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionIncidentsAsPerson);
				_fareEvasionIncidentsAsPerson.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionIncidentsAsPerson.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionIncidentsAsPerson.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_fareEvasionIncidentsAsPerson.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionIncidentsAsPerson = true;
			}
			return _fareEvasionIncidentsAsPerson;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionIncidentsAsPerson'. These settings will be taken into account
		/// when the property FareEvasionIncidentsAsPerson is requested or GetMultiFareEvasionIncidentsAsPerson is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionIncidentsAsPerson(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionIncidentsAsPerson.SortClauses=sortClauses;
			_fareEvasionIncidentsAsPerson.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PersonAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch)
		{
			return GetMultiPersonAddresses(forceFetch, _personAddresses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PersonAddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPersonAddresses(forceFetch, _personAddresses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPersonAddresses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PersonAddressCollection GetMultiPersonAddresses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPersonAddresses || forceFetch || _alwaysFetchPersonAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_personAddresses);
				_personAddresses.SuppressClearInGetMulti=!forceFetch;
				_personAddresses.EntityFactoryToUse = entityFactoryToUse;
				_personAddresses.GetMultiManyToOne(null, this, filter);
				_personAddresses.SuppressClearInGetMulti=false;
				_alreadyFetchedPersonAddresses = true;
			}
			return _personAddresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'PersonAddresses'. These settings will be taken into account
		/// when the property PersonAddresses is requested or GetMultiPersonAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPersonAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_personAddresses.SortClauses=sortClauses;
			_personAddresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PhotographEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch)
		{
			return GetMultiPhotographs(forceFetch, _photographs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PhotographEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPhotographs(forceFetch, _photographs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPhotographs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PhotographCollection GetMultiPhotographs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPhotographs || forceFetch || _alwaysFetchPhotographs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_photographs);
				_photographs.SuppressClearInGetMulti=!forceFetch;
				_photographs.EntityFactoryToUse = entityFactoryToUse;
				_photographs.GetMultiManyToOne(null, this, filter);
				_photographs.SuppressClearInGetMulti=false;
				_alreadyFetchedPhotographs = true;
			}
			return _photographs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Photographs'. These settings will be taken into account
		/// when the property Photographs is requested or GetMultiPhotographs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPhotographs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_photographs.SortClauses=sortClauses;
			_photographs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RecipientGroupMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch)
		{
			return GetMultiRecipientGroupMembers(forceFetch, _recipientGroupMembers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RecipientGroupMemberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRecipientGroupMembers(forceFetch, _recipientGroupMembers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRecipientGroupMembers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection GetMultiRecipientGroupMembers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRecipientGroupMembers || forceFetch || _alwaysFetchRecipientGroupMembers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_recipientGroupMembers);
				_recipientGroupMembers.SuppressClearInGetMulti=!forceFetch;
				_recipientGroupMembers.EntityFactoryToUse = entityFactoryToUse;
				_recipientGroupMembers.GetMultiManyToOne(this, null, filter);
				_recipientGroupMembers.SuppressClearInGetMulti=false;
				_alreadyFetchedRecipientGroupMembers = true;
			}
			return _recipientGroupMembers;
		}

		/// <summary> Sets the collection parameters for the collection for 'RecipientGroupMembers'. These settings will be taken into account
		/// when the property RecipientGroupMembers is requested or GetMultiRecipientGroupMembers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRecipientGroupMembers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_recipientGroupMembers.SortClauses=sortClauses;
			_recipientGroupMembers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch)
		{
			return GetMultiSurveyResponses(forceFetch, _surveyResponses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResponseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResponses(forceFetch, _surveyResponses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResponses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SurveyResponseCollection GetMultiSurveyResponses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResponses || forceFetch || _alwaysFetchSurveyResponses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResponses);
				_surveyResponses.SuppressClearInGetMulti=!forceFetch;
				_surveyResponses.EntityFactoryToUse = entityFactoryToUse;
				_surveyResponses.GetMultiManyToOne(this, null, filter);
				_surveyResponses.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResponses = true;
			}
			return _surveyResponses;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResponses'. These settings will be taken into account
		/// when the property SurveyResponses is requested or GetMultiSurveyResponses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResponses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResponses.SortClauses=sortClauses;
			_surveyResponses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AddressEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch)
		{
			return GetMultiAddresses(forceFetch, _addresses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AddressCollection GetMultiAddresses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAddresses || forceFetch || _alwaysFetchAddresses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_addresses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(PersonFields.PersonID, ComparisonOperator.Equal, this.PersonID, "PersonEntity__"));
				_addresses.SuppressClearInGetMulti=!forceFetch;
				_addresses.EntityFactoryToUse = entityFactoryToUse;
				_addresses.GetMulti(filter, GetRelationsForField("Addresses"));
				_addresses.SuppressClearInGetMulti=false;
				_alreadyFetchedAddresses = true;
			}
			return _addresses;
		}

		/// <summary> Sets the collection parameters for the collection for 'Addresses'. These settings will be taken into account
		/// when the property Addresses is requested or GetMultiAddresses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAddresses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_addresses.SortClauses=sortClauses;
			_addresses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public AddressEntity GetSingleAddress()
		{
			return GetSingleAddress(false);
		}
		
		/// <summary> Retrieves the related entity of type 'AddressEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AddressEntity' which is related to this entity.</returns>
		public virtual AddressEntity GetSingleAddress(bool forceFetch)
		{
			if( ( !_alreadyFetchedAddress || forceFetch || _alwaysFetchAddress) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AddressEntityUsingAddressID);
				AddressEntity newEntity = new AddressEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AddressID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AddressEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_addressReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Address = newEntity;
				_alreadyFetchedAddress = fetchResult;
			}
			return _address;
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public OrganizationEntity GetSingleContactPersonOnOrganization()
		{
			return GetSingleContactPersonOnOrganization(false);
		}
		
		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public virtual OrganizationEntity GetSingleContactPersonOnOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedContactPersonOnOrganization || forceFetch || _alwaysFetchContactPersonOnOrganization) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationEntityUsingContactPersonID);
				OrganizationEntity newEntity = new OrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCContactPersonID(this.PersonID);
				}
				if(fetchResult)
				{
					newEntity = (OrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contactPersonOnOrganizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ContactPersonOnOrganization = newEntity;
				_alreadyFetchedContactPersonOnOrganization = fetchResult;
			}
			return _contactPersonOnOrganization;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BankConnectionDatas", _bankConnectionDatas);
			toReturn.Add("CustomAttributeValueToPeople", _customAttributeValueToPeople);
			toReturn.Add("CustomerAccounts", _customerAccounts);
			toReturn.Add("EmailEventResendLocks", _emailEventResendLocks);
			toReturn.Add("Entitlements", _entitlements);
			toReturn.Add("FareEvasionIncidentsAsGuardian", _fareEvasionIncidentsAsGuardian);
			toReturn.Add("FareEvasionIncidentsAsPerson", _fareEvasionIncidentsAsPerson);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("PersonAddresses", _personAddresses);
			toReturn.Add("Photographs", _photographs);
			toReturn.Add("RecipientGroupMembers", _recipientGroupMembers);
			toReturn.Add("Sales", _sales);
			toReturn.Add("SurveyResponses", _surveyResponses);
			toReturn.Add("Addresses", _addresses);
			toReturn.Add("Address", _address);
			toReturn.Add("ContactPersonOnOrganization", _contactPersonOnOrganization);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)PersonFieldIndex.TypeDiscriminator, "Person");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="validator">The validator object for this PersonEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 personID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(personID, prefetchPathToUse, null, null);
			if(this.Fields.State==EntityState.New)
			{
				this.Fields.ForcedValueWrite((int)PersonFieldIndex.TypeDiscriminator, "Person");
			}
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_bankConnectionDatas = new VarioSL.Entities.CollectionClasses.BankConnectionDataCollection();
			_bankConnectionDatas.SetContainingEntityInfo(this, "AccountOwner");

			_customAttributeValueToPeople = new VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection();
			_customAttributeValueToPeople.SetContainingEntityInfo(this, "Person");

			_customerAccounts = new VarioSL.Entities.CollectionClasses.CustomerAccountCollection();
			_customerAccounts.SetContainingEntityInfo(this, "Person");

			_emailEventResendLocks = new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection();
			_emailEventResendLocks.SetContainingEntityInfo(this, "Person");

			_entitlements = new VarioSL.Entities.CollectionClasses.EntitlementCollection();
			_entitlements.SetContainingEntityInfo(this, "Person");

			_fareEvasionIncidentsAsGuardian = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidentsAsGuardian.SetContainingEntityInfo(this, "Guardian");

			_fareEvasionIncidentsAsPerson = new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection();
			_fareEvasionIncidentsAsPerson.SetContainingEntityInfo(this, "Person");

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "Person");

			_personAddresses = new VarioSL.Entities.CollectionClasses.PersonAddressCollection();
			_personAddresses.SetContainingEntityInfo(this, "Person");

			_photographs = new VarioSL.Entities.CollectionClasses.PhotographCollection();
			_photographs.SetContainingEntityInfo(this, "Person");

			_recipientGroupMembers = new VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection();
			_recipientGroupMembers.SetContainingEntityInfo(this, "Person");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "Person");

			_surveyResponses = new VarioSL.Entities.CollectionClasses.SurveyResponseCollection();
			_surveyResponses.SetContainingEntityInfo(this, "Person");
			_addresses = new VarioSL.Entities.CollectionClasses.AddressCollection();
			_addressReturnsNewIfNotFound = false;
			_contactPersonOnOrganizationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AcceptTermsAndConditions", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Authenticated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CellPhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataProtectionNoticeAccepted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DateOfBirth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FaxNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Gender", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HasDrivingLicense", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Identifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCellPhoneNumberConfirmed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsEmailConfirmed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsEmailEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsNewsletterAccepted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobTitle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MiddleName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationalIdentifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Salutation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeDiscriminator", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _address</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAddress(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticPersonRelations.AddressEntityUsingAddressIDStatic, true, signalRelatedEntity, "Person", resetFKFields, new int[] { (int)PersonFieldIndex.AddressID } );
			_address = null;
		}
	
		/// <summary> setups the sync logic for member _address</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAddress(IEntityCore relatedEntity)
		{
			if(_address!=relatedEntity)
			{
				DesetupSyncAddress(true, true);
				_address = (AddressEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _address, new PropertyChangedEventHandler( OnAddressPropertyChanged ), "Address", VarioSL.Entities.RelationClasses.StaticPersonRelations.AddressEntityUsingAddressIDStatic, true, ref _alreadyFetchedAddress, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAddressPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contactPersonOnOrganization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContactPersonOnOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contactPersonOnOrganization, new PropertyChangedEventHandler( OnContactPersonOnOrganizationPropertyChanged ), "ContactPersonOnOrganization", VarioSL.Entities.RelationClasses.StaticPersonRelations.OrganizationEntityUsingContactPersonIDStatic, false, signalRelatedEntity, "ContactPerson", false, new int[] { (int)PersonFieldIndex.PersonID } );
			_contactPersonOnOrganization = null;
		}
	
		/// <summary> setups the sync logic for member _contactPersonOnOrganization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContactPersonOnOrganization(IEntityCore relatedEntity)
		{
			if(_contactPersonOnOrganization!=relatedEntity)
			{
				DesetupSyncContactPersonOnOrganization(true, true);
				_contactPersonOnOrganization = (OrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contactPersonOnOrganization, new PropertyChangedEventHandler( OnContactPersonOnOrganizationPropertyChanged ), "ContactPersonOnOrganization", VarioSL.Entities.RelationClasses.StaticPersonRelations.OrganizationEntityUsingContactPersonIDStatic, false, ref _alreadyFetchedContactPersonOnOrganization, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContactPersonOnOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="personID">PK value for Person which data should be fetched into this Person object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 personID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PersonFieldIndex.PersonID].ForcedCurrentValueWrite(personID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePersonDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PersonEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PersonRelations Relations
		{
			get	{ return new PersonRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankConnectionData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankConnectionDatas
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankConnectionDataCollection(), (IEntityRelation)GetRelationsForField("BankConnectionDatas")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.BankConnectionDataEntity, 0, null, null, null, "BankConnectionDatas", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttributeValueToPerson' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttributeValueToPeople
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection(), (IEntityRelation)GetRelationsForField("CustomAttributeValueToPeople")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity, 0, null, null, null, "CustomAttributeValueToPeople", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccount' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccounts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountCollection(), (IEntityRelation)GetRelationsForField("CustomerAccounts")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.CustomerAccountEntity, 0, null, null, null, "CustomerAccounts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEventResendLock' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventResendLocks
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection(), (IEntityRelation)GetRelationsForField("EmailEventResendLocks")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, 0, null, null, null, "EmailEventResendLocks", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entitlement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntitlements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EntitlementCollection(), (IEntityRelation)GetRelationsForField("Entitlements")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.EntitlementEntity, 0, null, null, null, "Entitlements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidentsAsGuardian
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidentsAsGuardian")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidentsAsGuardian", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionIncident' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionIncidentsAsPerson
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection(), (IEntityRelation)GetRelationsForField("FareEvasionIncidentsAsPerson")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, 0, null, null, null, "FareEvasionIncidentsAsPerson", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PersonAddress' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPersonAddresses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonAddressCollection(), (IEntityRelation)GetRelationsForField("PersonAddresses")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.PersonAddressEntity, 0, null, null, null, "PersonAddresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Photograph' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPhotographs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PhotographCollection(), (IEntityRelation)GetRelationsForField("Photographs")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.PhotographEntity, 0, null, null, null, "Photographs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RecipientGroupMember' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRecipientGroupMembers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection(), (IEntityRelation)GetRelationsForField("RecipientGroupMembers")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.RecipientGroupMemberEntity, 0, null, null, null, "RecipientGroupMembers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResponse' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResponses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SurveyResponseCollection(), (IEntityRelation)GetRelationsForField("SurveyResponses")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.SurveyResponseEntity, 0, null, null, null, "SurveyResponses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddresses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.PersonAddressEntityUsingPersonID;
				intermediateRelation.SetAliases(string.Empty, "PersonAddress_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, GetRelationsForField("Addresses"), "Addresses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Address'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAddress
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AddressCollection(), (IEntityRelation)GetRelationsForField("Address")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.AddressEntity, 0, null, null, null, "Address", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContactPersonOnOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("ContactPersonOnOrganization")[0], (int)VarioSL.Entities.EntityType.PersonEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "ContactPersonOnOrganization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AcceptTermsAndConditions property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ACCEPTTERMSANDCONDITIONS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AcceptTermsAndConditions
		{
			get { return (System.DateTime)GetValue((int)PersonFieldIndex.AcceptTermsAndConditions, true); }
			set	{ SetValue((int)PersonFieldIndex.AcceptTermsAndConditions, value, true); }
		}

		/// <summary> The AddressID property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PersonFieldIndex.AddressID, false); }
			set	{ SetValue((int)PersonFieldIndex.AddressID, value, true); }
		}

		/// <summary> The Authenticated property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."AUTHENTICATED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Authenticated
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.Authenticated, true); }
			set	{ SetValue((int)PersonFieldIndex.Authenticated, value, true); }
		}

		/// <summary> The CellPhoneNumber property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."CELLPHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CellPhoneNumber
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.CellPhoneNumber, true); }
			set	{ SetValue((int)PersonFieldIndex.CellPhoneNumber, value, true); }
		}

		/// <summary> The DataProtectionNoticeAccepted property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."DATAPROTECTIONNOTICEACCEPTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DataProtectionNoticeAccepted
		{
			get { return (System.DateTime)GetValue((int)PersonFieldIndex.DataProtectionNoticeAccepted, true); }
			set	{ SetValue((int)PersonFieldIndex.DataProtectionNoticeAccepted, value, true); }
		}

		/// <summary> The DateOfBirth property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."DATEOFBIRTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DateOfBirth
		{
			get { return (System.DateTime)GetValue((int)PersonFieldIndex.DateOfBirth, true); }
			set	{ SetValue((int)PersonFieldIndex.DateOfBirth, value, true); }
		}

		/// <summary> The Email property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."EMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.Email, true); }
			set	{ SetValue((int)PersonFieldIndex.Email, value, true); }
		}

		/// <summary> The FaxNumber property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."FAXNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FaxNumber
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.FaxNumber, true); }
			set	{ SetValue((int)PersonFieldIndex.FaxNumber, value, true); }
		}

		/// <summary> The FirstName property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."FIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.FirstName, true); }
			set	{ SetValue((int)PersonFieldIndex.FirstName, value, true); }
		}

		/// <summary> The Gender property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."GENDER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.Gender> Gender
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.Gender>)GetValue((int)PersonFieldIndex.Gender, false); }
			set	{ SetValue((int)PersonFieldIndex.Gender, value, true); }
		}

		/// <summary> The HasDrivingLicense property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."HASDRIVINGLICENSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean HasDrivingLicense
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.HasDrivingLicense, true); }
			set	{ SetValue((int)PersonFieldIndex.HasDrivingLicense, value, true); }
		}

		/// <summary> The Identifier property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."IDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.Identifier, true); }
			set	{ SetValue((int)PersonFieldIndex.Identifier, value, true); }
		}

		/// <summary> The IsCellPhoneNumberConfirmed property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ISCELLPHONENUMBERCONFIRMED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCellPhoneNumberConfirmed
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.IsCellPhoneNumberConfirmed, true); }
			set	{ SetValue((int)PersonFieldIndex.IsCellPhoneNumberConfirmed, value, true); }
		}

		/// <summary> The IsEmailConfirmed property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ISEMAILCONFIRMED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsEmailConfirmed
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.IsEmailConfirmed, true); }
			set	{ SetValue((int)PersonFieldIndex.IsEmailConfirmed, value, true); }
		}

		/// <summary> The IsEmailEnabled property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ISEMAILENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsEmailEnabled
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.IsEmailEnabled, true); }
			set	{ SetValue((int)PersonFieldIndex.IsEmailEnabled, value, true); }
		}

		/// <summary> The IsNewsletterAccepted property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ISNEWSLETTERACCEPTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsNewsletterAccepted
		{
			get { return (System.Boolean)GetValue((int)PersonFieldIndex.IsNewsletterAccepted, true); }
			set	{ SetValue((int)PersonFieldIndex.IsNewsletterAccepted, value, true); }
		}

		/// <summary> The JobTitle property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."JOBTITLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String JobTitle
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.JobTitle, true); }
			set	{ SetValue((int)PersonFieldIndex.JobTitle, value, true); }
		}

		/// <summary> The LastModified property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PersonFieldIndex.LastModified, true); }
			set	{ SetValue((int)PersonFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastName property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."LASTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastName
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.LastName, true); }
			set	{ SetValue((int)PersonFieldIndex.LastName, value, true); }
		}

		/// <summary> The LastUser property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.LastUser, true); }
			set	{ SetValue((int)PersonFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MiddleName property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."MIDDLENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MiddleName
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.MiddleName, true); }
			set	{ SetValue((int)PersonFieldIndex.MiddleName, value, true); }
		}

		/// <summary> The OrganizationalIdentifier property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."ORGANIZATIONALIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrganizationalIdentifier
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.OrganizationalIdentifier, true); }
			set	{ SetValue((int)PersonFieldIndex.OrganizationalIdentifier, value, true); }
		}

		/// <summary> The PersonID property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)PersonFieldIndex.PersonID, true); }
			set	{ SetValue((int)PersonFieldIndex.PersonID, value, true); }
		}

		/// <summary> The PhoneNumber property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."PHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhoneNumber
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.PhoneNumber, true); }
			set	{ SetValue((int)PersonFieldIndex.PhoneNumber, value, true); }
		}

		/// <summary> The Salutation property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."SALUTATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.Salutation Salutation
		{
			get { return (VarioSL.Entities.Enumerations.Salutation)GetValue((int)PersonFieldIndex.Salutation, true); }
			set	{ SetValue((int)PersonFieldIndex.Salutation, value, true); }
		}

		/// <summary> The Title property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."TITLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.Title, true); }
			set	{ SetValue((int)PersonFieldIndex.Title, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PersonFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PersonFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TypeDiscriminator property of the Entity Person<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PERSON"."TYPEDISCRIMINATOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String TypeDiscriminator
		{
			get { return (System.String)GetValue((int)PersonFieldIndex.TypeDiscriminator, true); }

		}

		/// <summary> Retrieves all related entities of type 'BankConnectionDataEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBankConnectionDatas()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BankConnectionDataCollection BankConnectionDatas
		{
			get	{ return GetMultiBankConnectionDatas(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BankConnectionDatas. When set to true, BankConnectionDatas is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankConnectionDatas is accessed. You can always execute/ a forced fetch by calling GetMultiBankConnectionDatas(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankConnectionDatas
		{
			get	{ return _alwaysFetchBankConnectionDatas; }
			set	{ _alwaysFetchBankConnectionDatas = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BankConnectionDatas already has been fetched. Setting this property to false when BankConnectionDatas has been fetched
		/// will clear the BankConnectionDatas collection well. Setting this property to true while BankConnectionDatas hasn't been fetched disables lazy loading for BankConnectionDatas</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankConnectionDatas
		{
			get { return _alreadyFetchedBankConnectionDatas;}
			set 
			{
				if(_alreadyFetchedBankConnectionDatas && !value && (_bankConnectionDatas != null))
				{
					_bankConnectionDatas.Clear();
				}
				_alreadyFetchedBankConnectionDatas = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomAttributeValueToPersonEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomAttributeValueToPeople()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueToPersonCollection CustomAttributeValueToPeople
		{
			get	{ return GetMultiCustomAttributeValueToPeople(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttributeValueToPeople. When set to true, CustomAttributeValueToPeople is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttributeValueToPeople is accessed. You can always execute/ a forced fetch by calling GetMultiCustomAttributeValueToPeople(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttributeValueToPeople
		{
			get	{ return _alwaysFetchCustomAttributeValueToPeople; }
			set	{ _alwaysFetchCustomAttributeValueToPeople = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttributeValueToPeople already has been fetched. Setting this property to false when CustomAttributeValueToPeople has been fetched
		/// will clear the CustomAttributeValueToPeople collection well. Setting this property to true while CustomAttributeValueToPeople hasn't been fetched disables lazy loading for CustomAttributeValueToPeople</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttributeValueToPeople
		{
			get { return _alreadyFetchedCustomAttributeValueToPeople;}
			set 
			{
				if(_alreadyFetchedCustomAttributeValueToPeople && !value && (_customAttributeValueToPeople != null))
				{
					_customAttributeValueToPeople.Clear();
				}
				_alreadyFetchedCustomAttributeValueToPeople = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomerAccountEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomerAccounts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomerAccountCollection CustomerAccounts
		{
			get	{ return GetMultiCustomerAccounts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccounts. When set to true, CustomerAccounts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccounts is accessed. You can always execute/ a forced fetch by calling GetMultiCustomerAccounts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccounts
		{
			get	{ return _alwaysFetchCustomerAccounts; }
			set	{ _alwaysFetchCustomerAccounts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccounts already has been fetched. Setting this property to false when CustomerAccounts has been fetched
		/// will clear the CustomerAccounts collection well. Setting this property to true while CustomerAccounts hasn't been fetched disables lazy loading for CustomerAccounts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccounts
		{
			get { return _alreadyFetchedCustomerAccounts;}
			set 
			{
				if(_alreadyFetchedCustomerAccounts && !value && (_customerAccounts != null))
				{
					_customerAccounts.Clear();
				}
				_alreadyFetchedCustomerAccounts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventResendLocks()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection EmailEventResendLocks
		{
			get	{ return GetMultiEmailEventResendLocks(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventResendLocks. When set to true, EmailEventResendLocks is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventResendLocks is accessed. You can always execute/ a forced fetch by calling GetMultiEmailEventResendLocks(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventResendLocks
		{
			get	{ return _alwaysFetchEmailEventResendLocks; }
			set	{ _alwaysFetchEmailEventResendLocks = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventResendLocks already has been fetched. Setting this property to false when EmailEventResendLocks has been fetched
		/// will clear the EmailEventResendLocks collection well. Setting this property to true while EmailEventResendLocks hasn't been fetched disables lazy loading for EmailEventResendLocks</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventResendLocks
		{
			get { return _alreadyFetchedEmailEventResendLocks;}
			set 
			{
				if(_alreadyFetchedEmailEventResendLocks && !value && (_emailEventResendLocks != null))
				{
					_emailEventResendLocks.Clear();
				}
				_alreadyFetchedEmailEventResendLocks = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntitlementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntitlements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementCollection Entitlements
		{
			get	{ return GetMultiEntitlements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Entitlements. When set to true, Entitlements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Entitlements is accessed. You can always execute/ a forced fetch by calling GetMultiEntitlements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntitlements
		{
			get	{ return _alwaysFetchEntitlements; }
			set	{ _alwaysFetchEntitlements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Entitlements already has been fetched. Setting this property to false when Entitlements has been fetched
		/// will clear the Entitlements collection well. Setting this property to true while Entitlements hasn't been fetched disables lazy loading for Entitlements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntitlements
		{
			get { return _alreadyFetchedEntitlements;}
			set 
			{
				if(_alreadyFetchedEntitlements && !value && (_entitlements != null))
				{
					_entitlements.Clear();
				}
				_alreadyFetchedEntitlements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidentsAsGuardian()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidentsAsGuardian
		{
			get	{ return GetMultiFareEvasionIncidentsAsGuardian(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidentsAsGuardian. When set to true, FareEvasionIncidentsAsGuardian is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidentsAsGuardian is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidentsAsGuardian(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidentsAsGuardian
		{
			get	{ return _alwaysFetchFareEvasionIncidentsAsGuardian; }
			set	{ _alwaysFetchFareEvasionIncidentsAsGuardian = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidentsAsGuardian already has been fetched. Setting this property to false when FareEvasionIncidentsAsGuardian has been fetched
		/// will clear the FareEvasionIncidentsAsGuardian collection well. Setting this property to true while FareEvasionIncidentsAsGuardian hasn't been fetched disables lazy loading for FareEvasionIncidentsAsGuardian</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidentsAsGuardian
		{
			get { return _alreadyFetchedFareEvasionIncidentsAsGuardian;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidentsAsGuardian && !value && (_fareEvasionIncidentsAsGuardian != null))
				{
					_fareEvasionIncidentsAsGuardian.Clear();
				}
				_alreadyFetchedFareEvasionIncidentsAsGuardian = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionIncidentEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionIncidentsAsPerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionIncidentCollection FareEvasionIncidentsAsPerson
		{
			get	{ return GetMultiFareEvasionIncidentsAsPerson(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionIncidentsAsPerson. When set to true, FareEvasionIncidentsAsPerson is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionIncidentsAsPerson is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionIncidentsAsPerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionIncidentsAsPerson
		{
			get	{ return _alwaysFetchFareEvasionIncidentsAsPerson; }
			set	{ _alwaysFetchFareEvasionIncidentsAsPerson = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionIncidentsAsPerson already has been fetched. Setting this property to false when FareEvasionIncidentsAsPerson has been fetched
		/// will clear the FareEvasionIncidentsAsPerson collection well. Setting this property to true while FareEvasionIncidentsAsPerson hasn't been fetched disables lazy loading for FareEvasionIncidentsAsPerson</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionIncidentsAsPerson
		{
			get { return _alreadyFetchedFareEvasionIncidentsAsPerson;}
			set 
			{
				if(_alreadyFetchedFareEvasionIncidentsAsPerson && !value && (_fareEvasionIncidentsAsPerson != null))
				{
					_fareEvasionIncidentsAsPerson.Clear();
				}
				_alreadyFetchedFareEvasionIncidentsAsPerson = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PersonAddressEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPersonAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PersonAddressCollection PersonAddresses
		{
			get	{ return GetMultiPersonAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PersonAddresses. When set to true, PersonAddresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PersonAddresses is accessed. You can always execute/ a forced fetch by calling GetMultiPersonAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPersonAddresses
		{
			get	{ return _alwaysFetchPersonAddresses; }
			set	{ _alwaysFetchPersonAddresses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PersonAddresses already has been fetched. Setting this property to false when PersonAddresses has been fetched
		/// will clear the PersonAddresses collection well. Setting this property to true while PersonAddresses hasn't been fetched disables lazy loading for PersonAddresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPersonAddresses
		{
			get { return _alreadyFetchedPersonAddresses;}
			set 
			{
				if(_alreadyFetchedPersonAddresses && !value && (_personAddresses != null))
				{
					_personAddresses.Clear();
				}
				_alreadyFetchedPersonAddresses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PhotographEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPhotographs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PhotographCollection Photographs
		{
			get	{ return GetMultiPhotographs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Photographs. When set to true, Photographs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Photographs is accessed. You can always execute/ a forced fetch by calling GetMultiPhotographs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPhotographs
		{
			get	{ return _alwaysFetchPhotographs; }
			set	{ _alwaysFetchPhotographs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Photographs already has been fetched. Setting this property to false when Photographs has been fetched
		/// will clear the Photographs collection well. Setting this property to true while Photographs hasn't been fetched disables lazy loading for Photographs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPhotographs
		{
			get { return _alreadyFetchedPhotographs;}
			set 
			{
				if(_alreadyFetchedPhotographs && !value && (_photographs != null))
				{
					_photographs.Clear();
				}
				_alreadyFetchedPhotographs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RecipientGroupMemberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRecipientGroupMembers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RecipientGroupMemberCollection RecipientGroupMembers
		{
			get	{ return GetMultiRecipientGroupMembers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RecipientGroupMembers. When set to true, RecipientGroupMembers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RecipientGroupMembers is accessed. You can always execute/ a forced fetch by calling GetMultiRecipientGroupMembers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRecipientGroupMembers
		{
			get	{ return _alwaysFetchRecipientGroupMembers; }
			set	{ _alwaysFetchRecipientGroupMembers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RecipientGroupMembers already has been fetched. Setting this property to false when RecipientGroupMembers has been fetched
		/// will clear the RecipientGroupMembers collection well. Setting this property to true while RecipientGroupMembers hasn't been fetched disables lazy loading for RecipientGroupMembers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRecipientGroupMembers
		{
			get { return _alreadyFetchedRecipientGroupMembers;}
			set 
			{
				if(_alreadyFetchedRecipientGroupMembers && !value && (_recipientGroupMembers != null))
				{
					_recipientGroupMembers.Clear();
				}
				_alreadyFetchedRecipientGroupMembers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResponseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResponses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SurveyResponseCollection SurveyResponses
		{
			get	{ return GetMultiSurveyResponses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResponses. When set to true, SurveyResponses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResponses is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResponses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResponses
		{
			get	{ return _alwaysFetchSurveyResponses; }
			set	{ _alwaysFetchSurveyResponses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResponses already has been fetched. Setting this property to false when SurveyResponses has been fetched
		/// will clear the SurveyResponses collection well. Setting this property to true while SurveyResponses hasn't been fetched disables lazy loading for SurveyResponses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResponses
		{
			get { return _alreadyFetchedSurveyResponses;}
			set 
			{
				if(_alreadyFetchedSurveyResponses && !value && (_surveyResponses != null))
				{
					_surveyResponses.Clear();
				}
				_alreadyFetchedSurveyResponses = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'AddressEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAddresses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AddressCollection Addresses
		{
			get { return GetMultiAddresses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Addresses. When set to true, Addresses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Addresses is accessed. You can always execute a forced fetch by calling GetMultiAddresses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddresses
		{
			get	{ return _alwaysFetchAddresses; }
			set	{ _alwaysFetchAddresses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Addresses already has been fetched. Setting this property to false when Addresses has been fetched
		/// will clear the Addresses collection well. Setting this property to true while Addresses hasn't been fetched disables lazy loading for Addresses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddresses
		{
			get { return _alreadyFetchedAddresses;}
			set 
			{
				if(_alreadyFetchedAddresses && !value && (_addresses != null))
				{
					_addresses.Clear();
				}
				_alreadyFetchedAddresses = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AddressEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAddress()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AddressEntity Address
		{
			get	{ return GetSingleAddress(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncAddress(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_address !=null);
						DesetupSyncAddress(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Address");
						}
					}
					else
					{
						if(_address!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Person");
							SetupSyncAddress(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Address. When set to true, Address is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Address is accessed. You can always execute a forced fetch by calling GetSingleAddress(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAddress
		{
			get	{ return _alwaysFetchAddress; }
			set	{ _alwaysFetchAddress = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Address already has been fetched. Setting this property to false when Address has been fetched
		/// will set Address to null as well. Setting this property to true while Address hasn't been fetched disables lazy loading for Address</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAddress
		{
			get { return _alreadyFetchedAddress;}
			set 
			{
				if(_alreadyFetchedAddress && !value)
				{
					this.Address = null;
				}
				_alreadyFetchedAddress = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Address is not found
		/// in the database. When set to true, Address will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AddressReturnsNewIfNotFound
		{
			get	{ return _addressReturnsNewIfNotFound; }
			set	{ _addressReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'OrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContactPersonOnOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationEntity ContactPersonOnOrganization
		{
			get	{ return GetSingleContactPersonOnOrganization(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncContactPersonOnOrganization(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_contactPersonOnOrganization !=null);
						DesetupSyncContactPersonOnOrganization(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ContactPersonOnOrganization");
						}
					}
					else
					{
						if(_contactPersonOnOrganization!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "ContactPerson");
							SetupSyncContactPersonOnOrganization(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ContactPersonOnOrganization. When set to true, ContactPersonOnOrganization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContactPersonOnOrganization is accessed. You can always execute a forced fetch by calling GetSingleContactPersonOnOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContactPersonOnOrganization
		{
			get	{ return _alwaysFetchContactPersonOnOrganization; }
			set	{ _alwaysFetchContactPersonOnOrganization = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ContactPersonOnOrganization already has been fetched. Setting this property to false when ContactPersonOnOrganization has been fetched
		/// will set ContactPersonOnOrganization to null as well. Setting this property to true while ContactPersonOnOrganization hasn't been fetched disables lazy loading for ContactPersonOnOrganization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContactPersonOnOrganization
		{
			get { return _alreadyFetchedContactPersonOnOrganization;}
			set 
			{
				if(_alreadyFetchedContactPersonOnOrganization && !value)
				{
					this.ContactPersonOnOrganization = null;
				}
				_alreadyFetchedContactPersonOnOrganization = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ContactPersonOnOrganization is not found
		/// in the database. When set to true, ContactPersonOnOrganization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContactPersonOnOrganizationReturnsNewIfNotFound
		{
			get	{ return _contactPersonOnOrganizationReturnsNewIfNotFound; }
			set	{ _contactPersonOnOrganizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.TargetPerEntityHierarchy;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PersonEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
