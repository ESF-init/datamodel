﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettledRevenue'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettledRevenueEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private RevenueRecognitionEntity _revenueRecognition;
		private bool	_alwaysFetchRevenueRecognition, _alreadyFetchedRevenueRecognition, _revenueRecognitionReturnsNewIfNotFound;
		private RevenueSettlementEntity _revenueSettlement;
		private bool	_alwaysFetchRevenueSettlement, _alreadyFetchedRevenueSettlement, _revenueSettlementReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RevenueRecognition</summary>
			public static readonly string RevenueRecognition = "RevenueRecognition";
			/// <summary>Member name RevenueSettlement</summary>
			public static readonly string RevenueSettlement = "RevenueSettlement";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettledRevenueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettledRevenueEntity() :base("SettledRevenueEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		public SettledRevenueEntity(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID):base("SettledRevenueEntity")
		{
			InitClassFetch(revenueRecognitionID, revenueSettlementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettledRevenueEntity(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse):base("SettledRevenueEntity")
		{
			InitClassFetch(revenueRecognitionID, revenueSettlementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="validator">The custom validator object for this SettledRevenueEntity</param>
		public SettledRevenueEntity(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IValidator validator):base("SettledRevenueEntity")
		{
			InitClassFetch(revenueRecognitionID, revenueSettlementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettledRevenueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_revenueRecognition = (RevenueRecognitionEntity)info.GetValue("_revenueRecognition", typeof(RevenueRecognitionEntity));
			if(_revenueRecognition!=null)
			{
				_revenueRecognition.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueRecognitionReturnsNewIfNotFound = info.GetBoolean("_revenueRecognitionReturnsNewIfNotFound");
			_alwaysFetchRevenueRecognition = info.GetBoolean("_alwaysFetchRevenueRecognition");
			_alreadyFetchedRevenueRecognition = info.GetBoolean("_alreadyFetchedRevenueRecognition");

			_revenueSettlement = (RevenueSettlementEntity)info.GetValue("_revenueSettlement", typeof(RevenueSettlementEntity));
			if(_revenueSettlement!=null)
			{
				_revenueSettlement.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_revenueSettlementReturnsNewIfNotFound = info.GetBoolean("_revenueSettlementReturnsNewIfNotFound");
			_alwaysFetchRevenueSettlement = info.GetBoolean("_alwaysFetchRevenueSettlement");
			_alreadyFetchedRevenueSettlement = info.GetBoolean("_alreadyFetchedRevenueSettlement");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettledRevenueFieldIndex)fieldIndex)
			{
				case SettledRevenueFieldIndex.RevenueRecognitionID:
					DesetupSyncRevenueRecognition(true, false);
					_alreadyFetchedRevenueRecognition = false;
					break;
				case SettledRevenueFieldIndex.RevenueSettlementID:
					DesetupSyncRevenueSettlement(true, false);
					_alreadyFetchedRevenueSettlement = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRevenueRecognition = (_revenueRecognition != null);
			_alreadyFetchedRevenueSettlement = (_revenueSettlement != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RevenueRecognition":
					toReturn.Add(Relations.RevenueRecognitionEntityUsingRevenueRecognitionID);
					break;
				case "RevenueSettlement":
					toReturn.Add(Relations.RevenueSettlementEntityUsingRevenueSettlementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_revenueRecognition", (!this.MarkedForDeletion?_revenueRecognition:null));
			info.AddValue("_revenueRecognitionReturnsNewIfNotFound", _revenueRecognitionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueRecognition", _alwaysFetchRevenueRecognition);
			info.AddValue("_alreadyFetchedRevenueRecognition", _alreadyFetchedRevenueRecognition);
			info.AddValue("_revenueSettlement", (!this.MarkedForDeletion?_revenueSettlement:null));
			info.AddValue("_revenueSettlementReturnsNewIfNotFound", _revenueSettlementReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRevenueSettlement", _alwaysFetchRevenueSettlement);
			info.AddValue("_alreadyFetchedRevenueSettlement", _alreadyFetchedRevenueSettlement);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RevenueRecognition":
					_alreadyFetchedRevenueRecognition = true;
					this.RevenueRecognition = (RevenueRecognitionEntity)entity;
					break;
				case "RevenueSettlement":
					_alreadyFetchedRevenueSettlement = true;
					this.RevenueSettlement = (RevenueSettlementEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RevenueRecognition":
					SetupSyncRevenueRecognition(relatedEntity);
					break;
				case "RevenueSettlement":
					SetupSyncRevenueSettlement(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RevenueRecognition":
					DesetupSyncRevenueRecognition(false, true);
					break;
				case "RevenueSettlement":
					DesetupSyncRevenueSettlement(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_revenueRecognition!=null)
			{
				toReturn.Add(_revenueRecognition);
			}
			if(_revenueSettlement!=null)
			{
				toReturn.Add(_revenueSettlement);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID)
		{
			return FetchUsingPK(revenueRecognitionID, revenueSettlementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(revenueRecognitionID, revenueSettlementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(revenueRecognitionID, revenueSettlementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(revenueRecognitionID, revenueSettlementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.RevenueRecognitionID, this.RevenueSettlementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettledRevenueRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'RevenueRecognitionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueRecognitionEntity' which is related to this entity.</returns>
		public RevenueRecognitionEntity GetSingleRevenueRecognition()
		{
			return GetSingleRevenueRecognition(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueRecognitionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueRecognitionEntity' which is related to this entity.</returns>
		public virtual RevenueRecognitionEntity GetSingleRevenueRecognition(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueRecognition || forceFetch || _alwaysFetchRevenueRecognition) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueRecognitionEntityUsingRevenueRecognitionID);
				RevenueRecognitionEntity newEntity = new RevenueRecognitionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RevenueRecognitionID);
				}
				if(fetchResult)
				{
					newEntity = (RevenueRecognitionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueRecognitionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueRecognition = newEntity;
				_alreadyFetchedRevenueRecognition = fetchResult;
			}
			return _revenueRecognition;
		}


		/// <summary> Retrieves the related entity of type 'RevenueSettlementEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RevenueSettlementEntity' which is related to this entity.</returns>
		public RevenueSettlementEntity GetSingleRevenueSettlement()
		{
			return GetSingleRevenueSettlement(false);
		}

		/// <summary> Retrieves the related entity of type 'RevenueSettlementEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RevenueSettlementEntity' which is related to this entity.</returns>
		public virtual RevenueSettlementEntity GetSingleRevenueSettlement(bool forceFetch)
		{
			if( ( !_alreadyFetchedRevenueSettlement || forceFetch || _alwaysFetchRevenueSettlement) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RevenueSettlementEntityUsingRevenueSettlementID);
				RevenueSettlementEntity newEntity = new RevenueSettlementEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RevenueSettlementID);
				}
				if(fetchResult)
				{
					newEntity = (RevenueSettlementEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_revenueSettlementReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RevenueSettlement = newEntity;
				_alreadyFetchedRevenueSettlement = fetchResult;
			}
			return _revenueSettlement;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RevenueRecognition", _revenueRecognition);
			toReturn.Add("RevenueSettlement", _revenueSettlement);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="validator">The validator object for this SettledRevenueEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(revenueRecognitionID, revenueSettlementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_revenueRecognitionReturnsNewIfNotFound = false;
			_revenueSettlementReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _revenueRecognition</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueRecognition(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueRecognition, new PropertyChangedEventHandler( OnRevenueRecognitionPropertyChanged ), "RevenueRecognition", VarioSL.Entities.RelationClasses.StaticSettledRevenueRelations.RevenueRecognitionEntityUsingRevenueRecognitionIDStatic, true, signalRelatedEntity, "SettledRevenues", resetFKFields, new int[] { (int)SettledRevenueFieldIndex.RevenueRecognitionID } );		
			_revenueRecognition = null;
		}
		
		/// <summary> setups the sync logic for member _revenueRecognition</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueRecognition(IEntityCore relatedEntity)
		{
			if(_revenueRecognition!=relatedEntity)
			{		
				DesetupSyncRevenueRecognition(true, true);
				_revenueRecognition = (RevenueRecognitionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueRecognition, new PropertyChangedEventHandler( OnRevenueRecognitionPropertyChanged ), "RevenueRecognition", VarioSL.Entities.RelationClasses.StaticSettledRevenueRelations.RevenueRecognitionEntityUsingRevenueRecognitionIDStatic, true, ref _alreadyFetchedRevenueRecognition, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueRecognitionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _revenueSettlement</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevenueSettlement(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _revenueSettlement, new PropertyChangedEventHandler( OnRevenueSettlementPropertyChanged ), "RevenueSettlement", VarioSL.Entities.RelationClasses.StaticSettledRevenueRelations.RevenueSettlementEntityUsingRevenueSettlementIDStatic, true, signalRelatedEntity, "SettledRevenues", resetFKFields, new int[] { (int)SettledRevenueFieldIndex.RevenueSettlementID } );		
			_revenueSettlement = null;
		}
		
		/// <summary> setups the sync logic for member _revenueSettlement</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevenueSettlement(IEntityCore relatedEntity)
		{
			if(_revenueSettlement!=relatedEntity)
			{		
				DesetupSyncRevenueSettlement(true, true);
				_revenueSettlement = (RevenueSettlementEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _revenueSettlement, new PropertyChangedEventHandler( OnRevenueSettlementPropertyChanged ), "RevenueSettlement", VarioSL.Entities.RelationClasses.StaticSettledRevenueRelations.RevenueSettlementEntityUsingRevenueSettlementIDStatic, true, ref _alreadyFetchedRevenueSettlement, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevenueSettlementPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="revenueRecognitionID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="revenueSettlementID">PK value for SettledRevenue which data should be fetched into this SettledRevenue object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 revenueRecognitionID, System.Int64 revenueSettlementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettledRevenueFieldIndex.RevenueRecognitionID].ForcedCurrentValueWrite(revenueRecognitionID);
				this.Fields[(int)SettledRevenueFieldIndex.RevenueSettlementID].ForcedCurrentValueWrite(revenueSettlementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettledRevenueDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettledRevenueEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettledRevenueRelations Relations
		{
			get	{ return new SettledRevenueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueRecognition'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueRecognition
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueRecognitionCollection(), (IEntityRelation)GetRelationsForField("RevenueRecognition")[0], (int)VarioSL.Entities.EntityType.SettledRevenueEntity, (int)VarioSL.Entities.EntityType.RevenueRecognitionEntity, 0, null, null, null, "RevenueRecognition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RevenueSettlement'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRevenueSettlement
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RevenueSettlementCollection(), (IEntityRelation)GetRelationsForField("RevenueSettlement")[0], (int)VarioSL.Entities.EntityType.SettledRevenueEntity, (int)VarioSL.Entities.EntityType.RevenueSettlementEntity, 0, null, null, null, "RevenueSettlement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The RevenueRecognitionID property of the Entity SettledRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SETTLEDREVENUE"."REVENUERECOGNITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)SettledRevenueFieldIndex.RevenueRecognitionID, true); }
			set	{ SetValue((int)SettledRevenueFieldIndex.RevenueRecognitionID, value, true); }
		}

		/// <summary> The RevenueSettlementID property of the Entity SettledRevenue<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_SETTLEDREVENUE"."REVENUESETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 RevenueSettlementID
		{
			get { return (System.Int64)GetValue((int)SettledRevenueFieldIndex.RevenueSettlementID, true); }
			set	{ SetValue((int)SettledRevenueFieldIndex.RevenueSettlementID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'RevenueRecognitionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueRecognition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueRecognitionEntity RevenueRecognition
		{
			get	{ return GetSingleRevenueRecognition(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueRecognition(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettledRevenues", "RevenueRecognition", _revenueRecognition, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueRecognition. When set to true, RevenueRecognition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueRecognition is accessed. You can always execute a forced fetch by calling GetSingleRevenueRecognition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueRecognition
		{
			get	{ return _alwaysFetchRevenueRecognition; }
			set	{ _alwaysFetchRevenueRecognition = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueRecognition already has been fetched. Setting this property to false when RevenueRecognition has been fetched
		/// will set RevenueRecognition to null as well. Setting this property to true while RevenueRecognition hasn't been fetched disables lazy loading for RevenueRecognition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueRecognition
		{
			get { return _alreadyFetchedRevenueRecognition;}
			set 
			{
				if(_alreadyFetchedRevenueRecognition && !value)
				{
					this.RevenueRecognition = null;
				}
				_alreadyFetchedRevenueRecognition = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueRecognition is not found
		/// in the database. When set to true, RevenueRecognition will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueRecognitionReturnsNewIfNotFound
		{
			get	{ return _revenueRecognitionReturnsNewIfNotFound; }
			set { _revenueRecognitionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RevenueSettlementEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRevenueSettlement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RevenueSettlementEntity RevenueSettlement
		{
			get	{ return GetSingleRevenueSettlement(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRevenueSettlement(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettledRevenues", "RevenueSettlement", _revenueSettlement, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RevenueSettlement. When set to true, RevenueSettlement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RevenueSettlement is accessed. You can always execute a forced fetch by calling GetSingleRevenueSettlement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRevenueSettlement
		{
			get	{ return _alwaysFetchRevenueSettlement; }
			set	{ _alwaysFetchRevenueSettlement = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RevenueSettlement already has been fetched. Setting this property to false when RevenueSettlement has been fetched
		/// will set RevenueSettlement to null as well. Setting this property to true while RevenueSettlement hasn't been fetched disables lazy loading for RevenueSettlement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRevenueSettlement
		{
			get { return _alreadyFetchedRevenueSettlement;}
			set 
			{
				if(_alreadyFetchedRevenueSettlement && !value)
				{
					this.RevenueSettlement = null;
				}
				_alreadyFetchedRevenueSettlement = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RevenueSettlement is not found
		/// in the database. When set to true, RevenueSettlement will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RevenueSettlementReturnsNewIfNotFound
		{
			get	{ return _revenueSettlementReturnsNewIfNotFound; }
			set { _revenueSettlementReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettledRevenueEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
