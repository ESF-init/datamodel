﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
#if !CF
using System.Runtime.Serialization;
#endif

namespace VarioSL.Entities.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Common base class which is the base class for all generated entities which aren't a subtype of another entity.</summary>
	[Serializable]
	public abstract partial class CommonEntityBase : EntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
        // __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		/// <summary>CTor</summary>
		protected CommonEntityBase()
		{
		}
						
		/// <summary> CTor</summary>
		protected CommonEntityBase(string name):base(name)
		{
		}
		
		/// <summary>Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CommonEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        protected override void OnSave()
        {
            base.OnSave();

            if (!IsNew)
            {
                IEntityField transactionCounter = Fields["TransactionCounter"];
                if (transactionCounter != null)
                {
                    decimal fieldValue = (decimal)GetCurrentFieldValue(transactionCounter.FieldIndex);
                    SetValue(transactionCounter.FieldIndex, ++fieldValue, false);
                }
            }

            var lastModified = Fields["LastModified"];
            if (lastModified != null)
                SetValue(lastModified.FieldIndex, DateTime.Now, false);

            var lastUser = Fields["LastUser"];
            if (lastUser != null)
                SetValue(lastUser.FieldIndex, string.IsNullOrEmpty(GetPrincipalOrActorName())
                    ? Environment.UserName
                    : GetPrincipalOrActorName());
		}

        public static string GetPrincipalOrActorName()
        {
            if (System.Web.HttpContext.Current != null)
            {
				var loggedInUserHeader = System.Web.HttpContext.Current.Request.Headers.GetValues("Logged-In-User");
                if (loggedInUserHeader != null && loggedInUserHeader.Length == 1)
                    return loggedInUserHeader[0];
			}
            
            dynamic identity = System.Threading.Thread.CurrentPrincipal.Identity;
            if (identity.Actor != null)
                return identity.Actor.Name;

            return System.Threading.Thread.CurrentPrincipal.Identity.Name;
        }
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
