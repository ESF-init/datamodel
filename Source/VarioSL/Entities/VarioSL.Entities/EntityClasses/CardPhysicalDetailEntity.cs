﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CardPhysicalDetail'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CardPhysicalDetailEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CardTicketCollection	_cardTicket;
		private bool	_alwaysFetchCardTicket, _alreadyFetchedCardTicket;
		private VarioSL.Entities.CollectionClasses.CardCollection	_cards;
		private bool	_alwaysFetchCards, _alreadyFetchedCards;
		private VarioSL.Entities.CollectionClasses.JobCardImportCollection	_jobCardImports;
		private bool	_alwaysFetchJobCardImports, _alreadyFetchedJobCardImports;
		private CardChipTypeEntity _cardChipType;
		private bool	_alwaysFetchCardChipType, _alreadyFetchedCardChipType, _cardChipTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardChipType</summary>
			public static readonly string CardChipType = "CardChipType";
			/// <summary>Member name CardTicket</summary>
			public static readonly string CardTicket = "CardTicket";
			/// <summary>Member name Cards</summary>
			public static readonly string Cards = "Cards";
			/// <summary>Member name JobCardImports</summary>
			public static readonly string JobCardImports = "JobCardImports";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CardPhysicalDetailEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CardPhysicalDetailEntity() :base("CardPhysicalDetailEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		public CardPhysicalDetailEntity(System.Int64 cardPhysicalDetailID):base("CardPhysicalDetailEntity")
		{
			InitClassFetch(cardPhysicalDetailID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CardPhysicalDetailEntity(System.Int64 cardPhysicalDetailID, IPrefetchPath prefetchPathToUse):base("CardPhysicalDetailEntity")
		{
			InitClassFetch(cardPhysicalDetailID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="validator">The custom validator object for this CardPhysicalDetailEntity</param>
		public CardPhysicalDetailEntity(System.Int64 cardPhysicalDetailID, IValidator validator):base("CardPhysicalDetailEntity")
		{
			InitClassFetch(cardPhysicalDetailID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CardPhysicalDetailEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardTicket = (VarioSL.Entities.CollectionClasses.CardTicketCollection)info.GetValue("_cardTicket", typeof(VarioSL.Entities.CollectionClasses.CardTicketCollection));
			_alwaysFetchCardTicket = info.GetBoolean("_alwaysFetchCardTicket");
			_alreadyFetchedCardTicket = info.GetBoolean("_alreadyFetchedCardTicket");

			_cards = (VarioSL.Entities.CollectionClasses.CardCollection)info.GetValue("_cards", typeof(VarioSL.Entities.CollectionClasses.CardCollection));
			_alwaysFetchCards = info.GetBoolean("_alwaysFetchCards");
			_alreadyFetchedCards = info.GetBoolean("_alreadyFetchedCards");

			_jobCardImports = (VarioSL.Entities.CollectionClasses.JobCardImportCollection)info.GetValue("_jobCardImports", typeof(VarioSL.Entities.CollectionClasses.JobCardImportCollection));
			_alwaysFetchJobCardImports = info.GetBoolean("_alwaysFetchJobCardImports");
			_alreadyFetchedJobCardImports = info.GetBoolean("_alreadyFetchedJobCardImports");
			_cardChipType = (CardChipTypeEntity)info.GetValue("_cardChipType", typeof(CardChipTypeEntity));
			if(_cardChipType!=null)
			{
				_cardChipType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardChipTypeReturnsNewIfNotFound = info.GetBoolean("_cardChipTypeReturnsNewIfNotFound");
			_alwaysFetchCardChipType = info.GetBoolean("_alwaysFetchCardChipType");
			_alreadyFetchedCardChipType = info.GetBoolean("_alreadyFetchedCardChipType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CardPhysicalDetailFieldIndex)fieldIndex)
			{
				case CardPhysicalDetailFieldIndex.ChipTypeID:
					DesetupSyncCardChipType(true, false);
					_alreadyFetchedCardChipType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardTicket = (_cardTicket.Count > 0);
			_alreadyFetchedCards = (_cards.Count > 0);
			_alreadyFetchedJobCardImports = (_jobCardImports.Count > 0);
			_alreadyFetchedCardChipType = (_cardChipType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardChipType":
					toReturn.Add(Relations.CardChipTypeEntityUsingChipTypeID);
					break;
				case "CardTicket":
					toReturn.Add(Relations.CardTicketEntityUsingCardPhysicalDetailID);
					break;
				case "Cards":
					toReturn.Add(Relations.CardEntityUsingCardPhysicalDetailID);
					break;
				case "JobCardImports":
					toReturn.Add(Relations.JobCardImportEntityUsingCardPhysicalDetailID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardTicket", (!this.MarkedForDeletion?_cardTicket:null));
			info.AddValue("_alwaysFetchCardTicket", _alwaysFetchCardTicket);
			info.AddValue("_alreadyFetchedCardTicket", _alreadyFetchedCardTicket);
			info.AddValue("_cards", (!this.MarkedForDeletion?_cards:null));
			info.AddValue("_alwaysFetchCards", _alwaysFetchCards);
			info.AddValue("_alreadyFetchedCards", _alreadyFetchedCards);
			info.AddValue("_jobCardImports", (!this.MarkedForDeletion?_jobCardImports:null));
			info.AddValue("_alwaysFetchJobCardImports", _alwaysFetchJobCardImports);
			info.AddValue("_alreadyFetchedJobCardImports", _alreadyFetchedJobCardImports);
			info.AddValue("_cardChipType", (!this.MarkedForDeletion?_cardChipType:null));
			info.AddValue("_cardChipTypeReturnsNewIfNotFound", _cardChipTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardChipType", _alwaysFetchCardChipType);
			info.AddValue("_alreadyFetchedCardChipType", _alreadyFetchedCardChipType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardChipType":
					_alreadyFetchedCardChipType = true;
					this.CardChipType = (CardChipTypeEntity)entity;
					break;
				case "CardTicket":
					_alreadyFetchedCardTicket = true;
					if(entity!=null)
					{
						this.CardTicket.Add((CardTicketEntity)entity);
					}
					break;
				case "Cards":
					_alreadyFetchedCards = true;
					if(entity!=null)
					{
						this.Cards.Add((CardEntity)entity);
					}
					break;
				case "JobCardImports":
					_alreadyFetchedJobCardImports = true;
					if(entity!=null)
					{
						this.JobCardImports.Add((JobCardImportEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardChipType":
					SetupSyncCardChipType(relatedEntity);
					break;
				case "CardTicket":
					_cardTicket.Add((CardTicketEntity)relatedEntity);
					break;
				case "Cards":
					_cards.Add((CardEntity)relatedEntity);
					break;
				case "JobCardImports":
					_jobCardImports.Add((JobCardImportEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardChipType":
					DesetupSyncCardChipType(false, true);
					break;
				case "CardTicket":
					this.PerformRelatedEntityRemoval(_cardTicket, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Cards":
					this.PerformRelatedEntityRemoval(_cards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "JobCardImports":
					this.PerformRelatedEntityRemoval(_jobCardImports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardChipType!=null)
			{
				toReturn.Add(_cardChipType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cardTicket);
			toReturn.Add(_cards);
			toReturn.Add(_jobCardImports);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardPhysicalDetailID)
		{
			return FetchUsingPK(cardPhysicalDetailID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardPhysicalDetailID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cardPhysicalDetailID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardPhysicalDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cardPhysicalDetailID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cardPhysicalDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cardPhysicalDetailID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CardPhysicalDetailID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CardPhysicalDetailRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch)
		{
			return GetMultiCardTicket(forceFetch, _cardTicket.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardTicket(forceFetch, _cardTicket.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardTicket(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTicket(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardTicket || forceFetch || _alwaysFetchCardTicket) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardTicket);
				_cardTicket.SuppressClearInGetMulti=!forceFetch;
				_cardTicket.EntityFactoryToUse = entityFactoryToUse;
				_cardTicket.GetMultiManyToOne(null, this, null, filter);
				_cardTicket.SuppressClearInGetMulti=false;
				_alreadyFetchedCardTicket = true;
			}
			return _cardTicket;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardTicket'. These settings will be taken into account
		/// when the property CardTicket is requested or GetMultiCardTicket is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardTicket(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardTicket.SortClauses=sortClauses;
			_cardTicket.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCards(forceFetch, _cards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection GetMultiCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCards || forceFetch || _alwaysFetchCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cards);
				_cards.SuppressClearInGetMulti=!forceFetch;
				_cards.EntityFactoryToUse = entityFactoryToUse;
				_cards.GetMultiManyToOne(null, null, null, null, null, this, null, filter);
				_cards.SuppressClearInGetMulti=false;
				_alreadyFetchedCards = true;
			}
			return _cards;
		}

		/// <summary> Sets the collection parameters for the collection for 'Cards'. These settings will be taken into account
		/// when the property Cards is requested or GetMultiCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cards.SortClauses=sortClauses;
			_cards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'JobCardImportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiJobCardImports(forceFetch, _jobCardImports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiJobCardImports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection GetMultiJobCardImports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedJobCardImports || forceFetch || _alwaysFetchJobCardImports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_jobCardImports);
				_jobCardImports.SuppressClearInGetMulti=!forceFetch;
				_jobCardImports.EntityFactoryToUse = entityFactoryToUse;
				_jobCardImports.GetMultiManyToOne(this, null, null, filter);
				_jobCardImports.SuppressClearInGetMulti=false;
				_alreadyFetchedJobCardImports = true;
			}
			return _jobCardImports;
		}

		/// <summary> Sets the collection parameters for the collection for 'JobCardImports'. These settings will be taken into account
		/// when the property JobCardImports is requested or GetMultiJobCardImports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersJobCardImports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_jobCardImports.SortClauses=sortClauses;
			_jobCardImports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardChipTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardChipTypeEntity' which is related to this entity.</returns>
		public CardChipTypeEntity GetSingleCardChipType()
		{
			return GetSingleCardChipType(false);
		}

		/// <summary> Retrieves the related entity of type 'CardChipTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardChipTypeEntity' which is related to this entity.</returns>
		public virtual CardChipTypeEntity GetSingleCardChipType(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardChipType || forceFetch || _alwaysFetchCardChipType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardChipTypeEntityUsingChipTypeID);
				CardChipTypeEntity newEntity = new CardChipTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ChipTypeID);
				}
				if(fetchResult)
				{
					newEntity = (CardChipTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardChipTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardChipType = newEntity;
				_alreadyFetchedCardChipType = fetchResult;
			}
			return _cardChipType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardChipType", _cardChipType);
			toReturn.Add("CardTicket", _cardTicket);
			toReturn.Add("Cards", _cards);
			toReturn.Add("JobCardImports", _jobCardImports);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="validator">The validator object for this CardPhysicalDetailEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cardPhysicalDetailID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cardPhysicalDetailID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cardTicket = new VarioSL.Entities.CollectionClasses.CardTicketCollection();
			_cardTicket.SetContainingEntityInfo(this, "CardPhysicalDetail");

			_cards = new VarioSL.Entities.CollectionClasses.CardCollection();
			_cards.SetContainingEntityInfo(this, "CardPhysicalDetail");

			_jobCardImports = new VarioSL.Entities.CollectionClasses.JobCardImportCollection();
			_jobCardImports.SetContainingEntityInfo(this, "CardPhysicalDetail");
			_cardChipTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BodyType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChipTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPrinted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardChipType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardChipType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardChipType, new PropertyChangedEventHandler( OnCardChipTypePropertyChanged ), "CardChipType", VarioSL.Entities.RelationClasses.StaticCardPhysicalDetailRelations.CardChipTypeEntityUsingChipTypeIDStatic, true, signalRelatedEntity, "CardPhysicalDetails", resetFKFields, new int[] { (int)CardPhysicalDetailFieldIndex.ChipTypeID } );		
			_cardChipType = null;
		}
		
		/// <summary> setups the sync logic for member _cardChipType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardChipType(IEntityCore relatedEntity)
		{
			if(_cardChipType!=relatedEntity)
			{		
				DesetupSyncCardChipType(true, true);
				_cardChipType = (CardChipTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardChipType, new PropertyChangedEventHandler( OnCardChipTypePropertyChanged ), "CardChipType", VarioSL.Entities.RelationClasses.StaticCardPhysicalDetailRelations.CardChipTypeEntityUsingChipTypeIDStatic, true, ref _alreadyFetchedCardChipType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardChipTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cardPhysicalDetailID">PK value for CardPhysicalDetail which data should be fetched into this CardPhysicalDetail object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cardPhysicalDetailID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CardPhysicalDetailFieldIndex.CardPhysicalDetailID].ForcedCurrentValueWrite(cardPhysicalDetailID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCardPhysicalDetailDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CardPhysicalDetailEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CardPhysicalDetailRelations Relations
		{
			get	{ return new CardPhysicalDetailRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTicket
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketCollection(), (IEntityRelation)GetRelationsForField("CardTicket")[0], (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, (int)VarioSL.Entities.EntityType.CardTicketEntity, 0, null, null, null, "CardTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Cards")[0], (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Cards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'JobCardImport' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJobCardImports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCardImportCollection(), (IEntityRelation)GetRelationsForField("JobCardImports")[0], (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, (int)VarioSL.Entities.EntityType.JobCardImportEntity, 0, null, null, null, "JobCardImports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardChipType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardChipType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardChipTypeCollection(), (IEntityRelation)GetRelationsForField("CardChipType")[0], (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, (int)VarioSL.Entities.EntityType.CardChipTypeEntity, 0, null, null, null, "CardChipType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BodyType property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."BODYTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CardBodyType BodyType
		{
			get { return (VarioSL.Entities.Enumerations.CardBodyType)GetValue((int)CardPhysicalDetailFieldIndex.BodyType, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.BodyType, value, true); }
		}

		/// <summary> The CardPhysicalDetailID property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."CARDPHYSICALDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CardPhysicalDetailID
		{
			get { return (System.Int64)GetValue((int)CardPhysicalDetailFieldIndex.CardPhysicalDetailID, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.CardPhysicalDetailID, value, true); }
		}

		/// <summary> The ChipTypeID property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."CHIPTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ChipTypeID
		{
			get { return (System.Int64)GetValue((int)CardPhysicalDetailFieldIndex.ChipTypeID, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.ChipTypeID, value, true); }
		}

		/// <summary> The Description property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CardPhysicalDetailFieldIndex.Description, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.Description, value, true); }
		}

		/// <summary> The IsPrinted property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."ISPRINTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPrinted
		{
			get { return (System.Boolean)GetValue((int)CardPhysicalDetailFieldIndex.IsPrinted, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.IsPrinted, value, true); }
		}

		/// <summary> The LastModified property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CardPhysicalDetailFieldIndex.LastModified, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CardPhysicalDetailFieldIndex.LastUser, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CardPhysicalDetail<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CARDPHYSICALDETAIL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CardPhysicalDetailFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CardPhysicalDetailFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection CardTicket
		{
			get	{ return GetMultiCardTicket(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardTicket. When set to true, CardTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTicket is accessed. You can always execute/ a forced fetch by calling GetMultiCardTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTicket
		{
			get	{ return _alwaysFetchCardTicket; }
			set	{ _alwaysFetchCardTicket = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardTicket already has been fetched. Setting this property to false when CardTicket has been fetched
		/// will clear the CardTicket collection well. Setting this property to true while CardTicket hasn't been fetched disables lazy loading for CardTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTicket
		{
			get { return _alreadyFetchedCardTicket;}
			set 
			{
				if(_alreadyFetchedCardTicket && !value && (_cardTicket != null))
				{
					_cardTicket.Clear();
				}
				_alreadyFetchedCardTicket = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardCollection Cards
		{
			get	{ return GetMultiCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Cards. When set to true, Cards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Cards is accessed. You can always execute/ a forced fetch by calling GetMultiCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCards
		{
			get	{ return _alwaysFetchCards; }
			set	{ _alwaysFetchCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Cards already has been fetched. Setting this property to false when Cards has been fetched
		/// will clear the Cards collection well. Setting this property to true while Cards hasn't been fetched disables lazy loading for Cards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCards
		{
			get { return _alreadyFetchedCards;}
			set 
			{
				if(_alreadyFetchedCards && !value && (_cards != null))
				{
					_cards.Clear();
				}
				_alreadyFetchedCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'JobCardImportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiJobCardImports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.JobCardImportCollection JobCardImports
		{
			get	{ return GetMultiJobCardImports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for JobCardImports. When set to true, JobCardImports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time JobCardImports is accessed. You can always execute/ a forced fetch by calling GetMultiJobCardImports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJobCardImports
		{
			get	{ return _alwaysFetchJobCardImports; }
			set	{ _alwaysFetchJobCardImports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property JobCardImports already has been fetched. Setting this property to false when JobCardImports has been fetched
		/// will clear the JobCardImports collection well. Setting this property to true while JobCardImports hasn't been fetched disables lazy loading for JobCardImports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJobCardImports
		{
			get { return _alreadyFetchedJobCardImports;}
			set 
			{
				if(_alreadyFetchedJobCardImports && !value && (_jobCardImports != null))
				{
					_jobCardImports.Clear();
				}
				_alreadyFetchedJobCardImports = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardChipTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardChipType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardChipTypeEntity CardChipType
		{
			get	{ return GetSingleCardChipType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardChipType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CardPhysicalDetails", "CardChipType", _cardChipType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardChipType. When set to true, CardChipType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardChipType is accessed. You can always execute a forced fetch by calling GetSingleCardChipType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardChipType
		{
			get	{ return _alwaysFetchCardChipType; }
			set	{ _alwaysFetchCardChipType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardChipType already has been fetched. Setting this property to false when CardChipType has been fetched
		/// will set CardChipType to null as well. Setting this property to true while CardChipType hasn't been fetched disables lazy loading for CardChipType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardChipType
		{
			get { return _alreadyFetchedCardChipType;}
			set 
			{
				if(_alreadyFetchedCardChipType && !value)
				{
					this.CardChipType = null;
				}
				_alreadyFetchedCardChipType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardChipType is not found
		/// in the database. When set to true, CardChipType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardChipTypeReturnsNewIfNotFound
		{
			get	{ return _cardChipTypeReturnsNewIfNotFound; }
			set { _cardChipTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
