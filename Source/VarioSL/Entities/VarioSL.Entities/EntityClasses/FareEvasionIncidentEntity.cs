﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareEvasionIncident'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareEvasionIncidentEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private BinaryDataEntity _binaryData;
		private bool	_alwaysFetchBinaryData, _alreadyFetchedBinaryData, _binaryDataReturnsNewIfNotFound;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DebtorEntity _debtor;
		private bool	_alwaysFetchDebtor, _alreadyFetchedDebtor, _debtorReturnsNewIfNotFound;
		private FareEvasionCategoryEntity _fareEvasionCategory;
		private bool	_alwaysFetchFareEvasionCategory, _alreadyFetchedFareEvasionCategory, _fareEvasionCategoryReturnsNewIfNotFound;
		private FareEvasionReasonEntity _fareEvasionReason;
		private bool	_alwaysFetchFareEvasionReason, _alreadyFetchedFareEvasionReason, _fareEvasionReasonReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private FareChangeCauseEntity _fareChangeCause;
		private bool	_alwaysFetchFareChangeCause, _alreadyFetchedFareChangeCause, _fareChangeCauseReturnsNewIfNotFound;
		private FareEvasionBehaviourEntity _fareEvasionBehaviour;
		private bool	_alwaysFetchFareEvasionBehaviour, _alreadyFetchedFareEvasionBehaviour, _fareEvasionBehaviourReturnsNewIfNotFound;
		private IdentificationTypeEntity _identificationType;
		private bool	_alwaysFetchIdentificationType, _alreadyFetchedIdentificationType, _identificationTypeReturnsNewIfNotFound;
		private InspectionReportEntity _inspectionReport;
		private bool	_alwaysFetchInspectionReport, _alreadyFetchedInspectionReport, _inspectionReportReturnsNewIfNotFound;
		private PersonEntity _guardian;
		private bool	_alwaysFetchGuardian, _alreadyFetchedGuardian, _guardianReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;
		private TransportCompanyEntity _transportCompany;
		private bool	_alwaysFetchTransportCompany, _alreadyFetchedTransportCompany, _transportCompanyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BinaryData</summary>
			public static readonly string BinaryData = "BinaryData";
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Debtor</summary>
			public static readonly string Debtor = "Debtor";
			/// <summary>Member name FareEvasionCategory</summary>
			public static readonly string FareEvasionCategory = "FareEvasionCategory";
			/// <summary>Member name FareEvasionReason</summary>
			public static readonly string FareEvasionReason = "FareEvasionReason";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name FareChangeCause</summary>
			public static readonly string FareChangeCause = "FareChangeCause";
			/// <summary>Member name FareEvasionBehaviour</summary>
			public static readonly string FareEvasionBehaviour = "FareEvasionBehaviour";
			/// <summary>Member name IdentificationType</summary>
			public static readonly string IdentificationType = "IdentificationType";
			/// <summary>Member name InspectionReport</summary>
			public static readonly string InspectionReport = "InspectionReport";
			/// <summary>Member name Guardian</summary>
			public static readonly string Guardian = "Guardian";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name TransportCompany</summary>
			public static readonly string TransportCompany = "TransportCompany";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareEvasionIncidentEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareEvasionIncidentEntity() :base("FareEvasionIncidentEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		public FareEvasionIncidentEntity(System.Int64 fareEvasionIncidentID):base("FareEvasionIncidentEntity")
		{
			InitClassFetch(fareEvasionIncidentID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareEvasionIncidentEntity(System.Int64 fareEvasionIncidentID, IPrefetchPath prefetchPathToUse):base("FareEvasionIncidentEntity")
		{
			InitClassFetch(fareEvasionIncidentID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="validator">The custom validator object for this FareEvasionIncidentEntity</param>
		public FareEvasionIncidentEntity(System.Int64 fareEvasionIncidentID, IValidator validator):base("FareEvasionIncidentEntity")
		{
			InitClassFetch(fareEvasionIncidentID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareEvasionIncidentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_binaryData = (BinaryDataEntity)info.GetValue("_binaryData", typeof(BinaryDataEntity));
			if(_binaryData!=null)
			{
				_binaryData.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_binaryDataReturnsNewIfNotFound = info.GetBoolean("_binaryDataReturnsNewIfNotFound");
			_alwaysFetchBinaryData = info.GetBoolean("_alwaysFetchBinaryData");
			_alreadyFetchedBinaryData = info.GetBoolean("_alreadyFetchedBinaryData");

			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_debtor = (DebtorEntity)info.GetValue("_debtor", typeof(DebtorEntity));
			if(_debtor!=null)
			{
				_debtor.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_debtorReturnsNewIfNotFound = info.GetBoolean("_debtorReturnsNewIfNotFound");
			_alwaysFetchDebtor = info.GetBoolean("_alwaysFetchDebtor");
			_alreadyFetchedDebtor = info.GetBoolean("_alreadyFetchedDebtor");

			_fareEvasionCategory = (FareEvasionCategoryEntity)info.GetValue("_fareEvasionCategory", typeof(FareEvasionCategoryEntity));
			if(_fareEvasionCategory!=null)
			{
				_fareEvasionCategory.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionCategoryReturnsNewIfNotFound = info.GetBoolean("_fareEvasionCategoryReturnsNewIfNotFound");
			_alwaysFetchFareEvasionCategory = info.GetBoolean("_alwaysFetchFareEvasionCategory");
			_alreadyFetchedFareEvasionCategory = info.GetBoolean("_alreadyFetchedFareEvasionCategory");

			_fareEvasionReason = (FareEvasionReasonEntity)info.GetValue("_fareEvasionReason", typeof(FareEvasionReasonEntity));
			if(_fareEvasionReason!=null)
			{
				_fareEvasionReason.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionReasonReturnsNewIfNotFound = info.GetBoolean("_fareEvasionReasonReturnsNewIfNotFound");
			_alwaysFetchFareEvasionReason = info.GetBoolean("_alwaysFetchFareEvasionReason");
			_alreadyFetchedFareEvasionReason = info.GetBoolean("_alreadyFetchedFareEvasionReason");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_fareChangeCause = (FareChangeCauseEntity)info.GetValue("_fareChangeCause", typeof(FareChangeCauseEntity));
			if(_fareChangeCause!=null)
			{
				_fareChangeCause.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareChangeCauseReturnsNewIfNotFound = info.GetBoolean("_fareChangeCauseReturnsNewIfNotFound");
			_alwaysFetchFareChangeCause = info.GetBoolean("_alwaysFetchFareChangeCause");
			_alreadyFetchedFareChangeCause = info.GetBoolean("_alreadyFetchedFareChangeCause");

			_fareEvasionBehaviour = (FareEvasionBehaviourEntity)info.GetValue("_fareEvasionBehaviour", typeof(FareEvasionBehaviourEntity));
			if(_fareEvasionBehaviour!=null)
			{
				_fareEvasionBehaviour.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_fareEvasionBehaviourReturnsNewIfNotFound = info.GetBoolean("_fareEvasionBehaviourReturnsNewIfNotFound");
			_alwaysFetchFareEvasionBehaviour = info.GetBoolean("_alwaysFetchFareEvasionBehaviour");
			_alreadyFetchedFareEvasionBehaviour = info.GetBoolean("_alreadyFetchedFareEvasionBehaviour");

			_identificationType = (IdentificationTypeEntity)info.GetValue("_identificationType", typeof(IdentificationTypeEntity));
			if(_identificationType!=null)
			{
				_identificationType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_identificationTypeReturnsNewIfNotFound = info.GetBoolean("_identificationTypeReturnsNewIfNotFound");
			_alwaysFetchIdentificationType = info.GetBoolean("_alwaysFetchIdentificationType");
			_alreadyFetchedIdentificationType = info.GetBoolean("_alreadyFetchedIdentificationType");

			_inspectionReport = (InspectionReportEntity)info.GetValue("_inspectionReport", typeof(InspectionReportEntity));
			if(_inspectionReport!=null)
			{
				_inspectionReport.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_inspectionReportReturnsNewIfNotFound = info.GetBoolean("_inspectionReportReturnsNewIfNotFound");
			_alwaysFetchInspectionReport = info.GetBoolean("_alwaysFetchInspectionReport");
			_alreadyFetchedInspectionReport = info.GetBoolean("_alreadyFetchedInspectionReport");

			_guardian = (PersonEntity)info.GetValue("_guardian", typeof(PersonEntity));
			if(_guardian!=null)
			{
				_guardian.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_guardianReturnsNewIfNotFound = info.GetBoolean("_guardianReturnsNewIfNotFound");
			_alwaysFetchGuardian = info.GetBoolean("_alwaysFetchGuardian");
			_alreadyFetchedGuardian = info.GetBoolean("_alreadyFetchedGuardian");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");

			_transportCompany = (TransportCompanyEntity)info.GetValue("_transportCompany", typeof(TransportCompanyEntity));
			if(_transportCompany!=null)
			{
				_transportCompany.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_transportCompanyReturnsNewIfNotFound = info.GetBoolean("_transportCompanyReturnsNewIfNotFound");
			_alwaysFetchTransportCompany = info.GetBoolean("_alwaysFetchTransportCompany");
			_alreadyFetchedTransportCompany = info.GetBoolean("_alreadyFetchedTransportCompany");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareEvasionIncidentFieldIndex)fieldIndex)
			{
				case FareEvasionIncidentFieldIndex.BinaryDataID:
					DesetupSyncBinaryData(true, false);
					_alreadyFetchedBinaryData = false;
					break;
				case FareEvasionIncidentFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case FareEvasionIncidentFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case FareEvasionIncidentFieldIndex.DebtorID:
					DesetupSyncDebtor(true, false);
					_alreadyFetchedDebtor = false;
					break;
				case FareEvasionIncidentFieldIndex.FareChangeCauseID:
					DesetupSyncFareChangeCause(true, false);
					_alreadyFetchedFareChangeCause = false;
					break;
				case FareEvasionIncidentFieldIndex.FareEvasionBehaviourID:
					DesetupSyncFareEvasionBehaviour(true, false);
					_alreadyFetchedFareEvasionBehaviour = false;
					break;
				case FareEvasionIncidentFieldIndex.FareEvasionCategoryID:
					DesetupSyncFareEvasionCategory(true, false);
					_alreadyFetchedFareEvasionCategory = false;
					break;
				case FareEvasionIncidentFieldIndex.FareEvasionReasonID:
					DesetupSyncFareEvasionReason(true, false);
					_alreadyFetchedFareEvasionReason = false;
					break;
				case FareEvasionIncidentFieldIndex.GuardianID:
					DesetupSyncGuardian(true, false);
					_alreadyFetchedGuardian = false;
					break;
				case FareEvasionIncidentFieldIndex.IdentificationTypeID:
					DesetupSyncIdentificationType(true, false);
					_alreadyFetchedIdentificationType = false;
					break;
				case FareEvasionIncidentFieldIndex.InspectionReportID:
					DesetupSyncInspectionReport(true, false);
					_alreadyFetchedInspectionReport = false;
					break;
				case FareEvasionIncidentFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				case FareEvasionIncidentFieldIndex.TransportCompanyID:
					DesetupSyncTransportCompany(true, false);
					_alreadyFetchedTransportCompany = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBinaryData = (_binaryData != null);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDebtor = (_debtor != null);
			_alreadyFetchedFareEvasionCategory = (_fareEvasionCategory != null);
			_alreadyFetchedFareEvasionReason = (_fareEvasionReason != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedFareChangeCause = (_fareChangeCause != null);
			_alreadyFetchedFareEvasionBehaviour = (_fareEvasionBehaviour != null);
			_alreadyFetchedIdentificationType = (_identificationType != null);
			_alreadyFetchedInspectionReport = (_inspectionReport != null);
			_alreadyFetchedGuardian = (_guardian != null);
			_alreadyFetchedPerson = (_person != null);
			_alreadyFetchedTransportCompany = (_transportCompany != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BinaryData":
					toReturn.Add(Relations.BinaryDataEntityUsingBinaryDataID);
					break;
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "Debtor":
					toReturn.Add(Relations.DebtorEntityUsingDebtorID);
					break;
				case "FareEvasionCategory":
					toReturn.Add(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
					break;
				case "FareEvasionReason":
					toReturn.Add(Relations.FareEvasionReasonEntityUsingFareEvasionReasonID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "FareChangeCause":
					toReturn.Add(Relations.FareChangeCauseEntityUsingFareChangeCauseID);
					break;
				case "FareEvasionBehaviour":
					toReturn.Add(Relations.FareEvasionBehaviourEntityUsingFareEvasionBehaviourID);
					break;
				case "IdentificationType":
					toReturn.Add(Relations.IdentificationTypeEntityUsingIdentificationTypeID);
					break;
				case "InspectionReport":
					toReturn.Add(Relations.InspectionReportEntityUsingInspectionReportID);
					break;
				case "Guardian":
					toReturn.Add(Relations.PersonEntityUsingGuardianID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "TransportCompany":
					toReturn.Add(Relations.TransportCompanyEntityUsingTransportCompanyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_binaryData", (!this.MarkedForDeletion?_binaryData:null));
			info.AddValue("_binaryDataReturnsNewIfNotFound", _binaryDataReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBinaryData", _alwaysFetchBinaryData);
			info.AddValue("_alreadyFetchedBinaryData", _alreadyFetchedBinaryData);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_debtor", (!this.MarkedForDeletion?_debtor:null));
			info.AddValue("_debtorReturnsNewIfNotFound", _debtorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDebtor", _alwaysFetchDebtor);
			info.AddValue("_alreadyFetchedDebtor", _alreadyFetchedDebtor);
			info.AddValue("_fareEvasionCategory", (!this.MarkedForDeletion?_fareEvasionCategory:null));
			info.AddValue("_fareEvasionCategoryReturnsNewIfNotFound", _fareEvasionCategoryReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionCategory", _alwaysFetchFareEvasionCategory);
			info.AddValue("_alreadyFetchedFareEvasionCategory", _alreadyFetchedFareEvasionCategory);
			info.AddValue("_fareEvasionReason", (!this.MarkedForDeletion?_fareEvasionReason:null));
			info.AddValue("_fareEvasionReasonReturnsNewIfNotFound", _fareEvasionReasonReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionReason", _alwaysFetchFareEvasionReason);
			info.AddValue("_alreadyFetchedFareEvasionReason", _alreadyFetchedFareEvasionReason);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_fareChangeCause", (!this.MarkedForDeletion?_fareChangeCause:null));
			info.AddValue("_fareChangeCauseReturnsNewIfNotFound", _fareChangeCauseReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareChangeCause", _alwaysFetchFareChangeCause);
			info.AddValue("_alreadyFetchedFareChangeCause", _alreadyFetchedFareChangeCause);
			info.AddValue("_fareEvasionBehaviour", (!this.MarkedForDeletion?_fareEvasionBehaviour:null));
			info.AddValue("_fareEvasionBehaviourReturnsNewIfNotFound", _fareEvasionBehaviourReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFareEvasionBehaviour", _alwaysFetchFareEvasionBehaviour);
			info.AddValue("_alreadyFetchedFareEvasionBehaviour", _alreadyFetchedFareEvasionBehaviour);
			info.AddValue("_identificationType", (!this.MarkedForDeletion?_identificationType:null));
			info.AddValue("_identificationTypeReturnsNewIfNotFound", _identificationTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIdentificationType", _alwaysFetchIdentificationType);
			info.AddValue("_alreadyFetchedIdentificationType", _alreadyFetchedIdentificationType);
			info.AddValue("_inspectionReport", (!this.MarkedForDeletion?_inspectionReport:null));
			info.AddValue("_inspectionReportReturnsNewIfNotFound", _inspectionReportReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchInspectionReport", _alwaysFetchInspectionReport);
			info.AddValue("_alreadyFetchedInspectionReport", _alreadyFetchedInspectionReport);
			info.AddValue("_guardian", (!this.MarkedForDeletion?_guardian:null));
			info.AddValue("_guardianReturnsNewIfNotFound", _guardianReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchGuardian", _alwaysFetchGuardian);
			info.AddValue("_alreadyFetchedGuardian", _alreadyFetchedGuardian);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);
			info.AddValue("_transportCompany", (!this.MarkedForDeletion?_transportCompany:null));
			info.AddValue("_transportCompanyReturnsNewIfNotFound", _transportCompanyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTransportCompany", _alwaysFetchTransportCompany);
			info.AddValue("_alreadyFetchedTransportCompany", _alreadyFetchedTransportCompany);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BinaryData":
					_alreadyFetchedBinaryData = true;
					this.BinaryData = (BinaryDataEntity)entity;
					break;
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Debtor":
					_alreadyFetchedDebtor = true;
					this.Debtor = (DebtorEntity)entity;
					break;
				case "FareEvasionCategory":
					_alreadyFetchedFareEvasionCategory = true;
					this.FareEvasionCategory = (FareEvasionCategoryEntity)entity;
					break;
				case "FareEvasionReason":
					_alreadyFetchedFareEvasionReason = true;
					this.FareEvasionReason = (FareEvasionReasonEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "FareChangeCause":
					_alreadyFetchedFareChangeCause = true;
					this.FareChangeCause = (FareChangeCauseEntity)entity;
					break;
				case "FareEvasionBehaviour":
					_alreadyFetchedFareEvasionBehaviour = true;
					this.FareEvasionBehaviour = (FareEvasionBehaviourEntity)entity;
					break;
				case "IdentificationType":
					_alreadyFetchedIdentificationType = true;
					this.IdentificationType = (IdentificationTypeEntity)entity;
					break;
				case "InspectionReport":
					_alreadyFetchedInspectionReport = true;
					this.InspectionReport = (InspectionReportEntity)entity;
					break;
				case "Guardian":
					_alreadyFetchedGuardian = true;
					this.Guardian = (PersonEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "TransportCompany":
					_alreadyFetchedTransportCompany = true;
					this.TransportCompany = (TransportCompanyEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BinaryData":
					SetupSyncBinaryData(relatedEntity);
					break;
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Debtor":
					SetupSyncDebtor(relatedEntity);
					break;
				case "FareEvasionCategory":
					SetupSyncFareEvasionCategory(relatedEntity);
					break;
				case "FareEvasionReason":
					SetupSyncFareEvasionReason(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "FareChangeCause":
					SetupSyncFareChangeCause(relatedEntity);
					break;
				case "FareEvasionBehaviour":
					SetupSyncFareEvasionBehaviour(relatedEntity);
					break;
				case "IdentificationType":
					SetupSyncIdentificationType(relatedEntity);
					break;
				case "InspectionReport":
					SetupSyncInspectionReport(relatedEntity);
					break;
				case "Guardian":
					SetupSyncGuardian(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "TransportCompany":
					SetupSyncTransportCompany(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BinaryData":
					DesetupSyncBinaryData(false, true);
					break;
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Debtor":
					DesetupSyncDebtor(false, true);
					break;
				case "FareEvasionCategory":
					DesetupSyncFareEvasionCategory(false, true);
					break;
				case "FareEvasionReason":
					DesetupSyncFareEvasionReason(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "FareChangeCause":
					DesetupSyncFareChangeCause(false, true);
					break;
				case "FareEvasionBehaviour":
					DesetupSyncFareEvasionBehaviour(false, true);
					break;
				case "IdentificationType":
					DesetupSyncIdentificationType(false, true);
					break;
				case "InspectionReport":
					DesetupSyncInspectionReport(false, true);
					break;
				case "Guardian":
					DesetupSyncGuardian(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "TransportCompany":
					DesetupSyncTransportCompany(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_binaryData!=null)
			{
				toReturn.Add(_binaryData);
			}
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_debtor!=null)
			{
				toReturn.Add(_debtor);
			}
			if(_fareEvasionCategory!=null)
			{
				toReturn.Add(_fareEvasionCategory);
			}
			if(_fareEvasionReason!=null)
			{
				toReturn.Add(_fareEvasionReason);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_fareChangeCause!=null)
			{
				toReturn.Add(_fareChangeCause);
			}
			if(_fareEvasionBehaviour!=null)
			{
				toReturn.Add(_fareEvasionBehaviour);
			}
			if(_identificationType!=null)
			{
				toReturn.Add(_identificationType);
			}
			if(_inspectionReport!=null)
			{
				toReturn.Add(_inspectionReport);
			}
			if(_guardian!=null)
			{
				toReturn.Add(_guardian);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			if(_transportCompany!=null)
			{
				toReturn.Add(_transportCompany);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="incidentNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIncidentNumber(System.String incidentNumber)
		{
			return FetchUsingUCIncidentNumber( incidentNumber, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="incidentNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIncidentNumber(System.String incidentNumber, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCIncidentNumber( incidentNumber, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="incidentNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIncidentNumber(System.String incidentNumber, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCIncidentNumber( incidentNumber, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="incidentNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIncidentNumber(System.String incidentNumber, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((FareEvasionIncidentDAO)CreateDAOInstance()).FetchFareEvasionIncidentUsingUCIncidentNumber(this, this.Transaction, incidentNumber, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionIncidentID)
		{
			return FetchUsingPK(fareEvasionIncidentID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionIncidentID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareEvasionIncidentID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionIncidentID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareEvasionIncidentID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareEvasionIncidentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareEvasionIncidentID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareEvasionIncidentID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareEvasionIncidentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BinaryDataEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BinaryDataEntity' which is related to this entity.</returns>
		public BinaryDataEntity GetSingleBinaryData()
		{
			return GetSingleBinaryData(false);
		}

		/// <summary> Retrieves the related entity of type 'BinaryDataEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BinaryDataEntity' which is related to this entity.</returns>
		public virtual BinaryDataEntity GetSingleBinaryData(bool forceFetch)
		{
			if( ( !_alreadyFetchedBinaryData || forceFetch || _alwaysFetchBinaryData) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BinaryDataEntityUsingBinaryDataID);
				BinaryDataEntity newEntity = new BinaryDataEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BinaryDataID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (BinaryDataEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_binaryDataReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BinaryData = newEntity;
				_alreadyFetchedBinaryData = fetchResult;
			}
			return _binaryData;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public DebtorEntity GetSingleDebtor()
		{
			return GetSingleDebtor(false);
		}

		/// <summary> Retrieves the related entity of type 'DebtorEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DebtorEntity' which is related to this entity.</returns>
		public virtual DebtorEntity GetSingleDebtor(bool forceFetch)
		{
			if( ( !_alreadyFetchedDebtor || forceFetch || _alwaysFetchDebtor) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DebtorEntityUsingDebtorID);
				DebtorEntity newEntity = new DebtorEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DebtorID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DebtorEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_debtorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Debtor = newEntity;
				_alreadyFetchedDebtor = fetchResult;
			}
			return _debtor;
		}


		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public FareEvasionCategoryEntity GetSingleFareEvasionCategory()
		{
			return GetSingleFareEvasionCategory(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionCategoryEntity' which is related to this entity.</returns>
		public virtual FareEvasionCategoryEntity GetSingleFareEvasionCategory(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionCategory || forceFetch || _alwaysFetchFareEvasionCategory) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
				FareEvasionCategoryEntity newEntity = new FareEvasionCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionCategoryID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionCategoryReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionCategory = newEntity;
				_alreadyFetchedFareEvasionCategory = fetchResult;
			}
			return _fareEvasionCategory;
		}


		/// <summary> Retrieves the related entity of type 'FareEvasionReasonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionReasonEntity' which is related to this entity.</returns>
		public FareEvasionReasonEntity GetSingleFareEvasionReason()
		{
			return GetSingleFareEvasionReason(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionReasonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionReasonEntity' which is related to this entity.</returns>
		public virtual FareEvasionReasonEntity GetSingleFareEvasionReason(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionReason || forceFetch || _alwaysFetchFareEvasionReason) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionReasonEntityUsingFareEvasionReasonID);
				FareEvasionReasonEntity newEntity = new FareEvasionReasonEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionReasonID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionReasonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionReasonReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionReason = newEntity;
				_alreadyFetchedFareEvasionReason = fetchResult;
			}
			return _fareEvasionReason;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'FareChangeCauseEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareChangeCauseEntity' which is related to this entity.</returns>
		public FareChangeCauseEntity GetSingleFareChangeCause()
		{
			return GetSingleFareChangeCause(false);
		}

		/// <summary> Retrieves the related entity of type 'FareChangeCauseEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareChangeCauseEntity' which is related to this entity.</returns>
		public virtual FareChangeCauseEntity GetSingleFareChangeCause(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareChangeCause || forceFetch || _alwaysFetchFareChangeCause) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareChangeCauseEntityUsingFareChangeCauseID);
				FareChangeCauseEntity newEntity = new FareChangeCauseEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareChangeCauseID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareChangeCauseEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareChangeCauseReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareChangeCause = newEntity;
				_alreadyFetchedFareChangeCause = fetchResult;
			}
			return _fareChangeCause;
		}


		/// <summary> Retrieves the related entity of type 'FareEvasionBehaviourEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareEvasionBehaviourEntity' which is related to this entity.</returns>
		public FareEvasionBehaviourEntity GetSingleFareEvasionBehaviour()
		{
			return GetSingleFareEvasionBehaviour(false);
		}

		/// <summary> Retrieves the related entity of type 'FareEvasionBehaviourEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareEvasionBehaviourEntity' which is related to this entity.</returns>
		public virtual FareEvasionBehaviourEntity GetSingleFareEvasionBehaviour(bool forceFetch)
		{
			if( ( !_alreadyFetchedFareEvasionBehaviour || forceFetch || _alwaysFetchFareEvasionBehaviour) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareEvasionBehaviourEntityUsingFareEvasionBehaviourID);
				FareEvasionBehaviourEntity newEntity = new FareEvasionBehaviourEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.FareEvasionBehaviourID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareEvasionBehaviourEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_fareEvasionBehaviourReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.FareEvasionBehaviour = newEntity;
				_alreadyFetchedFareEvasionBehaviour = fetchResult;
			}
			return _fareEvasionBehaviour;
		}


		/// <summary> Retrieves the related entity of type 'IdentificationTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'IdentificationTypeEntity' which is related to this entity.</returns>
		public IdentificationTypeEntity GetSingleIdentificationType()
		{
			return GetSingleIdentificationType(false);
		}

		/// <summary> Retrieves the related entity of type 'IdentificationTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'IdentificationTypeEntity' which is related to this entity.</returns>
		public virtual IdentificationTypeEntity GetSingleIdentificationType(bool forceFetch)
		{
			if( ( !_alreadyFetchedIdentificationType || forceFetch || _alwaysFetchIdentificationType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.IdentificationTypeEntityUsingIdentificationTypeID);
				IdentificationTypeEntity newEntity = new IdentificationTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IdentificationTypeID);
				}
				if(fetchResult)
				{
					newEntity = (IdentificationTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_identificationTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IdentificationType = newEntity;
				_alreadyFetchedIdentificationType = fetchResult;
			}
			return _identificationType;
		}


		/// <summary> Retrieves the related entity of type 'InspectionReportEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'InspectionReportEntity' which is related to this entity.</returns>
		public InspectionReportEntity GetSingleInspectionReport()
		{
			return GetSingleInspectionReport(false);
		}

		/// <summary> Retrieves the related entity of type 'InspectionReportEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'InspectionReportEntity' which is related to this entity.</returns>
		public virtual InspectionReportEntity GetSingleInspectionReport(bool forceFetch)
		{
			if( ( !_alreadyFetchedInspectionReport || forceFetch || _alwaysFetchInspectionReport) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.InspectionReportEntityUsingInspectionReportID);
				InspectionReportEntity newEntity = new InspectionReportEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.InspectionReportID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (InspectionReportEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_inspectionReportReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.InspectionReport = newEntity;
				_alreadyFetchedInspectionReport = fetchResult;
			}
			return _inspectionReport;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSingleGuardian()
		{
			return GetSingleGuardian(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSingleGuardian(bool forceFetch)
		{
			if( ( !_alreadyFetchedGuardian || forceFetch || _alwaysFetchGuardian) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingGuardianID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.GuardianID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_guardianReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Guardian = newEntity;
				_alreadyFetchedGuardian = fetchResult;
			}
			return _guardian;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID, this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary> Retrieves the related entity of type 'TransportCompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TransportCompanyEntity' which is related to this entity.</returns>
		public TransportCompanyEntity GetSingleTransportCompany()
		{
			return GetSingleTransportCompany(false);
		}

		/// <summary> Retrieves the related entity of type 'TransportCompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TransportCompanyEntity' which is related to this entity.</returns>
		public virtual TransportCompanyEntity GetSingleTransportCompany(bool forceFetch)
		{
			if( ( !_alreadyFetchedTransportCompany || forceFetch || _alwaysFetchTransportCompany) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TransportCompanyEntityUsingTransportCompanyID);
				TransportCompanyEntity newEntity = new TransportCompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TransportCompanyID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TransportCompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_transportCompanyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TransportCompany = newEntity;
				_alreadyFetchedTransportCompany = fetchResult;
			}
			return _transportCompany;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BinaryData", _binaryData);
			toReturn.Add("Client", _client);
			toReturn.Add("Debtor", _debtor);
			toReturn.Add("FareEvasionCategory", _fareEvasionCategory);
			toReturn.Add("FareEvasionReason", _fareEvasionReason);
			toReturn.Add("Contract", _contract);
			toReturn.Add("FareChangeCause", _fareChangeCause);
			toReturn.Add("FareEvasionBehaviour", _fareEvasionBehaviour);
			toReturn.Add("IdentificationType", _identificationType);
			toReturn.Add("InspectionReport", _inspectionReport);
			toReturn.Add("Guardian", _guardian);
			toReturn.Add("Person", _person);
			toReturn.Add("TransportCompany", _transportCompany);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="validator">The validator object for this FareEvasionIncidentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareEvasionIncidentID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareEvasionIncidentID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_binaryDataReturnsNewIfNotFound = false;
			_clientReturnsNewIfNotFound = false;
			_debtorReturnsNewIfNotFound = false;
			_fareEvasionCategoryReturnsNewIfNotFound = false;
			_fareEvasionReasonReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_fareChangeCauseReturnsNewIfNotFound = false;
			_fareEvasionBehaviourReturnsNewIfNotFound = false;
			_identificationTypeReturnsNewIfNotFound = false;
			_inspectionReportReturnsNewIfNotFound = false;
			_guardianReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			_transportCompanyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AppealDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BinaryDataID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletedDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DirectionNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Endstation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNote", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareChangeCauseID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionBehaviourID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionCategoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionIncidentID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareEvasionReasonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GuardianID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdentificationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IdentificationTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IncidentNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InsertionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InsertionUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionReportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionStopNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InspectionTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAddressPoliceChecked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsAppealed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBlackened", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsManuallyInserted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPayed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPenaltyDemanded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTicketWithdrawn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentDueDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PenaltyDemandDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrincipalClaim", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptPadNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecurringLinkID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StatusChangeDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransportCompanyID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _binaryData</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBinaryData(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _binaryData, new PropertyChangedEventHandler( OnBinaryDataPropertyChanged ), "BinaryData", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.BinaryDataEntityUsingBinaryDataIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.BinaryDataID } );		
			_binaryData = null;
		}
		
		/// <summary> setups the sync logic for member _binaryData</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBinaryData(IEntityCore relatedEntity)
		{
			if(_binaryData!=relatedEntity)
			{		
				DesetupSyncBinaryData(true, true);
				_binaryData = (BinaryDataEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _binaryData, new PropertyChangedEventHandler( OnBinaryDataPropertyChanged ), "BinaryData", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.BinaryDataEntityUsingBinaryDataIDStatic, true, ref _alreadyFetchedBinaryData, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBinaryDataPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _debtor</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDebtor(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.DebtorEntityUsingDebtorIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.DebtorID } );		
			_debtor = null;
		}
		
		/// <summary> setups the sync logic for member _debtor</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDebtor(IEntityCore relatedEntity)
		{
			if(_debtor!=relatedEntity)
			{		
				DesetupSyncDebtor(true, true);
				_debtor = (DebtorEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _debtor, new PropertyChangedEventHandler( OnDebtorPropertyChanged ), "Debtor", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.DebtorEntityUsingDebtorIDStatic, true, ref _alreadyFetchedDebtor, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDebtorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareEvasionCategory</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionCategory(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.FareEvasionCategoryID } );		
			_fareEvasionCategory = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionCategory</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionCategory(IEntityCore relatedEntity)
		{
			if(_fareEvasionCategory!=relatedEntity)
			{		
				DesetupSyncFareEvasionCategory(true, true);
				_fareEvasionCategory = (FareEvasionCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionCategory, new PropertyChangedEventHandler( OnFareEvasionCategoryPropertyChanged ), "FareEvasionCategory", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic, true, ref _alreadyFetchedFareEvasionCategory, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionCategoryPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareEvasionReason</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionReason(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionReason, new PropertyChangedEventHandler( OnFareEvasionReasonPropertyChanged ), "FareEvasionReason", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionReasonEntityUsingFareEvasionReasonIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.FareEvasionReasonID } );		
			_fareEvasionReason = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionReason</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionReason(IEntityCore relatedEntity)
		{
			if(_fareEvasionReason!=relatedEntity)
			{		
				DesetupSyncFareEvasionReason(true, true);
				_fareEvasionReason = (FareEvasionReasonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionReason, new PropertyChangedEventHandler( OnFareEvasionReasonPropertyChanged ), "FareEvasionReason", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionReasonEntityUsingFareEvasionReasonIDStatic, true, ref _alreadyFetchedFareEvasionReason, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionReasonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareChangeCause</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareChangeCause(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareChangeCause, new PropertyChangedEventHandler( OnFareChangeCausePropertyChanged ), "FareChangeCause", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareChangeCauseEntityUsingFareChangeCauseIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.FareChangeCauseID } );		
			_fareChangeCause = null;
		}
		
		/// <summary> setups the sync logic for member _fareChangeCause</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareChangeCause(IEntityCore relatedEntity)
		{
			if(_fareChangeCause!=relatedEntity)
			{		
				DesetupSyncFareChangeCause(true, true);
				_fareChangeCause = (FareChangeCauseEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareChangeCause, new PropertyChangedEventHandler( OnFareChangeCausePropertyChanged ), "FareChangeCause", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareChangeCauseEntityUsingFareChangeCauseIDStatic, true, ref _alreadyFetchedFareChangeCause, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareChangeCausePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fareEvasionBehaviour</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFareEvasionBehaviour(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _fareEvasionBehaviour, new PropertyChangedEventHandler( OnFareEvasionBehaviourPropertyChanged ), "FareEvasionBehaviour", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionBehaviourEntityUsingFareEvasionBehaviourIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.FareEvasionBehaviourID } );		
			_fareEvasionBehaviour = null;
		}
		
		/// <summary> setups the sync logic for member _fareEvasionBehaviour</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFareEvasionBehaviour(IEntityCore relatedEntity)
		{
			if(_fareEvasionBehaviour!=relatedEntity)
			{		
				DesetupSyncFareEvasionBehaviour(true, true);
				_fareEvasionBehaviour = (FareEvasionBehaviourEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _fareEvasionBehaviour, new PropertyChangedEventHandler( OnFareEvasionBehaviourPropertyChanged ), "FareEvasionBehaviour", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.FareEvasionBehaviourEntityUsingFareEvasionBehaviourIDStatic, true, ref _alreadyFetchedFareEvasionBehaviour, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFareEvasionBehaviourPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _identificationType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIdentificationType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _identificationType, new PropertyChangedEventHandler( OnIdentificationTypePropertyChanged ), "IdentificationType", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.IdentificationTypeEntityUsingIdentificationTypeIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.IdentificationTypeID } );		
			_identificationType = null;
		}
		
		/// <summary> setups the sync logic for member _identificationType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIdentificationType(IEntityCore relatedEntity)
		{
			if(_identificationType!=relatedEntity)
			{		
				DesetupSyncIdentificationType(true, true);
				_identificationType = (IdentificationTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _identificationType, new PropertyChangedEventHandler( OnIdentificationTypePropertyChanged ), "IdentificationType", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.IdentificationTypeEntityUsingIdentificationTypeIDStatic, true, ref _alreadyFetchedIdentificationType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIdentificationTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _inspectionReport</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncInspectionReport(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _inspectionReport, new PropertyChangedEventHandler( OnInspectionReportPropertyChanged ), "InspectionReport", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.InspectionReportEntityUsingInspectionReportIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.InspectionReportID } );		
			_inspectionReport = null;
		}
		
		/// <summary> setups the sync logic for member _inspectionReport</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncInspectionReport(IEntityCore relatedEntity)
		{
			if(_inspectionReport!=relatedEntity)
			{		
				DesetupSyncInspectionReport(true, true);
				_inspectionReport = (InspectionReportEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _inspectionReport, new PropertyChangedEventHandler( OnInspectionReportPropertyChanged ), "InspectionReport", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.InspectionReportEntityUsingInspectionReportIDStatic, true, ref _alreadyFetchedInspectionReport, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnInspectionReportPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _guardian</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncGuardian(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _guardian, new PropertyChangedEventHandler( OnGuardianPropertyChanged ), "Guardian", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.PersonEntityUsingGuardianIDStatic, true, signalRelatedEntity, "FareEvasionIncidentsAsGuardian", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.GuardianID } );		
			_guardian = null;
		}
		
		/// <summary> setups the sync logic for member _guardian</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncGuardian(IEntityCore relatedEntity)
		{
			if(_guardian!=relatedEntity)
			{		
				DesetupSyncGuardian(true, true);
				_guardian = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _guardian, new PropertyChangedEventHandler( OnGuardianPropertyChanged ), "Guardian", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.PersonEntityUsingGuardianIDStatic, true, ref _alreadyFetchedGuardian, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnGuardianPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "FareEvasionIncidentsAsPerson", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _transportCompany</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTransportCompany(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _transportCompany, new PropertyChangedEventHandler( OnTransportCompanyPropertyChanged ), "TransportCompany", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.TransportCompanyEntityUsingTransportCompanyIDStatic, true, signalRelatedEntity, "FareEvasionIncidents", resetFKFields, new int[] { (int)FareEvasionIncidentFieldIndex.TransportCompanyID } );		
			_transportCompany = null;
		}
		
		/// <summary> setups the sync logic for member _transportCompany</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTransportCompany(IEntityCore relatedEntity)
		{
			if(_transportCompany!=relatedEntity)
			{		
				DesetupSyncTransportCompany(true, true);
				_transportCompany = (TransportCompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _transportCompany, new PropertyChangedEventHandler( OnTransportCompanyPropertyChanged ), "TransportCompany", VarioSL.Entities.RelationClasses.StaticFareEvasionIncidentRelations.TransportCompanyEntityUsingTransportCompanyIDStatic, true, ref _alreadyFetchedTransportCompany, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTransportCompanyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareEvasionIncidentID">PK value for FareEvasionIncident which data should be fetched into this FareEvasionIncident object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareEvasionIncidentID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareEvasionIncidentFieldIndex.FareEvasionIncidentID].ForcedCurrentValueWrite(fareEvasionIncidentID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareEvasionIncidentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareEvasionIncidentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareEvasionIncidentRelations Relations
		{
			get	{ return new FareEvasionIncidentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BinaryData'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBinaryData
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BinaryDataCollection(), (IEntityRelation)GetRelationsForField("BinaryData")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.BinaryDataEntity, 0, null, null, null, "BinaryData", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Debtor'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDebtor
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DebtorCollection(), (IEntityRelation)GetRelationsForField("Debtor")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.DebtorEntity, 0, null, null, null, "Debtor", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionCategory
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection(), (IEntityRelation)GetRelationsForField("FareEvasionCategory")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, 0, null, null, null, "FareEvasionCategory", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionReason'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionReason
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection(), (IEntityRelation)GetRelationsForField("FareEvasionReason")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.FareEvasionReasonEntity, 0, null, null, null, "FareEvasionReason", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareChangeCause'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareChangeCause
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareChangeCauseCollection(), (IEntityRelation)GetRelationsForField("FareChangeCause")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.FareChangeCauseEntity, 0, null, null, null, "FareChangeCause", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionBehaviour'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionBehaviour
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionBehaviourCollection(), (IEntityRelation)GetRelationsForField("FareEvasionBehaviour")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.FareEvasionBehaviourEntity, 0, null, null, null, "FareEvasionBehaviour", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'IdentificationType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIdentificationType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.IdentificationTypeCollection(), (IEntityRelation)GetRelationsForField("IdentificationType")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.IdentificationTypeEntity, 0, null, null, null, "IdentificationType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'InspectionReport'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathInspectionReport
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.InspectionReportCollection(), (IEntityRelation)GetRelationsForField("InspectionReport")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.InspectionReportEntity, 0, null, null, null, "InspectionReport", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGuardian
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Guardian")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Guardian", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransportCompany'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransportCompany
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransportCompanyCollection(), (IEntityRelation)GetRelationsForField("TransportCompany")[0], (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity, (int)VarioSL.Entities.EntityType.TransportCompanyEntity, 0, null, null, null, "TransportCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AppealDate property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."APPEALDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime AppealDate
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.AppealDate, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.AppealDate, value, true); }
		}

		/// <summary> The BinaryDataID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."BINARYDATAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BinaryDataID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.BinaryDataID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.BinaryDataID, value, true); }
		}

		/// <summary> The ClientID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)FareEvasionIncidentFieldIndex.ClientID, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CompletedDateTime property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."COMPLETEDDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CompletedDateTime
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.CompletedDateTime, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.CompletedDateTime, value, true); }
		}

		/// <summary> The ContractID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.ContractID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.ContractID, value, true); }
		}

		/// <summary> The DebtorID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.DebtorID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The DirectionNumber property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."DIRECTIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DirectionNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.DirectionNumber, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.DirectionNumber, value, true); }
		}

		/// <summary> The Endstation property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ENDSTATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Endstation
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.Endstation, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.Endstation, value, true); }
		}

		/// <summary> The ExternalNote property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."EXTERNALNOTE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalNote
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.ExternalNote, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.ExternalNote, value, true); }
		}

		/// <summary> The FareChangeCauseID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."FARECHANGECAUSEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareChangeCauseID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.FareChangeCauseID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.FareChangeCauseID, value, true); }
		}

		/// <summary> The FareEvasionBehaviourID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."FAREEVASIONBEHAVIOURID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareEvasionBehaviourID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.FareEvasionBehaviourID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.FareEvasionBehaviourID, value, true); }
		}

		/// <summary> The FareEvasionCategoryID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."FAREEVASIONCATEGORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareEvasionCategoryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.FareEvasionCategoryID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.FareEvasionCategoryID, value, true); }
		}

		/// <summary> The FareEvasionIncidentID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."FAREEVASIONINCIDENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FareEvasionIncidentID
		{
			get { return (System.Int64)GetValue((int)FareEvasionIncidentFieldIndex.FareEvasionIncidentID, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.FareEvasionIncidentID, value, true); }
		}

		/// <summary> The FareEvasionReasonID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."FAREEVASIONREASONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> FareEvasionReasonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.FareEvasionReasonID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.FareEvasionReasonID, value, true); }
		}

		/// <summary> The GuardianID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."GUARDIANID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GuardianID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.GuardianID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.GuardianID, value, true); }
		}

		/// <summary> The IdentificationNumber property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."IDENTIFICATIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String IdentificationNumber
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.IdentificationNumber, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IdentificationNumber, value, true); }
		}

		/// <summary> The IdentificationTypeID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."IDENTIFICATIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 IdentificationTypeID
		{
			get { return (System.Int64)GetValue((int)FareEvasionIncidentFieldIndex.IdentificationTypeID, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IdentificationTypeID, value, true); }
		}

		/// <summary> The IncidentNumber property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INCIDENTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String IncidentNumber
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.IncidentNumber, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IncidentNumber, value, true); }
		}

		/// <summary> The InsertionTime property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INSERTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InsertionTime
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.InsertionTime, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.InsertionTime, value, true); }
		}

		/// <summary> The InsertionUser property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INSERTIONUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String InsertionUser
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.InsertionUser, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.InsertionUser, value, true); }
		}

		/// <summary> The InspectionReportID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INSPECTIONREPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InspectionReportID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.InspectionReportID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.InspectionReportID, value, true); }
		}

		/// <summary> The InspectionStopNumber property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INSPECTIONSTOPNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> InspectionStopNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.InspectionStopNumber, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.InspectionStopNumber, value, true); }
		}

		/// <summary> The InspectionTime property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."INSPECTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InspectionTime
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.InspectionTime, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.InspectionTime, value, true); }
		}

		/// <summary> The IsAddressPoliceChecked property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISADDRESSPOLICECHECKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAddressPoliceChecked
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsAddressPoliceChecked, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsAddressPoliceChecked, value, true); }
		}

		/// <summary> The IsAppealed property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISAPPEALED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsAppealed
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsAppealed, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsAppealed, value, true); }
		}

		/// <summary> The IsBlackened property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISBLACKENED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsBlackened
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsBlackened, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsBlackened, value, true); }
		}

		/// <summary> The IsManuallyInserted property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISMANUALLYINSERTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsManuallyInserted
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsManuallyInserted, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsManuallyInserted, value, true); }
		}

		/// <summary> The IsPayed property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISPAYED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPayed
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsPayed, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsPayed, value, true); }
		}

		/// <summary> The IsPenaltyDemanded property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISPENALTYDEMANDED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPenaltyDemanded
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsPenaltyDemanded, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsPenaltyDemanded, value, true); }
		}

		/// <summary> The IsTicketWithdrawn property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."ISTICKETWITHDRAWN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsTicketWithdrawn
		{
			get { return (System.Boolean)GetValue((int)FareEvasionIncidentFieldIndex.IsTicketWithdrawn, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.IsTicketWithdrawn, value, true); }
		}

		/// <summary> The LastModified property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.LastModified, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.LastUser, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LineName property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."LINENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LineName
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.LineName, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.LineName, value, true); }
		}

		/// <summary> The Notes property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."NOTES"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.Notes, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.Notes, value, true); }
		}

		/// <summary> The PaymentDueDate property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."PAYMENTDUEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PaymentDueDate
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.PaymentDueDate, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.PaymentDueDate, value, true); }
		}

		/// <summary> The PenaltyDemandDate property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."PENALTYDEMANDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime PenaltyDemandDate
		{
			get { return (System.DateTime)GetValue((int)FareEvasionIncidentFieldIndex.PenaltyDemandDate, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.PenaltyDemandDate, value, true); }
		}

		/// <summary> The PersonID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PersonID
		{
			get { return (System.Int64)GetValue((int)FareEvasionIncidentFieldIndex.PersonID, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.PersonID, value, true); }
		}

		/// <summary> The PrincipalClaim property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."PRINCIPALCLAIM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PrincipalClaim
		{
			get { return (Nullable<System.Int32>)GetValue((int)FareEvasionIncidentFieldIndex.PrincipalClaim, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.PrincipalClaim, value, true); }
		}

		/// <summary> The ReceiptPadNumber property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."RECEIPTPADNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptPadNumber
		{
			get { return (System.String)GetValue((int)FareEvasionIncidentFieldIndex.ReceiptPadNumber, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.ReceiptPadNumber, value, true); }
		}

		/// <summary> The RecurringLinkID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."RECURRINGLINKID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Guid> RecurringLinkID
		{
			get { return (Nullable<System.Guid>)GetValue((int)FareEvasionIncidentFieldIndex.RecurringLinkID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.RecurringLinkID, value, true); }
		}

		/// <summary> The Status property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.FareEvasionState Status
		{
			get { return (VarioSL.Entities.Enumerations.FareEvasionState)GetValue((int)FareEvasionIncidentFieldIndex.Status, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.Status, value, true); }
		}

		/// <summary> The StatusChangeDate property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."STATUSCHANGEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> StatusChangeDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)FareEvasionIncidentFieldIndex.StatusChangeDate, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.StatusChangeDate, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FareEvasionIncidentFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransportCompanyID property of the Entity FareEvasionIncident<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_FAREEVASIONINCIDENT"."TRANSPORTCOMPANYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransportCompanyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareEvasionIncidentFieldIndex.TransportCompanyID, false); }
			set	{ SetValue((int)FareEvasionIncidentFieldIndex.TransportCompanyID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BinaryDataEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBinaryData()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BinaryDataEntity BinaryData
		{
			get	{ return GetSingleBinaryData(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBinaryData(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "BinaryData", _binaryData, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BinaryData. When set to true, BinaryData is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BinaryData is accessed. You can always execute a forced fetch by calling GetSingleBinaryData(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBinaryData
		{
			get	{ return _alwaysFetchBinaryData; }
			set	{ _alwaysFetchBinaryData = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BinaryData already has been fetched. Setting this property to false when BinaryData has been fetched
		/// will set BinaryData to null as well. Setting this property to true while BinaryData hasn't been fetched disables lazy loading for BinaryData</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBinaryData
		{
			get { return _alreadyFetchedBinaryData;}
			set 
			{
				if(_alreadyFetchedBinaryData && !value)
				{
					this.BinaryData = null;
				}
				_alreadyFetchedBinaryData = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BinaryData is not found
		/// in the database. When set to true, BinaryData will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BinaryDataReturnsNewIfNotFound
		{
			get	{ return _binaryDataReturnsNewIfNotFound; }
			set { _binaryDataReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DebtorEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDebtor()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DebtorEntity Debtor
		{
			get	{ return GetSingleDebtor(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDebtor(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "Debtor", _debtor, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Debtor. When set to true, Debtor is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Debtor is accessed. You can always execute a forced fetch by calling GetSingleDebtor(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDebtor
		{
			get	{ return _alwaysFetchDebtor; }
			set	{ _alwaysFetchDebtor = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Debtor already has been fetched. Setting this property to false when Debtor has been fetched
		/// will set Debtor to null as well. Setting this property to true while Debtor hasn't been fetched disables lazy loading for Debtor</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDebtor
		{
			get { return _alreadyFetchedDebtor;}
			set 
			{
				if(_alreadyFetchedDebtor && !value)
				{
					this.Debtor = null;
				}
				_alreadyFetchedDebtor = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Debtor is not found
		/// in the database. When set to true, Debtor will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DebtorReturnsNewIfNotFound
		{
			get	{ return _debtorReturnsNewIfNotFound; }
			set { _debtorReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionCategory()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionCategoryEntity FareEvasionCategory
		{
			get	{ return GetSingleFareEvasionCategory(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionCategory(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "FareEvasionCategory", _fareEvasionCategory, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionCategory. When set to true, FareEvasionCategory is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionCategory is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionCategory(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionCategory
		{
			get	{ return _alwaysFetchFareEvasionCategory; }
			set	{ _alwaysFetchFareEvasionCategory = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionCategory already has been fetched. Setting this property to false when FareEvasionCategory has been fetched
		/// will set FareEvasionCategory to null as well. Setting this property to true while FareEvasionCategory hasn't been fetched disables lazy loading for FareEvasionCategory</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionCategory
		{
			get { return _alreadyFetchedFareEvasionCategory;}
			set 
			{
				if(_alreadyFetchedFareEvasionCategory && !value)
				{
					this.FareEvasionCategory = null;
				}
				_alreadyFetchedFareEvasionCategory = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionCategory is not found
		/// in the database. When set to true, FareEvasionCategory will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionCategoryReturnsNewIfNotFound
		{
			get	{ return _fareEvasionCategoryReturnsNewIfNotFound; }
			set { _fareEvasionCategoryReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionReasonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionReason()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionReasonEntity FareEvasionReason
		{
			get	{ return GetSingleFareEvasionReason(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionReason(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "FareEvasionReason", _fareEvasionReason, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionReason. When set to true, FareEvasionReason is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionReason is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionReason(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionReason
		{
			get	{ return _alwaysFetchFareEvasionReason; }
			set	{ _alwaysFetchFareEvasionReason = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionReason already has been fetched. Setting this property to false when FareEvasionReason has been fetched
		/// will set FareEvasionReason to null as well. Setting this property to true while FareEvasionReason hasn't been fetched disables lazy loading for FareEvasionReason</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionReason
		{
			get { return _alreadyFetchedFareEvasionReason;}
			set 
			{
				if(_alreadyFetchedFareEvasionReason && !value)
				{
					this.FareEvasionReason = null;
				}
				_alreadyFetchedFareEvasionReason = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionReason is not found
		/// in the database. When set to true, FareEvasionReason will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionReasonReturnsNewIfNotFound
		{
			get	{ return _fareEvasionReasonReturnsNewIfNotFound; }
			set { _fareEvasionReasonReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareChangeCauseEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareChangeCause()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareChangeCauseEntity FareChangeCause
		{
			get	{ return GetSingleFareChangeCause(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareChangeCause(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "FareChangeCause", _fareChangeCause, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareChangeCause. When set to true, FareChangeCause is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareChangeCause is accessed. You can always execute a forced fetch by calling GetSingleFareChangeCause(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareChangeCause
		{
			get	{ return _alwaysFetchFareChangeCause; }
			set	{ _alwaysFetchFareChangeCause = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareChangeCause already has been fetched. Setting this property to false when FareChangeCause has been fetched
		/// will set FareChangeCause to null as well. Setting this property to true while FareChangeCause hasn't been fetched disables lazy loading for FareChangeCause</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareChangeCause
		{
			get { return _alreadyFetchedFareChangeCause;}
			set 
			{
				if(_alreadyFetchedFareChangeCause && !value)
				{
					this.FareChangeCause = null;
				}
				_alreadyFetchedFareChangeCause = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareChangeCause is not found
		/// in the database. When set to true, FareChangeCause will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareChangeCauseReturnsNewIfNotFound
		{
			get	{ return _fareChangeCauseReturnsNewIfNotFound; }
			set { _fareChangeCauseReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareEvasionBehaviourEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFareEvasionBehaviour()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareEvasionBehaviourEntity FareEvasionBehaviour
		{
			get	{ return GetSingleFareEvasionBehaviour(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFareEvasionBehaviour(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "FareEvasionBehaviour", _fareEvasionBehaviour, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionBehaviour. When set to true, FareEvasionBehaviour is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionBehaviour is accessed. You can always execute a forced fetch by calling GetSingleFareEvasionBehaviour(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionBehaviour
		{
			get	{ return _alwaysFetchFareEvasionBehaviour; }
			set	{ _alwaysFetchFareEvasionBehaviour = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionBehaviour already has been fetched. Setting this property to false when FareEvasionBehaviour has been fetched
		/// will set FareEvasionBehaviour to null as well. Setting this property to true while FareEvasionBehaviour hasn't been fetched disables lazy loading for FareEvasionBehaviour</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionBehaviour
		{
			get { return _alreadyFetchedFareEvasionBehaviour;}
			set 
			{
				if(_alreadyFetchedFareEvasionBehaviour && !value)
				{
					this.FareEvasionBehaviour = null;
				}
				_alreadyFetchedFareEvasionBehaviour = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property FareEvasionBehaviour is not found
		/// in the database. When set to true, FareEvasionBehaviour will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FareEvasionBehaviourReturnsNewIfNotFound
		{
			get	{ return _fareEvasionBehaviourReturnsNewIfNotFound; }
			set { _fareEvasionBehaviourReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'IdentificationTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIdentificationType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual IdentificationTypeEntity IdentificationType
		{
			get	{ return GetSingleIdentificationType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIdentificationType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "IdentificationType", _identificationType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IdentificationType. When set to true, IdentificationType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IdentificationType is accessed. You can always execute a forced fetch by calling GetSingleIdentificationType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIdentificationType
		{
			get	{ return _alwaysFetchIdentificationType; }
			set	{ _alwaysFetchIdentificationType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IdentificationType already has been fetched. Setting this property to false when IdentificationType has been fetched
		/// will set IdentificationType to null as well. Setting this property to true while IdentificationType hasn't been fetched disables lazy loading for IdentificationType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIdentificationType
		{
			get { return _alreadyFetchedIdentificationType;}
			set 
			{
				if(_alreadyFetchedIdentificationType && !value)
				{
					this.IdentificationType = null;
				}
				_alreadyFetchedIdentificationType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IdentificationType is not found
		/// in the database. When set to true, IdentificationType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool IdentificationTypeReturnsNewIfNotFound
		{
			get	{ return _identificationTypeReturnsNewIfNotFound; }
			set { _identificationTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'InspectionReportEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleInspectionReport()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual InspectionReportEntity InspectionReport
		{
			get	{ return GetSingleInspectionReport(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncInspectionReport(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "InspectionReport", _inspectionReport, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for InspectionReport. When set to true, InspectionReport is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time InspectionReport is accessed. You can always execute a forced fetch by calling GetSingleInspectionReport(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchInspectionReport
		{
			get	{ return _alwaysFetchInspectionReport; }
			set	{ _alwaysFetchInspectionReport = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property InspectionReport already has been fetched. Setting this property to false when InspectionReport has been fetched
		/// will set InspectionReport to null as well. Setting this property to true while InspectionReport hasn't been fetched disables lazy loading for InspectionReport</summary>
		[Browsable(false)]
		public bool AlreadyFetchedInspectionReport
		{
			get { return _alreadyFetchedInspectionReport;}
			set 
			{
				if(_alreadyFetchedInspectionReport && !value)
				{
					this.InspectionReport = null;
				}
				_alreadyFetchedInspectionReport = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property InspectionReport is not found
		/// in the database. When set to true, InspectionReport will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool InspectionReportReturnsNewIfNotFound
		{
			get	{ return _inspectionReportReturnsNewIfNotFound; }
			set { _inspectionReportReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleGuardian()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Guardian
		{
			get	{ return GetSingleGuardian(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncGuardian(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidentsAsGuardian", "Guardian", _guardian, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Guardian. When set to true, Guardian is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Guardian is accessed. You can always execute a forced fetch by calling GetSingleGuardian(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGuardian
		{
			get	{ return _alwaysFetchGuardian; }
			set	{ _alwaysFetchGuardian = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Guardian already has been fetched. Setting this property to false when Guardian has been fetched
		/// will set Guardian to null as well. Setting this property to true while Guardian hasn't been fetched disables lazy loading for Guardian</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGuardian
		{
			get { return _alreadyFetchedGuardian;}
			set 
			{
				if(_alreadyFetchedGuardian && !value)
				{
					this.Guardian = null;
				}
				_alreadyFetchedGuardian = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Guardian is not found
		/// in the database. When set to true, Guardian will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool GuardianReturnsNewIfNotFound
		{
			get	{ return _guardianReturnsNewIfNotFound; }
			set { _guardianReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidentsAsPerson", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TransportCompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTransportCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TransportCompanyEntity TransportCompany
		{
			get	{ return GetSingleTransportCompany(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTransportCompany(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareEvasionIncidents", "TransportCompany", _transportCompany, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TransportCompany. When set to true, TransportCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransportCompany is accessed. You can always execute a forced fetch by calling GetSingleTransportCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransportCompany
		{
			get	{ return _alwaysFetchTransportCompany; }
			set	{ _alwaysFetchTransportCompany = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransportCompany already has been fetched. Setting this property to false when TransportCompany has been fetched
		/// will set TransportCompany to null as well. Setting this property to true while TransportCompany hasn't been fetched disables lazy loading for TransportCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransportCompany
		{
			get { return _alreadyFetchedTransportCompany;}
			set 
			{
				if(_alreadyFetchedTransportCompany && !value)
				{
					this.TransportCompany = null;
				}
				_alreadyFetchedTransportCompany = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TransportCompany is not found
		/// in the database. When set to true, TransportCompany will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TransportCompanyReturnsNewIfNotFound
		{
			get	{ return _transportCompanyReturnsNewIfNotFound; }
			set { _transportCompanyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareEvasionIncidentEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
