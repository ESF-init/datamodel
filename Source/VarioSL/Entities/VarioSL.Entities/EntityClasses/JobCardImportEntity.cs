﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'JobCardImport'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class JobCardImportEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CardPhysicalDetailEntity _cardPhysicalDetail;
		private bool	_alwaysFetchCardPhysicalDetail, _alreadyFetchedCardPhysicalDetail, _cardPhysicalDetailReturnsNewIfNotFound;
		private JobEntity _job;
		private bool	_alwaysFetchJob, _alreadyFetchedJob, _jobReturnsNewIfNotFound;
		private StorageLocationEntity _storageLocation;
		private bool	_alwaysFetchStorageLocation, _alreadyFetchedStorageLocation, _storageLocationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CardPhysicalDetail</summary>
			public static readonly string CardPhysicalDetail = "CardPhysicalDetail";
			/// <summary>Member name Job</summary>
			public static readonly string Job = "Job";
			/// <summary>Member name StorageLocation</summary>
			public static readonly string StorageLocation = "StorageLocation";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static JobCardImportEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public JobCardImportEntity() :base("JobCardImportEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		public JobCardImportEntity(System.Int64 jobCardImportID):base("JobCardImportEntity")
		{
			InitClassFetch(jobCardImportID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public JobCardImportEntity(System.Int64 jobCardImportID, IPrefetchPath prefetchPathToUse):base("JobCardImportEntity")
		{
			InitClassFetch(jobCardImportID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="validator">The custom validator object for this JobCardImportEntity</param>
		public JobCardImportEntity(System.Int64 jobCardImportID, IValidator validator):base("JobCardImportEntity")
		{
			InitClassFetch(jobCardImportID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected JobCardImportEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cardPhysicalDetail = (CardPhysicalDetailEntity)info.GetValue("_cardPhysicalDetail", typeof(CardPhysicalDetailEntity));
			if(_cardPhysicalDetail!=null)
			{
				_cardPhysicalDetail.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardPhysicalDetailReturnsNewIfNotFound = info.GetBoolean("_cardPhysicalDetailReturnsNewIfNotFound");
			_alwaysFetchCardPhysicalDetail = info.GetBoolean("_alwaysFetchCardPhysicalDetail");
			_alreadyFetchedCardPhysicalDetail = info.GetBoolean("_alreadyFetchedCardPhysicalDetail");

			_job = (JobEntity)info.GetValue("_job", typeof(JobEntity));
			if(_job!=null)
			{
				_job.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_jobReturnsNewIfNotFound = info.GetBoolean("_jobReturnsNewIfNotFound");
			_alwaysFetchJob = info.GetBoolean("_alwaysFetchJob");
			_alreadyFetchedJob = info.GetBoolean("_alreadyFetchedJob");

			_storageLocation = (StorageLocationEntity)info.GetValue("_storageLocation", typeof(StorageLocationEntity));
			if(_storageLocation!=null)
			{
				_storageLocation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_storageLocationReturnsNewIfNotFound = info.GetBoolean("_storageLocationReturnsNewIfNotFound");
			_alwaysFetchStorageLocation = info.GetBoolean("_alwaysFetchStorageLocation");
			_alreadyFetchedStorageLocation = info.GetBoolean("_alreadyFetchedStorageLocation");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((JobCardImportFieldIndex)fieldIndex)
			{
				case JobCardImportFieldIndex.CardPhysicalDetailID:
					DesetupSyncCardPhysicalDetail(true, false);
					_alreadyFetchedCardPhysicalDetail = false;
					break;
				case JobCardImportFieldIndex.JobID:
					DesetupSyncJob(true, false);
					_alreadyFetchedJob = false;
					break;
				case JobCardImportFieldIndex.StorageLocationID:
					DesetupSyncStorageLocation(true, false);
					_alreadyFetchedStorageLocation = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCardPhysicalDetail = (_cardPhysicalDetail != null);
			_alreadyFetchedJob = (_job != null);
			_alreadyFetchedStorageLocation = (_storageLocation != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CardPhysicalDetail":
					toReturn.Add(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
					break;
				case "Job":
					toReturn.Add(Relations.JobEntityUsingJobID);
					break;
				case "StorageLocation":
					toReturn.Add(Relations.StorageLocationEntityUsingStorageLocationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cardPhysicalDetail", (!this.MarkedForDeletion?_cardPhysicalDetail:null));
			info.AddValue("_cardPhysicalDetailReturnsNewIfNotFound", _cardPhysicalDetailReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCardPhysicalDetail", _alwaysFetchCardPhysicalDetail);
			info.AddValue("_alreadyFetchedCardPhysicalDetail", _alreadyFetchedCardPhysicalDetail);
			info.AddValue("_job", (!this.MarkedForDeletion?_job:null));
			info.AddValue("_jobReturnsNewIfNotFound", _jobReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchJob", _alwaysFetchJob);
			info.AddValue("_alreadyFetchedJob", _alreadyFetchedJob);
			info.AddValue("_storageLocation", (!this.MarkedForDeletion?_storageLocation:null));
			info.AddValue("_storageLocationReturnsNewIfNotFound", _storageLocationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchStorageLocation", _alwaysFetchStorageLocation);
			info.AddValue("_alreadyFetchedStorageLocation", _alreadyFetchedStorageLocation);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CardPhysicalDetail":
					_alreadyFetchedCardPhysicalDetail = true;
					this.CardPhysicalDetail = (CardPhysicalDetailEntity)entity;
					break;
				case "Job":
					_alreadyFetchedJob = true;
					this.Job = (JobEntity)entity;
					break;
				case "StorageLocation":
					_alreadyFetchedStorageLocation = true;
					this.StorageLocation = (StorageLocationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CardPhysicalDetail":
					SetupSyncCardPhysicalDetail(relatedEntity);
					break;
				case "Job":
					SetupSyncJob(relatedEntity);
					break;
				case "StorageLocation":
					SetupSyncStorageLocation(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CardPhysicalDetail":
					DesetupSyncCardPhysicalDetail(false, true);
					break;
				case "Job":
					DesetupSyncJob(false, true);
					break;
				case "StorageLocation":
					DesetupSyncStorageLocation(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_cardPhysicalDetail!=null)
			{
				toReturn.Add(_cardPhysicalDetail);
			}
			if(_job!=null)
			{
				toReturn.Add(_job);
			}
			if(_storageLocation!=null)
			{
				toReturn.Add(_storageLocation);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobCardImportID)
		{
			return FetchUsingPK(jobCardImportID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobCardImportID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(jobCardImportID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobCardImportID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(jobCardImportID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 jobCardImportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(jobCardImportID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.JobCardImportID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new JobCardImportRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public CardPhysicalDetailEntity GetSingleCardPhysicalDetail()
		{
			return GetSingleCardPhysicalDetail(false);
		}

		/// <summary> Retrieves the related entity of type 'CardPhysicalDetailEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardPhysicalDetailEntity' which is related to this entity.</returns>
		public virtual CardPhysicalDetailEntity GetSingleCardPhysicalDetail(bool forceFetch)
		{
			if( ( !_alreadyFetchedCardPhysicalDetail || forceFetch || _alwaysFetchCardPhysicalDetail) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
				CardPhysicalDetailEntity newEntity = new CardPhysicalDetailEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardPhysicalDetailID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardPhysicalDetailEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardPhysicalDetailReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CardPhysicalDetail = newEntity;
				_alreadyFetchedCardPhysicalDetail = fetchResult;
			}
			return _cardPhysicalDetail;
		}


		/// <summary> Retrieves the related entity of type 'JobEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'JobEntity' which is related to this entity.</returns>
		public JobEntity GetSingleJob()
		{
			return GetSingleJob(false);
		}

		/// <summary> Retrieves the related entity of type 'JobEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'JobEntity' which is related to this entity.</returns>
		public virtual JobEntity GetSingleJob(bool forceFetch)
		{
			if( ( !_alreadyFetchedJob || forceFetch || _alwaysFetchJob) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.JobEntityUsingJobID);
				JobEntity newEntity = new JobEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.JobID);
				}
				if(fetchResult)
				{
					newEntity = (JobEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_jobReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Job = newEntity;
				_alreadyFetchedJob = fetchResult;
			}
			return _job;
		}


		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public StorageLocationEntity GetSingleStorageLocation()
		{
			return GetSingleStorageLocation(false);
		}

		/// <summary> Retrieves the related entity of type 'StorageLocationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'StorageLocationEntity' which is related to this entity.</returns>
		public virtual StorageLocationEntity GetSingleStorageLocation(bool forceFetch)
		{
			if( ( !_alreadyFetchedStorageLocation || forceFetch || _alwaysFetchStorageLocation) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.StorageLocationEntityUsingStorageLocationID);
				StorageLocationEntity newEntity = new StorageLocationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.StorageLocationID);
				}
				if(fetchResult)
				{
					newEntity = (StorageLocationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_storageLocationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.StorageLocation = newEntity;
				_alreadyFetchedStorageLocation = fetchResult;
			}
			return _storageLocation;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CardPhysicalDetail", _cardPhysicalDetail);
			toReturn.Add("Job", _job);
			toReturn.Add("StorageLocation", _storageLocation);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="validator">The validator object for this JobCardImportEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 jobCardImportID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(jobCardImportID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_cardPhysicalDetailReturnsNewIfNotFound = false;
			_jobReturnsNewIfNotFound = false;
			_storageLocationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BatchNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardCount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardPhysicalDetailID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilePath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobCardImportID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("JobID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StorageLocationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCardPhysicalDetail(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, signalRelatedEntity, "JobCardImports", resetFKFields, new int[] { (int)JobCardImportFieldIndex.CardPhysicalDetailID } );		
			_cardPhysicalDetail = null;
		}
		
		/// <summary> setups the sync logic for member _cardPhysicalDetail</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCardPhysicalDetail(IEntityCore relatedEntity)
		{
			if(_cardPhysicalDetail!=relatedEntity)
			{		
				DesetupSyncCardPhysicalDetail(true, true);
				_cardPhysicalDetail = (CardPhysicalDetailEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _cardPhysicalDetail, new PropertyChangedEventHandler( OnCardPhysicalDetailPropertyChanged ), "CardPhysicalDetail", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic, true, ref _alreadyFetchedCardPhysicalDetail, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPhysicalDetailPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _job</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncJob(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _job, new PropertyChangedEventHandler( OnJobPropertyChanged ), "Job", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.JobEntityUsingJobIDStatic, true, signalRelatedEntity, "JobCardImports", resetFKFields, new int[] { (int)JobCardImportFieldIndex.JobID } );		
			_job = null;
		}
		
		/// <summary> setups the sync logic for member _job</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncJob(IEntityCore relatedEntity)
		{
			if(_job!=relatedEntity)
			{		
				DesetupSyncJob(true, true);
				_job = (JobEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _job, new PropertyChangedEventHandler( OnJobPropertyChanged ), "Job", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.JobEntityUsingJobIDStatic, true, ref _alreadyFetchedJob, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnJobPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _storageLocation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncStorageLocation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _storageLocation, new PropertyChangedEventHandler( OnStorageLocationPropertyChanged ), "StorageLocation", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.StorageLocationEntityUsingStorageLocationIDStatic, true, signalRelatedEntity, "JobCardImports", resetFKFields, new int[] { (int)JobCardImportFieldIndex.StorageLocationID } );		
			_storageLocation = null;
		}
		
		/// <summary> setups the sync logic for member _storageLocation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncStorageLocation(IEntityCore relatedEntity)
		{
			if(_storageLocation!=relatedEntity)
			{		
				DesetupSyncStorageLocation(true, true);
				_storageLocation = (StorageLocationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _storageLocation, new PropertyChangedEventHandler( OnStorageLocationPropertyChanged ), "StorageLocation", VarioSL.Entities.RelationClasses.StaticJobCardImportRelations.StorageLocationEntityUsingStorageLocationIDStatic, true, ref _alreadyFetchedStorageLocation, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnStorageLocationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="jobCardImportID">PK value for JobCardImport which data should be fetched into this JobCardImport object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 jobCardImportID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)JobCardImportFieldIndex.JobCardImportID].ForcedCurrentValueWrite(jobCardImportID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateJobCardImportDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new JobCardImportEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static JobCardImportRelations Relations
		{
			get	{ return new JobCardImportRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardPhysicalDetail'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardPhysicalDetail
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardPhysicalDetailCollection(), (IEntityRelation)GetRelationsForField("CardPhysicalDetail")[0], (int)VarioSL.Entities.EntityType.JobCardImportEntity, (int)VarioSL.Entities.EntityType.CardPhysicalDetailEntity, 0, null, null, null, "CardPhysicalDetail", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Job'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathJob
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.JobCollection(), (IEntityRelation)GetRelationsForField("Job")[0], (int)VarioSL.Entities.EntityType.JobCardImportEntity, (int)VarioSL.Entities.EntityType.JobEntity, 0, null, null, null, "Job", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'StorageLocation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStorageLocation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StorageLocationCollection(), (IEntityRelation)GetRelationsForField("StorageLocation")[0], (int)VarioSL.Entities.EntityType.JobCardImportEntity, (int)VarioSL.Entities.EntityType.StorageLocationEntity, 0, null, null, null, "StorageLocation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BatchNumber property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."BATCHNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BatchNumber
		{
			get { return (System.String)GetValue((int)JobCardImportFieldIndex.BatchNumber, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.BatchNumber, value, true); }
		}

		/// <summary> The CardCount property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."CARDCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardCount
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobCardImportFieldIndex.CardCount, false); }
			set	{ SetValue((int)JobCardImportFieldIndex.CardCount, value, true); }
		}

		/// <summary> The CardPhysicalDetailID property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."CARDPHYSICALDETAILID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardPhysicalDetailID
		{
			get { return (Nullable<System.Int64>)GetValue((int)JobCardImportFieldIndex.CardPhysicalDetailID, false); }
			set	{ SetValue((int)JobCardImportFieldIndex.CardPhysicalDetailID, value, true); }
		}

		/// <summary> The FilePath property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."FILEPATH"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FilePath
		{
			get { return (System.String)GetValue((int)JobCardImportFieldIndex.FilePath, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.FilePath, value, true); }
		}

		/// <summary> The JobCardImportID property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."JOBCARDIMPORTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 JobCardImportID
		{
			get { return (System.Int64)GetValue((int)JobCardImportFieldIndex.JobCardImportID, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.JobCardImportID, value, true); }
		}

		/// <summary> The JobID property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."JOBID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 JobID
		{
			get { return (System.Int64)GetValue((int)JobCardImportFieldIndex.JobID, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.JobID, value, true); }
		}

		/// <summary> The LastModified property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)JobCardImportFieldIndex.LastModified, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)JobCardImportFieldIndex.LastUser, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.LastUser, value, true); }
		}

		/// <summary> The StorageLocationID property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."STORAGELOCATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 StorageLocationID
		{
			get { return (System.Int64)GetValue((int)JobCardImportFieldIndex.StorageLocationID, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.StorageLocationID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity JobCardImport<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_JOBCARDIMPORT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)JobCardImportFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)JobCardImportFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CardPhysicalDetailEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCardPhysicalDetail()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardPhysicalDetailEntity CardPhysicalDetail
		{
			get	{ return GetSingleCardPhysicalDetail(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCardPhysicalDetail(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "JobCardImports", "CardPhysicalDetail", _cardPhysicalDetail, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CardPhysicalDetail. When set to true, CardPhysicalDetail is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardPhysicalDetail is accessed. You can always execute a forced fetch by calling GetSingleCardPhysicalDetail(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardPhysicalDetail
		{
			get	{ return _alwaysFetchCardPhysicalDetail; }
			set	{ _alwaysFetchCardPhysicalDetail = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardPhysicalDetail already has been fetched. Setting this property to false when CardPhysicalDetail has been fetched
		/// will set CardPhysicalDetail to null as well. Setting this property to true while CardPhysicalDetail hasn't been fetched disables lazy loading for CardPhysicalDetail</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardPhysicalDetail
		{
			get { return _alreadyFetchedCardPhysicalDetail;}
			set 
			{
				if(_alreadyFetchedCardPhysicalDetail && !value)
				{
					this.CardPhysicalDetail = null;
				}
				_alreadyFetchedCardPhysicalDetail = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CardPhysicalDetail is not found
		/// in the database. When set to true, CardPhysicalDetail will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardPhysicalDetailReturnsNewIfNotFound
		{
			get	{ return _cardPhysicalDetailReturnsNewIfNotFound; }
			set { _cardPhysicalDetailReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'JobEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleJob()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual JobEntity Job
		{
			get	{ return GetSingleJob(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncJob(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "JobCardImports", "Job", _job, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Job. When set to true, Job is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Job is accessed. You can always execute a forced fetch by calling GetSingleJob(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchJob
		{
			get	{ return _alwaysFetchJob; }
			set	{ _alwaysFetchJob = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Job already has been fetched. Setting this property to false when Job has been fetched
		/// will set Job to null as well. Setting this property to true while Job hasn't been fetched disables lazy loading for Job</summary>
		[Browsable(false)]
		public bool AlreadyFetchedJob
		{
			get { return _alreadyFetchedJob;}
			set 
			{
				if(_alreadyFetchedJob && !value)
				{
					this.Job = null;
				}
				_alreadyFetchedJob = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Job is not found
		/// in the database. When set to true, Job will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool JobReturnsNewIfNotFound
		{
			get	{ return _jobReturnsNewIfNotFound; }
			set { _jobReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'StorageLocationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleStorageLocation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual StorageLocationEntity StorageLocation
		{
			get	{ return GetSingleStorageLocation(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncStorageLocation(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "JobCardImports", "StorageLocation", _storageLocation, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for StorageLocation. When set to true, StorageLocation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time StorageLocation is accessed. You can always execute a forced fetch by calling GetSingleStorageLocation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStorageLocation
		{
			get	{ return _alwaysFetchStorageLocation; }
			set	{ _alwaysFetchStorageLocation = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property StorageLocation already has been fetched. Setting this property to false when StorageLocation has been fetched
		/// will set StorageLocation to null as well. Setting this property to true while StorageLocation hasn't been fetched disables lazy loading for StorageLocation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStorageLocation
		{
			get { return _alreadyFetchedStorageLocation;}
			set 
			{
				if(_alreadyFetchedStorageLocation && !value)
				{
					this.StorageLocation = null;
				}
				_alreadyFetchedStorageLocation = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property StorageLocation is not found
		/// in the database. When set to true, StorageLocation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool StorageLocationReturnsNewIfNotFound
		{
			get	{ return _storageLocationReturnsNewIfNotFound; }
			set { _storageLocationReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.JobCardImportEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
