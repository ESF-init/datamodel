﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BankStatement'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BankStatementEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private PaymentJournalEntity _paymentJournal;
		private bool	_alwaysFetchPaymentJournal, _alreadyFetchedPaymentJournal, _paymentJournalReturnsNewIfNotFound;
		private BankStatementVerificationEntity _bankStatementVerification;
		private bool	_alwaysFetchBankStatementVerification, _alreadyFetchedBankStatementVerification, _bankStatementVerificationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name PaymentJournal</summary>
			public static readonly string PaymentJournal = "PaymentJournal";
			/// <summary>Member name BankStatementVerification</summary>
			public static readonly string BankStatementVerification = "BankStatementVerification";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BankStatementEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BankStatementEntity() :base("BankStatementEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		public BankStatementEntity(System.Int64 bankStatementID):base("BankStatementEntity")
		{
			InitClassFetch(bankStatementID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BankStatementEntity(System.Int64 bankStatementID, IPrefetchPath prefetchPathToUse):base("BankStatementEntity")
		{
			InitClassFetch(bankStatementID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="validator">The custom validator object for this BankStatementEntity</param>
		public BankStatementEntity(System.Int64 bankStatementID, IValidator validator):base("BankStatementEntity")
		{
			InitClassFetch(bankStatementID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BankStatementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentJournal = (PaymentJournalEntity)info.GetValue("_paymentJournal", typeof(PaymentJournalEntity));
			if(_paymentJournal!=null)
			{
				_paymentJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentJournalReturnsNewIfNotFound = info.GetBoolean("_paymentJournalReturnsNewIfNotFound");
			_alwaysFetchPaymentJournal = info.GetBoolean("_alwaysFetchPaymentJournal");
			_alreadyFetchedPaymentJournal = info.GetBoolean("_alreadyFetchedPaymentJournal");
			_bankStatementVerification = (BankStatementVerificationEntity)info.GetValue("_bankStatementVerification", typeof(BankStatementVerificationEntity));
			if(_bankStatementVerification!=null)
			{
				_bankStatementVerification.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bankStatementVerificationReturnsNewIfNotFound = info.GetBoolean("_bankStatementVerificationReturnsNewIfNotFound");
			_alwaysFetchBankStatementVerification = info.GetBoolean("_alwaysFetchBankStatementVerification");
			_alreadyFetchedBankStatementVerification = info.GetBoolean("_alreadyFetchedBankStatementVerification");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BankStatementFieldIndex)fieldIndex)
			{
				case BankStatementFieldIndex.PaymentJournalID:
					DesetupSyncPaymentJournal(true, false);
					_alreadyFetchedPaymentJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentJournal = (_paymentJournal != null);
			_alreadyFetchedBankStatementVerification = (_bankStatementVerification != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "PaymentJournal":
					toReturn.Add(Relations.PaymentJournalEntityUsingPaymentJournalID);
					break;
				case "BankStatementVerification":
					toReturn.Add(Relations.BankStatementVerificationEntityUsingBankStatementID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentJournal", (!this.MarkedForDeletion?_paymentJournal:null));
			info.AddValue("_paymentJournalReturnsNewIfNotFound", _paymentJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentJournal", _alwaysFetchPaymentJournal);
			info.AddValue("_alreadyFetchedPaymentJournal", _alreadyFetchedPaymentJournal);

			info.AddValue("_bankStatementVerification", (!this.MarkedForDeletion?_bankStatementVerification:null));
			info.AddValue("_bankStatementVerificationReturnsNewIfNotFound", _bankStatementVerificationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBankStatementVerification", _alwaysFetchBankStatementVerification);
			info.AddValue("_alreadyFetchedBankStatementVerification", _alreadyFetchedBankStatementVerification);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "PaymentJournal":
					_alreadyFetchedPaymentJournal = true;
					this.PaymentJournal = (PaymentJournalEntity)entity;
					break;
				case "BankStatementVerification":
					_alreadyFetchedBankStatementVerification = true;
					this.BankStatementVerification = (BankStatementVerificationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "PaymentJournal":
					SetupSyncPaymentJournal(relatedEntity);
					break;
				case "BankStatementVerification":
					SetupSyncBankStatementVerification(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "PaymentJournal":
					DesetupSyncPaymentJournal(false, true);
					break;
				case "BankStatementVerification":
					DesetupSyncBankStatementVerification(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_bankStatementVerification!=null)
			{
				toReturn.Add(_bankStatementVerification);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_paymentJournal!=null)
			{
				toReturn.Add(_paymentJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankStatementID)
		{
			return FetchUsingPK(bankStatementID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankStatementID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(bankStatementID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankStatementID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(bankStatementID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 bankStatementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(bankStatementID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BankStatementID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BankStatementRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public PaymentJournalEntity GetSinglePaymentJournal()
		{
			return GetSinglePaymentJournal(false);
		}

		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public virtual PaymentJournalEntity GetSinglePaymentJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentJournal || forceFetch || _alwaysFetchPaymentJournal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentJournalEntityUsingPaymentJournalID);
				PaymentJournalEntity newEntity = new PaymentJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentJournalID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PaymentJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentJournal = newEntity;
				_alreadyFetchedPaymentJournal = fetchResult;
			}
			return _paymentJournal;
		}

		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public BankStatementVerificationEntity GetSingleBankStatementVerification()
		{
			return GetSingleBankStatementVerification(false);
		}
		
		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public virtual BankStatementVerificationEntity GetSingleBankStatementVerification(bool forceFetch)
		{
			if( ( !_alreadyFetchedBankStatementVerification || forceFetch || _alwaysFetchBankStatementVerification) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BankStatementVerificationEntityUsingBankStatementID);
				BankStatementVerificationEntity newEntity = new BankStatementVerificationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCBankStatementID(this.BankStatementID);
				}
				if(fetchResult)
				{
					newEntity = (BankStatementVerificationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bankStatementVerificationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BankStatementVerification = newEntity;
				_alreadyFetchedBankStatementVerification = fetchResult;
			}
			return _bankStatementVerification;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("PaymentJournal", _paymentJournal);
			toReturn.Add("BankStatementVerification", _bankStatementVerification);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="validator">The validator object for this BankStatementEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 bankStatementID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(bankStatementID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_paymentJournalReturnsNewIfNotFound = false;
			_bankStatementVerificationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuthorizationCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankStatementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardBrandType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargebackCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ChargebackDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisputeDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FileReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaskedPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RecordType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortPan", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Transactiontime", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _paymentJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticBankStatementRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, signalRelatedEntity, "BankStatements", resetFKFields, new int[] { (int)BankStatementFieldIndex.PaymentJournalID } );		
			_paymentJournal = null;
		}
		
		/// <summary> setups the sync logic for member _paymentJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentJournal(IEntityCore relatedEntity)
		{
			if(_paymentJournal!=relatedEntity)
			{		
				DesetupSyncPaymentJournal(true, true);
				_paymentJournal = (PaymentJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticBankStatementRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, ref _alreadyFetchedPaymentJournal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bankStatementVerification</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBankStatementVerification(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticBankStatementRelations.BankStatementVerificationEntityUsingBankStatementIDStatic, false, signalRelatedEntity, "BankStatement", false, new int[] { (int)BankStatementFieldIndex.BankStatementID } );
			_bankStatementVerification = null;
		}
	
		/// <summary> setups the sync logic for member _bankStatementVerification</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBankStatementVerification(IEntityCore relatedEntity)
		{
			if(_bankStatementVerification!=relatedEntity)
			{
				DesetupSyncBankStatementVerification(true, true);
				_bankStatementVerification = (BankStatementVerificationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticBankStatementRelations.BankStatementVerificationEntityUsingBankStatementIDStatic, false, ref _alreadyFetchedBankStatementVerification, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBankStatementVerificationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="bankStatementID">PK value for BankStatement which data should be fetched into this BankStatement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 bankStatementID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BankStatementFieldIndex.BankStatementID].ForcedCurrentValueWrite(bankStatementID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBankStatementDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BankStatementEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BankStatementRelations Relations
		{
			get	{ return new BankStatementRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournal")[0], (int)VarioSL.Entities.EntityType.BankStatementEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankStatementVerification'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankStatementVerification
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankStatementVerificationCollection(), (IEntityRelation)GetRelationsForField("BankStatementVerification")[0], (int)VarioSL.Entities.EntityType.BankStatementEntity, (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, 0, null, null, null, "BankStatementVerification", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)BankStatementFieldIndex.Amount, true); }
			set	{ SetValue((int)BankStatementFieldIndex.Amount, value, true); }
		}

		/// <summary> The AuthorizationCode property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."AUTHORIZATIONCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AuthorizationCode
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.AuthorizationCode, true); }
			set	{ SetValue((int)BankStatementFieldIndex.AuthorizationCode, value, true); }
		}

		/// <summary> The BankStatementID property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."BANKSTATEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 BankStatementID
		{
			get { return (System.Int64)GetValue((int)BankStatementFieldIndex.BankStatementID, true); }
			set	{ SetValue((int)BankStatementFieldIndex.BankStatementID, value, true); }
		}

		/// <summary> The CardBrandType property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."CARDBRANDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CardBrandType
		{
			get { return (Nullable<System.Int32>)GetValue((int)BankStatementFieldIndex.CardBrandType, false); }
			set	{ SetValue((int)BankStatementFieldIndex.CardBrandType, value, true); }
		}

		/// <summary> The ChargebackCode property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."CHARGEBACKCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ChargebackCode
		{
			get { return (Nullable<System.Int32>)GetValue((int)BankStatementFieldIndex.ChargebackCode, false); }
			set	{ SetValue((int)BankStatementFieldIndex.ChargebackCode, value, true); }
		}

		/// <summary> The ChargebackDescription property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."CHARGEBACKDESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ChargebackDescription
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.ChargebackDescription, true); }
			set	{ SetValue((int)BankStatementFieldIndex.ChargebackDescription, value, true); }
		}

		/// <summary> The DisputeDate property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."DISPUTEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DisputeDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)BankStatementFieldIndex.DisputeDate, false); }
			set	{ SetValue((int)BankStatementFieldIndex.DisputeDate, value, true); }
		}

		/// <summary> The FileDate property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."FILEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime FileDate
		{
			get { return (System.DateTime)GetValue((int)BankStatementFieldIndex.FileDate, true); }
			set	{ SetValue((int)BankStatementFieldIndex.FileDate, value, true); }
		}

		/// <summary> The FileReference property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."FILEREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String FileReference
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.FileReference, true); }
			set	{ SetValue((int)BankStatementFieldIndex.FileReference, value, true); }
		}

		/// <summary> The LastModified property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)BankStatementFieldIndex.LastModified, true); }
			set	{ SetValue((int)BankStatementFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.LastUser, true); }
			set	{ SetValue((int)BankStatementFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MaskedPan property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."MASKEDPAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MaskedPan
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.MaskedPan, true); }
			set	{ SetValue((int)BankStatementFieldIndex.MaskedPan, value, true); }
		}

		/// <summary> The PaymentJournalID property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."PAYMENTJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PaymentJournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)BankStatementFieldIndex.PaymentJournalID, false); }
			set	{ SetValue((int)BankStatementFieldIndex.PaymentJournalID, value, true); }
		}

		/// <summary> The PaymentReference property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."PAYMENTREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PaymentReference
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.PaymentReference, true); }
			set	{ SetValue((int)BankStatementFieldIndex.PaymentReference, value, true); }
		}

		/// <summary> The RecordType property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."RECORDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.BankStatementRecordType RecordType
		{
			get { return (VarioSL.Entities.Enumerations.BankStatementRecordType)GetValue((int)BankStatementFieldIndex.RecordType, true); }
			set	{ SetValue((int)BankStatementFieldIndex.RecordType, value, true); }
		}

		/// <summary> The ShortPan property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."SHORTPAN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortPan
		{
			get { return (System.String)GetValue((int)BankStatementFieldIndex.ShortPan, true); }
			set	{ SetValue((int)BankStatementFieldIndex.ShortPan, value, true); }
		}

		/// <summary> The State property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.BankStatementState State
		{
			get { return (VarioSL.Entities.Enumerations.BankStatementState)GetValue((int)BankStatementFieldIndex.State, true); }
			set	{ SetValue((int)BankStatementFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)BankStatementFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)BankStatementFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionDate property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."TRANSACTIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TransactionDate
		{
			get { return (System.DateTime)GetValue((int)BankStatementFieldIndex.TransactionDate, true); }
			set	{ SetValue((int)BankStatementFieldIndex.TransactionDate, value, true); }
		}

		/// <summary> The Transactiontime property of the Entity BankStatement<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_BANKSTATEMENT"."TRANSACTIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Transactiontime
		{
			get { return (Nullable<System.Int32>)GetValue((int)BankStatementFieldIndex.Transactiontime, false); }
			set	{ SetValue((int)BankStatementFieldIndex.Transactiontime, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'PaymentJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentJournalEntity PaymentJournal
		{
			get	{ return GetSinglePaymentJournal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPaymentJournal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BankStatements", "PaymentJournal", _paymentJournal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournal. When set to true, PaymentJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournal is accessed. You can always execute a forced fetch by calling GetSinglePaymentJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournal
		{
			get	{ return _alwaysFetchPaymentJournal; }
			set	{ _alwaysFetchPaymentJournal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournal already has been fetched. Setting this property to false when PaymentJournal has been fetched
		/// will set PaymentJournal to null as well. Setting this property to true while PaymentJournal hasn't been fetched disables lazy loading for PaymentJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournal
		{
			get { return _alreadyFetchedPaymentJournal;}
			set 
			{
				if(_alreadyFetchedPaymentJournal && !value)
				{
					this.PaymentJournal = null;
				}
				_alreadyFetchedPaymentJournal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentJournal is not found
		/// in the database. When set to true, PaymentJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentJournalReturnsNewIfNotFound
		{
			get	{ return _paymentJournalReturnsNewIfNotFound; }
			set { _paymentJournalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BankStatementVerificationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBankStatementVerification()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BankStatementVerificationEntity BankStatementVerification
		{
			get	{ return GetSingleBankStatementVerification(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncBankStatementVerification(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_bankStatementVerification !=null);
						DesetupSyncBankStatementVerification(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("BankStatementVerification");
						}
					}
					else
					{
						if(_bankStatementVerification!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "BankStatement");
							SetupSyncBankStatementVerification(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BankStatementVerification. When set to true, BankStatementVerification is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankStatementVerification is accessed. You can always execute a forced fetch by calling GetSingleBankStatementVerification(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankStatementVerification
		{
			get	{ return _alwaysFetchBankStatementVerification; }
			set	{ _alwaysFetchBankStatementVerification = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property BankStatementVerification already has been fetched. Setting this property to false when BankStatementVerification has been fetched
		/// will set BankStatementVerification to null as well. Setting this property to true while BankStatementVerification hasn't been fetched disables lazy loading for BankStatementVerification</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankStatementVerification
		{
			get { return _alreadyFetchedBankStatementVerification;}
			set 
			{
				if(_alreadyFetchedBankStatementVerification && !value)
				{
					this.BankStatementVerification = null;
				}
				_alreadyFetchedBankStatementVerification = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BankStatementVerification is not found
		/// in the database. When set to true, BankStatementVerification will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BankStatementVerificationReturnsNewIfNotFound
		{
			get	{ return _bankStatementVerificationReturnsNewIfNotFound; }
			set	{ _bankStatementVerificationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BankStatementEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
