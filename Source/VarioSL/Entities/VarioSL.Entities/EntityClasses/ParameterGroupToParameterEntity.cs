﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ParameterGroupToParameter'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ParameterGroupToParameterEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ParameterEntity _parameter;
		private bool	_alwaysFetchParameter, _alreadyFetchedParameter, _parameterReturnsNewIfNotFound;
		private ParameterGroupEntity _parameterGroup;
		private bool	_alwaysFetchParameterGroup, _alreadyFetchedParameterGroup, _parameterGroupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Parameter</summary>
			public static readonly string Parameter = "Parameter";
			/// <summary>Member name ParameterGroup</summary>
			public static readonly string ParameterGroup = "ParameterGroup";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ParameterGroupToParameterEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ParameterGroupToParameterEntity() :base("ParameterGroupToParameterEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		public ParameterGroupToParameterEntity(System.Int64 parameterGroupID, System.Int64 parameterID):base("ParameterGroupToParameterEntity")
		{
			InitClassFetch(parameterGroupID, parameterID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ParameterGroupToParameterEntity(System.Int64 parameterGroupID, System.Int64 parameterID, IPrefetchPath prefetchPathToUse):base("ParameterGroupToParameterEntity")
		{
			InitClassFetch(parameterGroupID, parameterID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="validator">The custom validator object for this ParameterGroupToParameterEntity</param>
		public ParameterGroupToParameterEntity(System.Int64 parameterGroupID, System.Int64 parameterID, IValidator validator):base("ParameterGroupToParameterEntity")
		{
			InitClassFetch(parameterGroupID, parameterID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ParameterGroupToParameterEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parameter = (ParameterEntity)info.GetValue("_parameter", typeof(ParameterEntity));
			if(_parameter!=null)
			{
				_parameter.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parameterReturnsNewIfNotFound = info.GetBoolean("_parameterReturnsNewIfNotFound");
			_alwaysFetchParameter = info.GetBoolean("_alwaysFetchParameter");
			_alreadyFetchedParameter = info.GetBoolean("_alreadyFetchedParameter");

			_parameterGroup = (ParameterGroupEntity)info.GetValue("_parameterGroup", typeof(ParameterGroupEntity));
			if(_parameterGroup!=null)
			{
				_parameterGroup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parameterGroupReturnsNewIfNotFound = info.GetBoolean("_parameterGroupReturnsNewIfNotFound");
			_alwaysFetchParameterGroup = info.GetBoolean("_alwaysFetchParameterGroup");
			_alreadyFetchedParameterGroup = info.GetBoolean("_alreadyFetchedParameterGroup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ParameterGroupToParameterFieldIndex)fieldIndex)
			{
				case ParameterGroupToParameterFieldIndex.ParameterGroupID:
					DesetupSyncParameterGroup(true, false);
					_alreadyFetchedParameterGroup = false;
					break;
				case ParameterGroupToParameterFieldIndex.ParameterID:
					DesetupSyncParameter(true, false);
					_alreadyFetchedParameter = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParameter = (_parameter != null);
			_alreadyFetchedParameterGroup = (_parameterGroup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Parameter":
					toReturn.Add(Relations.ParameterEntityUsingParameterID);
					break;
				case "ParameterGroup":
					toReturn.Add(Relations.ParameterGroupEntityUsingParameterGroupID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parameter", (!this.MarkedForDeletion?_parameter:null));
			info.AddValue("_parameterReturnsNewIfNotFound", _parameterReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParameter", _alwaysFetchParameter);
			info.AddValue("_alreadyFetchedParameter", _alreadyFetchedParameter);
			info.AddValue("_parameterGroup", (!this.MarkedForDeletion?_parameterGroup:null));
			info.AddValue("_parameterGroupReturnsNewIfNotFound", _parameterGroupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParameterGroup", _alwaysFetchParameterGroup);
			info.AddValue("_alreadyFetchedParameterGroup", _alreadyFetchedParameterGroup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Parameter":
					_alreadyFetchedParameter = true;
					this.Parameter = (ParameterEntity)entity;
					break;
				case "ParameterGroup":
					_alreadyFetchedParameterGroup = true;
					this.ParameterGroup = (ParameterGroupEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Parameter":
					SetupSyncParameter(relatedEntity);
					break;
				case "ParameterGroup":
					SetupSyncParameterGroup(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Parameter":
					DesetupSyncParameter(false, true);
					break;
				case "ParameterGroup":
					DesetupSyncParameterGroup(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_parameter!=null)
			{
				toReturn.Add(_parameter);
			}
			if(_parameterGroup!=null)
			{
				toReturn.Add(_parameterGroup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterGroupID, System.Int64 parameterID)
		{
			return FetchUsingPK(parameterGroupID, parameterID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterGroupID, System.Int64 parameterID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(parameterGroupID, parameterID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterGroupID, System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(parameterGroupID, parameterID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterGroupID, System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(parameterGroupID, parameterID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParameterGroupID, this.ParameterID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ParameterGroupToParameterRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ParameterEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParameterEntity' which is related to this entity.</returns>
		public ParameterEntity GetSingleParameter()
		{
			return GetSingleParameter(false);
		}

		/// <summary> Retrieves the related entity of type 'ParameterEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParameterEntity' which is related to this entity.</returns>
		public virtual ParameterEntity GetSingleParameter(bool forceFetch)
		{
			if( ( !_alreadyFetchedParameter || forceFetch || _alwaysFetchParameter) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParameterEntityUsingParameterID);
				ParameterEntity newEntity = new ParameterEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParameterID);
				}
				if(fetchResult)
				{
					newEntity = (ParameterEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parameterReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Parameter = newEntity;
				_alreadyFetchedParameter = fetchResult;
			}
			return _parameter;
		}


		/// <summary> Retrieves the related entity of type 'ParameterGroupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParameterGroupEntity' which is related to this entity.</returns>
		public ParameterGroupEntity GetSingleParameterGroup()
		{
			return GetSingleParameterGroup(false);
		}

		/// <summary> Retrieves the related entity of type 'ParameterGroupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParameterGroupEntity' which is related to this entity.</returns>
		public virtual ParameterGroupEntity GetSingleParameterGroup(bool forceFetch)
		{
			if( ( !_alreadyFetchedParameterGroup || forceFetch || _alwaysFetchParameterGroup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParameterGroupEntityUsingParameterGroupID);
				ParameterGroupEntity newEntity = new ParameterGroupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParameterGroupID);
				}
				if(fetchResult)
				{
					newEntity = (ParameterGroupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parameterGroupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParameterGroup = newEntity;
				_alreadyFetchedParameterGroup = fetchResult;
			}
			return _parameterGroup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Parameter", _parameter);
			toReturn.Add("ParameterGroup", _parameterGroup);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="validator">The validator object for this ParameterGroupToParameterEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 parameterGroupID, System.Int64 parameterID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(parameterGroupID, parameterID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_parameterReturnsNewIfNotFound = false;
			_parameterGroupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultValueOverride", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _parameter</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParameter(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parameter, new PropertyChangedEventHandler( OnParameterPropertyChanged ), "Parameter", VarioSL.Entities.RelationClasses.StaticParameterGroupToParameterRelations.ParameterEntityUsingParameterIDStatic, true, signalRelatedEntity, "ParameterGroupToParameters", resetFKFields, new int[] { (int)ParameterGroupToParameterFieldIndex.ParameterID } );		
			_parameter = null;
		}
		
		/// <summary> setups the sync logic for member _parameter</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParameter(IEntityCore relatedEntity)
		{
			if(_parameter!=relatedEntity)
			{		
				DesetupSyncParameter(true, true);
				_parameter = (ParameterEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parameter, new PropertyChangedEventHandler( OnParameterPropertyChanged ), "Parameter", VarioSL.Entities.RelationClasses.StaticParameterGroupToParameterRelations.ParameterEntityUsingParameterIDStatic, true, ref _alreadyFetchedParameter, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParameterPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parameterGroup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParameterGroup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parameterGroup, new PropertyChangedEventHandler( OnParameterGroupPropertyChanged ), "ParameterGroup", VarioSL.Entities.RelationClasses.StaticParameterGroupToParameterRelations.ParameterGroupEntityUsingParameterGroupIDStatic, true, signalRelatedEntity, "ParameterGroupToParameters", resetFKFields, new int[] { (int)ParameterGroupToParameterFieldIndex.ParameterGroupID } );		
			_parameterGroup = null;
		}
		
		/// <summary> setups the sync logic for member _parameterGroup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParameterGroup(IEntityCore relatedEntity)
		{
			if(_parameterGroup!=relatedEntity)
			{		
				DesetupSyncParameterGroup(true, true);
				_parameterGroup = (ParameterGroupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parameterGroup, new PropertyChangedEventHandler( OnParameterGroupPropertyChanged ), "ParameterGroup", VarioSL.Entities.RelationClasses.StaticParameterGroupToParameterRelations.ParameterGroupEntityUsingParameterGroupIDStatic, true, ref _alreadyFetchedParameterGroup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParameterGroupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="parameterGroupID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="parameterID">PK value for ParameterGroupToParameter which data should be fetched into this ParameterGroupToParameter object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 parameterGroupID, System.Int64 parameterID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ParameterGroupToParameterFieldIndex.ParameterGroupID].ForcedCurrentValueWrite(parameterGroupID);
				this.Fields[(int)ParameterGroupToParameterFieldIndex.ParameterID].ForcedCurrentValueWrite(parameterID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateParameterGroupToParameterDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ParameterGroupToParameterEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ParameterGroupToParameterRelations Relations
		{
			get	{ return new ParameterGroupToParameterRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Parameter'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameter
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterCollection(), (IEntityRelation)GetRelationsForField("Parameter")[0], (int)VarioSL.Entities.EntityType.ParameterGroupToParameterEntity, (int)VarioSL.Entities.EntityType.ParameterEntity, 0, null, null, null, "Parameter", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterGroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterGroup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterGroupCollection(), (IEntityRelation)GetRelationsForField("ParameterGroup")[0], (int)VarioSL.Entities.EntityType.ParameterGroupToParameterEntity, (int)VarioSL.Entities.EntityType.ParameterGroupEntity, 0, null, null, null, "ParameterGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DefaultValueOverride property of the Entity ParameterGroupToParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERGROUPTOPARAMETER"."DEFAULTVALUEOVERRIDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultValueOverride
		{
			get { return (System.String)GetValue((int)ParameterGroupToParameterFieldIndex.DefaultValueOverride, true); }
			set	{ SetValue((int)ParameterGroupToParameterFieldIndex.DefaultValueOverride, value, true); }
		}

		/// <summary> The ParameterGroupID property of the Entity ParameterGroupToParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERGROUPTOPARAMETER"."PARAMETERGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ParameterGroupID
		{
			get { return (System.Int64)GetValue((int)ParameterGroupToParameterFieldIndex.ParameterGroupID, true); }
			set	{ SetValue((int)ParameterGroupToParameterFieldIndex.ParameterGroupID, value, true); }
		}

		/// <summary> The ParameterID property of the Entity ParameterGroupToParameter<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERGROUPTOPARAMETER"."PARAMETERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 ParameterID
		{
			get { return (System.Int64)GetValue((int)ParameterGroupToParameterFieldIndex.ParameterID, true); }
			set	{ SetValue((int)ParameterGroupToParameterFieldIndex.ParameterID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ParameterEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParameter()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParameterEntity Parameter
		{
			get	{ return GetSingleParameter(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParameter(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterGroupToParameters", "Parameter", _parameter, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Parameter. When set to true, Parameter is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Parameter is accessed. You can always execute a forced fetch by calling GetSingleParameter(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameter
		{
			get	{ return _alwaysFetchParameter; }
			set	{ _alwaysFetchParameter = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Parameter already has been fetched. Setting this property to false when Parameter has been fetched
		/// will set Parameter to null as well. Setting this property to true while Parameter hasn't been fetched disables lazy loading for Parameter</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameter
		{
			get { return _alreadyFetchedParameter;}
			set 
			{
				if(_alreadyFetchedParameter && !value)
				{
					this.Parameter = null;
				}
				_alreadyFetchedParameter = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Parameter is not found
		/// in the database. When set to true, Parameter will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParameterReturnsNewIfNotFound
		{
			get	{ return _parameterReturnsNewIfNotFound; }
			set { _parameterReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ParameterGroupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParameterGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParameterGroupEntity ParameterGroup
		{
			get	{ return GetSingleParameterGroup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParameterGroup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterGroupToParameters", "ParameterGroup", _parameterGroup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterGroup. When set to true, ParameterGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterGroup is accessed. You can always execute a forced fetch by calling GetSingleParameterGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterGroup
		{
			get	{ return _alwaysFetchParameterGroup; }
			set	{ _alwaysFetchParameterGroup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterGroup already has been fetched. Setting this property to false when ParameterGroup has been fetched
		/// will set ParameterGroup to null as well. Setting this property to true while ParameterGroup hasn't been fetched disables lazy loading for ParameterGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterGroup
		{
			get { return _alreadyFetchedParameterGroup;}
			set 
			{
				if(_alreadyFetchedParameterGroup && !value)
				{
					this.ParameterGroup = null;
				}
				_alreadyFetchedParameterGroup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParameterGroup is not found
		/// in the database. When set to true, ParameterGroup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParameterGroupReturnsNewIfNotFound
		{
			get	{ return _parameterGroupReturnsNewIfNotFound; }
			set { _parameterGroupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ParameterGroupToParameterEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
