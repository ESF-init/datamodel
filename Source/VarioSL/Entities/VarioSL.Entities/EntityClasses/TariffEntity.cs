﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Tariff'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TariffEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AreaListCollection	_areaList;
		private bool	_alwaysFetchAreaList, _alreadyFetchedAreaList;
		private VarioSL.Entities.CollectionClasses.AreaListElementCollection	_areaListElements;
		private bool	_alwaysFetchAreaListElements, _alreadyFetchedAreaListElements;
		private VarioSL.Entities.CollectionClasses.AttributeCollection	_attributes;
		private bool	_alwaysFetchAttributes, _alreadyFetchedAttributes;
		private VarioSL.Entities.CollectionClasses.BusinessRuleCollection	_businessRule;
		private bool	_alwaysFetchBusinessRule, _alreadyFetchedBusinessRule;
		private VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection	_businessRuleCondition;
		private bool	_alwaysFetchBusinessRuleCondition, _alreadyFetchedBusinessRuleCondition;
		private VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection	_businessRuleResult;
		private bool	_alwaysFetchBusinessRuleResult, _alreadyFetchedBusinessRuleResult;
		private VarioSL.Entities.CollectionClasses.CalendarCollection	_calendars;
		private bool	_alwaysFetchCalendars, _alreadyFetchedCalendars;
		private VarioSL.Entities.CollectionClasses.CardTicketCollection	_cardTickets;
		private bool	_alwaysFetchCardTickets, _alreadyFetchedCardTickets;
		private VarioSL.Entities.CollectionClasses.ChoiceCollection	_choices;
		private bool	_alwaysFetchChoices, _alreadyFetchedChoices;
		private VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection	_clientAdaptedLayoutObjects;
		private bool	_alwaysFetchClientAdaptedLayoutObjects, _alreadyFetchedClientAdaptedLayoutObjects;
		private VarioSL.Entities.CollectionClasses.EticketCollection	_etickets;
		private bool	_alwaysFetchEtickets, _alreadyFetchedEtickets;
		private VarioSL.Entities.CollectionClasses.ExternalCardCollection	_externalCards;
		private bool	_alwaysFetchExternalCards, _alreadyFetchedExternalCards;
		private VarioSL.Entities.CollectionClasses.ExternalEffortCollection	_externalEfforts;
		private bool	_alwaysFetchExternalEfforts, _alreadyFetchedExternalEfforts;
		private VarioSL.Entities.CollectionClasses.ExternalPacketCollection	_externalPackets;
		private bool	_alwaysFetchExternalPackets, _alreadyFetchedExternalPackets;
		private VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection	_externalPacketefforts;
		private bool	_alwaysFetchExternalPacketefforts, _alreadyFetchedExternalPacketefforts;
		private VarioSL.Entities.CollectionClasses.ExternalTypeCollection	_externalTypes;
		private bool	_alwaysFetchExternalTypes, _alreadyFetchedExternalTypes;
		private VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection	_fareEvasionCategories;
		private bool	_alwaysFetchFareEvasionCategories, _alreadyFetchedFareEvasionCategories;
		private VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection	_fareEvasionReasons;
		private bool	_alwaysFetchFareEvasionReasons, _alreadyFetchedFareEvasionReasons;
		private VarioSL.Entities.CollectionClasses.FareMatrixCollection	_fareMatrices;
		private bool	_alwaysFetchFareMatrices, _alreadyFetchedFareMatrices;
		private VarioSL.Entities.CollectionClasses.FareStageListCollection	_fareStageLists;
		private bool	_alwaysFetchFareStageLists, _alreadyFetchedFareStageLists;
		private VarioSL.Entities.CollectionClasses.FareTableCollection	_fareTables;
		private bool	_alwaysFetchFareTables, _alreadyFetchedFareTables;
		private VarioSL.Entities.CollectionClasses.GuiDefCollection	_guiDefs;
		private bool	_alwaysFetchGuiDefs, _alreadyFetchedGuiDefs;
		private VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection	_keyAttributeTransforms;
		private bool	_alwaysFetchKeyAttributeTransforms, _alreadyFetchedKeyAttributeTransforms;
		private VarioSL.Entities.CollectionClasses.LayoutCollection	_layouts;
		private bool	_alwaysFetchLayouts, _alreadyFetchedLayouts;
		private VarioSL.Entities.CollectionClasses.LineGroupCollection	_lineGroups;
		private bool	_alwaysFetchLineGroups, _alreadyFetchedLineGroups;
		private VarioSL.Entities.CollectionClasses.LogoCollection	_logos;
		private bool	_alwaysFetchLogos, _alreadyFetchedLogos;
		private VarioSL.Entities.CollectionClasses.PageContentGroupCollection	_pageContentGroups;
		private bool	_alwaysFetchPageContentGroups, _alreadyFetchedPageContentGroups;
		private VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection	_parameterAttributeValues;
		private bool	_alwaysFetchParameterAttributeValues, _alreadyFetchedParameterAttributeValues;
		private VarioSL.Entities.CollectionClasses.ParameterFareStageCollection	_parameterFareStage;
		private bool	_alwaysFetchParameterFareStage, _alreadyFetchedParameterFareStage;
		private VarioSL.Entities.CollectionClasses.ParameterTariffCollection	_parameterTariff;
		private bool	_alwaysFetchParameterTariff, _alreadyFetchedParameterTariff;
		private VarioSL.Entities.CollectionClasses.ParameterTicketCollection	_parameterTickets;
		private bool	_alwaysFetchParameterTickets, _alreadyFetchedParameterTickets;
		private VarioSL.Entities.CollectionClasses.PredefinedKeyCollection	_predefinedKeys;
		private bool	_alwaysFetchPredefinedKeys, _alreadyFetchedPredefinedKeys;
		private VarioSL.Entities.CollectionClasses.PrimalKeyCollection	_primalKeys;
		private bool	_alwaysFetchPrimalKeys, _alreadyFetchedPrimalKeys;
		private VarioSL.Entities.CollectionClasses.PrintTextCollection	_printTexts;
		private bool	_alwaysFetchPrintTexts, _alreadyFetchedPrintTexts;
		private VarioSL.Entities.CollectionClasses.RouteCollection	_routes;
		private bool	_alwaysFetchRoutes, _alreadyFetchedRoutes;
		private VarioSL.Entities.CollectionClasses.RouteNameCollection	_routeNames;
		private bool	_alwaysFetchRouteNames, _alreadyFetchedRouteNames;
		private VarioSL.Entities.CollectionClasses.RuleCappingCollection	_ruleCappings;
		private bool	_alwaysFetchRuleCappings, _alreadyFetchedRuleCappings;
		private VarioSL.Entities.CollectionClasses.ServiceAllocationCollection	_serviceAllocations;
		private bool	_alwaysFetchServiceAllocations, _alreadyFetchedServiceAllocations;
		private VarioSL.Entities.CollectionClasses.ShortDistanceCollection	_shortDistances;
		private bool	_alwaysFetchShortDistances, _alreadyFetchedShortDistances;
		private VarioSL.Entities.CollectionClasses.SpecialReceiptCollection	_specialReceipts;
		private bool	_alwaysFetchSpecialReceipts, _alreadyFetchedSpecialReceipts;
		private VarioSL.Entities.CollectionClasses.SystemTextCollection	_systemTexts;
		private bool	_alwaysFetchSystemTexts, _alreadyFetchedSystemTexts;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_childTariffs;
		private bool	_alwaysFetchChildTariffs, _alreadyFetchedChildTariffs;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_matrixParentTariff;
		private bool	_alwaysFetchMatrixParentTariff, _alreadyFetchedMatrixParentTariff;
		private VarioSL.Entities.CollectionClasses.TariffReleaseCollection	_tariffRelease;
		private bool	_alwaysFetchTariffRelease, _alreadyFetchedTariffRelease;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketGroupCollection	_ticketGroups;
		private bool	_alwaysFetchTicketGroups, _alreadyFetchedTicketGroups;
		private VarioSL.Entities.CollectionClasses.TranslationCollection	_translations;
		private bool	_alwaysFetchTranslations, _alreadyFetchedTranslations;
		private VarioSL.Entities.CollectionClasses.UserKeyCollection	_userKeys;
		private bool	_alwaysFetchUserKeys, _alreadyFetchedUserKeys;
		private VarioSL.Entities.CollectionClasses.VdvKeySetCollection	_vdvKeySet;
		private bool	_alwaysFetchVdvKeySet, _alreadyFetchedVdvKeySet;
		private VarioSL.Entities.CollectionClasses.VdvLayoutCollection	_vdvLayout;
		private bool	_alwaysFetchVdvLayout, _alreadyFetchedVdvLayout;
		private VarioSL.Entities.CollectionClasses.VdvProductCollection	_vdvProduct;
		private bool	_alwaysFetchVdvProduct, _alreadyFetchedVdvProduct;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private ClientEntity _ownerClient;
		private bool	_alwaysFetchOwnerClient, _alreadyFetchedOwnerClient, _ownerClientReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private NetEntity _net;
		private bool	_alwaysFetchNet, _alreadyFetchedNet, _netReturnsNewIfNotFound;
		private TariffEntity _baseTariff;
		private bool	_alwaysFetchBaseTariff, _alreadyFetchedBaseTariff, _baseTariffReturnsNewIfNotFound;
		private TariffEntity _matrixTariff;
		private bool	_alwaysFetchMatrixTariff, _alreadyFetchedMatrixTariff, _matrixTariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name OwnerClient</summary>
			public static readonly string OwnerClient = "OwnerClient";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Net</summary>
			public static readonly string Net = "Net";
			/// <summary>Member name BaseTariff</summary>
			public static readonly string BaseTariff = "BaseTariff";
			/// <summary>Member name MatrixTariff</summary>
			public static readonly string MatrixTariff = "MatrixTariff";
			/// <summary>Member name AreaList</summary>
			public static readonly string AreaList = "AreaList";
			/// <summary>Member name AreaListElements</summary>
			public static readonly string AreaListElements = "AreaListElements";
			/// <summary>Member name Attributes</summary>
			public static readonly string Attributes = "Attributes";
			/// <summary>Member name BusinessRule</summary>
			public static readonly string BusinessRule = "BusinessRule";
			/// <summary>Member name BusinessRuleCondition</summary>
			public static readonly string BusinessRuleCondition = "BusinessRuleCondition";
			/// <summary>Member name BusinessRuleResult</summary>
			public static readonly string BusinessRuleResult = "BusinessRuleResult";
			/// <summary>Member name Calendars</summary>
			public static readonly string Calendars = "Calendars";
			/// <summary>Member name CardTickets</summary>
			public static readonly string CardTickets = "CardTickets";
			/// <summary>Member name Choices</summary>
			public static readonly string Choices = "Choices";
			/// <summary>Member name ClientAdaptedLayoutObjects</summary>
			public static readonly string ClientAdaptedLayoutObjects = "ClientAdaptedLayoutObjects";
			/// <summary>Member name Etickets</summary>
			public static readonly string Etickets = "Etickets";
			/// <summary>Member name ExternalCards</summary>
			public static readonly string ExternalCards = "ExternalCards";
			/// <summary>Member name ExternalEfforts</summary>
			public static readonly string ExternalEfforts = "ExternalEfforts";
			/// <summary>Member name ExternalPackets</summary>
			public static readonly string ExternalPackets = "ExternalPackets";
			/// <summary>Member name ExternalPacketefforts</summary>
			public static readonly string ExternalPacketefforts = "ExternalPacketefforts";
			/// <summary>Member name ExternalTypes</summary>
			public static readonly string ExternalTypes = "ExternalTypes";
			/// <summary>Member name FareEvasionCategories</summary>
			public static readonly string FareEvasionCategories = "FareEvasionCategories";
			/// <summary>Member name FareEvasionReasons</summary>
			public static readonly string FareEvasionReasons = "FareEvasionReasons";
			/// <summary>Member name FareMatrices</summary>
			public static readonly string FareMatrices = "FareMatrices";
			/// <summary>Member name FareStageLists</summary>
			public static readonly string FareStageLists = "FareStageLists";
			/// <summary>Member name FareTables</summary>
			public static readonly string FareTables = "FareTables";
			/// <summary>Member name GuiDefs</summary>
			public static readonly string GuiDefs = "GuiDefs";
			/// <summary>Member name KeyAttributeTransforms</summary>
			public static readonly string KeyAttributeTransforms = "KeyAttributeTransforms";
			/// <summary>Member name Layouts</summary>
			public static readonly string Layouts = "Layouts";
			/// <summary>Member name LineGroups</summary>
			public static readonly string LineGroups = "LineGroups";
			/// <summary>Member name Logos</summary>
			public static readonly string Logos = "Logos";
			/// <summary>Member name PageContentGroups</summary>
			public static readonly string PageContentGroups = "PageContentGroups";
			/// <summary>Member name ParameterAttributeValues</summary>
			public static readonly string ParameterAttributeValues = "ParameterAttributeValues";
			/// <summary>Member name ParameterFareStage</summary>
			public static readonly string ParameterFareStage = "ParameterFareStage";
			/// <summary>Member name ParameterTariff</summary>
			public static readonly string ParameterTariff = "ParameterTariff";
			/// <summary>Member name ParameterTickets</summary>
			public static readonly string ParameterTickets = "ParameterTickets";
			/// <summary>Member name PredefinedKeys</summary>
			public static readonly string PredefinedKeys = "PredefinedKeys";
			/// <summary>Member name PrimalKeys</summary>
			public static readonly string PrimalKeys = "PrimalKeys";
			/// <summary>Member name PrintTexts</summary>
			public static readonly string PrintTexts = "PrintTexts";
			/// <summary>Member name Routes</summary>
			public static readonly string Routes = "Routes";
			/// <summary>Member name RouteNames</summary>
			public static readonly string RouteNames = "RouteNames";
			/// <summary>Member name RuleCappings</summary>
			public static readonly string RuleCappings = "RuleCappings";
			/// <summary>Member name ServiceAllocations</summary>
			public static readonly string ServiceAllocations = "ServiceAllocations";
			/// <summary>Member name ShortDistances</summary>
			public static readonly string ShortDistances = "ShortDistances";
			/// <summary>Member name SpecialReceipts</summary>
			public static readonly string SpecialReceipts = "SpecialReceipts";
			/// <summary>Member name SystemTexts</summary>
			public static readonly string SystemTexts = "SystemTexts";
			/// <summary>Member name ChildTariffs</summary>
			public static readonly string ChildTariffs = "ChildTariffs";
			/// <summary>Member name MatrixParentTariff</summary>
			public static readonly string MatrixParentTariff = "MatrixParentTariff";
			/// <summary>Member name TariffRelease</summary>
			public static readonly string TariffRelease = "TariffRelease";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketGroups</summary>
			public static readonly string TicketGroups = "TicketGroups";
			/// <summary>Member name Translations</summary>
			public static readonly string Translations = "Translations";
			/// <summary>Member name UserKeys</summary>
			public static readonly string UserKeys = "UserKeys";
			/// <summary>Member name VdvKeySet</summary>
			public static readonly string VdvKeySet = "VdvKeySet";
			/// <summary>Member name VdvLayout</summary>
			public static readonly string VdvLayout = "VdvLayout";
			/// <summary>Member name VdvProduct</summary>
			public static readonly string VdvProduct = "VdvProduct";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TariffEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TariffEntity() :base("TariffEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		public TariffEntity(System.Int64 tarifID):base("TariffEntity")
		{
			InitClassFetch(tarifID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TariffEntity(System.Int64 tarifID, IPrefetchPath prefetchPathToUse):base("TariffEntity")
		{
			InitClassFetch(tarifID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="validator">The custom validator object for this TariffEntity</param>
		public TariffEntity(System.Int64 tarifID, IValidator validator):base("TariffEntity")
		{
			InitClassFetch(tarifID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TariffEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaList = (VarioSL.Entities.CollectionClasses.AreaListCollection)info.GetValue("_areaList", typeof(VarioSL.Entities.CollectionClasses.AreaListCollection));
			_alwaysFetchAreaList = info.GetBoolean("_alwaysFetchAreaList");
			_alreadyFetchedAreaList = info.GetBoolean("_alreadyFetchedAreaList");

			_areaListElements = (VarioSL.Entities.CollectionClasses.AreaListElementCollection)info.GetValue("_areaListElements", typeof(VarioSL.Entities.CollectionClasses.AreaListElementCollection));
			_alwaysFetchAreaListElements = info.GetBoolean("_alwaysFetchAreaListElements");
			_alreadyFetchedAreaListElements = info.GetBoolean("_alreadyFetchedAreaListElements");

			_attributes = (VarioSL.Entities.CollectionClasses.AttributeCollection)info.GetValue("_attributes", typeof(VarioSL.Entities.CollectionClasses.AttributeCollection));
			_alwaysFetchAttributes = info.GetBoolean("_alwaysFetchAttributes");
			_alreadyFetchedAttributes = info.GetBoolean("_alreadyFetchedAttributes");

			_businessRule = (VarioSL.Entities.CollectionClasses.BusinessRuleCollection)info.GetValue("_businessRule", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleCollection));
			_alwaysFetchBusinessRule = info.GetBoolean("_alwaysFetchBusinessRule");
			_alreadyFetchedBusinessRule = info.GetBoolean("_alreadyFetchedBusinessRule");

			_businessRuleCondition = (VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection)info.GetValue("_businessRuleCondition", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection));
			_alwaysFetchBusinessRuleCondition = info.GetBoolean("_alwaysFetchBusinessRuleCondition");
			_alreadyFetchedBusinessRuleCondition = info.GetBoolean("_alreadyFetchedBusinessRuleCondition");

			_businessRuleResult = (VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection)info.GetValue("_businessRuleResult", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection));
			_alwaysFetchBusinessRuleResult = info.GetBoolean("_alwaysFetchBusinessRuleResult");
			_alreadyFetchedBusinessRuleResult = info.GetBoolean("_alreadyFetchedBusinessRuleResult");

			_calendars = (VarioSL.Entities.CollectionClasses.CalendarCollection)info.GetValue("_calendars", typeof(VarioSL.Entities.CollectionClasses.CalendarCollection));
			_alwaysFetchCalendars = info.GetBoolean("_alwaysFetchCalendars");
			_alreadyFetchedCalendars = info.GetBoolean("_alreadyFetchedCalendars");

			_cardTickets = (VarioSL.Entities.CollectionClasses.CardTicketCollection)info.GetValue("_cardTickets", typeof(VarioSL.Entities.CollectionClasses.CardTicketCollection));
			_alwaysFetchCardTickets = info.GetBoolean("_alwaysFetchCardTickets");
			_alreadyFetchedCardTickets = info.GetBoolean("_alreadyFetchedCardTickets");

			_choices = (VarioSL.Entities.CollectionClasses.ChoiceCollection)info.GetValue("_choices", typeof(VarioSL.Entities.CollectionClasses.ChoiceCollection));
			_alwaysFetchChoices = info.GetBoolean("_alwaysFetchChoices");
			_alreadyFetchedChoices = info.GetBoolean("_alreadyFetchedChoices");

			_clientAdaptedLayoutObjects = (VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection)info.GetValue("_clientAdaptedLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection));
			_alwaysFetchClientAdaptedLayoutObjects = info.GetBoolean("_alwaysFetchClientAdaptedLayoutObjects");
			_alreadyFetchedClientAdaptedLayoutObjects = info.GetBoolean("_alreadyFetchedClientAdaptedLayoutObjects");

			_etickets = (VarioSL.Entities.CollectionClasses.EticketCollection)info.GetValue("_etickets", typeof(VarioSL.Entities.CollectionClasses.EticketCollection));
			_alwaysFetchEtickets = info.GetBoolean("_alwaysFetchEtickets");
			_alreadyFetchedEtickets = info.GetBoolean("_alreadyFetchedEtickets");

			_externalCards = (VarioSL.Entities.CollectionClasses.ExternalCardCollection)info.GetValue("_externalCards", typeof(VarioSL.Entities.CollectionClasses.ExternalCardCollection));
			_alwaysFetchExternalCards = info.GetBoolean("_alwaysFetchExternalCards");
			_alreadyFetchedExternalCards = info.GetBoolean("_alreadyFetchedExternalCards");

			_externalEfforts = (VarioSL.Entities.CollectionClasses.ExternalEffortCollection)info.GetValue("_externalEfforts", typeof(VarioSL.Entities.CollectionClasses.ExternalEffortCollection));
			_alwaysFetchExternalEfforts = info.GetBoolean("_alwaysFetchExternalEfforts");
			_alreadyFetchedExternalEfforts = info.GetBoolean("_alreadyFetchedExternalEfforts");

			_externalPackets = (VarioSL.Entities.CollectionClasses.ExternalPacketCollection)info.GetValue("_externalPackets", typeof(VarioSL.Entities.CollectionClasses.ExternalPacketCollection));
			_alwaysFetchExternalPackets = info.GetBoolean("_alwaysFetchExternalPackets");
			_alreadyFetchedExternalPackets = info.GetBoolean("_alreadyFetchedExternalPackets");

			_externalPacketefforts = (VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection)info.GetValue("_externalPacketefforts", typeof(VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection));
			_alwaysFetchExternalPacketefforts = info.GetBoolean("_alwaysFetchExternalPacketefforts");
			_alreadyFetchedExternalPacketefforts = info.GetBoolean("_alreadyFetchedExternalPacketefforts");

			_externalTypes = (VarioSL.Entities.CollectionClasses.ExternalTypeCollection)info.GetValue("_externalTypes", typeof(VarioSL.Entities.CollectionClasses.ExternalTypeCollection));
			_alwaysFetchExternalTypes = info.GetBoolean("_alwaysFetchExternalTypes");
			_alreadyFetchedExternalTypes = info.GetBoolean("_alreadyFetchedExternalTypes");

			_fareEvasionCategories = (VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection)info.GetValue("_fareEvasionCategories", typeof(VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection));
			_alwaysFetchFareEvasionCategories = info.GetBoolean("_alwaysFetchFareEvasionCategories");
			_alreadyFetchedFareEvasionCategories = info.GetBoolean("_alreadyFetchedFareEvasionCategories");

			_fareEvasionReasons = (VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection)info.GetValue("_fareEvasionReasons", typeof(VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection));
			_alwaysFetchFareEvasionReasons = info.GetBoolean("_alwaysFetchFareEvasionReasons");
			_alreadyFetchedFareEvasionReasons = info.GetBoolean("_alreadyFetchedFareEvasionReasons");

			_fareMatrices = (VarioSL.Entities.CollectionClasses.FareMatrixCollection)info.GetValue("_fareMatrices", typeof(VarioSL.Entities.CollectionClasses.FareMatrixCollection));
			_alwaysFetchFareMatrices = info.GetBoolean("_alwaysFetchFareMatrices");
			_alreadyFetchedFareMatrices = info.GetBoolean("_alreadyFetchedFareMatrices");

			_fareStageLists = (VarioSL.Entities.CollectionClasses.FareStageListCollection)info.GetValue("_fareStageLists", typeof(VarioSL.Entities.CollectionClasses.FareStageListCollection));
			_alwaysFetchFareStageLists = info.GetBoolean("_alwaysFetchFareStageLists");
			_alreadyFetchedFareStageLists = info.GetBoolean("_alreadyFetchedFareStageLists");

			_fareTables = (VarioSL.Entities.CollectionClasses.FareTableCollection)info.GetValue("_fareTables", typeof(VarioSL.Entities.CollectionClasses.FareTableCollection));
			_alwaysFetchFareTables = info.GetBoolean("_alwaysFetchFareTables");
			_alreadyFetchedFareTables = info.GetBoolean("_alreadyFetchedFareTables");

			_guiDefs = (VarioSL.Entities.CollectionClasses.GuiDefCollection)info.GetValue("_guiDefs", typeof(VarioSL.Entities.CollectionClasses.GuiDefCollection));
			_alwaysFetchGuiDefs = info.GetBoolean("_alwaysFetchGuiDefs");
			_alreadyFetchedGuiDefs = info.GetBoolean("_alreadyFetchedGuiDefs");

			_keyAttributeTransforms = (VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection)info.GetValue("_keyAttributeTransforms", typeof(VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection));
			_alwaysFetchKeyAttributeTransforms = info.GetBoolean("_alwaysFetchKeyAttributeTransforms");
			_alreadyFetchedKeyAttributeTransforms = info.GetBoolean("_alreadyFetchedKeyAttributeTransforms");

			_layouts = (VarioSL.Entities.CollectionClasses.LayoutCollection)info.GetValue("_layouts", typeof(VarioSL.Entities.CollectionClasses.LayoutCollection));
			_alwaysFetchLayouts = info.GetBoolean("_alwaysFetchLayouts");
			_alreadyFetchedLayouts = info.GetBoolean("_alreadyFetchedLayouts");

			_lineGroups = (VarioSL.Entities.CollectionClasses.LineGroupCollection)info.GetValue("_lineGroups", typeof(VarioSL.Entities.CollectionClasses.LineGroupCollection));
			_alwaysFetchLineGroups = info.GetBoolean("_alwaysFetchLineGroups");
			_alreadyFetchedLineGroups = info.GetBoolean("_alreadyFetchedLineGroups");

			_logos = (VarioSL.Entities.CollectionClasses.LogoCollection)info.GetValue("_logos", typeof(VarioSL.Entities.CollectionClasses.LogoCollection));
			_alwaysFetchLogos = info.GetBoolean("_alwaysFetchLogos");
			_alreadyFetchedLogos = info.GetBoolean("_alreadyFetchedLogos");

			_pageContentGroups = (VarioSL.Entities.CollectionClasses.PageContentGroupCollection)info.GetValue("_pageContentGroups", typeof(VarioSL.Entities.CollectionClasses.PageContentGroupCollection));
			_alwaysFetchPageContentGroups = info.GetBoolean("_alwaysFetchPageContentGroups");
			_alreadyFetchedPageContentGroups = info.GetBoolean("_alreadyFetchedPageContentGroups");

			_parameterAttributeValues = (VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection)info.GetValue("_parameterAttributeValues", typeof(VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection));
			_alwaysFetchParameterAttributeValues = info.GetBoolean("_alwaysFetchParameterAttributeValues");
			_alreadyFetchedParameterAttributeValues = info.GetBoolean("_alreadyFetchedParameterAttributeValues");

			_parameterFareStage = (VarioSL.Entities.CollectionClasses.ParameterFareStageCollection)info.GetValue("_parameterFareStage", typeof(VarioSL.Entities.CollectionClasses.ParameterFareStageCollection));
			_alwaysFetchParameterFareStage = info.GetBoolean("_alwaysFetchParameterFareStage");
			_alreadyFetchedParameterFareStage = info.GetBoolean("_alreadyFetchedParameterFareStage");

			_parameterTariff = (VarioSL.Entities.CollectionClasses.ParameterTariffCollection)info.GetValue("_parameterTariff", typeof(VarioSL.Entities.CollectionClasses.ParameterTariffCollection));
			_alwaysFetchParameterTariff = info.GetBoolean("_alwaysFetchParameterTariff");
			_alreadyFetchedParameterTariff = info.GetBoolean("_alreadyFetchedParameterTariff");

			_parameterTickets = (VarioSL.Entities.CollectionClasses.ParameterTicketCollection)info.GetValue("_parameterTickets", typeof(VarioSL.Entities.CollectionClasses.ParameterTicketCollection));
			_alwaysFetchParameterTickets = info.GetBoolean("_alwaysFetchParameterTickets");
			_alreadyFetchedParameterTickets = info.GetBoolean("_alreadyFetchedParameterTickets");

			_predefinedKeys = (VarioSL.Entities.CollectionClasses.PredefinedKeyCollection)info.GetValue("_predefinedKeys", typeof(VarioSL.Entities.CollectionClasses.PredefinedKeyCollection));
			_alwaysFetchPredefinedKeys = info.GetBoolean("_alwaysFetchPredefinedKeys");
			_alreadyFetchedPredefinedKeys = info.GetBoolean("_alreadyFetchedPredefinedKeys");

			_primalKeys = (VarioSL.Entities.CollectionClasses.PrimalKeyCollection)info.GetValue("_primalKeys", typeof(VarioSL.Entities.CollectionClasses.PrimalKeyCollection));
			_alwaysFetchPrimalKeys = info.GetBoolean("_alwaysFetchPrimalKeys");
			_alreadyFetchedPrimalKeys = info.GetBoolean("_alreadyFetchedPrimalKeys");

			_printTexts = (VarioSL.Entities.CollectionClasses.PrintTextCollection)info.GetValue("_printTexts", typeof(VarioSL.Entities.CollectionClasses.PrintTextCollection));
			_alwaysFetchPrintTexts = info.GetBoolean("_alwaysFetchPrintTexts");
			_alreadyFetchedPrintTexts = info.GetBoolean("_alreadyFetchedPrintTexts");

			_routes = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_routes", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchRoutes = info.GetBoolean("_alwaysFetchRoutes");
			_alreadyFetchedRoutes = info.GetBoolean("_alreadyFetchedRoutes");

			_routeNames = (VarioSL.Entities.CollectionClasses.RouteNameCollection)info.GetValue("_routeNames", typeof(VarioSL.Entities.CollectionClasses.RouteNameCollection));
			_alwaysFetchRouteNames = info.GetBoolean("_alwaysFetchRouteNames");
			_alreadyFetchedRouteNames = info.GetBoolean("_alreadyFetchedRouteNames");

			_ruleCappings = (VarioSL.Entities.CollectionClasses.RuleCappingCollection)info.GetValue("_ruleCappings", typeof(VarioSL.Entities.CollectionClasses.RuleCappingCollection));
			_alwaysFetchRuleCappings = info.GetBoolean("_alwaysFetchRuleCappings");
			_alreadyFetchedRuleCappings = info.GetBoolean("_alreadyFetchedRuleCappings");

			_serviceAllocations = (VarioSL.Entities.CollectionClasses.ServiceAllocationCollection)info.GetValue("_serviceAllocations", typeof(VarioSL.Entities.CollectionClasses.ServiceAllocationCollection));
			_alwaysFetchServiceAllocations = info.GetBoolean("_alwaysFetchServiceAllocations");
			_alreadyFetchedServiceAllocations = info.GetBoolean("_alreadyFetchedServiceAllocations");

			_shortDistances = (VarioSL.Entities.CollectionClasses.ShortDistanceCollection)info.GetValue("_shortDistances", typeof(VarioSL.Entities.CollectionClasses.ShortDistanceCollection));
			_alwaysFetchShortDistances = info.GetBoolean("_alwaysFetchShortDistances");
			_alreadyFetchedShortDistances = info.GetBoolean("_alreadyFetchedShortDistances");

			_specialReceipts = (VarioSL.Entities.CollectionClasses.SpecialReceiptCollection)info.GetValue("_specialReceipts", typeof(VarioSL.Entities.CollectionClasses.SpecialReceiptCollection));
			_alwaysFetchSpecialReceipts = info.GetBoolean("_alwaysFetchSpecialReceipts");
			_alreadyFetchedSpecialReceipts = info.GetBoolean("_alreadyFetchedSpecialReceipts");

			_systemTexts = (VarioSL.Entities.CollectionClasses.SystemTextCollection)info.GetValue("_systemTexts", typeof(VarioSL.Entities.CollectionClasses.SystemTextCollection));
			_alwaysFetchSystemTexts = info.GetBoolean("_alwaysFetchSystemTexts");
			_alreadyFetchedSystemTexts = info.GetBoolean("_alreadyFetchedSystemTexts");

			_childTariffs = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_childTariffs", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchChildTariffs = info.GetBoolean("_alwaysFetchChildTariffs");
			_alreadyFetchedChildTariffs = info.GetBoolean("_alreadyFetchedChildTariffs");

			_matrixParentTariff = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_matrixParentTariff", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchMatrixParentTariff = info.GetBoolean("_alwaysFetchMatrixParentTariff");
			_alreadyFetchedMatrixParentTariff = info.GetBoolean("_alreadyFetchedMatrixParentTariff");

			_tariffRelease = (VarioSL.Entities.CollectionClasses.TariffReleaseCollection)info.GetValue("_tariffRelease", typeof(VarioSL.Entities.CollectionClasses.TariffReleaseCollection));
			_alwaysFetchTariffRelease = info.GetBoolean("_alwaysFetchTariffRelease");
			_alreadyFetchedTariffRelease = info.GetBoolean("_alreadyFetchedTariffRelease");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketGroups = (VarioSL.Entities.CollectionClasses.TicketGroupCollection)info.GetValue("_ticketGroups", typeof(VarioSL.Entities.CollectionClasses.TicketGroupCollection));
			_alwaysFetchTicketGroups = info.GetBoolean("_alwaysFetchTicketGroups");
			_alreadyFetchedTicketGroups = info.GetBoolean("_alreadyFetchedTicketGroups");

			_translations = (VarioSL.Entities.CollectionClasses.TranslationCollection)info.GetValue("_translations", typeof(VarioSL.Entities.CollectionClasses.TranslationCollection));
			_alwaysFetchTranslations = info.GetBoolean("_alwaysFetchTranslations");
			_alreadyFetchedTranslations = info.GetBoolean("_alreadyFetchedTranslations");

			_userKeys = (VarioSL.Entities.CollectionClasses.UserKeyCollection)info.GetValue("_userKeys", typeof(VarioSL.Entities.CollectionClasses.UserKeyCollection));
			_alwaysFetchUserKeys = info.GetBoolean("_alwaysFetchUserKeys");
			_alreadyFetchedUserKeys = info.GetBoolean("_alreadyFetchedUserKeys");

			_vdvKeySet = (VarioSL.Entities.CollectionClasses.VdvKeySetCollection)info.GetValue("_vdvKeySet", typeof(VarioSL.Entities.CollectionClasses.VdvKeySetCollection));
			_alwaysFetchVdvKeySet = info.GetBoolean("_alwaysFetchVdvKeySet");
			_alreadyFetchedVdvKeySet = info.GetBoolean("_alreadyFetchedVdvKeySet");

			_vdvLayout = (VarioSL.Entities.CollectionClasses.VdvLayoutCollection)info.GetValue("_vdvLayout", typeof(VarioSL.Entities.CollectionClasses.VdvLayoutCollection));
			_alwaysFetchVdvLayout = info.GetBoolean("_alwaysFetchVdvLayout");
			_alreadyFetchedVdvLayout = info.GetBoolean("_alreadyFetchedVdvLayout");

			_vdvProduct = (VarioSL.Entities.CollectionClasses.VdvProductCollection)info.GetValue("_vdvProduct", typeof(VarioSL.Entities.CollectionClasses.VdvProductCollection));
			_alwaysFetchVdvProduct = info.GetBoolean("_alwaysFetchVdvProduct");
			_alreadyFetchedVdvProduct = info.GetBoolean("_alreadyFetchedVdvProduct");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_ownerClient = (ClientEntity)info.GetValue("_ownerClient", typeof(ClientEntity));
			if(_ownerClient!=null)
			{
				_ownerClient.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ownerClientReturnsNewIfNotFound = info.GetBoolean("_ownerClientReturnsNewIfNotFound");
			_alwaysFetchOwnerClient = info.GetBoolean("_alwaysFetchOwnerClient");
			_alreadyFetchedOwnerClient = info.GetBoolean("_alreadyFetchedOwnerClient");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_net = (NetEntity)info.GetValue("_net", typeof(NetEntity));
			if(_net!=null)
			{
				_net.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_netReturnsNewIfNotFound = info.GetBoolean("_netReturnsNewIfNotFound");
			_alwaysFetchNet = info.GetBoolean("_alwaysFetchNet");
			_alreadyFetchedNet = info.GetBoolean("_alreadyFetchedNet");

			_baseTariff = (TariffEntity)info.GetValue("_baseTariff", typeof(TariffEntity));
			if(_baseTariff!=null)
			{
				_baseTariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_baseTariffReturnsNewIfNotFound = info.GetBoolean("_baseTariffReturnsNewIfNotFound");
			_alwaysFetchBaseTariff = info.GetBoolean("_alwaysFetchBaseTariff");
			_alreadyFetchedBaseTariff = info.GetBoolean("_alreadyFetchedBaseTariff");

			_matrixTariff = (TariffEntity)info.GetValue("_matrixTariff", typeof(TariffEntity));
			if(_matrixTariff!=null)
			{
				_matrixTariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_matrixTariffReturnsNewIfNotFound = info.GetBoolean("_matrixTariffReturnsNewIfNotFound");
			_alwaysFetchMatrixTariff = info.GetBoolean("_alwaysFetchMatrixTariff");
			_alreadyFetchedMatrixTariff = info.GetBoolean("_alreadyFetchedMatrixTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TariffFieldIndex)fieldIndex)
			{
				case TariffFieldIndex.BaseTariffID:
					DesetupSyncBaseTariff(true, false);
					_alreadyFetchedBaseTariff = false;
					break;
				case TariffFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case TariffFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case TariffFieldIndex.MatrixTariffID:
					DesetupSyncMatrixTariff(true, false);
					_alreadyFetchedMatrixTariff = false;
					break;
				case TariffFieldIndex.NetID:
					DesetupSyncNet(true, false);
					_alreadyFetchedNet = false;
					break;
				case TariffFieldIndex.OwnerClientID:
					DesetupSyncOwnerClient(true, false);
					_alreadyFetchedOwnerClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaList = (_areaList.Count > 0);
			_alreadyFetchedAreaListElements = (_areaListElements.Count > 0);
			_alreadyFetchedAttributes = (_attributes.Count > 0);
			_alreadyFetchedBusinessRule = (_businessRule.Count > 0);
			_alreadyFetchedBusinessRuleCondition = (_businessRuleCondition.Count > 0);
			_alreadyFetchedBusinessRuleResult = (_businessRuleResult.Count > 0);
			_alreadyFetchedCalendars = (_calendars.Count > 0);
			_alreadyFetchedCardTickets = (_cardTickets.Count > 0);
			_alreadyFetchedChoices = (_choices.Count > 0);
			_alreadyFetchedClientAdaptedLayoutObjects = (_clientAdaptedLayoutObjects.Count > 0);
			_alreadyFetchedEtickets = (_etickets.Count > 0);
			_alreadyFetchedExternalCards = (_externalCards.Count > 0);
			_alreadyFetchedExternalEfforts = (_externalEfforts.Count > 0);
			_alreadyFetchedExternalPackets = (_externalPackets.Count > 0);
			_alreadyFetchedExternalPacketefforts = (_externalPacketefforts.Count > 0);
			_alreadyFetchedExternalTypes = (_externalTypes.Count > 0);
			_alreadyFetchedFareEvasionCategories = (_fareEvasionCategories.Count > 0);
			_alreadyFetchedFareEvasionReasons = (_fareEvasionReasons.Count > 0);
			_alreadyFetchedFareMatrices = (_fareMatrices.Count > 0);
			_alreadyFetchedFareStageLists = (_fareStageLists.Count > 0);
			_alreadyFetchedFareTables = (_fareTables.Count > 0);
			_alreadyFetchedGuiDefs = (_guiDefs.Count > 0);
			_alreadyFetchedKeyAttributeTransforms = (_keyAttributeTransforms.Count > 0);
			_alreadyFetchedLayouts = (_layouts.Count > 0);
			_alreadyFetchedLineGroups = (_lineGroups.Count > 0);
			_alreadyFetchedLogos = (_logos.Count > 0);
			_alreadyFetchedPageContentGroups = (_pageContentGroups.Count > 0);
			_alreadyFetchedParameterAttributeValues = (_parameterAttributeValues.Count > 0);
			_alreadyFetchedParameterFareStage = (_parameterFareStage.Count > 0);
			_alreadyFetchedParameterTariff = (_parameterTariff.Count > 0);
			_alreadyFetchedParameterTickets = (_parameterTickets.Count > 0);
			_alreadyFetchedPredefinedKeys = (_predefinedKeys.Count > 0);
			_alreadyFetchedPrimalKeys = (_primalKeys.Count > 0);
			_alreadyFetchedPrintTexts = (_printTexts.Count > 0);
			_alreadyFetchedRoutes = (_routes.Count > 0);
			_alreadyFetchedRouteNames = (_routeNames.Count > 0);
			_alreadyFetchedRuleCappings = (_ruleCappings.Count > 0);
			_alreadyFetchedServiceAllocations = (_serviceAllocations.Count > 0);
			_alreadyFetchedShortDistances = (_shortDistances.Count > 0);
			_alreadyFetchedSpecialReceipts = (_specialReceipts.Count > 0);
			_alreadyFetchedSystemTexts = (_systemTexts.Count > 0);
			_alreadyFetchedChildTariffs = (_childTariffs.Count > 0);
			_alreadyFetchedMatrixParentTariff = (_matrixParentTariff.Count > 0);
			_alreadyFetchedTariffRelease = (_tariffRelease.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketGroups = (_ticketGroups.Count > 0);
			_alreadyFetchedTranslations = (_translations.Count > 0);
			_alreadyFetchedUserKeys = (_userKeys.Count > 0);
			_alreadyFetchedVdvKeySet = (_vdvKeySet.Count > 0);
			_alreadyFetchedVdvLayout = (_vdvLayout.Count > 0);
			_alreadyFetchedVdvProduct = (_vdvProduct.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedOwnerClient = (_ownerClient != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedNet = (_net != null);
			_alreadyFetchedBaseTariff = (_baseTariff != null);
			_alreadyFetchedMatrixTariff = (_matrixTariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "OwnerClient":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "Net":
					toReturn.Add(Relations.NetEntityUsingNetID);
					break;
				case "BaseTariff":
					toReturn.Add(Relations.TariffEntityUsingTarifIDBaseTariffID);
					break;
				case "MatrixTariff":
					toReturn.Add(Relations.TariffEntityUsingTarifIDMatrixTariffID);
					break;
				case "AreaList":
					toReturn.Add(Relations.AreaListEntityUsingTariffID);
					break;
				case "AreaListElements":
					toReturn.Add(Relations.AreaListElementEntityUsingTariffID);
					break;
				case "Attributes":
					toReturn.Add(Relations.AttributeEntityUsingTarifID);
					break;
				case "BusinessRule":
					toReturn.Add(Relations.BusinessRuleEntityUsingTariffID);
					break;
				case "BusinessRuleCondition":
					toReturn.Add(Relations.BusinessRuleConditionEntityUsingTariffID);
					break;
				case "BusinessRuleResult":
					toReturn.Add(Relations.BusinessRuleResultEntityUsingTariffID);
					break;
				case "Calendars":
					toReturn.Add(Relations.CalendarEntityUsingTariffId);
					break;
				case "CardTickets":
					toReturn.Add(Relations.CardTicketEntityUsingTariffID);
					break;
				case "Choices":
					toReturn.Add(Relations.ChoiceEntityUsingTariffID);
					break;
				case "ClientAdaptedLayoutObjects":
					toReturn.Add(Relations.ClientAdaptedLayoutObjectEntityUsingTariffID);
					break;
				case "Etickets":
					toReturn.Add(Relations.EticketEntityUsingTariffID);
					break;
				case "ExternalCards":
					toReturn.Add(Relations.ExternalCardEntityUsingTariffId);
					break;
				case "ExternalEfforts":
					toReturn.Add(Relations.ExternalEffortEntityUsingTariffID);
					break;
				case "ExternalPackets":
					toReturn.Add(Relations.ExternalPacketEntityUsingTariffID);
					break;
				case "ExternalPacketefforts":
					toReturn.Add(Relations.ExternalPacketEffortEntityUsingTariffID);
					break;
				case "ExternalTypes":
					toReturn.Add(Relations.ExternalTypeEntityUsingTariffID);
					break;
				case "FareEvasionCategories":
					toReturn.Add(Relations.FareEvasionCategoryEntityUsingTariffID);
					break;
				case "FareEvasionReasons":
					toReturn.Add(Relations.FareEvasionReasonEntityUsingTariffID);
					break;
				case "FareMatrices":
					toReturn.Add(Relations.FareMatrixEntityUsingTariffID);
					break;
				case "FareStageLists":
					toReturn.Add(Relations.FareStageListEntityUsingTariffID);
					break;
				case "FareTables":
					toReturn.Add(Relations.FareTableEntityUsingTariffID);
					break;
				case "GuiDefs":
					toReturn.Add(Relations.GuiDefEntityUsingTariffID);
					break;
				case "KeyAttributeTransforms":
					toReturn.Add(Relations.KeyAttributeTransfromEntityUsingTariffID);
					break;
				case "Layouts":
					toReturn.Add(Relations.LayoutEntityUsingTariffID);
					break;
				case "LineGroups":
					toReturn.Add(Relations.LineGroupEntityUsingTariffID);
					break;
				case "Logos":
					toReturn.Add(Relations.LogoEntityUsingTariffId);
					break;
				case "PageContentGroups":
					toReturn.Add(Relations.PageContentGroupEntityUsingTariffID);
					break;
				case "ParameterAttributeValues":
					toReturn.Add(Relations.ParameterAttributeValueEntityUsingTariffID);
					break;
				case "ParameterFareStage":
					toReturn.Add(Relations.ParameterFareStageEntityUsingTariffID);
					break;
				case "ParameterTariff":
					toReturn.Add(Relations.ParameterTariffEntityUsingTariffID);
					break;
				case "ParameterTickets":
					toReturn.Add(Relations.ParameterTicketEntityUsingTariffID);
					break;
				case "PredefinedKeys":
					toReturn.Add(Relations.PredefinedKeyEntityUsingTariffID);
					break;
				case "PrimalKeys":
					toReturn.Add(Relations.PrimalKeyEntityUsingTariffID);
					break;
				case "PrintTexts":
					toReturn.Add(Relations.PrintTextEntityUsingTariffId);
					break;
				case "Routes":
					toReturn.Add(Relations.RouteEntityUsingTariffID);
					break;
				case "RouteNames":
					toReturn.Add(Relations.RouteNameEntityUsingTariffId);
					break;
				case "RuleCappings":
					toReturn.Add(Relations.RuleCappingEntityUsingTariffID);
					break;
				case "ServiceAllocations":
					toReturn.Add(Relations.ServiceAllocationEntityUsingTariffId);
					break;
				case "ShortDistances":
					toReturn.Add(Relations.ShortDistanceEntityUsingTariffID);
					break;
				case "SpecialReceipts":
					toReturn.Add(Relations.SpecialReceiptEntityUsingTariffID);
					break;
				case "SystemTexts":
					toReturn.Add(Relations.SystemTextEntityUsingTariffID);
					break;
				case "ChildTariffs":
					toReturn.Add(Relations.TariffEntityUsingBaseTariffID);
					break;
				case "MatrixParentTariff":
					toReturn.Add(Relations.TariffEntityUsingMatrixTariffID);
					break;
				case "TariffRelease":
					toReturn.Add(Relations.TariffReleaseEntityUsingTariffID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingTarifID);
					break;
				case "TicketGroups":
					toReturn.Add(Relations.TicketGroupEntityUsingTariffID);
					break;
				case "Translations":
					toReturn.Add(Relations.TranslationEntityUsingTariffID);
					break;
				case "UserKeys":
					toReturn.Add(Relations.UserKeyEntityUsingTariffID);
					break;
				case "VdvKeySet":
					toReturn.Add(Relations.VdvKeySetEntityUsingTariffID);
					break;
				case "VdvLayout":
					toReturn.Add(Relations.VdvLayoutEntityUsingTariffID);
					break;
				case "VdvProduct":
					toReturn.Add(Relations.VdvProductEntityUsingTariffID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaList", (!this.MarkedForDeletion?_areaList:null));
			info.AddValue("_alwaysFetchAreaList", _alwaysFetchAreaList);
			info.AddValue("_alreadyFetchedAreaList", _alreadyFetchedAreaList);
			info.AddValue("_areaListElements", (!this.MarkedForDeletion?_areaListElements:null));
			info.AddValue("_alwaysFetchAreaListElements", _alwaysFetchAreaListElements);
			info.AddValue("_alreadyFetchedAreaListElements", _alreadyFetchedAreaListElements);
			info.AddValue("_attributes", (!this.MarkedForDeletion?_attributes:null));
			info.AddValue("_alwaysFetchAttributes", _alwaysFetchAttributes);
			info.AddValue("_alreadyFetchedAttributes", _alreadyFetchedAttributes);
			info.AddValue("_businessRule", (!this.MarkedForDeletion?_businessRule:null));
			info.AddValue("_alwaysFetchBusinessRule", _alwaysFetchBusinessRule);
			info.AddValue("_alreadyFetchedBusinessRule", _alreadyFetchedBusinessRule);
			info.AddValue("_businessRuleCondition", (!this.MarkedForDeletion?_businessRuleCondition:null));
			info.AddValue("_alwaysFetchBusinessRuleCondition", _alwaysFetchBusinessRuleCondition);
			info.AddValue("_alreadyFetchedBusinessRuleCondition", _alreadyFetchedBusinessRuleCondition);
			info.AddValue("_businessRuleResult", (!this.MarkedForDeletion?_businessRuleResult:null));
			info.AddValue("_alwaysFetchBusinessRuleResult", _alwaysFetchBusinessRuleResult);
			info.AddValue("_alreadyFetchedBusinessRuleResult", _alreadyFetchedBusinessRuleResult);
			info.AddValue("_calendars", (!this.MarkedForDeletion?_calendars:null));
			info.AddValue("_alwaysFetchCalendars", _alwaysFetchCalendars);
			info.AddValue("_alreadyFetchedCalendars", _alreadyFetchedCalendars);
			info.AddValue("_cardTickets", (!this.MarkedForDeletion?_cardTickets:null));
			info.AddValue("_alwaysFetchCardTickets", _alwaysFetchCardTickets);
			info.AddValue("_alreadyFetchedCardTickets", _alreadyFetchedCardTickets);
			info.AddValue("_choices", (!this.MarkedForDeletion?_choices:null));
			info.AddValue("_alwaysFetchChoices", _alwaysFetchChoices);
			info.AddValue("_alreadyFetchedChoices", _alreadyFetchedChoices);
			info.AddValue("_clientAdaptedLayoutObjects", (!this.MarkedForDeletion?_clientAdaptedLayoutObjects:null));
			info.AddValue("_alwaysFetchClientAdaptedLayoutObjects", _alwaysFetchClientAdaptedLayoutObjects);
			info.AddValue("_alreadyFetchedClientAdaptedLayoutObjects", _alreadyFetchedClientAdaptedLayoutObjects);
			info.AddValue("_etickets", (!this.MarkedForDeletion?_etickets:null));
			info.AddValue("_alwaysFetchEtickets", _alwaysFetchEtickets);
			info.AddValue("_alreadyFetchedEtickets", _alreadyFetchedEtickets);
			info.AddValue("_externalCards", (!this.MarkedForDeletion?_externalCards:null));
			info.AddValue("_alwaysFetchExternalCards", _alwaysFetchExternalCards);
			info.AddValue("_alreadyFetchedExternalCards", _alreadyFetchedExternalCards);
			info.AddValue("_externalEfforts", (!this.MarkedForDeletion?_externalEfforts:null));
			info.AddValue("_alwaysFetchExternalEfforts", _alwaysFetchExternalEfforts);
			info.AddValue("_alreadyFetchedExternalEfforts", _alreadyFetchedExternalEfforts);
			info.AddValue("_externalPackets", (!this.MarkedForDeletion?_externalPackets:null));
			info.AddValue("_alwaysFetchExternalPackets", _alwaysFetchExternalPackets);
			info.AddValue("_alreadyFetchedExternalPackets", _alreadyFetchedExternalPackets);
			info.AddValue("_externalPacketefforts", (!this.MarkedForDeletion?_externalPacketefforts:null));
			info.AddValue("_alwaysFetchExternalPacketefforts", _alwaysFetchExternalPacketefforts);
			info.AddValue("_alreadyFetchedExternalPacketefforts", _alreadyFetchedExternalPacketefforts);
			info.AddValue("_externalTypes", (!this.MarkedForDeletion?_externalTypes:null));
			info.AddValue("_alwaysFetchExternalTypes", _alwaysFetchExternalTypes);
			info.AddValue("_alreadyFetchedExternalTypes", _alreadyFetchedExternalTypes);
			info.AddValue("_fareEvasionCategories", (!this.MarkedForDeletion?_fareEvasionCategories:null));
			info.AddValue("_alwaysFetchFareEvasionCategories", _alwaysFetchFareEvasionCategories);
			info.AddValue("_alreadyFetchedFareEvasionCategories", _alreadyFetchedFareEvasionCategories);
			info.AddValue("_fareEvasionReasons", (!this.MarkedForDeletion?_fareEvasionReasons:null));
			info.AddValue("_alwaysFetchFareEvasionReasons", _alwaysFetchFareEvasionReasons);
			info.AddValue("_alreadyFetchedFareEvasionReasons", _alreadyFetchedFareEvasionReasons);
			info.AddValue("_fareMatrices", (!this.MarkedForDeletion?_fareMatrices:null));
			info.AddValue("_alwaysFetchFareMatrices", _alwaysFetchFareMatrices);
			info.AddValue("_alreadyFetchedFareMatrices", _alreadyFetchedFareMatrices);
			info.AddValue("_fareStageLists", (!this.MarkedForDeletion?_fareStageLists:null));
			info.AddValue("_alwaysFetchFareStageLists", _alwaysFetchFareStageLists);
			info.AddValue("_alreadyFetchedFareStageLists", _alreadyFetchedFareStageLists);
			info.AddValue("_fareTables", (!this.MarkedForDeletion?_fareTables:null));
			info.AddValue("_alwaysFetchFareTables", _alwaysFetchFareTables);
			info.AddValue("_alreadyFetchedFareTables", _alreadyFetchedFareTables);
			info.AddValue("_guiDefs", (!this.MarkedForDeletion?_guiDefs:null));
			info.AddValue("_alwaysFetchGuiDefs", _alwaysFetchGuiDefs);
			info.AddValue("_alreadyFetchedGuiDefs", _alreadyFetchedGuiDefs);
			info.AddValue("_keyAttributeTransforms", (!this.MarkedForDeletion?_keyAttributeTransforms:null));
			info.AddValue("_alwaysFetchKeyAttributeTransforms", _alwaysFetchKeyAttributeTransforms);
			info.AddValue("_alreadyFetchedKeyAttributeTransforms", _alreadyFetchedKeyAttributeTransforms);
			info.AddValue("_layouts", (!this.MarkedForDeletion?_layouts:null));
			info.AddValue("_alwaysFetchLayouts", _alwaysFetchLayouts);
			info.AddValue("_alreadyFetchedLayouts", _alreadyFetchedLayouts);
			info.AddValue("_lineGroups", (!this.MarkedForDeletion?_lineGroups:null));
			info.AddValue("_alwaysFetchLineGroups", _alwaysFetchLineGroups);
			info.AddValue("_alreadyFetchedLineGroups", _alreadyFetchedLineGroups);
			info.AddValue("_logos", (!this.MarkedForDeletion?_logos:null));
			info.AddValue("_alwaysFetchLogos", _alwaysFetchLogos);
			info.AddValue("_alreadyFetchedLogos", _alreadyFetchedLogos);
			info.AddValue("_pageContentGroups", (!this.MarkedForDeletion?_pageContentGroups:null));
			info.AddValue("_alwaysFetchPageContentGroups", _alwaysFetchPageContentGroups);
			info.AddValue("_alreadyFetchedPageContentGroups", _alreadyFetchedPageContentGroups);
			info.AddValue("_parameterAttributeValues", (!this.MarkedForDeletion?_parameterAttributeValues:null));
			info.AddValue("_alwaysFetchParameterAttributeValues", _alwaysFetchParameterAttributeValues);
			info.AddValue("_alreadyFetchedParameterAttributeValues", _alreadyFetchedParameterAttributeValues);
			info.AddValue("_parameterFareStage", (!this.MarkedForDeletion?_parameterFareStage:null));
			info.AddValue("_alwaysFetchParameterFareStage", _alwaysFetchParameterFareStage);
			info.AddValue("_alreadyFetchedParameterFareStage", _alreadyFetchedParameterFareStage);
			info.AddValue("_parameterTariff", (!this.MarkedForDeletion?_parameterTariff:null));
			info.AddValue("_alwaysFetchParameterTariff", _alwaysFetchParameterTariff);
			info.AddValue("_alreadyFetchedParameterTariff", _alreadyFetchedParameterTariff);
			info.AddValue("_parameterTickets", (!this.MarkedForDeletion?_parameterTickets:null));
			info.AddValue("_alwaysFetchParameterTickets", _alwaysFetchParameterTickets);
			info.AddValue("_alreadyFetchedParameterTickets", _alreadyFetchedParameterTickets);
			info.AddValue("_predefinedKeys", (!this.MarkedForDeletion?_predefinedKeys:null));
			info.AddValue("_alwaysFetchPredefinedKeys", _alwaysFetchPredefinedKeys);
			info.AddValue("_alreadyFetchedPredefinedKeys", _alreadyFetchedPredefinedKeys);
			info.AddValue("_primalKeys", (!this.MarkedForDeletion?_primalKeys:null));
			info.AddValue("_alwaysFetchPrimalKeys", _alwaysFetchPrimalKeys);
			info.AddValue("_alreadyFetchedPrimalKeys", _alreadyFetchedPrimalKeys);
			info.AddValue("_printTexts", (!this.MarkedForDeletion?_printTexts:null));
			info.AddValue("_alwaysFetchPrintTexts", _alwaysFetchPrintTexts);
			info.AddValue("_alreadyFetchedPrintTexts", _alreadyFetchedPrintTexts);
			info.AddValue("_routes", (!this.MarkedForDeletion?_routes:null));
			info.AddValue("_alwaysFetchRoutes", _alwaysFetchRoutes);
			info.AddValue("_alreadyFetchedRoutes", _alreadyFetchedRoutes);
			info.AddValue("_routeNames", (!this.MarkedForDeletion?_routeNames:null));
			info.AddValue("_alwaysFetchRouteNames", _alwaysFetchRouteNames);
			info.AddValue("_alreadyFetchedRouteNames", _alreadyFetchedRouteNames);
			info.AddValue("_ruleCappings", (!this.MarkedForDeletion?_ruleCappings:null));
			info.AddValue("_alwaysFetchRuleCappings", _alwaysFetchRuleCappings);
			info.AddValue("_alreadyFetchedRuleCappings", _alreadyFetchedRuleCappings);
			info.AddValue("_serviceAllocations", (!this.MarkedForDeletion?_serviceAllocations:null));
			info.AddValue("_alwaysFetchServiceAllocations", _alwaysFetchServiceAllocations);
			info.AddValue("_alreadyFetchedServiceAllocations", _alreadyFetchedServiceAllocations);
			info.AddValue("_shortDistances", (!this.MarkedForDeletion?_shortDistances:null));
			info.AddValue("_alwaysFetchShortDistances", _alwaysFetchShortDistances);
			info.AddValue("_alreadyFetchedShortDistances", _alreadyFetchedShortDistances);
			info.AddValue("_specialReceipts", (!this.MarkedForDeletion?_specialReceipts:null));
			info.AddValue("_alwaysFetchSpecialReceipts", _alwaysFetchSpecialReceipts);
			info.AddValue("_alreadyFetchedSpecialReceipts", _alreadyFetchedSpecialReceipts);
			info.AddValue("_systemTexts", (!this.MarkedForDeletion?_systemTexts:null));
			info.AddValue("_alwaysFetchSystemTexts", _alwaysFetchSystemTexts);
			info.AddValue("_alreadyFetchedSystemTexts", _alreadyFetchedSystemTexts);
			info.AddValue("_childTariffs", (!this.MarkedForDeletion?_childTariffs:null));
			info.AddValue("_alwaysFetchChildTariffs", _alwaysFetchChildTariffs);
			info.AddValue("_alreadyFetchedChildTariffs", _alreadyFetchedChildTariffs);
			info.AddValue("_matrixParentTariff", (!this.MarkedForDeletion?_matrixParentTariff:null));
			info.AddValue("_alwaysFetchMatrixParentTariff", _alwaysFetchMatrixParentTariff);
			info.AddValue("_alreadyFetchedMatrixParentTariff", _alreadyFetchedMatrixParentTariff);
			info.AddValue("_tariffRelease", (!this.MarkedForDeletion?_tariffRelease:null));
			info.AddValue("_alwaysFetchTariffRelease", _alwaysFetchTariffRelease);
			info.AddValue("_alreadyFetchedTariffRelease", _alreadyFetchedTariffRelease);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketGroups", (!this.MarkedForDeletion?_ticketGroups:null));
			info.AddValue("_alwaysFetchTicketGroups", _alwaysFetchTicketGroups);
			info.AddValue("_alreadyFetchedTicketGroups", _alreadyFetchedTicketGroups);
			info.AddValue("_translations", (!this.MarkedForDeletion?_translations:null));
			info.AddValue("_alwaysFetchTranslations", _alwaysFetchTranslations);
			info.AddValue("_alreadyFetchedTranslations", _alreadyFetchedTranslations);
			info.AddValue("_userKeys", (!this.MarkedForDeletion?_userKeys:null));
			info.AddValue("_alwaysFetchUserKeys", _alwaysFetchUserKeys);
			info.AddValue("_alreadyFetchedUserKeys", _alreadyFetchedUserKeys);
			info.AddValue("_vdvKeySet", (!this.MarkedForDeletion?_vdvKeySet:null));
			info.AddValue("_alwaysFetchVdvKeySet", _alwaysFetchVdvKeySet);
			info.AddValue("_alreadyFetchedVdvKeySet", _alreadyFetchedVdvKeySet);
			info.AddValue("_vdvLayout", (!this.MarkedForDeletion?_vdvLayout:null));
			info.AddValue("_alwaysFetchVdvLayout", _alwaysFetchVdvLayout);
			info.AddValue("_alreadyFetchedVdvLayout", _alreadyFetchedVdvLayout);
			info.AddValue("_vdvProduct", (!this.MarkedForDeletion?_vdvProduct:null));
			info.AddValue("_alwaysFetchVdvProduct", _alwaysFetchVdvProduct);
			info.AddValue("_alreadyFetchedVdvProduct", _alreadyFetchedVdvProduct);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_ownerClient", (!this.MarkedForDeletion?_ownerClient:null));
			info.AddValue("_ownerClientReturnsNewIfNotFound", _ownerClientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOwnerClient", _alwaysFetchOwnerClient);
			info.AddValue("_alreadyFetchedOwnerClient", _alreadyFetchedOwnerClient);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_net", (!this.MarkedForDeletion?_net:null));
			info.AddValue("_netReturnsNewIfNotFound", _netReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchNet", _alwaysFetchNet);
			info.AddValue("_alreadyFetchedNet", _alreadyFetchedNet);
			info.AddValue("_baseTariff", (!this.MarkedForDeletion?_baseTariff:null));
			info.AddValue("_baseTariffReturnsNewIfNotFound", _baseTariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBaseTariff", _alwaysFetchBaseTariff);
			info.AddValue("_alreadyFetchedBaseTariff", _alreadyFetchedBaseTariff);
			info.AddValue("_matrixTariff", (!this.MarkedForDeletion?_matrixTariff:null));
			info.AddValue("_matrixTariffReturnsNewIfNotFound", _matrixTariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMatrixTariff", _alwaysFetchMatrixTariff);
			info.AddValue("_alreadyFetchedMatrixTariff", _alreadyFetchedMatrixTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "OwnerClient":
					_alreadyFetchedOwnerClient = true;
					this.OwnerClient = (ClientEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Net":
					_alreadyFetchedNet = true;
					this.Net = (NetEntity)entity;
					break;
				case "BaseTariff":
					_alreadyFetchedBaseTariff = true;
					this.BaseTariff = (TariffEntity)entity;
					break;
				case "MatrixTariff":
					_alreadyFetchedMatrixTariff = true;
					this.MatrixTariff = (TariffEntity)entity;
					break;
				case "AreaList":
					_alreadyFetchedAreaList = true;
					if(entity!=null)
					{
						this.AreaList.Add((AreaListEntity)entity);
					}
					break;
				case "AreaListElements":
					_alreadyFetchedAreaListElements = true;
					if(entity!=null)
					{
						this.AreaListElements.Add((AreaListElementEntity)entity);
					}
					break;
				case "Attributes":
					_alreadyFetchedAttributes = true;
					if(entity!=null)
					{
						this.Attributes.Add((AttributeEntity)entity);
					}
					break;
				case "BusinessRule":
					_alreadyFetchedBusinessRule = true;
					if(entity!=null)
					{
						this.BusinessRule.Add((BusinessRuleEntity)entity);
					}
					break;
				case "BusinessRuleCondition":
					_alreadyFetchedBusinessRuleCondition = true;
					if(entity!=null)
					{
						this.BusinessRuleCondition.Add((BusinessRuleConditionEntity)entity);
					}
					break;
				case "BusinessRuleResult":
					_alreadyFetchedBusinessRuleResult = true;
					if(entity!=null)
					{
						this.BusinessRuleResult.Add((BusinessRuleResultEntity)entity);
					}
					break;
				case "Calendars":
					_alreadyFetchedCalendars = true;
					if(entity!=null)
					{
						this.Calendars.Add((CalendarEntity)entity);
					}
					break;
				case "CardTickets":
					_alreadyFetchedCardTickets = true;
					if(entity!=null)
					{
						this.CardTickets.Add((CardTicketEntity)entity);
					}
					break;
				case "Choices":
					_alreadyFetchedChoices = true;
					if(entity!=null)
					{
						this.Choices.Add((ChoiceEntity)entity);
					}
					break;
				case "ClientAdaptedLayoutObjects":
					_alreadyFetchedClientAdaptedLayoutObjects = true;
					if(entity!=null)
					{
						this.ClientAdaptedLayoutObjects.Add((ClientAdaptedLayoutObjectEntity)entity);
					}
					break;
				case "Etickets":
					_alreadyFetchedEtickets = true;
					if(entity!=null)
					{
						this.Etickets.Add((EticketEntity)entity);
					}
					break;
				case "ExternalCards":
					_alreadyFetchedExternalCards = true;
					if(entity!=null)
					{
						this.ExternalCards.Add((ExternalCardEntity)entity);
					}
					break;
				case "ExternalEfforts":
					_alreadyFetchedExternalEfforts = true;
					if(entity!=null)
					{
						this.ExternalEfforts.Add((ExternalEffortEntity)entity);
					}
					break;
				case "ExternalPackets":
					_alreadyFetchedExternalPackets = true;
					if(entity!=null)
					{
						this.ExternalPackets.Add((ExternalPacketEntity)entity);
					}
					break;
				case "ExternalPacketefforts":
					_alreadyFetchedExternalPacketefforts = true;
					if(entity!=null)
					{
						this.ExternalPacketefforts.Add((ExternalPacketEffortEntity)entity);
					}
					break;
				case "ExternalTypes":
					_alreadyFetchedExternalTypes = true;
					if(entity!=null)
					{
						this.ExternalTypes.Add((ExternalTypeEntity)entity);
					}
					break;
				case "FareEvasionCategories":
					_alreadyFetchedFareEvasionCategories = true;
					if(entity!=null)
					{
						this.FareEvasionCategories.Add((FareEvasionCategoryEntity)entity);
					}
					break;
				case "FareEvasionReasons":
					_alreadyFetchedFareEvasionReasons = true;
					if(entity!=null)
					{
						this.FareEvasionReasons.Add((FareEvasionReasonEntity)entity);
					}
					break;
				case "FareMatrices":
					_alreadyFetchedFareMatrices = true;
					if(entity!=null)
					{
						this.FareMatrices.Add((FareMatrixEntity)entity);
					}
					break;
				case "FareStageLists":
					_alreadyFetchedFareStageLists = true;
					if(entity!=null)
					{
						this.FareStageLists.Add((FareStageListEntity)entity);
					}
					break;
				case "FareTables":
					_alreadyFetchedFareTables = true;
					if(entity!=null)
					{
						this.FareTables.Add((FareTableEntity)entity);
					}
					break;
				case "GuiDefs":
					_alreadyFetchedGuiDefs = true;
					if(entity!=null)
					{
						this.GuiDefs.Add((GuiDefEntity)entity);
					}
					break;
				case "KeyAttributeTransforms":
					_alreadyFetchedKeyAttributeTransforms = true;
					if(entity!=null)
					{
						this.KeyAttributeTransforms.Add((KeyAttributeTransfromEntity)entity);
					}
					break;
				case "Layouts":
					_alreadyFetchedLayouts = true;
					if(entity!=null)
					{
						this.Layouts.Add((LayoutEntity)entity);
					}
					break;
				case "LineGroups":
					_alreadyFetchedLineGroups = true;
					if(entity!=null)
					{
						this.LineGroups.Add((LineGroupEntity)entity);
					}
					break;
				case "Logos":
					_alreadyFetchedLogos = true;
					if(entity!=null)
					{
						this.Logos.Add((LogoEntity)entity);
					}
					break;
				case "PageContentGroups":
					_alreadyFetchedPageContentGroups = true;
					if(entity!=null)
					{
						this.PageContentGroups.Add((PageContentGroupEntity)entity);
					}
					break;
				case "ParameterAttributeValues":
					_alreadyFetchedParameterAttributeValues = true;
					if(entity!=null)
					{
						this.ParameterAttributeValues.Add((ParameterAttributeValueEntity)entity);
					}
					break;
				case "ParameterFareStage":
					_alreadyFetchedParameterFareStage = true;
					if(entity!=null)
					{
						this.ParameterFareStage.Add((ParameterFareStageEntity)entity);
					}
					break;
				case "ParameterTariff":
					_alreadyFetchedParameterTariff = true;
					if(entity!=null)
					{
						this.ParameterTariff.Add((ParameterTariffEntity)entity);
					}
					break;
				case "ParameterTickets":
					_alreadyFetchedParameterTickets = true;
					if(entity!=null)
					{
						this.ParameterTickets.Add((ParameterTicketEntity)entity);
					}
					break;
				case "PredefinedKeys":
					_alreadyFetchedPredefinedKeys = true;
					if(entity!=null)
					{
						this.PredefinedKeys.Add((PredefinedKeyEntity)entity);
					}
					break;
				case "PrimalKeys":
					_alreadyFetchedPrimalKeys = true;
					if(entity!=null)
					{
						this.PrimalKeys.Add((PrimalKeyEntity)entity);
					}
					break;
				case "PrintTexts":
					_alreadyFetchedPrintTexts = true;
					if(entity!=null)
					{
						this.PrintTexts.Add((PrintTextEntity)entity);
					}
					break;
				case "Routes":
					_alreadyFetchedRoutes = true;
					if(entity!=null)
					{
						this.Routes.Add((RouteEntity)entity);
					}
					break;
				case "RouteNames":
					_alreadyFetchedRouteNames = true;
					if(entity!=null)
					{
						this.RouteNames.Add((RouteNameEntity)entity);
					}
					break;
				case "RuleCappings":
					_alreadyFetchedRuleCappings = true;
					if(entity!=null)
					{
						this.RuleCappings.Add((RuleCappingEntity)entity);
					}
					break;
				case "ServiceAllocations":
					_alreadyFetchedServiceAllocations = true;
					if(entity!=null)
					{
						this.ServiceAllocations.Add((ServiceAllocationEntity)entity);
					}
					break;
				case "ShortDistances":
					_alreadyFetchedShortDistances = true;
					if(entity!=null)
					{
						this.ShortDistances.Add((ShortDistanceEntity)entity);
					}
					break;
				case "SpecialReceipts":
					_alreadyFetchedSpecialReceipts = true;
					if(entity!=null)
					{
						this.SpecialReceipts.Add((SpecialReceiptEntity)entity);
					}
					break;
				case "SystemTexts":
					_alreadyFetchedSystemTexts = true;
					if(entity!=null)
					{
						this.SystemTexts.Add((SystemTextEntity)entity);
					}
					break;
				case "ChildTariffs":
					_alreadyFetchedChildTariffs = true;
					if(entity!=null)
					{
						this.ChildTariffs.Add((TariffEntity)entity);
					}
					break;
				case "MatrixParentTariff":
					_alreadyFetchedMatrixParentTariff = true;
					if(entity!=null)
					{
						this.MatrixParentTariff.Add((TariffEntity)entity);
					}
					break;
				case "TariffRelease":
					_alreadyFetchedTariffRelease = true;
					if(entity!=null)
					{
						this.TariffRelease.Add((TariffReleaseEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketGroups":
					_alreadyFetchedTicketGroups = true;
					if(entity!=null)
					{
						this.TicketGroups.Add((TicketGroupEntity)entity);
					}
					break;
				case "Translations":
					_alreadyFetchedTranslations = true;
					if(entity!=null)
					{
						this.Translations.Add((TranslationEntity)entity);
					}
					break;
				case "UserKeys":
					_alreadyFetchedUserKeys = true;
					if(entity!=null)
					{
						this.UserKeys.Add((UserKeyEntity)entity);
					}
					break;
				case "VdvKeySet":
					_alreadyFetchedVdvKeySet = true;
					if(entity!=null)
					{
						this.VdvKeySet.Add((VdvKeySetEntity)entity);
					}
					break;
				case "VdvLayout":
					_alreadyFetchedVdvLayout = true;
					if(entity!=null)
					{
						this.VdvLayout.Add((VdvLayoutEntity)entity);
					}
					break;
				case "VdvProduct":
					_alreadyFetchedVdvProduct = true;
					if(entity!=null)
					{
						this.VdvProduct.Add((VdvProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "OwnerClient":
					SetupSyncOwnerClient(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Net":
					SetupSyncNet(relatedEntity);
					break;
				case "BaseTariff":
					SetupSyncBaseTariff(relatedEntity);
					break;
				case "MatrixTariff":
					SetupSyncMatrixTariff(relatedEntity);
					break;
				case "AreaList":
					_areaList.Add((AreaListEntity)relatedEntity);
					break;
				case "AreaListElements":
					_areaListElements.Add((AreaListElementEntity)relatedEntity);
					break;
				case "Attributes":
					_attributes.Add((AttributeEntity)relatedEntity);
					break;
				case "BusinessRule":
					_businessRule.Add((BusinessRuleEntity)relatedEntity);
					break;
				case "BusinessRuleCondition":
					_businessRuleCondition.Add((BusinessRuleConditionEntity)relatedEntity);
					break;
				case "BusinessRuleResult":
					_businessRuleResult.Add((BusinessRuleResultEntity)relatedEntity);
					break;
				case "Calendars":
					_calendars.Add((CalendarEntity)relatedEntity);
					break;
				case "CardTickets":
					_cardTickets.Add((CardTicketEntity)relatedEntity);
					break;
				case "Choices":
					_choices.Add((ChoiceEntity)relatedEntity);
					break;
				case "ClientAdaptedLayoutObjects":
					_clientAdaptedLayoutObjects.Add((ClientAdaptedLayoutObjectEntity)relatedEntity);
					break;
				case "Etickets":
					_etickets.Add((EticketEntity)relatedEntity);
					break;
				case "ExternalCards":
					_externalCards.Add((ExternalCardEntity)relatedEntity);
					break;
				case "ExternalEfforts":
					_externalEfforts.Add((ExternalEffortEntity)relatedEntity);
					break;
				case "ExternalPackets":
					_externalPackets.Add((ExternalPacketEntity)relatedEntity);
					break;
				case "ExternalPacketefforts":
					_externalPacketefforts.Add((ExternalPacketEffortEntity)relatedEntity);
					break;
				case "ExternalTypes":
					_externalTypes.Add((ExternalTypeEntity)relatedEntity);
					break;
				case "FareEvasionCategories":
					_fareEvasionCategories.Add((FareEvasionCategoryEntity)relatedEntity);
					break;
				case "FareEvasionReasons":
					_fareEvasionReasons.Add((FareEvasionReasonEntity)relatedEntity);
					break;
				case "FareMatrices":
					_fareMatrices.Add((FareMatrixEntity)relatedEntity);
					break;
				case "FareStageLists":
					_fareStageLists.Add((FareStageListEntity)relatedEntity);
					break;
				case "FareTables":
					_fareTables.Add((FareTableEntity)relatedEntity);
					break;
				case "GuiDefs":
					_guiDefs.Add((GuiDefEntity)relatedEntity);
					break;
				case "KeyAttributeTransforms":
					_keyAttributeTransforms.Add((KeyAttributeTransfromEntity)relatedEntity);
					break;
				case "Layouts":
					_layouts.Add((LayoutEntity)relatedEntity);
					break;
				case "LineGroups":
					_lineGroups.Add((LineGroupEntity)relatedEntity);
					break;
				case "Logos":
					_logos.Add((LogoEntity)relatedEntity);
					break;
				case "PageContentGroups":
					_pageContentGroups.Add((PageContentGroupEntity)relatedEntity);
					break;
				case "ParameterAttributeValues":
					_parameterAttributeValues.Add((ParameterAttributeValueEntity)relatedEntity);
					break;
				case "ParameterFareStage":
					_parameterFareStage.Add((ParameterFareStageEntity)relatedEntity);
					break;
				case "ParameterTariff":
					_parameterTariff.Add((ParameterTariffEntity)relatedEntity);
					break;
				case "ParameterTickets":
					_parameterTickets.Add((ParameterTicketEntity)relatedEntity);
					break;
				case "PredefinedKeys":
					_predefinedKeys.Add((PredefinedKeyEntity)relatedEntity);
					break;
				case "PrimalKeys":
					_primalKeys.Add((PrimalKeyEntity)relatedEntity);
					break;
				case "PrintTexts":
					_printTexts.Add((PrintTextEntity)relatedEntity);
					break;
				case "Routes":
					_routes.Add((RouteEntity)relatedEntity);
					break;
				case "RouteNames":
					_routeNames.Add((RouteNameEntity)relatedEntity);
					break;
				case "RuleCappings":
					_ruleCappings.Add((RuleCappingEntity)relatedEntity);
					break;
				case "ServiceAllocations":
					_serviceAllocations.Add((ServiceAllocationEntity)relatedEntity);
					break;
				case "ShortDistances":
					_shortDistances.Add((ShortDistanceEntity)relatedEntity);
					break;
				case "SpecialReceipts":
					_specialReceipts.Add((SpecialReceiptEntity)relatedEntity);
					break;
				case "SystemTexts":
					_systemTexts.Add((SystemTextEntity)relatedEntity);
					break;
				case "ChildTariffs":
					_childTariffs.Add((TariffEntity)relatedEntity);
					break;
				case "MatrixParentTariff":
					_matrixParentTariff.Add((TariffEntity)relatedEntity);
					break;
				case "TariffRelease":
					_tariffRelease.Add((TariffReleaseEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketGroups":
					_ticketGroups.Add((TicketGroupEntity)relatedEntity);
					break;
				case "Translations":
					_translations.Add((TranslationEntity)relatedEntity);
					break;
				case "UserKeys":
					_userKeys.Add((UserKeyEntity)relatedEntity);
					break;
				case "VdvKeySet":
					_vdvKeySet.Add((VdvKeySetEntity)relatedEntity);
					break;
				case "VdvLayout":
					_vdvLayout.Add((VdvLayoutEntity)relatedEntity);
					break;
				case "VdvProduct":
					_vdvProduct.Add((VdvProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "OwnerClient":
					DesetupSyncOwnerClient(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Net":
					DesetupSyncNet(false, true);
					break;
				case "BaseTariff":
					DesetupSyncBaseTariff(false, true);
					break;
				case "MatrixTariff":
					DesetupSyncMatrixTariff(false, true);
					break;
				case "AreaList":
					this.PerformRelatedEntityRemoval(_areaList, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AreaListElements":
					this.PerformRelatedEntityRemoval(_areaListElements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Attributes":
					this.PerformRelatedEntityRemoval(_attributes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRule":
					this.PerformRelatedEntityRemoval(_businessRule, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleCondition":
					this.PerformRelatedEntityRemoval(_businessRuleCondition, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleResult":
					this.PerformRelatedEntityRemoval(_businessRuleResult, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Calendars":
					this.PerformRelatedEntityRemoval(_calendars, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CardTickets":
					this.PerformRelatedEntityRemoval(_cardTickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Choices":
					this.PerformRelatedEntityRemoval(_choices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ClientAdaptedLayoutObjects":
					this.PerformRelatedEntityRemoval(_clientAdaptedLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Etickets":
					this.PerformRelatedEntityRemoval(_etickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalCards":
					this.PerformRelatedEntityRemoval(_externalCards, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalEfforts":
					this.PerformRelatedEntityRemoval(_externalEfforts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalPackets":
					this.PerformRelatedEntityRemoval(_externalPackets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalPacketefforts":
					this.PerformRelatedEntityRemoval(_externalPacketefforts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ExternalTypes":
					this.PerformRelatedEntityRemoval(_externalTypes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionCategories":
					this.PerformRelatedEntityRemoval(_fareEvasionCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareEvasionReasons":
					this.PerformRelatedEntityRemoval(_fareEvasionReasons, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareMatrices":
					this.PerformRelatedEntityRemoval(_fareMatrices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareStageLists":
					this.PerformRelatedEntityRemoval(_fareStageLists, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareTables":
					this.PerformRelatedEntityRemoval(_fareTables, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GuiDefs":
					this.PerformRelatedEntityRemoval(_guiDefs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "KeyAttributeTransforms":
					this.PerformRelatedEntityRemoval(_keyAttributeTransforms, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Layouts":
					this.PerformRelatedEntityRemoval(_layouts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "LineGroups":
					this.PerformRelatedEntityRemoval(_lineGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Logos":
					this.PerformRelatedEntityRemoval(_logos, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PageContentGroups":
					this.PerformRelatedEntityRemoval(_pageContentGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterAttributeValues":
					this.PerformRelatedEntityRemoval(_parameterAttributeValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterFareStage":
					this.PerformRelatedEntityRemoval(_parameterFareStage, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterTariff":
					this.PerformRelatedEntityRemoval(_parameterTariff, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterTickets":
					this.PerformRelatedEntityRemoval(_parameterTickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PredefinedKeys":
					this.PerformRelatedEntityRemoval(_predefinedKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PrimalKeys":
					this.PerformRelatedEntityRemoval(_primalKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PrintTexts":
					this.PerformRelatedEntityRemoval(_printTexts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Routes":
					this.PerformRelatedEntityRemoval(_routes, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RouteNames":
					this.PerformRelatedEntityRemoval(_routeNames, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleCappings":
					this.PerformRelatedEntityRemoval(_ruleCappings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ServiceAllocations":
					this.PerformRelatedEntityRemoval(_serviceAllocations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ShortDistances":
					this.PerformRelatedEntityRemoval(_shortDistances, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SpecialReceipts":
					this.PerformRelatedEntityRemoval(_specialReceipts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SystemTexts":
					this.PerformRelatedEntityRemoval(_systemTexts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ChildTariffs":
					this.PerformRelatedEntityRemoval(_childTariffs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MatrixParentTariff":
					this.PerformRelatedEntityRemoval(_matrixParentTariff, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TariffRelease":
					this.PerformRelatedEntityRemoval(_tariffRelease, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketGroups":
					this.PerformRelatedEntityRemoval(_ticketGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Translations":
					this.PerformRelatedEntityRemoval(_translations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UserKeys":
					this.PerformRelatedEntityRemoval(_userKeys, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvKeySet":
					this.PerformRelatedEntityRemoval(_vdvKeySet, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvLayout":
					this.PerformRelatedEntityRemoval(_vdvLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "VdvProduct":
					this.PerformRelatedEntityRemoval(_vdvProduct, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_ownerClient!=null)
			{
				toReturn.Add(_ownerClient);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_net!=null)
			{
				toReturn.Add(_net);
			}
			if(_baseTariff!=null)
			{
				toReturn.Add(_baseTariff);
			}
			if(_matrixTariff!=null)
			{
				toReturn.Add(_matrixTariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_areaList);
			toReturn.Add(_areaListElements);
			toReturn.Add(_attributes);
			toReturn.Add(_businessRule);
			toReturn.Add(_businessRuleCondition);
			toReturn.Add(_businessRuleResult);
			toReturn.Add(_calendars);
			toReturn.Add(_cardTickets);
			toReturn.Add(_choices);
			toReturn.Add(_clientAdaptedLayoutObjects);
			toReturn.Add(_etickets);
			toReturn.Add(_externalCards);
			toReturn.Add(_externalEfforts);
			toReturn.Add(_externalPackets);
			toReturn.Add(_externalPacketefforts);
			toReturn.Add(_externalTypes);
			toReturn.Add(_fareEvasionCategories);
			toReturn.Add(_fareEvasionReasons);
			toReturn.Add(_fareMatrices);
			toReturn.Add(_fareStageLists);
			toReturn.Add(_fareTables);
			toReturn.Add(_guiDefs);
			toReturn.Add(_keyAttributeTransforms);
			toReturn.Add(_layouts);
			toReturn.Add(_lineGroups);
			toReturn.Add(_logos);
			toReturn.Add(_pageContentGroups);
			toReturn.Add(_parameterAttributeValues);
			toReturn.Add(_parameterFareStage);
			toReturn.Add(_parameterTariff);
			toReturn.Add(_parameterTickets);
			toReturn.Add(_predefinedKeys);
			toReturn.Add(_primalKeys);
			toReturn.Add(_printTexts);
			toReturn.Add(_routes);
			toReturn.Add(_routeNames);
			toReturn.Add(_ruleCappings);
			toReturn.Add(_serviceAllocations);
			toReturn.Add(_shortDistances);
			toReturn.Add(_specialReceipts);
			toReturn.Add(_systemTexts);
			toReturn.Add(_childTariffs);
			toReturn.Add(_matrixParentTariff);
			toReturn.Add(_tariffRelease);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketGroups);
			toReturn.Add(_translations);
			toReturn.Add(_userKeys);
			toReturn.Add(_vdvKeySet);
			toReturn.Add(_vdvLayout);
			toReturn.Add(_vdvProduct);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="validFrom">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDValidFromVersion(Nullable<System.Int64> clientID, System.DateTime validFrom, System.Int64 version)
		{
			return FetchUsingUCClientIDValidFromVersion( clientID,  validFrom,  version, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="validFrom">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDValidFromVersion(Nullable<System.Int64> clientID, System.DateTime validFrom, System.Int64 version, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCClientIDValidFromVersion( clientID,  validFrom,  version, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="validFrom">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDValidFromVersion(Nullable<System.Int64> clientID, System.DateTime validFrom, System.Int64 version, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCClientIDValidFromVersion( clientID,  validFrom,  version, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="clientID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="validFrom">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="version">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCClientIDValidFromVersion(Nullable<System.Int64> clientID, System.DateTime validFrom, System.Int64 version, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((TariffDAO)CreateDAOInstance()).FetchTariffUsingUCClientIDValidFromVersion(this, this.Transaction, clientID, validFrom, version, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tarifID)
		{
			return FetchUsingPK(tarifID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tarifID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tarifID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tarifID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tarifID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tarifID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tarifID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TarifID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TariffRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch)
		{
			return GetMultiAreaList(forceFetch, _areaList.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaList(forceFetch, _areaList.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaList(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaList(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaList || forceFetch || _alwaysFetchAreaList) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaList);
				_areaList.SuppressClearInGetMulti=!forceFetch;
				_areaList.EntityFactoryToUse = entityFactoryToUse;
				_areaList.GetMultiManyToOne(null, null, null, this, filter);
				_areaList.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaList = true;
			}
			return _areaList;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaList'. These settings will be taken into account
		/// when the property AreaList is requested or GetMultiAreaList is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaList(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaList.SortClauses=sortClauses;
			_areaList.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElements(bool forceFetch)
		{
			return GetMultiAreaListElements(forceFetch, _areaListElements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaListElements(forceFetch, _areaListElements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaListElements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaListElements || forceFetch || _alwaysFetchAreaListElements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaListElements);
				_areaListElements.SuppressClearInGetMulti=!forceFetch;
				_areaListElements.EntityFactoryToUse = entityFactoryToUse;
				_areaListElements.GetMultiManyToOne(null, null, null, this, filter);
				_areaListElements.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaListElements = true;
			}
			return _areaListElements;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaListElements'. These settings will be taken into account
		/// when the property AreaListElements is requested or GetMultiAreaListElements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaListElements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaListElements.SortClauses=sortClauses;
			_areaListElements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeCollection GetMultiAttributes(bool forceFetch)
		{
			return GetMultiAttributes(forceFetch, _attributes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeCollection GetMultiAttributes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributes(forceFetch, _attributes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeCollection GetMultiAttributes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeCollection GetMultiAttributes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributes || forceFetch || _alwaysFetchAttributes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributes);
				_attributes.SuppressClearInGetMulti=!forceFetch;
				_attributes.EntityFactoryToUse = entityFactoryToUse;
				_attributes.GetMultiManyToOne(this, filter);
				_attributes.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributes = true;
			}
			return _attributes;
		}

		/// <summary> Sets the collection parameters for the collection for 'Attributes'. These settings will be taken into account
		/// when the property Attributes is requested or GetMultiAttributes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributes.SortClauses=sortClauses;
			_attributes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRule(bool forceFetch)
		{
			return GetMultiBusinessRule(forceFetch, _businessRule.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRule(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRule(forceFetch, _businessRule.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRule(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRule(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRule(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRule || forceFetch || _alwaysFetchBusinessRule) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRule);
				_businessRule.SuppressClearInGetMulti=!forceFetch;
				_businessRule.EntityFactoryToUse = entityFactoryToUse;
				_businessRule.GetMultiManyToOne(null, this, filter);
				_businessRule.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRule = true;
			}
			return _businessRule;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRule'. These settings will be taken into account
		/// when the property BusinessRule is requested or GetMultiBusinessRule is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRule(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRule.SortClauses=sortClauses;
			_businessRule.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleConditionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch)
		{
			return GetMultiBusinessRuleCondition(forceFetch, _businessRuleCondition.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleConditionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleCondition(forceFetch, _businessRuleCondition.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleCondition(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleCondition || forceFetch || _alwaysFetchBusinessRuleCondition) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleCondition);
				_businessRuleCondition.SuppressClearInGetMulti=!forceFetch;
				_businessRuleCondition.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleCondition.GetMultiManyToOne(null, this, filter);
				_businessRuleCondition.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleCondition = true;
			}
			return _businessRuleCondition;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleCondition'. These settings will be taken into account
		/// when the property BusinessRuleCondition is requested or GetMultiBusinessRuleCondition is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleCondition(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleCondition.SortClauses=sortClauses;
			_businessRuleCondition.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleResult(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleResult || forceFetch || _alwaysFetchBusinessRuleResult) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleResult);
				_businessRuleResult.SuppressClearInGetMulti=!forceFetch;
				_businessRuleResult.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleResult.GetMultiManyToOne(null, null, null, null, this, filter);
				_businessRuleResult.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleResult = true;
			}
			return _businessRuleResult;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleResult'. These settings will be taken into account
		/// when the property BusinessRuleResult is requested or GetMultiBusinessRuleResult is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleResult(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleResult.SortClauses=sortClauses;
			_businessRuleResult.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendars(bool forceFetch)
		{
			return GetMultiCalendars(forceFetch, _calendars.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CalendarEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendars(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCalendars(forceFetch, _calendars.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendars(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCalendars(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CalendarCollection GetMultiCalendars(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCalendars || forceFetch || _alwaysFetchCalendars) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_calendars);
				_calendars.SuppressClearInGetMulti=!forceFetch;
				_calendars.EntityFactoryToUse = entityFactoryToUse;
				_calendars.GetMultiManyToOne(null, this, filter);
				_calendars.SuppressClearInGetMulti=false;
				_alreadyFetchedCalendars = true;
			}
			return _calendars;
		}

		/// <summary> Sets the collection parameters for the collection for 'Calendars'. These settings will be taken into account
		/// when the property Calendars is requested or GetMultiCalendars is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCalendars(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_calendars.SortClauses=sortClauses;
			_calendars.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTickets(bool forceFetch)
		{
			return GetMultiCardTickets(forceFetch, _cardTickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CardTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCardTickets(forceFetch, _cardTickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCardTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection GetMultiCardTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCardTickets || forceFetch || _alwaysFetchCardTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cardTickets);
				_cardTickets.SuppressClearInGetMulti=!forceFetch;
				_cardTickets.EntityFactoryToUse = entityFactoryToUse;
				_cardTickets.GetMultiManyToOne(this, null, null, filter);
				_cardTickets.SuppressClearInGetMulti=false;
				_alreadyFetchedCardTickets = true;
			}
			return _cardTickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'CardTickets'. These settings will be taken into account
		/// when the property CardTickets is requested or GetMultiCardTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCardTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cardTickets.SortClauses=sortClauses;
			_cardTickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ChoiceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChoices(forceFetch, _choices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChoices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection GetMultiChoices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChoices || forceFetch || _alwaysFetchChoices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_choices);
				_choices.SuppressClearInGetMulti=!forceFetch;
				_choices.EntityFactoryToUse = entityFactoryToUse;
				_choices.GetMultiManyToOne(null, null, this, null, filter);
				_choices.SuppressClearInGetMulti=false;
				_alreadyFetchedChoices = true;
			}
			return _choices;
		}

		/// <summary> Sets the collection parameters for the collection for 'Choices'. These settings will be taken into account
		/// when the property Choices is requested or GetMultiChoices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChoices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_choices.SortClauses=sortClauses;
			_choices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientAdaptedLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, _clientAdaptedLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientAdaptedLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, _clientAdaptedLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientAdaptedLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection GetMultiClientAdaptedLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientAdaptedLayoutObjects || forceFetch || _alwaysFetchClientAdaptedLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientAdaptedLayoutObjects);
				_clientAdaptedLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_clientAdaptedLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_clientAdaptedLayoutObjects.GetMultiManyToOne(null, this, filter);
				_clientAdaptedLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedClientAdaptedLayoutObjects = true;
			}
			return _clientAdaptedLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientAdaptedLayoutObjects'. These settings will be taken into account
		/// when the property ClientAdaptedLayoutObjects is requested or GetMultiClientAdaptedLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientAdaptedLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientAdaptedLayoutObjects.SortClauses=sortClauses;
			_clientAdaptedLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EticketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch)
		{
			return GetMultiEtickets(forceFetch, _etickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EticketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEtickets(forceFetch, _etickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEtickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEtickets || forceFetch || _alwaysFetchEtickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_etickets);
				_etickets.SuppressClearInGetMulti=!forceFetch;
				_etickets.EntityFactoryToUse = entityFactoryToUse;
				_etickets.GetMultiManyToOne(null, this, filter);
				_etickets.SuppressClearInGetMulti=false;
				_alreadyFetchedEtickets = true;
			}
			return _etickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Etickets'. These settings will be taken into account
		/// when the property Etickets is requested or GetMultiEtickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEtickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_etickets.SortClauses=sortClauses;
			_etickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalCardCollection GetMultiExternalCards(bool forceFetch)
		{
			return GetMultiExternalCards(forceFetch, _externalCards.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalCardEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalCardCollection GetMultiExternalCards(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalCards(forceFetch, _externalCards.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalCardCollection GetMultiExternalCards(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalCards(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalCardEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalCardCollection GetMultiExternalCards(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalCards || forceFetch || _alwaysFetchExternalCards) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalCards);
				_externalCards.SuppressClearInGetMulti=!forceFetch;
				_externalCards.EntityFactoryToUse = entityFactoryToUse;
				_externalCards.GetMultiManyToOne(null, null, null, this, filter);
				_externalCards.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalCards = true;
			}
			return _externalCards;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalCards'. These settings will be taken into account
		/// when the property ExternalCards is requested or GetMultiExternalCards is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalCards(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalCards.SortClauses=sortClauses;
			_externalCards.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalEffortCollection GetMultiExternalEfforts(bool forceFetch)
		{
			return GetMultiExternalEfforts(forceFetch, _externalEfforts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalEffortCollection GetMultiExternalEfforts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalEfforts(forceFetch, _externalEfforts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalEffortCollection GetMultiExternalEfforts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalEfforts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalEffortCollection GetMultiExternalEfforts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalEfforts || forceFetch || _alwaysFetchExternalEfforts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalEfforts);
				_externalEfforts.SuppressClearInGetMulti=!forceFetch;
				_externalEfforts.EntityFactoryToUse = entityFactoryToUse;
				_externalEfforts.GetMultiManyToOne(this, filter);
				_externalEfforts.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalEfforts = true;
			}
			return _externalEfforts;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalEfforts'. These settings will be taken into account
		/// when the property ExternalEfforts is requested or GetMultiExternalEfforts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalEfforts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalEfforts.SortClauses=sortClauses;
			_externalEfforts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketCollection GetMultiExternalPackets(bool forceFetch)
		{
			return GetMultiExternalPackets(forceFetch, _externalPackets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketCollection GetMultiExternalPackets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalPackets(forceFetch, _externalPackets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketCollection GetMultiExternalPackets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalPackets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketCollection GetMultiExternalPackets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalPackets || forceFetch || _alwaysFetchExternalPackets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalPackets);
				_externalPackets.SuppressClearInGetMulti=!forceFetch;
				_externalPackets.EntityFactoryToUse = entityFactoryToUse;
				_externalPackets.GetMultiManyToOne(this, filter);
				_externalPackets.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalPackets = true;
			}
			return _externalPackets;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalPackets'. These settings will be taken into account
		/// when the property ExternalPackets is requested or GetMultiExternalPackets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalPackets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalPackets.SortClauses=sortClauses;
			_externalPackets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch)
		{
			return GetMultiExternalPacketefforts(forceFetch, _externalPacketefforts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalPacketEffortEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalPacketefforts(forceFetch, _externalPacketefforts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalPacketefforts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection GetMultiExternalPacketefforts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalPacketefforts || forceFetch || _alwaysFetchExternalPacketefforts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalPacketefforts);
				_externalPacketefforts.SuppressClearInGetMulti=!forceFetch;
				_externalPacketefforts.EntityFactoryToUse = entityFactoryToUse;
				_externalPacketefforts.GetMultiManyToOne(null, null, null, null, null, this, null, filter);
				_externalPacketefforts.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalPacketefforts = true;
			}
			return _externalPacketefforts;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalPacketefforts'. These settings will be taken into account
		/// when the property ExternalPacketefforts is requested or GetMultiExternalPacketefforts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalPacketefforts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalPacketefforts.SortClauses=sortClauses;
			_externalPacketefforts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ExternalTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ExternalTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalTypeCollection GetMultiExternalTypes(bool forceFetch)
		{
			return GetMultiExternalTypes(forceFetch, _externalTypes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ExternalTypeEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ExternalTypeCollection GetMultiExternalTypes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiExternalTypes(forceFetch, _externalTypes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ExternalTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ExternalTypeCollection GetMultiExternalTypes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiExternalTypes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ExternalTypeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ExternalTypeCollection GetMultiExternalTypes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedExternalTypes || forceFetch || _alwaysFetchExternalTypes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_externalTypes);
				_externalTypes.SuppressClearInGetMulti=!forceFetch;
				_externalTypes.EntityFactoryToUse = entityFactoryToUse;
				_externalTypes.GetMultiManyToOne(this, filter);
				_externalTypes.SuppressClearInGetMulti=false;
				_alreadyFetchedExternalTypes = true;
			}
			return _externalTypes;
		}

		/// <summary> Sets the collection parameters for the collection for 'ExternalTypes'. These settings will be taken into account
		/// when the property ExternalTypes is requested or GetMultiExternalTypes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersExternalTypes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_externalTypes.SortClauses=sortClauses;
			_externalTypes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection GetMultiFareEvasionCategories(bool forceFetch)
		{
			return GetMultiFareEvasionCategories(forceFetch, _fareEvasionCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection GetMultiFareEvasionCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionCategories(forceFetch, _fareEvasionCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection GetMultiFareEvasionCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection GetMultiFareEvasionCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionCategories || forceFetch || _alwaysFetchFareEvasionCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionCategories);
				_fareEvasionCategories.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionCategories.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionCategories.GetMultiManyToOne(this, filter);
				_fareEvasionCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionCategories = true;
			}
			return _fareEvasionCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionCategories'. These settings will be taken into account
		/// when the property FareEvasionCategories is requested or GetMultiFareEvasionCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionCategories.SortClauses=sortClauses;
			_fareEvasionCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionReasonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiFareEvasionReasons(bool forceFetch)
		{
			return GetMultiFareEvasionReasons(forceFetch, _fareEvasionReasons.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareEvasionReasonEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiFareEvasionReasons(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareEvasionReasons(forceFetch, _fareEvasionReasons.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiFareEvasionReasons(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareEvasionReasons(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection GetMultiFareEvasionReasons(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareEvasionReasons || forceFetch || _alwaysFetchFareEvasionReasons) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareEvasionReasons);
				_fareEvasionReasons.SuppressClearInGetMulti=!forceFetch;
				_fareEvasionReasons.EntityFactoryToUse = entityFactoryToUse;
				_fareEvasionReasons.GetMultiManyToOne(this, filter);
				_fareEvasionReasons.SuppressClearInGetMulti=false;
				_alreadyFetchedFareEvasionReasons = true;
			}
			return _fareEvasionReasons;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareEvasionReasons'. These settings will be taken into account
		/// when the property FareEvasionReasons is requested or GetMultiFareEvasionReasons is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareEvasionReasons(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareEvasionReasons.SortClauses=sortClauses;
			_fareEvasionReasons.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch)
		{
			return GetMultiFareMatrices(forceFetch, _fareMatrices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrices(forceFetch, _fareMatrices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection GetMultiFareMatrices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrices || forceFetch || _alwaysFetchFareMatrices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrices);
				_fareMatrices.SuppressClearInGetMulti=!forceFetch;
				_fareMatrices.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrices.GetMultiManyToOne(null, null, null, this, filter);
				_fareMatrices.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrices = true;
			}
			return _fareMatrices;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrices'. These settings will be taken into account
		/// when the property FareMatrices is requested or GetMultiFareMatrices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrices.SortClauses=sortClauses;
			_fareMatrices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareStageListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch)
		{
			return GetMultiFareStageLists(forceFetch, _fareStageLists.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareStageListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareStageLists(forceFetch, _fareStageLists.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareStageLists(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareStageListCollection GetMultiFareStageLists(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareStageLists || forceFetch || _alwaysFetchFareStageLists) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareStageLists);
				_fareStageLists.SuppressClearInGetMulti=!forceFetch;
				_fareStageLists.EntityFactoryToUse = entityFactoryToUse;
				_fareStageLists.GetMultiManyToOne(null, this, filter);
				_fareStageLists.SuppressClearInGetMulti=false;
				_alreadyFetchedFareStageLists = true;
			}
			return _fareStageLists;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareStageLists'. These settings will be taken into account
		/// when the property FareStageLists is requested or GetMultiFareStageLists is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareStageLists(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareStageLists.SortClauses=sortClauses;
			_fareStageLists.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTables(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTables || forceFetch || _alwaysFetchFareTables) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTables);
				_fareTables.SuppressClearInGetMulti=!forceFetch;
				_fareTables.EntityFactoryToUse = entityFactoryToUse;
				_fareTables.GetMultiManyToOne(null, null, this, filter);
				_fareTables.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTables = true;
			}
			return _fareTables;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTables'. These settings will be taken into account
		/// when the property FareTables is requested or GetMultiFareTables is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTables(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTables.SortClauses=sortClauses;
			_fareTables.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGuiDefs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGuiDefs || forceFetch || _alwaysFetchGuiDefs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_guiDefs);
				_guiDefs.SuppressClearInGetMulti=!forceFetch;
				_guiDefs.EntityFactoryToUse = entityFactoryToUse;
				_guiDefs.GetMultiManyToOne(null, null, this, filter);
				_guiDefs.SuppressClearInGetMulti=false;
				_alreadyFetchedGuiDefs = true;
			}
			return _guiDefs;
		}

		/// <summary> Sets the collection parameters for the collection for 'GuiDefs'. These settings will be taken into account
		/// when the property GuiDefs is requested or GetMultiGuiDefs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGuiDefs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_guiDefs.SortClauses=sortClauses;
			_guiDefs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, _keyAttributeTransforms.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'KeyAttributeTransfromEntity'</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, _keyAttributeTransforms.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiKeyAttributeTransforms(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection GetMultiKeyAttributeTransforms(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedKeyAttributeTransforms || forceFetch || _alwaysFetchKeyAttributeTransforms) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_keyAttributeTransforms);
				_keyAttributeTransforms.SuppressClearInGetMulti=!forceFetch;
				_keyAttributeTransforms.EntityFactoryToUse = entityFactoryToUse;
				_keyAttributeTransforms.GetMultiManyToOne(null, null, null, null, this, filter);
				_keyAttributeTransforms.SuppressClearInGetMulti=false;
				_alreadyFetchedKeyAttributeTransforms = true;
			}
			return _keyAttributeTransforms;
		}

		/// <summary> Sets the collection parameters for the collection for 'KeyAttributeTransforms'. These settings will be taken into account
		/// when the property KeyAttributeTransforms is requested or GetMultiKeyAttributeTransforms is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersKeyAttributeTransforms(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_keyAttributeTransforms.SortClauses=sortClauses;
			_keyAttributeTransforms.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLayouts(forceFetch, _layouts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLayouts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection GetMultiLayouts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLayouts || forceFetch || _alwaysFetchLayouts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_layouts);
				_layouts.SuppressClearInGetMulti=!forceFetch;
				_layouts.EntityFactoryToUse = entityFactoryToUse;
				_layouts.GetMultiManyToOne(null, null, this, null, filter);
				_layouts.SuppressClearInGetMulti=false;
				_alreadyFetchedLayouts = true;
			}
			return _layouts;
		}

		/// <summary> Sets the collection parameters for the collection for 'Layouts'. These settings will be taken into account
		/// when the property Layouts is requested or GetMultiLayouts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLayouts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_layouts.SortClauses=sortClauses;
			_layouts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupCollection GetMultiLineGroups(bool forceFetch)
		{
			return GetMultiLineGroups(forceFetch, _lineGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LineGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupCollection GetMultiLineGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLineGroups(forceFetch, _lineGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineGroupCollection GetMultiLineGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLineGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupCollection GetMultiLineGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLineGroups || forceFetch || _alwaysFetchLineGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_lineGroups);
				_lineGroups.SuppressClearInGetMulti=!forceFetch;
				_lineGroups.EntityFactoryToUse = entityFactoryToUse;
				_lineGroups.GetMultiManyToOne(this, filter);
				_lineGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedLineGroups = true;
			}
			return _lineGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'LineGroups'. These settings will be taken into account
		/// when the property LineGroups is requested or GetMultiLineGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLineGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_lineGroups.SortClauses=sortClauses;
			_lineGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LogoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LogoEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LogoCollection GetMultiLogos(bool forceFetch)
		{
			return GetMultiLogos(forceFetch, _logos.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LogoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LogoEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LogoCollection GetMultiLogos(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLogos(forceFetch, _logos.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LogoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LogoCollection GetMultiLogos(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLogos(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LogoEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LogoCollection GetMultiLogos(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLogos || forceFetch || _alwaysFetchLogos) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_logos);
				_logos.SuppressClearInGetMulti=!forceFetch;
				_logos.EntityFactoryToUse = entityFactoryToUse;
				_logos.GetMultiManyToOne(this, filter);
				_logos.SuppressClearInGetMulti=false;
				_alreadyFetchedLogos = true;
			}
			return _logos;
		}

		/// <summary> Sets the collection parameters for the collection for 'Logos'. These settings will be taken into account
		/// when the property Logos is requested or GetMultiLogos is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLogos(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_logos.SortClauses=sortClauses;
			_logos.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PageContentGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroups(bool forceFetch)
		{
			return GetMultiPageContentGroups(forceFetch, _pageContentGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PageContentGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPageContentGroups(forceFetch, _pageContentGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPageContentGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PageContentGroupCollection GetMultiPageContentGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPageContentGroups || forceFetch || _alwaysFetchPageContentGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pageContentGroups);
				_pageContentGroups.SuppressClearInGetMulti=!forceFetch;
				_pageContentGroups.EntityFactoryToUse = entityFactoryToUse;
				_pageContentGroups.GetMultiManyToOne(this, filter);
				_pageContentGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedPageContentGroups = true;
			}
			return _pageContentGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'PageContentGroups'. These settings will be taken into account
		/// when the property PageContentGroups is requested or GetMultiPageContentGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPageContentGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pageContentGroups.SortClauses=sortClauses;
			_pageContentGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterAttributeValues(forceFetch, _parameterAttributeValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterAttributeValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection GetMultiParameterAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterAttributeValues || forceFetch || _alwaysFetchParameterAttributeValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterAttributeValues);
				_parameterAttributeValues.SuppressClearInGetMulti=!forceFetch;
				_parameterAttributeValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterAttributeValues.GetMultiManyToOne(null, this, null, filter);
				_parameterAttributeValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterAttributeValues = true;
			}
			return _parameterAttributeValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterAttributeValues'. These settings will be taken into account
		/// when the property ParameterAttributeValues is requested or GetMultiParameterAttributeValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterAttributeValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterAttributeValues.SortClauses=sortClauses;
			_parameterAttributeValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterFareStageEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterFareStage(forceFetch, _parameterFareStage.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterFareStage(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection GetMultiParameterFareStage(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterFareStage || forceFetch || _alwaysFetchParameterFareStage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterFareStage);
				_parameterFareStage.SuppressClearInGetMulti=!forceFetch;
				_parameterFareStage.EntityFactoryToUse = entityFactoryToUse;
				_parameterFareStage.GetMultiManyToOne(null, this, null, filter);
				_parameterFareStage.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterFareStage = true;
			}
			return _parameterFareStage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterFareStage'. These settings will be taken into account
		/// when the property ParameterFareStage is requested or GetMultiParameterFareStage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterFareStage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterFareStage.SortClauses=sortClauses;
			_parameterFareStage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch)
		{
			return GetMultiParameterTariff(forceFetch, _parameterTariff.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterTariff(forceFetch, _parameterTariff.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterTariff(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTariffCollection GetMultiParameterTariff(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterTariff || forceFetch || _alwaysFetchParameterTariff) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterTariff);
				_parameterTariff.SuppressClearInGetMulti=!forceFetch;
				_parameterTariff.EntityFactoryToUse = entityFactoryToUse;
				_parameterTariff.GetMultiManyToOne(this, null, filter);
				_parameterTariff.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterTariff = true;
			}
			return _parameterTariff;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterTariff'. These settings will be taken into account
		/// when the property ParameterTariff is requested or GetMultiParameterTariff is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterTariff(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterTariff.SortClauses=sortClauses;
			_parameterTariff.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTickets(bool forceFetch)
		{
			return GetMultiParameterTickets(forceFetch, _parameterTickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterTickets(forceFetch, _parameterTickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection GetMultiParameterTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterTickets || forceFetch || _alwaysFetchParameterTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterTickets);
				_parameterTickets.SuppressClearInGetMulti=!forceFetch;
				_parameterTickets.EntityFactoryToUse = entityFactoryToUse;
				_parameterTickets.GetMultiManyToOne(this, null, null, filter);
				_parameterTickets.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterTickets = true;
			}
			return _parameterTickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterTickets'. These settings will be taken into account
		/// when the property ParameterTickets is requested or GetMultiParameterTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterTickets.SortClauses=sortClauses;
			_parameterTickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PredefinedKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch)
		{
			return GetMultiPredefinedKeys(forceFetch, _predefinedKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PredefinedKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPredefinedKeys(forceFetch, _predefinedKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPredefinedKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PredefinedKeyCollection GetMultiPredefinedKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPredefinedKeys || forceFetch || _alwaysFetchPredefinedKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_predefinedKeys);
				_predefinedKeys.SuppressClearInGetMulti=!forceFetch;
				_predefinedKeys.EntityFactoryToUse = entityFactoryToUse;
				_predefinedKeys.GetMultiManyToOne(null, this, filter);
				_predefinedKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPredefinedKeys = true;
			}
			return _predefinedKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PredefinedKeys'. These settings will be taken into account
		/// when the property PredefinedKeys is requested or GetMultiPredefinedKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPredefinedKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_predefinedKeys.SortClauses=sortClauses;
			_predefinedKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PrimalKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPrimalKeys(forceFetch, _primalKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPrimalKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection GetMultiPrimalKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPrimalKeys || forceFetch || _alwaysFetchPrimalKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_primalKeys);
				_primalKeys.SuppressClearInGetMulti=!forceFetch;
				_primalKeys.EntityFactoryToUse = entityFactoryToUse;
				_primalKeys.GetMultiManyToOne(null, this, null, filter);
				_primalKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedPrimalKeys = true;
			}
			return _primalKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'PrimalKeys'. These settings will be taken into account
		/// when the property PrimalKeys is requested or GetMultiPrimalKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPrimalKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_primalKeys.SortClauses=sortClauses;
			_primalKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PrintTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PrintTextEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrintTextCollection GetMultiPrintTexts(bool forceFetch)
		{
			return GetMultiPrintTexts(forceFetch, _printTexts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrintTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PrintTextEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PrintTextCollection GetMultiPrintTexts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPrintTexts(forceFetch, _printTexts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PrintTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PrintTextCollection GetMultiPrintTexts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPrintTexts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PrintTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PrintTextCollection GetMultiPrintTexts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPrintTexts || forceFetch || _alwaysFetchPrintTexts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_printTexts);
				_printTexts.SuppressClearInGetMulti=!forceFetch;
				_printTexts.EntityFactoryToUse = entityFactoryToUse;
				_printTexts.GetMultiManyToOne(this, filter);
				_printTexts.SuppressClearInGetMulti=false;
				_alreadyFetchedPrintTexts = true;
			}
			return _printTexts;
		}

		/// <summary> Sets the collection parameters for the collection for 'PrintTexts'. These settings will be taken into account
		/// when the property PrintTexts is requested or GetMultiPrintTexts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPrintTexts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_printTexts.SortClauses=sortClauses;
			_printTexts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutes(bool forceFetch)
		{
			return GetMultiRoutes(forceFetch, _routes.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutes(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutes(forceFetch, _routes.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutes(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutes(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutes(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutes || forceFetch || _alwaysFetchRoutes) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routes);
				_routes.SuppressClearInGetMulti=!forceFetch;
				_routes.EntityFactoryToUse = entityFactoryToUse;
				_routes.GetMultiManyToOne(null, null, null, null, null, null, this, filter);
				_routes.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutes = true;
			}
			return _routes;
		}

		/// <summary> Sets the collection parameters for the collection for 'Routes'. These settings will be taken into account
		/// when the property Routes is requested or GetMultiRoutes is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutes(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routes.SortClauses=sortClauses;
			_routes.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteNameEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteNameEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteNameCollection GetMultiRouteNames(bool forceFetch)
		{
			return GetMultiRouteNames(forceFetch, _routeNames.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteNameEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteNameEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteNameCollection GetMultiRouteNames(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRouteNames(forceFetch, _routeNames.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteNameEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteNameCollection GetMultiRouteNames(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRouteNames(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteNameEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteNameCollection GetMultiRouteNames(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRouteNames || forceFetch || _alwaysFetchRouteNames) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeNames);
				_routeNames.SuppressClearInGetMulti=!forceFetch;
				_routeNames.EntityFactoryToUse = entityFactoryToUse;
				_routeNames.GetMultiManyToOne(this, filter);
				_routeNames.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteNames = true;
			}
			return _routeNames;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteNames'. These settings will be taken into account
		/// when the property RouteNames is requested or GetMultiRouteNames is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteNames(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeNames.SortClauses=sortClauses;
			_routeNames.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappings(bool forceFetch)
		{
			return GetMultiRuleCappings(forceFetch, _ruleCappings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleCappingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleCappings(forceFetch, _ruleCappings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleCappings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection GetMultiRuleCappings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleCappings || forceFetch || _alwaysFetchRuleCappings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleCappings);
				_ruleCappings.SuppressClearInGetMulti=!forceFetch;
				_ruleCappings.EntityFactoryToUse = entityFactoryToUse;
				_ruleCappings.GetMultiManyToOne(null, null, null, this, filter);
				_ruleCappings.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleCappings = true;
			}
			return _ruleCappings;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleCappings'. These settings will be taken into account
		/// when the property RuleCappings is requested or GetMultiRuleCappings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleCappings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleCappings.SortClauses=sortClauses;
			_ruleCappings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ServiceAllocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ServiceAllocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ServiceAllocationCollection GetMultiServiceAllocations(bool forceFetch)
		{
			return GetMultiServiceAllocations(forceFetch, _serviceAllocations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceAllocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ServiceAllocationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ServiceAllocationCollection GetMultiServiceAllocations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiServiceAllocations(forceFetch, _serviceAllocations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ServiceAllocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ServiceAllocationCollection GetMultiServiceAllocations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiServiceAllocations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ServiceAllocationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ServiceAllocationCollection GetMultiServiceAllocations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedServiceAllocations || forceFetch || _alwaysFetchServiceAllocations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_serviceAllocations);
				_serviceAllocations.SuppressClearInGetMulti=!forceFetch;
				_serviceAllocations.EntityFactoryToUse = entityFactoryToUse;
				_serviceAllocations.GetMultiManyToOne(this, filter);
				_serviceAllocations.SuppressClearInGetMulti=false;
				_alreadyFetchedServiceAllocations = true;
			}
			return _serviceAllocations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ServiceAllocations'. These settings will be taken into account
		/// when the property ServiceAllocations is requested or GetMultiServiceAllocations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersServiceAllocations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_serviceAllocations.SortClauses=sortClauses;
			_serviceAllocations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ShortDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ShortDistanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ShortDistanceCollection GetMultiShortDistances(bool forceFetch)
		{
			return GetMultiShortDistances(forceFetch, _shortDistances.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ShortDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ShortDistanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ShortDistanceCollection GetMultiShortDistances(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiShortDistances(forceFetch, _shortDistances.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ShortDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ShortDistanceCollection GetMultiShortDistances(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiShortDistances(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ShortDistanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ShortDistanceCollection GetMultiShortDistances(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedShortDistances || forceFetch || _alwaysFetchShortDistances) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_shortDistances);
				_shortDistances.SuppressClearInGetMulti=!forceFetch;
				_shortDistances.EntityFactoryToUse = entityFactoryToUse;
				_shortDistances.GetMultiManyToOne(this, filter);
				_shortDistances.SuppressClearInGetMulti=false;
				_alreadyFetchedShortDistances = true;
			}
			return _shortDistances;
		}

		/// <summary> Sets the collection parameters for the collection for 'ShortDistances'. These settings will be taken into account
		/// when the property ShortDistances is requested or GetMultiShortDistances is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersShortDistances(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_shortDistances.SortClauses=sortClauses;
			_shortDistances.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSpecialReceipts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSpecialReceipts || forceFetch || _alwaysFetchSpecialReceipts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_specialReceipts);
				_specialReceipts.SuppressClearInGetMulti=!forceFetch;
				_specialReceipts.EntityFactoryToUse = entityFactoryToUse;
				_specialReceipts.GetMultiManyToOne(null, null, this, filter);
				_specialReceipts.SuppressClearInGetMulti=false;
				_alreadyFetchedSpecialReceipts = true;
			}
			return _specialReceipts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SpecialReceipts'. These settings will be taken into account
		/// when the property SpecialReceipts is requested or GetMultiSpecialReceipts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSpecialReceipts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_specialReceipts.SortClauses=sortClauses;
			_specialReceipts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SystemTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SystemTextEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemTextCollection GetMultiSystemTexts(bool forceFetch)
		{
			return GetMultiSystemTexts(forceFetch, _systemTexts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SystemTextEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemTextCollection GetMultiSystemTexts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSystemTexts(forceFetch, _systemTexts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SystemTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SystemTextCollection GetMultiSystemTexts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSystemTexts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SystemTextCollection GetMultiSystemTexts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSystemTexts || forceFetch || _alwaysFetchSystemTexts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_systemTexts);
				_systemTexts.SuppressClearInGetMulti=!forceFetch;
				_systemTexts.EntityFactoryToUse = entityFactoryToUse;
				_systemTexts.GetMultiManyToOne(this, filter);
				_systemTexts.SuppressClearInGetMulti=false;
				_alreadyFetchedSystemTexts = true;
			}
			return _systemTexts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SystemTexts'. These settings will be taken into account
		/// when the property SystemTexts is requested or GetMultiSystemTexts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSystemTexts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_systemTexts.SortClauses=sortClauses;
			_systemTexts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiChildTariffs(bool forceFetch)
		{
			return GetMultiChildTariffs(forceFetch, _childTariffs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiChildTariffs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiChildTariffs(forceFetch, _childTariffs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiChildTariffs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiChildTariffs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiChildTariffs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedChildTariffs || forceFetch || _alwaysFetchChildTariffs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_childTariffs);
				_childTariffs.SuppressClearInGetMulti=!forceFetch;
				_childTariffs.EntityFactoryToUse = entityFactoryToUse;
				_childTariffs.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_childTariffs.SuppressClearInGetMulti=false;
				_alreadyFetchedChildTariffs = true;
			}
			return _childTariffs;
		}

		/// <summary> Sets the collection parameters for the collection for 'ChildTariffs'. These settings will be taken into account
		/// when the property ChildTariffs is requested or GetMultiChildTariffs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersChildTariffs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_childTariffs.SortClauses=sortClauses;
			_childTariffs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiMatrixParentTariff(bool forceFetch)
		{
			return GetMultiMatrixParentTariff(forceFetch, _matrixParentTariff.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiMatrixParentTariff(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMatrixParentTariff(forceFetch, _matrixParentTariff.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiMatrixParentTariff(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMatrixParentTariff(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiMatrixParentTariff(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMatrixParentTariff || forceFetch || _alwaysFetchMatrixParentTariff) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_matrixParentTariff);
				_matrixParentTariff.SuppressClearInGetMulti=!forceFetch;
				_matrixParentTariff.EntityFactoryToUse = entityFactoryToUse;
				_matrixParentTariff.GetMultiManyToOne(null, null, null, null, null, this, filter);
				_matrixParentTariff.SuppressClearInGetMulti=false;
				_alreadyFetchedMatrixParentTariff = true;
			}
			return _matrixParentTariff;
		}

		/// <summary> Sets the collection parameters for the collection for 'MatrixParentTariff'. These settings will be taken into account
		/// when the property MatrixParentTariff is requested or GetMultiMatrixParentTariff is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMatrixParentTariff(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_matrixParentTariff.SortClauses=sortClauses;
			_matrixParentTariff.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch)
		{
			return GetMultiTariffRelease(forceFetch, _tariffRelease.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffRelease(forceFetch, _tariffRelease.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffRelease(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffRelease || forceFetch || _alwaysFetchTariffRelease) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffRelease);
				_tariffRelease.SuppressClearInGetMulti=!forceFetch;
				_tariffRelease.EntityFactoryToUse = entityFactoryToUse;
				_tariffRelease.GetMultiManyToOne(null, this, filter);
				_tariffRelease.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffRelease = true;
			}
			return _tariffRelease;
		}

		/// <summary> Sets the collection parameters for the collection for 'TariffRelease'. These settings will be taken into account
		/// when the property TariffRelease is requested or GetMultiTariffRelease is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffRelease(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffRelease.SortClauses=sortClauses;
			_tariffRelease.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroups(bool forceFetch)
		{
			return GetMultiTicketGroups(forceFetch, _ticketGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketGroups(forceFetch, _ticketGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketGroupCollection GetMultiTicketGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketGroups || forceFetch || _alwaysFetchTicketGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketGroups);
				_ticketGroups.SuppressClearInGetMulti=!forceFetch;
				_ticketGroups.EntityFactoryToUse = entityFactoryToUse;
				_ticketGroups.GetMultiManyToOne(this, filter);
				_ticketGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketGroups = true;
			}
			return _ticketGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketGroups'. These settings will be taken into account
		/// when the property TicketGroups is requested or GetMultiTicketGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketGroups.SortClauses=sortClauses;
			_ticketGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TranslationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TranslationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TranslationCollection GetMultiTranslations(bool forceFetch)
		{
			return GetMultiTranslations(forceFetch, _translations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TranslationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TranslationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TranslationCollection GetMultiTranslations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTranslations(forceFetch, _translations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TranslationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TranslationCollection GetMultiTranslations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTranslations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TranslationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TranslationCollection GetMultiTranslations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTranslations || forceFetch || _alwaysFetchTranslations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_translations);
				_translations.SuppressClearInGetMulti=!forceFetch;
				_translations.EntityFactoryToUse = entityFactoryToUse;
				_translations.GetMultiManyToOne(null, this, filter);
				_translations.SuppressClearInGetMulti=false;
				_alreadyFetchedTranslations = true;
			}
			return _translations;
		}

		/// <summary> Sets the collection parameters for the collection for 'Translations'. These settings will be taken into account
		/// when the property Translations is requested or GetMultiTranslations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTranslations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_translations.SortClauses=sortClauses;
			_translations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch)
		{
			return GetMultiUserKeys(forceFetch, _userKeys.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UserKeyEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUserKeys(forceFetch, _userKeys.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUserKeys(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection GetMultiUserKeys(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUserKeys || forceFetch || _alwaysFetchUserKeys) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userKeys);
				_userKeys.SuppressClearInGetMulti=!forceFetch;
				_userKeys.EntityFactoryToUse = entityFactoryToUse;
				_userKeys.GetMultiManyToOne(null, null, this, filter);
				_userKeys.SuppressClearInGetMulti=false;
				_alreadyFetchedUserKeys = true;
			}
			return _userKeys;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserKeys'. These settings will be taken into account
		/// when the property UserKeys is requested or GetMultiUserKeys is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserKeys(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userKeys.SortClauses=sortClauses;
			_userKeys.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvKeySetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvKeySet(forceFetch, _vdvKeySet.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvKeySet(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection GetMultiVdvKeySet(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvKeySet || forceFetch || _alwaysFetchVdvKeySet) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvKeySet);
				_vdvKeySet.SuppressClearInGetMulti=!forceFetch;
				_vdvKeySet.EntityFactoryToUse = entityFactoryToUse;
				_vdvKeySet.GetMultiManyToOne(null, this, null, filter);
				_vdvKeySet.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvKeySet = true;
			}
			return _vdvKeySet;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvKeySet'. These settings will be taken into account
		/// when the property VdvKeySet is requested or GetMultiVdvKeySet is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvKeySet(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvKeySet.SortClauses=sortClauses;
			_vdvKeySet.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutCollection GetMultiVdvLayout(bool forceFetch)
		{
			return GetMultiVdvLayout(forceFetch, _vdvLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvLayoutEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutCollection GetMultiVdvLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvLayout(forceFetch, _vdvLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvLayoutCollection GetMultiVdvLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvLayoutEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutCollection GetMultiVdvLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvLayout || forceFetch || _alwaysFetchVdvLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvLayout);
				_vdvLayout.SuppressClearInGetMulti=!forceFetch;
				_vdvLayout.EntityFactoryToUse = entityFactoryToUse;
				_vdvLayout.GetMultiManyToOne(this, filter);
				_vdvLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvLayout = true;
			}
			return _vdvLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvLayout'. These settings will be taken into account
		/// when the property VdvLayout is requested or GetMultiVdvLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvLayout.SortClauses=sortClauses;
			_vdvLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'VdvProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiVdvProduct(forceFetch, _vdvProduct.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiVdvProduct(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection GetMultiVdvProduct(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedVdvProduct || forceFetch || _alwaysFetchVdvProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_vdvProduct);
				_vdvProduct.SuppressClearInGetMulti=!forceFetch;
				_vdvProduct.EntityFactoryToUse = entityFactoryToUse;
				_vdvProduct.GetMultiManyToOne(null, null, this, null, null, null, null, null, filter);
				_vdvProduct.SuppressClearInGetMulti=false;
				_alreadyFetchedVdvProduct = true;
			}
			return _vdvProduct;
		}

		/// <summary> Sets the collection parameters for the collection for 'VdvProduct'. These settings will be taken into account
		/// when the property VdvProduct is requested or GetMultiVdvProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersVdvProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_vdvProduct.SortClauses=sortClauses;
			_vdvProduct.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleOwnerClient()
		{
			return GetSingleOwnerClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleOwnerClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedOwnerClient || forceFetch || _alwaysFetchOwnerClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ownerClientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OwnerClient = newEntity;
				_alreadyFetchedOwnerClient = fetchResult;
			}
			return _ownerClient;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public NetEntity GetSingleNet()
		{
			return GetSingleNet(false);
		}

		/// <summary> Retrieves the related entity of type 'NetEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'NetEntity' which is related to this entity.</returns>
		public virtual NetEntity GetSingleNet(bool forceFetch)
		{
			if( ( !_alreadyFetchedNet || forceFetch || _alwaysFetchNet) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.NetEntityUsingNetID);
				NetEntity newEntity = new NetEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.NetID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (NetEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_netReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Net = newEntity;
				_alreadyFetchedNet = fetchResult;
			}
			return _net;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleBaseTariff()
		{
			return GetSingleBaseTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleBaseTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedBaseTariff || forceFetch || _alwaysFetchBaseTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTarifIDBaseTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BaseTariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_baseTariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BaseTariff = newEntity;
				_alreadyFetchedBaseTariff = fetchResult;
			}
			return _baseTariff;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleMatrixTariff()
		{
			return GetSingleMatrixTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleMatrixTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedMatrixTariff || forceFetch || _alwaysFetchMatrixTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTarifIDMatrixTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MatrixTariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_matrixTariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MatrixTariff = newEntity;
				_alreadyFetchedMatrixTariff = fetchResult;
			}
			return _matrixTariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("OwnerClient", _ownerClient);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Net", _net);
			toReturn.Add("BaseTariff", _baseTariff);
			toReturn.Add("MatrixTariff", _matrixTariff);
			toReturn.Add("AreaList", _areaList);
			toReturn.Add("AreaListElements", _areaListElements);
			toReturn.Add("Attributes", _attributes);
			toReturn.Add("BusinessRule", _businessRule);
			toReturn.Add("BusinessRuleCondition", _businessRuleCondition);
			toReturn.Add("BusinessRuleResult", _businessRuleResult);
			toReturn.Add("Calendars", _calendars);
			toReturn.Add("CardTickets", _cardTickets);
			toReturn.Add("Choices", _choices);
			toReturn.Add("ClientAdaptedLayoutObjects", _clientAdaptedLayoutObjects);
			toReturn.Add("Etickets", _etickets);
			toReturn.Add("ExternalCards", _externalCards);
			toReturn.Add("ExternalEfforts", _externalEfforts);
			toReturn.Add("ExternalPackets", _externalPackets);
			toReturn.Add("ExternalPacketefforts", _externalPacketefforts);
			toReturn.Add("ExternalTypes", _externalTypes);
			toReturn.Add("FareEvasionCategories", _fareEvasionCategories);
			toReturn.Add("FareEvasionReasons", _fareEvasionReasons);
			toReturn.Add("FareMatrices", _fareMatrices);
			toReturn.Add("FareStageLists", _fareStageLists);
			toReturn.Add("FareTables", _fareTables);
			toReturn.Add("GuiDefs", _guiDefs);
			toReturn.Add("KeyAttributeTransforms", _keyAttributeTransforms);
			toReturn.Add("Layouts", _layouts);
			toReturn.Add("LineGroups", _lineGroups);
			toReturn.Add("Logos", _logos);
			toReturn.Add("PageContentGroups", _pageContentGroups);
			toReturn.Add("ParameterAttributeValues", _parameterAttributeValues);
			toReturn.Add("ParameterFareStage", _parameterFareStage);
			toReturn.Add("ParameterTariff", _parameterTariff);
			toReturn.Add("ParameterTickets", _parameterTickets);
			toReturn.Add("PredefinedKeys", _predefinedKeys);
			toReturn.Add("PrimalKeys", _primalKeys);
			toReturn.Add("PrintTexts", _printTexts);
			toReturn.Add("Routes", _routes);
			toReturn.Add("RouteNames", _routeNames);
			toReturn.Add("RuleCappings", _ruleCappings);
			toReturn.Add("ServiceAllocations", _serviceAllocations);
			toReturn.Add("ShortDistances", _shortDistances);
			toReturn.Add("SpecialReceipts", _specialReceipts);
			toReturn.Add("SystemTexts", _systemTexts);
			toReturn.Add("ChildTariffs", _childTariffs);
			toReturn.Add("MatrixParentTariff", _matrixParentTariff);
			toReturn.Add("TariffRelease", _tariffRelease);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketGroups", _ticketGroups);
			toReturn.Add("Translations", _translations);
			toReturn.Add("UserKeys", _userKeys);
			toReturn.Add("VdvKeySet", _vdvKeySet);
			toReturn.Add("VdvLayout", _vdvLayout);
			toReturn.Add("VdvProduct", _vdvProduct);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="validator">The validator object for this TariffEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 tarifID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tarifID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_areaList = new VarioSL.Entities.CollectionClasses.AreaListCollection();
			_areaList.SetContainingEntityInfo(this, "Tariff");

			_areaListElements = new VarioSL.Entities.CollectionClasses.AreaListElementCollection();
			_areaListElements.SetContainingEntityInfo(this, "Tariff");

			_attributes = new VarioSL.Entities.CollectionClasses.AttributeCollection();
			_attributes.SetContainingEntityInfo(this, "Tariff");

			_businessRule = new VarioSL.Entities.CollectionClasses.BusinessRuleCollection();
			_businessRule.SetContainingEntityInfo(this, "Tariff");

			_businessRuleCondition = new VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection();
			_businessRuleCondition.SetContainingEntityInfo(this, "Tariff");

			_businessRuleResult = new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection();
			_businessRuleResult.SetContainingEntityInfo(this, "Tariff");

			_calendars = new VarioSL.Entities.CollectionClasses.CalendarCollection();
			_calendars.SetContainingEntityInfo(this, "Tariff");

			_cardTickets = new VarioSL.Entities.CollectionClasses.CardTicketCollection();
			_cardTickets.SetContainingEntityInfo(this, "Tariff");

			_choices = new VarioSL.Entities.CollectionClasses.ChoiceCollection();
			_choices.SetContainingEntityInfo(this, "Tariff");

			_clientAdaptedLayoutObjects = new VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection();
			_clientAdaptedLayoutObjects.SetContainingEntityInfo(this, "Tariff");

			_etickets = new VarioSL.Entities.CollectionClasses.EticketCollection();
			_etickets.SetContainingEntityInfo(this, "Tariff");

			_externalCards = new VarioSL.Entities.CollectionClasses.ExternalCardCollection();
			_externalCards.SetContainingEntityInfo(this, "Tariff");

			_externalEfforts = new VarioSL.Entities.CollectionClasses.ExternalEffortCollection();
			_externalEfforts.SetContainingEntityInfo(this, "Tariff");

			_externalPackets = new VarioSL.Entities.CollectionClasses.ExternalPacketCollection();
			_externalPackets.SetContainingEntityInfo(this, "Tariff");

			_externalPacketefforts = new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection();
			_externalPacketefforts.SetContainingEntityInfo(this, "Tariff");

			_externalTypes = new VarioSL.Entities.CollectionClasses.ExternalTypeCollection();
			_externalTypes.SetContainingEntityInfo(this, "Tariff");

			_fareEvasionCategories = new VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection();
			_fareEvasionCategories.SetContainingEntityInfo(this, "Tariff");

			_fareEvasionReasons = new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection();
			_fareEvasionReasons.SetContainingEntityInfo(this, "Tariff");

			_fareMatrices = new VarioSL.Entities.CollectionClasses.FareMatrixCollection();
			_fareMatrices.SetContainingEntityInfo(this, "Tariff");

			_fareStageLists = new VarioSL.Entities.CollectionClasses.FareStageListCollection();
			_fareStageLists.SetContainingEntityInfo(this, "Tarif");

			_fareTables = new VarioSL.Entities.CollectionClasses.FareTableCollection();
			_fareTables.SetContainingEntityInfo(this, "Tariff");

			_guiDefs = new VarioSL.Entities.CollectionClasses.GuiDefCollection();
			_guiDefs.SetContainingEntityInfo(this, "Tariff");

			_keyAttributeTransforms = new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection();
			_keyAttributeTransforms.SetContainingEntityInfo(this, "Tariff");

			_layouts = new VarioSL.Entities.CollectionClasses.LayoutCollection();
			_layouts.SetContainingEntityInfo(this, "Tariff");

			_lineGroups = new VarioSL.Entities.CollectionClasses.LineGroupCollection();
			_lineGroups.SetContainingEntityInfo(this, "Tariff");

			_logos = new VarioSL.Entities.CollectionClasses.LogoCollection();
			_logos.SetContainingEntityInfo(this, "Tariff");

			_pageContentGroups = new VarioSL.Entities.CollectionClasses.PageContentGroupCollection();
			_pageContentGroups.SetContainingEntityInfo(this, "Tariff");

			_parameterAttributeValues = new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection();
			_parameterAttributeValues.SetContainingEntityInfo(this, "Tariff");

			_parameterFareStage = new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection();
			_parameterFareStage.SetContainingEntityInfo(this, "Tariff");

			_parameterTariff = new VarioSL.Entities.CollectionClasses.ParameterTariffCollection();
			_parameterTariff.SetContainingEntityInfo(this, "Tariff");

			_parameterTickets = new VarioSL.Entities.CollectionClasses.ParameterTicketCollection();
			_parameterTickets.SetContainingEntityInfo(this, "Tariff");

			_predefinedKeys = new VarioSL.Entities.CollectionClasses.PredefinedKeyCollection();
			_predefinedKeys.SetContainingEntityInfo(this, "Tariff");

			_primalKeys = new VarioSL.Entities.CollectionClasses.PrimalKeyCollection();
			_primalKeys.SetContainingEntityInfo(this, "Tariff");

			_printTexts = new VarioSL.Entities.CollectionClasses.PrintTextCollection();
			_printTexts.SetContainingEntityInfo(this, "Tariff");

			_routes = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_routes.SetContainingEntityInfo(this, "Tariff");

			_routeNames = new VarioSL.Entities.CollectionClasses.RouteNameCollection();
			_routeNames.SetContainingEntityInfo(this, "Tariff");

			_ruleCappings = new VarioSL.Entities.CollectionClasses.RuleCappingCollection();
			_ruleCappings.SetContainingEntityInfo(this, "Tariff");

			_serviceAllocations = new VarioSL.Entities.CollectionClasses.ServiceAllocationCollection();
			_serviceAllocations.SetContainingEntityInfo(this, "Tariff");

			_shortDistances = new VarioSL.Entities.CollectionClasses.ShortDistanceCollection();
			_shortDistances.SetContainingEntityInfo(this, "Tariff");

			_specialReceipts = new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection();
			_specialReceipts.SetContainingEntityInfo(this, "Tariff");

			_systemTexts = new VarioSL.Entities.CollectionClasses.SystemTextCollection();
			_systemTexts.SetContainingEntityInfo(this, "Tariff");

			_childTariffs = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_childTariffs.SetContainingEntityInfo(this, "BaseTariff");

			_matrixParentTariff = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_matrixParentTariff.SetContainingEntityInfo(this, "MatrixTariff");

			_tariffRelease = new VarioSL.Entities.CollectionClasses.TariffReleaseCollection();
			_tariffRelease.SetContainingEntityInfo(this, "Tariff");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "Tariff");

			_ticketGroups = new VarioSL.Entities.CollectionClasses.TicketGroupCollection();
			_ticketGroups.SetContainingEntityInfo(this, "Tariff");

			_translations = new VarioSL.Entities.CollectionClasses.TranslationCollection();
			_translations.SetContainingEntityInfo(this, "Tariff");

			_userKeys = new VarioSL.Entities.CollectionClasses.UserKeyCollection();
			_userKeys.SetContainingEntityInfo(this, "Tariff");

			_vdvKeySet = new VarioSL.Entities.CollectionClasses.VdvKeySetCollection();
			_vdvKeySet.SetContainingEntityInfo(this, "Tariff");

			_vdvLayout = new VarioSL.Entities.CollectionClasses.VdvLayoutCollection();
			_vdvLayout.SetContainingEntityInfo(this, "Tariff");

			_vdvProduct = new VarioSL.Entities.CollectionClasses.VdvProductCollection();
			_vdvProduct.SetContainingEntityInfo(this, "Tariff");
			_clientReturnsNewIfNotFound = false;
			_ownerClientReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_netReturnsNewIfNotFound = false;
			_baseTariffReturnsNewIfNotFound = false;
			_matrixTariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseTariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MatrixTariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubReleaseCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TarifID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFromTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTariffRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ClientTariffs", resetFKFields, new int[] { (int)TariffFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticTariffRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ownerClient</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOwnerClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ownerClient, new PropertyChangedEventHandler( OnOwnerClientPropertyChanged ), "OwnerClient", VarioSL.Entities.RelationClasses.StaticTariffRelations.ClientEntityUsingOwnerClientIDStatic, true, signalRelatedEntity, "OwnerClientTariffs", resetFKFields, new int[] { (int)TariffFieldIndex.OwnerClientID } );		
			_ownerClient = null;
		}
		
		/// <summary> setups the sync logic for member _ownerClient</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOwnerClient(IEntityCore relatedEntity)
		{
			if(_ownerClient!=relatedEntity)
			{		
				DesetupSyncOwnerClient(true, true);
				_ownerClient = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ownerClient, new PropertyChangedEventHandler( OnOwnerClientPropertyChanged ), "OwnerClient", VarioSL.Entities.RelationClasses.StaticTariffRelations.ClientEntityUsingOwnerClientIDStatic, true, ref _alreadyFetchedOwnerClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOwnerClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTariffRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "Tariffs", resetFKFields, new int[] { (int)TariffFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticTariffRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _net</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncNet(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticTariffRelations.NetEntityUsingNetIDStatic, true, signalRelatedEntity, "Tariffs", resetFKFields, new int[] { (int)TariffFieldIndex.NetID } );		
			_net = null;
		}
		
		/// <summary> setups the sync logic for member _net</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncNet(IEntityCore relatedEntity)
		{
			if(_net!=relatedEntity)
			{		
				DesetupSyncNet(true, true);
				_net = (NetEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _net, new PropertyChangedEventHandler( OnNetPropertyChanged ), "Net", VarioSL.Entities.RelationClasses.StaticTariffRelations.NetEntityUsingNetIDStatic, true, ref _alreadyFetchedNet, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnNetPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _baseTariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBaseTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _baseTariff, new PropertyChangedEventHandler( OnBaseTariffPropertyChanged ), "BaseTariff", VarioSL.Entities.RelationClasses.StaticTariffRelations.TariffEntityUsingTarifIDBaseTariffIDStatic, true, signalRelatedEntity, "ChildTariffs", resetFKFields, new int[] { (int)TariffFieldIndex.BaseTariffID } );		
			_baseTariff = null;
		}
		
		/// <summary> setups the sync logic for member _baseTariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBaseTariff(IEntityCore relatedEntity)
		{
			if(_baseTariff!=relatedEntity)
			{		
				DesetupSyncBaseTariff(true, true);
				_baseTariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _baseTariff, new PropertyChangedEventHandler( OnBaseTariffPropertyChanged ), "BaseTariff", VarioSL.Entities.RelationClasses.StaticTariffRelations.TariffEntityUsingTarifIDBaseTariffIDStatic, true, ref _alreadyFetchedBaseTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBaseTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _matrixTariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMatrixTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _matrixTariff, new PropertyChangedEventHandler( OnMatrixTariffPropertyChanged ), "MatrixTariff", VarioSL.Entities.RelationClasses.StaticTariffRelations.TariffEntityUsingTarifIDMatrixTariffIDStatic, true, signalRelatedEntity, "MatrixParentTariff", resetFKFields, new int[] { (int)TariffFieldIndex.MatrixTariffID } );		
			_matrixTariff = null;
		}
		
		/// <summary> setups the sync logic for member _matrixTariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMatrixTariff(IEntityCore relatedEntity)
		{
			if(_matrixTariff!=relatedEntity)
			{		
				DesetupSyncMatrixTariff(true, true);
				_matrixTariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _matrixTariff, new PropertyChangedEventHandler( OnMatrixTariffPropertyChanged ), "MatrixTariff", VarioSL.Entities.RelationClasses.StaticTariffRelations.TariffEntityUsingTarifIDMatrixTariffIDStatic, true, ref _alreadyFetchedMatrixTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMatrixTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tarifID">PK value for Tariff which data should be fetched into this Tariff object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 tarifID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TariffFieldIndex.TarifID].ForcedCurrentValueWrite(tarifID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTariffDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TariffEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TariffRelations Relations
		{
			get	{ return new TariffRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaList
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListCollection(), (IEntityRelation)GetRelationsForField("AreaList")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.AreaListEntity, 0, null, null, null, "AreaList", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaListElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListElements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListElementCollection(), (IEntityRelation)GetRelationsForField("AreaListElements")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.AreaListElementEntity, 0, null, null, null, "AreaListElements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attribute' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeCollection(), (IEntityRelation)GetRelationsForField("Attributes")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.AttributeEntity, 0, null, null, null, "Attributes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRule
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRule")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRule", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleCondition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleCondition
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleCondition")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.BusinessRuleConditionEntity, 0, null, null, null, "BusinessRuleCondition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleResult
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleResult")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, 0, null, null, null, "BusinessRuleResult", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Calendar' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCalendars
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CalendarCollection(), (IEntityRelation)GetRelationsForField("Calendars")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.CalendarEntity, 0, null, null, null, "Calendars", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CardTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCardTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardTicketCollection(), (IEntityRelation)GetRelationsForField("CardTickets")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.CardTicketEntity, 0, null, null, null, "CardTickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Choice' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChoices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ChoiceCollection(), (IEntityRelation)GetRelationsForField("Choices")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ChoiceEntity, 0, null, null, null, "Choices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientAdaptedLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientAdaptedLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ClientAdaptedLayoutObjects")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity, 0, null, null, null, "ClientAdaptedLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Eticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEtickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EticketCollection(), (IEntityRelation)GetRelationsForField("Etickets")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.EticketEntity, 0, null, null, null, "Etickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalCard' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalCards
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalCardCollection(), (IEntityRelation)GetRelationsForField("ExternalCards")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ExternalCardEntity, 0, null, null, null, "ExternalCards", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalEffort' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalEfforts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalEfforts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ExternalEffortEntity, 0, null, null, null, "ExternalEfforts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPackets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketCollection(), (IEntityRelation)GetRelationsForField("ExternalPackets")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEntity, 0, null, null, null, "ExternalPackets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalPacketEffort' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalPacketefforts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection(), (IEntityRelation)GetRelationsForField("ExternalPacketefforts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ExternalPacketEffortEntity, 0, null, null, null, "ExternalPacketefforts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ExternalType' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathExternalTypes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ExternalTypeCollection(), (IEntityRelation)GetRelationsForField("ExternalTypes")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ExternalTypeEntity, 0, null, null, null, "ExternalTypes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection(), (IEntityRelation)GetRelationsForField("FareEvasionCategories")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.FareEvasionCategoryEntity, 0, null, null, null, "FareEvasionCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareEvasionReason' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareEvasionReasons
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection(), (IEntityRelation)GetRelationsForField("FareEvasionReasons")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.FareEvasionReasonEntity, 0, null, null, null, "FareEvasionReasons", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrix' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixCollection(), (IEntityRelation)GetRelationsForField("FareMatrices")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntity, 0, null, null, null, "FareMatrices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareStageList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareStageLists
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareStageListCollection(), (IEntityRelation)GetRelationsForField("FareStageLists")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.FareStageListEntity, 0, null, null, null, "FareStageLists", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTables
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTables")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTables", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GuiDef' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGuiDefs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.GuiDefCollection(), (IEntityRelation)GetRelationsForField("GuiDefs")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.GuiDefEntity, 0, null, null, null, "GuiDefs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'KeyAttributeTransfrom' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathKeyAttributeTransforms
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection(), (IEntityRelation)GetRelationsForField("KeyAttributeTransforms")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, 0, null, null, null, "KeyAttributeTransforms", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Layout' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLayouts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LayoutCollection(), (IEntityRelation)GetRelationsForField("Layouts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.LayoutEntity, 0, null, null, null, "Layouts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'LineGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLineGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineGroupCollection(), (IEntityRelation)GetRelationsForField("LineGroups")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.LineGroupEntity, 0, null, null, null, "LineGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Logo' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLogos
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LogoCollection(), (IEntityRelation)GetRelationsForField("Logos")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.LogoEntity, 0, null, null, null, "Logos", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PageContentGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageContentGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PageContentGroupCollection(), (IEntityRelation)GetRelationsForField("PageContentGroups")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.PageContentGroupEntity, 0, null, null, null, "PageContentGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterAttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterAttributeValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection(), (IEntityRelation)GetRelationsForField("ParameterAttributeValues")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ParameterAttributeValueEntity, 0, null, null, null, "ParameterAttributeValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterFareStage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterFareStage
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterFareStageCollection(), (IEntityRelation)GetRelationsForField("ParameterFareStage")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ParameterFareStageEntity, 0, null, null, null, "ParameterFareStage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterTariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterTariff
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterTariffCollection(), (IEntityRelation)GetRelationsForField("ParameterTariff")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ParameterTariffEntity, 0, null, null, null, "ParameterTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterTicketCollection(), (IEntityRelation)GetRelationsForField("ParameterTickets")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ParameterTicketEntity, 0, null, null, null, "ParameterTickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PredefinedKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPredefinedKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PredefinedKeyCollection(), (IEntityRelation)GetRelationsForField("PredefinedKeys")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, 0, null, null, null, "PredefinedKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrimalKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrimalKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrimalKeyCollection(), (IEntityRelation)GetRelationsForField("PrimalKeys")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.PrimalKeyEntity, 0, null, null, null, "PrimalKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PrintText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPrintTexts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PrintTextCollection(), (IEntityRelation)GetRelationsForField("PrintTexts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.PrintTextEntity, 0, null, null, null, "PrintTexts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutes
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("Routes")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "Routes", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RouteName' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteNames
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteNameCollection(), (IEntityRelation)GetRelationsForField("RouteNames")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.RouteNameEntity, 0, null, null, null, "RouteNames", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleCapping' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleCappings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleCappingCollection(), (IEntityRelation)GetRelationsForField("RuleCappings")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.RuleCappingEntity, 0, null, null, null, "RuleCappings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ServiceAllocation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathServiceAllocations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ServiceAllocationCollection(), (IEntityRelation)GetRelationsForField("ServiceAllocations")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ServiceAllocationEntity, 0, null, null, null, "ServiceAllocations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ShortDistance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathShortDistances
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ShortDistanceCollection(), (IEntityRelation)GetRelationsForField("ShortDistances")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ShortDistanceEntity, 0, null, null, null, "ShortDistances", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SpecialReceipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSpecialReceipts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection(), (IEntityRelation)GetRelationsForField("SpecialReceipts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.SpecialReceiptEntity, 0, null, null, null, "SpecialReceipts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SystemText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemTexts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SystemTextCollection(), (IEntityRelation)GetRelationsForField("SystemTexts")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.SystemTextEntity, 0, null, null, null, "SystemTexts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathChildTariffs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("ChildTariffs")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "ChildTariffs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMatrixParentTariff
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("MatrixParentTariff")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "MatrixParentTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TariffRelease' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffRelease
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffReleaseCollection(), (IEntityRelation)GetRelationsForField("TariffRelease")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TariffReleaseEntity, 0, null, null, null, "TariffRelease", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketGroupCollection(), (IEntityRelation)GetRelationsForField("TicketGroups")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TicketGroupEntity, 0, null, null, null, "TicketGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Translation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTranslations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TranslationCollection(), (IEntityRelation)GetRelationsForField("Translations")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TranslationEntity, 0, null, null, null, "Translations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UserKey' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserKeys
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UserKeyCollection(), (IEntityRelation)GetRelationsForField("UserKeys")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.UserKeyEntity, 0, null, null, null, "UserKeys", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvKeySet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvKeySet
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvKeySetCollection(), (IEntityRelation)GetRelationsForField("VdvKeySet")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.VdvKeySetEntity, 0, null, null, null, "VdvKeySet", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayout' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutCollection(), (IEntityRelation)GetRelationsForField("VdvLayout")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.VdvLayoutEntity, 0, null, null, null, "VdvLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvProduct
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvProductCollection(), (IEntityRelation)GetRelationsForField("VdvProduct")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.VdvProductEntity, 0, null, null, null, "VdvProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOwnerClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("OwnerClient")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "OwnerClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Net'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathNet
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.NetCollection(), (IEntityRelation)GetRelationsForField("Net")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.NetEntity, 0, null, null, null, "Net", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBaseTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("BaseTariff")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "BaseTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMatrixTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("MatrixTariff")[0], (int)VarioSL.Entities.EntityType.TariffEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "MatrixTariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BaseTariffID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."BASETARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseTariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.BaseTariffID, false); }
			set	{ SetValue((int)TariffFieldIndex.BaseTariffID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.ClientID, false); }
			set	{ SetValue((int)TariffFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)TariffFieldIndex.Description, true); }
			set	{ SetValue((int)TariffFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)TariffFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The ExportState property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."EXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExportState
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.ExportState, false); }
			set	{ SetValue((int)TariffFieldIndex.ExportState, value, true); }
		}

		/// <summary> The ExternalVersion property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."EXTERNALVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalVersion
		{
			get { return (System.String)GetValue((int)TariffFieldIndex.ExternalVersion, true); }
			set	{ SetValue((int)TariffFieldIndex.ExternalVersion, value, true); }
		}

		/// <summary> The MatrixTariffID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."MATRIXTARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MatrixTariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.MatrixTariffID, false); }
			set	{ SetValue((int)TariffFieldIndex.MatrixTariffID, value, true); }
		}

		/// <summary> The NetID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."NETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> NetID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.NetID, false); }
			set	{ SetValue((int)TariffFieldIndex.NetID, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.OwnerClientID, false); }
			set	{ SetValue((int)TariffFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The ReleaseCounter property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."RELEASECOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReleaseCounter
		{
			get { return (Nullable<System.Int64>)GetValue((int)TariffFieldIndex.ReleaseCounter, false); }
			set	{ SetValue((int)TariffFieldIndex.ReleaseCounter, value, true); }
		}

		/// <summary> The State property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ArchiveState State
		{
			get { return (VarioSL.Entities.Enumerations.ArchiveState)GetValue((int)TariffFieldIndex.State, true); }
			set	{ SetValue((int)TariffFieldIndex.State, value, true); }
		}

		/// <summary> The SubReleaseCounter property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."SUBRELEASECOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SubReleaseCounter
		{
			get { return (System.Int64)GetValue((int)TariffFieldIndex.SubReleaseCounter, true); }
			set	{ SetValue((int)TariffFieldIndex.SubReleaseCounter, value, true); }
		}

		/// <summary> The TarifID property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TarifID
		{
			get { return (System.Int64)GetValue((int)TariffFieldIndex.TarifID, true); }
			set	{ SetValue((int)TariffFieldIndex.TarifID, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)TariffFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)TariffFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidFromTime property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."VALIDFROMTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ValidFromTime
		{
			get { return (Nullable<System.Int32>)GetValue((int)TariffFieldIndex.ValidFromTime, false); }
			set	{ SetValue((int)TariffFieldIndex.ValidFromTime, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ValidUntil
		{
			get { return (Nullable<System.DateTime>)GetValue((int)TariffFieldIndex.ValidUntil, false); }
			set	{ SetValue((int)TariffFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> The Version property of the Entity Tariff<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TARIF"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Version
		{
			get { return (System.Int64)GetValue((int)TariffFieldIndex.Version, true); }
			set	{ SetValue((int)TariffFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaList()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection AreaList
		{
			get	{ return GetMultiAreaList(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaList. When set to true, AreaList is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaList is accessed. You can always execute/ a forced fetch by calling GetMultiAreaList(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaList
		{
			get	{ return _alwaysFetchAreaList; }
			set	{ _alwaysFetchAreaList = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaList already has been fetched. Setting this property to false when AreaList has been fetched
		/// will clear the AreaList collection well. Setting this property to true while AreaList hasn't been fetched disables lazy loading for AreaList</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaList
		{
			get { return _alreadyFetchedAreaList;}
			set 
			{
				if(_alreadyFetchedAreaList && !value && (_areaList != null))
				{
					_areaList.Clear();
				}
				_alreadyFetchedAreaList = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaListElements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection AreaListElements
		{
			get	{ return GetMultiAreaListElements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListElements. When set to true, AreaListElements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListElements is accessed. You can always execute/ a forced fetch by calling GetMultiAreaListElements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListElements
		{
			get	{ return _alwaysFetchAreaListElements; }
			set	{ _alwaysFetchAreaListElements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListElements already has been fetched. Setting this property to false when AreaListElements has been fetched
		/// will clear the AreaListElements collection well. Setting this property to true while AreaListElements hasn't been fetched disables lazy loading for AreaListElements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListElements
		{
			get { return _alreadyFetchedAreaListElements;}
			set 
			{
				if(_alreadyFetchedAreaListElements && !value && (_areaListElements != null))
				{
					_areaListElements.Clear();
				}
				_alreadyFetchedAreaListElements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeCollection Attributes
		{
			get	{ return GetMultiAttributes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Attributes. When set to true, Attributes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Attributes is accessed. You can always execute/ a forced fetch by calling GetMultiAttributes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributes
		{
			get	{ return _alwaysFetchAttributes; }
			set	{ _alwaysFetchAttributes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Attributes already has been fetched. Setting this property to false when Attributes has been fetched
		/// will clear the Attributes collection well. Setting this property to true while Attributes hasn't been fetched disables lazy loading for Attributes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributes
		{
			get { return _alreadyFetchedAttributes;}
			set 
			{
				if(_alreadyFetchedAttributes && !value && (_attributes != null))
				{
					_attributes.Clear();
				}
				_alreadyFetchedAttributes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRule()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleCollection BusinessRule
		{
			get	{ return GetMultiBusinessRule(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRule. When set to true, BusinessRule is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRule is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRule(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRule
		{
			get	{ return _alwaysFetchBusinessRule; }
			set	{ _alwaysFetchBusinessRule = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRule already has been fetched. Setting this property to false when BusinessRule has been fetched
		/// will clear the BusinessRule collection well. Setting this property to true while BusinessRule hasn't been fetched disables lazy loading for BusinessRule</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRule
		{
			get { return _alreadyFetchedBusinessRule;}
			set 
			{
				if(_alreadyFetchedBusinessRule && !value && (_businessRule != null))
				{
					_businessRule.Clear();
				}
				_alreadyFetchedBusinessRule = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleCondition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection BusinessRuleCondition
		{
			get	{ return GetMultiBusinessRuleCondition(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleCondition. When set to true, BusinessRuleCondition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleCondition is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleCondition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleCondition
		{
			get	{ return _alwaysFetchBusinessRuleCondition; }
			set	{ _alwaysFetchBusinessRuleCondition = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleCondition already has been fetched. Setting this property to false when BusinessRuleCondition has been fetched
		/// will clear the BusinessRuleCondition collection well. Setting this property to true while BusinessRuleCondition hasn't been fetched disables lazy loading for BusinessRuleCondition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleCondition
		{
			get { return _alreadyFetchedBusinessRuleCondition;}
			set 
			{
				if(_alreadyFetchedBusinessRuleCondition && !value && (_businessRuleCondition != null))
				{
					_businessRuleCondition.Clear();
				}
				_alreadyFetchedBusinessRuleCondition = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleResult()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection BusinessRuleResult
		{
			get	{ return GetMultiBusinessRuleResult(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleResult. When set to true, BusinessRuleResult is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleResult is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleResult(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleResult
		{
			get	{ return _alwaysFetchBusinessRuleResult; }
			set	{ _alwaysFetchBusinessRuleResult = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleResult already has been fetched. Setting this property to false when BusinessRuleResult has been fetched
		/// will clear the BusinessRuleResult collection well. Setting this property to true while BusinessRuleResult hasn't been fetched disables lazy loading for BusinessRuleResult</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleResult
		{
			get { return _alreadyFetchedBusinessRuleResult;}
			set 
			{
				if(_alreadyFetchedBusinessRuleResult && !value && (_businessRuleResult != null))
				{
					_businessRuleResult.Clear();
				}
				_alreadyFetchedBusinessRuleResult = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CalendarEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCalendars()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CalendarCollection Calendars
		{
			get	{ return GetMultiCalendars(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Calendars. When set to true, Calendars is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Calendars is accessed. You can always execute/ a forced fetch by calling GetMultiCalendars(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCalendars
		{
			get	{ return _alwaysFetchCalendars; }
			set	{ _alwaysFetchCalendars = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Calendars already has been fetched. Setting this property to false when Calendars has been fetched
		/// will clear the Calendars collection well. Setting this property to true while Calendars hasn't been fetched disables lazy loading for Calendars</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCalendars
		{
			get { return _alreadyFetchedCalendars;}
			set 
			{
				if(_alreadyFetchedCalendars && !value && (_calendars != null))
				{
					_calendars.Clear();
				}
				_alreadyFetchedCalendars = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CardTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCardTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CardTicketCollection CardTickets
		{
			get	{ return GetMultiCardTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CardTickets. When set to true, CardTickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CardTickets is accessed. You can always execute/ a forced fetch by calling GetMultiCardTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCardTickets
		{
			get	{ return _alwaysFetchCardTickets; }
			set	{ _alwaysFetchCardTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CardTickets already has been fetched. Setting this property to false when CardTickets has been fetched
		/// will clear the CardTickets collection well. Setting this property to true while CardTickets hasn't been fetched disables lazy loading for CardTickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCardTickets
		{
			get { return _alreadyFetchedCardTickets;}
			set 
			{
				if(_alreadyFetchedCardTickets && !value && (_cardTickets != null))
				{
					_cardTickets.Clear();
				}
				_alreadyFetchedCardTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ChoiceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChoices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ChoiceCollection Choices
		{
			get	{ return GetMultiChoices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Choices. When set to true, Choices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Choices is accessed. You can always execute/ a forced fetch by calling GetMultiChoices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChoices
		{
			get	{ return _alwaysFetchChoices; }
			set	{ _alwaysFetchChoices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Choices already has been fetched. Setting this property to false when Choices has been fetched
		/// will clear the Choices collection well. Setting this property to true while Choices hasn't been fetched disables lazy loading for Choices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChoices
		{
			get { return _alreadyFetchedChoices;}
			set 
			{
				if(_alreadyFetchedChoices && !value && (_choices != null))
				{
					_choices.Clear();
				}
				_alreadyFetchedChoices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ClientAdaptedLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientAdaptedLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClientAdaptedLayoutObjectCollection ClientAdaptedLayoutObjects
		{
			get	{ return GetMultiClientAdaptedLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientAdaptedLayoutObjects. When set to true, ClientAdaptedLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientAdaptedLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiClientAdaptedLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientAdaptedLayoutObjects
		{
			get	{ return _alwaysFetchClientAdaptedLayoutObjects; }
			set	{ _alwaysFetchClientAdaptedLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientAdaptedLayoutObjects already has been fetched. Setting this property to false when ClientAdaptedLayoutObjects has been fetched
		/// will clear the ClientAdaptedLayoutObjects collection well. Setting this property to true while ClientAdaptedLayoutObjects hasn't been fetched disables lazy loading for ClientAdaptedLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientAdaptedLayoutObjects
		{
			get { return _alreadyFetchedClientAdaptedLayoutObjects;}
			set 
			{
				if(_alreadyFetchedClientAdaptedLayoutObjects && !value && (_clientAdaptedLayoutObjects != null))
				{
					_clientAdaptedLayoutObjects.Clear();
				}
				_alreadyFetchedClientAdaptedLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEtickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EticketCollection Etickets
		{
			get	{ return GetMultiEtickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Etickets. When set to true, Etickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Etickets is accessed. You can always execute/ a forced fetch by calling GetMultiEtickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEtickets
		{
			get	{ return _alwaysFetchEtickets; }
			set	{ _alwaysFetchEtickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Etickets already has been fetched. Setting this property to false when Etickets has been fetched
		/// will clear the Etickets collection well. Setting this property to true while Etickets hasn't been fetched disables lazy loading for Etickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEtickets
		{
			get { return _alreadyFetchedEtickets;}
			set 
			{
				if(_alreadyFetchedEtickets && !value && (_etickets != null))
				{
					_etickets.Clear();
				}
				_alreadyFetchedEtickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalCardEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalCards()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalCardCollection ExternalCards
		{
			get	{ return GetMultiExternalCards(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalCards. When set to true, ExternalCards is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalCards is accessed. You can always execute/ a forced fetch by calling GetMultiExternalCards(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalCards
		{
			get	{ return _alwaysFetchExternalCards; }
			set	{ _alwaysFetchExternalCards = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalCards already has been fetched. Setting this property to false when ExternalCards has been fetched
		/// will clear the ExternalCards collection well. Setting this property to true while ExternalCards hasn't been fetched disables lazy loading for ExternalCards</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalCards
		{
			get { return _alreadyFetchedExternalCards;}
			set 
			{
				if(_alreadyFetchedExternalCards && !value && (_externalCards != null))
				{
					_externalCards.Clear();
				}
				_alreadyFetchedExternalCards = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalEffortEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalEfforts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalEffortCollection ExternalEfforts
		{
			get	{ return GetMultiExternalEfforts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalEfforts. When set to true, ExternalEfforts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalEfforts is accessed. You can always execute/ a forced fetch by calling GetMultiExternalEfforts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalEfforts
		{
			get	{ return _alwaysFetchExternalEfforts; }
			set	{ _alwaysFetchExternalEfforts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalEfforts already has been fetched. Setting this property to false when ExternalEfforts has been fetched
		/// will clear the ExternalEfforts collection well. Setting this property to true while ExternalEfforts hasn't been fetched disables lazy loading for ExternalEfforts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalEfforts
		{
			get { return _alreadyFetchedExternalEfforts;}
			set 
			{
				if(_alreadyFetchedExternalEfforts && !value && (_externalEfforts != null))
				{
					_externalEfforts.Clear();
				}
				_alreadyFetchedExternalEfforts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalPacketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalPackets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketCollection ExternalPackets
		{
			get	{ return GetMultiExternalPackets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPackets. When set to true, ExternalPackets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPackets is accessed. You can always execute/ a forced fetch by calling GetMultiExternalPackets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPackets
		{
			get	{ return _alwaysFetchExternalPackets; }
			set	{ _alwaysFetchExternalPackets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPackets already has been fetched. Setting this property to false when ExternalPackets has been fetched
		/// will clear the ExternalPackets collection well. Setting this property to true while ExternalPackets hasn't been fetched disables lazy loading for ExternalPackets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPackets
		{
			get { return _alreadyFetchedExternalPackets;}
			set 
			{
				if(_alreadyFetchedExternalPackets && !value && (_externalPackets != null))
				{
					_externalPackets.Clear();
				}
				_alreadyFetchedExternalPackets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalPacketEffortEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalPacketefforts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalPacketEffortCollection ExternalPacketefforts
		{
			get	{ return GetMultiExternalPacketefforts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalPacketefforts. When set to true, ExternalPacketefforts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalPacketefforts is accessed. You can always execute/ a forced fetch by calling GetMultiExternalPacketefforts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalPacketefforts
		{
			get	{ return _alwaysFetchExternalPacketefforts; }
			set	{ _alwaysFetchExternalPacketefforts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalPacketefforts already has been fetched. Setting this property to false when ExternalPacketefforts has been fetched
		/// will clear the ExternalPacketefforts collection well. Setting this property to true while ExternalPacketefforts hasn't been fetched disables lazy loading for ExternalPacketefforts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalPacketefforts
		{
			get { return _alreadyFetchedExternalPacketefforts;}
			set 
			{
				if(_alreadyFetchedExternalPacketefforts && !value && (_externalPacketefforts != null))
				{
					_externalPacketefforts.Clear();
				}
				_alreadyFetchedExternalPacketefforts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ExternalTypeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiExternalTypes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ExternalTypeCollection ExternalTypes
		{
			get	{ return GetMultiExternalTypes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ExternalTypes. When set to true, ExternalTypes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ExternalTypes is accessed. You can always execute/ a forced fetch by calling GetMultiExternalTypes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchExternalTypes
		{
			get	{ return _alwaysFetchExternalTypes; }
			set	{ _alwaysFetchExternalTypes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ExternalTypes already has been fetched. Setting this property to false when ExternalTypes has been fetched
		/// will clear the ExternalTypes collection well. Setting this property to true while ExternalTypes hasn't been fetched disables lazy loading for ExternalTypes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedExternalTypes
		{
			get { return _alreadyFetchedExternalTypes;}
			set 
			{
				if(_alreadyFetchedExternalTypes && !value && (_externalTypes != null))
				{
					_externalTypes.Clear();
				}
				_alreadyFetchedExternalTypes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionCategoryCollection FareEvasionCategories
		{
			get	{ return GetMultiFareEvasionCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionCategories. When set to true, FareEvasionCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionCategories is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionCategories
		{
			get	{ return _alwaysFetchFareEvasionCategories; }
			set	{ _alwaysFetchFareEvasionCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionCategories already has been fetched. Setting this property to false when FareEvasionCategories has been fetched
		/// will clear the FareEvasionCategories collection well. Setting this property to true while FareEvasionCategories hasn't been fetched disables lazy loading for FareEvasionCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionCategories
		{
			get { return _alreadyFetchedFareEvasionCategories;}
			set 
			{
				if(_alreadyFetchedFareEvasionCategories && !value && (_fareEvasionCategories != null))
				{
					_fareEvasionCategories.Clear();
				}
				_alreadyFetchedFareEvasionCategories = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareEvasionReasonEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareEvasionReasons()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareEvasionReasonCollection FareEvasionReasons
		{
			get	{ return GetMultiFareEvasionReasons(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareEvasionReasons. When set to true, FareEvasionReasons is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareEvasionReasons is accessed. You can always execute/ a forced fetch by calling GetMultiFareEvasionReasons(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareEvasionReasons
		{
			get	{ return _alwaysFetchFareEvasionReasons; }
			set	{ _alwaysFetchFareEvasionReasons = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareEvasionReasons already has been fetched. Setting this property to false when FareEvasionReasons has been fetched
		/// will clear the FareEvasionReasons collection well. Setting this property to true while FareEvasionReasons hasn't been fetched disables lazy loading for FareEvasionReasons</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareEvasionReasons
		{
			get { return _alreadyFetchedFareEvasionReasons;}
			set 
			{
				if(_alreadyFetchedFareEvasionReasons && !value && (_fareEvasionReasons != null))
				{
					_fareEvasionReasons.Clear();
				}
				_alreadyFetchedFareEvasionReasons = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareMatrixEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixCollection FareMatrices
		{
			get	{ return GetMultiFareMatrices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrices. When set to true, FareMatrices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrices is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrices
		{
			get	{ return _alwaysFetchFareMatrices; }
			set	{ _alwaysFetchFareMatrices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrices already has been fetched. Setting this property to false when FareMatrices has been fetched
		/// will clear the FareMatrices collection well. Setting this property to true while FareMatrices hasn't been fetched disables lazy loading for FareMatrices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrices
		{
			get { return _alreadyFetchedFareMatrices;}
			set 
			{
				if(_alreadyFetchedFareMatrices && !value && (_fareMatrices != null))
				{
					_fareMatrices.Clear();
				}
				_alreadyFetchedFareMatrices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareStageListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareStageLists()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareStageListCollection FareStageLists
		{
			get	{ return GetMultiFareStageLists(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareStageLists. When set to true, FareStageLists is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareStageLists is accessed. You can always execute/ a forced fetch by calling GetMultiFareStageLists(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareStageLists
		{
			get	{ return _alwaysFetchFareStageLists; }
			set	{ _alwaysFetchFareStageLists = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareStageLists already has been fetched. Setting this property to false when FareStageLists has been fetched
		/// will clear the FareStageLists collection well. Setting this property to true while FareStageLists hasn't been fetched disables lazy loading for FareStageLists</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareStageLists
		{
			get { return _alreadyFetchedFareStageLists;}
			set 
			{
				if(_alreadyFetchedFareStageLists && !value && (_fareStageLists != null))
				{
					_fareStageLists.Clear();
				}
				_alreadyFetchedFareStageLists = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTables()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection FareTables
		{
			get	{ return GetMultiFareTables(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTables. When set to true, FareTables is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTables is accessed. You can always execute/ a forced fetch by calling GetMultiFareTables(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTables
		{
			get	{ return _alwaysFetchFareTables; }
			set	{ _alwaysFetchFareTables = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTables already has been fetched. Setting this property to false when FareTables has been fetched
		/// will clear the FareTables collection well. Setting this property to true while FareTables hasn't been fetched disables lazy loading for FareTables</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTables
		{
			get { return _alreadyFetchedFareTables;}
			set 
			{
				if(_alreadyFetchedFareTables && !value && (_fareTables != null))
				{
					_fareTables.Clear();
				}
				_alreadyFetchedFareTables = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGuiDefs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GuiDefs
		{
			get	{ return GetMultiGuiDefs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GuiDefs. When set to true, GuiDefs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GuiDefs is accessed. You can always execute/ a forced fetch by calling GetMultiGuiDefs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGuiDefs
		{
			get	{ return _alwaysFetchGuiDefs; }
			set	{ _alwaysFetchGuiDefs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GuiDefs already has been fetched. Setting this property to false when GuiDefs has been fetched
		/// will clear the GuiDefs collection well. Setting this property to true while GuiDefs hasn't been fetched disables lazy loading for GuiDefs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGuiDefs
		{
			get { return _alreadyFetchedGuiDefs;}
			set 
			{
				if(_alreadyFetchedGuiDefs && !value && (_guiDefs != null))
				{
					_guiDefs.Clear();
				}
				_alreadyFetchedGuiDefs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'KeyAttributeTransfromEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiKeyAttributeTransforms()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.KeyAttributeTransfromCollection KeyAttributeTransforms
		{
			get	{ return GetMultiKeyAttributeTransforms(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for KeyAttributeTransforms. When set to true, KeyAttributeTransforms is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time KeyAttributeTransforms is accessed. You can always execute/ a forced fetch by calling GetMultiKeyAttributeTransforms(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchKeyAttributeTransforms
		{
			get	{ return _alwaysFetchKeyAttributeTransforms; }
			set	{ _alwaysFetchKeyAttributeTransforms = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property KeyAttributeTransforms already has been fetched. Setting this property to false when KeyAttributeTransforms has been fetched
		/// will clear the KeyAttributeTransforms collection well. Setting this property to true while KeyAttributeTransforms hasn't been fetched disables lazy loading for KeyAttributeTransforms</summary>
		[Browsable(false)]
		public bool AlreadyFetchedKeyAttributeTransforms
		{
			get { return _alreadyFetchedKeyAttributeTransforms;}
			set 
			{
				if(_alreadyFetchedKeyAttributeTransforms && !value && (_keyAttributeTransforms != null))
				{
					_keyAttributeTransforms.Clear();
				}
				_alreadyFetchedKeyAttributeTransforms = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LayoutEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLayouts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LayoutCollection Layouts
		{
			get	{ return GetMultiLayouts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Layouts. When set to true, Layouts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Layouts is accessed. You can always execute/ a forced fetch by calling GetMultiLayouts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLayouts
		{
			get	{ return _alwaysFetchLayouts; }
			set	{ _alwaysFetchLayouts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Layouts already has been fetched. Setting this property to false when Layouts has been fetched
		/// will clear the Layouts collection well. Setting this property to true while Layouts hasn't been fetched disables lazy loading for Layouts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLayouts
		{
			get { return _alreadyFetchedLayouts;}
			set 
			{
				if(_alreadyFetchedLayouts && !value && (_layouts != null))
				{
					_layouts.Clear();
				}
				_alreadyFetchedLayouts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LineGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLineGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineGroupCollection LineGroups
		{
			get	{ return GetMultiLineGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LineGroups. When set to true, LineGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LineGroups is accessed. You can always execute/ a forced fetch by calling GetMultiLineGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLineGroups
		{
			get	{ return _alwaysFetchLineGroups; }
			set	{ _alwaysFetchLineGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property LineGroups already has been fetched. Setting this property to false when LineGroups has been fetched
		/// will clear the LineGroups collection well. Setting this property to true while LineGroups hasn't been fetched disables lazy loading for LineGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLineGroups
		{
			get { return _alreadyFetchedLineGroups;}
			set 
			{
				if(_alreadyFetchedLineGroups && !value && (_lineGroups != null))
				{
					_lineGroups.Clear();
				}
				_alreadyFetchedLineGroups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'LogoEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLogos()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LogoCollection Logos
		{
			get	{ return GetMultiLogos(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Logos. When set to true, Logos is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Logos is accessed. You can always execute/ a forced fetch by calling GetMultiLogos(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLogos
		{
			get	{ return _alwaysFetchLogos; }
			set	{ _alwaysFetchLogos = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Logos already has been fetched. Setting this property to false when Logos has been fetched
		/// will clear the Logos collection well. Setting this property to true while Logos hasn't been fetched disables lazy loading for Logos</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLogos
		{
			get { return _alreadyFetchedLogos;}
			set 
			{
				if(_alreadyFetchedLogos && !value && (_logos != null))
				{
					_logos.Clear();
				}
				_alreadyFetchedLogos = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PageContentGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPageContentGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PageContentGroupCollection PageContentGroups
		{
			get	{ return GetMultiPageContentGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PageContentGroups. When set to true, PageContentGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageContentGroups is accessed. You can always execute/ a forced fetch by calling GetMultiPageContentGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageContentGroups
		{
			get	{ return _alwaysFetchPageContentGroups; }
			set	{ _alwaysFetchPageContentGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageContentGroups already has been fetched. Setting this property to false when PageContentGroups has been fetched
		/// will clear the PageContentGroups collection well. Setting this property to true while PageContentGroups hasn't been fetched disables lazy loading for PageContentGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageContentGroups
		{
			get { return _alreadyFetchedPageContentGroups;}
			set 
			{
				if(_alreadyFetchedPageContentGroups && !value && (_pageContentGroups != null))
				{
					_pageContentGroups.Clear();
				}
				_alreadyFetchedPageContentGroups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterAttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterAttributeValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterAttributeValueCollection ParameterAttributeValues
		{
			get	{ return GetMultiParameterAttributeValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterAttributeValues. When set to true, ParameterAttributeValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterAttributeValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterAttributeValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterAttributeValues
		{
			get	{ return _alwaysFetchParameterAttributeValues; }
			set	{ _alwaysFetchParameterAttributeValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterAttributeValues already has been fetched. Setting this property to false when ParameterAttributeValues has been fetched
		/// will clear the ParameterAttributeValues collection well. Setting this property to true while ParameterAttributeValues hasn't been fetched disables lazy loading for ParameterAttributeValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterAttributeValues
		{
			get { return _alreadyFetchedParameterAttributeValues;}
			set 
			{
				if(_alreadyFetchedParameterAttributeValues && !value && (_parameterAttributeValues != null))
				{
					_parameterAttributeValues.Clear();
				}
				_alreadyFetchedParameterAttributeValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterFareStageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterFareStage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterFareStageCollection ParameterFareStage
		{
			get	{ return GetMultiParameterFareStage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterFareStage. When set to true, ParameterFareStage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterFareStage is accessed. You can always execute/ a forced fetch by calling GetMultiParameterFareStage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterFareStage
		{
			get	{ return _alwaysFetchParameterFareStage; }
			set	{ _alwaysFetchParameterFareStage = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterFareStage already has been fetched. Setting this property to false when ParameterFareStage has been fetched
		/// will clear the ParameterFareStage collection well. Setting this property to true while ParameterFareStage hasn't been fetched disables lazy loading for ParameterFareStage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterFareStage
		{
			get { return _alreadyFetchedParameterFareStage;}
			set 
			{
				if(_alreadyFetchedParameterFareStage && !value && (_parameterFareStage != null))
				{
					_parameterFareStage.Clear();
				}
				_alreadyFetchedParameterFareStage = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterTariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTariffCollection ParameterTariff
		{
			get	{ return GetMultiParameterTariff(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterTariff. When set to true, ParameterTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterTariff is accessed. You can always execute/ a forced fetch by calling GetMultiParameterTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterTariff
		{
			get	{ return _alwaysFetchParameterTariff; }
			set	{ _alwaysFetchParameterTariff = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterTariff already has been fetched. Setting this property to false when ParameterTariff has been fetched
		/// will clear the ParameterTariff collection well. Setting this property to true while ParameterTariff hasn't been fetched disables lazy loading for ParameterTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterTariff
		{
			get { return _alreadyFetchedParameterTariff;}
			set 
			{
				if(_alreadyFetchedParameterTariff && !value && (_parameterTariff != null))
				{
					_parameterTariff.Clear();
				}
				_alreadyFetchedParameterTariff = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterTicketCollection ParameterTickets
		{
			get	{ return GetMultiParameterTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterTickets. When set to true, ParameterTickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterTickets is accessed. You can always execute/ a forced fetch by calling GetMultiParameterTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterTickets
		{
			get	{ return _alwaysFetchParameterTickets; }
			set	{ _alwaysFetchParameterTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterTickets already has been fetched. Setting this property to false when ParameterTickets has been fetched
		/// will clear the ParameterTickets collection well. Setting this property to true while ParameterTickets hasn't been fetched disables lazy loading for ParameterTickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterTickets
		{
			get { return _alreadyFetchedParameterTickets;}
			set 
			{
				if(_alreadyFetchedParameterTickets && !value && (_parameterTickets != null))
				{
					_parameterTickets.Clear();
				}
				_alreadyFetchedParameterTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PredefinedKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPredefinedKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PredefinedKeyCollection PredefinedKeys
		{
			get	{ return GetMultiPredefinedKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PredefinedKeys. When set to true, PredefinedKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PredefinedKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPredefinedKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPredefinedKeys
		{
			get	{ return _alwaysFetchPredefinedKeys; }
			set	{ _alwaysFetchPredefinedKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PredefinedKeys already has been fetched. Setting this property to false when PredefinedKeys has been fetched
		/// will clear the PredefinedKeys collection well. Setting this property to true while PredefinedKeys hasn't been fetched disables lazy loading for PredefinedKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPredefinedKeys
		{
			get { return _alreadyFetchedPredefinedKeys;}
			set 
			{
				if(_alreadyFetchedPredefinedKeys && !value && (_predefinedKeys != null))
				{
					_predefinedKeys.Clear();
				}
				_alreadyFetchedPredefinedKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PrimalKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPrimalKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PrimalKeyCollection PrimalKeys
		{
			get	{ return GetMultiPrimalKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PrimalKeys. When set to true, PrimalKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrimalKeys is accessed. You can always execute/ a forced fetch by calling GetMultiPrimalKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrimalKeys
		{
			get	{ return _alwaysFetchPrimalKeys; }
			set	{ _alwaysFetchPrimalKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrimalKeys already has been fetched. Setting this property to false when PrimalKeys has been fetched
		/// will clear the PrimalKeys collection well. Setting this property to true while PrimalKeys hasn't been fetched disables lazy loading for PrimalKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrimalKeys
		{
			get { return _alreadyFetchedPrimalKeys;}
			set 
			{
				if(_alreadyFetchedPrimalKeys && !value && (_primalKeys != null))
				{
					_primalKeys.Clear();
				}
				_alreadyFetchedPrimalKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PrintTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPrintTexts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PrintTextCollection PrintTexts
		{
			get	{ return GetMultiPrintTexts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PrintTexts. When set to true, PrintTexts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PrintTexts is accessed. You can always execute/ a forced fetch by calling GetMultiPrintTexts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPrintTexts
		{
			get	{ return _alwaysFetchPrintTexts; }
			set	{ _alwaysFetchPrintTexts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PrintTexts already has been fetched. Setting this property to false when PrintTexts has been fetched
		/// will clear the PrintTexts collection well. Setting this property to true while PrintTexts hasn't been fetched disables lazy loading for PrintTexts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPrintTexts
		{
			get { return _alreadyFetchedPrintTexts;}
			set 
			{
				if(_alreadyFetchedPrintTexts && !value && (_printTexts != null))
				{
					_printTexts.Clear();
				}
				_alreadyFetchedPrintTexts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutes()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection Routes
		{
			get	{ return GetMultiRoutes(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Routes. When set to true, Routes is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Routes is accessed. You can always execute/ a forced fetch by calling GetMultiRoutes(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutes
		{
			get	{ return _alwaysFetchRoutes; }
			set	{ _alwaysFetchRoutes = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Routes already has been fetched. Setting this property to false when Routes has been fetched
		/// will clear the Routes collection well. Setting this property to true while Routes hasn't been fetched disables lazy loading for Routes</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutes
		{
			get { return _alreadyFetchedRoutes;}
			set 
			{
				if(_alreadyFetchedRoutes && !value && (_routes != null))
				{
					_routes.Clear();
				}
				_alreadyFetchedRoutes = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteNameEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteNames()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteNameCollection RouteNames
		{
			get	{ return GetMultiRouteNames(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteNames. When set to true, RouteNames is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteNames is accessed. You can always execute/ a forced fetch by calling GetMultiRouteNames(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteNames
		{
			get	{ return _alwaysFetchRouteNames; }
			set	{ _alwaysFetchRouteNames = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteNames already has been fetched. Setting this property to false when RouteNames has been fetched
		/// will clear the RouteNames collection well. Setting this property to true while RouteNames hasn't been fetched disables lazy loading for RouteNames</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteNames
		{
			get { return _alreadyFetchedRouteNames;}
			set 
			{
				if(_alreadyFetchedRouteNames && !value && (_routeNames != null))
				{
					_routeNames.Clear();
				}
				_alreadyFetchedRouteNames = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleCappingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleCappings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleCappingCollection RuleCappings
		{
			get	{ return GetMultiRuleCappings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleCappings. When set to true, RuleCappings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleCappings is accessed. You can always execute/ a forced fetch by calling GetMultiRuleCappings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleCappings
		{
			get	{ return _alwaysFetchRuleCappings; }
			set	{ _alwaysFetchRuleCappings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleCappings already has been fetched. Setting this property to false when RuleCappings has been fetched
		/// will clear the RuleCappings collection well. Setting this property to true while RuleCappings hasn't been fetched disables lazy loading for RuleCappings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleCappings
		{
			get { return _alreadyFetchedRuleCappings;}
			set 
			{
				if(_alreadyFetchedRuleCappings && !value && (_ruleCappings != null))
				{
					_ruleCappings.Clear();
				}
				_alreadyFetchedRuleCappings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ServiceAllocationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiServiceAllocations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ServiceAllocationCollection ServiceAllocations
		{
			get	{ return GetMultiServiceAllocations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ServiceAllocations. When set to true, ServiceAllocations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ServiceAllocations is accessed. You can always execute/ a forced fetch by calling GetMultiServiceAllocations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchServiceAllocations
		{
			get	{ return _alwaysFetchServiceAllocations; }
			set	{ _alwaysFetchServiceAllocations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ServiceAllocations already has been fetched. Setting this property to false when ServiceAllocations has been fetched
		/// will clear the ServiceAllocations collection well. Setting this property to true while ServiceAllocations hasn't been fetched disables lazy loading for ServiceAllocations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedServiceAllocations
		{
			get { return _alreadyFetchedServiceAllocations;}
			set 
			{
				if(_alreadyFetchedServiceAllocations && !value && (_serviceAllocations != null))
				{
					_serviceAllocations.Clear();
				}
				_alreadyFetchedServiceAllocations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ShortDistanceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiShortDistances()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ShortDistanceCollection ShortDistances
		{
			get	{ return GetMultiShortDistances(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ShortDistances. When set to true, ShortDistances is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ShortDistances is accessed. You can always execute/ a forced fetch by calling GetMultiShortDistances(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchShortDistances
		{
			get	{ return _alwaysFetchShortDistances; }
			set	{ _alwaysFetchShortDistances = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ShortDistances already has been fetched. Setting this property to false when ShortDistances has been fetched
		/// will clear the ShortDistances collection well. Setting this property to true while ShortDistances hasn't been fetched disables lazy loading for ShortDistances</summary>
		[Browsable(false)]
		public bool AlreadyFetchedShortDistances
		{
			get { return _alreadyFetchedShortDistances;}
			set 
			{
				if(_alreadyFetchedShortDistances && !value && (_shortDistances != null))
				{
					_shortDistances.Clear();
				}
				_alreadyFetchedShortDistances = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSpecialReceipts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection SpecialReceipts
		{
			get	{ return GetMultiSpecialReceipts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SpecialReceipts. When set to true, SpecialReceipts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SpecialReceipts is accessed. You can always execute/ a forced fetch by calling GetMultiSpecialReceipts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSpecialReceipts
		{
			get	{ return _alwaysFetchSpecialReceipts; }
			set	{ _alwaysFetchSpecialReceipts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SpecialReceipts already has been fetched. Setting this property to false when SpecialReceipts has been fetched
		/// will clear the SpecialReceipts collection well. Setting this property to true while SpecialReceipts hasn't been fetched disables lazy loading for SpecialReceipts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSpecialReceipts
		{
			get { return _alreadyFetchedSpecialReceipts;}
			set 
			{
				if(_alreadyFetchedSpecialReceipts && !value && (_specialReceipts != null))
				{
					_specialReceipts.Clear();
				}
				_alreadyFetchedSpecialReceipts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SystemTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSystemTexts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SystemTextCollection SystemTexts
		{
			get	{ return GetMultiSystemTexts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SystemTexts. When set to true, SystemTexts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemTexts is accessed. You can always execute/ a forced fetch by calling GetMultiSystemTexts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemTexts
		{
			get	{ return _alwaysFetchSystemTexts; }
			set	{ _alwaysFetchSystemTexts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemTexts already has been fetched. Setting this property to false when SystemTexts has been fetched
		/// will clear the SystemTexts collection well. Setting this property to true while SystemTexts hasn't been fetched disables lazy loading for SystemTexts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemTexts
		{
			get { return _alreadyFetchedSystemTexts;}
			set 
			{
				if(_alreadyFetchedSystemTexts && !value && (_systemTexts != null))
				{
					_systemTexts.Clear();
				}
				_alreadyFetchedSystemTexts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiChildTariffs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection ChildTariffs
		{
			get	{ return GetMultiChildTariffs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ChildTariffs. When set to true, ChildTariffs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ChildTariffs is accessed. You can always execute/ a forced fetch by calling GetMultiChildTariffs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchChildTariffs
		{
			get	{ return _alwaysFetchChildTariffs; }
			set	{ _alwaysFetchChildTariffs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ChildTariffs already has been fetched. Setting this property to false when ChildTariffs has been fetched
		/// will clear the ChildTariffs collection well. Setting this property to true while ChildTariffs hasn't been fetched disables lazy loading for ChildTariffs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedChildTariffs
		{
			get { return _alreadyFetchedChildTariffs;}
			set 
			{
				if(_alreadyFetchedChildTariffs && !value && (_childTariffs != null))
				{
					_childTariffs.Clear();
				}
				_alreadyFetchedChildTariffs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMatrixParentTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection MatrixParentTariff
		{
			get	{ return GetMultiMatrixParentTariff(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MatrixParentTariff. When set to true, MatrixParentTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MatrixParentTariff is accessed. You can always execute/ a forced fetch by calling GetMultiMatrixParentTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMatrixParentTariff
		{
			get	{ return _alwaysFetchMatrixParentTariff; }
			set	{ _alwaysFetchMatrixParentTariff = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MatrixParentTariff already has been fetched. Setting this property to false when MatrixParentTariff has been fetched
		/// will clear the MatrixParentTariff collection well. Setting this property to true while MatrixParentTariff hasn't been fetched disables lazy loading for MatrixParentTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMatrixParentTariff
		{
			get { return _alreadyFetchedMatrixParentTariff;}
			set 
			{
				if(_alreadyFetchedMatrixParentTariff && !value && (_matrixParentTariff != null))
				{
					_matrixParentTariff.Clear();
				}
				_alreadyFetchedMatrixParentTariff = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffRelease()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffReleaseCollection TariffRelease
		{
			get	{ return GetMultiTariffRelease(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TariffRelease. When set to true, TariffRelease is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TariffRelease is accessed. You can always execute/ a forced fetch by calling GetMultiTariffRelease(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffRelease
		{
			get	{ return _alwaysFetchTariffRelease; }
			set	{ _alwaysFetchTariffRelease = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TariffRelease already has been fetched. Setting this property to false when TariffRelease has been fetched
		/// will clear the TariffRelease collection well. Setting this property to true while TariffRelease hasn't been fetched disables lazy loading for TariffRelease</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffRelease
		{
			get { return _alreadyFetchedTariffRelease;}
			set 
			{
				if(_alreadyFetchedTariffRelease && !value && (_tariffRelease != null))
				{
					_tariffRelease.Clear();
				}
				_alreadyFetchedTariffRelease = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketGroupCollection TicketGroups
		{
			get	{ return GetMultiTicketGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketGroups. When set to true, TicketGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketGroups is accessed. You can always execute/ a forced fetch by calling GetMultiTicketGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketGroups
		{
			get	{ return _alwaysFetchTicketGroups; }
			set	{ _alwaysFetchTicketGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketGroups already has been fetched. Setting this property to false when TicketGroups has been fetched
		/// will clear the TicketGroups collection well. Setting this property to true while TicketGroups hasn't been fetched disables lazy loading for TicketGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketGroups
		{
			get { return _alreadyFetchedTicketGroups;}
			set 
			{
				if(_alreadyFetchedTicketGroups && !value && (_ticketGroups != null))
				{
					_ticketGroups.Clear();
				}
				_alreadyFetchedTicketGroups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TranslationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTranslations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TranslationCollection Translations
		{
			get	{ return GetMultiTranslations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Translations. When set to true, Translations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Translations is accessed. You can always execute/ a forced fetch by calling GetMultiTranslations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTranslations
		{
			get	{ return _alwaysFetchTranslations; }
			set	{ _alwaysFetchTranslations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Translations already has been fetched. Setting this property to false when Translations has been fetched
		/// will clear the Translations collection well. Setting this property to true while Translations hasn't been fetched disables lazy loading for Translations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTranslations
		{
			get { return _alreadyFetchedTranslations;}
			set 
			{
				if(_alreadyFetchedTranslations && !value && (_translations != null))
				{
					_translations.Clear();
				}
				_alreadyFetchedTranslations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UserKeyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserKeys()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UserKeyCollection UserKeys
		{
			get	{ return GetMultiUserKeys(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserKeys. When set to true, UserKeys is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserKeys is accessed. You can always execute/ a forced fetch by calling GetMultiUserKeys(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserKeys
		{
			get	{ return _alwaysFetchUserKeys; }
			set	{ _alwaysFetchUserKeys = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserKeys already has been fetched. Setting this property to false when UserKeys has been fetched
		/// will clear the UserKeys collection well. Setting this property to true while UserKeys hasn't been fetched disables lazy loading for UserKeys</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserKeys
		{
			get { return _alreadyFetchedUserKeys;}
			set 
			{
				if(_alreadyFetchedUserKeys && !value && (_userKeys != null))
				{
					_userKeys.Clear();
				}
				_alreadyFetchedUserKeys = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvKeySetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvKeySet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvKeySetCollection VdvKeySet
		{
			get	{ return GetMultiVdvKeySet(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvKeySet. When set to true, VdvKeySet is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvKeySet is accessed. You can always execute/ a forced fetch by calling GetMultiVdvKeySet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvKeySet
		{
			get	{ return _alwaysFetchVdvKeySet; }
			set	{ _alwaysFetchVdvKeySet = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvKeySet already has been fetched. Setting this property to false when VdvKeySet has been fetched
		/// will clear the VdvKeySet collection well. Setting this property to true while VdvKeySet hasn't been fetched disables lazy loading for VdvKeySet</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvKeySet
		{
			get { return _alreadyFetchedVdvKeySet;}
			set 
			{
				if(_alreadyFetchedVdvKeySet && !value && (_vdvKeySet != null))
				{
					_vdvKeySet.Clear();
				}
				_alreadyFetchedVdvKeySet = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvLayoutEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvLayoutCollection VdvLayout
		{
			get	{ return GetMultiVdvLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvLayout. When set to true, VdvLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvLayout is accessed. You can always execute/ a forced fetch by calling GetMultiVdvLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvLayout
		{
			get	{ return _alwaysFetchVdvLayout; }
			set	{ _alwaysFetchVdvLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvLayout already has been fetched. Setting this property to false when VdvLayout has been fetched
		/// will clear the VdvLayout collection well. Setting this property to true while VdvLayout hasn't been fetched disables lazy loading for VdvLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvLayout
		{
			get { return _alreadyFetchedVdvLayout;}
			set 
			{
				if(_alreadyFetchedVdvLayout && !value && (_vdvLayout != null))
				{
					_vdvLayout.Clear();
				}
				_alreadyFetchedVdvLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'VdvProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiVdvProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.VdvProductCollection VdvProduct
		{
			get	{ return GetMultiVdvProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for VdvProduct. When set to true, VdvProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvProduct is accessed. You can always execute/ a forced fetch by calling GetMultiVdvProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvProduct
		{
			get	{ return _alwaysFetchVdvProduct; }
			set	{ _alwaysFetchVdvProduct = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvProduct already has been fetched. Setting this property to false when VdvProduct has been fetched
		/// will clear the VdvProduct collection well. Setting this property to true while VdvProduct hasn't been fetched disables lazy loading for VdvProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvProduct
		{
			get { return _alreadyFetchedVdvProduct;}
			set 
			{
				if(_alreadyFetchedVdvProduct && !value && (_vdvProduct != null))
				{
					_vdvProduct.Clear();
				}
				_alreadyFetchedVdvProduct = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ClientTariffs", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOwnerClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity OwnerClient
		{
			get	{ return GetSingleOwnerClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOwnerClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OwnerClientTariffs", "OwnerClient", _ownerClient, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OwnerClient. When set to true, OwnerClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OwnerClient is accessed. You can always execute a forced fetch by calling GetSingleOwnerClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOwnerClient
		{
			get	{ return _alwaysFetchOwnerClient; }
			set	{ _alwaysFetchOwnerClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OwnerClient already has been fetched. Setting this property to false when OwnerClient has been fetched
		/// will set OwnerClient to null as well. Setting this property to true while OwnerClient hasn't been fetched disables lazy loading for OwnerClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOwnerClient
		{
			get { return _alreadyFetchedOwnerClient;}
			set 
			{
				if(_alreadyFetchedOwnerClient && !value)
				{
					this.OwnerClient = null;
				}
				_alreadyFetchedOwnerClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OwnerClient is not found
		/// in the database. When set to true, OwnerClient will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OwnerClientReturnsNewIfNotFound
		{
			get	{ return _ownerClientReturnsNewIfNotFound; }
			set { _ownerClientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tariffs", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'NetEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleNet()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual NetEntity Net
		{
			get	{ return GetSingleNet(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncNet(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Tariffs", "Net", _net, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Net. When set to true, Net is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Net is accessed. You can always execute a forced fetch by calling GetSingleNet(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchNet
		{
			get	{ return _alwaysFetchNet; }
			set	{ _alwaysFetchNet = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Net already has been fetched. Setting this property to false when Net has been fetched
		/// will set Net to null as well. Setting this property to true while Net hasn't been fetched disables lazy loading for Net</summary>
		[Browsable(false)]
		public bool AlreadyFetchedNet
		{
			get { return _alreadyFetchedNet;}
			set 
			{
				if(_alreadyFetchedNet && !value)
				{
					this.Net = null;
				}
				_alreadyFetchedNet = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Net is not found
		/// in the database. When set to true, Net will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool NetReturnsNewIfNotFound
		{
			get	{ return _netReturnsNewIfNotFound; }
			set { _netReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBaseTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity BaseTariff
		{
			get	{ return GetSingleBaseTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBaseTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ChildTariffs", "BaseTariff", _baseTariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BaseTariff. When set to true, BaseTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BaseTariff is accessed. You can always execute a forced fetch by calling GetSingleBaseTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBaseTariff
		{
			get	{ return _alwaysFetchBaseTariff; }
			set	{ _alwaysFetchBaseTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BaseTariff already has been fetched. Setting this property to false when BaseTariff has been fetched
		/// will set BaseTariff to null as well. Setting this property to true while BaseTariff hasn't been fetched disables lazy loading for BaseTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBaseTariff
		{
			get { return _alreadyFetchedBaseTariff;}
			set 
			{
				if(_alreadyFetchedBaseTariff && !value)
				{
					this.BaseTariff = null;
				}
				_alreadyFetchedBaseTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BaseTariff is not found
		/// in the database. When set to true, BaseTariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BaseTariffReturnsNewIfNotFound
		{
			get	{ return _baseTariffReturnsNewIfNotFound; }
			set { _baseTariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMatrixTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity MatrixTariff
		{
			get	{ return GetSingleMatrixTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMatrixTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MatrixParentTariff", "MatrixTariff", _matrixTariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MatrixTariff. When set to true, MatrixTariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MatrixTariff is accessed. You can always execute a forced fetch by calling GetSingleMatrixTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMatrixTariff
		{
			get	{ return _alwaysFetchMatrixTariff; }
			set	{ _alwaysFetchMatrixTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MatrixTariff already has been fetched. Setting this property to false when MatrixTariff has been fetched
		/// will set MatrixTariff to null as well. Setting this property to true while MatrixTariff hasn't been fetched disables lazy loading for MatrixTariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMatrixTariff
		{
			get { return _alreadyFetchedMatrixTariff;}
			set 
			{
				if(_alreadyFetchedMatrixTariff && !value)
				{
					this.MatrixTariff = null;
				}
				_alreadyFetchedMatrixTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MatrixTariff is not found
		/// in the database. When set to true, MatrixTariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MatrixTariffReturnsNewIfNotFound
		{
			get	{ return _matrixTariffReturnsNewIfNotFound; }
			set { _matrixTariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TariffEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
