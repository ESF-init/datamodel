﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementAccount'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementAccountEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementSetupCollection	_fromSettlementSetups;
		private bool	_alwaysFetchFromSettlementSetups, _alreadyFetchedFromSettlementSetups;
		private VarioSL.Entities.CollectionClasses.SettlementSetupCollection	_toSettlementSetups;
		private bool	_alwaysFetchToSettlementSetups, _alreadyFetchedToSettlementSetups;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FromSettlementSetups</summary>
			public static readonly string FromSettlementSetups = "FromSettlementSetups";
			/// <summary>Member name ToSettlementSetups</summary>
			public static readonly string ToSettlementSetups = "ToSettlementSetups";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementAccountEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementAccountEntity() :base("SettlementAccountEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		public SettlementAccountEntity(System.Int64 settlementAccountID):base("SettlementAccountEntity")
		{
			InitClassFetch(settlementAccountID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementAccountEntity(System.Int64 settlementAccountID, IPrefetchPath prefetchPathToUse):base("SettlementAccountEntity")
		{
			InitClassFetch(settlementAccountID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="validator">The custom validator object for this SettlementAccountEntity</param>
		public SettlementAccountEntity(System.Int64 settlementAccountID, IValidator validator):base("SettlementAccountEntity")
		{
			InitClassFetch(settlementAccountID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementAccountEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fromSettlementSetups = (VarioSL.Entities.CollectionClasses.SettlementSetupCollection)info.GetValue("_fromSettlementSetups", typeof(VarioSL.Entities.CollectionClasses.SettlementSetupCollection));
			_alwaysFetchFromSettlementSetups = info.GetBoolean("_alwaysFetchFromSettlementSetups");
			_alreadyFetchedFromSettlementSetups = info.GetBoolean("_alreadyFetchedFromSettlementSetups");

			_toSettlementSetups = (VarioSL.Entities.CollectionClasses.SettlementSetupCollection)info.GetValue("_toSettlementSetups", typeof(VarioSL.Entities.CollectionClasses.SettlementSetupCollection));
			_alwaysFetchToSettlementSetups = info.GetBoolean("_alwaysFetchToSettlementSetups");
			_alreadyFetchedToSettlementSetups = info.GetBoolean("_alreadyFetchedToSettlementSetups");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFromSettlementSetups = (_fromSettlementSetups.Count > 0);
			_alreadyFetchedToSettlementSetups = (_toSettlementSetups.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FromSettlementSetups":
					toReturn.Add(Relations.SettlementSetupEntityUsingFromAccountID);
					break;
				case "ToSettlementSetups":
					toReturn.Add(Relations.SettlementSetupEntityUsingToAccountID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fromSettlementSetups", (!this.MarkedForDeletion?_fromSettlementSetups:null));
			info.AddValue("_alwaysFetchFromSettlementSetups", _alwaysFetchFromSettlementSetups);
			info.AddValue("_alreadyFetchedFromSettlementSetups", _alreadyFetchedFromSettlementSetups);
			info.AddValue("_toSettlementSetups", (!this.MarkedForDeletion?_toSettlementSetups:null));
			info.AddValue("_alwaysFetchToSettlementSetups", _alwaysFetchToSettlementSetups);
			info.AddValue("_alreadyFetchedToSettlementSetups", _alreadyFetchedToSettlementSetups);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FromSettlementSetups":
					_alreadyFetchedFromSettlementSetups = true;
					if(entity!=null)
					{
						this.FromSettlementSetups.Add((SettlementSetupEntity)entity);
					}
					break;
				case "ToSettlementSetups":
					_alreadyFetchedToSettlementSetups = true;
					if(entity!=null)
					{
						this.ToSettlementSetups.Add((SettlementSetupEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FromSettlementSetups":
					_fromSettlementSetups.Add((SettlementSetupEntity)relatedEntity);
					break;
				case "ToSettlementSetups":
					_toSettlementSetups.Add((SettlementSetupEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FromSettlementSetups":
					this.PerformRelatedEntityRemoval(_fromSettlementSetups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ToSettlementSetups":
					this.PerformRelatedEntityRemoval(_toSettlementSetups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fromSettlementSetups);
			toReturn.Add(_toSettlementSetups);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementAccountID)
		{
			return FetchUsingPK(settlementAccountID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementAccountID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementAccountID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementAccountID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementAccountID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementAccountID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementAccountRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementSetupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiFromSettlementSetups(bool forceFetch)
		{
			return GetMultiFromSettlementSetups(forceFetch, _fromSettlementSetups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementSetupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiFromSettlementSetups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFromSettlementSetups(forceFetch, _fromSettlementSetups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiFromSettlementSetups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFromSettlementSetups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiFromSettlementSetups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFromSettlementSetups || forceFetch || _alwaysFetchFromSettlementSetups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fromSettlementSetups);
				_fromSettlementSetups.SuppressClearInGetMulti=!forceFetch;
				_fromSettlementSetups.EntityFactoryToUse = entityFactoryToUse;
				_fromSettlementSetups.GetMultiManyToOne(this, null, filter);
				_fromSettlementSetups.SuppressClearInGetMulti=false;
				_alreadyFetchedFromSettlementSetups = true;
			}
			return _fromSettlementSetups;
		}

		/// <summary> Sets the collection parameters for the collection for 'FromSettlementSetups'. These settings will be taken into account
		/// when the property FromSettlementSetups is requested or GetMultiFromSettlementSetups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFromSettlementSetups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fromSettlementSetups.SortClauses=sortClauses;
			_fromSettlementSetups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementSetupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiToSettlementSetups(bool forceFetch)
		{
			return GetMultiToSettlementSetups(forceFetch, _toSettlementSetups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementSetupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiToSettlementSetups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiToSettlementSetups(forceFetch, _toSettlementSetups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiToSettlementSetups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiToSettlementSetups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementSetupCollection GetMultiToSettlementSetups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedToSettlementSetups || forceFetch || _alwaysFetchToSettlementSetups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_toSettlementSetups);
				_toSettlementSetups.SuppressClearInGetMulti=!forceFetch;
				_toSettlementSetups.EntityFactoryToUse = entityFactoryToUse;
				_toSettlementSetups.GetMultiManyToOne(null, this, filter);
				_toSettlementSetups.SuppressClearInGetMulti=false;
				_alreadyFetchedToSettlementSetups = true;
			}
			return _toSettlementSetups;
		}

		/// <summary> Sets the collection parameters for the collection for 'ToSettlementSetups'. These settings will be taken into account
		/// when the property ToSettlementSetups is requested or GetMultiToSettlementSetups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersToSettlementSetups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_toSettlementSetups.SortClauses=sortClauses;
			_toSettlementSetups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FromSettlementSetups", _fromSettlementSetups);
			toReturn.Add("ToSettlementSetups", _toSettlementSetups);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="validator">The validator object for this SettlementAccountEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementAccountID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementAccountID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fromSettlementSetups = new VarioSL.Entities.CollectionClasses.SettlementSetupCollection();
			_fromSettlementSetups.SetContainingEntityInfo(this, "FromSettlementAccount");

			_toSettlementSetups = new VarioSL.Entities.CollectionClasses.SettlementSetupCollection();
			_toSettlementSetups.SetContainingEntityInfo(this, "ToSettlementAccount");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementAccountID">PK value for SettlementAccount which data should be fetched into this SettlementAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementAccountID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementAccountFieldIndex.SettlementAccountID].ForcedCurrentValueWrite(settlementAccountID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementAccountDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementAccountEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementAccountRelations Relations
		{
			get	{ return new SettlementAccountRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementSetup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFromSettlementSetups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementSetupCollection(), (IEntityRelation)GetRelationsForField("FromSettlementSetups")[0], (int)VarioSL.Entities.EntityType.SettlementAccountEntity, (int)VarioSL.Entities.EntityType.SettlementSetupEntity, 0, null, null, null, "FromSettlementSetups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementSetup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathToSettlementSetups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementSetupCollection(), (IEntityRelation)GetRelationsForField("ToSettlementSetups")[0], (int)VarioSL.Entities.EntityType.SettlementAccountEntity, (int)VarioSL.Entities.EntityType.SettlementSetupEntity, 0, null, null, null, "ToSettlementSetups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SettlementAccountFieldIndex.Description, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementAccountFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementAccountFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SettlementAccountFieldIndex.Name, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.Name, value, true); }
		}

		/// <summary> The SettlementAccountID property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."SETTLEMENTACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementAccountID
		{
			get { return (System.Int64)GetValue((int)SettlementAccountFieldIndex.SettlementAccountID, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.SettlementAccountID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementAccount<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTACCOUNT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementAccountFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementAccountFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFromSettlementSetups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementSetupCollection FromSettlementSetups
		{
			get	{ return GetMultiFromSettlementSetups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FromSettlementSetups. When set to true, FromSettlementSetups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FromSettlementSetups is accessed. You can always execute/ a forced fetch by calling GetMultiFromSettlementSetups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFromSettlementSetups
		{
			get	{ return _alwaysFetchFromSettlementSetups; }
			set	{ _alwaysFetchFromSettlementSetups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FromSettlementSetups already has been fetched. Setting this property to false when FromSettlementSetups has been fetched
		/// will clear the FromSettlementSetups collection well. Setting this property to true while FromSettlementSetups hasn't been fetched disables lazy loading for FromSettlementSetups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFromSettlementSetups
		{
			get { return _alreadyFetchedFromSettlementSetups;}
			set 
			{
				if(_alreadyFetchedFromSettlementSetups && !value && (_fromSettlementSetups != null))
				{
					_fromSettlementSetups.Clear();
				}
				_alreadyFetchedFromSettlementSetups = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementSetupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiToSettlementSetups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementSetupCollection ToSettlementSetups
		{
			get	{ return GetMultiToSettlementSetups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ToSettlementSetups. When set to true, ToSettlementSetups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ToSettlementSetups is accessed. You can always execute/ a forced fetch by calling GetMultiToSettlementSetups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchToSettlementSetups
		{
			get	{ return _alwaysFetchToSettlementSetups; }
			set	{ _alwaysFetchToSettlementSetups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ToSettlementSetups already has been fetched. Setting this property to false when ToSettlementSetups has been fetched
		/// will clear the ToSettlementSetups collection well. Setting this property to true while ToSettlementSetups hasn't been fetched disables lazy loading for ToSettlementSetups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedToSettlementSetups
		{
			get { return _alreadyFetchedToSettlementSetups;}
			set 
			{
				if(_alreadyFetchedToSettlementSetups && !value && (_toSettlementSetups != null))
				{
					_toSettlementSetups.Clear();
				}
				_alreadyFetchedToSettlementSetups = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementAccountEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
