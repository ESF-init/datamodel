﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SystemField'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SystemFieldEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection	_systemFieldBarcodeLayoutObjects;
		private bool	_alwaysFetchSystemFieldBarcodeLayoutObjects, _alreadyFetchedSystemFieldBarcodeLayoutObjects;
		private VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection	_systemFieldTextLayoutObjects;
		private bool	_alwaysFetchSystemFieldTextLayoutObjects, _alreadyFetchedSystemFieldTextLayoutObjects;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SystemFieldBarcodeLayoutObjects</summary>
			public static readonly string SystemFieldBarcodeLayoutObjects = "SystemFieldBarcodeLayoutObjects";
			/// <summary>Member name SystemFieldTextLayoutObjects</summary>
			public static readonly string SystemFieldTextLayoutObjects = "SystemFieldTextLayoutObjects";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SystemFieldEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SystemFieldEntity() :base("SystemFieldEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		public SystemFieldEntity(System.Int64 systemFieldID):base("SystemFieldEntity")
		{
			InitClassFetch(systemFieldID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SystemFieldEntity(System.Int64 systemFieldID, IPrefetchPath prefetchPathToUse):base("SystemFieldEntity")
		{
			InitClassFetch(systemFieldID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="validator">The custom validator object for this SystemFieldEntity</param>
		public SystemFieldEntity(System.Int64 systemFieldID, IValidator validator):base("SystemFieldEntity")
		{
			InitClassFetch(systemFieldID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SystemFieldEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_systemFieldBarcodeLayoutObjects = (VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection)info.GetValue("_systemFieldBarcodeLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection));
			_alwaysFetchSystemFieldBarcodeLayoutObjects = info.GetBoolean("_alwaysFetchSystemFieldBarcodeLayoutObjects");
			_alreadyFetchedSystemFieldBarcodeLayoutObjects = info.GetBoolean("_alreadyFetchedSystemFieldBarcodeLayoutObjects");

			_systemFieldTextLayoutObjects = (VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection)info.GetValue("_systemFieldTextLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection));
			_alwaysFetchSystemFieldTextLayoutObjects = info.GetBoolean("_alwaysFetchSystemFieldTextLayoutObjects");
			_alreadyFetchedSystemFieldTextLayoutObjects = info.GetBoolean("_alreadyFetchedSystemFieldTextLayoutObjects");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSystemFieldBarcodeLayoutObjects = (_systemFieldBarcodeLayoutObjects.Count > 0);
			_alreadyFetchedSystemFieldTextLayoutObjects = (_systemFieldTextLayoutObjects.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SystemFieldBarcodeLayoutObjects":
					toReturn.Add(Relations.SystemFieldBarcodeLayoutObjectEntityUsingSystemFieldID);
					break;
				case "SystemFieldTextLayoutObjects":
					toReturn.Add(Relations.SystemFieldTextLayoutObjectEntityUsingSystemFieldID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_systemFieldBarcodeLayoutObjects", (!this.MarkedForDeletion?_systemFieldBarcodeLayoutObjects:null));
			info.AddValue("_alwaysFetchSystemFieldBarcodeLayoutObjects", _alwaysFetchSystemFieldBarcodeLayoutObjects);
			info.AddValue("_alreadyFetchedSystemFieldBarcodeLayoutObjects", _alreadyFetchedSystemFieldBarcodeLayoutObjects);
			info.AddValue("_systemFieldTextLayoutObjects", (!this.MarkedForDeletion?_systemFieldTextLayoutObjects:null));
			info.AddValue("_alwaysFetchSystemFieldTextLayoutObjects", _alwaysFetchSystemFieldTextLayoutObjects);
			info.AddValue("_alreadyFetchedSystemFieldTextLayoutObjects", _alreadyFetchedSystemFieldTextLayoutObjects);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SystemFieldBarcodeLayoutObjects":
					_alreadyFetchedSystemFieldBarcodeLayoutObjects = true;
					if(entity!=null)
					{
						this.SystemFieldBarcodeLayoutObjects.Add((SystemFieldBarcodeLayoutObjectEntity)entity);
					}
					break;
				case "SystemFieldTextLayoutObjects":
					_alreadyFetchedSystemFieldTextLayoutObjects = true;
					if(entity!=null)
					{
						this.SystemFieldTextLayoutObjects.Add((SystemFieldTextLayoutObjectEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SystemFieldBarcodeLayoutObjects":
					_systemFieldBarcodeLayoutObjects.Add((SystemFieldBarcodeLayoutObjectEntity)relatedEntity);
					break;
				case "SystemFieldTextLayoutObjects":
					_systemFieldTextLayoutObjects.Add((SystemFieldTextLayoutObjectEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SystemFieldBarcodeLayoutObjects":
					this.PerformRelatedEntityRemoval(_systemFieldBarcodeLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SystemFieldTextLayoutObjects":
					this.PerformRelatedEntityRemoval(_systemFieldTextLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_systemFieldBarcodeLayoutObjects);
			toReturn.Add(_systemFieldTextLayoutObjects);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 systemFieldID)
		{
			return FetchUsingPK(systemFieldID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 systemFieldID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(systemFieldID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 systemFieldID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(systemFieldID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 systemFieldID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(systemFieldID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SystemFieldID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SystemFieldRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldBarcodeLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, _systemFieldBarcodeLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldBarcodeLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, _systemFieldBarcodeLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSystemFieldBarcodeLayoutObjects || forceFetch || _alwaysFetchSystemFieldBarcodeLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_systemFieldBarcodeLayoutObjects);
				_systemFieldBarcodeLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_systemFieldBarcodeLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_systemFieldBarcodeLayoutObjects.GetMultiManyToOne(null, this, filter);
				_systemFieldBarcodeLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedSystemFieldBarcodeLayoutObjects = true;
			}
			return _systemFieldBarcodeLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'SystemFieldBarcodeLayoutObjects'. These settings will be taken into account
		/// when the property SystemFieldBarcodeLayoutObjects is requested or GetMultiSystemFieldBarcodeLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSystemFieldBarcodeLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_systemFieldBarcodeLayoutObjects.SortClauses=sortClauses;
			_systemFieldBarcodeLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutObjects(bool forceFetch)
		{
			return GetMultiSystemFieldTextLayoutObjects(forceFetch, _systemFieldTextLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSystemFieldTextLayoutObjects(forceFetch, _systemFieldTextLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSystemFieldTextLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSystemFieldTextLayoutObjects || forceFetch || _alwaysFetchSystemFieldTextLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_systemFieldTextLayoutObjects);
				_systemFieldTextLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_systemFieldTextLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_systemFieldTextLayoutObjects.GetMultiManyToOne(null, this, filter);
				_systemFieldTextLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedSystemFieldTextLayoutObjects = true;
			}
			return _systemFieldTextLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'SystemFieldTextLayoutObjects'. These settings will be taken into account
		/// when the property SystemFieldTextLayoutObjects is requested or GetMultiSystemFieldTextLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSystemFieldTextLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_systemFieldTextLayoutObjects.SortClauses=sortClauses;
			_systemFieldTextLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SystemFieldBarcodeLayoutObjects", _systemFieldBarcodeLayoutObjects);
			toReturn.Add("SystemFieldTextLayoutObjects", _systemFieldTextLayoutObjects);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="validator">The validator object for this SystemFieldEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 systemFieldID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(systemFieldID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_systemFieldBarcodeLayoutObjects = new VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection();
			_systemFieldBarcodeLayoutObjects.SetContainingEntityInfo(this, "SystemField");

			_systemFieldTextLayoutObjects = new VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection();
			_systemFieldTextLayoutObjects.SetContainingEntityInfo(this, "SystemField");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FormatString", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutObjectWidth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NxName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SampleText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SampleTextFunction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemFieldID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="systemFieldID">PK value for SystemField which data should be fetched into this SystemField object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 systemFieldID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SystemFieldFieldIndex.SystemFieldID].ForcedCurrentValueWrite(systemFieldID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSystemFieldDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SystemFieldEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SystemFieldRelations Relations
		{
			get	{ return new SystemFieldRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SystemFieldBarcodeLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemFieldBarcodeLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("SystemFieldBarcodeLayoutObjects")[0], (int)VarioSL.Entities.EntityType.SystemFieldEntity, (int)VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity, 0, null, null, null, "SystemFieldBarcodeLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SystemFieldTextLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemFieldTextLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("SystemFieldTextLayoutObjects")[0], (int)VarioSL.Entities.EntityType.SystemFieldEntity, (int)VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity, 0, null, null, null, "SystemFieldTextLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.Description, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.Description, value, true); }
		}

		/// <summary> The FormatString property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."FORMATSTRING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FormatString
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.FormatString, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.FormatString, value, true); }
		}

		/// <summary> The LayoutObjectWidth property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."LAYOUTOBJECTWIDTH_MM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 12, 2, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Double> LayoutObjectWidth
		{
			get { return (Nullable<System.Double>)GetValue((int)SystemFieldFieldIndex.LayoutObjectWidth, false); }
			set	{ SetValue((int)SystemFieldFieldIndex.LayoutObjectWidth, value, true); }
		}

		/// <summary> The Name property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.Name, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Number
		{
			get { return (Nullable<System.Int64>)GetValue((int)SystemFieldFieldIndex.Number, false); }
			set	{ SetValue((int)SystemFieldFieldIndex.Number, value, true); }
		}

		/// <summary> The NxName property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."NXNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NxName
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.NxName, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.NxName, value, true); }
		}

		/// <summary> The SampleText property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."SAMPLETEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SampleText
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.SampleText, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.SampleText, value, true); }
		}

		/// <summary> The SampleTextFunction property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."SAMPLETEXTFUNCTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SampleTextFunction
		{
			get { return (System.String)GetValue((int)SystemFieldFieldIndex.SampleTextFunction, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.SampleTextFunction, value, true); }
		}

		/// <summary> The SystemFieldID property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."SYSTEMFIELDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 SystemFieldID
		{
			get { return (System.Int64)GetValue((int)SystemFieldFieldIndex.SystemFieldID, true); }
			set	{ SetValue((int)SystemFieldFieldIndex.SystemFieldID, value, true); }
		}

		/// <summary> The Visible property of the Entity SystemField<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_SYSTEMFIELD"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Visible
		{
			get { return (Nullable<System.Int16>)GetValue((int)SystemFieldFieldIndex.Visible, false); }
			set	{ SetValue((int)SystemFieldFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSystemFieldBarcodeLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection SystemFieldBarcodeLayoutObjects
		{
			get	{ return GetMultiSystemFieldBarcodeLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SystemFieldBarcodeLayoutObjects. When set to true, SystemFieldBarcodeLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemFieldBarcodeLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiSystemFieldBarcodeLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemFieldBarcodeLayoutObjects
		{
			get	{ return _alwaysFetchSystemFieldBarcodeLayoutObjects; }
			set	{ _alwaysFetchSystemFieldBarcodeLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemFieldBarcodeLayoutObjects already has been fetched. Setting this property to false when SystemFieldBarcodeLayoutObjects has been fetched
		/// will clear the SystemFieldBarcodeLayoutObjects collection well. Setting this property to true while SystemFieldBarcodeLayoutObjects hasn't been fetched disables lazy loading for SystemFieldBarcodeLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemFieldBarcodeLayoutObjects
		{
			get { return _alreadyFetchedSystemFieldBarcodeLayoutObjects;}
			set 
			{
				if(_alreadyFetchedSystemFieldBarcodeLayoutObjects && !value && (_systemFieldBarcodeLayoutObjects != null))
				{
					_systemFieldBarcodeLayoutObjects.Clear();
				}
				_alreadyFetchedSystemFieldBarcodeLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSystemFieldTextLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection SystemFieldTextLayoutObjects
		{
			get	{ return GetMultiSystemFieldTextLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SystemFieldTextLayoutObjects. When set to true, SystemFieldTextLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemFieldTextLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiSystemFieldTextLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemFieldTextLayoutObjects
		{
			get	{ return _alwaysFetchSystemFieldTextLayoutObjects; }
			set	{ _alwaysFetchSystemFieldTextLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemFieldTextLayoutObjects already has been fetched. Setting this property to false when SystemFieldTextLayoutObjects has been fetched
		/// will clear the SystemFieldTextLayoutObjects collection well. Setting this property to true while SystemFieldTextLayoutObjects hasn't been fetched disables lazy loading for SystemFieldTextLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemFieldTextLayoutObjects
		{
			get { return _alreadyFetchedSystemFieldTextLayoutObjects;}
			set 
			{
				if(_alreadyFetchedSystemFieldTextLayoutObjects && !value && (_systemFieldTextLayoutObjects != null))
				{
					_systemFieldTextLayoutObjects.Clear();
				}
				_alreadyFetchedSystemFieldTextLayoutObjects = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SystemFieldEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
