﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PrintText'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PrintTextEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.RouteCollection	_routesPrintText1;
		private bool	_alwaysFetchRoutesPrintText1, _alreadyFetchedRoutesPrintText1;
		private VarioSL.Entities.CollectionClasses.RouteCollection	_routesPrintText2;
		private bool	_alwaysFetchRoutesPrintText2, _alreadyFetchedRoutesPrintText2;
		private VarioSL.Entities.CollectionClasses.RouteCollection	_routesPrintText3;
		private bool	_alwaysFetchRoutesPrintText3, _alreadyFetchedRoutesPrintText3;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name RoutesPrintText1</summary>
			public static readonly string RoutesPrintText1 = "RoutesPrintText1";
			/// <summary>Member name RoutesPrintText2</summary>
			public static readonly string RoutesPrintText2 = "RoutesPrintText2";
			/// <summary>Member name RoutesPrintText3</summary>
			public static readonly string RoutesPrintText3 = "RoutesPrintText3";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PrintTextEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PrintTextEntity() :base("PrintTextEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		public PrintTextEntity(System.Int64 printTextID):base("PrintTextEntity")
		{
			InitClassFetch(printTextID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PrintTextEntity(System.Int64 printTextID, IPrefetchPath prefetchPathToUse):base("PrintTextEntity")
		{
			InitClassFetch(printTextID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="validator">The custom validator object for this PrintTextEntity</param>
		public PrintTextEntity(System.Int64 printTextID, IValidator validator):base("PrintTextEntity")
		{
			InitClassFetch(printTextID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PrintTextEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_routesPrintText1 = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_routesPrintText1", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchRoutesPrintText1 = info.GetBoolean("_alwaysFetchRoutesPrintText1");
			_alreadyFetchedRoutesPrintText1 = info.GetBoolean("_alreadyFetchedRoutesPrintText1");

			_routesPrintText2 = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_routesPrintText2", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchRoutesPrintText2 = info.GetBoolean("_alwaysFetchRoutesPrintText2");
			_alreadyFetchedRoutesPrintText2 = info.GetBoolean("_alreadyFetchedRoutesPrintText2");

			_routesPrintText3 = (VarioSL.Entities.CollectionClasses.RouteCollection)info.GetValue("_routesPrintText3", typeof(VarioSL.Entities.CollectionClasses.RouteCollection));
			_alwaysFetchRoutesPrintText3 = info.GetBoolean("_alwaysFetchRoutesPrintText3");
			_alreadyFetchedRoutesPrintText3 = info.GetBoolean("_alreadyFetchedRoutesPrintText3");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PrintTextFieldIndex)fieldIndex)
			{
				case PrintTextFieldIndex.TariffId:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRoutesPrintText1 = (_routesPrintText1.Count > 0);
			_alreadyFetchedRoutesPrintText2 = (_routesPrintText2.Count > 0);
			_alreadyFetchedRoutesPrintText3 = (_routesPrintText3.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffId);
					break;
				case "RoutesPrintText1":
					toReturn.Add(Relations.RouteEntityUsingPrintText1ID);
					break;
				case "RoutesPrintText2":
					toReturn.Add(Relations.RouteEntityUsingPrintText2ID);
					break;
				case "RoutesPrintText3":
					toReturn.Add(Relations.RouteEntityUsingPrintText3ID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_routesPrintText1", (!this.MarkedForDeletion?_routesPrintText1:null));
			info.AddValue("_alwaysFetchRoutesPrintText1", _alwaysFetchRoutesPrintText1);
			info.AddValue("_alreadyFetchedRoutesPrintText1", _alreadyFetchedRoutesPrintText1);
			info.AddValue("_routesPrintText2", (!this.MarkedForDeletion?_routesPrintText2:null));
			info.AddValue("_alwaysFetchRoutesPrintText2", _alwaysFetchRoutesPrintText2);
			info.AddValue("_alreadyFetchedRoutesPrintText2", _alreadyFetchedRoutesPrintText2);
			info.AddValue("_routesPrintText3", (!this.MarkedForDeletion?_routesPrintText3:null));
			info.AddValue("_alwaysFetchRoutesPrintText3", _alwaysFetchRoutesPrintText3);
			info.AddValue("_alreadyFetchedRoutesPrintText3", _alreadyFetchedRoutesPrintText3);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "RoutesPrintText1":
					_alreadyFetchedRoutesPrintText1 = true;
					if(entity!=null)
					{
						this.RoutesPrintText1.Add((RouteEntity)entity);
					}
					break;
				case "RoutesPrintText2":
					_alreadyFetchedRoutesPrintText2 = true;
					if(entity!=null)
					{
						this.RoutesPrintText2.Add((RouteEntity)entity);
					}
					break;
				case "RoutesPrintText3":
					_alreadyFetchedRoutesPrintText3 = true;
					if(entity!=null)
					{
						this.RoutesPrintText3.Add((RouteEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "RoutesPrintText1":
					_routesPrintText1.Add((RouteEntity)relatedEntity);
					break;
				case "RoutesPrintText2":
					_routesPrintText2.Add((RouteEntity)relatedEntity);
					break;
				case "RoutesPrintText3":
					_routesPrintText3.Add((RouteEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "RoutesPrintText1":
					this.PerformRelatedEntityRemoval(_routesPrintText1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutesPrintText2":
					this.PerformRelatedEntityRemoval(_routesPrintText2, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RoutesPrintText3":
					this.PerformRelatedEntityRemoval(_routesPrintText3, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_routesPrintText1);
			toReturn.Add(_routesPrintText2);
			toReturn.Add(_routesPrintText3);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printTextID)
		{
			return FetchUsingPK(printTextID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printTextID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(printTextID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printTextID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(printTextID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 printTextID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(printTextID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PrintTextID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PrintTextRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText1(bool forceFetch)
		{
			return GetMultiRoutesPrintText1(forceFetch, _routesPrintText1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutesPrintText1(forceFetch, _routesPrintText1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutesPrintText1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutesPrintText1 || forceFetch || _alwaysFetchRoutesPrintText1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routesPrintText1);
				_routesPrintText1.SuppressClearInGetMulti=!forceFetch;
				_routesPrintText1.EntityFactoryToUse = entityFactoryToUse;
				_routesPrintText1.GetMultiManyToOne(null, null, this, null, null, null, null, filter);
				_routesPrintText1.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutesPrintText1 = true;
			}
			return _routesPrintText1;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutesPrintText1'. These settings will be taken into account
		/// when the property RoutesPrintText1 is requested or GetMultiRoutesPrintText1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutesPrintText1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routesPrintText1.SortClauses=sortClauses;
			_routesPrintText1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText2(bool forceFetch)
		{
			return GetMultiRoutesPrintText2(forceFetch, _routesPrintText2.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText2(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutesPrintText2(forceFetch, _routesPrintText2.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText2(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutesPrintText2(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText2(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutesPrintText2 || forceFetch || _alwaysFetchRoutesPrintText2) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routesPrintText2);
				_routesPrintText2.SuppressClearInGetMulti=!forceFetch;
				_routesPrintText2.EntityFactoryToUse = entityFactoryToUse;
				_routesPrintText2.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_routesPrintText2.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutesPrintText2 = true;
			}
			return _routesPrintText2;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutesPrintText2'. These settings will be taken into account
		/// when the property RoutesPrintText2 is requested or GetMultiRoutesPrintText2 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutesPrintText2(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routesPrintText2.SortClauses=sortClauses;
			_routesPrintText2.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText3(bool forceFetch)
		{
			return GetMultiRoutesPrintText3(forceFetch, _routesPrintText3.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText3(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoutesPrintText3(forceFetch, _routesPrintText3.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText3(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoutesPrintText3(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection GetMultiRoutesPrintText3(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoutesPrintText3 || forceFetch || _alwaysFetchRoutesPrintText3) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routesPrintText3);
				_routesPrintText3.SuppressClearInGetMulti=!forceFetch;
				_routesPrintText3.EntityFactoryToUse = entityFactoryToUse;
				_routesPrintText3.GetMultiManyToOne(null, null, null, null, this, null, null, filter);
				_routesPrintText3.SuppressClearInGetMulti=false;
				_alreadyFetchedRoutesPrintText3 = true;
			}
			return _routesPrintText3;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoutesPrintText3'. These settings will be taken into account
		/// when the property RoutesPrintText3 is requested or GetMultiRoutesPrintText3 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoutesPrintText3(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routesPrintText3.SortClauses=sortClauses;
			_routesPrintText3.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffId);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("RoutesPrintText1", _routesPrintText1);
			toReturn.Add("RoutesPrintText2", _routesPrintText2);
			toReturn.Add("RoutesPrintText3", _routesPrintText3);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="validator">The validator object for this PrintTextEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 printTextID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(printTextID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_routesPrintText1 = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_routesPrintText1.SetContainingEntityInfo(this, "PrintText1");

			_routesPrintText2 = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_routesPrintText2.SetContainingEntityInfo(this, "PrintText2");

			_routesPrintText3 = new VarioSL.Entities.CollectionClasses.RouteCollection();
			_routesPrintText3.SetContainingEntityInfo(this, "PrintText3");
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrintTextID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticPrintTextRelations.TariffEntityUsingTariffIdStatic, true, signalRelatedEntity, "PrintTexts", resetFKFields, new int[] { (int)PrintTextFieldIndex.TariffId } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticPrintTextRelations.TariffEntityUsingTariffIdStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="printTextID">PK value for PrintText which data should be fetched into this PrintText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 printTextID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PrintTextFieldIndex.PrintTextID].ForcedCurrentValueWrite(printTextID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePrintTextDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PrintTextEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PrintTextRelations Relations
		{
			get	{ return new PrintTextRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutesPrintText1
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RoutesPrintText1")[0], (int)VarioSL.Entities.EntityType.PrintTextEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "RoutesPrintText1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutesPrintText2
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RoutesPrintText2")[0], (int)VarioSL.Entities.EntityType.PrintTextEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "RoutesPrintText2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoutesPrintText3
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RouteCollection(), (IEntityRelation)GetRelationsForField("RoutesPrintText3")[0], (int)VarioSL.Entities.EntityType.PrintTextEntity, (int)VarioSL.Entities.EntityType.RouteEntity, 0, null, null, null, "RoutesPrintText3", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.PrintTextEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PrintText property of the Entity PrintText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PRINTTEXT"."PRINTTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrintText
		{
			get { return (System.String)GetValue((int)PrintTextFieldIndex.PrintText, true); }
			set	{ SetValue((int)PrintTextFieldIndex.PrintText, value, true); }
		}

		/// <summary> The PrintTextID property of the Entity PrintText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PRINTTEXT"."PRINTTEXTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PrintTextID
		{
			get { return (System.Int64)GetValue((int)PrintTextFieldIndex.PrintTextID, true); }
			set	{ SetValue((int)PrintTextFieldIndex.PrintTextID, value, true); }
		}

		/// <summary> The TariffId property of the Entity PrintText<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_PRINTTEXT"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffId
		{
			get { return (Nullable<System.Int64>)GetValue((int)PrintTextFieldIndex.TariffId, false); }
			set	{ SetValue((int)PrintTextFieldIndex.TariffId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutesPrintText1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection RoutesPrintText1
		{
			get	{ return GetMultiRoutesPrintText1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutesPrintText1. When set to true, RoutesPrintText1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutesPrintText1 is accessed. You can always execute/ a forced fetch by calling GetMultiRoutesPrintText1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutesPrintText1
		{
			get	{ return _alwaysFetchRoutesPrintText1; }
			set	{ _alwaysFetchRoutesPrintText1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutesPrintText1 already has been fetched. Setting this property to false when RoutesPrintText1 has been fetched
		/// will clear the RoutesPrintText1 collection well. Setting this property to true while RoutesPrintText1 hasn't been fetched disables lazy loading for RoutesPrintText1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutesPrintText1
		{
			get { return _alreadyFetchedRoutesPrintText1;}
			set 
			{
				if(_alreadyFetchedRoutesPrintText1 && !value && (_routesPrintText1 != null))
				{
					_routesPrintText1.Clear();
				}
				_alreadyFetchedRoutesPrintText1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutesPrintText2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection RoutesPrintText2
		{
			get	{ return GetMultiRoutesPrintText2(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutesPrintText2. When set to true, RoutesPrintText2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutesPrintText2 is accessed. You can always execute/ a forced fetch by calling GetMultiRoutesPrintText2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutesPrintText2
		{
			get	{ return _alwaysFetchRoutesPrintText2; }
			set	{ _alwaysFetchRoutesPrintText2 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutesPrintText2 already has been fetched. Setting this property to false when RoutesPrintText2 has been fetched
		/// will clear the RoutesPrintText2 collection well. Setting this property to true while RoutesPrintText2 hasn't been fetched disables lazy loading for RoutesPrintText2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutesPrintText2
		{
			get { return _alreadyFetchedRoutesPrintText2;}
			set 
			{
				if(_alreadyFetchedRoutesPrintText2 && !value && (_routesPrintText2 != null))
				{
					_routesPrintText2.Clear();
				}
				_alreadyFetchedRoutesPrintText2 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoutesPrintText3()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RouteCollection RoutesPrintText3
		{
			get	{ return GetMultiRoutesPrintText3(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoutesPrintText3. When set to true, RoutesPrintText3 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoutesPrintText3 is accessed. You can always execute/ a forced fetch by calling GetMultiRoutesPrintText3(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoutesPrintText3
		{
			get	{ return _alwaysFetchRoutesPrintText3; }
			set	{ _alwaysFetchRoutesPrintText3 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoutesPrintText3 already has been fetched. Setting this property to false when RoutesPrintText3 has been fetched
		/// will clear the RoutesPrintText3 collection well. Setting this property to true while RoutesPrintText3 hasn't been fetched disables lazy loading for RoutesPrintText3</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoutesPrintText3
		{
			get { return _alreadyFetchedRoutesPrintText3;}
			set 
			{
				if(_alreadyFetchedRoutesPrintText3 && !value && (_routesPrintText3 != null))
				{
					_routesPrintText3.Clear();
				}
				_alreadyFetchedRoutesPrintText3 = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PrintTexts", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PrintTextEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
