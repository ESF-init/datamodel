﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Voucher'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VoucherEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VoucherEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VoucherEntity() :base("VoucherEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		public VoucherEntity(System.Int64 voucherID):base("VoucherEntity")
		{
			InitClassFetch(voucherID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VoucherEntity(System.Int64 voucherID, IPrefetchPath prefetchPathToUse):base("VoucherEntity")
		{
			InitClassFetch(voucherID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="validator">The custom validator object for this VoucherEntity</param>
		public VoucherEntity(System.Int64 voucherID, IValidator validator):base("VoucherEntity")
		{
			InitClassFetch(voucherID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VoucherEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");
			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VoucherFieldIndex)fieldIndex)
			{
				case VoucherFieldIndex.RedeemedForCardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case VoucherFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProducts = (_products.Count > 0);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedCard = (_card != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingRedeemedForCardID);
					break;
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingVoucherID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_products);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="voucherCode">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCVoucherCode(System.String voucherCode)
		{
			return FetchUsingUCVoucherCode( voucherCode, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="voucherCode">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCVoucherCode(System.String voucherCode, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCVoucherCode( voucherCode, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="voucherCode">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCVoucherCode(System.String voucherCode, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCVoucherCode( voucherCode, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="voucherCode">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCVoucherCode(System.String voucherCode, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((VoucherDAO)CreateDAOInstance()).FetchVoucherUsingUCVoucherCode(this, this.Transaction, voucherCode, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 voucherID)
		{
			return FetchUsingPK(voucherID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 voucherID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(voucherID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 voucherID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(voucherID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 voucherID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(voucherID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.VoucherID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VoucherRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingRedeemedForCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RedeemedForCardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("Card", _card);
			toReturn.Add("Products", _products);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="validator">The validator object for this VoucherEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 voucherID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(voucherID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "Voucher");
			_ticketReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedOn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RedeemableFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RedeemableTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RedeemedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RedeemedForCardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RedeemedOn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoucherCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoucherID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticVoucherRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "Vouchers", resetFKFields, new int[] { (int)VoucherFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticVoucherRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticVoucherRelations.CardEntityUsingRedeemedForCardIDStatic, true, signalRelatedEntity, "Vouchers", resetFKFields, new int[] { (int)VoucherFieldIndex.RedeemedForCardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticVoucherRelations.CardEntityUsingRedeemedForCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="voucherID">PK value for Voucher which data should be fetched into this Voucher object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 voucherID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VoucherFieldIndex.VoucherID].ForcedCurrentValueWrite(voucherID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVoucherDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VoucherEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VoucherRelations Relations
		{
			get	{ return new VoucherRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.VoucherEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.VoucherEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.VoucherEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreatedBy property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."CREATEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreatedBy
		{
			get { return (System.String)GetValue((int)VoucherFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)VoucherFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The CreatedOn property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."CREATEDON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CreatedOn
		{
			get { return (System.DateTime)GetValue((int)VoucherFieldIndex.CreatedOn, true); }
			set	{ SetValue((int)VoucherFieldIndex.CreatedOn, value, true); }
		}

		/// <summary> The LastModified property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)VoucherFieldIndex.LastModified, true); }
			set	{ SetValue((int)VoucherFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)VoucherFieldIndex.LastUser, true); }
			set	{ SetValue((int)VoucherFieldIndex.LastUser, value, true); }
		}

		/// <summary> The RedeemableFrom property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."REDEEMABLEFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime RedeemableFrom
		{
			get { return (System.DateTime)GetValue((int)VoucherFieldIndex.RedeemableFrom, true); }
			set	{ SetValue((int)VoucherFieldIndex.RedeemableFrom, value, true); }
		}

		/// <summary> The RedeemableTo property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."REDEEMABLETO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime RedeemableTo
		{
			get { return (System.DateTime)GetValue((int)VoucherFieldIndex.RedeemableTo, true); }
			set	{ SetValue((int)VoucherFieldIndex.RedeemableTo, value, true); }
		}

		/// <summary> The RedeemedBy property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."REDEEMEDBY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RedeemedBy
		{
			get { return (System.String)GetValue((int)VoucherFieldIndex.RedeemedBy, true); }
			set	{ SetValue((int)VoucherFieldIndex.RedeemedBy, value, true); }
		}

		/// <summary> The RedeemedForCardID property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."REDEEMEDFORCARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RedeemedForCardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VoucherFieldIndex.RedeemedForCardID, false); }
			set	{ SetValue((int)VoucherFieldIndex.RedeemedForCardID, value, true); }
		}

		/// <summary> The RedeemedOn property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."REDEEMEDON"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> RedeemedOn
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VoucherFieldIndex.RedeemedOn, false); }
			set	{ SetValue((int)VoucherFieldIndex.RedeemedOn, value, true); }
		}

		/// <summary> The TicketID property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)VoucherFieldIndex.TicketID, true); }
			set	{ SetValue((int)VoucherFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."TICKETINTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketInternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)VoucherFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)VoucherFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketValidFrom property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."TICKETVALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TicketValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VoucherFieldIndex.TicketValidFrom, false); }
			set	{ SetValue((int)VoucherFieldIndex.TicketValidFrom, value, true); }
		}

		/// <summary> The TicketValidTo property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."TICKETVALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> TicketValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)VoucherFieldIndex.TicketValidTo, false); }
			set	{ SetValue((int)VoucherFieldIndex.TicketValidTo, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)VoucherFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)VoucherFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VoucherCode property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."VOUCHERCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String VoucherCode
		{
			get { return (System.String)GetValue((int)VoucherFieldIndex.VoucherCode, true); }
			set	{ SetValue((int)VoucherFieldIndex.VoucherCode, value, true); }
		}

		/// <summary> The VoucherID property of the Entity Voucher<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_VOUCHER"."VOUCHERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 VoucherID
		{
			get { return (System.Int64)GetValue((int)VoucherFieldIndex.VoucherID, true); }
			set	{ SetValue((int)VoucherFieldIndex.VoucherID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Vouchers", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Vouchers", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VoucherEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
