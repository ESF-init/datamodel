﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomerAccountNotification'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomerAccountNotificationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CustomerAccountEntity _customerAccount;
		private bool	_alwaysFetchCustomerAccount, _alreadyFetchedCustomerAccount, _customerAccountReturnsNewIfNotFound;
		private DeviceTokenEntity _deviceToken;
		private bool	_alwaysFetchDeviceToken, _alreadyFetchedDeviceToken, _deviceTokenReturnsNewIfNotFound;
		private MessageTypeEntity _messageType;
		private bool	_alwaysFetchMessageType, _alreadyFetchedMessageType, _messageTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomerAccount</summary>
			public static readonly string CustomerAccount = "CustomerAccount";
			/// <summary>Member name DeviceToken</summary>
			public static readonly string DeviceToken = "DeviceToken";
			/// <summary>Member name MessageType</summary>
			public static readonly string MessageType = "MessageType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomerAccountNotificationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomerAccountNotificationEntity() :base("CustomerAccountNotificationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		public CustomerAccountNotificationEntity(System.Int64 customerAccountNotificationID):base("CustomerAccountNotificationEntity")
		{
			InitClassFetch(customerAccountNotificationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomerAccountNotificationEntity(System.Int64 customerAccountNotificationID, IPrefetchPath prefetchPathToUse):base("CustomerAccountNotificationEntity")
		{
			InitClassFetch(customerAccountNotificationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="validator">The custom validator object for this CustomerAccountNotificationEntity</param>
		public CustomerAccountNotificationEntity(System.Int64 customerAccountNotificationID, IValidator validator):base("CustomerAccountNotificationEntity")
		{
			InitClassFetch(customerAccountNotificationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomerAccountNotificationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customerAccount = (CustomerAccountEntity)info.GetValue("_customerAccount", typeof(CustomerAccountEntity));
			if(_customerAccount!=null)
			{
				_customerAccount.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerAccountReturnsNewIfNotFound = info.GetBoolean("_customerAccountReturnsNewIfNotFound");
			_alwaysFetchCustomerAccount = info.GetBoolean("_alwaysFetchCustomerAccount");
			_alreadyFetchedCustomerAccount = info.GetBoolean("_alreadyFetchedCustomerAccount");

			_deviceToken = (DeviceTokenEntity)info.GetValue("_deviceToken", typeof(DeviceTokenEntity));
			if(_deviceToken!=null)
			{
				_deviceToken.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceTokenReturnsNewIfNotFound = info.GetBoolean("_deviceTokenReturnsNewIfNotFound");
			_alwaysFetchDeviceToken = info.GetBoolean("_alwaysFetchDeviceToken");
			_alreadyFetchedDeviceToken = info.GetBoolean("_alreadyFetchedDeviceToken");

			_messageType = (MessageTypeEntity)info.GetValue("_messageType", typeof(MessageTypeEntity));
			if(_messageType!=null)
			{
				_messageType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_messageTypeReturnsNewIfNotFound = info.GetBoolean("_messageTypeReturnsNewIfNotFound");
			_alwaysFetchMessageType = info.GetBoolean("_alwaysFetchMessageType");
			_alreadyFetchedMessageType = info.GetBoolean("_alreadyFetchedMessageType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CustomerAccountNotificationFieldIndex)fieldIndex)
			{
				case CustomerAccountNotificationFieldIndex.CustomerAccountID:
					DesetupSyncCustomerAccount(true, false);
					_alreadyFetchedCustomerAccount = false;
					break;
				case CustomerAccountNotificationFieldIndex.DeviceTokenID:
					DesetupSyncDeviceToken(true, false);
					_alreadyFetchedDeviceToken = false;
					break;
				case CustomerAccountNotificationFieldIndex.MessageTypeID:
					DesetupSyncMessageType(true, false);
					_alreadyFetchedMessageType = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomerAccount = (_customerAccount != null);
			_alreadyFetchedDeviceToken = (_deviceToken != null);
			_alreadyFetchedMessageType = (_messageType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomerAccount":
					toReturn.Add(Relations.CustomerAccountEntityUsingCustomerAccountID);
					break;
				case "DeviceToken":
					toReturn.Add(Relations.DeviceTokenEntityUsingDeviceTokenID);
					break;
				case "MessageType":
					toReturn.Add(Relations.MessageTypeEntityUsingMessageTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customerAccount", (!this.MarkedForDeletion?_customerAccount:null));
			info.AddValue("_customerAccountReturnsNewIfNotFound", _customerAccountReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerAccount", _alwaysFetchCustomerAccount);
			info.AddValue("_alreadyFetchedCustomerAccount", _alreadyFetchedCustomerAccount);
			info.AddValue("_deviceToken", (!this.MarkedForDeletion?_deviceToken:null));
			info.AddValue("_deviceTokenReturnsNewIfNotFound", _deviceTokenReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceToken", _alwaysFetchDeviceToken);
			info.AddValue("_alreadyFetchedDeviceToken", _alreadyFetchedDeviceToken);
			info.AddValue("_messageType", (!this.MarkedForDeletion?_messageType:null));
			info.AddValue("_messageTypeReturnsNewIfNotFound", _messageTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMessageType", _alwaysFetchMessageType);
			info.AddValue("_alreadyFetchedMessageType", _alreadyFetchedMessageType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomerAccount":
					_alreadyFetchedCustomerAccount = true;
					this.CustomerAccount = (CustomerAccountEntity)entity;
					break;
				case "DeviceToken":
					_alreadyFetchedDeviceToken = true;
					this.DeviceToken = (DeviceTokenEntity)entity;
					break;
				case "MessageType":
					_alreadyFetchedMessageType = true;
					this.MessageType = (MessageTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomerAccount":
					SetupSyncCustomerAccount(relatedEntity);
					break;
				case "DeviceToken":
					SetupSyncDeviceToken(relatedEntity);
					break;
				case "MessageType":
					SetupSyncMessageType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomerAccount":
					DesetupSyncCustomerAccount(false, true);
					break;
				case "DeviceToken":
					DesetupSyncDeviceToken(false, true);
					break;
				case "MessageType":
					DesetupSyncMessageType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customerAccount!=null)
			{
				toReturn.Add(_customerAccount);
			}
			if(_deviceToken!=null)
			{
				toReturn.Add(_deviceToken);
			}
			if(_messageType!=null)
			{
				toReturn.Add(_messageType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountNotificationID)
		{
			return FetchUsingPK(customerAccountNotificationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountNotificationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customerAccountNotificationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountNotificationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customerAccountNotificationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customerAccountNotificationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customerAccountNotificationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomerAccountNotificationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomerAccountNotificationRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CustomerAccountEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerAccountEntity' which is related to this entity.</returns>
		public CustomerAccountEntity GetSingleCustomerAccount()
		{
			return GetSingleCustomerAccount(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerAccountEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerAccountEntity' which is related to this entity.</returns>
		public virtual CustomerAccountEntity GetSingleCustomerAccount(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerAccount || forceFetch || _alwaysFetchCustomerAccount) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerAccountEntityUsingCustomerAccountID);
				CustomerAccountEntity newEntity = new CustomerAccountEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerAccountID);
				}
				if(fetchResult)
				{
					newEntity = (CustomerAccountEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerAccountReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerAccount = newEntity;
				_alreadyFetchedCustomerAccount = fetchResult;
			}
			return _customerAccount;
		}


		/// <summary> Retrieves the related entity of type 'DeviceTokenEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceTokenEntity' which is related to this entity.</returns>
		public DeviceTokenEntity GetSingleDeviceToken()
		{
			return GetSingleDeviceToken(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceTokenEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceTokenEntity' which is related to this entity.</returns>
		public virtual DeviceTokenEntity GetSingleDeviceToken(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceToken || forceFetch || _alwaysFetchDeviceToken) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceTokenEntityUsingDeviceTokenID);
				DeviceTokenEntity newEntity = new DeviceTokenEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceTokenID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceTokenEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceTokenReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceToken = newEntity;
				_alreadyFetchedDeviceToken = fetchResult;
			}
			return _deviceToken;
		}


		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public MessageTypeEntity GetSingleMessageType()
		{
			return GetSingleMessageType(false);
		}

		/// <summary> Retrieves the related entity of type 'MessageTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MessageTypeEntity' which is related to this entity.</returns>
		public virtual MessageTypeEntity GetSingleMessageType(bool forceFetch)
		{
			if( ( !_alreadyFetchedMessageType || forceFetch || _alwaysFetchMessageType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MessageTypeEntityUsingMessageTypeID);
				MessageTypeEntity newEntity = new MessageTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MessageTypeID);
				}
				if(fetchResult)
				{
					newEntity = (MessageTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_messageTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MessageType = newEntity;
				_alreadyFetchedMessageType = fetchResult;
			}
			return _messageType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomerAccount", _customerAccount);
			toReturn.Add("DeviceToken", _deviceToken);
			toReturn.Add("MessageType", _messageType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="validator">The validator object for this CustomerAccountNotificationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customerAccountNotificationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customerAccountNotificationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_customerAccountReturnsNewIfNotFound = false;
			_deviceTokenReturnsNewIfNotFound = false;
			_messageTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerAccountNotificationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTokenID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Enabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SendTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customerAccount</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerAccount(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerAccount, new PropertyChangedEventHandler( OnCustomerAccountPropertyChanged ), "CustomerAccount", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.CustomerAccountEntityUsingCustomerAccountIDStatic, true, signalRelatedEntity, "CustomerAccountNotifications", resetFKFields, new int[] { (int)CustomerAccountNotificationFieldIndex.CustomerAccountID } );		
			_customerAccount = null;
		}
		
		/// <summary> setups the sync logic for member _customerAccount</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerAccount(IEntityCore relatedEntity)
		{
			if(_customerAccount!=relatedEntity)
			{		
				DesetupSyncCustomerAccount(true, true);
				_customerAccount = (CustomerAccountEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerAccount, new PropertyChangedEventHandler( OnCustomerAccountPropertyChanged ), "CustomerAccount", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.CustomerAccountEntityUsingCustomerAccountIDStatic, true, ref _alreadyFetchedCustomerAccount, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerAccountPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceToken</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceToken(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceToken, new PropertyChangedEventHandler( OnDeviceTokenPropertyChanged ), "DeviceToken", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.DeviceTokenEntityUsingDeviceTokenIDStatic, true, signalRelatedEntity, "CustomerAccountNotifications", resetFKFields, new int[] { (int)CustomerAccountNotificationFieldIndex.DeviceTokenID } );		
			_deviceToken = null;
		}
		
		/// <summary> setups the sync logic for member _deviceToken</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceToken(IEntityCore relatedEntity)
		{
			if(_deviceToken!=relatedEntity)
			{		
				DesetupSyncDeviceToken(true, true);
				_deviceToken = (DeviceTokenEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceToken, new PropertyChangedEventHandler( OnDeviceTokenPropertyChanged ), "DeviceToken", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.DeviceTokenEntityUsingDeviceTokenIDStatic, true, ref _alreadyFetchedDeviceToken, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceTokenPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _messageType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMessageType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _messageType, new PropertyChangedEventHandler( OnMessageTypePropertyChanged ), "MessageType", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.MessageTypeEntityUsingMessageTypeIDStatic, true, signalRelatedEntity, "CustomerAccountNotifications", resetFKFields, new int[] { (int)CustomerAccountNotificationFieldIndex.MessageTypeID } );		
			_messageType = null;
		}
		
		/// <summary> setups the sync logic for member _messageType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMessageType(IEntityCore relatedEntity)
		{
			if(_messageType!=relatedEntity)
			{		
				DesetupSyncMessageType(true, true);
				_messageType = (MessageTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _messageType, new PropertyChangedEventHandler( OnMessageTypePropertyChanged ), "MessageType", VarioSL.Entities.RelationClasses.StaticCustomerAccountNotificationRelations.MessageTypeEntityUsingMessageTypeIDStatic, true, ref _alreadyFetchedMessageType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMessageTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customerAccountNotificationID">PK value for CustomerAccountNotification which data should be fetched into this CustomerAccountNotification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customerAccountNotificationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomerAccountNotificationFieldIndex.CustomerAccountNotificationID].ForcedCurrentValueWrite(customerAccountNotificationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomerAccountNotificationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomerAccountNotificationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomerAccountNotificationRelations Relations
		{
			get	{ return new CustomerAccountNotificationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomerAccount'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerAccount
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomerAccountCollection(), (IEntityRelation)GetRelationsForField("CustomerAccount")[0], (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, (int)VarioSL.Entities.EntityType.CustomerAccountEntity, 0, null, null, null, "CustomerAccount", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceToken'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceToken
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceTokenCollection(), (IEntityRelation)GetRelationsForField("DeviceToken")[0], (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, (int)VarioSL.Entities.EntityType.DeviceTokenEntity, 0, null, null, null, "DeviceToken", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.MessageTypeCollection(), (IEntityRelation)GetRelationsForField("MessageType")[0], (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity, (int)VarioSL.Entities.EntityType.MessageTypeEntity, 0, null, null, null, "MessageType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomerAccountID property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."CUSTOMERACCOUNTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CustomerAccountID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountNotificationFieldIndex.CustomerAccountID, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.CustomerAccountID, value, true); }
		}

		/// <summary> The CustomerAccountNotificationID property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."CUSTOMERACCOUNTNOTIFICATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CustomerAccountNotificationID
		{
			get { return (System.Int64)GetValue((int)CustomerAccountNotificationFieldIndex.CustomerAccountNotificationID, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.CustomerAccountNotificationID, value, true); }
		}

		/// <summary> The DeviceTokenID property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."DEVICETOKENID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceTokenID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CustomerAccountNotificationFieldIndex.DeviceTokenID, false); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.DeviceTokenID, value, true); }
		}

		/// <summary> The Enabled property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."ENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Enabled
		{
			get { return (System.Boolean)GetValue((int)CustomerAccountNotificationFieldIndex.Enabled, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.Enabled, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CustomerAccountNotificationFieldIndex.LastModified, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomerAccountNotificationFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MessageTypeID property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."MESSAGETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessageTypeID
		{
			get { return (System.Int32)GetValue((int)CustomerAccountNotificationFieldIndex.MessageTypeID, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.MessageTypeID, value, true); }
		}

		/// <summary> The SendTo property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."SENDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 60<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SendTo
		{
			get { return (System.String)GetValue((int)CustomerAccountNotificationFieldIndex.SendTo, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.SendTo, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomerAccountNotification<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CUSTOMERACCOUNTNOTIFICATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomerAccountNotificationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomerAccountNotificationFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CustomerAccountEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerAccount()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CustomerAccountEntity CustomerAccount
		{
			get	{ return GetSingleCustomerAccount(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerAccount(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccountNotifications", "CustomerAccount", _customerAccount, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerAccount. When set to true, CustomerAccount is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerAccount is accessed. You can always execute a forced fetch by calling GetSingleCustomerAccount(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerAccount
		{
			get	{ return _alwaysFetchCustomerAccount; }
			set	{ _alwaysFetchCustomerAccount = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerAccount already has been fetched. Setting this property to false when CustomerAccount has been fetched
		/// will set CustomerAccount to null as well. Setting this property to true while CustomerAccount hasn't been fetched disables lazy loading for CustomerAccount</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerAccount
		{
			get { return _alreadyFetchedCustomerAccount;}
			set 
			{
				if(_alreadyFetchedCustomerAccount && !value)
				{
					this.CustomerAccount = null;
				}
				_alreadyFetchedCustomerAccount = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerAccount is not found
		/// in the database. When set to true, CustomerAccount will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CustomerAccountReturnsNewIfNotFound
		{
			get	{ return _customerAccountReturnsNewIfNotFound; }
			set { _customerAccountReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceTokenEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceToken()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceTokenEntity DeviceToken
		{
			get	{ return GetSingleDeviceToken(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceToken(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccountNotifications", "DeviceToken", _deviceToken, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceToken. When set to true, DeviceToken is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceToken is accessed. You can always execute a forced fetch by calling GetSingleDeviceToken(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceToken
		{
			get	{ return _alwaysFetchDeviceToken; }
			set	{ _alwaysFetchDeviceToken = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceToken already has been fetched. Setting this property to false when DeviceToken has been fetched
		/// will set DeviceToken to null as well. Setting this property to true while DeviceToken hasn't been fetched disables lazy loading for DeviceToken</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceToken
		{
			get { return _alreadyFetchedDeviceToken;}
			set 
			{
				if(_alreadyFetchedDeviceToken && !value)
				{
					this.DeviceToken = null;
				}
				_alreadyFetchedDeviceToken = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceToken is not found
		/// in the database. When set to true, DeviceToken will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceTokenReturnsNewIfNotFound
		{
			get	{ return _deviceTokenReturnsNewIfNotFound; }
			set { _deviceTokenReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MessageTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMessageType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual MessageTypeEntity MessageType
		{
			get	{ return GetSingleMessageType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMessageType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CustomerAccountNotifications", "MessageType", _messageType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MessageType. When set to true, MessageType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageType is accessed. You can always execute a forced fetch by calling GetSingleMessageType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageType
		{
			get	{ return _alwaysFetchMessageType; }
			set	{ _alwaysFetchMessageType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageType already has been fetched. Setting this property to false when MessageType has been fetched
		/// will set MessageType to null as well. Setting this property to true while MessageType hasn't been fetched disables lazy loading for MessageType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageType
		{
			get { return _alreadyFetchedMessageType;}
			set 
			{
				if(_alreadyFetchedMessageType && !value)
				{
					this.MessageType = null;
				}
				_alreadyFetchedMessageType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MessageType is not found
		/// in the database. When set to true, MessageType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool MessageTypeReturnsNewIfNotFound
		{
			get	{ return _messageTypeReturnsNewIfNotFound; }
			set { _messageTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomerAccountNotificationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
