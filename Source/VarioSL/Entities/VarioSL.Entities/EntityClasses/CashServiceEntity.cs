﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CashService'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CashServiceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection	_cashServiceBalance;
		private bool	_alwaysFetchCashServiceBalance, _alreadyFetchedCashServiceBalance;
		private VarioSL.Entities.CollectionClasses.CashServiceDataCollection	_amCashservicedatas;
		private bool	_alwaysFetchAmCashservicedatas, _alreadyFetchedAmCashservicedatas;
		private AutomatEntity _automat;
		private bool	_alwaysFetchAutomat, _alreadyFetchedAutomat, _automatReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Automat</summary>
			public static readonly string Automat = "Automat";
			/// <summary>Member name CashServiceBalance</summary>
			public static readonly string CashServiceBalance = "CashServiceBalance";
			/// <summary>Member name AmCashservicedatas</summary>
			public static readonly string AmCashservicedatas = "AmCashservicedatas";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CashServiceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CashServiceEntity() :base("CashServiceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		public CashServiceEntity(System.Int64 cashServiceID):base("CashServiceEntity")
		{
			InitClassFetch(cashServiceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CashServiceEntity(System.Int64 cashServiceID, IPrefetchPath prefetchPathToUse):base("CashServiceEntity")
		{
			InitClassFetch(cashServiceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="validator">The custom validator object for this CashServiceEntity</param>
		public CashServiceEntity(System.Int64 cashServiceID, IValidator validator):base("CashServiceEntity")
		{
			InitClassFetch(cashServiceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CashServiceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_cashServiceBalance = (VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection)info.GetValue("_cashServiceBalance", typeof(VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection));
			_alwaysFetchCashServiceBalance = info.GetBoolean("_alwaysFetchCashServiceBalance");
			_alreadyFetchedCashServiceBalance = info.GetBoolean("_alreadyFetchedCashServiceBalance");

			_amCashservicedatas = (VarioSL.Entities.CollectionClasses.CashServiceDataCollection)info.GetValue("_amCashservicedatas", typeof(VarioSL.Entities.CollectionClasses.CashServiceDataCollection));
			_alwaysFetchAmCashservicedatas = info.GetBoolean("_alwaysFetchAmCashservicedatas");
			_alreadyFetchedAmCashservicedatas = info.GetBoolean("_alreadyFetchedAmCashservicedatas");
			_automat = (AutomatEntity)info.GetValue("_automat", typeof(AutomatEntity));
			if(_automat!=null)
			{
				_automat.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_automatReturnsNewIfNotFound = info.GetBoolean("_automatReturnsNewIfNotFound");
			_alwaysFetchAutomat = info.GetBoolean("_alwaysFetchAutomat");
			_alreadyFetchedAutomat = info.GetBoolean("_alreadyFetchedAutomat");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CashServiceFieldIndex)fieldIndex)
			{
				case CashServiceFieldIndex.AutomatID:
					DesetupSyncAutomat(true, false);
					_alreadyFetchedAutomat = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCashServiceBalance = (_cashServiceBalance.Count > 0);
			_alreadyFetchedAmCashservicedatas = (_amCashservicedatas.Count > 0);
			_alreadyFetchedAutomat = (_automat != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Automat":
					toReturn.Add(Relations.AutomatEntityUsingAutomatID);
					break;
				case "CashServiceBalance":
					toReturn.Add(Relations.CashServiceBalanceEntityUsingCashServiceID);
					break;
				case "AmCashservicedatas":
					toReturn.Add(Relations.CashServiceDataEntityUsingCashServiceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_cashServiceBalance", (!this.MarkedForDeletion?_cashServiceBalance:null));
			info.AddValue("_alwaysFetchCashServiceBalance", _alwaysFetchCashServiceBalance);
			info.AddValue("_alreadyFetchedCashServiceBalance", _alreadyFetchedCashServiceBalance);
			info.AddValue("_amCashservicedatas", (!this.MarkedForDeletion?_amCashservicedatas:null));
			info.AddValue("_alwaysFetchAmCashservicedatas", _alwaysFetchAmCashservicedatas);
			info.AddValue("_alreadyFetchedAmCashservicedatas", _alreadyFetchedAmCashservicedatas);
			info.AddValue("_automat", (!this.MarkedForDeletion?_automat:null));
			info.AddValue("_automatReturnsNewIfNotFound", _automatReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAutomat", _alwaysFetchAutomat);
			info.AddValue("_alreadyFetchedAutomat", _alreadyFetchedAutomat);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Automat":
					_alreadyFetchedAutomat = true;
					this.Automat = (AutomatEntity)entity;
					break;
				case "CashServiceBalance":
					_alreadyFetchedCashServiceBalance = true;
					if(entity!=null)
					{
						this.CashServiceBalance.Add((CashServiceBalanceEntity)entity);
					}
					break;
				case "AmCashservicedatas":
					_alreadyFetchedAmCashservicedatas = true;
					if(entity!=null)
					{
						this.AmCashservicedatas.Add((CashServiceDataEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Automat":
					SetupSyncAutomat(relatedEntity);
					break;
				case "CashServiceBalance":
					_cashServiceBalance.Add((CashServiceBalanceEntity)relatedEntity);
					break;
				case "AmCashservicedatas":
					_amCashservicedatas.Add((CashServiceDataEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Automat":
					DesetupSyncAutomat(false, true);
					break;
				case "CashServiceBalance":
					this.PerformRelatedEntityRemoval(_cashServiceBalance, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AmCashservicedatas":
					this.PerformRelatedEntityRemoval(_amCashservicedatas, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_automat!=null)
			{
				toReturn.Add(_automat);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_cashServiceBalance);
			toReturn.Add(_amCashservicedatas);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceID)
		{
			return FetchUsingPK(cashServiceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(cashServiceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(cashServiceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 cashServiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(cashServiceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CashServiceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CashServiceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceBalanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch)
		{
			return GetMultiCashServiceBalance(forceFetch, _cashServiceBalance.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceBalanceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCashServiceBalance(forceFetch, _cashServiceBalance.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCashServiceBalance(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection GetMultiCashServiceBalance(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCashServiceBalance || forceFetch || _alwaysFetchCashServiceBalance) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_cashServiceBalance);
				_cashServiceBalance.SuppressClearInGetMulti=!forceFetch;
				_cashServiceBalance.EntityFactoryToUse = entityFactoryToUse;
				_cashServiceBalance.GetMultiManyToOne(this, null, filter);
				_cashServiceBalance.SuppressClearInGetMulti=false;
				_alreadyFetchedCashServiceBalance = true;
			}
			return _cashServiceBalance;
		}

		/// <summary> Sets the collection parameters for the collection for 'CashServiceBalance'. These settings will be taken into account
		/// when the property CashServiceBalance is requested or GetMultiCashServiceBalance is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCashServiceBalance(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_cashServiceBalance.SortClauses=sortClauses;
			_cashServiceBalance.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch)
		{
			return GetMultiAmCashservicedatas(forceFetch, _amCashservicedatas.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CashServiceDataEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAmCashservicedatas(forceFetch, _amCashservicedatas.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAmCashservicedatas(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceDataCollection GetMultiAmCashservicedatas(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAmCashservicedatas || forceFetch || _alwaysFetchAmCashservicedatas) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_amCashservicedatas);
				_amCashservicedatas.SuppressClearInGetMulti=!forceFetch;
				_amCashservicedatas.EntityFactoryToUse = entityFactoryToUse;
				_amCashservicedatas.GetMultiManyToOne(this, null, filter);
				_amCashservicedatas.SuppressClearInGetMulti=false;
				_alreadyFetchedAmCashservicedatas = true;
			}
			return _amCashservicedatas;
		}

		/// <summary> Sets the collection parameters for the collection for 'AmCashservicedatas'. These settings will be taken into account
		/// when the property AmCashservicedatas is requested or GetMultiAmCashservicedatas is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAmCashservicedatas(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_amCashservicedatas.SortClauses=sortClauses;
			_amCashservicedatas.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public AutomatEntity GetSingleAutomat()
		{
			return GetSingleAutomat(false);
		}

		/// <summary> Retrieves the related entity of type 'AutomatEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AutomatEntity' which is related to this entity.</returns>
		public virtual AutomatEntity GetSingleAutomat(bool forceFetch)
		{
			if( ( !_alreadyFetchedAutomat || forceFetch || _alwaysFetchAutomat) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AutomatEntityUsingAutomatID);
				AutomatEntity newEntity = new AutomatEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AutomatID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AutomatEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_automatReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Automat = newEntity;
				_alreadyFetchedAutomat = fetchResult;
			}
			return _automat;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Automat", _automat);
			toReturn.Add("CashServiceBalance", _cashServiceBalance);
			toReturn.Add("AmCashservicedatas", _amCashservicedatas);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="validator">The validator object for this CashServiceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 cashServiceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(cashServiceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_cashServiceBalance = new VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection();
			_cashServiceBalance.SetContainingEntityInfo(this, "CashService");

			_amCashservicedatas = new VarioSL.Entities.CollectionClasses.CashServiceDataCollection();
			_amCashservicedatas.SetContainingEntityInfo(this, "CashService");
			_automatReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutomatID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashAmountBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashAmountEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Cashpaymentid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashServiceBegin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashServiceEnd", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CashServiceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsComplete", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsLastCashService", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceStaffNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _automat</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAutomat(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticCashServiceRelations.AutomatEntityUsingAutomatIDStatic, true, signalRelatedEntity, "AmCashservices", resetFKFields, new int[] { (int)CashServiceFieldIndex.AutomatID } );		
			_automat = null;
		}
		
		/// <summary> setups the sync logic for member _automat</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAutomat(IEntityCore relatedEntity)
		{
			if(_automat!=relatedEntity)
			{		
				DesetupSyncAutomat(true, true);
				_automat = (AutomatEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _automat, new PropertyChangedEventHandler( OnAutomatPropertyChanged ), "Automat", VarioSL.Entities.RelationClasses.StaticCashServiceRelations.AutomatEntityUsingAutomatIDStatic, true, ref _alreadyFetchedAutomat, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAutomatPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="cashServiceID">PK value for CashService which data should be fetched into this CashService object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 cashServiceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CashServiceFieldIndex.CashServiceID].ForcedCurrentValueWrite(cashServiceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCashServiceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CashServiceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CashServiceRelations Relations
		{
			get	{ return new CashServiceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashServiceBalance' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCashServiceBalance
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection(), (IEntityRelation)GetRelationsForField("CashServiceBalance")[0], (int)VarioSL.Entities.EntityType.CashServiceEntity, (int)VarioSL.Entities.EntityType.CashServiceBalanceEntity, 0, null, null, null, "CashServiceBalance", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CashServiceData' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmCashservicedatas
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CashServiceDataCollection(), (IEntityRelation)GetRelationsForField("AmCashservicedatas")[0], (int)VarioSL.Entities.EntityType.CashServiceEntity, (int)VarioSL.Entities.EntityType.CashServiceDataEntity, 0, null, null, null, "AmCashservicedatas", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Automat'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutomat
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutomatCollection(), (IEntityRelation)GetRelationsForField("Automat")[0], (int)VarioSL.Entities.EntityType.CashServiceEntity, (int)VarioSL.Entities.EntityType.AutomatEntity, 0, null, null, null, "Automat", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutomatID property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."AUTOMATID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AutomatID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.AutomatID, false); }
			set	{ SetValue((int)CashServiceFieldIndex.AutomatID, value, true); }
		}

		/// <summary> The CashAmountBegin property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHAMOUNTBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashAmountBegin
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.CashAmountBegin, false); }
			set	{ SetValue((int)CashServiceFieldIndex.CashAmountBegin, value, true); }
		}

		/// <summary> The CashAmountEnd property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHAMOUNTEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CashAmountEnd
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.CashAmountEnd, false); }
			set	{ SetValue((int)CashServiceFieldIndex.CashAmountEnd, value, true); }
		}

		/// <summary> The Cashpaymentid property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHPAYMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Cashpaymentid
		{
			get { return (System.Int64)GetValue((int)CashServiceFieldIndex.Cashpaymentid, true); }
			set	{ SetValue((int)CashServiceFieldIndex.Cashpaymentid, value, true); }
		}

		/// <summary> The CashServiceBegin property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHSERVICEBEGIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CashServiceBegin
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CashServiceFieldIndex.CashServiceBegin, false); }
			set	{ SetValue((int)CashServiceFieldIndex.CashServiceBegin, value, true); }
		}

		/// <summary> The CashServiceEnd property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHSERVICEEND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CashServiceEnd
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CashServiceFieldIndex.CashServiceEnd, false); }
			set	{ SetValue((int)CashServiceFieldIndex.CashServiceEnd, value, true); }
		}

		/// <summary> The CashServiceID property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CASHSERVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CashServiceID
		{
			get { return (System.Int64)GetValue((int)CashServiceFieldIndex.CashServiceID, true); }
			set	{ SetValue((int)CashServiceFieldIndex.CashServiceID, value, true); }
		}

		/// <summary> The ClientID property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.ClientID, false); }
			set	{ SetValue((int)CashServiceFieldIndex.ClientID, value, true); }
		}

		/// <summary> The DebtorID property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.DebtorID, false); }
			set	{ SetValue((int)CashServiceFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The IsComplete property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."ISCOMPLETE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsComplete
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CashServiceFieldIndex.IsComplete, false); }
			set	{ SetValue((int)CashServiceFieldIndex.IsComplete, value, true); }
		}

		/// <summary> The IsLastCashService property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."ISLASTCASHSERVICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsLastCashService
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CashServiceFieldIndex.IsLastCashService, false); }
			set	{ SetValue((int)CashServiceFieldIndex.IsLastCashService, value, true); }
		}

		/// <summary> The ServiceStaffNumber property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."SERVICESTAFFNO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ServiceStaffNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.ServiceStaffNumber, false); }
			set	{ SetValue((int)CashServiceFieldIndex.ServiceStaffNumber, value, true); }
		}

		/// <summary> The SettlementID property of the Entity CashService<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AM_CASHSERVICE"."SETTLEMENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)CashServiceFieldIndex.SettlementID, false); }
			set	{ SetValue((int)CashServiceFieldIndex.SettlementID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CashServiceBalanceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCashServiceBalance()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceBalanceCollection CashServiceBalance
		{
			get	{ return GetMultiCashServiceBalance(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CashServiceBalance. When set to true, CashServiceBalance is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CashServiceBalance is accessed. You can always execute/ a forced fetch by calling GetMultiCashServiceBalance(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCashServiceBalance
		{
			get	{ return _alwaysFetchCashServiceBalance; }
			set	{ _alwaysFetchCashServiceBalance = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CashServiceBalance already has been fetched. Setting this property to false when CashServiceBalance has been fetched
		/// will clear the CashServiceBalance collection well. Setting this property to true while CashServiceBalance hasn't been fetched disables lazy loading for CashServiceBalance</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCashServiceBalance
		{
			get { return _alreadyFetchedCashServiceBalance;}
			set 
			{
				if(_alreadyFetchedCashServiceBalance && !value && (_cashServiceBalance != null))
				{
					_cashServiceBalance.Clear();
				}
				_alreadyFetchedCashServiceBalance = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CashServiceDataEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAmCashservicedatas()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CashServiceDataCollection AmCashservicedatas
		{
			get	{ return GetMultiAmCashservicedatas(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AmCashservicedatas. When set to true, AmCashservicedatas is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmCashservicedatas is accessed. You can always execute/ a forced fetch by calling GetMultiAmCashservicedatas(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmCashservicedatas
		{
			get	{ return _alwaysFetchAmCashservicedatas; }
			set	{ _alwaysFetchAmCashservicedatas = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmCashservicedatas already has been fetched. Setting this property to false when AmCashservicedatas has been fetched
		/// will clear the AmCashservicedatas collection well. Setting this property to true while AmCashservicedatas hasn't been fetched disables lazy loading for AmCashservicedatas</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmCashservicedatas
		{
			get { return _alreadyFetchedAmCashservicedatas;}
			set 
			{
				if(_alreadyFetchedAmCashservicedatas && !value && (_amCashservicedatas != null))
				{
					_amCashservicedatas.Clear();
				}
				_alreadyFetchedAmCashservicedatas = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'AutomatEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAutomat()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AutomatEntity Automat
		{
			get	{ return GetSingleAutomat(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAutomat(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AmCashservices", "Automat", _automat, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Automat. When set to true, Automat is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Automat is accessed. You can always execute a forced fetch by calling GetSingleAutomat(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutomat
		{
			get	{ return _alwaysFetchAutomat; }
			set	{ _alwaysFetchAutomat = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Automat already has been fetched. Setting this property to false when Automat has been fetched
		/// will set Automat to null as well. Setting this property to true while Automat hasn't been fetched disables lazy loading for Automat</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutomat
		{
			get { return _alreadyFetchedAutomat;}
			set 
			{
				if(_alreadyFetchedAutomat && !value)
				{
					this.Automat = null;
				}
				_alreadyFetchedAutomat = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Automat is not found
		/// in the database. When set to true, Automat will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AutomatReturnsNewIfNotFound
		{
			get	{ return _automatReturnsNewIfNotFound; }
			set { _automatReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CashServiceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
