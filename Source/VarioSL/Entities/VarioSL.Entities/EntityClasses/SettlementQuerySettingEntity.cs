﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SettlementQuerySetting'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SettlementQuerySettingEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection	_settlementQuerySettingToTickets;
		private bool	_alwaysFetchSettlementQuerySettingToTickets, _alreadyFetchedSettlementQuerySettingToTickets;
		private VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection	_settlementQueryValues;
		private bool	_alwaysFetchSettlementQueryValues, _alreadyFetchedSettlementQueryValues;
		private VarioSL.Entities.CollectionClasses.TicketCollection _tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private SettlementCalendarEntity _settlementCalendar;
		private bool	_alwaysFetchSettlementCalendar, _alreadyFetchedSettlementCalendar, _settlementCalendarReturnsNewIfNotFound;
		private SettlementSetupEntity _settlementSetup;
		private bool	_alwaysFetchSettlementSetup, _alreadyFetchedSettlementSetup, _settlementSetupReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SettlementCalendar</summary>
			public static readonly string SettlementCalendar = "SettlementCalendar";
			/// <summary>Member name SettlementSetup</summary>
			public static readonly string SettlementSetup = "SettlementSetup";
			/// <summary>Member name SettlementQuerySettingToTickets</summary>
			public static readonly string SettlementQuerySettingToTickets = "SettlementQuerySettingToTickets";
			/// <summary>Member name SettlementQueryValues</summary>
			public static readonly string SettlementQueryValues = "SettlementQueryValues";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SettlementQuerySettingEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SettlementQuerySettingEntity() :base("SettlementQuerySettingEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		public SettlementQuerySettingEntity(System.Int64 settlementQuerySettingID):base("SettlementQuerySettingEntity")
		{
			InitClassFetch(settlementQuerySettingID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SettlementQuerySettingEntity(System.Int64 settlementQuerySettingID, IPrefetchPath prefetchPathToUse):base("SettlementQuerySettingEntity")
		{
			InitClassFetch(settlementQuerySettingID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="validator">The custom validator object for this SettlementQuerySettingEntity</param>
		public SettlementQuerySettingEntity(System.Int64 settlementQuerySettingID, IValidator validator):base("SettlementQuerySettingEntity")
		{
			InitClassFetch(settlementQuerySettingID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SettlementQuerySettingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_settlementQuerySettingToTickets = (VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection)info.GetValue("_settlementQuerySettingToTickets", typeof(VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection));
			_alwaysFetchSettlementQuerySettingToTickets = info.GetBoolean("_alwaysFetchSettlementQuerySettingToTickets");
			_alreadyFetchedSettlementQuerySettingToTickets = info.GetBoolean("_alreadyFetchedSettlementQuerySettingToTickets");

			_settlementQueryValues = (VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection)info.GetValue("_settlementQueryValues", typeof(VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection));
			_alwaysFetchSettlementQueryValues = info.GetBoolean("_alwaysFetchSettlementQueryValues");
			_alreadyFetchedSettlementQueryValues = info.GetBoolean("_alreadyFetchedSettlementQueryValues");
			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");
			_settlementCalendar = (SettlementCalendarEntity)info.GetValue("_settlementCalendar", typeof(SettlementCalendarEntity));
			if(_settlementCalendar!=null)
			{
				_settlementCalendar.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_settlementCalendarReturnsNewIfNotFound = info.GetBoolean("_settlementCalendarReturnsNewIfNotFound");
			_alwaysFetchSettlementCalendar = info.GetBoolean("_alwaysFetchSettlementCalendar");
			_alreadyFetchedSettlementCalendar = info.GetBoolean("_alreadyFetchedSettlementCalendar");

			_settlementSetup = (SettlementSetupEntity)info.GetValue("_settlementSetup", typeof(SettlementSetupEntity));
			if(_settlementSetup!=null)
			{
				_settlementSetup.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_settlementSetupReturnsNewIfNotFound = info.GetBoolean("_settlementSetupReturnsNewIfNotFound");
			_alwaysFetchSettlementSetup = info.GetBoolean("_alwaysFetchSettlementSetup");
			_alreadyFetchedSettlementSetup = info.GetBoolean("_alreadyFetchedSettlementSetup");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SettlementQuerySettingFieldIndex)fieldIndex)
			{
				case SettlementQuerySettingFieldIndex.Settlementcalendarid:
					DesetupSyncSettlementCalendar(true, false);
					_alreadyFetchedSettlementCalendar = false;
					break;
				case SettlementQuerySettingFieldIndex.SettlementSetupID:
					DesetupSyncSettlementSetup(true, false);
					_alreadyFetchedSettlementSetup = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSettlementQuerySettingToTickets = (_settlementQuerySettingToTickets.Count > 0);
			_alreadyFetchedSettlementQueryValues = (_settlementQueryValues.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedSettlementCalendar = (_settlementCalendar != null);
			_alreadyFetchedSettlementSetup = (_settlementSetup != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SettlementCalendar":
					toReturn.Add(Relations.SettlementCalendarEntityUsingSettlementcalendarid);
					break;
				case "SettlementSetup":
					toReturn.Add(Relations.SettlementSetupEntityUsingSettlementSetupID);
					break;
				case "SettlementQuerySettingToTickets":
					toReturn.Add(Relations.SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID);
					break;
				case "SettlementQueryValues":
					toReturn.Add(Relations.SettlementQueryValueEntityUsingSettlementQuerySettingID);
					break;
				case "Tickets":
					toReturn.Add(Relations.SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID, "SettlementQuerySettingEntity__", "SettlementQuerySettingToTicket_", JoinHint.None);
					toReturn.Add(SettlementQuerySettingToTicketEntity.Relations.TicketEntityUsingTicketID, "SettlementQuerySettingToTicket_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_settlementQuerySettingToTickets", (!this.MarkedForDeletion?_settlementQuerySettingToTickets:null));
			info.AddValue("_alwaysFetchSettlementQuerySettingToTickets", _alwaysFetchSettlementQuerySettingToTickets);
			info.AddValue("_alreadyFetchedSettlementQuerySettingToTickets", _alreadyFetchedSettlementQuerySettingToTickets);
			info.AddValue("_settlementQueryValues", (!this.MarkedForDeletion?_settlementQueryValues:null));
			info.AddValue("_alwaysFetchSettlementQueryValues", _alwaysFetchSettlementQueryValues);
			info.AddValue("_alreadyFetchedSettlementQueryValues", _alreadyFetchedSettlementQueryValues);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_settlementCalendar", (!this.MarkedForDeletion?_settlementCalendar:null));
			info.AddValue("_settlementCalendarReturnsNewIfNotFound", _settlementCalendarReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSettlementCalendar", _alwaysFetchSettlementCalendar);
			info.AddValue("_alreadyFetchedSettlementCalendar", _alreadyFetchedSettlementCalendar);
			info.AddValue("_settlementSetup", (!this.MarkedForDeletion?_settlementSetup:null));
			info.AddValue("_settlementSetupReturnsNewIfNotFound", _settlementSetupReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSettlementSetup", _alwaysFetchSettlementSetup);
			info.AddValue("_alreadyFetchedSettlementSetup", _alreadyFetchedSettlementSetup);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SettlementCalendar":
					_alreadyFetchedSettlementCalendar = true;
					this.SettlementCalendar = (SettlementCalendarEntity)entity;
					break;
				case "SettlementSetup":
					_alreadyFetchedSettlementSetup = true;
					this.SettlementSetup = (SettlementSetupEntity)entity;
					break;
				case "SettlementQuerySettingToTickets":
					_alreadyFetchedSettlementQuerySettingToTickets = true;
					if(entity!=null)
					{
						this.SettlementQuerySettingToTickets.Add((SettlementQuerySettingToTicketEntity)entity);
					}
					break;
				case "SettlementQueryValues":
					_alreadyFetchedSettlementQueryValues = true;
					if(entity!=null)
					{
						this.SettlementQueryValues.Add((SettlementQueryValueEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SettlementCalendar":
					SetupSyncSettlementCalendar(relatedEntity);
					break;
				case "SettlementSetup":
					SetupSyncSettlementSetup(relatedEntity);
					break;
				case "SettlementQuerySettingToTickets":
					_settlementQuerySettingToTickets.Add((SettlementQuerySettingToTicketEntity)relatedEntity);
					break;
				case "SettlementQueryValues":
					_settlementQueryValues.Add((SettlementQueryValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SettlementCalendar":
					DesetupSyncSettlementCalendar(false, true);
					break;
				case "SettlementSetup":
					DesetupSyncSettlementSetup(false, true);
					break;
				case "SettlementQuerySettingToTickets":
					this.PerformRelatedEntityRemoval(_settlementQuerySettingToTickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SettlementQueryValues":
					this.PerformRelatedEntityRemoval(_settlementQueryValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_settlementCalendar!=null)
			{
				toReturn.Add(_settlementCalendar);
			}
			if(_settlementSetup!=null)
			{
				toReturn.Add(_settlementSetup);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_settlementQuerySettingToTickets);
			toReturn.Add(_settlementQueryValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementQuerySettingID)
		{
			return FetchUsingPK(settlementQuerySettingID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementQuerySettingID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(settlementQuerySettingID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementQuerySettingID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(settlementQuerySettingID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 settlementQuerySettingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(settlementQuerySettingID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SettlementQuerySettingID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SettlementQuerySettingRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, _settlementQuerySettingToTickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQuerySettingToTicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, _settlementQuerySettingToTickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQuerySettingToTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection GetMultiSettlementQuerySettingToTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQuerySettingToTickets || forceFetch || _alwaysFetchSettlementQuerySettingToTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQuerySettingToTickets);
				_settlementQuerySettingToTickets.SuppressClearInGetMulti=!forceFetch;
				_settlementQuerySettingToTickets.EntityFactoryToUse = entityFactoryToUse;
				_settlementQuerySettingToTickets.GetMultiManyToOne(null, this, filter);
				_settlementQuerySettingToTickets.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQuerySettingToTickets = true;
			}
			return _settlementQuerySettingToTickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQuerySettingToTickets'. These settings will be taken into account
		/// when the property SettlementQuerySettingToTickets is requested or GetMultiSettlementQuerySettingToTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQuerySettingToTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQuerySettingToTickets.SortClauses=sortClauses;
			_settlementQuerySettingToTickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQueryValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch)
		{
			return GetMultiSettlementQueryValues(forceFetch, _settlementQueryValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SettlementQueryValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSettlementQueryValues(forceFetch, _settlementQueryValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSettlementQueryValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection GetMultiSettlementQueryValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSettlementQueryValues || forceFetch || _alwaysFetchSettlementQueryValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_settlementQueryValues);
				_settlementQueryValues.SuppressClearInGetMulti=!forceFetch;
				_settlementQueryValues.EntityFactoryToUse = entityFactoryToUse;
				_settlementQueryValues.GetMultiManyToOne(this, null, filter);
				_settlementQueryValues.SuppressClearInGetMulti=false;
				_alreadyFetchedSettlementQueryValues = true;
			}
			return _settlementQueryValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'SettlementQueryValues'. These settings will be taken into account
		/// when the property SettlementQueryValues is requested or GetMultiSettlementQueryValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSettlementQueryValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_settlementQueryValues.SortClauses=sortClauses;
			_settlementQueryValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SettlementQuerySettingFields.SettlementQuerySettingID, ComparisonOperator.Equal, this.SettlementQuerySettingID, "SettlementQuerySettingEntity__"));
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMulti(filter, GetRelationsForField("Tickets"));
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SettlementCalendarEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementCalendarEntity' which is related to this entity.</returns>
		public SettlementCalendarEntity GetSingleSettlementCalendar()
		{
			return GetSingleSettlementCalendar(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementCalendarEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementCalendarEntity' which is related to this entity.</returns>
		public virtual SettlementCalendarEntity GetSingleSettlementCalendar(bool forceFetch)
		{
			if( ( !_alreadyFetchedSettlementCalendar || forceFetch || _alwaysFetchSettlementCalendar) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementCalendarEntityUsingSettlementcalendarid);
				SettlementCalendarEntity newEntity = new SettlementCalendarEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Settlementcalendarid.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SettlementCalendarEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_settlementCalendarReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SettlementCalendar = newEntity;
				_alreadyFetchedSettlementCalendar = fetchResult;
			}
			return _settlementCalendar;
		}


		/// <summary> Retrieves the related entity of type 'SettlementSetupEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SettlementSetupEntity' which is related to this entity.</returns>
		public SettlementSetupEntity GetSingleSettlementSetup()
		{
			return GetSingleSettlementSetup(false);
		}

		/// <summary> Retrieves the related entity of type 'SettlementSetupEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SettlementSetupEntity' which is related to this entity.</returns>
		public virtual SettlementSetupEntity GetSingleSettlementSetup(bool forceFetch)
		{
			if( ( !_alreadyFetchedSettlementSetup || forceFetch || _alwaysFetchSettlementSetup) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SettlementSetupEntityUsingSettlementSetupID);
				SettlementSetupEntity newEntity = new SettlementSetupEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SettlementSetupID);
				}
				if(fetchResult)
				{
					newEntity = (SettlementSetupEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_settlementSetupReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SettlementSetup = newEntity;
				_alreadyFetchedSettlementSetup = fetchResult;
			}
			return _settlementSetup;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SettlementCalendar", _settlementCalendar);
			toReturn.Add("SettlementSetup", _settlementSetup);
			toReturn.Add("SettlementQuerySettingToTickets", _settlementQuerySettingToTickets);
			toReturn.Add("SettlementQueryValues", _settlementQueryValues);
			toReturn.Add("Tickets", _tickets);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="validator">The validator object for this SettlementQuerySettingEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 settlementQuerySettingID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(settlementQuerySettingID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_settlementQuerySettingToTickets = new VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection();
			_settlementQuerySettingToTickets.SetContainingEntityInfo(this, "SettlementQuerySetting");

			_settlementQueryValues = new VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection();
			_settlementQueryValues.SetContainingEntityInfo(this, "SettlementQuerySetting");
			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_settlementCalendarReturnsNewIfNotFound = false;
			_settlementSetupReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AgencyOwningVehicle", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Operation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Settlementcalendarid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementQuerySettingID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SettlementSetupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StaticValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionTypeID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _settlementCalendar</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSettlementCalendar(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _settlementCalendar, new PropertyChangedEventHandler( OnSettlementCalendarPropertyChanged ), "SettlementCalendar", VarioSL.Entities.RelationClasses.StaticSettlementQuerySettingRelations.SettlementCalendarEntityUsingSettlementcalendaridStatic, true, signalRelatedEntity, "SettlementQuerySettings", resetFKFields, new int[] { (int)SettlementQuerySettingFieldIndex.Settlementcalendarid } );		
			_settlementCalendar = null;
		}
		
		/// <summary> setups the sync logic for member _settlementCalendar</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSettlementCalendar(IEntityCore relatedEntity)
		{
			if(_settlementCalendar!=relatedEntity)
			{		
				DesetupSyncSettlementCalendar(true, true);
				_settlementCalendar = (SettlementCalendarEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _settlementCalendar, new PropertyChangedEventHandler( OnSettlementCalendarPropertyChanged ), "SettlementCalendar", VarioSL.Entities.RelationClasses.StaticSettlementQuerySettingRelations.SettlementCalendarEntityUsingSettlementcalendaridStatic, true, ref _alreadyFetchedSettlementCalendar, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSettlementCalendarPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _settlementSetup</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSettlementSetup(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _settlementSetup, new PropertyChangedEventHandler( OnSettlementSetupPropertyChanged ), "SettlementSetup", VarioSL.Entities.RelationClasses.StaticSettlementQuerySettingRelations.SettlementSetupEntityUsingSettlementSetupIDStatic, true, signalRelatedEntity, "SettlementQuerySettings", resetFKFields, new int[] { (int)SettlementQuerySettingFieldIndex.SettlementSetupID } );		
			_settlementSetup = null;
		}
		
		/// <summary> setups the sync logic for member _settlementSetup</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSettlementSetup(IEntityCore relatedEntity)
		{
			if(_settlementSetup!=relatedEntity)
			{		
				DesetupSyncSettlementSetup(true, true);
				_settlementSetup = (SettlementSetupEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _settlementSetup, new PropertyChangedEventHandler( OnSettlementSetupPropertyChanged ), "SettlementSetup", VarioSL.Entities.RelationClasses.StaticSettlementQuerySettingRelations.SettlementSetupEntityUsingSettlementSetupIDStatic, true, ref _alreadyFetchedSettlementSetup, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSettlementSetupPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="settlementQuerySettingID">PK value for SettlementQuerySetting which data should be fetched into this SettlementQuerySetting object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 settlementQuerySettingID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SettlementQuerySettingFieldIndex.SettlementQuerySettingID].ForcedCurrentValueWrite(settlementQuerySettingID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSettlementQuerySettingDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SettlementQuerySettingEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SettlementQuerySettingRelations Relations
		{
			get	{ return new SettlementQuerySettingRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQuerySettingToTicket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQuerySettingToTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection(), (IEntityRelation)GetRelationsForField("SettlementQuerySettingToTickets")[0], (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, (int)VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity, 0, null, null, null, "SettlementQuerySettingToTickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementQueryValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementQueryValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection(), (IEntityRelation)GetRelationsForField("SettlementQueryValues")[0], (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, (int)VarioSL.Entities.EntityType.SettlementQueryValueEntity, 0, null, null, null, "SettlementQueryValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID;
				intermediateRelation.SetAliases(string.Empty, "SettlementQuerySettingToTicket_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("Tickets"), "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementCalendar'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementCalendar
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementCalendarCollection(), (IEntityRelation)GetRelationsForField("SettlementCalendar")[0], (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, (int)VarioSL.Entities.EntityType.SettlementCalendarEntity, 0, null, null, null, "SettlementCalendar", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SettlementSetup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSettlementSetup
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SettlementSetupCollection(), (IEntityRelation)GetRelationsForField("SettlementSetup")[0], (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity, (int)VarioSL.Entities.EntityType.SettlementSetupEntity, 0, null, null, null, "SettlementSetup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AgencyOwningVehicle property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."AGENCYOWNINGVEHICLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AgencyOwningVehicle
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementQuerySettingFieldIndex.AgencyOwningVehicle, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.AgencyOwningVehicle, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."DEVICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 8, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)SettlementQuerySettingFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SettlementQuerySettingFieldIndex.LastModified, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SettlementQuerySettingFieldIndex.LastUser, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Line property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."LINE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Line
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementQuerySettingFieldIndex.Line, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.Line, value, true); }
		}

		/// <summary> The Name property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SettlementQuerySettingFieldIndex.Name, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.Name, value, true); }
		}

		/// <summary> The Operation property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."OPERATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SettlementOperation Operation
		{
			get { return (VarioSL.Entities.Enumerations.SettlementOperation)GetValue((int)SettlementQuerySettingFieldIndex.Operation, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.Operation, value, true); }
		}

		/// <summary> The SalesChannel property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."SALESCHANNEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> SalesChannel
		{
			get { return (Nullable<System.Int16>)GetValue((int)SettlementQuerySettingFieldIndex.SalesChannel, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.SalesChannel, value, true); }
		}

		/// <summary> The Settlementcalendarid property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."SETTLEMENTCALENDARID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Settlementcalendarid
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementQuerySettingFieldIndex.Settlementcalendarid, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.Settlementcalendarid, value, true); }
		}

		/// <summary> The SettlementQuerySettingID property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."SETTLEMENTQUERYSETTINGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SettlementQuerySettingID
		{
			get { return (System.Int64)GetValue((int)SettlementQuerySettingFieldIndex.SettlementQuerySettingID, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.SettlementQuerySettingID, value, true); }
		}

		/// <summary> The SettlementSetupID property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."SETTLEMENTSETUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SettlementSetupID
		{
			get { return (System.Int64)GetValue((int)SettlementQuerySettingFieldIndex.SettlementSetupID, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.SettlementSetupID, value, true); }
		}

		/// <summary> The StaticValue property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."STATICVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal StaticValue
		{
			get { return (System.Decimal)GetValue((int)SettlementQuerySettingFieldIndex.StaticValue, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.StaticValue, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SettlementQuerySettingFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionTypeID property of the Entity SettlementQuerySetting<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SETTLEMENTQUERYSETTING"."TRANSACTIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SettlementQuerySettingFieldIndex.TransactionTypeID, false); }
			set	{ SetValue((int)SettlementQuerySettingFieldIndex.TransactionTypeID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SettlementQuerySettingToTicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQuerySettingToTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQuerySettingToTicketCollection SettlementQuerySettingToTickets
		{
			get	{ return GetMultiSettlementQuerySettingToTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQuerySettingToTickets. When set to true, SettlementQuerySettingToTickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQuerySettingToTickets is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQuerySettingToTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQuerySettingToTickets
		{
			get	{ return _alwaysFetchSettlementQuerySettingToTickets; }
			set	{ _alwaysFetchSettlementQuerySettingToTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQuerySettingToTickets already has been fetched. Setting this property to false when SettlementQuerySettingToTickets has been fetched
		/// will clear the SettlementQuerySettingToTickets collection well. Setting this property to true while SettlementQuerySettingToTickets hasn't been fetched disables lazy loading for SettlementQuerySettingToTickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQuerySettingToTickets
		{
			get { return _alreadyFetchedSettlementQuerySettingToTickets;}
			set 
			{
				if(_alreadyFetchedSettlementQuerySettingToTickets && !value && (_settlementQuerySettingToTickets != null))
				{
					_settlementQuerySettingToTickets.Clear();
				}
				_alreadyFetchedSettlementQuerySettingToTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SettlementQueryValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSettlementQueryValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SettlementQueryValueCollection SettlementQueryValues
		{
			get	{ return GetMultiSettlementQueryValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementQueryValues. When set to true, SettlementQueryValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementQueryValues is accessed. You can always execute/ a forced fetch by calling GetMultiSettlementQueryValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementQueryValues
		{
			get	{ return _alwaysFetchSettlementQueryValues; }
			set	{ _alwaysFetchSettlementQueryValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementQueryValues already has been fetched. Setting this property to false when SettlementQueryValues has been fetched
		/// will clear the SettlementQueryValues collection well. Setting this property to true while SettlementQueryValues hasn't been fetched disables lazy loading for SettlementQueryValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementQueryValues
		{
			get { return _alreadyFetchedSettlementQueryValues;}
			set 
			{
				if(_alreadyFetchedSettlementQueryValues && !value && (_settlementQueryValues != null))
				{
					_settlementQueryValues.Clear();
				}
				_alreadyFetchedSettlementQueryValues = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get { return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SettlementCalendarEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSettlementCalendar()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementCalendarEntity SettlementCalendar
		{
			get	{ return GetSingleSettlementCalendar(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSettlementCalendar(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementQuerySettings", "SettlementCalendar", _settlementCalendar, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementCalendar. When set to true, SettlementCalendar is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementCalendar is accessed. You can always execute a forced fetch by calling GetSingleSettlementCalendar(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementCalendar
		{
			get	{ return _alwaysFetchSettlementCalendar; }
			set	{ _alwaysFetchSettlementCalendar = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementCalendar already has been fetched. Setting this property to false when SettlementCalendar has been fetched
		/// will set SettlementCalendar to null as well. Setting this property to true while SettlementCalendar hasn't been fetched disables lazy loading for SettlementCalendar</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementCalendar
		{
			get { return _alreadyFetchedSettlementCalendar;}
			set 
			{
				if(_alreadyFetchedSettlementCalendar && !value)
				{
					this.SettlementCalendar = null;
				}
				_alreadyFetchedSettlementCalendar = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SettlementCalendar is not found
		/// in the database. When set to true, SettlementCalendar will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SettlementCalendarReturnsNewIfNotFound
		{
			get	{ return _settlementCalendarReturnsNewIfNotFound; }
			set { _settlementCalendarReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SettlementSetupEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSettlementSetup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SettlementSetupEntity SettlementSetup
		{
			get	{ return GetSingleSettlementSetup(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSettlementSetup(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SettlementQuerySettings", "SettlementSetup", _settlementSetup, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SettlementSetup. When set to true, SettlementSetup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SettlementSetup is accessed. You can always execute a forced fetch by calling GetSingleSettlementSetup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSettlementSetup
		{
			get	{ return _alwaysFetchSettlementSetup; }
			set	{ _alwaysFetchSettlementSetup = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SettlementSetup already has been fetched. Setting this property to false when SettlementSetup has been fetched
		/// will set SettlementSetup to null as well. Setting this property to true while SettlementSetup hasn't been fetched disables lazy loading for SettlementSetup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSettlementSetup
		{
			get { return _alreadyFetchedSettlementSetup;}
			set 
			{
				if(_alreadyFetchedSettlementSetup && !value)
				{
					this.SettlementSetup = null;
				}
				_alreadyFetchedSettlementSetup = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SettlementSetup is not found
		/// in the database. When set to true, SettlementSetup will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SettlementSetupReturnsNewIfNotFound
		{
			get	{ return _settlementSetupReturnsNewIfNotFound; }
			set { _settlementSetupReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SettlementQuerySettingEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
